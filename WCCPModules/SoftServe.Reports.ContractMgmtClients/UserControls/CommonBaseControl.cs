﻿using System;
using System.Windows.Forms;
using SoftServe.Reports.ContractMgmtClients;
using System.Collections.Generic;
using DevExpress.XtraEditors.DXErrorProvider;
using DevExpress.XtraEditors;
using Logica.Reports.BaseReportControl.Utility;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraTreeList;
using DevExpress.XtraTreeList.Columns;
using Logica.Reports.BaseReportControl.CommonControls;
using Logica.Reports.BaseReportControl.CommonControls.Addresses.DataAccess;
using Logica.Reports.BaseReportControl;

namespace SoftServe.Reports.ContractMgmtClients
{
    /// <summary>
    /// 
    /// </summary>
    public partial class CommonBaseControl : UserControl
    {
        private IList<Control> controlsToRemove = new List<Control>();
        internal CommonBaseExternalData MainData { get; set; }
        public event EventHandler UpdateMainView;
        DXErrorProvider errorProvider = new DXErrorProvider();

        /// <summary>
        /// Initializes a new instance of the <see cref="CommonBaseControl"/> class.
        /// </summary>
        public CommonBaseControl()
        {
            MainData = new CommonBaseExternalData();
            errorProvider.ContainerControl = this;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CommonBaseControl"/> class.
        /// </summary>
        /// <param name="parentTab">The parent tab.</param>
        public CommonBaseControl(CommonBaseTab parentTab)
        {
            MainData = new CommonBaseExternalData();
            ParentTab = parentTab;
            errorProvider.ContainerControl = this;            
        }

        /// <summary>
        /// Loads the data.
        /// </summary>
        /// <param name="clientId">The client id.</param>
        /// <param name="firstAddressId">The first address id.</param>
        public virtual void LoadData(int clientId, long firstAddressId)
        {
            ClearErrors();

            UpdateReadOnlyMode();
        }

        /// <summary>
        /// Clears the errors.
        /// </summary>
        internal void ClearErrors()
        {
            if (errorProvider != null)
            {
                errorProvider.ClearErrors();
            }
        }

        /// <summary>
        /// Validates the data and returns ValidateMetadata object that has IsCorrect property, that indicates whether validation is passed.
        /// Also always returns null Message property in order to give possibility for derived classes customize ValidateMetadata object. 
        /// </summary>
        /// <returns></returns>
        internal virtual ValidateMetadata ValidateData()
        {
            ClearErrors();
            return new ValidateMetadata(ValidateChildren(ValidationConstraints.Selectable), null);
        }

        /// <summary>
        /// Sets the error.
        /// </summary>
        /// <param name="control">The control.</param>
        /// <param name="errorText">The error text.</param>
        internal void SetError(Control control, string errorText)
        {
            if (errorProvider != null && control != null && !string.IsNullOrEmpty(errorText))
            {
                errorProvider.SetError(control, errorText);
            }
        }

        /// <summary>
        /// Fires the update main view.
        /// </summary>
        protected void FireUpdateMainView()
        {
            if (UpdateMainView != null)
            {
                UpdateMainView(this, new EventArgs());
            }
        }

        /// <summary>
        /// Gets or sets the parent tab.
        /// </summary>
        /// <value>The parent tab.</value>
        public CommonBaseTab ParentTab
        {
            get;
            set;
        }

        /// <summary>
        /// Updates the read only mode.
        /// </summary>
        public virtual void UpdateReadOnlyMode()
        {
            if (!MainData.CheckAccessLevel(AccessLevelType.ReadWrite) || !MainData.HasAccessToAllAddresses)
            {
                SetReadOnlyModeRecursively(this);
            }
        }

        /// <summary>
        /// Sets the read only mode recursively.
        /// </summary>
        /// <param name="currentControl">The current control.</param>
        protected void SetReadOnlyModeRecursively(Control currentControl)
        {
            if (currentControl != null)
            {
                foreach (Control control in currentControl.Controls)
                {
                    if (control != null)
                    {
                        if (control is ManageAddressesList)
                        {
                            ManageAddressesList addressControl = control as ManageAddressesList;
                            addressControl.EditMode = AddressEditMode.ReadOnly;
                        }
                        else if (control is BaseEdit)
                        {
                            bool isContractsDropDown = false;
                            if (control.Tag != null)
                            {
                                isContractsDropDown = control.Tag.ToString() == Constants.CD_TAG_CONTRACTS_DROP_DOWN; //Checking whether control is drop-down list with contracts
                            }
                                (control as BaseEdit).Properties.AllowFocused = isContractsDropDown;
                                (control as BaseEdit).Properties.ReadOnly = !isContractsDropDown;
                        }
                        else if (control is IControlReadOnly)
                        {
                            (control as IControlReadOnly).ReadOnly = true;
                        }
                        else if (control is GridControl)
                        {
                            GridControl gridControl = control as GridControl;
                            foreach (GridView view in gridControl.Views)
                            {
                                gridControl.UseEmbeddedNavigator = false;
                                foreach (GridColumn column in view.Columns)
                                {
                                    column.OptionsColumn.AllowEdit = false;
                                    column.OptionsColumn.ReadOnly = true;
                                }
                            }
                        }
                        else if (control is TreeList)
                        {
                            TreeList treeList = control as TreeList;
                            foreach (TreeListColumn column in treeList.Columns)
                            {
                                column.OptionsColumn.AllowEdit = false;
                                column.OptionsColumn.ReadOnly = true;
                            }
                        }
                        else if (control.HasChildren)
                        {
                            SetReadOnlyModeRecursively(control);
                        }
                    }
                }
            }
        }

        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // CommonBaseControl
            // 
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.Name = "CommonBaseControl";
            this.ResumeLayout(false);

        }

        /// <summary>
        /// Virtual method that you may override to trigger some visual updates in derieved control.
        /// </summary>
       public virtual void UpdateControlVisualState()  { }
    }
}
