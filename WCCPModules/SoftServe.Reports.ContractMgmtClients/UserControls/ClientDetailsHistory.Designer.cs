﻿namespace SoftServe.Reports.ContractMgmtClients
{
    partial class ClientDetailsHistory
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition1 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition2 = new DevExpress.XtraGrid.StyleFormatCondition();
            this.gwContractHistory = new DevExpress.XtraGrid.GridControl();
            this.gridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.column1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.column2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.column3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.column12 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryStatusLookUp = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.column4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.column5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.column6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.column7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.column8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.column9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.column10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.column11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryTopicEdit = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryActionLookUp = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.repositoryApproveCheck = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryCommentActionEdit = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryResponsibleLookUp = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.repositoryCommentStatusEdit = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryPOCMoveToookUp = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.repositoryItemDateEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            ((System.ComponentModel.ISupportInitialize)(this.gwContractHistory)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryStatusLookUp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryTopicEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryActionLookUp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryApproveCheck)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryCommentActionEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryResponsibleLookUp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryCommentStatusEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryPOCMoveToookUp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1.VistaTimeProperties)).BeginInit();
            this.SuspendLayout();
            // 
            // gwContractHistory
            // 
            this.gwContractHistory.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gwContractHistory.Location = new System.Drawing.Point(0, 0);
            this.gwContractHistory.MainView = this.gridView;
            this.gwContractHistory.Name = "gwContractHistory";
            this.gwContractHistory.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryTopicEdit,
            this.repositoryActionLookUp,
            this.repositoryApproveCheck,
            this.repositoryCommentActionEdit,
            this.repositoryResponsibleLookUp,
            this.repositoryStatusLookUp,
            this.repositoryCommentStatusEdit,
            this.repositoryPOCMoveToookUp,
            this.repositoryItemDateEdit1});
            this.gwContractHistory.Size = new System.Drawing.Size(798, 330);
            this.gwContractHistory.TabIndex = 5;
            this.gwContractHistory.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView});
            // 
            // gridView
            // 
            this.gridView.ColumnPanelRowHeight = 35;
            this.gridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.column1,
            this.column2,
            this.column3,
            this.column4,
            this.column5,
            this.column6,
            this.column7,
            this.column8,
            this.column9,
            this.column10,
            this.column11,
            this.column12});
            styleFormatCondition1.Appearance.ForeColor = System.Drawing.Color.Gray;
            styleFormatCondition1.Appearance.Options.UseForeColor = true;
            styleFormatCondition1.ApplyToRow = true;
            styleFormatCondition1.Condition = DevExpress.XtraGrid.FormatConditionEnum.Expression;
            styleFormatCondition1.Expression = "[VALID_TILL] < Today()";
            styleFormatCondition2.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(177)))), ((int)(((byte)(100)))));
            styleFormatCondition2.Appearance.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(177)))), ((int)(((byte)(100)))));
            styleFormatCondition2.Appearance.Options.UseBackColor = true;
            styleFormatCondition2.ApplyToRow = true;
            styleFormatCondition2.Condition = DevExpress.XtraGrid.FormatConditionEnum.Expression;
            styleFormatCondition2.Expression = "AddDays(Today(), 40) > [VALID_TILL]";
            styleFormatCondition2.Tag = "validTill";
            this.gridView.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition1,
            styleFormatCondition2});
            this.gridView.GridControl = this.gwContractHistory;
            this.gridView.Name = "gridView";
            this.gridView.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView.OptionsView.ShowAutoFilterRow = true;
            this.gridView.OptionsView.ShowGroupPanel = false;
            this.gridView.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.column2, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // column1
            // 
            this.column1.AppearanceHeader.Options.UseTextOptions = true;
            this.column1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.column1.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.column1.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.column1.Caption = "№ контракта";
            this.column1.FieldName = "CONTRACTNO";
            this.column1.Name = "column1";
            this.column1.OptionsColumn.AllowEdit = false;
            this.column1.OptionsColumn.AllowMove = false;
            this.column1.OptionsColumn.ReadOnly = true;
            this.column1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.column1.Visible = true;
            this.column1.VisibleIndex = 0;
            this.column1.Width = 50;
            // 
            // column2
            // 
            this.column2.AppearanceHeader.Options.UseTextOptions = true;
            this.column2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.column2.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.column2.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.column2.Caption = "Дата подписания";
            this.column2.FieldName = "DATE_SIGNED";
            this.column2.Name = "column2";
            this.column2.OptionsColumn.AllowEdit = false;
            this.column2.OptionsColumn.AllowMove = false;
            this.column2.OptionsColumn.ReadOnly = true;
            this.column2.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.column2.Visible = true;
            this.column2.VisibleIndex = 1;
            this.column2.Width = 71;
            // 
            // column3
            // 
            this.column3.AppearanceHeader.Options.UseTextOptions = true;
            this.column3.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.column3.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.column3.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.column3.Caption = "Дата окончания";
            this.column3.FieldName = "VALID_TILL";
            this.column3.Name = "column3";
            this.column3.OptionsColumn.AllowEdit = false;
            this.column3.OptionsColumn.AllowMove = false;
            this.column3.OptionsColumn.ReadOnly = true;
            this.column3.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.column3.Visible = true;
            this.column3.VisibleIndex = 2;
            this.column3.Width = 71;
            // 
            // column12
            // 
            this.column12.Caption = "Статус";
            this.column12.ColumnEdit = this.repositoryStatusLookUp;
            this.column12.FieldName = "CONTRACT_STATUS_ID";
            this.column12.Name = "column12";
            this.column12.Visible = true;
            this.column12.VisibleIndex = 11;
            // 
            // repositoryStatusLookUp
            // 
            this.repositoryStatusLookUp.AutoHeight = false;
            this.repositoryStatusLookUp.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryStatusLookUp.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("ID", "ID", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("NAME", "Статус")});
            this.repositoryStatusLookUp.DisplayMember = "NAME";
            this.repositoryStatusLookUp.Name = "repositoryStatusLookUp";
            this.repositoryStatusLookUp.NullText = "";
            this.repositoryStatusLookUp.ValueMember = "ID";
            // 
            // column4
            // 
            this.column4.AppearanceHeader.Options.UseTextOptions = true;
            this.column4.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.column4.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.column4.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.column4.Caption = "Сумма контракта факт, грн";
            this.column4.DisplayFormat.FormatString = "{0:N2}";
            this.column4.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.column4.FieldName = "ContractSumFact";
            this.column4.Name = "column4";
            this.column4.OptionsColumn.AllowEdit = false;
            this.column4.OptionsColumn.AllowMove = false;
            this.column4.OptionsColumn.ReadOnly = true;
            this.column4.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.column4.Visible = true;
            this.column4.VisibleIndex = 3;
            this.column4.Width = 71;
            // 
            // column5
            // 
            this.column5.AppearanceCell.Options.UseTextOptions = true;
            this.column5.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.column5.AppearanceHeader.Options.UseTextOptions = true;
            this.column5.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.column5.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.column5.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.column5.Caption = "Ретробонус, %";
            this.column5.DisplayFormat.FormatString = "{0:N2}";
            this.column5.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.column5.FieldName = "PLANNED_RETROBONUSE";
            this.column5.Name = "column5";
            this.column5.OptionsColumn.AllowEdit = false;
            this.column5.OptionsColumn.AllowMove = false;
            this.column5.OptionsColumn.ReadOnly = true;
            this.column5.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.column5.Visible = true;
            this.column5.VisibleIndex = 4;
            this.column5.Width = 71;
            // 
            // column6
            // 
            this.column6.AppearanceHeader.Options.UseTextOptions = true;
            this.column6.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.column6.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.column6.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.column6.Caption = "Налог на рекламу факт, грн";
            this.column6.DisplayFormat.FormatString = "{0:N2}";
            this.column6.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.column6.FieldName = "AdvertisementTax";
            this.column6.Name = "column6";
            this.column6.OptionsColumn.AllowEdit = false;
            this.column6.OptionsColumn.AllowMove = false;
            this.column6.OptionsColumn.ReadOnly = true;
            this.column6.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.column6.Visible = true;
            this.column6.VisibleIndex = 5;
            this.column6.Width = 71;
            // 
            // column7
            // 
            this.column7.AppearanceHeader.Options.UseTextOptions = true;
            this.column7.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.column7.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.column7.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.column7.Caption = "НДС факт, грн";
            this.column7.DisplayFormat.FormatString = "{0:N2}";
            this.column7.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.column7.FieldName = "VAT";
            this.column7.Name = "column7";
            this.column7.OptionsColumn.AllowEdit = false;
            this.column7.OptionsColumn.AllowMove = false;
            this.column7.OptionsColumn.ReadOnly = true;
            this.column7.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.column7.Visible = true;
            this.column7.VisibleIndex = 6;
            this.column7.Width = 71;
            // 
            // column8
            // 
            this.column8.AppearanceHeader.Options.UseTextOptions = true;
            this.column8.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.column8.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.column8.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.column8.Caption = "Общая сумма факт, грн";
            this.column8.DisplayFormat.FormatString = "{0:N2}";
            this.column8.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.column8.FieldName = "TotalSumFact";
            this.column8.Name = "column8";
            this.column8.OptionsColumn.AllowEdit = false;
            this.column8.OptionsColumn.AllowMove = false;
            this.column8.OptionsColumn.ReadOnly = true;
            this.column8.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.column8.Visible = true;
            this.column8.VisibleIndex = 7;
            this.column8.Width = 71;
            // 
            // column9
            // 
            this.column9.AppearanceHeader.Options.UseTextOptions = true;
            this.column9.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.column9.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.column9.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.column9.Caption = "План V, гл";
            this.column9.DisplayFormat.FormatString = "{0:N2}";
            this.column9.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.column9.FieldName = "VPlan";
            this.column9.Name = "column9";
            this.column9.OptionsColumn.AllowEdit = false;
            this.column9.OptionsColumn.AllowMove = false;
            this.column9.OptionsColumn.ReadOnly = true;
            this.column9.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.column9.Visible = true;
            this.column9.VisibleIndex = 8;
            this.column9.Width = 71;
            // 
            // column10
            // 
            this.column10.AppearanceHeader.Options.UseTextOptions = true;
            this.column10.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.column10.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.column10.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.column10.Caption = "Факт. V, гл";
            this.column10.DisplayFormat.FormatString = "{0:N2}";
            this.column10.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.column10.FieldName = "VFact";
            this.column10.Name = "column10";
            this.column10.OptionsColumn.AllowEdit = false;
            this.column10.OptionsColumn.AllowMove = false;
            this.column10.OptionsColumn.ReadOnly = true;
            this.column10.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.column10.Visible = true;
            this.column10.VisibleIndex = 9;
            this.column10.Width = 71;
            // 
            // column11
            // 
            this.column11.AppearanceHeader.Options.UseTextOptions = true;
            this.column11.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.column11.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.column11.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.column11.Caption = "Общая сумма план, грн";
            this.column11.DisplayFormat.FormatString = "{0:N2}";
            this.column11.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.column11.FieldName = "TotalSumPlan";
            this.column11.Name = "column11";
            this.column11.OptionsColumn.AllowEdit = false;
            this.column11.OptionsColumn.AllowMove = false;
            this.column11.OptionsColumn.ReadOnly = true;
            this.column11.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.column11.Visible = true;
            this.column11.VisibleIndex = 10;
            this.column11.Width = 88;
            // 
            // repositoryTopicEdit
            // 
            this.repositoryTopicEdit.AutoHeight = false;
            this.repositoryTopicEdit.Name = "repositoryTopicEdit";
            // 
            // repositoryActionLookUp
            // 
            this.repositoryActionLookUp.AutoHeight = false;
            this.repositoryActionLookUp.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryActionLookUp.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("ID", "ID", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("ActionName", "Action")});
            this.repositoryActionLookUp.DisplayMember = "ActionName";
            this.repositoryActionLookUp.Name = "repositoryActionLookUp";
            this.repositoryActionLookUp.NullText = "Не задано";
            this.repositoryActionLookUp.ValueMember = "ID";
            // 
            // repositoryApproveCheck
            // 
            this.repositoryApproveCheck.AutoHeight = false;
            this.repositoryApproveCheck.Name = "repositoryApproveCheck";
            // 
            // repositoryCommentActionEdit
            // 
            this.repositoryCommentActionEdit.AutoHeight = false;
            this.repositoryCommentActionEdit.Name = "repositoryCommentActionEdit";
            // 
            // repositoryResponsibleLookUp
            // 
            this.repositoryResponsibleLookUp.AutoHeight = false;
            this.repositoryResponsibleLookUp.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryResponsibleLookUp.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Supervisor_ID", "ID", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Supervisor_name", "M2")});
            this.repositoryResponsibleLookUp.DisplayMember = "Supervisor_name";
            this.repositoryResponsibleLookUp.Name = "repositoryResponsibleLookUp";
            this.repositoryResponsibleLookUp.NullText = "Не задан";
            this.repositoryResponsibleLookUp.ValueMember = "Supervisor_ID";
            // 
            // repositoryCommentStatusEdit
            // 
            this.repositoryCommentStatusEdit.AutoHeight = false;
            this.repositoryCommentStatusEdit.Name = "repositoryCommentStatusEdit";
            // 
            // repositoryPOCMoveToookUp
            // 
            this.repositoryPOCMoveToookUp.AutoHeight = false;
            this.repositoryPOCMoveToookUp.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryPOCMoveToookUp.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("ID", "ID", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("POCName", "ТТ")});
            this.repositoryPOCMoveToookUp.DisplayMember = "POCName";
            this.repositoryPOCMoveToookUp.Name = "repositoryPOCMoveToookUp";
            this.repositoryPOCMoveToookUp.NullText = "Без перемещения";
            this.repositoryPOCMoveToookUp.ValueMember = "ID";
            // 
            // repositoryItemDateEdit1
            // 
            this.repositoryItemDateEdit1.AutoHeight = false;
            this.repositoryItemDateEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit1.Name = "repositoryItemDateEdit1";
            this.repositoryItemDateEdit1.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            // 
            // ClientDetailsHistory
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gwContractHistory);
            this.Name = "ClientDetailsHistory";
            this.Size = new System.Drawing.Size(798, 330);
            ((System.ComponentModel.ISupportInitialize)(this.gwContractHistory)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryStatusLookUp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryTopicEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryActionLookUp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryApproveCheck)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryCommentActionEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryResponsibleLookUp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryCommentStatusEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryPOCMoveToookUp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gwContractHistory;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView;
        private DevExpress.XtraGrid.Columns.GridColumn column1;
        private DevExpress.XtraGrid.Columns.GridColumn column2;
        private DevExpress.XtraGrid.Columns.GridColumn column3;
        private DevExpress.XtraGrid.Columns.GridColumn column4;
        private DevExpress.XtraGrid.Columns.GridColumn column5;
        private DevExpress.XtraGrid.Columns.GridColumn column6;
        private DevExpress.XtraGrid.Columns.GridColumn column7;
        private DevExpress.XtraGrid.Columns.GridColumn column8;
        private DevExpress.XtraGrid.Columns.GridColumn column9;
        private DevExpress.XtraGrid.Columns.GridColumn column10;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryTopicEdit;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryActionLookUp;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryApproveCheck;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryCommentActionEdit;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryResponsibleLookUp;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryStatusLookUp;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryCommentStatusEdit;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryPOCMoveToookUp;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn column11;
        private DevExpress.XtraGrid.Columns.GridColumn column12;
    }
}
