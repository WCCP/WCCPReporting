﻿namespace SoftServe.Reports.ContractMgmtClients
{
    partial class ClientDetailsCommon
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupSalesAverage = new DevExpress.XtraEditors.GroupControl();
            this.monthlySalesCtrl = new Logica.Reports.BaseReportControl.CommonControls.Editors.MonthlySales();
            this.groupDistributors2 = new DevExpress.XtraEditors.GroupControl();
            this.listDistributorsOn = new DevExpress.XtraEditors.ListBoxControl();
            this.groupContactPerson = new DevExpress.XtraEditors.GroupControl();
            this.labelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.labelCPEmail = new DevExpress.XtraEditors.LabelControl();
            this.tbCPEmail = new DevExpress.XtraEditors.TextEdit();
            this.labelCPMobilePhone = new DevExpress.XtraEditors.LabelControl();
            this.labelCPPhone = new DevExpress.XtraEditors.LabelControl();
            this.labelCPName = new DevExpress.XtraEditors.LabelControl();
            this.labelCPPosition = new DevExpress.XtraEditors.LabelControl();
            this.tbCPMobilePhone = new DevExpress.XtraEditors.TextEdit();
            this.tbCPPhone = new DevExpress.XtraEditors.TextEdit();
            this.tbCPName = new DevExpress.XtraEditors.TextEdit();
            this.tbCPPosition = new DevExpress.XtraEditors.TextEdit();
            this.cbPersonTaxList = new DevExpress.XtraEditors.LookUpEdit();
            this.cbPersonCityType = new DevExpress.XtraEditors.LookUpEdit();
            this.cbPersonCity = new DevExpress.XtraEditors.LookUpEdit();
            this.cbPersonRegion = new DevExpress.XtraEditors.LookUpEdit();
            this.cbPersonDistrict = new DevExpress.XtraEditors.LookUpEdit();
            this.memoPersonAddress = new DevExpress.XtraEditors.MemoEdit();
            this.tbPersonName = new DevExpress.XtraEditors.TextEdit();
            this.tbEDRPOU = new DevExpress.XtraEditors.TextEdit();
            this.tbBookeeperName = new DevExpress.XtraEditors.TextEdit();
            this.tbINN = new DevExpress.XtraEditors.TextEdit();
            this.tbBookkeeperPhone = new DevExpress.XtraEditors.TextEdit();
            this.labelPersonName = new DevExpress.XtraEditors.LabelControl();
            this.memoPersonNotes = new DevExpress.XtraEditors.MemoEdit();
            this.labelPersonCode = new DevExpress.XtraEditors.LabelControl();
            this.labelPersonRegion = new DevExpress.XtraEditors.LabelControl();
            this.labelPersonAddress = new DevExpress.XtraEditors.LabelControl();
            this.labelTaxType = new DevExpress.XtraEditors.LabelControl();
            this.labelBookkeeperName = new DevExpress.XtraEditors.LabelControl();
            this.labelINN = new DevExpress.XtraEditors.LabelControl();
            this.labelPersonDistrict = new DevExpress.XtraEditors.LabelControl();
            this.labelCityType = new DevExpress.XtraEditors.LabelControl();
            this.labelCity = new DevExpress.XtraEditors.LabelControl();
            this.labelBookkeeperPhone = new DevExpress.XtraEditors.LabelControl();
            this.labelNotes = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelINNIsRequired = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl13 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl14 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl16 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.groupPerson = new DevExpress.XtraEditors.GroupControl();
            this.groupDistributors1 = new DevExpress.XtraEditors.GroupControl();
            this.listDistributorsOff = new DevExpress.XtraEditors.ListBoxControl();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.labelFixator = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lblCreator = new System.Windows.Forms.Label();
            this.gridControlPOCList = new DevExpress.XtraGrid.GridControl();
            this.gridViewPOCList = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnPOCPremiality = new DevExpress.XtraGrid.Columns.GridColumn();
            this.xtraScrollableControl1 = new DevExpress.XtraEditors.XtraScrollableControl();
            ((System.ComponentModel.ISupportInitialize)(this.groupSalesAverage)).BeginInit();
            this.groupSalesAverage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupDistributors2)).BeginInit();
            this.groupDistributors2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.listDistributorsOn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupContactPerson)).BeginInit();
            this.groupContactPerson.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbCPEmail.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbCPMobilePhone.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbCPPhone.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbCPName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbCPPosition.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbPersonTaxList.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbPersonCityType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbPersonCity.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbPersonRegion.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbPersonDistrict.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoPersonAddress.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbPersonName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbEDRPOU.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbBookeeperName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbINN.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbBookkeeperPhone.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoPersonNotes.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupPerson)).BeginInit();
            this.groupPerson.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupDistributors1)).BeginInit();
            this.groupDistributors1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.listDistributorsOff)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlPOCList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewPOCList)).BeginInit();
            this.xtraScrollableControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupSalesAverage
            // 
            this.groupSalesAverage.AppearanceCaption.Options.UseTextOptions = true;
            this.groupSalesAverage.AppearanceCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.groupSalesAverage.Controls.Add(this.monthlySalesCtrl);
            this.groupSalesAverage.Location = new System.Drawing.Point(433, 37);
            this.groupSalesAverage.Name = "groupSalesAverage";
            this.groupSalesAverage.Size = new System.Drawing.Size(588, 413);
            this.groupSalesAverage.TabIndex = 7;
            this.groupSalesAverage.Text = "Месячные средние продажи пива (за последние 12 мес.) в даллах";
            // 
            // monthlySalesCtrl
            // 
            this.monthlySalesCtrl.DataSource = null;
            this.monthlySalesCtrl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.monthlySalesCtrl.Location = new System.Drawing.Point(2, 21);
            this.monthlySalesCtrl.Name = "monthlySalesCtrl";
            this.monthlySalesCtrl.ReadOnly = true;
            this.monthlySalesCtrl.Size = new System.Drawing.Size(584, 390);
            this.monthlySalesCtrl.TabIndex = 2;
            // 
            // groupDistributors2
            // 
            this.groupDistributors2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.groupDistributors2.AppearanceCaption.Options.UseTextOptions = true;
            this.groupDistributors2.AppearanceCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.groupDistributors2.Controls.Add(this.listDistributorsOn);
            this.groupDistributors2.Location = new System.Drawing.Point(291, 6);
            this.groupDistributors2.Margin = new System.Windows.Forms.Padding(3, 3, 3, 30);
            this.groupDistributors2.Name = "groupDistributors2";
            this.groupDistributors2.Size = new System.Drawing.Size(291, 231);
            this.groupDistributors2.TabIndex = 0;
            this.groupDistributors2.Text = "Дистрибьюторы кега";
            // 
            // listDistributorsOn
            // 
            this.listDistributorsOn.DisplayMember = "DISTR_NAME";
            this.listDistributorsOn.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listDistributorsOn.Location = new System.Drawing.Point(2, 21);
            this.listDistributorsOn.Name = "listDistributorsOn";
            this.listDistributorsOn.Size = new System.Drawing.Size(287, 208);
            this.listDistributorsOn.TabIndex = 0;
            this.listDistributorsOn.ValueMember = "DISTR_ID";
            // 
            // groupContactPerson
            // 
            this.groupContactPerson.AppearanceCaption.Options.UseTextOptions = true;
            this.groupContactPerson.AppearanceCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.groupContactPerson.Controls.Add(this.labelControl11);
            this.groupContactPerson.Controls.Add(this.labelControl10);
            this.groupContactPerson.Controls.Add(this.labelControl9);
            this.groupContactPerson.Controls.Add(this.labelCPEmail);
            this.groupContactPerson.Controls.Add(this.tbCPEmail);
            this.groupContactPerson.Controls.Add(this.labelCPMobilePhone);
            this.groupContactPerson.Controls.Add(this.labelCPPhone);
            this.groupContactPerson.Controls.Add(this.labelCPName);
            this.groupContactPerson.Controls.Add(this.labelCPPosition);
            this.groupContactPerson.Controls.Add(this.tbCPMobilePhone);
            this.groupContactPerson.Controls.Add(this.tbCPPhone);
            this.groupContactPerson.Controls.Add(this.tbCPName);
            this.groupContactPerson.Controls.Add(this.tbCPPosition);
            this.groupContactPerson.Location = new System.Drawing.Point(3, 453);
            this.groupContactPerson.Name = "groupContactPerson";
            this.groupContactPerson.Size = new System.Drawing.Size(195, 253);
            this.groupContactPerson.TabIndex = 4;
            this.groupContactPerson.Text = "Контактное лицо";
            // 
            // labelControl11
            // 
            this.labelControl11.Appearance.ForeColor = System.Drawing.Color.Red;
            this.labelControl11.Location = new System.Drawing.Point(74, 162);
            this.labelControl11.Name = "labelControl11";
            this.labelControl11.Size = new System.Drawing.Size(6, 13);
            this.labelControl11.TabIndex = 15;
            this.labelControl11.Text = "*";
            // 
            // labelControl10
            // 
            this.labelControl10.Appearance.ForeColor = System.Drawing.Color.Red;
            this.labelControl10.Location = new System.Drawing.Point(60, 116);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(6, 13);
            this.labelControl10.TabIndex = 14;
            this.labelControl10.Text = "*";
            // 
            // labelControl9
            // 
            this.labelControl9.Appearance.ForeColor = System.Drawing.Color.Red;
            this.labelControl9.Location = new System.Drawing.Point(39, 69);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(6, 13);
            this.labelControl9.TabIndex = 13;
            this.labelControl9.Text = "*";
            // 
            // labelCPEmail
            // 
            this.labelCPEmail.Location = new System.Drawing.Point(6, 208);
            this.labelCPEmail.Name = "labelCPEmail";
            this.labelCPEmail.Size = new System.Drawing.Size(28, 13);
            this.labelCPEmail.TabIndex = 6;
            this.labelCPEmail.Text = "Email:";
            // 
            // tbCPEmail
            // 
            this.tbCPEmail.Location = new System.Drawing.Point(6, 227);
            this.tbCPEmail.Name = "tbCPEmail";
            this.tbCPEmail.Properties.Mask.EditMask = "[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,4}";
            this.tbCPEmail.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Regular;
            this.tbCPEmail.Size = new System.Drawing.Size(170, 20);
            this.tbCPEmail.TabIndex = 5;
            // 
            // labelCPMobilePhone
            // 
            this.labelCPMobilePhone.Location = new System.Drawing.Point(6, 162);
            this.labelCPMobilePhone.Name = "labelCPMobilePhone";
            this.labelCPMobilePhone.Size = new System.Drawing.Size(62, 13);
            this.labelCPMobilePhone.TabIndex = 4;
            this.labelCPMobilePhone.Text = "Мобильный:";
            // 
            // labelCPPhone
            // 
            this.labelCPPhone.Location = new System.Drawing.Point(6, 116);
            this.labelCPPhone.Name = "labelCPPhone";
            this.labelCPPhone.Size = new System.Drawing.Size(48, 13);
            this.labelCPPhone.TabIndex = 4;
            this.labelCPPhone.Text = "Телефон:";
            // 
            // labelCPName
            // 
            this.labelCPName.Location = new System.Drawing.Point(6, 69);
            this.labelCPName.Name = "labelCPName";
            this.labelCPName.Size = new System.Drawing.Size(27, 13);
            this.labelCPName.TabIndex = 4;
            this.labelCPName.Text = "ФИО:";
            // 
            // labelCPPosition
            // 
            this.labelCPPosition.Location = new System.Drawing.Point(6, 22);
            this.labelCPPosition.Name = "labelCPPosition";
            this.labelCPPosition.Size = new System.Drawing.Size(61, 13);
            this.labelCPPosition.TabIndex = 4;
            this.labelCPPosition.Text = "Должность:";
            // 
            // tbCPMobilePhone
            // 
            this.tbCPMobilePhone.Location = new System.Drawing.Point(6, 180);
            this.tbCPMobilePhone.Name = "tbCPMobilePhone";
            this.tbCPMobilePhone.Properties.Mask.EditMask = "[\\d \\( \\) \\- \\s]*";
            this.tbCPMobilePhone.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Regular;
            this.tbCPMobilePhone.Size = new System.Drawing.Size(170, 20);
            this.tbCPMobilePhone.TabIndex = 3;
            this.tbCPMobilePhone.Validating += new System.ComponentModel.CancelEventHandler(this.OnValidating);
            // 
            // tbCPPhone
            // 
            this.tbCPPhone.Location = new System.Drawing.Point(6, 135);
            this.tbCPPhone.Name = "tbCPPhone";
            this.tbCPPhone.Properties.Mask.EditMask = "[\\d \\( \\) \\- \\s]*";
            this.tbCPPhone.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Regular;
            this.tbCPPhone.Size = new System.Drawing.Size(170, 20);
            this.tbCPPhone.TabIndex = 3;
            this.tbCPPhone.Validating += new System.ComponentModel.CancelEventHandler(this.OnValidating);
            // 
            // tbCPName
            // 
            this.tbCPName.Location = new System.Drawing.Point(6, 88);
            this.tbCPName.Name = "tbCPName";
            this.tbCPName.Size = new System.Drawing.Size(170, 20);
            this.tbCPName.TabIndex = 3;
            this.tbCPName.Validating += new System.ComponentModel.CancelEventHandler(this.OnValidating);
            // 
            // tbCPPosition
            // 
            this.tbCPPosition.Location = new System.Drawing.Point(6, 41);
            this.tbCPPosition.Name = "tbCPPosition";
            this.tbCPPosition.Size = new System.Drawing.Size(170, 20);
            this.tbCPPosition.TabIndex = 3;
            // 
            // cbPersonTaxList
            // 
            this.cbPersonTaxList.Location = new System.Drawing.Point(6, 134);
            this.cbPersonTaxList.Name = "cbPersonTaxList";
            this.cbPersonTaxList.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbPersonTaxList.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("CODE", "Название")});
            this.cbPersonTaxList.Properties.DisplayMember = "CODE";
            this.cbPersonTaxList.Properties.NullText = "<Не задано>";
            this.cbPersonTaxList.Properties.ValueMember = "ID";
            this.cbPersonTaxList.Size = new System.Drawing.Size(216, 20);
            this.cbPersonTaxList.TabIndex = 1;
            this.cbPersonTaxList.EditValueChanged += new System.EventHandler(this.cbPersonTaxList_EditValueChanged);
            this.cbPersonTaxList.MouseDown += new System.Windows.Forms.MouseEventHandler(this.cbPersonTaxList_MouseDown);
            this.cbPersonTaxList.Validating += new System.ComponentModel.CancelEventHandler(this.OnValidating);
            // 
            // cbPersonCityType
            // 
            this.cbPersonCityType.Location = new System.Drawing.Point(228, 134);
            this.cbPersonCityType.Name = "cbPersonCityType";
            this.cbPersonCityType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbPersonCityType.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Settlement_name", "Тип населенного пункта")});
            this.cbPersonCityType.Properties.DisplayMember = "Settlement_name";
            this.cbPersonCityType.Properties.NullText = "<Не задано>";
            this.cbPersonCityType.Properties.ValueMember = "Settlement_id";
            this.cbPersonCityType.Size = new System.Drawing.Size(191, 20);
            this.cbPersonCityType.TabIndex = 1;
            this.cbPersonCityType.EditValueChanged += new System.EventHandler(this.cbPersonCityType_EditValueChanged);
            this.cbPersonCityType.MouseDown += new System.Windows.Forms.MouseEventHandler(this.cbPersonCityType_MouseDown);
            this.cbPersonCityType.Validating += new System.ComponentModel.CancelEventHandler(this.OnValidating);
            // 
            // cbPersonCity
            // 
            this.cbPersonCity.Location = new System.Drawing.Point(228, 180);
            this.cbPersonCity.Name = "cbPersonCity";
            this.cbPersonCity.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbPersonCity.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("City_name", "Населенный пункт")});
            this.cbPersonCity.Properties.DisplayMember = "City_name";
            this.cbPersonCity.Properties.NullText = "<Не задано>";
            this.cbPersonCity.Properties.ValueMember = "City_id";
            this.cbPersonCity.Size = new System.Drawing.Size(191, 20);
            this.cbPersonCity.TabIndex = 1;
            this.cbPersonCity.Validating += new System.ComponentModel.CancelEventHandler(this.OnValidating);
            // 
            // cbPersonRegion
            // 
            this.cbPersonRegion.Location = new System.Drawing.Point(228, 40);
            this.cbPersonRegion.Name = "cbPersonRegion";
            this.cbPersonRegion.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbPersonRegion.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Region_name", "Регион")});
            this.cbPersonRegion.Properties.DisplayMember = "Region_name";
            this.cbPersonRegion.Properties.NullText = "<Не задано>";
            this.cbPersonRegion.Properties.ValueMember = "Region_id";
            this.cbPersonRegion.Size = new System.Drawing.Size(191, 20);
            this.cbPersonRegion.TabIndex = 1;
            this.cbPersonRegion.EditValueChanged += new System.EventHandler(this.cbPersonRegion_EditValueChanged);
            this.cbPersonRegion.MouseDown += new System.Windows.Forms.MouseEventHandler(this.cbPersonRegion_MouseDown);
            this.cbPersonRegion.Validating += new System.ComponentModel.CancelEventHandler(this.OnValidating);
            // 
            // cbPersonDistrict
            // 
            this.cbPersonDistrict.Location = new System.Drawing.Point(228, 87);
            this.cbPersonDistrict.Name = "cbPersonDistrict";
            this.cbPersonDistrict.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbPersonDistrict.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("District_name", "Область")});
            this.cbPersonDistrict.Properties.DisplayMember = "District_name";
            this.cbPersonDistrict.Properties.NullText = "<Не задано>";
            this.cbPersonDistrict.Properties.ValueMember = "District_id";
            this.cbPersonDistrict.Size = new System.Drawing.Size(191, 20);
            this.cbPersonDistrict.TabIndex = 1;
            this.cbPersonDistrict.EditValueChanged += new System.EventHandler(this.cbPersonDistrict_EditValueChanged);
            this.cbPersonDistrict.Validating += new System.ComponentModel.CancelEventHandler(this.OnValidating);
            // 
            // memoPersonAddress
            // 
            this.memoPersonAddress.Location = new System.Drawing.Point(6, 227);
            this.memoPersonAddress.Name = "memoPersonAddress";
            this.memoPersonAddress.Size = new System.Drawing.Size(216, 70);
            this.memoPersonAddress.TabIndex = 2;
            this.memoPersonAddress.Validating += new System.ComponentModel.CancelEventHandler(this.OnValidating);
            // 
            // tbPersonName
            // 
            this.tbPersonName.Location = new System.Drawing.Point(6, 40);
            this.tbPersonName.Name = "tbPersonName";
            this.tbPersonName.Size = new System.Drawing.Size(216, 20);
            this.tbPersonName.TabIndex = 3;
            this.tbPersonName.Validating += new System.ComponentModel.CancelEventHandler(this.OnValidating);
            // 
            // tbEDRPOU
            // 
            this.tbEDRPOU.Location = new System.Drawing.Point(6, 87);
            this.tbEDRPOU.Name = "tbEDRPOU";
            this.tbEDRPOU.Properties.Mask.EditMask = "\\d+";
            this.tbEDRPOU.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Regular;
            this.tbEDRPOU.Size = new System.Drawing.Size(216, 20);
            this.tbEDRPOU.TabIndex = 3;
            this.tbEDRPOU.Validating += new System.ComponentModel.CancelEventHandler(this.OnValidating);
            // 
            // tbBookeeperName
            // 
            this.tbBookeeperName.Location = new System.Drawing.Point(228, 227);
            this.tbBookeeperName.Name = "tbBookeeperName";
            this.tbBookeeperName.Size = new System.Drawing.Size(191, 20);
            this.tbBookeeperName.TabIndex = 3;
            // 
            // tbINN
            // 
            this.tbINN.Location = new System.Drawing.Point(6, 180);
            this.tbINN.Name = "tbINN";
            this.tbINN.Properties.Mask.EditMask = "\\d+";
            this.tbINN.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Regular;
            this.tbINN.Size = new System.Drawing.Size(216, 20);
            this.tbINN.TabIndex = 3;
            this.tbINN.Validating += new System.ComponentModel.CancelEventHandler(this.OnValidating);
            // 
            // tbBookkeeperPhone
            // 
            this.tbBookkeeperPhone.Location = new System.Drawing.Point(228, 275);
            this.tbBookkeeperPhone.Name = "tbBookkeeperPhone";
            this.tbBookkeeperPhone.Properties.Mask.EditMask = "[\\d \\( \\) \\- \\s]*";
            this.tbBookkeeperPhone.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Regular;
            this.tbBookkeeperPhone.Size = new System.Drawing.Size(191, 20);
            this.tbBookkeeperPhone.TabIndex = 3;
            // 
            // labelPersonName
            // 
            this.labelPersonName.Location = new System.Drawing.Point(6, 21);
            this.labelPersonName.Name = "labelPersonName";
            this.labelPersonName.Size = new System.Drawing.Size(45, 13);
            this.labelPersonName.TabIndex = 4;
            this.labelPersonName.Text = "Юр. имя:";
            // 
            // memoPersonNotes
            // 
            this.memoPersonNotes.Location = new System.Drawing.Point(6, 322);
            this.memoPersonNotes.Name = "memoPersonNotes";
            this.memoPersonNotes.Size = new System.Drawing.Size(413, 74);
            this.memoPersonNotes.TabIndex = 2;
            // 
            // labelPersonCode
            // 
            this.labelPersonCode.Location = new System.Drawing.Point(6, 68);
            this.labelPersonCode.Name = "labelPersonCode";
            this.labelPersonCode.Size = new System.Drawing.Size(46, 13);
            this.labelPersonCode.TabIndex = 4;
            this.labelPersonCode.Text = "ЕДРПОУ:";
            // 
            // labelPersonRegion
            // 
            this.labelPersonRegion.Location = new System.Drawing.Point(228, 68);
            this.labelPersonRegion.Name = "labelPersonRegion";
            this.labelPersonRegion.Size = new System.Drawing.Size(47, 13);
            this.labelPersonRegion.TabIndex = 4;
            this.labelPersonRegion.Text = "Область:";
            // 
            // labelPersonAddress
            // 
            this.labelPersonAddress.Location = new System.Drawing.Point(6, 208);
            this.labelPersonAddress.Name = "labelPersonAddress";
            this.labelPersonAddress.Size = new System.Drawing.Size(57, 13);
            this.labelPersonAddress.TabIndex = 4;
            this.labelPersonAddress.Text = "Юр. адрес:";
            // 
            // labelTaxType
            // 
            this.labelTaxType.Location = new System.Drawing.Point(6, 115);
            this.labelTaxType.Name = "labelTaxType";
            this.labelTaxType.Size = new System.Drawing.Size(130, 13);
            this.labelTaxType.TabIndex = 4;
            this.labelTaxType.Text = "Форма налогообложения:";
            // 
            // labelBookkeeperName
            // 
            this.labelBookkeeperName.Location = new System.Drawing.Point(228, 208);
            this.labelBookkeeperName.Name = "labelBookkeeperName";
            this.labelBookkeeperName.Size = new System.Drawing.Size(89, 13);
            this.labelBookkeeperName.TabIndex = 4;
            this.labelBookkeeperName.Text = "ФИО бухгалтера:";
            // 
            // labelINN
            // 
            this.labelINN.Location = new System.Drawing.Point(6, 162);
            this.labelINN.Name = "labelINN";
            this.labelINN.Size = new System.Drawing.Size(25, 13);
            this.labelINN.TabIndex = 4;
            this.labelINN.Text = "ИНН:";
            // 
            // labelPersonDistrict
            // 
            this.labelPersonDistrict.Location = new System.Drawing.Point(228, 21);
            this.labelPersonDistrict.Name = "labelPersonDistrict";
            this.labelPersonDistrict.Size = new System.Drawing.Size(39, 13);
            this.labelPersonDistrict.TabIndex = 4;
            this.labelPersonDistrict.Text = "Регион:";
            // 
            // labelCityType
            // 
            this.labelCityType.Location = new System.Drawing.Point(228, 115);
            this.labelCityType.Name = "labelCityType";
            this.labelCityType.Size = new System.Drawing.Size(128, 13);
            this.labelCityType.TabIndex = 4;
            this.labelCityType.Text = "Тип населенного пункта:";
            // 
            // labelCity
            // 
            this.labelCity.Location = new System.Drawing.Point(228, 161);
            this.labelCity.Name = "labelCity";
            this.labelCity.Size = new System.Drawing.Size(99, 13);
            this.labelCity.TabIndex = 4;
            this.labelCity.Text = "Населенный пункт:";
            // 
            // labelBookkeeperPhone
            // 
            this.labelBookkeeperPhone.Location = new System.Drawing.Point(228, 256);
            this.labelBookkeeperPhone.Name = "labelBookkeeperPhone";
            this.labelBookkeeperPhone.Size = new System.Drawing.Size(110, 13);
            this.labelBookkeeperPhone.TabIndex = 4;
            this.labelBookkeeperPhone.Text = "Телефон бухгалтера:";
            // 
            // labelNotes
            // 
            this.labelNotes.Location = new System.Drawing.Point(6, 303);
            this.labelNotes.Name = "labelNotes";
            this.labelNotes.Size = new System.Drawing.Size(71, 13);
            this.labelNotes.TabIndex = 4;
            this.labelNotes.Text = "Комментарии:";
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.ForeColor = System.Drawing.Color.Red;
            this.labelControl1.Location = new System.Drawing.Point(57, 21);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(6, 13);
            this.labelControl1.TabIndex = 5;
            this.labelControl1.Text = "*";
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.ForeColor = System.Drawing.Color.Red;
            this.labelControl2.Location = new System.Drawing.Point(57, 68);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(6, 13);
            this.labelControl2.TabIndex = 6;
            this.labelControl2.Text = "*";
            // 
            // labelINNIsRequired
            // 
            this.labelINNIsRequired.Appearance.ForeColor = System.Drawing.Color.Red;
            this.labelINNIsRequired.Location = new System.Drawing.Point(37, 162);
            this.labelINNIsRequired.Name = "labelINNIsRequired";
            this.labelINNIsRequired.Size = new System.Drawing.Size(6, 13);
            this.labelINNIsRequired.TabIndex = 7;
            this.labelINNIsRequired.Text = "*";
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.ForeColor = System.Drawing.Color.Red;
            this.labelControl4.Location = new System.Drawing.Point(69, 208);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(6, 13);
            this.labelControl4.TabIndex = 8;
            this.labelControl4.Text = "*";
            // 
            // labelControl13
            // 
            this.labelControl13.Appearance.ForeColor = System.Drawing.Color.Red;
            this.labelControl13.Location = new System.Drawing.Point(142, 115);
            this.labelControl13.Name = "labelControl13";
            this.labelControl13.Size = new System.Drawing.Size(6, 13);
            this.labelControl13.TabIndex = 12;
            this.labelControl13.Text = "*";
            // 
            // labelControl14
            // 
            this.labelControl14.Appearance.ForeColor = System.Drawing.Color.Red;
            this.labelControl14.Location = new System.Drawing.Point(273, 21);
            this.labelControl14.Name = "labelControl14";
            this.labelControl14.Size = new System.Drawing.Size(6, 13);
            this.labelControl14.TabIndex = 15;
            this.labelControl14.Text = "*";
            // 
            // labelControl16
            // 
            this.labelControl16.Appearance.ForeColor = System.Drawing.Color.Red;
            this.labelControl16.Location = new System.Drawing.Point(332, 161);
            this.labelControl16.Name = "labelControl16";
            this.labelControl16.Size = new System.Drawing.Size(6, 13);
            this.labelControl16.TabIndex = 17;
            this.labelControl16.Text = "*";
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.ForeColor = System.Drawing.Color.Red;
            this.labelControl3.Location = new System.Drawing.Point(281, 68);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(6, 13);
            this.labelControl3.TabIndex = 18;
            this.labelControl3.Text = "*";
            // 
            // labelControl5
            // 
            this.labelControl5.Appearance.ForeColor = System.Drawing.Color.Red;
            this.labelControl5.Location = new System.Drawing.Point(362, 115);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(6, 13);
            this.labelControl5.TabIndex = 19;
            this.labelControl5.Text = "*";
            // 
            // groupPerson
            // 
            this.groupPerson.AppearanceCaption.Options.UseTextOptions = true;
            this.groupPerson.AppearanceCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.groupPerson.Controls.Add(this.labelControl5);
            this.groupPerson.Controls.Add(this.labelControl3);
            this.groupPerson.Controls.Add(this.labelControl16);
            this.groupPerson.Controls.Add(this.labelControl14);
            this.groupPerson.Controls.Add(this.labelControl13);
            this.groupPerson.Controls.Add(this.labelControl4);
            this.groupPerson.Controls.Add(this.labelINNIsRequired);
            this.groupPerson.Controls.Add(this.labelControl2);
            this.groupPerson.Controls.Add(this.labelControl1);
            this.groupPerson.Controls.Add(this.labelNotes);
            this.groupPerson.Controls.Add(this.labelBookkeeperPhone);
            this.groupPerson.Controls.Add(this.labelCity);
            this.groupPerson.Controls.Add(this.labelCityType);
            this.groupPerson.Controls.Add(this.labelPersonDistrict);
            this.groupPerson.Controls.Add(this.labelINN);
            this.groupPerson.Controls.Add(this.labelBookkeeperName);
            this.groupPerson.Controls.Add(this.labelTaxType);
            this.groupPerson.Controls.Add(this.labelPersonAddress);
            this.groupPerson.Controls.Add(this.labelPersonRegion);
            this.groupPerson.Controls.Add(this.labelPersonCode);
            this.groupPerson.Controls.Add(this.memoPersonNotes);
            this.groupPerson.Controls.Add(this.labelPersonName);
            this.groupPerson.Controls.Add(this.tbBookkeeperPhone);
            this.groupPerson.Controls.Add(this.tbINN);
            this.groupPerson.Controls.Add(this.tbBookeeperName);
            this.groupPerson.Controls.Add(this.tbEDRPOU);
            this.groupPerson.Controls.Add(this.tbPersonName);
            this.groupPerson.Controls.Add(this.memoPersonAddress);
            this.groupPerson.Controls.Add(this.cbPersonDistrict);
            this.groupPerson.Controls.Add(this.cbPersonRegion);
            this.groupPerson.Controls.Add(this.cbPersonCity);
            this.groupPerson.Controls.Add(this.cbPersonCityType);
            this.groupPerson.Controls.Add(this.cbPersonTaxList);
            this.groupPerson.Location = new System.Drawing.Point(3, 37);
            this.groupPerson.Name = "groupPerson";
            this.groupPerson.Size = new System.Drawing.Size(424, 413);
            this.groupPerson.TabIndex = 3;
            this.groupPerson.Text = "Реквизиты";
            // 
            // groupDistributors1
            // 
            this.groupDistributors1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.groupDistributors1.AppearanceCaption.Options.UseTextOptions = true;
            this.groupDistributors1.AppearanceCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.groupDistributors1.Controls.Add(this.listDistributorsOff);
            this.groupDistributors1.Location = new System.Drawing.Point(5, 5);
            this.groupDistributors1.Margin = new System.Windows.Forms.Padding(3, 3, 3, 30);
            this.groupDistributors1.Name = "groupDistributors1";
            this.groupDistributors1.Size = new System.Drawing.Size(283, 231);
            this.groupDistributors1.TabIndex = 12;
            this.groupDistributors1.Text = "Дистрибьюторы бут, ПЕТ, банка";
            // 
            // listDistributorsOff
            // 
            this.listDistributorsOff.DisplayMember = "DISTR_NAME";
            this.listDistributorsOff.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listDistributorsOff.Location = new System.Drawing.Point(2, 21);
            this.listDistributorsOff.Name = "listDistributorsOff";
            this.listDistributorsOff.Size = new System.Drawing.Size(279, 208);
            this.listDistributorsOff.TabIndex = 0;
            this.listDistributorsOff.ValueMember = "DISTR_ID";
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.groupDistributors2);
            this.panelControl1.Controls.Add(this.groupDistributors1);
            this.panelControl1.Location = new System.Drawing.Point(433, 463);
            this.panelControl1.Margin = new System.Windows.Forms.Padding(0, 0, 0, 100);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(588, 243);
            this.panelControl1.TabIndex = 8;
            // 
            // labelFixator
            // 
            this.labelFixator.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelFixator.BackColor = System.Drawing.SystemColors.Control;
            this.labelFixator.Location = new System.Drawing.Point(627, 700);
            this.labelFixator.Name = "labelFixator";
            this.labelFixator.Size = new System.Drawing.Size(1, 1);
            this.labelFixator.TabIndex = 9;
            this.labelFixator.Text = "   ";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.label1.Location = new System.Drawing.Point(6, 4);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(52, 15);
            this.label1.TabIndex = 10;
            this.label1.Text = "Создал:";
            // 
            // lblCreator
            // 
            this.lblCreator.AutoSize = true;
            this.lblCreator.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.lblCreator.Location = new System.Drawing.Point(57, 4);
            this.lblCreator.Name = "lblCreator";
            this.lblCreator.Size = new System.Drawing.Size(77, 15);
            this.lblCreator.TabIndex = 11;
            this.lblCreator.Text = "Неизвестно";
            // 
            // gridControlPOCList
            // 
            this.gridControlPOCList.Location = new System.Drawing.Point(201, 463);
            this.gridControlPOCList.MainView = this.gridViewPOCList;
            this.gridControlPOCList.Name = "gridControlPOCList";
            this.gridControlPOCList.Size = new System.Drawing.Size(226, 243);
            this.gridControlPOCList.TabIndex = 0;
            this.gridControlPOCList.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewPOCList});
            // 
            // gridViewPOCList
            // 
            this.gridViewPOCList.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2,
            this.columnPOCPremiality});
            this.gridViewPOCList.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.None;
            this.gridViewPOCList.GridControl = this.gridControlPOCList;
            this.gridViewPOCList.Name = "gridViewPOCList";
            this.gridViewPOCList.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridViewPOCList.OptionsSelection.EnableAppearanceFocusedRow = false;
            this.gridViewPOCList.OptionsView.ShowDetailButtons = false;
            this.gridViewPOCList.OptionsView.ShowFooter = true;
            this.gridViewPOCList.OptionsView.ShowGroupPanel = false;
            this.gridViewPOCList.OptionsView.ShowIndicator = false;
            this.gridViewPOCList.CustomDrawFooterCell += new DevExpress.XtraGrid.Views.Grid.FooterCellCustomDrawEventHandler(this.gridViewPOCList_CustomDrawFooterCell);
            // 
            // gridColumn1
            // 
            this.gridColumn1.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn1.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn1.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.gridColumn1.Caption = "Тип ТТ";
            this.gridColumn1.FieldName = "OLTYPE_NAME";
            this.gridColumn1.MinWidth = 10;
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowEdit = false;
            this.gridColumn1.OptionsColumn.AllowMove = false;
            this.gridColumn1.OptionsColumn.ReadOnly = true;
            this.gridColumn1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.gridColumn1.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Min, "PREMIALITY", "")});
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 0;
            this.gridColumn1.Width = 148;
            // 
            // gridColumn2
            // 
            this.gridColumn2.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn2.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn2.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.gridColumn2.Caption = "Кол-во";
            this.gridColumn2.FieldName = "OLTYPE_QUANTITY";
            this.gridColumn2.MinWidth = 10;
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.AllowEdit = false;
            this.gridColumn2.OptionsColumn.AllowMove = false;
            this.gridColumn2.OptionsColumn.ReadOnly = true;
            this.gridColumn2.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.gridColumn2.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)});
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 1;
            this.gridColumn2.Width = 60;
            // 
            // columnPOCPremiality
            // 
            this.columnPOCPremiality.Caption = "Премиальность";
            this.columnPOCPremiality.FieldName = "PREMIALITY";
            this.columnPOCPremiality.Name = "columnPOCPremiality";
            this.columnPOCPremiality.OptionsColumn.AllowEdit = false;
            this.columnPOCPremiality.OptionsColumn.ReadOnly = true;
            this.columnPOCPremiality.OptionsColumn.ShowInCustomizationForm = false;
            this.columnPOCPremiality.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            // 
            // xtraScrollableControl1
            // 
            this.xtraScrollableControl1.Controls.Add(this.gridControlPOCList);
            this.xtraScrollableControl1.Controls.Add(this.groupPerson);
            this.xtraScrollableControl1.Controls.Add(this.lblCreator);
            this.xtraScrollableControl1.Controls.Add(this.label1);
            this.xtraScrollableControl1.Controls.Add(this.groupSalesAverage);
            this.xtraScrollableControl1.Controls.Add(this.groupContactPerson);
            this.xtraScrollableControl1.Controls.Add(this.panelControl1);
            this.xtraScrollableControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraScrollableControl1.Location = new System.Drawing.Point(0, 0);
            this.xtraScrollableControl1.Name = "xtraScrollableControl1";
            this.xtraScrollableControl1.Size = new System.Drawing.Size(1028, 712);
            this.xtraScrollableControl1.TabIndex = 13;
            // 
            // ClientDetailsCommon
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.Controls.Add(this.xtraScrollableControl1);
            this.Controls.Add(this.labelFixator);
            this.Name = "ClientDetailsCommon";
            this.Size = new System.Drawing.Size(1028, 712);
            ((System.ComponentModel.ISupportInitialize)(this.groupSalesAverage)).EndInit();
            this.groupSalesAverage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupDistributors2)).EndInit();
            this.groupDistributors2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.listDistributorsOn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupContactPerson)).EndInit();
            this.groupContactPerson.ResumeLayout(false);
            this.groupContactPerson.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbCPEmail.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbCPMobilePhone.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbCPPhone.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbCPName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbCPPosition.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbPersonTaxList.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbPersonCityType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbPersonCity.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbPersonRegion.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbPersonDistrict.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoPersonAddress.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbPersonName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbEDRPOU.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbBookeeperName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbINN.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbBookkeeperPhone.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoPersonNotes.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupPerson)).EndInit();
            this.groupPerson.ResumeLayout(false);
            this.groupPerson.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupDistributors1)).EndInit();
            this.groupDistributors1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.listDistributorsOff)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlPOCList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewPOCList)).EndInit();
            this.xtraScrollableControl1.ResumeLayout(false);
            this.xtraScrollableControl1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.GroupControl groupContactPerson;
        private DevExpress.XtraEditors.LabelControl labelControl11;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.LabelControl labelCPEmail;
        private DevExpress.XtraEditors.TextEdit tbCPEmail;
        private DevExpress.XtraEditors.LabelControl labelCPMobilePhone;
        private DevExpress.XtraEditors.LabelControl labelCPPhone;
        private DevExpress.XtraEditors.LabelControl labelCPName;
        private DevExpress.XtraEditors.LabelControl labelCPPosition;
        private DevExpress.XtraEditors.TextEdit tbCPMobilePhone;
        private DevExpress.XtraEditors.TextEdit tbCPPhone;
        private DevExpress.XtraEditors.TextEdit tbCPName;
        private DevExpress.XtraEditors.TextEdit tbCPPosition;
        private DevExpress.XtraEditors.GroupControl groupSalesAverage;
        private DevExpress.XtraEditors.GroupControl groupPerson;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl16;
        private DevExpress.XtraEditors.LabelControl labelControl14;
        private DevExpress.XtraEditors.LabelControl labelControl13;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelINNIsRequired;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LabelControl labelNotes;
        private DevExpress.XtraEditors.LabelControl labelBookkeeperPhone;
        private DevExpress.XtraEditors.LabelControl labelCity;
        private DevExpress.XtraEditors.LabelControl labelCityType;
        private DevExpress.XtraEditors.LabelControl labelPersonDistrict;
        private DevExpress.XtraEditors.LabelControl labelINN;
        private DevExpress.XtraEditors.LabelControl labelBookkeeperName;
        private DevExpress.XtraEditors.LabelControl labelTaxType;
        private DevExpress.XtraEditors.LabelControl labelPersonAddress;
        private DevExpress.XtraEditors.LabelControl labelPersonRegion;
        private DevExpress.XtraEditors.LabelControl labelPersonCode;
        private DevExpress.XtraEditors.MemoEdit memoPersonNotes;
        private DevExpress.XtraEditors.LabelControl labelPersonName;
        private DevExpress.XtraEditors.TextEdit tbBookkeeperPhone;
        private DevExpress.XtraEditors.TextEdit tbINN;
        private DevExpress.XtraEditors.TextEdit tbBookeeperName;
        private DevExpress.XtraEditors.TextEdit tbEDRPOU;
        private DevExpress.XtraEditors.TextEdit tbPersonName;
        private DevExpress.XtraEditors.MemoEdit memoPersonAddress;
        private DevExpress.XtraEditors.LookUpEdit cbPersonDistrict;
        private DevExpress.XtraEditors.LookUpEdit cbPersonRegion;
        private DevExpress.XtraEditors.LookUpEdit cbPersonCity;
        private DevExpress.XtraEditors.LookUpEdit cbPersonCityType;
        private DevExpress.XtraEditors.LookUpEdit cbPersonTaxList;
        private DevExpress.XtraEditors.GroupControl groupDistributors1;
        private DevExpress.XtraEditors.ListBoxControl listDistributorsOff;
        private DevExpress.XtraEditors.ListBoxControl listDistributorsOn;
        private DevExpress.XtraEditors.GroupControl groupDistributors2;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private System.Windows.Forms.Label labelFixator;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblCreator;
        private DevExpress.XtraGrid.GridControl gridControlPOCList;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewPOCList;
        private DevExpress.XtraGrid.Columns.GridColumn columnPOCPremiality;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private Logica.Reports.BaseReportControl.CommonControls.Editors.MonthlySales monthlySalesCtrl;
        private DevExpress.XtraEditors.XtraScrollableControl xtraScrollableControl1;
    }
}
