﻿using System;
using System.Windows.Forms;
using DevExpress.XtraTab;
using Logica.Reports.BaseReportControl;
using DevExpress.XtraBars;
using System.Data;
using System.Collections.Generic;
using Logica.Reports.Common;
using DevExpress.XtraEditors;
using System.Collections.ObjectModel;
using DevExpress.XtraPrintingLinks;
using System.IO;
using Logica.Reports.BaseReportControl.CommonControls;
using System.Text;

namespace SoftServe.Reports.ContractMgmtClients
{
    /// <summary>
    /// 
    /// </summary>
    public partial class ClientDetailsControl : CommonBaseControl
    {
        private ClientDetailsCommon controlCommon;
        private ClientDetailsAddresses controlAddresses;
        private ClientDetailsContracts controlContracts;
        private ClientDetailsHistory controlHistory;

        private int clientId;
        private IList<CommonBaseControl> tabs;

        /// <summary>
        /// Initializes a new instance of the <see cref="ClientDetailsControl"/> class.
        /// </summary>
        public ClientDetailsControl(CommonBaseTab parentTab, int clientId)
            : base(parentTab)
        {
            InitializeComponent();

            controlCommon = new ClientDetailsCommon();
            controlAddresses = new ClientDetailsAddresses();
            controlContracts = new ClientDetailsContracts();
            controlHistory = new ClientDetailsHistory(parentTab);

            this.tabs = new List<CommonBaseControl> { controlCommon, controlAddresses, controlContracts, controlHistory };
            this.clientId = clientId;

            InitTabControl();
        }

        /// <summary>
        /// Loads the data.
        /// </summary>
        public void LoadData()
        {
            LoadData(true);
        }

        /// <summary>
        /// Loads the data.
        /// </summary>
        public void LoadData(bool selectTab)
        {
            LoadAccessLevel();

            controlCommon.LoadData(ClientId, controlAddresses.FirstAddressId);
            controlAddresses.LoadData(ClientId, controlAddresses.FirstAddressId);
            controlContracts.LoadData(ClientId, controlAddresses.FirstAddressId);
            controlHistory.LoadData(ClientId, controlAddresses.FirstAddressId);

            if (selectTab)
            {
                tabControl.SelectedTabPageIndex = IsEditMode ? (int)ClientDetailsTabType.Common : (int)ClientDetailsTabType.Addresses;
            }

            UpdateBarButtons();
        }

        /// <summary>
        /// Loads the access level.
        /// </summary>
        private void LoadAccessLevel()
        {
            MainData.AccessLevel = DataProvider.AccessLevel;
            MainData.UserLevel = DataProvider.UserLevel;
            MainData.HasAccessToAllAddresses = DataProvider.GetHasAccessToAddresses(ClientId);
        }

        /// <summary>
        /// Gets the client id.
        /// </summary>
        /// <value>The client id.</value>
        public int ClientId
        {
            get
            {
                return clientId;
            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance is edit mode.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance is edit mode; otherwise, <c>false</c>.
        /// </value>
        private bool IsEditMode
        {
            get
            {
                return clientId > 0;
            }
        }

        /// <summary>
        /// Inits the tab control.
        /// </summary>
        private void InitTabControl()
        {
            AddTab(controlCommon, Resource.TabInfo);
            AddTab(controlAddresses, Resource.TabAddresses);
            AddTab(controlContracts, Resource.TabContracts);
            AddTab(controlHistory, Resource.TabHistory);

            UpdateTabStatus();
        }

        /// <summary>
        /// Updates the tab status.
        /// </summary>
        public void UpdateTabStatus()
        {
            #region Ticket#1013710 — невозможно сохранить КК без адрески
            //bool enableSubTabs = controlAddresses.FirstAddressId > 0;

            //if (GetSubTab(ClientDetailsTabType.Common).PageEnabled == false &&
            //    enableSubTabs)
            //{
            //    OnFirstAddressIsLoaded(controlAddresses.FirstAddressId);
            //}
            
            bool enableSubTabs = true;
            bool isAddressNotEmpty = controlAddresses.FirstAddressId > 0;
            
            if (controlCommon.IsForAutoPopulate() && isAddressNotEmpty)
            {
                OnFirstAddressIsLoaded(controlAddresses.FirstAddressId);
            }
            #endregion

            EnableSubTab(ClientDetailsTabType.Common, enableSubTabs);
            EnableSubTab(ClientDetailsTabType.Contracts, enableSubTabs);
            EnableSubTab(ClientDetailsTabType.History, enableSubTabs);
        }

        /// <summary>
        /// Called when [first address is loaded].
        /// </summary>
        /// <param name="addressId">The address id.</param>
        private void OnFirstAddressIsLoaded(long addressId)
        {
            controlCommon.LoadData(ClientId, addressId);
        }

        /// <summary>
        /// Updates the bar menu.
        /// </summary>
        private void UpdateBarButtons()
        {
            ClientDetailsTabType tabType = (ClientDetailsTabType)tabControl.SelectedTabPageIndex;

            barButtonRemove.Enabled = IsEditMode && MainData.CheckAccessLevel(AccessLevelType.ReadWrite) && MainData.HasAccessToAllAddresses && controlContracts.ContractsCount == 0;
            barButtonSave.Enabled = MainData.CheckAccessLevel(AccessLevelType.ReadWrite);
        }

        /// <summary>
        /// Gets the sub tab.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <returns></returns>
        private CommonBaseTab GetSubTab(ClientDetailsTabType type)
        {
            return tabControl.TabPages[(int)type] as CommonBaseTab;
        }

        /// <summary>
        /// Enables the sub tab.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <param name="enable">if set to <c>true</c> [enable].</param>
        private void EnableSubTab(ClientDetailsTabType type, bool enable)
        {
            GetSubTab(type).PageEnabled = enable;
        }

        /// <summary>
        /// Adds the tab.
        /// </summary>
        /// <param name="content">The content.</param>
        /// <param name="tabName">Name of the tab.</param>
        private void AddTab(CommonBaseControl content, string tabName)
        {
            if (content != null && ParentTab != null)
            {
                CommonBaseTab newCommonBaseTab = new CommonBaseTab();
                newCommonBaseTab.Preferences = this.ParentTab.Preferences;
                content.ParentTab = newCommonBaseTab;
                newCommonBaseTab.Init(ParentTab.Bruc, content, tabName);
                tabControl.TabPages.Add(newCommonBaseTab);
                content.UpdateMainView += new EventHandler(content_UpdateMainView);
                content.AutoScroll = true;
                content.MainData = MainData;
            }
        }

        /// <summary>
        /// Deletes the client.
        /// </summary>
        private void DeleteClient()
        {
            if (ClientId > 0 && ParentTab != null)
            {
                if (XtraMessageBox.Show(
                    string.Format(Resource.ConfirmClientDelete, ParentTab.Text),
                    Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    ClientDetailsDTO clientDetails = new ClientDetailsDTO();
                    clientDetails.Id = ClientId;
                    DataProvider.DeleteClientCommon(clientDetails);

                    ParentTab.CloseTab();
                    ParentTab.UpdateClientListTab();
                }
            }
        }

        /// <summary>
        /// Saves the client.
        /// </summary>
        private void SaveClient()
        {
            // Validate all controls
            ValidateMetadata commonValidateData = controlCommon.ValidateData();
            ValidateMetadata addressesValidateData = controlAddresses.ValidateData(); //Getting validate metatdata

            if (commonValidateData.IsCorrect && addressesValidateData.IsCorrect) //Check whether all validations were passed
            {
                // Save client's common info
                ClientDetailsDTO clientDetails = new ClientDetailsDTO();
                controlCommon.GetAllFields(clientDetails, ClientId);
                int newClientId = DataProvider.InsertClientCommon(clientDetails, IsEditMode);

                if (newClientId <= 0)
                {
                    if (newClientId == Constants.ErrorClientWithThisNameExists)
                    {
                        XtraMessageBox.Show(Resource.ErrorClientWithThisNameExists, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }

                    ErrorManager.ShowErrorBox(Resource.SaveInvalidDataMultiple);
                    return;
                }

                if (!IsEditMode)
                {
                    clientId = newClientId;
                }

                var success = this.controlAddresses.SaveData(clientId);

                DataProvider.SetOutletDependentData(clientId);

                // update tab name and refresh addresses appearance
                if (ParentTab != null)
                {
                    ParentTab.Text = clientDetails.Name;
                }

                if (success)
                {
                    if (ParentTab != null)
                    {
                        ParentTab.UpdateClientListTab();
                    }
                    XtraMessageBox.Show(Resource.SavedSuccessfull, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    XtraMessageBox.Show(Resource.SaveError, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

                // Update all details
                LoadData(false);
                UpdateBarButtons();
            }
            else //Some of validations wasn't passed, so combining appropriate message to user
            {
                int incorrectCounter = 0;
                StringBuilder tabListBuilder = new StringBuilder();
                if (!commonValidateData.IsCorrect && commonValidateData.Message != null)
                {
                    tabListBuilder.AppendFormat("'{0}'", commonValidateData.Message);
                    incorrectCounter++;
                }
                if (!addressesValidateData.IsCorrect && addressesValidateData.Message != null)
                {
                    if (incorrectCounter == 0)
                        tabListBuilder.AppendFormat("'{0}'", addressesValidateData.Message);
                    else
                        tabListBuilder.AppendFormat(", '{0}'", addressesValidateData.Message);
                    incorrectCounter++;
                }
                string formattedMessage = string.Format(incorrectCounter == 1 ? Resource.SaveInvalidDataSingle : Resource.SaveInvalidDataMultiple, tabListBuilder);
                XtraMessageBox.Show(formattedMessage, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }

        #region Events

        /// <summary>
        /// Handles the UpdateMainView event of the content control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void content_UpdateMainView(object sender, EventArgs e)
        {
            UpdateTabStatus();
        }

        /// <summary>
        /// Handles the ItemClick event of the barButtonSave control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="DevExpress.XtraBars.ItemClickEventArgs"/> instance containing the event data.</param>
        private void barButtonSave_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            SaveClient();
        }

        /// <summary>
        /// Handles the ItemClick event of the barButtonRemove control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="DevExpress.XtraBars.ItemClickEventArgs"/> instance containing the event data.</param>
        private void barButtonRemove_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            DeleteClient();
        }

        /// <summary>
        /// Handles the SelectedPageChanged event of the tabControl control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="DevExpress.XtraTab.TabPageChangedEventArgs"/> instance containing the event data.</param>
        private void tabControl_SelectedPageChanged(object sender, TabPageChangedEventArgs e)
        {
            CommonBaseTab tab = e.Page as CommonBaseTab;

            if (tab != null && tab.UserControl == controlCommon && controlAddresses.PocListChanged)
            {
                controlCommon.RefreshPOCDependentData(ClientId, controlAddresses.AliveAddresses());
                controlAddresses.PocListChanged = false;
            }

            UpdateBarButtons();
        }

        /// <summary>
        /// Triggers update methods of inner controls collection
        /// </summary>
        public override void UpdateControlVisualState()
        {
            foreach (CommonBaseControl baseCtrl in this.tabs)
            {
                baseCtrl.UpdateControlVisualState();
            }
        }
        #endregion
    }
}