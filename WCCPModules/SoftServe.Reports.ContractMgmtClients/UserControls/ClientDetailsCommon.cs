﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraPrintingLinks;
using DevExpress.XtraPrinting;
using Logica.Reports.BaseReportControl;
using Logica.Reports.Common.WaitWindow;
using System.Text.RegularExpressions;
using Logica.Reports.BaseReportControl.Utility;

namespace SoftServe.Reports.ContractMgmtClients
{
    public partial class ClientDetailsCommon : CommonBaseControl
    {
        public ClientDetailsCommon()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Loads the data.
        /// </summary>
        /// <param name="clientId">The client id.</param>
        public override void LoadData(int clientId, long firstAddressId)
        {
            AllFieldsData = DataProvider.GetClientDetails(clientId, firstAddressId);
            SalesAverage = DataProvider.GetClientAverageMonthlySales(clientId);
            POCList = DataProvider.GetClientPOCList(clientId);
            DistributorsListOff = DataProvider.GetClientDistributorList(clientId, 2);
            DistributorsListOn = DataProvider.GetClientDistributorList(clientId, 1);

            base.LoadData(clientId, firstAddressId);
        }

        public void RefreshPOCDependentData(int clientId, string pocList)
        {
            WaitManager.StartWait();
            try
            {
                SalesAverage = DataProvider.GetClientAverageMonthlySales(clientId, pocList);
                POCList = DataProvider.GetClientPOCList(clientId, pocList);
                DistributorsListOff = DataProvider.GetClientDistributorList(clientId, 2, pocList);
                DistributorsListOn = DataProvider.GetClientDistributorList(clientId, 1, pocList);
            }
            finally
            {
                WaitManager.StopWait();
            }
        }

        /// <summary>
        /// Sets all fields data.
        /// </summary>
        /// <value>All fields data.</value>
        private DataTable AllFieldsData
        {
            set
            {
                if (value != null && value.Rows.Count == 1)
                {
                    DataRow row = value.Rows[0];

                    lblCreator.Text = ConvertEx.ToString(row[Constants.CD_FIELD_CREATED_BY]) + " " +
                        ((row[Constants.CD_FIELD_CREATED_DATE] != null && row[Constants.CD_FIELD_CREATED_DATE] != DBNull.Value)
                        ? row.Field<DateTime>(Constants.CD_FIELD_CREATED_DATE).ToShortDateString()
                        : string.Empty);
                    tbPersonName.EditValue = ConvertEx.ToString(row[Constants.CD_FIELD_LEGAL_NAME]);
                    tbEDRPOU.EditValue = ConvertEx.ToString(row[Constants.CD_FIELD_EDRPOU]);
                    tbINN.EditValue = ConvertEx.ToString(row[Constants.CD_FIELD_IIN]);
                    memoPersonAddress.EditValue = ConvertEx.ToString(row[Constants.CD_FIELD_LEGAL_ADDRESS]);
                    memoPersonNotes.EditValue = ConvertEx.ToString(row[Constants.CD_FIELD_NOTES]);
                    tbBookeeperName.EditValue = ConvertEx.ToString(row[Constants.CD_FIELD_ACCOUNTANT]);
                    tbBookkeeperPhone.EditValue = ConvertEx.ToString(row[Constants.CD_FIELD_ACCOUNTANT_PHONE]);
                    tbCPPosition.EditValue = ConvertEx.ToString(row[Constants.CD_FIELD_ASSIGNEE_APPOINTMENT]);
                    tbCPName.EditValue = ConvertEx.ToString(row[Constants.CD_FIELD_ASSIGNEE]);
                    tbCPPhone.EditValue = ConvertEx.ToString(row[Constants.CD_FIELD_ASSIGNEE_PHONE]);
                    tbCPMobilePhone.EditValue = ConvertEx.ToString(row[Constants.CD_FIELD_ASSIGNEE_MOBILE]);
                    tbCPEmail.EditValue = ConvertEx.ToString(row[Constants.CD_FIELD_ASSIGNEE_EMAIL]);

                    int taxId = ConvertEx.ToInt(row[Constants.CD_FIELD_FISCALTYPE_ID]);
                    LoadTaxList(taxId);

                    int regionId = ConvertEx.ToInt(row[Constants.CD_FIELD_REGION_ID]);
                    int districtId = ConvertEx.ToInt(row[Constants.CD_FIELD_DISTRICT_ID]);
                    int cityTypeId = ConvertEx.ToInt(row[Constants.CD_FIELD_CITY_TYPE_ID]);
                    int cityId = ConvertEx.ToInt(row[Constants.CD_FIELD_CITY_ID]);

                    LoadRegionCombo(regionId);
                    LoadDistrictCombo(regionId, districtId);
                    LoadCityTypeCombo(cityTypeId);
                    LoadCityCombo(districtId, cityTypeId, cityId);
                }
            }
        }

        /// <summary>
        /// Gets the client details.
        /// </summary>
        /// <value>The client details.</value>
        public void GetAllFields(ClientDetailsDTO dto, int clientId)
        {
            dto.Id = clientId;
            dto.Name = Convert.ToString(tbPersonName.EditValue);
            dto.Address = Convert.ToString(memoPersonAddress.EditValue);
            dto.Edrpou = Convert.ToString(tbEDRPOU.EditValue);
            dto.Iin = Convert.ToString(tbINN.EditValue);
            dto.TaxId = Convert.ToInt32(cbPersonTaxList.EditValue);
            dto.CityId = Convert.ToInt32(cbPersonCity.EditValue);
            dto.Bookkeeper = Convert.ToString(tbBookeeperName.EditValue);
            dto.BookkeeperPhone = Convert.ToString(tbBookkeeperPhone.EditValue);
            dto.Notes = Convert.ToString(memoPersonNotes.EditValue);
            dto.ContactName = Convert.ToString(tbCPName.EditValue);
            dto.ContactPosition = Convert.ToString(tbCPPosition.EditValue);
            dto.ContactPhone = Convert.ToString(tbCPPhone.EditValue);
            dto.ContactMobile = Convert.ToString(tbCPMobilePhone.EditValue);
            dto.ContactEmail = Convert.ToString(tbCPEmail.EditValue);
        }

        /// <summary>
        /// Loads the region combo.
        /// </summary>
        /// <param name="seletctedId">The selected id.</param>
        private void LoadRegionCombo(int selectedId)
        {
            cbPersonRegion.Properties.DataSource = DataProvider.RegionList;
            SelectLookUpValue(cbPersonRegion, selectedId);
        }

        /// <summary>
        /// Loads the district combo.
        /// </summary>
        /// <param name="regionId">The region id.</param>
        /// <param name="selectedId">The selected id.</param>
        private void LoadDistrictCombo(int regionId, int selectedId)
        {
            cbPersonDistrict.Properties.DataSource = DataProvider.GetDistrictList(regionId);
            SelectLookUpValue(cbPersonDistrict, selectedId);
        }

        /// <summary>
        /// Loads the city type combo.
        /// </summary>
        /// <param name="selectedId">The selected id.</param>
        private void LoadCityTypeCombo(int selectedId)
        {
            cbPersonCityType.Properties.DataSource = DataProvider.CityTypeList;
            SelectLookUpValue(cbPersonCityType, selectedId);
        }

        /// <summary>
        /// Loads the city combo.
        /// </summary>
        /// <param name="districtId">The district id.</param>
        /// <param name="cityTypeId">The city type id.</param>
        /// <param name="selectedId">The selected id.</param>
        private void LoadCityCombo(int districtId, int cityTypeId, int selectedId)
        {
            cbPersonCity.Properties.DataSource = DataProvider.GetCityList(districtId, cityTypeId);
            SelectLookUpValue(cbPersonCity, selectedId);
        }

        /// <summary>
        /// Selects the look up value.
        /// </summary>
        /// <param name="lookUpControl">The look up control.</param>
        /// <param name="selectedId">The selected id.</param>
        private void SelectLookUpValue(LookUpEditBase lookUpControl, int selectedId)
        {
            if (selectedId == 0)
            {
                selectedId = GetLookUpFirstId(lookUpControl);
            }
            if (selectedId == 0)
            {
                lookUpControl.EditValue = null;
            }
            else
            {
                lookUpControl.EditValue = selectedId;
            }
        }

        /// <summary>
        /// Gets the look up first id.
        /// </summary>
        /// <param name="lookUpControl">The look up control.</param>
        /// <returns></returns>
        private int GetLookUpFirstId(LookUpEditBase lookUpControl)
        {
            int id = 0;
            if (lookUpControl != null && lookUpControl.Properties.DataSource != null)
            {
                DataTable dataTable = lookUpControl.Properties.DataSource as DataTable;
                if (dataTable != null && dataTable.Rows.Count > 0
                    && !string.IsNullOrEmpty(lookUpControl.Properties.ValueMember))
                {
                    id = Convert.ToInt32(dataTable.Rows[0][lookUpControl.Properties.ValueMember]);
                }
            }
            return id;
        }

        /// <summary>
        /// Loads the tax list.
        /// </summary>
        /// <param name="selectedId">The selected id.</param>
        private void LoadTaxList(int selectedId)
        {
            cbPersonTaxList.Properties.DataSource = DataProvider.TaxesList;
            if (selectedId != 0)
            {
                cbPersonTaxList.EditValue = selectedId;
            }
        }

        /// <summary>
        /// Sets the sales average.
        /// </summary>
        /// <value>The sales average.</value>
        private DataTable SalesAverage
        {
            set
            {
                this.monthlySalesCtrl.DataSource = value;
            }
        }

        /// <summary>
        /// Sets the POC list.
        /// </summary>
        /// <value>The POC list.</value>
        private DataTable POCList
        {
            set
            {
                gridControlPOCList.DataSource = value;
            }
        }

        /// <summary>
        /// Sets the distributors list1.
        /// </summary>
        /// <value>The distributors list1.</value>
        private DataTable DistributorsListOff
        {
            set
            {
                listDistributorsOff.DataSource = value;
            }
        }

        /// <summary>
        /// Sets the distributors list2.
        /// </summary>
        /// <value>The distributors list2.</value>
        private DataTable DistributorsListOn
        {
            set
            {
                listDistributorsOn.DataSource = value;
            }
        }

        /// <summary>
        /// Gets the region id.
        /// </summary>
        /// <value>The region id.</value>
        private int RegionId
        {
            get
            {
                return Convert.ToInt32(cbPersonRegion.EditValue);
            }
        }

        /// <summary>
        /// Gets the district id.
        /// </summary>
        /// <value>The district id.</value>
        private int DistrictId
        {
            get
            {
                return Convert.ToInt32(cbPersonDistrict.EditValue);
            }
        }

        /// <summary>
        /// Gets the city type id.
        /// </summary>
        /// <value>The city type id.</value>
        private int CityTypeId
        {
            get
            {
                return Convert.ToInt32(cbPersonCityType.EditValue);
            }
        }

        /// <summary>
        /// Gets the city id.
        /// </summary>
        /// <value>The city id.</value>
        private int CityId
        {
            get
            {
                return Convert.ToInt32(cbPersonCity.EditValue);
            }
        }

        /// <summary>
        /// Handles the EditValueChanged event of the cbPersonRegion control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void cbPersonRegion_EditValueChanged(object sender, EventArgs e)
        {
            LoadDistrictCombo(RegionId, 0);
        }

        /// <summary>
        /// Handles the EditValueChanged event of the cbPersonDistrict control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void cbPersonDistrict_EditValueChanged(object sender, EventArgs e)
        {
            LoadCityCombo(DistrictId, CityTypeId, 0);
        }

        /// <summary>
        /// Handles the EditValueChanged event of the cbPersonCityType control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void cbPersonCityType_EditValueChanged(object sender, EventArgs e)
        {
            LoadCityCombo(DistrictId, CityTypeId, 0);
        }

        /// <summary>
        /// Called when [validating].
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.ComponentModel.CancelEventArgs"/> instance containing the event data.</param>
        private void OnValidating(object sender, CancelEventArgs e)
        {
            if (sender != null && !(sender as Control).Enabled) return;

            if (sender == this.tbINN)
            {
                if (this.cbPersonTaxList.EditValue != null && (int)this.cbPersonTaxList.EditValue != SqlConstants.PdvId)
                {
                    return;
                }
            }

            if (sender == this.tbCPPhone && tbCPMobilePhone.EditValue != null && tbCPMobilePhone.EditValue.ToString().Trim() != string.Empty
                || sender == this.tbCPMobilePhone && tbCPPhone.EditValue != null && tbCPPhone.EditValue.ToString().Trim() != string.Empty)
            {
                return;
            }

            BaseEdit baseEdit = sender as BaseEdit;
            if (baseEdit != null)
            {
                if ((baseEdit.EditValue == null || string.IsNullOrEmpty(baseEdit.EditValue.ToString())))
                {
                    e.Cancel = true;
                    baseEdit.ErrorText = Resource.FieldEmptyError;
                }
            }
        }

        /// <summary>
        /// Handles the EditValueChanged event of the cbPersonTaxList control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void cbPersonTaxList_EditValueChanged(object sender, EventArgs e)
        {
            labelINNIsRequired.Visible = (int)this.cbPersonTaxList.EditValue == SqlConstants.PdvId;
            tbINN.Enabled = labelINNIsRequired.Visible;
            if (!tbINN.Enabled)
            {
                tbINN.ErrorText = String.Empty;
            }
            //tbINN.Focus();
            //cbPersonTaxList.Focus();
        }

        private void gridViewPOCList_CustomDrawFooterCell(object sender, DevExpress.XtraGrid.Views.Grid.FooterCellCustomDrawEventArgs e)
        {
            if (e.Column == gridColumn1)
            {
                e.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            }
        }
           

        /// <summary>
        /// Validates data in contract details common and returns ValidateMetadata object that indicates whether validation was passed and appropriate message if it wasn't.
        /// </summary>
        /// <returns></returns>
        internal override ValidateMetadata ValidateData()
        {
            ValidateMetadata validMetadata = base.ValidateData();
            validMetadata.Message = Resource.CommonInfoCaption;
            return validMetadata;
        }

        #region Ticket#1013710 — невозможно сохранить КК без адрески
        public bool IsForAutoPopulate()
        {
            if (cbPersonRegion.EditValue != null && cbPersonCity.EditValue != null &&
                cbPersonDistrict.EditValue != null)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        private void cbPersonCityType_MouseDown(object sender, MouseEventArgs e)
        {
            if (cbPersonCityType.Properties.DataSource == null)
            {
                cbPersonCityType.Properties.DataSource = DataProvider.CityTypeList;
            }
        }

        private void cbPersonTaxList_MouseDown(object sender, MouseEventArgs e)
        {
            if (cbPersonTaxList.Properties.DataSource == null)
            {
                cbPersonTaxList.Properties.DataSource = DataProvider.TaxesList;
            }
        }

        private void cbPersonRegion_MouseDown(object sender, MouseEventArgs e)
        {
            if (cbPersonRegion.Properties.DataSource == null)
            {
                cbPersonRegion.Properties.DataSource = DataProvider.RegionList;
            }
        }
        #endregion
    }
}
