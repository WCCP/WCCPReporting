﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections.ObjectModel;
using DevExpress.XtraPrintingLinks;
using DevExpress.XtraPrinting;
using Logica.Reports.BaseReportControl.CommonControls;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Columns;
using Logica.Reports.BaseReportControl.CommonControls.Addresses;
using Logica.Reports.BaseReportControl.Utility;
using Logica.Reports.BaseReportControl.CommonControls.AddressesFilters;
using DevExpress.XtraEditors;

namespace SoftServe.Reports.ContractMgmtClients
{
    /// <summary>
    /// 
    /// </summary>
    public partial class ClientDetailsAddresses : CommonBaseControl
    {
        private string fieldOldStatus = "OldStatusColunm";
        private ChannelType channelType = ChannelType.All;

        /// <summary>
        /// Initializes a new instance of the <see cref="ClientDetailsAddresses"/> class.
        /// </summary>
        public ClientDetailsAddresses()
        {
            InitializeComponent();
            BestFitColumns();

            if (!DesignMode)
            {
                this.addressesListCtrl.InvisibleColumns = new[] { this.columnId, this.columnChannelId, this.columnOLId, this.columnUniqueId };
                this.addressesListCtrl.FrozenColumns = gridView.Columns.Cast<GridColumn>().Where(x => x.Fixed != FixedStyle.None).ToArray();
            }

            this.addressesListCtrl.OnFilterChanged += addressesListCtrl_OnFilterChanged;
            this.addressesListCtrl.OnDisplayFilterChanged += addressesListCtrl_OnDisplayFilterChanged;
        }

        public bool PocListChanged { get; set; }

        /// <summary>
        /// Gets or sets the client id.
        /// </summary>
        /// <value>The client id.</value>
        private int ClientId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the addresses list.
        /// </summary>
        /// <value>The addresses list.</value>
        private DataTable AddressesList
        {
            set
            {
                gridControlAddresses.DataSource = value;
                if (value != null)
                {
                    AddressesList.PrimaryKey = new DataColumn[] { AddressesList.Columns[columnUniqueId.FieldName] };
                }
            }
            get
            {
                gridView.PostEditor();
                gridView.UpdateCurrentRow();
                return (DataTable)gridControlAddresses.DataSource;
            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance is address set.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance is address set; otherwise, <c>false</c>.
        /// </value>
        public long FirstAddressId
        {
            get
            {
                long id = 0;
                DataTable dataTable = gridControlAddresses.DataSource as DataTable;
                if (dataTable != null && dataTable.Rows.Count > 0)
                {
                    id = ConvertEx.ToLongInt(dataTable.Rows[0][columnOLId.FieldName]);
                }
                return id;
            }
        }

        /// <summary>
        /// Loads the data.
        /// </summary>
        /// <param name="clientId">The client id.</param>
        public override void LoadData(int clientId, long firstAddressId)
        {
            ClientId = clientId;

            repositoryItemLookUpStatus.DataSource = DataProvider.AddressStatusList;
            DataTable table = DataProvider.GetAddressesList(ClientId);
            DataColumn col = table.Columns.Add(this.fieldOldStatus);
            this.AddActivityColumnsToGrid(table);
            AddressesList = table;
            this.PocListChanged = false;

            base.LoadData(clientId, firstAddressId);
        }

        /// <summary>
        /// Saves the data
        /// </summary>
        /// <param name="clientId">The clientId</param>
        /// <returns>True on success, False on error</returns>
        public bool SaveData(int clientId)
        {
            ClientId = clientId;

            DataTable errorTt = DataProvider.SaveAddresses(ClientId, GetAddressesList(AddressRowStatus.New),
                GetAddressesList(AddressRowStatus.Removed));

            if (errorTt != null && errorTt.Rows.Count > 0)
            {
                new OlErrorForm(errorTt).ShowDialog();

                return false;
            }
            else
                return true;
        }

        /// <summary>
        /// Gets the addresses list.
        /// </summary>
        /// <param name="status">The status.</param>
        /// <returns></returns>
        private string GetAddressesList(AddressRowStatus status)
        {
            string result = string.Empty;

            Collection<AddressDTO> addressList = new Collection<AddressDTO>();
            if (gridControlAddresses.DataSource != null)
            {
                DataView dataView = new DataView(gridControlAddresses.DataSource as DataTable);
                dataView.RowFilter = string.Format("{0} = {1}", columnStatus.FieldName, (int)status);

                foreach (DataRow row in dataView.ToTable().Rows)
                {
                    if (result != string.Empty) 
                    {
                        result += Constants.DELIMITER_ID_LIST;
                    }

                    result += ConvertEx.ToLongInt(row[columnOLId.FieldName]);
                }
            }

            return result;
        }

        /// <summary>
        /// Gets the all addresses.
        /// </summary>
        /// <returns>List of all addresses</returns>
        public string AliveAddresses()
        {
            return string.Join(",", AliveAddressesList());
        }

        public string[] AliveAddressesList()
        {
            DataTable table = gridControlAddresses.DataSource as DataTable;
            if (table == null) return new string[] { };

            return (from d in table.AsEnumerable()
                    let status = (AddressRowStatus)(int)d.Field<byte>(this.columnStatus.FieldName)
                    where status == AddressRowStatus.Common
                    || status == AddressRowStatus.New
                    select d.Field<String>(columnPOCCode.FieldName)).ToArray();
        }

        /// <summary>
        /// Addresseses the list CTRL_ filter preview changing.
        /// </summary>
        /// <param name="args">The args.</param>
        private void addressesListCtrl_FilterPreviewChanging(FilterPreviewChangingArgs args)
        {
            args.EntityId = this.ClientId;
            args.ModuleId = Constants.MODULE_ID;
        }

        /// <summary>
        /// Addresseses the list CTRL_ on filter changed.
        /// </summary>
        /// <param name="args">The args.</param>
        private void addressesListCtrl_OnFilterChanged(FilterChangedArgs args)
        {
            AddPOC(args.PocIds);
        }

        /// <summary>
        /// Addresseses the list CTRL_ on display filter changed.
        /// </summary>
        /// <param name="args">The args.</param>
        private void addressesListCtrl_OnDisplayFilterChanged(FilterChangedArgs args)
        {
            var filter = String.Format("[{0}] In ({1})", columnPOCCode.FieldName, args.PocIds);

            columnPOCCode.FilterInfo = new ColumnFilterInfo(ColumnFilterType.Custom, filter);
        }

        /// <summary>
        /// Adds the POC.
        /// </summary>
        /// <param name="pocList">The poc list.</param>
        private void AddPOC(string strPOCIdList)
        {
            if (!string.IsNullOrEmpty(strPOCIdList))
            {
                DataTable dataTable = DataProvider.GetAddressesListByOLIDs(strPOCIdList);

                if (dataTable != null && dataTable.Rows.Count > 0)
                {
                    DataColumn col = dataTable.Columns.Add(this.fieldOldStatus);
                    dataTable.PrimaryKey = new DataColumn[] { dataTable.Columns[columnUniqueId.FieldName] };

                    foreach (DataRow row in dataTable.Rows)
                    {
                        row[columnStatus.FieldName] = AddressRowStatus.New;
                    }
                    AddressesList.Merge(dataTable, false, MissingSchemaAction.Ignore);
                    this.AddActivityColumnsToGrid(AddressesList);
                    FireUpdateMainView();
                    this.PocListChanged = true;
                }
            }
        }

        /// <summary>
        /// Removes the selected addresses.
        /// </summary>
        private void RemoveSelectedAddresses()
        {
            if (gridView.SelectedRowsCount > 0 && MainData.CheckAccessLevel(AccessLevelType.ReadWrite))
            {
                int[] selectedRows = gridView.GetSelectedRows();
                foreach (int rowHandle in selectedRows)
                {
                    DataRowView row = gridView.GetRow(rowHandle) as DataRowView;
                    AddressRowStatus curStatus = (AddressRowStatus)Convert.ToByte(row[columnStatus.FieldName]);
                    if (curStatus == AddressRowStatus.Removed)
                    {
                        row[this.columnStatus.FieldName] = row[this.fieldOldStatus];
                    }
                    else
                    {
                        row[this.fieldOldStatus] = (byte)curStatus;
                        row[this.columnStatus.FieldName] = (byte)AddressRowStatus.Removed;
                    }

                    gridView.UpdateCurrentRow();
                    this.PocListChanged = true;
                }
            }
        }

        /// <summary>
        /// Handles the KeyDown event of the gridView control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Forms.KeyEventArgs"/> instance containing the event data.</param>
        private void gridView_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                RemoveSelectedAddresses();
            }
        }

        /// <summary>
        /// Handles the DataSourceChanged event of the gridView control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void gridView_DataSourceChanged(object sender, EventArgs e)
        {
            FireUpdateMainView();
        }

        /// <summary>
        /// Handles the ColumnChanged event of the gridView control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void gridView_ColumnChanged(object sender, EventArgs e)
        {
            BestFitColumns();
        }

        /// <summary>
        /// Handles the ColumnPositionChanged event of the gridView control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void gridView_ColumnPositionChanged(object sender, EventArgs e)
        {
            BestFitColumns();
        }

        /// <summary>
        /// Handles the Resize event of the gridControlAddresses control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void gridControlAddresses_Resize(object sender, EventArgs e)
        {
            BestFitColumns();
        }

        /// <summary>
        /// Handles the SizeChanged event of the gridControlAddresses control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void gridControlAddresses_SizeChanged(object sender, EventArgs e)
        {
            BestFitColumns();
        }

        private void addressesListCtrl_DeleteBtnClick(object sender, EventArgs e)
        {
            RemoveSelectedAddresses();
        }

        /// <summary>
        /// Bests the fit columns.
        /// </summary>
        private void BestFitColumns()
        {
            int width = 0;
            foreach (GridColumn column in gridView.Columns)
            {
                if (column.Visible)
                {
                    width += column.Width;
                }
            }

            gridView.OptionsView.ColumnAutoWidth = width <= this.gridControlAddresses.Width;
        }

        private const String activityColumnPrefix = "AFD_";

        /// <summary>
        /// Adds the activity columns to grid.
        /// </summary>
        /// <param name="table">The table.</param>
        private void AddActivityColumnsToGrid(DataTable table)
        {
            var columns = gridView.Columns.Cast<GridColumn>();

            foreach (DataColumn column in table.Columns)
            {
                if (!column.ColumnName.StartsWith(activityColumnPrefix) || columns.Where(x => x.FieldName == column.ColumnName).Count() > 0)
                {
                    continue;
                }

                gridView.Columns.Add(this.NewGridColumn(
                    column.ColumnName,
                    column.ColumnName.Substring(activityColumnPrefix.Length)
                    ));
            }

            this.addressesListCtrl.FrozenColumns = this.addressesListCtrl.FrozenColumns;
        }

        /// <summary>
        /// News the grid column.
        /// </summary>
        /// <param name="fieldName">Name of the field.</param>
        /// <param name="displayName">The display name.</param>
        /// <returns></returns>
        private GridColumn NewGridColumn(String fieldName, String displayName)
        {
            GridColumn column = new GridColumn();

            column.AppearanceHeader.Options.UseTextOptions = true;
            column.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            column.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            column.Caption = displayName;
            column.ColumnEdit = this.repositoryActivity;
            column.FieldName = fieldName;
            column.MinWidth = 50;
            column.OptionsColumn.AllowEdit = false;
            column.OptionsColumn.AllowMove = false;
            column.OptionsColumn.ReadOnly = true;
            column.OptionsFilter.FilterPopupMode = FilterPopupMode.CheckedList;
            column.Visible = false;
            column.Width = 100;

            return column;
        }

        /// <summary>
        /// Called when [validating].
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.ComponentModel.CancelEventArgs"/> instance containing the event data.</param>
        private void OnValidating(object sender, CancelEventArgs e)
        {
            e.Cancel = false; //this.AliveAddressesList().Length == 0; Ticket#1013710 — невозможно сохранить КК без адрески
        }

        private void gridControlAddresses_KeyDown(object sender, KeyEventArgs e)
        {
            if ((e.KeyCode == Keys.V) && e.Control)
            {
                this.addressesListCtrl.FilterDisplayedAddresses();
            }
        }
    }
}