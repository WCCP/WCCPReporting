﻿namespace SoftServe.Reports.ContractMgmtClients
{
    partial class ClientDetailsControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ClientDetailsControl));
            this.tabControl = new DevExpress.XtraTab.XtraTabControl();
            this.barManager = new DevExpress.XtraBars.BarManager(this.components);
            this.barMain = new DevExpress.XtraBars.Bar();
            this.barButtonSave = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonRemove = new DevExpress.XtraBars.BarButtonItem();
            this.barRefreshText = new DevExpress.XtraBars.BarStaticItem();
            this.barButtonRefreshImg = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.imCollection = new DevExpress.Utils.ImageCollection(this.components);
            this.barButtonRefreshText = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonRemoveAddr = new DevExpress.XtraBars.BarButtonItem();
            this.repositoryItemComboChannel = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.tabControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imCollection)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboChannel)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl
            // 
            this.tabControl.AppearancePage.Header.Options.UseTextOptions = true;
            this.tabControl.AppearancePage.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.tabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl.HeaderAutoFill = DevExpress.Utils.DefaultBoolean.True;
            this.tabControl.Location = new System.Drawing.Point(0, 34);
            this.tabControl.Name = "tabControl";
            this.tabControl.ShowHeaderFocus = DevExpress.Utils.DefaultBoolean.True;
            this.tabControl.ShowTabHeader = DevExpress.Utils.DefaultBoolean.True;
            this.tabControl.Size = new System.Drawing.Size(965, 512);
            this.tabControl.TabIndex = 4;
            this.tabControl.SelectedPageChanged += new DevExpress.XtraTab.TabPageChangedEventHandler(this.tabControl_SelectedPageChanged);
            // 
            // barManager
            // 
            this.barManager.AllowCustomization = false;
            this.barManager.AllowMoveBarOnToolbar = false;
            this.barManager.AllowQuickCustomization = false;
            this.barManager.AllowShowToolbarsPopup = false;
            this.barManager.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.barMain});
            this.barManager.DockControls.Add(this.barDockControlTop);
            this.barManager.DockControls.Add(this.barDockControlBottom);
            this.barManager.DockControls.Add(this.barDockControlLeft);
            this.barManager.DockControls.Add(this.barDockControlRight);
            this.barManager.Form = this;
            this.barManager.Images = this.imCollection;
            this.barManager.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barButtonRemove,
            this.barButtonRefreshText,
            this.barButtonRefreshImg,
            this.barButtonSave,
            this.barRefreshText,
            this.barButtonRemoveAddr});
            this.barManager.MaxItemId = 23;
            this.barManager.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemComboChannel});
            // 
            // barMain
            // 
            this.barMain.BarName = "Main";
            this.barMain.DockCol = 0;
            this.barMain.DockRow = 0;
            this.barMain.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.barMain.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonSave),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonRemove, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.barRefreshText, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonRefreshImg)});
            this.barMain.OptionsBar.AllowQuickCustomization = false;
            this.barMain.OptionsBar.DisableClose = true;
            this.barMain.OptionsBar.DisableCustomization = true;
            this.barMain.OptionsBar.DrawDragBorder = false;
            this.barMain.OptionsBar.UseWholeRow = true;
            this.barMain.Text = "Main";
            // 
            // barButtonSave
            // 
            this.barButtonSave.Caption = "Сохранить все";
            this.barButtonSave.Description = "Сохранить изменения";
            this.barButtonSave.Id = 7;
            this.barButtonSave.ImageIndex = 1;
            this.barButtonSave.Name = "barButtonSave";
            this.barButtonSave.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.barButtonSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonSave_ItemClick);
            // 
            // barButtonRemove
            // 
            this.barButtonRemove.Caption = "Удалить клиента";
            this.barButtonRemove.Id = 4;
            this.barButtonRemove.ImageIndex = 0;
            this.barButtonRemove.Name = "barButtonRemove";
            this.barButtonRemove.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.barButtonRemove.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonRemove_ItemClick);
            // 
            // barRefreshText
            // 
            this.barRefreshText.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.barRefreshText.Border = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.barRefreshText.Caption = "Для отмены изменений нажать кнопку \"Обновить\"";
            this.barRefreshText.Id = 16;
            this.barRefreshText.Name = "barRefreshText";
            this.barRefreshText.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.barRefreshText.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barButtonRefreshImg
            // 
            this.barButtonRefreshImg.Enabled = false;
            this.barButtonRefreshImg.Id = 6;
            this.barButtonRefreshImg.ImageIndex = 2;
            this.barButtonRefreshImg.Name = "barButtonRefreshImg";
            // 
            // imCollection
            // 
            this.imCollection.ImageSize = new System.Drawing.Size(24, 24);
            this.imCollection.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imCollection.ImageStream")));
            this.imCollection.Images.SetKeyName(0, "delete.png");
            this.imCollection.Images.SetKeyName(1, "save.png");
            this.imCollection.Images.SetKeyName(2, "refresh_24.png");
            this.imCollection.Images.SetKeyName(3, "filter.png");
            // 
            // barButtonRefreshText
            // 
            this.barButtonRefreshText.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.barButtonRefreshText.Caption = "Для отмены изменений нажать кнопку \"Обновить\"";
            this.barButtonRefreshText.Enabled = false;
            this.barButtonRefreshText.Id = 5;
            this.barButtonRefreshText.Name = "barButtonRefreshText";
            this.barButtonRefreshText.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // barButtonRemoveAddr
            // 
            this.barButtonRemoveAddr.Caption = "Удалить адреску";
            this.barButtonRemoveAddr.Id = 22;
            this.barButtonRemoveAddr.ImageIndex = 0;
            this.barButtonRemoveAddr.Name = "barButtonRemoveAddr";
            this.barButtonRemoveAddr.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // repositoryItemComboChannel
            // 
            this.repositoryItemComboChannel.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.repositoryItemComboChannel.AutoHeight = false;
            this.repositoryItemComboChannel.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboChannel.Name = "repositoryItemComboChannel";
            this.repositoryItemComboChannel.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            // 
            // ClientDetailsControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.Controls.Add(this.tabControl);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Name = "ClientDetailsControl";
            this.Size = new System.Drawing.Size(965, 546);
            ((System.ComponentModel.ISupportInitialize)(this.tabControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imCollection)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboChannel)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraTab.XtraTabControl tabControl;
        private DevExpress.XtraBars.BarManager barManager;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.Bar barMain;
        private DevExpress.XtraBars.BarButtonItem barButtonRemove;
        private DevExpress.Utils.ImageCollection imCollection;
        private DevExpress.XtraBars.BarButtonItem barButtonRefreshText;
        private DevExpress.XtraBars.BarButtonItem barButtonRefreshImg;
        private DevExpress.XtraBars.BarButtonItem barButtonSave;
        private DevExpress.XtraBars.BarStaticItem barRefreshText;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboChannel;
        private DevExpress.XtraBars.BarButtonItem barButtonRemoveAddr;


    }
}
