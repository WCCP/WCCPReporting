﻿namespace SoftServe.Reports.ContractMgmtClients
{
    partial class ClientDetailsAddresses
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition1 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition2 = new DevExpress.XtraGrid.StyleFormatCondition();
            this.columnStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemLookUpStatus = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.gridControlAddresses = new DevExpress.XtraGrid.GridControl();
            this.gridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.columnId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnRegionName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnM3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnM2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnM1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnLegalPOCName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnPhone = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnEmail = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnFactName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnPOCCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnCity = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnFactAddress = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnChannel = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnChannelId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnOLId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnUniqueId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnSpave = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnOLWHSize = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnVInBasePeriod = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnAvgVal = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnOlChanel = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnPrem = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnInBevPart = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnContractInBev = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnContractBBH = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnContractObolon = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn14 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn13 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn12 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryTopicEdit = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryActionLookUp = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.repositoryApproveCheck = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryCommentActionEdit = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryResponsibleLookUp = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.repositoryStatusLookUp = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.repositoryCommentStatusEdit = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryPOCMoveToookUp = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.repositoryActivity = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.addressesListCtrl = new Logica.Reports.BaseReportControl.CommonControls.ManageAddressesList();
            this.lblFixatorWidth = new System.Windows.Forms.Label();
            this.lblFixatorHeight = new System.Windows.Forms.Label();
            this.scrollableArea = new DevExpress.XtraEditors.XtraScrollableControl();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpStatus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlAddresses)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryTopicEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryActionLookUp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryApproveCheck)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryCommentActionEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryResponsibleLookUp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryStatusLookUp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryCommentStatusEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryPOCMoveToookUp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryActivity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.addressesListCtrl.WorkingArea)).BeginInit();
            this.addressesListCtrl.WorkingArea.SuspendLayout();
            this.addressesListCtrl.SuspendLayout();
            this.scrollableArea.SuspendLayout();
            this.SuspendLayout();
            // 
            // columnStatus
            // 
            this.columnStatus.AppearanceHeader.Options.UseTextOptions = true;
            this.columnStatus.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnStatus.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.columnStatus.Caption = "Статус ТТ в адреске";
            this.columnStatus.ColumnEdit = this.repositoryItemLookUpStatus;
            this.columnStatus.FieldName = "STATUS";
            this.columnStatus.MinWidth = 50;
            this.columnStatus.Name = "columnStatus";
            this.columnStatus.OptionsColumn.AllowMove = false;
            this.columnStatus.OptionsColumn.ReadOnly = true;
            this.columnStatus.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.columnStatus.Visible = true;
            this.columnStatus.VisibleIndex = 4;
            this.columnStatus.Width = 100;
            // 
            // repositoryItemLookUpStatus
            // 
            this.repositoryItemLookUpStatus.AutoHeight = false;
            this.repositoryItemLookUpStatus.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpStatus.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "Статус")});
            this.repositoryItemLookUpStatus.DisplayMember = "Name";
            this.repositoryItemLookUpStatus.Name = "repositoryItemLookUpStatus";
            this.repositoryItemLookUpStatus.NullText = "";
            this.repositoryItemLookUpStatus.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.repositoryItemLookUpStatus.ValueMember = "ID";
            // 
            // gridControlAddresses
            // 
            this.gridControlAddresses.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlAddresses.Location = new System.Drawing.Point(2, 2);
            this.gridControlAddresses.MainView = this.gridView;
            this.gridControlAddresses.Name = "gridControlAddresses";
            this.gridControlAddresses.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryTopicEdit,
            this.repositoryActionLookUp,
            this.repositoryApproveCheck,
            this.repositoryCommentActionEdit,
            this.repositoryResponsibleLookUp,
            this.repositoryStatusLookUp,
            this.repositoryCommentStatusEdit,
            this.repositoryPOCMoveToookUp,
            this.repositoryItemLookUpStatus,
            this.repositoryActivity});
            this.gridControlAddresses.Size = new System.Drawing.Size(1161, 287);
            this.gridControlAddresses.TabIndex = 4;
            this.gridControlAddresses.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView});
            this.gridControlAddresses.Validating += new System.ComponentModel.CancelEventHandler(this.OnValidating);
            this.gridControlAddresses.EditorKeyDown += new System.Windows.Forms.KeyEventHandler(this.gridControlAddresses_KeyDown);
            this.gridControlAddresses.SizeChanged += new System.EventHandler(this.gridControlAddresses_SizeChanged);
            this.gridControlAddresses.Resize += new System.EventHandler(this.gridControlAddresses_Resize);
            this.gridControlAddresses.KeyDown += new System.Windows.Forms.KeyEventHandler(this.gridControlAddresses_KeyDown);
            // 
            // gridView
            // 
            this.gridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.columnId,
            this.columnRegionName,
            this.columnM3,
            this.columnM2,
            this.columnM1,
            this.columnLegalPOCName,
            this.columnPhone,
            this.columnEmail,
            this.columnFactName,
            this.columnPOCCode,
            this.columnCity,
            this.columnFactAddress,
            this.columnChannel,
            this.columnStatus,
            this.columnChannelId,
            this.columnOLId,
            this.columnUniqueId,
            this.gridColumnSpave,
            this.gridColumnOLWHSize,
            this.gridColumnVInBasePeriod,
            this.gridColumnAvgVal,
            this.gridColumnOlChanel,
            this.gridColumnPrem,
            this.gridColumnInBevPart,
            this.gridColumnContractInBev,
            this.gridColumnContractBBH,
            this.gridColumnContractObolon,
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn3,
            this.gridColumn4,
            this.gridColumn5,
            this.gridColumn6,
            this.gridColumn14,
            this.gridColumn13,
            this.gridColumn12,
            this.gridColumn7,
            this.gridColumn8,
            this.gridColumn9,
            this.gridColumn10,
            this.gridColumn11});
            styleFormatCondition1.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            styleFormatCondition1.Appearance.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            styleFormatCondition1.Appearance.Options.UseBackColor = true;
            styleFormatCondition1.ApplyToRow = true;
            styleFormatCondition1.Column = this.columnStatus;
            styleFormatCondition1.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition1.Value1 = 1;
            styleFormatCondition2.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Strikeout);
            styleFormatCondition2.Appearance.Options.UseFont = true;
            styleFormatCondition2.ApplyToRow = true;
            styleFormatCondition2.Column = this.columnStatus;
            styleFormatCondition2.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition2.Value1 = 9;
            this.gridView.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition1,
            styleFormatCondition2});
            this.gridView.GridControl = this.gridControlAddresses;
            this.gridView.Name = "gridView";
            this.gridView.OptionsBehavior.EditorShowMode = DevExpress.Utils.EditorShowMode.MouseDownFocused;
            this.gridView.OptionsFilter.MaxCheckedListItemCount = 1000;
            this.gridView.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView.OptionsSelection.MultiSelect = true;
            this.gridView.OptionsView.ShowAutoFilterRow = true;
            this.gridView.OptionsView.ShowGroupPanel = false;
            this.gridView.DataSourceChanged += new System.EventHandler(this.gridView_DataSourceChanged);
            this.gridView.KeyDown += new System.Windows.Forms.KeyEventHandler(this.gridView_KeyDown);
            this.gridView.ColumnPositionChanged += new System.EventHandler(this.gridView_ColumnPositionChanged);
            this.gridView.ColumnChanged += new System.EventHandler(this.gridView_ColumnChanged);
            // 
            // columnId
            // 
            this.columnId.AppearanceHeader.Options.UseTextOptions = true;
            this.columnId.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnId.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.columnId.Caption = "ID";
            this.columnId.FieldName = "ID";
            this.columnId.MinWidth = 50;
            this.columnId.Name = "columnId";
            this.columnId.OptionsColumn.ReadOnly = true;
            this.columnId.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            // 
            // columnRegionName
            // 
            this.columnRegionName.AppearanceHeader.Options.UseTextOptions = true;
            this.columnRegionName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnRegionName.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.columnRegionName.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.columnRegionName.Caption = "Регион";
            this.columnRegionName.FieldName = "REGION_NAME";
            this.columnRegionName.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.columnRegionName.MinWidth = 50;
            this.columnRegionName.Name = "columnRegionName";
            this.columnRegionName.OptionsColumn.AllowMove = false;
            this.columnRegionName.OptionsColumn.ReadOnly = true;
            this.columnRegionName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.columnRegionName.Visible = true;
            this.columnRegionName.VisibleIndex = 0;
            this.columnRegionName.Width = 197;
            // 
            // columnM3
            // 
            this.columnM3.AppearanceHeader.Options.UseTextOptions = true;
            this.columnM3.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnM3.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.columnM3.Caption = "М3";
            this.columnM3.FieldName = "M3_NAME";
            this.columnM3.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.columnM3.MinWidth = 50;
            this.columnM3.Name = "columnM3";
            this.columnM3.OptionsColumn.AllowMove = false;
            this.columnM3.OptionsColumn.ReadOnly = true;
            this.columnM3.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.columnM3.Visible = true;
            this.columnM3.VisibleIndex = 1;
            this.columnM3.Width = 100;
            // 
            // columnM2
            // 
            this.columnM2.AppearanceHeader.Options.UseTextOptions = true;
            this.columnM2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnM2.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.columnM2.Caption = "М2";
            this.columnM2.FieldName = "M2_NAME";
            this.columnM2.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.columnM2.MinWidth = 50;
            this.columnM2.Name = "columnM2";
            this.columnM2.OptionsColumn.AllowMove = false;
            this.columnM2.OptionsColumn.ReadOnly = true;
            this.columnM2.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.columnM2.Visible = true;
            this.columnM2.VisibleIndex = 2;
            this.columnM2.Width = 100;
            // 
            // columnM1
            // 
            this.columnM1.AppearanceHeader.Options.UseTextOptions = true;
            this.columnM1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnM1.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.columnM1.Caption = "М1";
            this.columnM1.FieldName = "M1_NAME";
            this.columnM1.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.columnM1.MinWidth = 50;
            this.columnM1.Name = "columnM1";
            this.columnM1.OptionsColumn.AllowMove = false;
            this.columnM1.OptionsColumn.ReadOnly = true;
            this.columnM1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.columnM1.Visible = true;
            this.columnM1.VisibleIndex = 3;
            this.columnM1.Width = 100;
            // 
            // columnLegalPOCName
            // 
            this.columnLegalPOCName.AppearanceHeader.Options.UseTextOptions = true;
            this.columnLegalPOCName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnLegalPOCName.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.columnLegalPOCName.Caption = "Юр. имя ТТ";
            this.columnLegalPOCName.FieldName = "LEGAL_NAME";
            this.columnLegalPOCName.MinWidth = 50;
            this.columnLegalPOCName.Name = "columnLegalPOCName";
            this.columnLegalPOCName.OptionsColumn.AllowMove = false;
            this.columnLegalPOCName.OptionsColumn.ReadOnly = true;
            this.columnLegalPOCName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.columnLegalPOCName.Visible = true;
            this.columnLegalPOCName.VisibleIndex = 12;
            this.columnLegalPOCName.Width = 100;
            // 
            // columnPhone
            // 
            this.columnPhone.AppearanceHeader.Options.UseTextOptions = true;
            this.columnPhone.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnPhone.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.columnPhone.Caption = "Телефон";
            this.columnPhone.FieldName = "OLTELEPHONE";
            this.columnPhone.MinWidth = 50;
            this.columnPhone.Name = "columnPhone";
            this.columnPhone.OptionsColumn.AllowMove = false;
            this.columnPhone.OptionsColumn.ReadOnly = true;
            this.columnPhone.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.columnPhone.Visible = true;
            this.columnPhone.VisibleIndex = 11;
            this.columnPhone.Width = 100;
            // 
            // columnEmail
            // 
            this.columnEmail.AppearanceHeader.Options.UseTextOptions = true;
            this.columnEmail.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnEmail.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.columnEmail.Caption = "Email";
            this.columnEmail.FieldName = "OLEMAIL";
            this.columnEmail.MinWidth = 50;
            this.columnEmail.Name = "columnEmail";
            this.columnEmail.OptionsColumn.AllowMove = false;
            this.columnEmail.OptionsColumn.ReadOnly = true;
            this.columnEmail.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.columnEmail.Visible = true;
            this.columnEmail.VisibleIndex = 10;
            this.columnEmail.Width = 100;
            // 
            // columnFactName
            // 
            this.columnFactName.AppearanceHeader.Options.UseTextOptions = true;
            this.columnFactName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnFactName.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.columnFactName.Caption = "Факт. имя ТТ";
            this.columnFactName.FieldName = "TRADING_NAME";
            this.columnFactName.MinWidth = 50;
            this.columnFactName.Name = "columnFactName";
            this.columnFactName.OptionsColumn.AllowMove = false;
            this.columnFactName.OptionsColumn.ReadOnly = true;
            this.columnFactName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.columnFactName.Visible = true;
            this.columnFactName.VisibleIndex = 9;
            this.columnFactName.Width = 100;
            // 
            // columnPOCCode
            // 
            this.columnPOCCode.AppearanceHeader.Options.UseTextOptions = true;
            this.columnPOCCode.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnPOCCode.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.columnPOCCode.Caption = "Код ТТ";
            this.columnPOCCode.FieldName = "OUTLET_CODE";
            this.columnPOCCode.MinWidth = 50;
            this.columnPOCCode.Name = "columnPOCCode";
            this.columnPOCCode.OptionsColumn.AllowMove = false;
            this.columnPOCCode.OptionsColumn.ReadOnly = true;
            this.columnPOCCode.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.columnPOCCode.Visible = true;
            this.columnPOCCode.VisibleIndex = 8;
            this.columnPOCCode.Width = 100;
            // 
            // columnCity
            // 
            this.columnCity.AppearanceHeader.Options.UseTextOptions = true;
            this.columnCity.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnCity.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.columnCity.Caption = "Нас. пункт";
            this.columnCity.FieldName = "CITY";
            this.columnCity.MinWidth = 50;
            this.columnCity.Name = "columnCity";
            this.columnCity.OptionsColumn.AllowMove = false;
            this.columnCity.OptionsColumn.ReadOnly = true;
            this.columnCity.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.columnCity.Visible = true;
            this.columnCity.VisibleIndex = 7;
            this.columnCity.Width = 100;
            // 
            // columnFactAddress
            // 
            this.columnFactAddress.AppearanceHeader.Options.UseTextOptions = true;
            this.columnFactAddress.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnFactAddress.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.columnFactAddress.Caption = "Факт. адрес";
            this.columnFactAddress.FieldName = "DELIVERY_ADDRESS";
            this.columnFactAddress.MinWidth = 50;
            this.columnFactAddress.Name = "columnFactAddress";
            this.columnFactAddress.OptionsColumn.AllowMove = false;
            this.columnFactAddress.OptionsColumn.ReadOnly = true;
            this.columnFactAddress.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.columnFactAddress.Visible = true;
            this.columnFactAddress.VisibleIndex = 6;
            this.columnFactAddress.Width = 100;
            // 
            // columnChannel
            // 
            this.columnChannel.AppearanceHeader.Options.UseTextOptions = true;
            this.columnChannel.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnChannel.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.columnChannel.Caption = "Канал персонала";
            this.columnChannel.FieldName = "TRADE_CHANNEL";
            this.columnChannel.MinWidth = 50;
            this.columnChannel.Name = "columnChannel";
            this.columnChannel.OptionsColumn.AllowMove = false;
            this.columnChannel.OptionsColumn.ReadOnly = true;
            this.columnChannel.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.columnChannel.Visible = true;
            this.columnChannel.VisibleIndex = 5;
            this.columnChannel.Width = 100;
            // 
            // columnChannelId
            // 
            this.columnChannelId.AppearanceHeader.Options.UseTextOptions = true;
            this.columnChannelId.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnChannelId.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.columnChannelId.Caption = "Канал";
            this.columnChannelId.FieldName = "TRADE_CHANNEL_ID";
            this.columnChannelId.MinWidth = 50;
            this.columnChannelId.Name = "columnChannelId";
            this.columnChannelId.OptionsColumn.AllowMove = false;
            this.columnChannelId.OptionsColumn.ReadOnly = true;
            this.columnChannelId.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.columnChannelId.Width = 100;
            // 
            // columnOLId
            // 
            this.columnOLId.AppearanceHeader.Options.UseTextOptions = true;
            this.columnOLId.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnOLId.Caption = "OL_ID";
            this.columnOLId.FieldName = "OL_ID";
            this.columnOLId.MinWidth = 50;
            this.columnOLId.Name = "columnOLId";
            this.columnOLId.OptionsColumn.AllowMove = false;
            this.columnOLId.OptionsColumn.ReadOnly = true;
            this.columnOLId.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.columnOLId.Width = 100;
            // 
            // columnUniqueId
            // 
            this.columnUniqueId.AppearanceHeader.Options.UseTextOptions = true;
            this.columnUniqueId.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnUniqueId.Caption = "Unique ID";
            this.columnUniqueId.FieldName = "OL_UNIQUE_ID";
            this.columnUniqueId.MinWidth = 50;
            this.columnUniqueId.Name = "columnUniqueId";
            this.columnUniqueId.OptionsColumn.AllowMove = false;
            this.columnUniqueId.OptionsColumn.ReadOnly = true;
            this.columnUniqueId.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.columnUniqueId.Width = 100;
            // 
            // gridColumnSpave
            // 
            this.gridColumnSpave.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumnSpave.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumnSpave.Caption = "Площадь ТТ";
            this.gridColumnSpave.FieldName = "OLSize";
            this.gridColumnSpave.MinWidth = 50;
            this.gridColumnSpave.Name = "gridColumnSpave";
            this.gridColumnSpave.OptionsColumn.AllowMove = false;
            this.gridColumnSpave.OptionsColumn.ReadOnly = true;
            this.gridColumnSpave.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.gridColumnSpave.Width = 100;
            // 
            // gridColumnOLWHSize
            // 
            this.gridColumnOLWHSize.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumnOLWHSize.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumnOLWHSize.Caption = "Площадь склада";
            this.gridColumnOLWHSize.FieldName = "OLWHSize";
            this.gridColumnOLWHSize.MinWidth = 50;
            this.gridColumnOLWHSize.Name = "gridColumnOLWHSize";
            this.gridColumnOLWHSize.OptionsColumn.AllowMove = false;
            this.gridColumnOLWHSize.OptionsColumn.ReadOnly = true;
            this.gridColumnOLWHSize.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.gridColumnOLWHSize.Width = 100;
            // 
            // gridColumnVInBasePeriod
            // 
            this.gridColumnVInBasePeriod.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumnVInBasePeriod.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumnVInBasePeriod.Caption = "V в базовом периоде дал";
            this.gridColumnVInBasePeriod.DisplayFormat.FormatString = "{0:N2}";
            this.gridColumnVInBasePeriod.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumnVInBasePeriod.FieldName = "TOTAL_VOLUME";
            this.gridColumnVInBasePeriod.MinWidth = 50;
            this.gridColumnVInBasePeriod.Name = "gridColumnVInBasePeriod";
            this.gridColumnVInBasePeriod.OptionsColumn.AllowMove = false;
            this.gridColumnVInBasePeriod.OptionsColumn.ReadOnly = true;
            this.gridColumnVInBasePeriod.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.gridColumnVInBasePeriod.Width = 100;
            // 
            // gridColumnAvgVal
            // 
            this.gridColumnAvgVal.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumnAvgVal.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumnAvgVal.Caption = "Средний V на ТТ дал до контракта";
            this.gridColumnAvgVal.DisplayFormat.FormatString = "{0:N2}";
            this.gridColumnAvgVal.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumnAvgVal.FieldName = "AVERAGE_VOLUME";
            this.gridColumnAvgVal.MinWidth = 50;
            this.gridColumnAvgVal.Name = "gridColumnAvgVal";
            this.gridColumnAvgVal.OptionsColumn.AllowMove = false;
            this.gridColumnAvgVal.OptionsColumn.ReadOnly = true;
            this.gridColumnAvgVal.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.gridColumnAvgVal.Width = 100;
            // 
            // gridColumnOlChanel
            // 
            this.gridColumnOlChanel.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumnOlChanel.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumnOlChanel.Caption = "Канал ТТ";
            this.gridColumnOlChanel.FieldName = "OL_CHANNEL";
            this.gridColumnOlChanel.MinWidth = 50;
            this.gridColumnOlChanel.Name = "gridColumnOlChanel";
            this.gridColumnOlChanel.OptionsColumn.AllowMove = false;
            this.gridColumnOlChanel.OptionsColumn.ReadOnly = true;
            this.gridColumnOlChanel.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.gridColumnOlChanel.Width = 100;
            // 
            // gridColumnPrem
            // 
            this.gridColumnPrem.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumnPrem.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumnPrem.Caption = "Премиальность ТТ";
            this.gridColumnPrem.FieldName = "PREMIALITY";
            this.gridColumnPrem.MinWidth = 50;
            this.gridColumnPrem.Name = "gridColumnPrem";
            this.gridColumnPrem.OptionsColumn.AllowMove = false;
            this.gridColumnPrem.OptionsColumn.ReadOnly = true;
            this.gridColumnPrem.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.gridColumnPrem.Width = 100;
            // 
            // gridColumnInBevPart
            // 
            this.gridColumnInBevPart.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumnInBevPart.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumnInBevPart.Caption = "Доля InBev %";
            this.gridColumnInBevPart.DisplayFormat.FormatString = "P";
            this.gridColumnInBevPart.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumnInBevPart.FieldName = "SHAREINBEV";
            this.gridColumnInBevPart.MinWidth = 50;
            this.gridColumnInBevPart.Name = "gridColumnInBevPart";
            this.gridColumnInBevPart.OptionsColumn.AllowMove = false;
            this.gridColumnInBevPart.OptionsColumn.ReadOnly = true;
            this.gridColumnInBevPart.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.gridColumnInBevPart.Width = 100;
            // 
            // gridColumnContractInBev
            // 
            this.gridColumnContractInBev.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumnContractInBev.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumnContractInBev.Caption = "Контракт InBev";
            this.gridColumnContractInBev.FieldName = "CONTRACT_INBEV";
            this.gridColumnContractInBev.MinWidth = 50;
            this.gridColumnContractInBev.Name = "gridColumnContractInBev";
            this.gridColumnContractInBev.OptionsColumn.AllowMove = false;
            this.gridColumnContractInBev.OptionsColumn.ReadOnly = true;
            this.gridColumnContractInBev.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.gridColumnContractInBev.Width = 100;
            // 
            // gridColumnContractBBH
            // 
            this.gridColumnContractBBH.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumnContractBBH.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumnContractBBH.Caption = "Контракт BBH";
            this.gridColumnContractBBH.FieldName = "CONTRACT_BBH";
            this.gridColumnContractBBH.MinWidth = 50;
            this.gridColumnContractBBH.Name = "gridColumnContractBBH";
            this.gridColumnContractBBH.OptionsColumn.AllowMove = false;
            this.gridColumnContractBBH.OptionsColumn.ReadOnly = true;
            this.gridColumnContractBBH.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.gridColumnContractBBH.Width = 100;
            // 
            // gridColumnContractObolon
            // 
            this.gridColumnContractObolon.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumnContractObolon.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumnContractObolon.Caption = "Контракт Оболонь";
            this.gridColumnContractObolon.FieldName = "CONTRACT_OBOLON";
            this.gridColumnContractObolon.MinWidth = 50;
            this.gridColumnContractObolon.Name = "gridColumnContractObolon";
            this.gridColumnContractObolon.OptionsColumn.AllowMove = false;
            this.gridColumnContractObolon.OptionsColumn.ReadOnly = true;
            this.gridColumnContractObolon.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.gridColumnContractObolon.Width = 100;
            // 
            // gridColumn1
            // 
            this.gridColumn1.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn1.Caption = "ХШ 1-дверний InBev";
            this.gridColumn1.FieldName = "ONEDOORFRIDGES_INBEV";
            this.gridColumn1.MinWidth = 50;
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowMove = false;
            this.gridColumn1.OptionsColumn.ReadOnly = true;
            this.gridColumn1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.gridColumn1.Width = 100;
            // 
            // gridColumn2
            // 
            this.gridColumn2.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn2.Caption = "ХШ 2-дверний InBev";
            this.gridColumn2.FieldName = "TWODOORFRIDGES_INBEV";
            this.gridColumn2.MinWidth = 50;
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.AllowMove = false;
            this.gridColumn2.OptionsColumn.ReadOnly = true;
            this.gridColumn2.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.gridColumn2.Width = 100;
            // 
            // gridColumn3
            // 
            this.gridColumn3.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn3.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn3.Caption = "ХШ 1-дверний Оболонь";
            this.gridColumn3.FieldName = "ONEDOORFRIDGES_OBOLON";
            this.gridColumn3.MinWidth = 50;
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.AllowMove = false;
            this.gridColumn3.OptionsColumn.ReadOnly = true;
            this.gridColumn3.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.gridColumn3.Width = 100;
            // 
            // gridColumn4
            // 
            this.gridColumn4.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn4.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn4.Caption = "ХШ 2-дверний Оболонь";
            this.gridColumn4.FieldName = "TWODOORFRIDGES_OBOLON";
            this.gridColumn4.MinWidth = 50;
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.OptionsColumn.AllowMove = false;
            this.gridColumn4.OptionsColumn.ReadOnly = true;
            this.gridColumn4.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.gridColumn4.Width = 100;
            // 
            // gridColumn5
            // 
            this.gridColumn5.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn5.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn5.Caption = "ХШ 1-дверний BBH";
            this.gridColumn5.FieldName = "ONEDOORFRIDGES_BBH";
            this.gridColumn5.MinWidth = 50;
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.OptionsColumn.AllowMove = false;
            this.gridColumn5.OptionsColumn.ReadOnly = true;
            this.gridColumn5.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.gridColumn5.Width = 100;
            // 
            // gridColumn6
            // 
            this.gridColumn6.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn6.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn6.Caption = "ХШ 2-дверний BBH";
            this.gridColumn6.FieldName = "TWODOORFRIDGES_BBH";
            this.gridColumn6.MinWidth = 50;
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.OptionsColumn.AllowMove = false;
            this.gridColumn6.OptionsColumn.ReadOnly = true;
            this.gridColumn6.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.gridColumn6.Width = 100;
            // 
            // gridColumn14
            // 
            this.gridColumn14.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn14.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn14.Caption = "ХШ 1-дверные Всего";
            this.gridColumn14.FieldName = "ONEDOORFRIDGES";
            this.gridColumn14.MinWidth = 50;
            this.gridColumn14.Name = "gridColumn14";
            this.gridColumn14.OptionsColumn.AllowMove = false;
            this.gridColumn14.OptionsColumn.ReadOnly = true;
            this.gridColumn14.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.gridColumn14.Width = 100;
            // 
            // gridColumn13
            // 
            this.gridColumn13.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn13.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn13.Caption = "ХШ 2-дверные Всего";
            this.gridColumn13.FieldName = "TWODOORFRIDGES";
            this.gridColumn13.MinWidth = 50;
            this.gridColumn13.Name = "gridColumn13";
            this.gridColumn13.OptionsColumn.AllowMove = false;
            this.gridColumn13.OptionsColumn.ReadOnly = true;
            this.gridColumn13.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.gridColumn13.Width = 100;
            // 
            // gridColumn12
            // 
            this.gridColumn12.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn12.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn12.Caption = "ХШ не пивные";
            this.gridColumn12.FieldName = "NONBEERFRIDGES";
            this.gridColumn12.MinWidth = 50;
            this.gridColumn12.Name = "gridColumn12";
            this.gridColumn12.OptionsColumn.AllowMove = false;
            this.gridColumn12.OptionsColumn.ReadOnly = true;
            this.gridColumn12.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.gridColumn12.Width = 100;
            // 
            // gridColumn7
            // 
            this.gridColumn7.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn7.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn7.Caption = "РУ InBev";
            this.gridColumn7.FieldName = "COOLERS_INBEV";
            this.gridColumn7.MinWidth = 50;
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.OptionsColumn.AllowMove = false;
            this.gridColumn7.OptionsColumn.ReadOnly = true;
            this.gridColumn7.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.gridColumn7.Width = 100;
            // 
            // gridColumn8
            // 
            this.gridColumn8.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn8.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn8.Caption = "РУ всех конкурентов";
            this.gridColumn8.FieldName = "COOLERS_OTHER";
            this.gridColumn8.MinWidth = 50;
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.OptionsColumn.AllowMove = false;
            this.gridColumn8.OptionsColumn.ReadOnly = true;
            this.gridColumn8.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.gridColumn8.Width = 100;
            // 
            // gridColumn9
            // 
            this.gridColumn9.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn9.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn9.Caption = "Статус ТТ в SW";
            this.gridColumn9.FieldName = "SW_STATUS";
            this.gridColumn9.MinWidth = 50;
            this.gridColumn9.Name = "gridColumn9";
            this.gridColumn9.OptionsColumn.AllowMove = false;
            this.gridColumn9.OptionsColumn.ReadOnly = true;
            this.gridColumn9.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.gridColumn9.Width = 100;
            // 
            // gridColumn10
            // 
            this.gridColumn10.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn10.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn10.Caption = "Промо ТТ в активности";
            this.gridColumn10.FieldName = "IS_PROMO_MP";
            this.gridColumn10.MinWidth = 50;
            this.gridColumn10.Name = "gridColumn10";
            this.gridColumn10.OptionsColumn.AllowMove = false;
            this.gridColumn10.OptionsColumn.ReadOnly = true;
            this.gridColumn10.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.gridColumn10.Width = 100;
            // 
            // gridColumn11
            // 
            this.gridColumn11.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn11.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn11.Caption = "Контрольная ТТ в активности";
            this.gridColumn11.FieldName = "IS_CONTROL_MP";
            this.gridColumn11.MinWidth = 50;
            this.gridColumn11.Name = "gridColumn11";
            this.gridColumn11.OptionsColumn.AllowMove = false;
            this.gridColumn11.OptionsColumn.ReadOnly = true;
            this.gridColumn11.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.gridColumn11.Width = 100;
            // 
            // repositoryTopicEdit
            // 
            this.repositoryTopicEdit.AutoHeight = false;
            this.repositoryTopicEdit.Name = "repositoryTopicEdit";
            // 
            // repositoryActionLookUp
            // 
            this.repositoryActionLookUp.AutoHeight = false;
            this.repositoryActionLookUp.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryActionLookUp.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("ID", "ID", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("ActionName", "Action")});
            this.repositoryActionLookUp.DisplayMember = "ActionName";
            this.repositoryActionLookUp.Name = "repositoryActionLookUp";
            this.repositoryActionLookUp.NullText = "Не задано";
            this.repositoryActionLookUp.ValueMember = "ID";
            // 
            // repositoryApproveCheck
            // 
            this.repositoryApproveCheck.AutoHeight = false;
            this.repositoryApproveCheck.Name = "repositoryApproveCheck";
            // 
            // repositoryCommentActionEdit
            // 
            this.repositoryCommentActionEdit.AutoHeight = false;
            this.repositoryCommentActionEdit.Name = "repositoryCommentActionEdit";
            // 
            // repositoryResponsibleLookUp
            // 
            this.repositoryResponsibleLookUp.AutoHeight = false;
            this.repositoryResponsibleLookUp.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryResponsibleLookUp.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Supervisor_ID", "ID", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Supervisor_name", "M2")});
            this.repositoryResponsibleLookUp.DisplayMember = "Supervisor_name";
            this.repositoryResponsibleLookUp.Name = "repositoryResponsibleLookUp";
            this.repositoryResponsibleLookUp.NullText = "Не задан";
            this.repositoryResponsibleLookUp.ValueMember = "Supervisor_ID";
            // 
            // repositoryStatusLookUp
            // 
            this.repositoryStatusLookUp.AutoHeight = false;
            this.repositoryStatusLookUp.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryStatusLookUp.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("ID", "ID", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("StatusName", "Status")});
            this.repositoryStatusLookUp.DisplayMember = "StatusName";
            this.repositoryStatusLookUp.Name = "repositoryStatusLookUp";
            this.repositoryStatusLookUp.NullText = "";
            this.repositoryStatusLookUp.ValueMember = "ID";
            // 
            // repositoryCommentStatusEdit
            // 
            this.repositoryCommentStatusEdit.AutoHeight = false;
            this.repositoryCommentStatusEdit.Name = "repositoryCommentStatusEdit";
            // 
            // repositoryPOCMoveToookUp
            // 
            this.repositoryPOCMoveToookUp.AutoHeight = false;
            this.repositoryPOCMoveToookUp.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryPOCMoveToookUp.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("ID", "ID", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("POCName", "ТТ")});
            this.repositoryPOCMoveToookUp.DisplayMember = "POCName";
            this.repositoryPOCMoveToookUp.Name = "repositoryPOCMoveToookUp";
            this.repositoryPOCMoveToookUp.NullText = "Без перемещения";
            this.repositoryPOCMoveToookUp.ValueMember = "ID";
            // 
            // repositoryActivity
            // 
            this.repositoryActivity.AutoHeight = false;
            this.repositoryActivity.Name = "repositoryActivity";
            this.repositoryActivity.ValueGrayed = false;
            // 
            // addressesListCtrl
            // 
            this.addressesListCtrl.ChanelIdFieldName = "TRADE_CHANNEL_ID";
            this.addressesListCtrl.ChannelId = null;
            this.addressesListCtrl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.addressesListCtrl.EditMode = Logica.Reports.BaseReportControl.CommonControls.Addresses.DataAccess.AddressEditMode.AllowAll;
            this.addressesListCtrl.FrozenColumns = new DevExpress.XtraGrid.Columns.GridColumn[0];
            this.addressesListCtrl.InBevVolumeFieldName = null;
            this.addressesListCtrl.InsertButtonText = null;
            this.addressesListCtrl.InvisibleColumns = new DevExpress.XtraGrid.Columns.GridColumn[0];
            this.addressesListCtrl.Location = new System.Drawing.Point(0, 0);
            this.addressesListCtrl.Name = "addressesListCtrl";
            this.addressesListCtrl.Size = new System.Drawing.Size(1165, 396);
            this.addressesListCtrl.StatisticsText = null;
            this.addressesListCtrl.SumColumnOlId = "OUTLET_CODE";
            this.addressesListCtrl.TabIndex = 5;
            this.addressesListCtrl.TargetGrid = this.gridControlAddresses;
            // 
            // addressesListCtrl.WorkingArea
            // 
            this.addressesListCtrl.WorkingArea.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.addressesListCtrl.WorkingArea.Controls.Add(this.gridControlAddresses);
            this.addressesListCtrl.WorkingArea.Controls.Add(this.lblFixatorWidth);
            this.addressesListCtrl.WorkingArea.Controls.Add(this.lblFixatorHeight);
            this.addressesListCtrl.WorkingArea.Location = new System.Drawing.Point(0, 102);
            this.addressesListCtrl.WorkingArea.Name = "WorkingArea";
            this.addressesListCtrl.WorkingArea.Size = new System.Drawing.Size(1165, 291);
            this.addressesListCtrl.WorkingArea.TabIndex = 3;
            this.addressesListCtrl.FilterPreviewChanging += new Logica.Reports.BaseReportControl.CommonControls.AddressesFilters.FilterPreviewChangingHandler(this.addressesListCtrl_FilterPreviewChanging);
            this.addressesListCtrl.DeleteBtnClick += new System.EventHandler(this.addressesListCtrl_DeleteBtnClick);
            // 
            // lblFixatorWidth
            // 
            this.lblFixatorWidth.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblFixatorWidth.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(245)))), ((int)(((byte)(241)))));
            this.lblFixatorWidth.Location = new System.Drawing.Point(1158, -6);
            this.lblFixatorWidth.Name = "lblFixatorWidth";
            this.lblFixatorWidth.Size = new System.Drawing.Size(1, 1);
            this.lblFixatorWidth.TabIndex = 6;
            // 
            // lblFixatorHeight
            // 
            this.lblFixatorHeight.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblFixatorHeight.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(245)))), ((int)(((byte)(241)))));
            this.lblFixatorHeight.Location = new System.Drawing.Point(55, 284);
            this.lblFixatorHeight.Name = "lblFixatorHeight";
            this.lblFixatorHeight.Size = new System.Drawing.Size(1, 1);
            this.lblFixatorHeight.TabIndex = 5;
            // 
            // scrollableArea
            // 
            this.scrollableArea.AutoScrollMinSize = new System.Drawing.Size(1005, 229);
            this.scrollableArea.Controls.Add(this.addressesListCtrl);
            this.scrollableArea.Dock = System.Windows.Forms.DockStyle.Fill;
            this.scrollableArea.Location = new System.Drawing.Point(0, 0);
            this.scrollableArea.Name = "scrollableArea";
            this.scrollableArea.Size = new System.Drawing.Size(1165, 396);
            this.scrollableArea.TabIndex = 6;
            // 
            // ClientDetailsAddresses
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.Controls.Add(this.scrollableArea);
            this.Name = "ClientDetailsAddresses";
            this.Size = new System.Drawing.Size(1165, 396);
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpStatus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlAddresses)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryTopicEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryActionLookUp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryApproveCheck)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryCommentActionEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryResponsibleLookUp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryStatusLookUp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryCommentStatusEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryPOCMoveToookUp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryActivity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.addressesListCtrl.WorkingArea)).EndInit();
            this.addressesListCtrl.WorkingArea.ResumeLayout(false);
            this.addressesListCtrl.ResumeLayout(false);
            this.scrollableArea.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gridControlAddresses;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView;
        private DevExpress.XtraGrid.Columns.GridColumn columnRegionName;
        private DevExpress.XtraGrid.Columns.GridColumn columnM3;
        private DevExpress.XtraGrid.Columns.GridColumn columnM2;
        private DevExpress.XtraGrid.Columns.GridColumn columnM1;
        private DevExpress.XtraGrid.Columns.GridColumn columnLegalPOCName;
        private DevExpress.XtraGrid.Columns.GridColumn columnPhone;
        private DevExpress.XtraGrid.Columns.GridColumn columnEmail;
        private DevExpress.XtraGrid.Columns.GridColumn columnFactName;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryTopicEdit;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryActionLookUp;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryApproveCheck;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryCommentActionEdit;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryResponsibleLookUp;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryStatusLookUp;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryCommentStatusEdit;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryPOCMoveToookUp;
        private DevExpress.XtraGrid.Columns.GridColumn columnPOCCode;
        private DevExpress.XtraGrid.Columns.GridColumn columnCity;
        private DevExpress.XtraGrid.Columns.GridColumn columnFactAddress;
        private DevExpress.XtraGrid.Columns.GridColumn columnStatus;
        private DevExpress.XtraGrid.Columns.GridColumn columnChannelId;
        private DevExpress.XtraGrid.Columns.GridColumn columnChannel;
        private DevExpress.XtraGrid.Columns.GridColumn columnId;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpStatus;
        private DevExpress.XtraGrid.Columns.GridColumn columnOLId;
        private DevExpress.XtraGrid.Columns.GridColumn columnUniqueId;
        private Logica.Reports.BaseReportControl.CommonControls.ManageAddressesList addressesListCtrl;
        private DevExpress.XtraEditors.XtraScrollableControl scrollableArea;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnSpave;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnOLWHSize;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnVInBasePeriod;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnAvgVal;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnOlChanel;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnPrem;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnInBevPart;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnContractInBev;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnContractBBH;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnContractObolon;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn9;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn10;
        private System.Windows.Forms.Label lblFixatorHeight;
        private System.Windows.Forms.Label lblFixatorWidth;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn11;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn14;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn13;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn12;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryActivity;


    }
}
