﻿using System;
using System.Windows.Forms;
using SoftServe.Reports.ContractMgmtClients;
using DevExpress.XtraGrid;
using System.Data;
using System.Globalization;
using DevExpress.XtraEditors;
using Logica.Reports.Common;
using DevExpress.XtraGrid.Views.Grid;
using System.Drawing;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraPrintingLinks;
using DevExpress.XtraPrinting;
using Logica.Reports.BaseReportControl.Utility;
using System.Collections.Generic;
using DevExpress.XtraEditors.Controls;

namespace SoftServe.Reports.ContractMgmtClients
{
    /// <summary>
    /// 
    /// </summary>
    public partial class ClientListControl : CommonBaseControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ClientListControl"/> class.
        /// </summary>
        public ClientListControl()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ClientListControl"/> class.
        /// </summary>
        /// <param name="parentTab">The parent tab.</param>
        public ClientListControl(CommonBaseTab parentTab)
            : base(parentTab)
        {
            InitializeComponent();
        }

        /// <summary>
        /// Gets the grid control.
        /// </summary>
        /// <value>The grid control.</value>
        public GridControl GridControl
        {
            get
            {
                return gridControl;
            }
        }

        /// <summary>
        /// Gets or sets the client list.
        /// </summary>
        /// <value>The client list.</value>
        public DataTable ClientList
        {
            set
            {
                gridControl.DataSource = value;
                LoadAccessLevel();
                UpdateButtonsState();
            }
            get
            {
                gridView.PostEditor();
                gridView.UpdateCurrentRow();
                return (DataTable)gridControl.DataSource;
            }
        }

        /// <summary>
        /// Loads the access level.
        /// </summary>
        private void LoadAccessLevel()
        {
            MainData.AccessLevel = DataProvider.AccessLevel;
            MainData.UserLevel = DataProvider.UserLevel;
        }

        /// <summary>
        /// Handles the Load event of the ClientListControl control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void ClientListControl_Load(object sender, EventArgs e)
        {
            DateTime today = DateTime.Today;

            DateTime dateFrom = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1);
            barEditDateFrom.EditValue = dateFrom;

            DateTime dateTo = dateFrom.AddDays(DateTime.DaysInMonth(dateFrom.Year, dateFrom.Month) - 1);
            barEditDateTo.EditValue = dateTo;

            UserPreferences userPrefs = new UserPreferences();
            userPrefs.DateFrom = ConvertEx.ToDateTime(barEditDateFrom.EditValue);
            userPrefs.DateTo = ConvertEx.ToDateTime(barEditDateTo.EditValue);
            ParentTab.Preferences = userPrefs;
        }

        /// <summary>
        /// News this instance.
        /// </summary>
        private void New()
        {
            LoadDetailsTab(false);
        }

        /// <summary>
        /// Edits this instance.
        /// </summary>
        private void Edit()
        {
            if (!MainData.CheckAccessLevel(AccessLevelType.Unknown))
            {
                LoadDetailsTab(true);
            }
        }

        /// <summary>
        /// Loads the details tab.
        /// </summary>
        /// <param name="isEditMode">if set to <c>true</c> [is edit mode].</param>
        private void LoadDetailsTab(bool isEditMode)
        {
            if (ParentTab != null)
            {                
                string strTabName = Resource.NewDetailsTabName;
                int clientId = 0;
                if (isEditMode)
                {
                    clientId = Convert.ToInt32(gridView.GetFocusedRowCellValue(columnClientId));
                    strTabName = Convert.ToString(gridView.GetFocusedRowCellValue(columnLegalName));
                }
                if (!string.IsNullOrEmpty(strTabName))
                {
                    ParentTab.LoadDetailsTab(strTabName, clientId, isEditMode);
                }
            }
        }

        /// <summary>
        /// Updates the state of the buttons.
        /// </summary>
        private void UpdateButtonsState()
        {
            gridView.PostEditor();
            gridView.UpdateCurrentRow();

            barButtonNew.Enabled = MainData.CheckAccessLevel(AccessLevelType.ReadWrite) && !MainData.CheckAccessLevel(AccessLevelType.Unknown);
            barButtonEdit.Enabled = gridView.FocusedRowHandle >= 0 && !MainData.CheckAccessLevel(AccessLevelType.Unknown);
            barButtonEdit.Caption = MainData.CheckAccessLevel(AccessLevelType.ReadWrite) ? Resource.ButtonEdit : Resource.ButtonView;
        }

        /// <summary>
        /// Handles the FocusedRowChanged event of the gridView control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs"/> instance containing the event data.</param>
        private void gridView_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            UpdateButtonsState();
        }

        /// <summary>
        /// Handles the ItemClick event of the barButtonNew control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="DevExpress.XtraBars.ItemClickEventArgs"/> instance containing the event data.</param>
        private void barButtonNew_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            New();
        }

        /// <summary>
        /// Handles the ItemClick event of the barButtonEdit control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="DevExpress.XtraBars.ItemClickEventArgs"/> instance containing the event data.</param>
        private void barButtonEdit_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Edit();
        }

        /// <summary>
        /// Handles the DoubleClick event of the gridView control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void gridView_DoubleClick(object sender, EventArgs e)
        {
            if (gridView.FocusedRowHandle >= 0)
            {
                Point pt = gridView.GridControl.PointToClient(Control.MousePosition);
                GridHitInfo info = gridView.CalcHitInfo(pt);
                if (info.InRow || info.InRowCell)
                {
                    Edit();
                }
            }
        }

        /// <summary>
        /// Handles the EditValueChanged event of the barEditDateFrom control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void barEditDateFrom_EditValueChanged(object sender, EventArgs e)
        {
            UpdateRowsHighlightState();
            UpdateParentUserPreferences();
        }

        /// <summary>
        /// Handles the EditValueChanged event of the barEditDateTo control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void barEditDateTo_EditValueChanged(object sender, EventArgs e)
        {
            UpdateRowsHighlightState();
            UpdateParentUserPreferences();
            UpdateTabsVisualState();
        }

        /// <summary>
        /// Updates ParentTab.Preferences property with approriate values from controls.
        /// </summary>
        private void UpdateParentUserPreferences()
        {
            if (ParentTab.Preferences != null)
            {
                ParentTab.Preferences.DateFrom = ConvertEx.ToDateTime(barEditDateFrom.EditValue);
                ParentTab.Preferences.DateTo = ConvertEx.ToDateTime(barEditDateTo.EditValue);
                UpdateTabsVisualState();
            }
        }

        /// <summary>
        /// Updates the state of the rows highlight.
        /// </summary>
        private void UpdateRowsHighlightState()
        {
            DateTime dateFrom =  ConvertEx.ToDateTime(barEditDateFrom.EditValue);
            DateTime dateTo = ConvertEx.ToDateTime(barEditDateTo.EditValue);
            repositoryItemDateTo.MinValue = (DateTime)dateFrom;

            string highlightExpression = 
                Utility.HighlightingExpression.BuildDateRangeHighlightExpression(dateFrom, dateTo, columnExpirationDate.FieldName);

            if(gridView.FormatConditions.Count > 0)
            {
                gridView.FormatConditions[0].Expression = highlightExpression;
            }
        }

        /// <summary>
        /// Updates visual state of all opened tabs.
        /// </summary>
        private void UpdateTabsVisualState()
        {
            foreach (CommonBaseTab tab in this.ParentTab.Bruc.TabControl.TabPages)
            {
                tab.UpdateVisualState();
            }
        }

    }
}
