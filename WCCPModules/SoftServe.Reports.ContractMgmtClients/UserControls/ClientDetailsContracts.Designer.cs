﻿namespace SoftServe.Reports.ContractMgmtClients
{
    partial class ClientDetailsContracts
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.checkAdvertTax = new DevExpress.XtraEditors.CheckEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl16 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl17 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl15 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl14 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl13 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl12 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl18 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.spinTrandedAvrVolume = new DevExpress.XtraEditors.SpinEdit();
            this.spinMarketTrand = new DevExpress.XtraEditors.SpinEdit();
            this.spinPrevAvrVolume = new DevExpress.XtraEditors.SpinEdit();
            this.spinExpensesPerOutlet = new DevExpress.XtraEditors.SpinEdit();
            this.spinPlannedVolumeUplift = new DevExpress.XtraEditors.SpinEdit();
            this.layoutControl = new DevExpress.XtraLayout.LayoutControl();
            this.panelTop = new DevExpress.XtraEditors.PanelControl();
            this.textPaymentType = new DevExpress.XtraEditors.TextEdit();
            this.textPaymentSchema = new DevExpress.XtraEditors.TextEdit();
            this.spinPlannedAvrMacoPerDal = new DevExpress.XtraEditors.SpinEdit();
            this.spinUpliftedAvrVolume = new DevExpress.XtraEditors.SpinEdit();
            this.spinPlannedOutletsQty = new DevExpress.XtraEditors.SpinEdit();
            this.expensesTree = new Logica.Reports.BaseReportControl.CommonControls.ExpensesTree.ExpensesTree();
            this.controlIndicators = new Logica.Reports.BaseReportControl.CommonControls.IndicatorsGrid.IndicatorsGrid();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.cbContracts = new DevExpress.XtraEditors.LookUpEdit();
            this.lblContract = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.checkAdvertTax.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinTrandedAvrVolume.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinMarketTrand.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinPrevAvrVolume.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinExpensesPerOutlet.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinPlannedVolumeUplift.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl)).BeginInit();
            this.layoutControl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelTop)).BeginInit();
            this.panelTop.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textPaymentType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textPaymentSchema.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinPlannedAvrMacoPerDal.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinUpliftedAvrVolume.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinPlannedOutletsQty.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbContracts.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // checkAdvertTax
            // 
            this.checkAdvertTax.Location = new System.Drawing.Point(17, 290);
            this.checkAdvertTax.Name = "checkAdvertTax";
            this.checkAdvertTax.Properties.Caption = "Есть налог на рекламу";
            this.checkAdvertTax.Properties.ReadOnly = true;
            this.checkAdvertTax.Size = new System.Drawing.Size(221, 19);
            this.checkAdvertTax.TabIndex = 20;
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(19, 100);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(151, 13);
            this.labelControl3.TabIndex = 12;
            this.labelControl3.Text = "Тренд рынка (рост/падение):";
            // 
            // labelControl5
            // 
            this.labelControl5.Location = new System.Drawing.Point(19, 155);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(117, 13);
            this.labelControl5.TabIndex = 15;
            this.labelControl5.Text = "Планируемый прирост:";
            // 
            // labelControl8
            // 
            this.labelControl8.Location = new System.Drawing.Point(19, 267);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(130, 13);
            this.labelControl8.TabIndex = 14;
            this.labelControl8.Text = "План среднее МАСО/дал:";
            // 
            // labelControl9
            // 
            this.labelControl9.Location = new System.Drawing.Point(19, 239);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(97, 13);
            this.labelControl9.TabIndex = 13;
            this.labelControl9.Text = "План затрат на TT:";
            // 
            // labelControl7
            // 
            this.labelControl7.Location = new System.Drawing.Point(19, 211);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(123, 13);
            this.labelControl7.TabIndex = 13;
            this.labelControl7.Text = "Планируемое кол-во TT:";
            // 
            // labelControl6
            // 
            this.labelControl6.Location = new System.Drawing.Point(19, 184);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(129, 13);
            this.labelControl6.TabIndex = 16;
            this.labelControl6.Text = "Сред. V/TT после старта:";
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(19, 127);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(141, 13);
            this.labelControl4.TabIndex = 19;
            this.labelControl4.Text = "Сред. V/TT с учетом рынка:";
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(19, 72);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(132, 13);
            this.labelControl2.TabIndex = 18;
            this.labelControl2.Text = "Сред. V/TT до контракта:";
            // 
            // labelControl16
            // 
            this.labelControl16.Location = new System.Drawing.Point(246, 267);
            this.labelControl16.Name = "labelControl16";
            this.labelControl16.Size = new System.Drawing.Size(17, 13);
            this.labelControl16.TabIndex = 17;
            this.labelControl16.Text = "грн";
            // 
            // labelControl17
            // 
            this.labelControl17.Location = new System.Drawing.Point(246, 239);
            this.labelControl17.Name = "labelControl17";
            this.labelControl17.Size = new System.Drawing.Size(17, 13);
            this.labelControl17.TabIndex = 17;
            this.labelControl17.Text = "грн";
            // 
            // labelControl15
            // 
            this.labelControl15.Location = new System.Drawing.Point(246, 211);
            this.labelControl15.Name = "labelControl15";
            this.labelControl15.Size = new System.Drawing.Size(14, 13);
            this.labelControl15.TabIndex = 17;
            this.labelControl15.Text = "шт";
            // 
            // labelControl14
            // 
            this.labelControl14.Location = new System.Drawing.Point(246, 184);
            this.labelControl14.Name = "labelControl14";
            this.labelControl14.Size = new System.Drawing.Size(19, 13);
            this.labelControl14.TabIndex = 17;
            this.labelControl14.Text = "дал";
            // 
            // labelControl13
            // 
            this.labelControl13.Location = new System.Drawing.Point(246, 155);
            this.labelControl13.Name = "labelControl13";
            this.labelControl13.Size = new System.Drawing.Size(11, 13);
            this.labelControl13.TabIndex = 17;
            this.labelControl13.Text = "%";
            // 
            // labelControl12
            // 
            this.labelControl12.Location = new System.Drawing.Point(246, 127);
            this.labelControl12.Name = "labelControl12";
            this.labelControl12.Size = new System.Drawing.Size(19, 13);
            this.labelControl12.TabIndex = 17;
            this.labelControl12.Text = "дал";
            // 
            // labelControl11
            // 
            this.labelControl11.Location = new System.Drawing.Point(246, 100);
            this.labelControl11.Name = "labelControl11";
            this.labelControl11.Size = new System.Drawing.Size(11, 13);
            this.labelControl11.TabIndex = 17;
            this.labelControl11.Text = "%";
            // 
            // labelControl10
            // 
            this.labelControl10.Location = new System.Drawing.Point(244, 72);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(19, 13);
            this.labelControl10.TabIndex = 17;
            this.labelControl10.Text = "дал";
            // 
            // labelControl18
            // 
            this.labelControl18.Location = new System.Drawing.Point(19, 45);
            this.labelControl18.Name = "labelControl18";
            this.labelControl18.Size = new System.Drawing.Size(63, 13);
            this.labelControl18.TabIndex = 17;
            this.labelControl18.Text = "Тип оплаты:";
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(19, 17);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(76, 13);
            this.labelControl1.TabIndex = 17;
            this.labelControl1.Text = "Схема оплаты:";
            // 
            // spinTrandedAvrVolume
            // 
            this.spinTrandedAvrVolume.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spinTrandedAvrVolume.Location = new System.Drawing.Point(176, 123);
            this.spinTrandedAvrVolume.Name = "spinTrandedAvrVolume";
            this.spinTrandedAvrVolume.Properties.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.spinTrandedAvrVolume.Properties.DisplayFormat.FormatString = "{0:N2}";
            this.spinTrandedAvrVolume.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.spinTrandedAvrVolume.Properties.Mask.EditMask = "n2";
            this.spinTrandedAvrVolume.Properties.MaxLength = 10000000;
            this.spinTrandedAvrVolume.Properties.ReadOnly = true;
            this.spinTrandedAvrVolume.Size = new System.Drawing.Size(64, 20);
            this.spinTrandedAvrVolume.TabIndex = 6;
            // 
            // spinMarketTrand
            // 
            this.spinMarketTrand.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spinMarketTrand.Location = new System.Drawing.Point(176, 96);
            this.spinMarketTrand.Name = "spinMarketTrand";
            this.spinMarketTrand.Properties.DisplayFormat.FormatString = "{0:N1}";
            this.spinMarketTrand.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.spinMarketTrand.Properties.Mask.EditMask = "n1";
            this.spinMarketTrand.Properties.ReadOnly = true;
            this.spinMarketTrand.Size = new System.Drawing.Size(64, 20);
            this.spinMarketTrand.TabIndex = 7;
            // 
            // spinPrevAvrVolume
            // 
            this.spinPrevAvrVolume.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spinPrevAvrVolume.Location = new System.Drawing.Point(176, 68);
            this.spinPrevAvrVolume.Name = "spinPrevAvrVolume";
            this.spinPrevAvrVolume.Properties.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.spinPrevAvrVolume.Properties.DisplayFormat.FormatString = "{0:N2}";
            this.spinPrevAvrVolume.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.spinPrevAvrVolume.Properties.Mask.EditMask = "n2";
            this.spinPrevAvrVolume.Properties.MaxLength = 10000000;
            this.spinPrevAvrVolume.Properties.ReadOnly = true;
            this.spinPrevAvrVolume.Size = new System.Drawing.Size(64, 20);
            this.spinPrevAvrVolume.TabIndex = 4;
            // 
            // spinExpensesPerOutlet
            // 
            this.spinExpensesPerOutlet.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spinExpensesPerOutlet.Location = new System.Drawing.Point(176, 235);
            this.spinExpensesPerOutlet.Name = "spinExpensesPerOutlet";
            this.spinExpensesPerOutlet.Properties.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.spinExpensesPerOutlet.Properties.DisplayFormat.FormatString = "{0:N2}";
            this.spinExpensesPerOutlet.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.spinExpensesPerOutlet.Properties.Mask.EditMask = "n2";
            this.spinExpensesPerOutlet.Properties.MaxLength = 10000000;
            this.spinExpensesPerOutlet.Properties.ReadOnly = true;
            this.spinExpensesPerOutlet.Size = new System.Drawing.Size(64, 20);
            this.spinExpensesPerOutlet.TabIndex = 5;
            // 
            // spinPlannedVolumeUplift
            // 
            this.spinPlannedVolumeUplift.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spinPlannedVolumeUplift.Location = new System.Drawing.Point(176, 151);
            this.spinPlannedVolumeUplift.Name = "spinPlannedVolumeUplift";
            this.spinPlannedVolumeUplift.Properties.DisplayFormat.FormatString = "{0:N1}";
            this.spinPlannedVolumeUplift.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.spinPlannedVolumeUplift.Properties.Mask.EditMask = "n1";
            this.spinPlannedVolumeUplift.Properties.MaxLength = 100;
            this.spinPlannedVolumeUplift.Properties.ReadOnly = true;
            this.spinPlannedVolumeUplift.Size = new System.Drawing.Size(64, 20);
            this.spinPlannedVolumeUplift.TabIndex = 9;
            // 
            // layoutControl
            // 
            this.layoutControl.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.layoutControl.AutoScroll = false;
            this.layoutControl.Controls.Add(this.panelTop);
            this.layoutControl.Controls.Add(this.expensesTree);
            this.layoutControl.Controls.Add(this.controlIndicators);
            this.layoutControl.Location = new System.Drawing.Point(5, 32);
            this.layoutControl.Name = "layoutControl";
            this.layoutControl.Root = this.layoutControlGroup1;
            this.layoutControl.Size = new System.Drawing.Size(1209, 646);
            this.layoutControl.TabIndex = 22;
            this.layoutControl.Text = "layoutControl1";
            // 
            // panelTop
            // 
            this.panelTop.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.panelTop.Controls.Add(this.textPaymentType);
            this.panelTop.Controls.Add(this.textPaymentSchema);
            this.panelTop.Controls.Add(this.checkAdvertTax);
            this.panelTop.Controls.Add(this.labelControl18);
            this.panelTop.Controls.Add(this.labelControl3);
            this.panelTop.Controls.Add(this.labelControl10);
            this.panelTop.Controls.Add(this.labelControl5);
            this.panelTop.Controls.Add(this.labelControl11);
            this.panelTop.Controls.Add(this.labelControl8);
            this.panelTop.Controls.Add(this.labelControl9);
            this.panelTop.Controls.Add(this.labelControl12);
            this.panelTop.Controls.Add(this.spinTrandedAvrVolume);
            this.panelTop.Controls.Add(this.labelControl7);
            this.panelTop.Controls.Add(this.labelControl13);
            this.panelTop.Controls.Add(this.labelControl1);
            this.panelTop.Controls.Add(this.spinMarketTrand);
            this.panelTop.Controls.Add(this.labelControl6);
            this.panelTop.Controls.Add(this.labelControl14);
            this.panelTop.Controls.Add(this.spinPlannedAvrMacoPerDal);
            this.panelTop.Controls.Add(this.spinPrevAvrVolume);
            this.panelTop.Controls.Add(this.labelControl4);
            this.panelTop.Controls.Add(this.labelControl15);
            this.panelTop.Controls.Add(this.spinPlannedVolumeUplift);
            this.panelTop.Controls.Add(this.spinExpensesPerOutlet);
            this.panelTop.Controls.Add(this.labelControl2);
            this.panelTop.Controls.Add(this.labelControl17);
            this.panelTop.Controls.Add(this.spinUpliftedAvrVolume);
            this.panelTop.Controls.Add(this.spinPlannedOutletsQty);
            this.panelTop.Controls.Add(this.labelControl16);
            this.panelTop.Location = new System.Drawing.Point(12, 12);
            this.panelTop.Name = "panelTop";
            this.panelTop.Size = new System.Drawing.Size(281, 319);
            this.panelTop.TabIndex = 21;
            // 
            // textPaymentType
            // 
            this.textPaymentType.Location = new System.Drawing.Point(115, 40);
            this.textPaymentType.Name = "textPaymentType";
            this.textPaymentType.Properties.ReadOnly = true;
            this.textPaymentType.Size = new System.Drawing.Size(125, 20);
            this.textPaymentType.TabIndex = 21;
            // 
            // textPaymentSchema
            // 
            this.textPaymentSchema.Location = new System.Drawing.Point(115, 13);
            this.textPaymentSchema.Name = "textPaymentSchema";
            this.textPaymentSchema.Properties.ReadOnly = true;
            this.textPaymentSchema.Size = new System.Drawing.Size(125, 20);
            this.textPaymentSchema.TabIndex = 21;
            // 
            // spinPlannedAvrMacoPerDal
            // 
            this.spinPlannedAvrMacoPerDal.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spinPlannedAvrMacoPerDal.Location = new System.Drawing.Point(176, 263);
            this.spinPlannedAvrMacoPerDal.Name = "spinPlannedAvrMacoPerDal";
            this.spinPlannedAvrMacoPerDal.Properties.DisplayFormat.FormatString = "{0:N2}";
            this.spinPlannedAvrMacoPerDal.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.spinPlannedAvrMacoPerDal.Properties.Mask.EditMask = "n2";
            this.spinPlannedAvrMacoPerDal.Properties.MaxValue = new decimal(new int[] {
            10000000,
            0,
            0,
            0});
            this.spinPlannedAvrMacoPerDal.Properties.ReadOnly = true;
            this.spinPlannedAvrMacoPerDal.Size = new System.Drawing.Size(64, 20);
            this.spinPlannedAvrMacoPerDal.TabIndex = 8;
            // 
            // spinUpliftedAvrVolume
            // 
            this.spinUpliftedAvrVolume.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spinUpliftedAvrVolume.Location = new System.Drawing.Point(176, 180);
            this.spinUpliftedAvrVolume.Name = "spinUpliftedAvrVolume";
            this.spinUpliftedAvrVolume.Properties.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.spinUpliftedAvrVolume.Properties.DisplayFormat.FormatString = "{0:N2}";
            this.spinUpliftedAvrVolume.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.spinUpliftedAvrVolume.Properties.Mask.EditMask = "n2";
            this.spinUpliftedAvrVolume.Properties.MaxLength = 10000000;
            this.spinUpliftedAvrVolume.Properties.ReadOnly = true;
            this.spinUpliftedAvrVolume.Size = new System.Drawing.Size(64, 20);
            this.spinUpliftedAvrVolume.TabIndex = 10;
            // 
            // spinPlannedOutletsQty
            // 
            this.spinPlannedOutletsQty.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spinPlannedOutletsQty.Location = new System.Drawing.Point(176, 207);
            this.spinPlannedOutletsQty.Name = "spinPlannedOutletsQty";
            this.spinPlannedOutletsQty.Properties.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.spinPlannedOutletsQty.Properties.DisplayFormat.FormatString = "{0:N0}";
            this.spinPlannedOutletsQty.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.spinPlannedOutletsQty.Properties.IsFloatValue = false;
            this.spinPlannedOutletsQty.Properties.Mask.EditMask = "n0";
            this.spinPlannedOutletsQty.Properties.MaxLength = 10000000;
            this.spinPlannedOutletsQty.Properties.ReadOnly = true;
            this.spinPlannedOutletsQty.Size = new System.Drawing.Size(64, 20);
            this.spinPlannedOutletsQty.TabIndex = 5;
            // 
            // expensesTree
            // 
            this.expensesTree.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.expensesTree.Location = new System.Drawing.Point(297, 12);
            this.expensesTree.Name = "expensesTree";
            this.expensesTree.ReadOnly = true;
            this.expensesTree.Size = new System.Drawing.Size(900, 319);
            this.expensesTree.TabIndex = 8;
            // 
            // controlIndicators
            // 
            this.controlIndicators.AllowEditingForCurrentMonth = false;
            this.controlIndicators.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.controlIndicators.EditMode = Logica.Reports.BaseReportControl.CommonControls.IndicatorsGrid.DataAccess.IndicatorEditMode.ReadOnly;
            this.controlIndicators.Location = new System.Drawing.Point(12, 335);
            this.controlIndicators.MinimumSize = new System.Drawing.Size(0, 310);
            this.controlIndicators.Name = "controlIndicators";
            this.controlIndicators.Size = new System.Drawing.Size(1185, 310);
            this.controlIndicators.TabIndex = 9;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.layoutControlItem3});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.OptionsItemText.TextToControlDistance = 5;
            this.layoutControlGroup1.Size = new System.Drawing.Size(1209, 651);
            this.layoutControlGroup1.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup1.Text = "layoutControlGroup1";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.panelTop;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.MaxSize = new System.Drawing.Size(285, 323);
            this.layoutControlItem1.MinSize = new System.Drawing.Size(285, 323);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(285, 323);
            this.layoutControlItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem1.Text = "layoutControlItem1";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.expensesTree;
            this.layoutControlItem2.CustomizationFormText = "layoutControlItem2";
            this.layoutControlItem2.Location = new System.Drawing.Point(285, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(904, 323);
            this.layoutControlItem2.Text = "layoutControlItem2";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextToControlDistance = 0;
            this.layoutControlItem2.TextVisible = false;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.controlIndicators;
            this.layoutControlItem3.CustomizationFormText = "layoutControlItem3";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 323);
            this.layoutControlItem3.MaxSize = new System.Drawing.Size(0, 308);
            this.layoutControlItem3.MinSize = new System.Drawing.Size(110, 308);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(1189, 308);
            this.layoutControlItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem3.Text = "layoutControlItem3";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextToControlDistance = 0;
            this.layoutControlItem3.TextVisible = false;
            // 
            // cbContracts
            // 
            this.cbContracts.Location = new System.Drawing.Point(76, 6);
            this.cbContracts.Name = "cbContracts";
            this.cbContracts.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbContracts.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("ID", global::SoftServe.Reports.ContractMgmtClients.Resource.FieldContractIDCaption, 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("CONTRACTNO", global::SoftServe.Reports.ContractMgmtClients.Resource.FieldContractNumCaption),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("LEGAL_NAME", global::SoftServe.Reports.ContractMgmtClients.Resource.FieldContractNameCaption),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("DATE_SIGNED", global::SoftServe.Reports.ContractMgmtClients.Resource.FieldContractSignedDateCaption),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("VALID_TILL", global::SoftServe.Reports.ContractMgmtClients.Resource.FieldContractValidTillCaption),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("STATUS", global::SoftServe.Reports.ContractMgmtClients.Resource.FieldContractStatusCaption)});
            this.cbContracts.Properties.NullText = global::SoftServe.Reports.ContractMgmtClients.Resource.NoContracts;
            this.cbContracts.Properties.PopupWidth = 780;
            this.cbContracts.Properties.ValueMember = "ID";
            this.cbContracts.Size = new System.Drawing.Size(681, 20);
            this.cbContracts.TabIndex = 10;
            this.cbContracts.Tag = "cbContractsDropDown";
            this.cbContracts.EditValueChanged += new System.EventHandler(this.cbContracts_EditValueChanged);
            this.cbContracts.CustomDisplayText += new DevExpress.XtraEditors.Controls.CustomDisplayTextEventHandler(this.cbContracts_CustomDisplayText);
            // 
            // lblContract
            // 
            this.lblContract.Location = new System.Drawing.Point(17, 9);
            this.lblContract.Name = "lblContract";
            this.lblContract.Size = new System.Drawing.Size(53, 13);
            this.lblContract.TabIndex = 17;
            this.lblContract.Text = "Контракт:";
            // 
            // ClientDetailsContracts
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.layoutControl);
            this.Controls.Add(this.cbContracts);
            this.Controls.Add(this.lblContract);
            this.Name = "ClientDetailsContracts";
            this.Size = new System.Drawing.Size(1219, 681);
            ((System.ComponentModel.ISupportInitialize)(this.checkAdvertTax.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinTrandedAvrVolume.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinMarketTrand.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinPrevAvrVolume.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinExpensesPerOutlet.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinPlannedVolumeUplift.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl)).EndInit();
            this.layoutControl.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelTop)).EndInit();
            this.panelTop.ResumeLayout(false);
            this.panelTop.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textPaymentType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textPaymentSchema.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinPlannedAvrMacoPerDal.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinUpliftedAvrVolume.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinPlannedOutletsQty.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbContracts.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.CheckEdit checkAdvertTax;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl16;
        private DevExpress.XtraEditors.LabelControl labelControl17;
        private DevExpress.XtraEditors.LabelControl labelControl15;
        private DevExpress.XtraEditors.LabelControl labelControl14;
        private DevExpress.XtraEditors.LabelControl labelControl13;
        private DevExpress.XtraEditors.LabelControl labelControl12;
        private DevExpress.XtraEditors.LabelControl labelControl11;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraEditors.LabelControl labelControl18;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.SpinEdit spinTrandedAvrVolume;
        private DevExpress.XtraEditors.SpinEdit spinMarketTrand;
        private DevExpress.XtraEditors.SpinEdit spinPrevAvrVolume;
        private DevExpress.XtraEditors.SpinEdit spinExpensesPerOutlet;
        private DevExpress.XtraEditors.SpinEdit spinPlannedVolumeUplift;
        private Logica.Reports.BaseReportControl.CommonControls.IndicatorsGrid.IndicatorsGrid controlIndicators;
        private Logica.Reports.BaseReportControl.CommonControls.ExpensesTree.ExpensesTree expensesTree;
        private DevExpress.XtraEditors.SpinEdit spinPlannedOutletsQty;
        private DevExpress.XtraEditors.SpinEdit spinUpliftedAvrVolume;
        private DevExpress.XtraEditors.SpinEdit spinPlannedAvrMacoPerDal;
        private DevExpress.XtraEditors.LookUpEdit cbContracts;
        private DevExpress.XtraEditors.LabelControl lblContract;
        private DevExpress.XtraEditors.PanelControl panelTop;
        private DevExpress.XtraLayout.LayoutControl layoutControl;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraEditors.TextEdit textPaymentType;
        private DevExpress.XtraEditors.TextEdit textPaymentSchema;

    }
}
