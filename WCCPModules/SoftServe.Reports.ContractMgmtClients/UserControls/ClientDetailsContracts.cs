﻿using System;
using System.Data;
using Logica.Reports.BaseReportControl.Utility;
using DevExpress.XtraEditors;
using System.Text;

namespace SoftServe.Reports.ContractMgmtClients
{
    /// <summary>
    /// 
    /// </summary>
    public partial class ClientDetailsContracts : CommonBaseControl
    {
        private int ContractId { get; set; }
        private int ClientId { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="ContractDetailsGoals"/> class.
        /// </summary>
        public ClientDetailsContracts()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ClientDetailsContracts"/> class.
        /// </summary>
        /// <param name="parentTab">The parent tab.</param>
        public ClientDetailsContracts(CommonBaseTab parentTab)
            : base(parentTab)
        {
        }

        /// <summary>
        /// Gets the contracts count.
        /// </summary>
        /// <value>The contracts count.</value>
        public int ContractsCount
        {
            get
            {
                DataTable dataTable = cbContracts.Properties.DataSource as DataTable;
                if (dataTable != null)
                {
                    return dataTable.Rows.Count;
                }
                return 0;
            }
        }

        /// <summary>
        /// Loads the data.
        /// </summary>
        /// <param name="clientId">The client id.</param>
        public override void LoadData(int clientId, long firstAddressId)
        {
            ClientId = clientId;

            AttachEvents(false);

            LoadDropDowns();

            LoadContractView();

            UpdateContractView();

            AttachEvents(true);

            base.LoadData(clientId, firstAddressId);
        }

        /// <summary>
        /// Attaches the events.
        /// </summary>
        /// <param name="attach">if set to <c>true</c> [attach].</param>
        private void AttachEvents(bool attach)
        {
            if (attach)
            {
                cbContracts.EditValueChanged += new EventHandler(cbContracts_EditValueChanged);
            }
            else
            {
                cbContracts.EditValueChanged -= new EventHandler(cbContracts_EditValueChanged);
            }
        }

        /// <summary>
        /// Gets the current contract data row.
        /// </summary>
        /// <value>The current contract data row.</value>
        private DataRow CurrentContractDataRow
        {
            get
            {
                if (ContractId > 0)
                {
                    DataTable dataTable = cbContracts.Properties.DataSource as DataTable;
                    if (dataTable != null && dataTable.Rows.Count > 0)
                    {
                        return dataTable.Rows.Find(ContractId);
                    }
                }
                return null;
            }
        }

        /// <summary>
        /// Gets the current planned sign date.
        /// </summary>
        /// <value>The current planned sign date.</value>
        private DateTime CurrentPlannedSignDate
        {
            get
            {
                DataRow row = CurrentContractDataRow;
                if (row != null)
                {
                    return ConvertEx.ToDateTime(row[Constants.FIELD_ACTIVE_CONTRACT_PLANNED_SIGN_DATE]);
                }
                return DateTime.MinValue;
            }
        }

        /// <summary>
        /// Gets or sets the contract term.
        /// </summary>
        /// <value>The contract term.</value>
        private int ContractTerm
        {
            get
            {
                DataRow row = CurrentContractDataRow;
                if (row != null)
                {
                    return ConvertEx.ToInt(row[Constants.FIELD_ACTIVE_CONTRACT_TERM]);
                }
                return 0;
            }
        }

        /// <summary>
        /// Updates the contract view.
        /// </summary>
        private void UpdateContractView()
        {
            layoutControl.Visible = ContractId > 0;
        }

        /// <summary>
        /// Loads the contract view.
        /// </summary>
        /// <param name="cont">The cont.</param>
        private void LoadContractView()
        {
            if (ContractId > 0)
            {
                AllFieldsData = DataProvider.GetInvestmentDetails(ContractId);

                LoadExpensesTree();
                LoadIndicators();
            }
        }

        /// <summary>
        /// Loads presaved indicators
        /// </summary>
        public void LoadIndicators()
        {
            DataTable dataTable = DataProvider.GetIndicatorsTable(ContractId);
            controlIndicators.LoadIndicators(dataTable, CurrentPlannedSignDate);
        }

        /// <summary>
        /// Loads the expenses tree.
        /// </summary>
        /// <param name="avgMACO">The avg MACO.</param>
        private void LoadExpensesTree()
        {
            expensesTree.LoadExpensesTree(ContractId);
            
            // Update expenses tree
            expensesTree.SetCalculationInput(
                ConvertEx.ToDecimal(spinPlannedAvrMacoPerDal.EditValue),
                ConvertEx.ToDecimal(spinUpliftedAvrVolume.EditValue),
                ConvertEx.ToInt(spinPlannedOutletsQty.EditValue),
                CurrentPlannedSignDate, ContractTerm, checkAdvertTax.Checked ? expensesTree.TaxAdvertValue : 0M);
        }

        /// <summary>
        /// Loads the drop downs.
        /// </summary>
        private void LoadDropDowns()
        {
            DataTable dtContracts = DataProvider.GetActiveContracts(ClientId);
            dtContracts.PrimaryKey = new DataColumn[] { dtContracts.Columns[Constants.FIELD_ACTIVE_CONTRACT_ID] };
            cbContracts.Properties.PopupFormMinSize = cbContracts.Size;
            cbContracts.Properties.DataSource = dtContracts;
            if (dtContracts != null && dtContracts.Rows.Count > 0)
            {
                // select first value
                ContractId = ConvertEx.ToInt(dtContracts.Rows[0][cbContracts.Properties.ValueMember]);
                cbContracts.EditValue = ContractId;
            }
            else
            {
                ContractId = 0;
            }
        }

        /// <summary>
        /// Gets the display text by value.
        /// </summary>
        /// <param name="dataTable">The data table.</param>
        /// <param name="displayMember">The display member.</param>
        /// <param name="valueMember">The value member.</param>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        private static string GetDisplayTextByValue(DataTable dataTable, string displayMember, string valueMember, int value)
        {
            if (dataTable != null && dataTable.Rows.Count > 0)
            {
                foreach (DataRow row in dataTable.Rows)
                {
                    if (ConvertEx.ToInt(row[valueMember]) == value)
                    {
                        return ConvertEx.ToString(row[displayMember]);
                    }
                }
            }
            return string.Empty;
        }
     
        /// <summary>
        /// Sets all fields data.
        /// </summary>
        /// <value>All fields data.</value>
        private DataTable allFieldsTable;
        private DataTable AllFieldsData
        {
            set
            {
                allFieldsTable = value;
                if (allFieldsTable != null && allFieldsTable.Rows.Count == 1)
                {
                    DataRow row = allFieldsTable.Rows[0];

                    textPaymentSchema.EditValue = GetDisplayTextByValue(DataProvider.PaymentSchemaList,
                        Constants.FIElD_ACTIVE_CONTRACT_SCHEMA_DISPLAY, Constants.FIElD_ACTIVE_CONTRACT_SCHEMA_VALUE,
                        ConvertEx.ToInt(row[Constants.FIELD_COST_PAYMENT_SCHEMA_ID]));

                    textPaymentType.EditValue = GetDisplayTextByValue(DataProvider.PaymentTypeList,
                        Constants.FIElD_ACTIVE_CONTRACT_PAYMENT_DISPLAY, Constants.FIElD_ACTIVE_CONTRACT_PAYMENT_VALUE,
                        ConvertEx.ToInt(row[Constants.FIELD_COST_PAYMENT_TYPE_ID]));

                    spinPrevAvrVolume.EditValue = ConvertEx.ToDecimal(row[Constants.FIELD_COST_PREV_AVRVOLUME]);
                    spinMarketTrand.EditValue = ConvertEx.ToDecimal(row[Constants.FIELD_COST_MARKET_TRAND]);
                    spinTrandedAvrVolume.EditValue = ConvertEx.ToDecimal(row[Constants.FIELD_COST_TRANDED_AVRVOLUME]);
                    spinPlannedVolumeUplift.EditValue = ConvertEx.ToDecimal(row[Constants.FIELD_COST_PLANNED_VOLUMEUPLIFT]);
                    spinUpliftedAvrVolume.EditValue = ConvertEx.ToDecimal(row[Constants.FIELD_COST_UPLIFTED_AVRVOLUME]);
                    spinPlannedOutletsQty.EditValue = ConvertEx.ToDecimal(row[Constants.FIELD_COST_PLANNED_OUTLETSQANTITY]);
                    spinExpensesPerOutlet.EditValue = ConvertEx.ToDecimal(row[Constants.FIELD_COST_EXPENSES_PEROUTLET]);
                    spinPlannedAvrMacoPerDal.EditValue = ConvertEx.ToDecimal(row[Constants.FIELD_COST_PLANNED_AVRMACOPERDAL]);
                    checkAdvertTax.Checked = ConvertEx.ToBool(row[Constants.FIELD_COST_HAS_ADVERT]);
                }
            }
            get
            {
                if (allFieldsTable != null && allFieldsTable.Rows.Count == 1)
                {
                    DataRow row = allFieldsTable.Rows[0];

                    row[Constants.FIELD_COST_PAYMENT_SCHEMA_ID] = textPaymentSchema.EditValue;
                    row[Constants.FIELD_COST_PAYMENT_TYPE_ID] = textPaymentType.EditValue;
                    row[Constants.FIELD_COST_PREV_AVRVOLUME] = spinPrevAvrVolume.EditValue;
                    row[Constants.FIELD_COST_MARKET_TRAND] = spinMarketTrand.EditValue;
                    row[Constants.FIELD_COST_TRANDED_AVRVOLUME] = spinTrandedAvrVolume.EditValue;
                    row[Constants.FIELD_COST_PLANNED_VOLUMEUPLIFT] = spinPlannedVolumeUplift.EditValue;
                    row[Constants.FIELD_COST_UPLIFTED_AVRVOLUME] = spinUpliftedAvrVolume.EditValue;
                    row[Constants.FIELD_COST_PLANNED_OUTLETSQANTITY] = spinPlannedOutletsQty.EditValue;
                    row[Constants.FIELD_COST_EXPENSES_PEROUTLET] = spinExpensesPerOutlet.EditValue;
                    row[Constants.FIELD_COST_PLANNED_AVRMACOPERDAL] = spinPlannedAvrMacoPerDal.EditValue;
                    row[Constants.FIELD_COST_HAS_ADVERT] = checkAdvertTax.Checked;
                }
                return allFieldsTable;
            }
        }

        /// <summary>
        /// Handles the EditValueChanged event of the cbContracts control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void cbContracts_EditValueChanged(object sender, EventArgs e)
        {
            ContractId = ConvertEx.ToInt(cbContracts.EditValue);
            LoadContractView();
        }

        /// <summary>
        /// Event handler that is used to customise display text of current selected row in drop-down list from LookUpEdit.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cbContracts_CustomDisplayText(object sender, DevExpress.XtraEditors.Controls.CustomDisplayTextEventArgs e)
        {
            LookUpEdit edit = sender as LookUpEdit;
            DataTable table = edit.Properties.DataSource as DataTable;
            if (table != null && edit.ItemIndex >= 0) //Check wether selected item exists
            {
                DataRow selectedRow = table.Rows[edit.ItemIndex];
                e.DisplayText = string.Format(Resource.FieldContractNumCaption + ": {0}| "
                                            + Resource.FieldContractNameCaption + ": {1}| "
                                            + Resource.FieldContractSignedDateCaption + ": {2}| "
                                            + Resource.FieldContractValidTillCaption + ": {3}| "
                                            + Resource.FieldContractStatusCaption + ": {4} ",
                    selectedRow[Constants.CD_FIELD_CONTRACT_NUMBER],
                    selectedRow[Constants.CD_FIELD_CONTRACT_NAME],
                    selectedRow[Constants.CD_FIELD_CONTRACT_DATE_SIGNED],
                    selectedRow[Constants.CD_FIELD_CONTRACT_VALID_TILL],
                    selectedRow[Constants.CD_FIELD_CONTRACT_STATUS]);
            }
    
 
        }
    }
}

