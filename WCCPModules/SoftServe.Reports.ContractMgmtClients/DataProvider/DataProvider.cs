﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Logica.Reports.DataAccess;
using System.Collections.ObjectModel;
using Logica.Reports.Common;
using Logica.Reports.Common.WaitWindow;
using Logica.Reports.BaseReportControl.Utility;

namespace SoftServe.Reports.ContractMgmtClients
{
    /// <summary>
    /// 
    /// </summary>
    internal static class DataProvider
    {
        /// <summary>
        /// Gets the access level.
        /// </summary>
        /// <value>The access level.</value>
        internal static int AccessLevel
        {
            get
            {
                return ConvertEx.ToInt(ExecuteScalarStoredProcedure(Constants.SP_GET_ACCESS_LEVEL));
            }
        }

        /// <summary>
        /// Gets the user level. (M2, 3, 4...?)
        /// </summary>
        /// <value>The user level.</value>
        internal static UserLevelType UserLevel
        {
            get
            {
                return (UserLevelType)ConvertEx.ToInt(ExecuteScalarStoredProcedure(Constants.SP_GET_USER_LEVEL));
            }
        }

        /// <summary>
        /// Gets the contract client list.
        /// </summary>
        /// <value>The contract client list.</value>
        public static DataTable ContractClientList
        {
            get
            {
                return ExecuteStoredProcedure(Constants.SP_GET_CLIENT_LIST);
            }
        }

        /// <summary>
        /// Gets the contract status list.
        /// </summary>
        /// <value>The contract status list.</value>
        internal static DataTable ContractStatusList
        {
            get
            {
                return ExecuteStoredProcedure(Constants.SP_GET_CONTRACT_STATUS_LIST);
            }
        }

        /// <summary>
        /// Gets the client details.
        /// </summary>
        /// <param name="clientId">The client id.</param>
        /// <returns></returns>
        public static DataTable GetClientDetails(int clientId, long addressId)
        {
            object objClientId = DBNull.Value;
            object objAddressId = DBNull.Value;

            if (clientId > 0) objClientId = clientId;
            if (addressId > 0) objAddressId = addressId;

            return ExecuteStoredProcedure(Constants.SP_GET_CLIENT_COMMON_FIELDS,
                    new SqlParameter[] { 
                    new SqlParameter(Constants.SP_GET_CLIENT_DETAILS_PARAM1, objClientId),
                    new SqlParameter(Constants.SP_GET_CLIENT_DETAILS_PARAM2, objAddressId)
                });
        }

        public static DataTable GetClientAverageMonthlySales(int clientId)
        {
            return GetClientAverageMonthlySales(clientId, null);
        }

        /// <summary>
        /// Gets the client average monthly sales.
        /// </summary>
        /// <param name="clientId">The client id.</param>
        /// <returns></returns>
        public static DataTable GetClientAverageMonthlySales(int clientId, string pocList)
        {
            return ExecuteStoredProcedure(Constants.SP_GET_CLIENT_COMMON_AVG_SALES,
                    new SqlParameter[] 
                    {
                        new SqlParameter(Constants.SP_GET_CLIENT_COMMON_AVG_SALES_PARAM1, clientId) ,
                        new SqlParameter(Constants.SP_PARAM_OUTLET_LIST, pocList)

                    });
        }

        public static DataTable GetClientPOCList(int clientId)
        {
            return GetClientPOCList(clientId, null);
        }

        /// <summary>
        /// Gets the client POC list.
        /// </summary>
        /// <param name="clientId">The client id.</param>
        /// <returns></returns>
        public static DataTable GetClientPOCList(int clientId, string pocList)
        {
            return ExecuteStoredProcedure(Constants.SP_GET_CLIENT_COMMON_POC_LIST,
                    new SqlParameter[] 
                    {
                        new SqlParameter(Constants.SP_GET_CLIENT_COMMON_POC_LIST_PARAM1, clientId) ,
                        new SqlParameter(Constants.SP_PARAM_OUTLET_LIST, pocList)

                    });
        }

        public static DataTable GetClientDistributorList(int clientId, int distributorListNumber)
        {
            return GetClientDistributorList(clientId, distributorListNumber, null);
        }

        /// <summary>
        /// Gets the client distributor list.
        /// </summary>
        /// <param name="clientId">The client id.</param>
        /// <param name="distributorListNumber">The distributor list number.</param>
        /// <returns></returns>
        public static DataTable GetClientDistributorList(int clientId, int distributorListNumber, string pocList)
        {
            return ExecuteStoredProcedure(Constants.SP_GET_CLIENT_COMMON_DISTRIBUTORS,
                    new SqlParameter[] { 
                    new SqlParameter(Constants.SP_GET_CLIENT_COMMON_DISTRIBUTORS_PARAM1, clientId),
                    new SqlParameter(Constants.SP_GET_CLIENT_COMMON_DISTRIBUTORS_PARAM2, distributorListNumber),
                    new SqlParameter(Constants.SP_PARAM_OUTLET_LIST, pocList)
                });
        }

        /// <summary>
        /// Gets the region list.
        /// </summary>
        /// <value>The region list.</value>
        public static DataTable RegionList
        {
            get
            {
                    DataTable dataTable = ExecuteStoredProcedure(Constants.SP_GET_CLIENT_COMMON_REGIONS);
                    if (dataTable != null)
                    {
                        dataTable.DefaultView.Sort = Constants.FIELD_REGION_NAME;
                    }
                    return dataTable;
            }
        }

        /// <summary>
        /// Gets the district list.
        /// </summary>
        /// <param name="regionId">The region id.</param>
        /// <returns></returns>
        public static DataTable GetDistrictList(int regionId)
        {
            return ExecuteStoredProcedure(Constants.SP_GET_CLIENT_COMMON_DISTRICTS,
                    new SqlParameter[] { 
                    new SqlParameter(Constants.SP_GET_CLIENT_COMMON_DISTRICTS_PARAM1, regionId)
                });
        }

        /// <summary>
        /// Gets the city type list.
        /// </summary>
        /// <value>The city type list.</value>
        public static DataTable CityTypeList
        {
            get
            {
                return ExecuteStoredProcedure(Constants.SP_GET_CLIENT_COMMON_CITY_TYPE);
            }
        }

        /// <summary>
        /// Gets the city list.
        /// </summary>
        /// <param name="districtId">The district id.</param>
        /// <param name="cityTypeId">The city type id.</param>
        /// <returns></returns>
        public static DataTable GetCityList(int districtId, int cityTypeId)
        {
            return ExecuteStoredProcedure(Constants.SP_GET_CLIENT_COMMON_CITY_LIST,
                    new SqlParameter[] { 
                    new SqlParameter(Constants.SP_GET_CLIENT_COMMON_CITY_LIST_PARAM1, districtId),
                    new SqlParameter(Constants.SP_GET_CLIENT_COMMON_CITY_LIST_PARAM2, cityTypeId)
                });
        }

        /// <summary>
        /// Gets the taxes list.
        /// </summary>
        /// <value>The taxes list.</value>
        public static DataTable TaxesList
        {
            get
            {
                return ExecuteStoredProcedure(Constants.SP_GET_CLIENT_COMMON_TAXES_LIST);
            }
        }

        /// <summary>
        /// Gets the channels list.
        /// </summary>
        /// <value>The channels list.</value>
        public static DataTable ChannelsList
        {
            get
            {
                return ExecuteStoredProcedure(Constants.SP_GET_CHANNEL_LIST);
            }
        }

        /// <summary>
        /// Gets the address status list.
        /// </summary>
        /// <value>The address status list.</value>
        public static DataTable AddressStatusList
        {
            get
            {
                DataTable dataTable = new DataTable();
                dataTable.Columns.Add("ID", typeof(byte));
                dataTable.Columns.Add("Name", typeof(string));
                dataTable.Rows.Add(new object[] { (byte)AddressRowStatus.Common, "Нет изменений" });
                dataTable.Rows.Add(new object[] { (byte)AddressRowStatus.New, "Добавлена" });
                dataTable.Rows.Add(new object[] { (byte)AddressRowStatus.Removed, "Удалена" });
                return dataTable;
            }
        }

        /// <summary>
        /// Gets the addresses list.
        /// </summary>
        /// <param name="clientId">The client id.</param>
        /// <param name="channelId">The channel id.</param>
        /// <returns></returns>
        public static DataTable GetAddressesList(int clientId)
        {
            return ExecuteStoredProcedure(Constants.SP_GET_CLIENT_ADDRESSES_LIST,
                    new SqlParameter[] { 
                    new SqlParameter(Constants.SP_GET_CLIENT_ADDRESSES_LIST_PARAM1, clientId)
                });
        }

        /// <summary>
        /// Inserts the client common.
        /// </summary>
        /// <param name="client">The client.</param>
        /// <param name="editMode">if set to <c>true</c> [edit mode].</param>
        public static int InsertClientCommon(ClientDetailsDTO client, bool edit)
        {
            try
            {
                List<SqlParameter> sqlParameters = new List<SqlParameter> {
                new SqlParameter(Constants.SP_UPDATE_CLIENT_COMMON_PARAM1, client.Id),
                new SqlParameter(Constants.SP_UPDATE_CLIENT_COMMON_PARAM2, client.Name),
                new SqlParameter(Constants.SP_UPDATE_CLIENT_COMMON_PARAM3, client.Address),
                new SqlParameter(Constants.SP_UPDATE_CLIENT_COMMON_PARAM4, client.Edrpou),
                new SqlParameter(Constants.SP_UPDATE_CLIENT_COMMON_PARAM5, client.Iin),
                new SqlParameter(Constants.SP_UPDATE_CLIENT_COMMON_PARAM6, client.TaxId),
                new SqlParameter(Constants.SP_UPDATE_CLIENT_COMMON_PARAM7, client.CityId),
                new SqlParameter(Constants.SP_UPDATE_CLIENT_COMMON_PARAM8, client.Bookkeeper),
                new SqlParameter(Constants.SP_UPDATE_CLIENT_COMMON_PARAM9, client.BookkeeperPhone),
                new SqlParameter(Constants.SP_UPDATE_CLIENT_COMMON_PARAM10, client.Notes),
                new SqlParameter(Constants.SP_UPDATE_CLIENT_COMMON_PARAM11, client.ContactName),
                new SqlParameter(Constants.SP_UPDATE_CLIENT_COMMON_PARAM12, client.ContactPosition),
                new SqlParameter(Constants.SP_UPDATE_CLIENT_COMMON_PARAM13, client.ContactPhone),
                new SqlParameter(Constants.SP_UPDATE_CLIENT_COMMON_PARAM14, client.ContactMobile),
                new SqlParameter(Constants.SP_UPDATE_CLIENT_COMMON_PARAM15, client.ContactEmail)
            };

                if (!edit)
                {
                    sqlParameters.RemoveAt(0); // remove ID param
                }

                object retVal = ExecuteScalarStoredProcedure(
                    edit ? Constants.SP_UPDATE_CLIENT_COMMON : Constants.SP_INSERT_CLIENT_COMMON,
                    sqlParameters.ToArray());

                if (retVal != null && retVal is int) 
                {
                    return (int)retVal;
                }
            }
            catch (Exception e)
            {
                ErrorManager.ShowErrorBox(e.Message);
            }

            return -1;
        }

        /// <summary>
        /// Deletes the client common.
        /// </summary>
        /// <param name="client">The client.</param>
        public static void DeleteClientCommon(ClientDetailsDTO client)
        {
                DataAccessLayer.ExecuteStoredProcedure(Constants.SP_DELETE_CLIENT_COMMON, new [] {
                new SqlParameter(Constants.SP_UPDATE_CLIENT_COMMON_PARAM1, client.Id)
            });
        }

        /// <summary>
        /// Deletes the client common.
        /// </summary>
        /// <param name="client">The client.</param>
        public static void SetOutletDependentData(int clientId)
        {

            ExecuteStoredProcedure(Constants.SP_SET_OUTLET_DEPENDENT_DATA, new[] 
                {
                    new SqlParameter(Constants.SP_UPDATE_CLIENT_COMMON_PARAM1, clientId)
                });
        }

        /// <summary>
        /// Inserts the address.
        /// </summary>
        /// <param name="address">The address.</param>
        public static DataTable SaveAddresses(int clientId, string addressesToAdd, string addressesToDelete)
        {
            ExecuteStoredProcedure(Constants.SP_DELETE_ADDRESS, new SqlParameter[] {
                new SqlParameter(Constants.SP_SAVE_ADDRESS_PARAM_CLIENT_ID, clientId),
                new SqlParameter (Constants.SP_SAVE_ADDRESS_PARAM_OUTLETS_LIST, addressesToDelete)
            });

            return ExecuteStoredProcedure(Constants.SP_INSERT_ADDRESS, new SqlParameter[] {
                new SqlParameter(Constants.SP_SAVE_ADDRESS_PARAM_CLIENT_ID, clientId),
                new SqlParameter(Constants.SP_SAVE_ADDRESS_PARAM_OUTLETS_LIST, addressesToAdd)
            });
        }

        /// <summary>
        /// Gets the contract efficiency list.
        /// </summary>
        /// <param name="contractId">The contract id.</param>
        /// <returns></returns>
        public static DataTable GetContractEfficiency(int contractId)
        {
            return ExecuteStoredProcedure(Constants.SP_GET_CONTRACT_EFFICIENCY,
                    new SqlParameter[] { 
                    new SqlParameter(Constants.SP_GET_CONTRACT_EFFICIENCY_PARAM_CONTRACT_ID, contractId)
                });
        }

        /// <summary>
        /// Gets the contract sales list.
        /// </summary>
        /// <param name="contractId">The contract id.</param>
        /// <returns></returns>
        public static DataTable GetContractSales(int contractId)
        {
            return ExecuteStoredProcedure(Constants.SP_GET_CONTRACT_SALES,
                    new SqlParameter[] { 
                    new SqlParameter(Constants.SP_GET_CONTRACT_SALES_PARAM_CONTRACT_ID, contractId)
                });
        }

        /// <summary>
        /// Gets the contract expenses list.
        /// </summary>
        /// <param name="contractId">The contract id.</param>
        /// <returns></returns>
        public static DataTable GetContractExpenses(int contractId)
        {
            return ExecuteStoredProcedure(Constants.SP_GET_CONTRACT_EXPENSES,
                    new SqlParameter[] { 
                    new SqlParameter(Constants.SP_GET_CONTRACT_EXPENSES_PARAM_CONTRACT_ID, contractId)
                });
        }

        /// <summary>
        /// Gets the addresses list by OLID.
        /// </summary>
        /// <param name="strIdList">The STR id list.</param>
        /// <returns></returns>
        public static DataTable GetAddressesListByOLIDs(string strIdList)
        {
            return ExecuteStoredProcedure(Constants.SP_GET_CLIENT_ADDRESSES_LIST_BY_OLID,
                new SqlParameter[] { 
                        new SqlParameter(Constants.SP_GET_CLIENT_ADDRESSES_LIST_BY_OLID_PARAM1, strIdList)
                    });
        }

        /// <summary>
        /// Gets the active contracts list.
        /// </summary>
        /// <param name="clientId">The client id.</param>
        /// <returns></returns>
        public static DataTable GetActiveContracts(int clientId)
        {
            return ExecuteStoredProcedure(
                Constants.SP_GET_ACTIVE_CONTRACTS,
                new SqlParameter[] 
                { 
                    new SqlParameter(Constants.SP_GET_ACTIVE_CONTRACTS_PARAM_CLIENT_ID, clientId)
                });
        }

        /// <summary>
        /// Gets the investment details.
        /// </summary>
        /// <param name="contractId">The contract id.</param>
        /// <returns></returns>
        internal static DataTable GetInvestmentDetails(int contractId)
        {
            return ExecuteStoredProcedure(
                Constants.SP_GET_EXPENSES_FIELDS,
                new SqlParameter[] 
                { 
                    new SqlParameter(Constants.SP_GET_EXPENSES_FIELDS_PARAM1, contractId)
                });
        }

        /// <summary>
        /// Gets the payment schema list.
        /// </summary>
        /// <value>The payment schema list.</value>
        internal static DataTable PaymentSchemaList
        {
            get
            {
                return ExecuteStoredProcedure(Constants.SP_GET_EXPENSES_PAYMENT_SCHEMA_LIST);
            }
        }

        /// <summary>
        /// Gets the type of the payment.
        /// </summary>
        /// <value>The type of the payment.</value>
        internal static DataTable PaymentTypeList
        {
            get
            {
                return ExecuteStoredProcedure(Constants.SP_GET_EXPENSES_PAYMENT_TYPE_LIST);
            }
        }

        internal static DataTable GetIndicatorsTable(int contractId)
        {
            return ExecuteStoredProcedure(
                Constants.SP_GET_EXPENSES_INDICATORS_TABLE,
                new SqlParameter[] 
                { 
                    new SqlParameter(Constants.SP_GET_EXPENSES_INDICATORS_TABLE_PARAM1, contractId)
                });
        }

        internal static DataTable GetClientHistory(int clientId)
        {
            return ExecuteStoredProcedure(
                Constants.SP_GET_CONTRACT_HISTORY,
                new SqlParameter[] 
                { 
                    new SqlParameter(Constants.SP_GET_CONTRACT_HISTORY_PARAM_CLIENT_ID, clientId)
                });
        }

        /// <summary>
        /// Gets the has access to addresses.
        /// </summary>
        /// <param name="clientId">The client id.</param>
        /// <returns></returns>
        internal static bool GetHasAccessToAddresses(int clientId)
        {
            return ConvertEx.ToBool(ExecuteScalarStoredProcedure(
                Constants.SP_GET_HAS_ACCESS_TO_ADDRESSES,
                new SqlParameter[] 
                { 
                    new SqlParameter(Constants.SP_GET_HAS_ACCESS_TO_ADDRESSES_PARAM1, clientId)
                }));
        }

        /// <summary>
        /// Executes the stored procedure.
        /// </summary>
        /// <param name="spName">Name of the sp.</param>
        /// <returns></returns>
        private static DataTable ExecuteStoredProcedure(string spName)
        {
            return ExecuteStoredProcedure(spName, null);
        }

        /// <summary>
        /// Executes the stored procedure.
        /// </summary>
        /// <param name="spName">Name of the sp.</param>
        /// <param name="paramList">The param list.</param>
        /// <returns></returns>
        private static DataTable ExecuteStoredProcedure(string spName, params SqlParameter[] paramList)
        {
            WaitManager.StartWait();
            DataTable dataTable = null;
            try
            {
                DataSet dataSet = null;
                if (paramList != null)
                {
                    dataSet = DataAccessLayer.ExecuteStoredProcedure(spName, paramList);
                }
                else
                {
                    dataSet = DataAccessLayer.ExecuteStoredProcedure(spName);
                }
                if (dataSet != null && dataSet.Tables.Count > 0)
                {
                    dataTable = dataSet.Tables[0];
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler.HandleException(ex);
            }
            WaitManager.StopWait();
            return dataTable;
        }

        /// <summary>
        /// Executes the stored procedure.
        /// </summary>
        /// <param name="spName">Name of the sp.</param>
        /// <param name="paramList">The param list.</param>
        /// <returns></returns>
        private static object ExecuteScalarStoredProcedure(string spName, params SqlParameter[] paramList)
        {
            DataTable dataTable = ExecuteStoredProcedure(spName, paramList);
            if (dataTable != null && dataTable.Rows.Count > 0) { return dataTable.Rows[0][0]; }
            
            return null;
        }
    }
}