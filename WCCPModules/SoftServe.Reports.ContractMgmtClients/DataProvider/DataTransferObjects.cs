﻿using System;

namespace SoftServe.Reports.ContractMgmtClients
{
    /// <summary>
    /// 
    /// </summary>
    public class CommonBaseExternalData
    {
        public int AccessLevel { get; set; }
        public bool HasAccessToAllAddresses { get; set; }
        public UserLevelType UserLevel { get; set; }

        /// <summary>
        /// Checks the access level.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <returns></returns>
        public bool CheckAccessLevel(AccessLevelType type)
        {
            return (((int)AccessLevel & (int)type) == (int)type);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public class ClientDetailsDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string Edrpou { get; set; }
        public string Iin { get; set; }
        public int TaxId { get; set; }
        public int CityId { get; set; }
        public string Bookkeeper { get; set; }
        public string BookkeeperPhone { get; set; }
        public string Notes { get; set; }
        public string ContactName { get; set; }
        public string ContactPosition { get; set; }
        public string ContactPhone { get; set; }
        public string ContactMobile { get; set; }
        public string ContactEmail { get; set; }
        public int RecordId { get; set; }
        public int ErrorCode { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class AddressDTO
    {
        public Int64 Id { get; set; }
        public int ClientId { get; set; }
        public Int64 OutletId { get; set; }
        public int RecordId { get; set; }
        public int ErrorCode { get; set; }
    }
}
