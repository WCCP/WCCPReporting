﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;

namespace SoftServe.Reports.ContractMgmtClients.Utility
{

    /// <summary>
    /// Static help class, which faciliates building of string expression with different conditions.
    /// </summary>
    public static class HighlightingExpression
    {
        /// <summary>
        /// Builds expression within specified date range according to input condition field
        /// </summary>
        /// <param name="from">Start date</param>
        /// <param name="to">End date</param>
        /// <param name="conditionField">Condition field</param>
        /// <returns></returns>
        public static string BuildDateRangeHighlightExpression(DateTime from, DateTime to, string conditionField, params string[] additionalConditions)
        {
            string rangeExpr = string.Empty;

            if (from != null || to != null)
            {
                rangeExpr = string.Format(@"[{0}] != ?", conditionField);

                if (from != null)
                {
                    rangeExpr += string.Format(@" And [{0}] >= #{1}#",
                        conditionField, from.ToString(CultureInfo.CurrentUICulture));
                }
                if (to != null)
                {
                    rangeExpr += string.Format(@" And [{0}] <= #{1}#",
                        conditionField, to.ToString(CultureInfo.CurrentUICulture));
                }
            }
            if (additionalConditions != null)
            {
                foreach (string s in additionalConditions)
                {
                    rangeExpr += string.Format(" And {0}", s);
                }
            }
            return rangeExpr;
        }
    }
}
