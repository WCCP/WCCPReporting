﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Windows.Forms;
using DevExpress.Utils;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraEditors.Drawing;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraEditors.ViewInfo;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using SoftServe.Reports.AGNTProject.DataAccess;
using SoftServe.Reports.AGNTProject.Models;
using SoftServe.Reports.AGNTProject.Utils;
using Region = SoftServe.Reports.AGNTProject.Models.Region;

namespace SoftServe.Reports.AGNTProject
{
    public partial class SettingsForm : XtraForm
    {
        private bool _isLoading;
        private int _checkAllEditSatate;
        
        public readonly Settings Settings = new Settings();

        public SettingsForm()
        {
            InitializeComponent();
            SetCustomColumnCaption();
            SetCustomRepositoryEditors();           
        }

        public void LoadDataSources()
        {
            _isLoading = true;
            LoadUserLevel();
            LoadDatePeriods();
            LoadCountries(((Period) cbxPeriod.SelectedItem).ID);
            LoadRegions();
            PopulateGrid();
            _isLoading = false;
        }

        private void PopulateGrid()
        {
            gridCntrSettings.DataSource = DataRepository.GetSettingsGridSource(Settings.UserLevel);
        }

        private void LoadUserLevel()
        {
            AccessToApp accessToApp = AccessToApp.GetInstance();
        }

        private void LoadDatePeriods()
        {
            List<Period> lPeriods = DataProvider.GetPeriods();

            if (lPeriods.Count == 0)
            {
                lPeriods.Add(new Period() {ID = -1, Name = "Нет активного периода"});
                return;
            }

            ComboBoxItemCollection coll = cbxPeriod.Properties.Items;
            int yMonth = int.Parse(string.Concat(DateTime.Now.Year.ToString(),
                DateTime.Now.Month.ToString()));
            coll.BeginUpdate();
            foreach (var p in lPeriods)
            {
                coll.Add(p);
            }
            // to display current month period by default
            int current_prd_idx = lPeriods.FindIndex(o => o.ID == yMonth);

            if (current_prd_idx != -1)
            {
                cbxPeriod.SelectedIndex = current_prd_idx;
            }
            // if there is no current month period get the last one
            else
            {
                cbxPeriod.SelectedIndex = lPeriods.FindIndex(p => p.ID == lPeriods.Max(o => o.ID));
            }
            coll.EndUpdate();
        }

        private void LoadCountries(int periodId)
        {
            List<Country> lCountries = DataProvider.GetCountries(periodId);

            if (lCountries.Count == 0 || periodId == -1)
            {
                lCountries.Add(new Country() {ID = -1, Name = "Нет доступной страны"});
            }

            cbxCountry.Enabled = lCountries.First().ID != -1;

            ComboBoxItemCollection coll = cbxCountry.Properties.Items;
            coll.Clear();

            coll.BeginUpdate();
            foreach (var c in lCountries)
            {
                coll.Add(c);
            }
            cbxCountry.SelectedIndex = lCountries.Count > 1
                ? lCountries.FindIndex(c => c.ID == lCountries.Max(o => o.ID))
                //If there are two countries by default is Russia;
                : lCountries.Count - 1;
            coll.EndUpdate();

        }

        private void LoadRegions()
        {
            if (_isLoading || DataRepository.Regions == null)
                DataRepository.Regions = DataProvider.GetRegions().OrderBy(r => r.Name).ToList();

            if (DataRepository.Regions.Count == 0 || ((Country) cbxCountry.SelectedItem).ID == -1)
            {
                DataRepository.Regions.Clear();
                DataRepository.Regions.Add(new Region() {ID = -1, Name = "Нет доступных регионов", CountryID = -1});
            }

            cbxRegion.Enabled = DataRepository.Regions.First().ID != -1;

            CheckedListBoxItemCollection coll = cbxRegion.Properties.Items;
            coll.Clear();
            int countryId = ((Country) cbxCountry.SelectedItem).ID;

            coll.BeginUpdate();
            foreach (var r in DataRepository.Regions)
            {
                if (r.CountryID == countryId)
                    coll.Add(r);
            }
            cbxRegion.CheckAll();
            coll.EndUpdate();
        }

        private void SetCustomColumnCaption()
        {
            var itemIndex = gridCntrSettings.RepositoryItems.Add(new RepositoryItemCheckEdit());
            checkAllEdit = gridCntrSettings.RepositoryItems[itemIndex] as RepositoryItemCheckEdit;
            checkAllEdit.ValueChecked = 1;
            checkAllEdit.ValueUnchecked = 0;
            _checkAllEditSatate = GetReportsChkBoxesState();
            gridView1.CustomDrawColumnHeader += gridView_CustomDrawColumnHeader;
        }

        private void SetCustomRepositoryEditors()
        {
            var lst = DataRepository.LookupSource;
            rLkpMode.DataSource = lst;
            rLkpMode.ValueMember = "ID";
            rLkpMode.DisplayMember = "Name";
            rLkpMode.Columns.Add(new LookUpColumnInfo("Name"));
            rLkpMode.ShowHeader = false;
            rLkpMode.ShowFooter = false;
            rLkpMode.DropDownRows = lst.Count;
            rLkpMode.PopupFormMinSize = new Size(10, rLkpMode.PopupFormMinSize.Height);
        }

        private int GetReportsChkBoxesState()
        {
            if (gridView1.GridControl.DataSource is DataTable)
            {
                DataTable allowed = ((DataTable)gridView1.GridControl.DataSource).AsEnumerable().Where(v => v.Field<bool>("IsAllowed")).CopyToDataTable();
                int selectedCount = allowed.AsEnumerable().Count(r => r.Field<bool>("Generate"));

                if (selectedCount != 0 && selectedCount == allowed.Rows.Count)
                {
                    return 1;
                }
                if (selectedCount != 0 && selectedCount != allowed.Rows.Count)
                {
                    return -1;
                }
                return 0;
            }
            return 0;
        }

        private void DrawCheckBox(Graphics g, Rectangle r, RepositoryItemCheckEdit checkEdit, int isCheck)
        {
            CheckEditViewInfo info = default(CheckEditViewInfo);
            CheckEditPainter painter = default(CheckEditPainter);
            ControlGraphicsInfoArgs args = default(ControlGraphicsInfoArgs);
            checkEdit.GlyphAlignment = HorzAlignment.Far;
            checkEdit.Caption = @"Все";
            info = (CheckEditViewInfo) checkEdit.CreateViewInfo();
            painter = (CheckEditPainter) checkEdit.CreatePainter();

            info.EditValue = isCheck;
            _checkAllEditSatate = isCheck;
            info.Bounds = r;
            info.CalcViewInfo(g);
            args = new ControlGraphicsInfoArgs(info, new DevExpress.Utils.Drawing.GraphicsCache(g), r);
            painter.Draw(args);
            args.Cache.Dispose();
        }

        private bool DisableEditor(GridView view, int row)
        {
            GridColumn colCheckedReport = view.Columns.ColumnByFieldName(gridColCheckRep.FieldName);

            if ((bool) (view.GetRowCellValue(row, colCheckedReport)))
            {
                GridColumn colReportId = view.Columns.ColumnByFieldName(gridColReportId.FieldName);
                string val = (view.GetRowCellValue(row, colReportId)).ToString();
                //Allow editor for reports ID 2,4
                return (!(val == "2" || val == "4"));
            }
            // It mean's that aditor must be disabled
            return true;

        }

        private void LoadSetting()
        {
            Settings.ShowTotals = checkTotals.Checked;
            Settings.CountryId = ((Country) cbxCountry.SelectedItem).ID;
            var period = (Period) cbxPeriod.SelectedItem;
            Settings.PeriodName = period.Name;
            if (Settings.CountryId == -1)
            {
                return;
            }
            StartCsParam startCsParam = new StartCsParam(period.ID, Settings.CountryId);

            Settings.PeriodId = startCsParam.GetPeriodId();
            Settings.Status = startCsParam.GetStatus();
            Settings.RegionIDsList =
                cbxRegion.Properties.Items.GetCheckedValues().Select(v => ((Region) v).ID.ToString()).ToList();
            Settings.SelectedReports.Clear();
            for (var row = 0; row < gridView1.RowCount; row++)
            {
                if (Convert.ToBoolean(gridView1.GetRowCellValue(row, gridColCheckRep.FieldName)))
                {
                    Settings.SelectedReports.Add(gridView1.GetRowCellValue(row, gridColReport.FieldName).ToString(),
                        gridView1.GetRowCellValue(row, gridColModeValue.FieldName).ToString());
                }
            }
        }

        private bool ValidateSetting(out string message)
        {
            string lMessage = string.Empty;
            if (Settings.CountryId == -1)
            {
                lMessage += "- У пользователя нет привязаного персонала" + Environment.NewLine;
                message = lMessage;
                return string.IsNullOrEmpty(message);
            }
                if (Settings.RegionIDsList.Count == 0)
                lMessage += "- Не задан регион" + Environment.NewLine;
            if (Settings.SelectedReports.Count == 0)
                lMessage += "- Не выбрано ни одного отчета" + Environment.NewLine;
            
            if (Settings.SelectedReports.ContainsKey("Прогноз затрат"))
            {
                decimal percent = 0;

                decimal.TryParse(Settings.SelectedReports["Прогноз затрат"].Replace(".", ","), out percent);
                if (percent <= 0)
                {
                    lMessage += "- \"Прогноз затрат\", процент должен быть больше нуля" + Environment.NewLine;
                }
            }

            message = lMessage;

            return string.IsNullOrEmpty(lMessage);
        }

        private void gridView_CustomDrawColumnHeader(object sender,
            ColumnHeaderCustomDrawEventArgs e)
        {
            if (e.Column == null || e.Column.Name != gridColCheckRep.Name)
                return;
            if (e.Column.AbsoluteIndex != 1)
                return;
            Rectangle rect = e.Bounds;
            rect.Inflate(-1, -1);

            e.Info.InnerElements.Clear();
            e.Painter.DrawObject(e.Info);
            DrawCheckBox(e.Graphics, rect, checkAllEdit, _checkAllEditSatate);
            e.Handled = true;
        }

        private void cbxPeriod_SelectedIndexChanged(object sender, EventArgs e)
        {
            /*if (_isLoading)
                return;
            int periodId = ((Period) cbxPeriod.SelectedItem).ID;
            LoadCountries(periodId);
            DataRepository.Regions = null;
            LoadRegions();*/
        }

        private void cbxCountry_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (_isLoading)
                return;
            LoadRegions();
            btnGenerate.Enabled = cbxCountry.Enabled && cbxRegion.Enabled;
        }

        private void gridView1_CustomRowCellEdit(object sender, CustomRowCellEditEventArgs e)
        {
            if (e.Column == null || e.Column.FieldName != gridColModeValue.FieldName)
                return;
            GridView gv = sender as GridView;
            GridColumn c = gv.Columns.ColumnByFieldName(gridColReportId.FieldName);
            string fieldValue = gv.GetRowCellValue(e.RowHandle, c).ToString();

            switch (fieldValue)
            {
                case "2":
                    e.RepositoryItem = rLkpMode;
                    break;
                case "4":
                    e.RepositoryItem = rTxEdit;
                    break;
            }
        }

        private void gridView1_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            checkAllEdit.BeginUpdate();
            checkAllEdit.CreateViewInfo().EditValue = _checkAllEditSatate = GetReportsChkBoxesState();
            checkAllEdit.EndUpdate();
            GridView view = sender as GridView;

            if (e.Column.FieldName == gridColCheckRep.FieldName)
            {
                var repId = view.GetRowCellValue(e.RowHandle, gridColReportId.FieldName).ToString();
                switch (repId)
                {
                    case "2":
                        rLkpMode.Enabled = (bool) e.Value;
                        break;
                    case "4":
                        rTxEdit.Enabled = (bool)e.Value;
                        break;
                }
            }
        }

        private void gridView1_CellValueChanging(object sender,
            DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            if (e.Column.FieldName == gridColCheckRep.FieldName)
            {
                e.Column.View.SetRowCellValue(e.RowHandle, e.Column.FieldName, e.Value);                
            }
        }

        private void gridView1_MouseDown(object sender, MouseEventArgs e)
        {
            GridHitInfo info = ((GridView) sender).CalcHitInfo(e.Location);

            if (info.InColumnPanel && info.Column != null && info.Column.FieldName == gridColCheckRep.FieldName)
            {
                int oldState = _checkAllEditSatate;
                for (int i = 0; i < gridView1.RowCount; i++)
                {
                    if ((bool) gridView1.GetRowCellValue(i, gridColIsAllowed.FieldName))
                        gridView1.SetRowCellValue(i, info.Column, oldState != 1);
                }
                checkAllEdit.BeginUpdate();
                checkAllEdit.CreateViewInfo().EditValue = _checkAllEditSatate;
                checkAllEdit.EndUpdate();
            }
        }

        private void gridView1_ShowingEditor(object sender, System.ComponentModel.CancelEventArgs e)
        {
            GridView view = sender as GridView;
            if (view == null)
                return;

            //If User has only AccessLevel.BaseEmplEditor then rest reports must be shown as disabled
            if (!(bool) view.GetListSourceRowCellValue(view.FocusedRowHandle, gridColIsAllowed))
                e.Cancel = true;

            if (view.FocusedColumn.FieldName == gridColModeValue.FieldName && DisableEditor(view, view.FocusedRowHandle))

                e.Cancel = true;
        }

        private void btnGenerate_Click(object sender, EventArgs e)
        {            
            LoadSetting();

            string lWarningMessage;
            if (ValidateSetting(out lWarningMessage))
            {
                DialogResult = DialogResult.OK;
            }
            else
            {
                XtraMessageBox.Show("Некорректный набор параметров:" + Environment.NewLine + lWarningMessage,
                    "Параметры", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }

        }

        private void Отмена_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        private void gridView1_RowCellStyle(object sender, RowCellStyleEventArgs e)
        {
            GridView view = sender as GridView;
            if (view == null)
                return;

            
            if (!(bool)view.GetListSourceRowCellValue(e.RowHandle, gridColIsAllowed))
                e.Appearance.ForeColor = Color.DarkGray;


            if (e.Column.FieldName == gridColModeValue.FieldName && (bool)view.GetListSourceRowCellValue(e.RowHandle, gridColIsAllowed))
            {
                var repId = view.GetRowCellValue(e.RowHandle, gridColReportId.FieldName).ToString();

                switch (repId)
                {
                    case "2"://Partner Payments report
                        e.Appearance.ForeColor = (bool) view.GetRowCellValue(e.RowHandle, gridColCheckRep.FieldName)
                            ? Color.FromName("ff201f35")
                            : Color.DarkGray;
                        break;
                    case "4"://Costs forecast report
                        e.Appearance.ForeColor = (bool)view.GetRowCellValue(e.RowHandle, gridColCheckRep.FieldName)
                            ? Color.FromName("ff201f35")
                            : Color.DarkGray;
                        break;
                }
            }
        }

        private void gridView1_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            GridView view = sender as GridView;
            if (view == null)
                return;

            if (e.Column == gridColModeValue && view.GetRowCellValue(e.RowHandle, gridColReportId).ToString() == "4")
            {
                decimal displayVal = 0;
                var cellVal = view.GetRowCellValue(e.RowHandle, gridColModeValue);

                if (cellVal == null)
                    return;

                if (!decimal.TryParse(cellVal.ToString().Replace(".", ","), NumberStyles.Any, CultureInfo.CurrentCulture, out displayVal))
                    return;
                displayVal = displayVal / 100;
                e.DisplayText = displayVal.ToString("P");
            }
        }
    }
}
