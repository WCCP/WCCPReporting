﻿using System;
using ModularWinApp.Core.Interfaces;
using System.Reflection;
using System.Windows.Forms;
using System.ComponentModel.Composition;
using Logica.Reports.Common;

namespace SoftServe.Reports.AGNTProject
{
    [PartCreationPolicy(CreationPolicy.NonShared)]
    [Export(typeof(IStartupClass))]
    [ExportMetadata("ModuleName", "wccpAGNTProject.dll")]
    class WccpUI: IStartupClass
    {
        #region Fields
        private WccpUIControl _reportControl;
        private string _reportCaption;
        #endregion

        #region Properties
        public Control ReportControl
        {
            get { return _reportControl; }
        }
        
        public string ReportCaption
        {
            get { return _reportCaption; }
        }
        #endregion

        #region Methods
        public string GetVersion()
        {
            return Assembly.GetExecutingAssembly().GetName().Version.ToString();
        }

        public int ShowUI(int reportId, string reportCaption)
        {
            try
            {
                _reportControl = new WccpUIControl();
                _reportCaption = reportCaption;

                _reportControl.RestoreLayout();

                return _reportControl.InitReport();
            }
            catch (Exception lException)
            {               
                ErrorManager.ShowErrorBox(lException.Message);
            }

            return 1;
        }

        public void CloseUI()
        {
            if (_reportControl != null)
            {
                _reportControl.SaveLayout();
            }
        }

        public bool AllowClose()
        {
            return _reportControl.AllowClose();
        }
        #endregion

        static void Main(string[] args)
        {
            SettingsForm s = new SettingsForm();
            s.ShowDialog();
            WccpUIControl reportControl = new WccpUIControl();
            reportControl.InitReport();
        }
    }
}
