﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace SoftServe.Reports.AGNTProject.DataAccess
{
    public  static  class Constants
    {
        //Procedures
        public const string SP_GET_PERIOD = "AP.GetPeriod";
        public const string SP_GET_COUNTRY = "AP.GetCountry";
        public const string SP_GET_REGION = "AP.GetRegion";
        public const string SP_GET_PERIOD_ID = "AP.GetStartCSParam";
        public const string SP_GET_USER_ACCESS_LEVEL = "AP.GetAccessLevel";


        public const string SP_GET_COSTS_LIMITS = "AP.GetLimitsOfSpends";
        public const string SP_INSERT_COSTS_LIMITS = "AP.INS_CS_LimitsOfSpends";
        public const string SP_UPDATE_COSTS_LIMITS = "AP.UPD_CS_LimitsOfSpends";

        public const string SP_GET_BONUS_AMOUNT = "AP.GetBonusAmount";       
        public const string SP_INSERT_BONUS_AMOUNT = "AP.INS_CS_BonusAmount";
        public const string SP_UPDATE_BONUS_AMOUNT = "AP.UPD_CS_BonusAmount";

        public const string SP_GET_AMORTIZATION = "AP.GetAmortization";        
        public const string SP_INSERT_AMORTIZATION = "AP.INS_CS_Amortization";
        public const string SP_UPDATE_AMORTIZATION = "AP.UPD_CS_Amortization";

        public const string SP_GET_COUNTRY_SETTINGS = "AP.GetCountrySettings";
        public const string SP_UPDATE_COUNTRY_SETTINGS = "AP.UPD_CS_CountrySettings";

        public const string SP_GET_AGENT_PAYMENT_TYPES = "AP.GetAgentPaymentTypes";        
        public const string SP_UPDATE_AGENT_PAYMENT_TYPES = "AP.EditAgentPaymentTypes";

        public const string SpGetPartnerPayments = "AP.GetPaymentWithPartner";
        public const string SpGetDistrib = "AP.GetDistrib";
        public const string GetLastOpenPeriod = "AP.GetLastOpenPeriod";
        public const string SpGetRegionDistrib = "AP.GetRegionDistrib";
        public const string SpGetAgentPaymentTypesDistrib = "AP.GetAgentPaymentTypesDistrib";
        public const string SpGetEmployeesBase = "AP.GetEmployeesBase";
        public const string SpGetLimitsOfSpendsByCode = "AP.GetLimitsOfSpendsByCode";
        public const string SpEditPeriodStatus = "AP.EditPeriodStatus";
        public const string SpGetRegionCountry = "AP.GetRegionCountry";
        public const string SpGetCostForecast = "AP.GetCostForecast";
        public const string SpGetCostFact = "AP.GetCostFact";

        //Parameters
        public const string PAR_PERIOD = "@period";
        public const string PAR_PERIOD_ID = "@period_id";
        public const string PAR_COUNTRY_ID = "@country_id";
        public const string PAR_CODE = "@value_code";
        public const string PAR_VALUE = "@value";
        public const string PAR_LIMIT_ID = "@limits_id";
        public const string PAR_USER_TYPE = "@userType_id";
        public const string PAR_USER_LEVEL = "@userLevel";
        public const string PAR_BONUS_ID = "@Bonus_id";
        public const string PAR_AMORT_ID = "@amortization_id";
        public const string PAR_COUNTRY_SETNG_ID = "@сountrySettings_id";
        public const string PAR_PAYMENT_ID = "@agentPayment_id";
        public const string PAR_PAYMET_VALUE = "@typeAgentPayment";
        public const string PAR_PAYMET_STATUS = "@Status";
        public const string ParPAllHist = "@pAllHist";
        public const string ParDistrId = "@Distr_id";
        public const string Status = "@Status";
        public const string ShowAll = "@ShowAll";
        public const string RegionIdLst = "@RegionIdLst";
        public const string BonusPersent = "@bonusPersent";


        //Reports Captions
        public const string CaptionCommonSetting = "Общие настройки";
        public const string CaptionPaymentWithPartner = "Расчеты с партнером";
        public const string CaptionEmployeesBase = "База сотрудников";
        public const string CaptionCostsForecast = "Прогноз затрат";
        public const string CaptionCostsFacts = "Факт затрат";

        //Color
        public static readonly Color ColorModified = Color.FromArgb(254, 255, 132);
        public static readonly Color ColorAdded = Color.FromArgb(232, 255, 192);
        public static readonly Color ColorDiseble = Color.FromArgb(169, 169, 169);
    }
}
