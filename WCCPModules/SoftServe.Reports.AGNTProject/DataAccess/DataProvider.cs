﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using BLToolkit.Data;
using Logica.Reports.DataAccess;
using SoftServe.Reports.AGNTProject.Models;
using SoftServe.Reports.AGNTProject.Utils;

namespace SoftServe.Reports.AGNTProject.DataAccess
{
    public static class DataProvider
    {
        #region Fields
        private static DbManager _db;
        #endregion

        #region Properties
        private static DbManager Db
        {
            get
            {
                if (_db == null)
                {
                    DbManagerInit();
                }
                else if (CheckDbWasChanged())
                {
                    _db.Close();
                    DbManagerInit();
                    AccessToApp.ResetAccess();
                }
                return _db;
            }
        }
        #endregion

        #region Methods
        public static List<Period> GetPeriods()
        {
            List<Period> lData = Db.SetSpCommand(Constants.SP_GET_PERIOD).ExecuteList<Period>();
            return lData; 
        }
        public static List<Country> GetCountries(int periodId)
        {
            List<Country> lData =
                Db.SetSpCommand(Constants.SP_GET_COUNTRY, Db.Parameter(Constants.PAR_PERIOD, periodId))
                    .ExecuteList<Country>();
            return lData;
        }
        public static List<Region> GetRegions()
        {
            List<Region> lData = Db.SetSpCommand(Constants.SP_GET_REGION, Db.Parameter(Constants.PAR_PERIOD))
                .ExecuteList<Region>();
            return lData;
        }
        public static DataSet GetPeriodId(int period, int countryId)
        {
            DataSet dt =
                Db.SetSpCommand(Constants.SP_GET_PERIOD_ID, Db.Parameter(Constants.PAR_PERIOD, period),
                    Db.Parameter(Constants.PAR_COUNTRY_ID, countryId))
                    .ExecuteDataSet();
            return dt;
        }
        public static int GetUserLevelAccess()
        {
            int level = Db.SetSpCommand(Constants.SP_GET_USER_ACCESS_LEVEL).ExecuteScalar<int>();
            return level;
        }
        private static void DbManagerInit()
        {
            DbManager.DefaultConfiguration = ""; //to reset possible previous changes
            string conn = HostConfiguration.DBSqlConnection ??
            @"Server = eegdadb8; Database = SalesWorks; User Id = urmtest;Password = 3111;";
            //@"Server = eegdadb18; Database = SalesWorks; User Id = dimasik;Password = 3111;";
            //@"Server = eegdadb18; Database = SalesWorks; User Id = urmtest;Password = 3111;";
            DbManager.AddConnectionString(conn);

            _db = new DbManager();
            _db.Command.CommandTimeout = 15 * 60; // 30 minutes by default
        }

        private static bool CheckDbWasChanged()
        {
            return HostConfiguration.DBSqlConnection != null &&
                   !HostConfiguration.DBSqlConnection.Contains(_db.Connection.ConnectionString);
        }
        public static int GetPeriodStatus(int periodId)
        {
            Func<long, string> getCommand =
                l => "SELECT p.Status FROM ap.Period AS p WITH(NOLOCK) WHERE p.Period_id =  " + l.ToString();

            string command = getCommand(periodId);
            int status = Db.SetCommand(command).ExecuteScalar<int>();
            return status;
        }
        #endregion

        #region CommonSettings
        public static DataSet GetCostsLimits(int periodId)
        {
            DataSet dt =
                Db.SetSpCommand(Constants.SP_GET_COSTS_LIMITS, Db.Parameter(Constants.PAR_PERIOD_ID, periodId))
                    .ExecuteDataSet();
            return dt;
        }

        public static DataSet GetBonusAmount(int periodId)
        {
            DataSet dt =
                Db.SetSpCommand(Constants.SP_GET_BONUS_AMOUNT, Db.Parameter(Constants.PAR_PERIOD_ID, periodId))
                    .ExecuteDataSet();
            return dt;
        }
        public static DataSet GetAmortization(int periodId)
        {
            DataSet dt =
                Db.SetSpCommand(Constants.SP_GET_AMORTIZATION, Db.Parameter(Constants.PAR_PERIOD_ID, periodId))
                    .ExecuteDataSet();
            return dt;
        }
        public static DataSet GetCountrySettings(int periodId)
        {
            DataSet dt =
                Db.SetSpCommand(Constants.SP_GET_COUNTRY_SETTINGS, Db.Parameter(Constants.PAR_PERIOD_ID, periodId))
                    .ExecuteDataSet();
            return dt;
        }
        public static DataTable GetAgentPaymentTypes()
        {
            DataTable dt = Db.SetSpCommand(Constants.SP_GET_AGENT_PAYMENT_TYPES).ExecuteDataTable();
            return dt;
        }
        public static void SaveCostsLimChanges(int periodId, Dictionary<string, SimpleItemModel> data)
        {
            if (data.Count == 0)
                return;

            foreach (var item in data)
            {
                if (item.Value.State == ItemState.New)
                    Db.SetSpCommand(Constants.SP_INSERT_COSTS_LIMITS,
                        Db.Parameter(Constants.PAR_PERIOD_ID, periodId),
                        Db.Parameter(Constants.PAR_CODE, item.Value.Tag),
                        Db.Parameter(Constants.PAR_VALUE, item.Value.Value))
                        .ExecuteNonQuery();

                else if (item.Value.State == ItemState.Modified || item.Value.State == ItemState.Deactivated || item.Value.State == ItemState.Activated)
                {
                    int valueStatus = item.Value.State == ItemState.Deactivated ? 9 : 2;
                    Db.SetSpCommand(Constants.SP_UPDATE_COSTS_LIMITS,
                        Db.Parameter(Constants.PAR_LIMIT_ID, item.Value.Id),
                        Db.Parameter(Constants.PAR_VALUE,
                            item.Value.State == ItemState.Modified ? item.Value.Value : (object)DBNull.Value),
                        Db.Parameter(Constants.Status, valueStatus));

                    Db.ExecuteNonQuery();
                }
            }
        }
        public static void SaveBonusSizeChanges(int periodId, Dictionary<string, SimpleItemModel> data)
        {
            if (data.Count == 0)
                return;

            foreach (var item in data)
            {
                if (item.Value.State == ItemState.New)
                    Db.SetSpCommand(Constants.SP_INSERT_BONUS_AMOUNT,
                        Db.Parameter(Constants.PAR_PERIOD_ID, periodId),
                        Db.Parameter(Constants.PAR_USER_TYPE, item.Value.Tag2),
                        Db.Parameter(Constants.PAR_USER_LEVEL, item.Value.Tag),
                        Db.Parameter(Constants.PAR_VALUE, item.Value.Value))
                        .ExecuteNonQuery();
                else if (item.Value.State == ItemState.Modified || item.Value.State == ItemState.Deactivated || item.Value.State == ItemState.Activated)
                {
                    Db.SetSpCommand(Constants.SP_UPDATE_BONUS_AMOUNT,
                        Db.Parameter(Constants.PAR_BONUS_ID, item.Value.Id),
                        Db.Parameter(Constants.PAR_VALUE,
                            item.Value.State == ItemState.Deactivated ? (object)DBNull.Value : item.Value.Value));
                    Db.ExecuteNonQuery();
                }
            }
        }
        public static void SaveAmortizationChanges(int periodId, Dictionary<string, SimpleItemModel> data)
        {
            if (data.Count == 0)
                return;

            foreach (var item in data)
            {
                if (item.Value.State == ItemState.New)
                    Db.SetSpCommand(Constants.SP_INSERT_AMORTIZATION,
                        Db.Parameter(Constants.PAR_PERIOD_ID, periodId),
                        Db.Parameter(Constants.PAR_USER_TYPE, item.Value.Tag2),
                        Db.Parameter(Constants.PAR_USER_LEVEL, item.Value.Tag),
                        Db.Parameter(Constants.PAR_VALUE, item.Value.Value))
                        .ExecuteNonQuery();
                else if (item.Value.State == ItemState.Modified || item.Value.State == ItemState.Deactivated || item.Value.State == ItemState.Activated)
                {
                    Db.SetSpCommand(Constants.SP_UPDATE_AMORTIZATION,
                        Db.Parameter(Constants.PAR_AMORT_ID, item.Value.Id),
                        Db.Parameter(Constants.PAR_VALUE,
                            item.Value.State == ItemState.Deactivated ? (object)DBNull.Value : item.Value.Value));
                    Db.ExecuteNonQuery();
                }
            }
        }
        public static void SaveCountrySettingChanges(int periodId, Dictionary<string, SimpleItemModel> data)
        {
            if (data.Count == 0)
                return;

            foreach (var item in data)
            {
                if (item.Value.State == ItemState.Modified || item.Value.State == ItemState.Deactivated || item.Value.State == ItemState.Activated)
                {
                    string valueStr = item.Value.Value.ToString();
                    decimal value = Decimal.Parse(valueStr);
                    Db.SetSpCommand(Constants.SP_UPDATE_COUNTRY_SETTINGS,
                        Db.Parameter(Constants.PAR_COUNTRY_SETNG_ID, item.Value.Id),
                        Db.Parameter(Constants.PAR_VALUE,
                            item.Value.State == ItemState.Deactivated ? (object)DBNull.Value : value));
                    Db.ExecuteNonQuery();
                }
            }
        }
        public static void SavePaymentTypeChanges(Dictionary<string, SimpleItemModel> data)
        {
            if (data.Count == 0)
                return;

            foreach (var item in data)
            {
                if (item.Value.State == ItemState.New || item.Value.State == ItemState.Modified || item.Value.State == ItemState.Deactivated || item.Value.State == ItemState.Activated)
                {
                    var status = item.Value.State == ItemState.Deactivated ? 9 : 2; 
                    Db.SetSpCommand(Constants.SP_UPDATE_AGENT_PAYMENT_TYPES,
                        Db.Parameter(Constants.PAR_PAYMENT_ID, item.Value.State == ItemState.New ? (object)DBNull.Value : item.Value.Id),
                        Db.Parameter(Constants.PAR_PAYMET_VALUE, item.Value.Tag),
                        Db.Parameter(Constants.PAR_PAYMET_STATUS, item.Value.State == ItemState.New ? (object)DBNull.Value : status));
                    Db.ExecuteNonQuery();
                }
            }
        }
        #endregion

        #region PartnerPayment
        public static void InitBeforeSavePartnerPayment()
        {
            try
            {
                string command = "EXEC sys.sp_MSdroptemptable @tname = '#tmpPaymentWithPartner'";
                Db.SetCommand(command)
                    .ExecuteNonQuery();
                command = "SELECT TOP 0 pwp.distr_id" +
                          ", pwp.Region_id" +
                          ", pwp.agentPayment_id" +
                          ", pwp.Comment" +
                          ", pwp.date_from" +
                          ", pwp.date_to" +
                          ", pwp.amount" +
                          ", pwp.Status INTO #tmpPaymentWithPartner FROM AP.PaymentWithPartner AS pwp ";
                Db.SetCommand(command)
                    .ExecuteNonQuery();
                command = "ALTER TABLE #tmpPaymentWithPartner ADD paymentPartner_id BIGINT NULL";
                Db.SetCommand(command)
                    .ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        public static void SavePartnerPaymentTmpTable(DataRow row)
        {
            string dateFromStr;
            string dateToStr;

            try
            {
                //dateFrom - не может быть пустой
                DateTime dateFrom = Convert.ToDateTime(row["date_from"].ToString());
                dateFromStr = "'" + dateFrom.ToString("yyyyMMdd") + "'";
                //dateTo - может быть пустой
                DateTime dateTo = string.IsNullOrEmpty(row["date_to"].ToString())
                    ? DateTime.MinValue
                    : Convert.ToDateTime(row["date_to"].ToString());
                if (dateTo == DateTime.MinValue)
                {
                    dateToStr = "NULL";
                }
                else
                {
                    dateToStr = "'" + dateTo.ToString("yyyyMMdd") + "'";
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return;
            }

            try
            {
                string command = "INSERT #tmpPaymentWithPartner" +
                                 "(paymentPartner_id" +
                                 ", distr_id" +
                                 ", Region_id" +
                                 ", agentPayment_id" +
                                 ", Comment" +
                                 ", date_from" +
                                 ", date_to" +
                                 ", amount" +
                                 ", Status" +
                                 ") VALUES(" +
                                 (string.IsNullOrEmpty(row["paymentPartner_id"].ToString())
                                     ? "NULL"
                                     : row["paymentPartner_id"].ToString()) +
                                 ", " + row["distr_id"] + ", " + row["Region_id"] + ", " + row["agentPayment_id"] +
                                 ", '" + row["Comment"] + "', " + dateFromStr + ", " + dateToStr + ", " + row["amount"] +
                                 ", 2)";
                Db.SetCommand(command).ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        public static void SavePartnerPayment()
        {
            try
            {
                string command = "EXEC AP.EDIT_PaymentWithPartner";
                Db.SetCommand(command).ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        public static DataTable GetPartnerPayments(int allHist, int periodId, string regionIdLst)
        {
            DataTable dt =
                Db.SetSpCommand(Constants.SpGetPartnerPayments, Db.Parameter(Constants.ParPAllHist, allHist),
                    Db.Parameter(Constants.PAR_PERIOD_ID, periodId),
                    Db.Parameter(Constants.RegionIdLst, regionIdLst))
                    .ExecuteDataTable();
            return dt;
        }
        public static DateTime LastOpenPeriod(int periodId)
        {
            //possible DataException
            DateTime lastOpenPeriod =
                Db.SetSpCommand(Constants.GetLastOpenPeriod, Db.Parameter(Constants.PAR_PERIOD_ID, periodId))
                    .ExecuteScalar<DateTime>();
            return lastOpenPeriod;
        }
        public static DataTable GetDistrib(int? periodId, int? showAll, string regionIdLst)
        {
            DataTable dt =
                Db.SetSpCommand(Constants.SpGetDistrib, Db.Parameter(Constants.PAR_PERIOD_ID, periodId),
                    Db.Parameter(Constants.ShowAll, showAll), Db.Parameter(Constants.RegionIdLst, regionIdLst))
                    .ExecuteDataTable();
            return dt;
        }
        public static DataTable GetAgentPaymentTypesDistrib(int? showAll)
        {
            DataTable dt =
                Db.SetSpCommand(Constants.SpGetAgentPaymentTypesDistrib, Db.Parameter(Constants.ShowAll, showAll))
                    .ExecuteDataTable();
            return dt;
        }
        public static DataTable GetRegionCountry(int countryId)
        {
            DataTable dt =
                Db.SetSpCommand(Constants.SpGetRegionCountry, Db.Parameter(Constants.PAR_COUNTRY_ID, countryId))
                    .ExecuteDataTable();
            return dt;
        }
        public static DataTable GetRegionDistrib(int? distrId, int? showAll)
        {
            DataTable dt =
                Db.SetSpCommand(Constants.SpGetRegionDistrib, Db.Parameter(Constants.ParDistrId, distrId),
                    Db.Parameter(Constants.ShowAll, showAll))
                    .ExecuteDataTable();
            return dt;
        }
        #endregion

        #region EmployeesBase

        public static DataTable GetEmployeesBase(int periodId, string regionIdLst)
        {
            DataTable dt = null;
            try
            {
                dt =
                    Db.SetSpCommand(Constants.SpGetEmployeesBase, Db.Parameter(Constants.PAR_PERIOD_ID, periodId),
                        Db.Parameter(Constants.RegionIdLst, regionIdLst))
                        .ExecuteDataTable();
                DataColumn[] keys = new DataColumn[2];
                keys[0] = dt.Columns["Sector_id"];
                keys[1] = dt.Columns["Period_id"];
                dt.PrimaryKey = keys;
            }
            catch (Exception ex)
            {
                MessageBox.Show(@"Ошибка: - " + ex.Message + @"; periodId - " + periodId + @"; regionIdLst - " +
                                regionIdLst);
            }
            return dt;
        }

        public static DataTable GetLimitsOfSpendsByCode(int periodId, string valueCode, int? showAll)
        {
            DataTable dt =
                Db.SetSpCommand(Constants.SpGetLimitsOfSpendsByCode, Db.Parameter(Constants.PAR_PERIOD_ID, periodId),
                    Db.Parameter(Constants.PAR_CODE, valueCode), Db.Parameter(Constants.ShowAll, showAll))
                    .ExecuteDataTable();
            return dt;
        }

        public static void SaveEmployeesBaseTmpTable(DataRow row)
        {
            Func<long, string> getCommand = l => "SELECT value " +
                                                 "FROM AP.CS_LimitsOfSpends WITH(NOLOCK)" +
                                                 "WHERE limits_id = " + l.ToString();

            string activeFromStr;
            try
            {
                //ActiveFrom - МОЖЕТ быть пустой
                DateTime activeFrom = string.IsNullOrEmpty(row["ActiveFrom"].ToString())
                    ? DateTime.MinValue
                    : Convert.ToDateTime(row["ActiveFrom"].ToString());
                if (activeFrom == DateTime.MinValue)
                {
                    activeFromStr = "NULL";
                }
                else
                {
                    activeFromStr = "'" + activeFrom.ToString("yyyyMMdd") + "'";
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return;
            }

            string publicTrCompensationVal;
            string connectionCompensationVal;
            string connectionTabletCompensationVal;
            string foodCompensationVal;
            string stationeryCompensationVal;
            try
            {
                var publicTrCompensationId = row["PublicTrCompensation_LimitID"] != DBNull.Value
                    ? (long)row["PublicTrCompensation_LimitID"]
                    : 0;

                var connectionCompensationId = row["ConnectionCompensation_LimitID"] != DBNull.Value
                    ? (long)row["ConnectionCompensation_LimitID"]
                    : 0;

                var connectionTabletCompensationId = row["ConnectionTabletCompensation_LimitID"] != DBNull.Value
                    ? (long)row["ConnectionTabletCompensation_LimitID"]
                    : 0;

                var foodCompensationId = row["FoodCompensation_LimitID"] != DBNull.Value
                    ? (long)row["FoodCompensation_LimitID"]
                    : 0;

                var stationeryCompensationId = row["StationeryCompensation_LimitID"] != DBNull.Value
                    ? (long)row["StationeryCompensation_LimitID"]
                    : 0;


                string command;
                if (publicTrCompensationId != 0)
                {
                    command = getCommand(publicTrCompensationId);
                    publicTrCompensationVal = Db.SetCommand(command).ExecuteScalar<long>().ToString();
                }
                else
                {
                    publicTrCompensationVal = "NULL";
                }

                if (connectionCompensationId != 0)
                {
                    command = getCommand(connectionCompensationId);
                    connectionCompensationVal = Db.SetCommand(command).ExecuteScalar<long>().ToString();
                }
                else
                {
                    connectionCompensationVal = "NULL";
                }

                if (connectionTabletCompensationId != 0)
                {
                    command = getCommand(connectionTabletCompensationId);
                    connectionTabletCompensationVal = Db.SetCommand(command).ExecuteScalar<long>().ToString();
                }
                else
                {
                    connectionTabletCompensationVal = "NULL";
                }

                if (foodCompensationId != 0)
                {
                    command = getCommand(foodCompensationId);
                    foodCompensationVal = Db.SetCommand(command).ExecuteScalar<long>().ToString();
                }
                else
                {
                    foodCompensationVal = "NULL";
                }

                if (stationeryCompensationId != 0)
                {
                    command = getCommand(stationeryCompensationId);
                    stationeryCompensationVal = Db.SetCommand(command).ExecuteScalar<long>().ToString();
                }
                else
                {
                    stationeryCompensationVal = "NULL";
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return;
            }

            var statusIdStr = row["Status"].ToString();
            string statusValStr = "";
            StatusSectorList status = new StatusSectorList();
            foreach (StatusSector statusVal in status.StatusSector.Where(sector => sector.StatusSectorId.ToString() == statusIdStr))
            {
                statusValStr = statusVal.StatusSectorName;
                break;
            }

            var workSchemaIdStr = row["WorkSchema"].ToString();
            string workSchemaValStr = "";
            WorkSchemaList workSchema = new WorkSchemaList();
            foreach (WorkSchema schemaVal in workSchema.WorkSchemata.Where(schema => schema.WorkSchemaId.ToString() == workSchemaIdStr))
            {
                workSchemaValStr = schemaVal.WorkSchemaName;
                break;
            }

            try
            {
                string command = "INSERT #UPDEmpBaseIn" +
                                 "(Sector_id" +
                                 ", Period_id" +
                                 ", ActiveFrom" +
                                 ", WorkSchema" +
                                 ", Salary1" +
                                 ", ConstPart_sum" +
                                 ", Amortization_norm" +
                                 ", MileagePerMonth_fact" +
                                 ", FuelConsumption_norm" +
                                 ", Fuel_price" +
                                 ", PublicTrCompensation_val" +
                                 ", ConnectionCompensation_val" +
                                 ", ConnectionTabletCompensation_val" +
                                 ", FoodCompensation_val" +
                                 ", StationeryCompensation_val" +
                                 ", WorkDays_total" +
                                 ", WorkedDays_num" +
                                 ", AddCompensation" +
                                 ", VariablePartClosingPerc_fact" +
                                 ", CorrectVarPartPrevP_sum" +
                                 ", Comment" +
                                 ", Status" +
                                 ")VALUES(" + row["Sector_id"] + ", " + row["Period_id"] + ", " + activeFromStr + ", '" +
                                 workSchemaValStr + "', " + row["Salary1"] + ", " + row["ConstPart_sum"] + ", " +
                                 row["Amortization_norm"].ToString().Replace(",", ".") + ", " +
                                 row["MileagePerMonth_fact"] + ", " +
                                 row["FuelConsumption_norm"].ToString().Replace(",", ".") + ", " + row["Fuel_price"].ToString().Replace(",", ".")
                                 + ", " + publicTrCompensationVal + ", " + connectionCompensationVal + ", " + connectionTabletCompensationVal + ", " +
                                 foodCompensationVal + ", " + stationeryCompensationVal + ", " + row["WorkDays_total"] +
                                 ", " +
                                 row["WorkedDays_num"] + ", " + row["AddCompensation"] + ", " +
                                 row["VariablePartClosingPerc_fact"] + ", " +
                                 row["CorrectVarPartPrevP_sum"] + ", '" + row["Comment"] + "', '" + statusValStr + "')";
                Db.SetCommand(command).ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public static DataSet SaveEmployeesBase()
        {
            try
            {
                string command = "EXEC AP.UPD_EmployeesBase";
                DataSet emplUpd = Db.SetCommand(command).ExecuteDataSet();
                return emplUpd;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            return null;
        }

        public static void InitBeforeSaveEmployeesBase()
        {
            try
            {
                string command = "EXEC sys.sp_MSdroptemptable @tname = '#UPDEmpBaseIn'";
                Db.SetCommand(command)
                    .ExecuteNonQuery();
                command = " CREATE TABLE #UPDEmpBaseIn(" +
                          "Sector_id INT NOT NULL" +
                          ", Period_id INT NOT NULL" +
                          ", ActiveFrom VARCHAR(500) NULL" +
                          ", WorkSchema VARCHAR(500) NULL" +
                          ", Salary1 VARCHAR(500) NULL" +
                          ", ConstPart_sum VARCHAR(500) NULL" +
                          ", Amortization_norm VARCHAR(500) NULL" +
                          ", MileagePerMonth_fact VARCHAR(500) NULL" +
                          ", FuelConsumption_norm VARCHAR(500) NULL" +
                          ", Fuel_price VARCHAR(500) NULL" +
                          ", PublicTrCompensation_val VARCHAR(500) NULL" +
                          ", ConnectionCompensation_val VARCHAR(500) NULL" +
                          ", ConnectionTabletCompensation_val VARCHAR(500) NULL" +
                          ", FoodCompensation_val VARCHAR(500) NULL" +
                          ", StationeryCompensation_val VARCHAR(500) NULL" +
                          ", WorkDays_total VARCHAR(500) NULL" +
                          ", WorkedDays_num VARCHAR(500) NULL" +
                          ", AddCompensation VARCHAR(500) NULL" +
                          ", VariablePartClosingPerc_fact VARCHAR(500) NULL" +
                          ", CorrectVarPartPrevP_sum VARCHAR(500) NULL" +
                          ", Comment VARCHAR(500) NULL" +
                          ", Status VARCHAR(500) NULL" +
                          ", isNotValid BIT NOT NULL DEFAULT(0))";
                Db.SetCommand(command)
                    .ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public static void RecalcEmployeesBase(int periodId)
        {
            try
            {
                string command = "EXEC AP.ReloadEmployeesBase " +
                                 "@Period_id = " + periodId +
                                 ", @isFullRecalc = 0" +
                                 ", @debug = NULL ";

                Db.SetCommand(command)
                    .ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public static long GetIdLimitsFromValue(int periodId, string valueCode, int value)
        {
            string command = "SELECT clos.limits_id " +
                             "FROM AP.CS_LimitsOfSpends AS clos WITH (NOLOCK) " +
                             "WHERE clos.period_id = " + periodId +
                             "AND clos.value_code = '" + valueCode + "' " +
                             "AND clos.value = " + value + "" +
                             "AND clos.Status = 2";

            long status = Db.SetCommand(command).ExecuteScalar<long>();
            return status;
        }
        #endregion

        #region CostForecast

        public static DataTable GetCostForecast(int periodId, decimal bonusPersent, string regionIdLst)
        {
            DataTable dt =
                Db.SetSpCommand(Constants.SpGetCostForecast, Db.Parameter(Constants.PAR_PERIOD_ID, periodId),
                    Db.Parameter(Constants.BonusPersent, bonusPersent), Db.Parameter(Constants.RegionIdLst, regionIdLst))
                    .ExecuteDataTable();

            return dt;
        }

        #endregion

        #region CostFact

        public static DataTable GetCostFact(int periodId, string regionIdLst)
        {
            DataTable dt =
                Db.SetSpCommand(Constants.SpGetCostFact, Db.Parameter(Constants.PAR_PERIOD_ID, periodId),
                    Db.Parameter(Constants.RegionIdLst, regionIdLst))
                    .ExecuteDataTable();

            return dt;
        }

        #endregion

        public static void EditPeriodStatus(int periodId, string status)
        {
            Db.SetSpCommand(Constants.SpEditPeriodStatus, Db.Parameter(Constants.PAR_PERIOD_ID, periodId),
                Db.Parameter(Constants.Status, status))
                .ExecuteNonQuery();
        }

    }
}