﻿using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows.Forms;
using DevExpress.Utils;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraEditors.Drawing;
using DevExpress.XtraEditors.Mask;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraEditors.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;

namespace SoftServe.Reports.AGNTProject
{
    partial class SettingsForm : XtraForm
    {
        private LabelControl lblCountry;
        private GroupControl groupControl1;
        private LabelControl lblRegion;
        private ComboBoxEdit cbxCountry;
        private SimpleButton btnGenerate;
        private SimpleButton Отмена;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColReport;
        private DevExpress.XtraGrid.Columns.GridColumn gridColCheckRep;
        private DevExpress.XtraGrid.Columns.GridColumn gridColModeName;
        private DevExpress.XtraGrid.Columns.GridColumn gridColModeValue;
        private DevExpress.XtraGrid.GridControl gridCntrSettings;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private LabelControl lblPeriod;

        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SettingsForm));
            this.lblCountry = new DevExpress.XtraEditors.LabelControl();
            this.lblPeriod = new DevExpress.XtraEditors.LabelControl();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.cbxRegion = new DevExpress.XtraEditors.CheckedComboBoxEdit();
            this.cbxPeriod = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cbxCountry = new DevExpress.XtraEditors.ComboBoxEdit();
            this.lblRegion = new DevExpress.XtraEditors.LabelControl();
            this.btnGenerate = new DevExpress.XtraEditors.SimpleButton();
            this.Отмена = new DevExpress.XtraEditors.SimpleButton();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColReport = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColCheckRep = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColModeName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColModeValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.rTxEdit = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColReportId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColIsAllowed = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridCntrSettings = new DevExpress.XtraGrid.GridControl();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.rLkpMode = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.groupControl3 = new DevExpress.XtraEditors.GroupControl();
            this.checkTotals = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbxRegion.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbxPeriod.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbxCountry.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rTxEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridCntrSettings)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rLkpMode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).BeginInit();
            this.groupControl3.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblCountry
            // 
            this.lblCountry.Location = new System.Drawing.Point(8, 40);
            this.lblCountry.Name = "lblCountry";
            this.lblCountry.Size = new System.Drawing.Size(41, 13);
            this.lblCountry.TabIndex = 0;
            this.lblCountry.Text = "Страна:";
            // 
            // lblPeriod
            // 
            this.lblPeriod.Location = new System.Drawing.Point(8, 13);
            this.lblPeriod.Name = "lblPeriod";
            this.lblPeriod.Size = new System.Drawing.Size(42, 13);
            this.lblPeriod.TabIndex = 1;
            this.lblPeriod.Text = "Период:";
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.cbxRegion);
            this.groupControl1.Controls.Add(this.cbxPeriod);
            this.groupControl1.Controls.Add(this.cbxCountry);
            this.groupControl1.Controls.Add(this.lblRegion);
            this.groupControl1.Controls.Add(this.lblPeriod);
            this.groupControl1.Controls.Add(this.lblCountry);
            this.groupControl1.Location = new System.Drawing.Point(7, 7);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.ShowCaption = false;
            this.groupControl1.Size = new System.Drawing.Size(293, 92);
            this.groupControl1.TabIndex = 2;
            // 
            // cbxRegion
            // 
            this.cbxRegion.Location = new System.Drawing.Point(56, 61);
            this.cbxRegion.Name = "cbxRegion";
            this.cbxRegion.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbxRegion.Properties.PopupSizeable = false;
            this.cbxRegion.Size = new System.Drawing.Size(232, 20);
            this.cbxRegion.TabIndex = 3;
            // 
            // cbxPeriod
            // 
            this.cbxPeriod.Location = new System.Drawing.Point(56, 8);
            this.cbxPeriod.Name = "cbxPeriod";
            this.cbxPeriod.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbxPeriod.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbxPeriod.Size = new System.Drawing.Size(232, 20);
            this.cbxPeriod.TabIndex = 1;
            this.cbxPeriod.SelectedIndexChanged += new System.EventHandler(this.cbxPeriod_SelectedIndexChanged);
            // 
            // cbxCountry
            // 
            this.cbxCountry.Location = new System.Drawing.Point(56, 35);
            this.cbxCountry.Name = "cbxCountry";
            this.cbxCountry.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbxCountry.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbxCountry.Size = new System.Drawing.Size(232, 20);
            this.cbxCountry.TabIndex = 2;
            this.cbxCountry.SelectedIndexChanged += new System.EventHandler(this.cbxCountry_SelectedIndexChanged);
            // 
            // lblRegion
            // 
            this.lblRegion.Location = new System.Drawing.Point(8, 66);
            this.lblRegion.Name = "lblRegion";
            this.lblRegion.Size = new System.Drawing.Size(39, 13);
            this.lblRegion.TabIndex = 2;
            this.lblRegion.Text = "Регион:";
            // 
            // btnGenerate
            // 
            this.btnGenerate.Image = ((System.Drawing.Image)(resources.GetObject("btnGenerate.Image")));
            this.btnGenerate.Location = new System.Drawing.Point(231, 266);
            this.btnGenerate.Name = "btnGenerate";
            this.btnGenerate.Size = new System.Drawing.Size(116, 23);
            this.btnGenerate.TabIndex = 6;
            this.btnGenerate.Text = "Генерировать";
            this.btnGenerate.Click += new System.EventHandler(this.btnGenerate_Click);
            // 
            // Отмена
            // 
            this.Отмена.Image = ((System.Drawing.Image)(resources.GetObject("Отмена.Image")));
            this.Отмена.Location = new System.Drawing.Point(357, 266);
            this.Отмена.Name = "Отмена";
            this.Отмена.Size = new System.Drawing.Size(77, 23);
            this.Отмена.TabIndex = 7;
            this.Отмена.Text = "Отмена";
            this.Отмена.Click += new System.EventHandler(this.Отмена_Click);
            // 
            // gridView1
            // 
            this.gridView1.Appearance.HeaderPanel.BackColor = System.Drawing.Color.AliceBlue;
            this.gridView1.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.gridView1.ColumnPanelRowHeight = 25;
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColReport,
            this.gridColCheckRep,
            this.gridColModeName,
            this.gridColModeValue,
            this.gridColReportId,
            this.gridColIsAllowed});
            this.gridView1.GridControl = this.gridCntrSettings;
            this.gridView1.HorzScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Never;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsMenu.EnableColumnMenu = false;
            this.gridView1.OptionsSelection.EnableAppearanceFocusedRow = false;
            this.gridView1.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CellSelect;
            this.gridView1.OptionsView.ColumnHeaderAutoHeight = DevExpress.Utils.DefaultBoolean.True;
            this.gridView1.OptionsView.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.OptionsView.ShowIndicator = false;
            this.gridView1.ScrollStyle = DevExpress.XtraGrid.Views.Grid.ScrollStyleFlags.None;
            this.gridView1.VertScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Never;
            this.gridView1.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView1_CustomDrawCell);
            this.gridView1.RowCellStyle += new DevExpress.XtraGrid.Views.Grid.RowCellStyleEventHandler(this.gridView1_RowCellStyle);
            this.gridView1.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gridView1_CustomRowCellEdit);
            this.gridView1.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.gridView1_ShowingEditor);
            this.gridView1.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.gridView1_CellValueChanged);
            this.gridView1.CellValueChanging += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.gridView1_CellValueChanging);
            this.gridView1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.gridView1_MouseDown);            
            // 
            // gridColReport
            // 
            this.gridColReport.Caption = "Отчет";
            this.gridColReport.FieldName = "ReportName";
            this.gridColReport.MaxWidth = 120;
            this.gridColReport.MinWidth = 120;
            this.gridColReport.Name = "gridColReport";
            this.gridColReport.OptionsColumn.AllowEdit = false;
            this.gridColReport.OptionsColumn.AllowFocus = false;
            this.gridColReport.OptionsColumn.AllowMove = false;
            this.gridColReport.OptionsColumn.AllowSize = false;
            this.gridColReport.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.gridColReport.OptionsColumn.FixedWidth = true;
            this.gridColReport.OptionsColumn.ReadOnly = true;
            this.gridColReport.OptionsFilter.AllowAutoFilter = false;
            this.gridColReport.OptionsFilter.AllowFilter = false;
            this.gridColReport.Visible = true;
            this.gridColReport.VisibleIndex = 0;
            this.gridColReport.Width = 120;
            // 
            // gridColCheckRep
            // 
            this.gridColCheckRep.FieldName = "Generate";
            this.gridColCheckRep.MaxWidth = 80;
            this.gridColCheckRep.MinWidth = 80;
            this.gridColCheckRep.Name = "gridColCheckRep";
            this.gridColCheckRep.OptionsColumn.AllowMove = false;
            this.gridColCheckRep.OptionsColumn.AllowSize = false;
            this.gridColCheckRep.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.gridColCheckRep.OptionsColumn.ShowCaption = false;
            this.gridColCheckRep.OptionsFilter.AllowAutoFilter = false;
            this.gridColCheckRep.OptionsFilter.AllowFilter = false;
            this.gridColCheckRep.Visible = true;
            this.gridColCheckRep.VisibleIndex = 1;
            this.gridColCheckRep.Width = 80;
            // 
            // gridColModeName
            // 
            this.gridColModeName.FieldName = "ModeName";
            this.gridColModeName.MaxWidth = 115;
            this.gridColModeName.MinWidth = 115;
            this.gridColModeName.Name = "gridColModeName";
            this.gridColModeName.OptionsColumn.AllowEdit = false;
            this.gridColModeName.OptionsColumn.AllowFocus = false;
            this.gridColModeName.OptionsColumn.AllowMove = false;
            this.gridColModeName.OptionsColumn.AllowSize = false;
            this.gridColModeName.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.gridColModeName.OptionsColumn.ReadOnly = true;
            this.gridColModeName.OptionsColumn.ShowCaption = false;
            this.gridColModeName.OptionsFilter.AllowAutoFilter = false;
            this.gridColModeName.OptionsFilter.AllowFilter = false;
            this.gridColModeName.Visible = true;
            this.gridColModeName.VisibleIndex = 2;
            this.gridColModeName.Width = 115;
            // 
            // gridColModeValue
            // 
            this.gridColModeValue.ColumnEdit = this.rTxEdit;
            this.gridColModeValue.FieldName = "ModeValue";
            this.gridColModeValue.MaxWidth = 107;
            this.gridColModeValue.MinWidth = 107;
            this.gridColModeValue.Name = "gridColModeValue";
            this.gridColModeValue.OptionsColumn.AllowMove = false;
            this.gridColModeValue.OptionsColumn.AllowSize = false;
            this.gridColModeValue.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.gridColModeValue.OptionsColumn.ShowCaption = false;
            this.gridColModeValue.OptionsFilter.AllowAutoFilter = false;
            this.gridColModeValue.OptionsFilter.AllowFilter = false;
            this.gridColModeValue.Visible = true;
            this.gridColModeValue.VisibleIndex = 3;
            this.gridColModeValue.Width = 107;
            // 
            // rTxEdit
            // 
            this.rTxEdit.AutoHeight = false;
            this.rTxEdit.Mask.EditMask = "P";
            this.rTxEdit.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.rTxEdit.Name = "rTxEdit";
            // 
            // gridColReportId
            // 
            this.gridColReportId.Caption = "ID";
            this.gridColReportId.FieldName = "ReportId";
            this.gridColReportId.Name = "gridColReportId";
            // 
            // gridColIsAllowed
            // 
            this.gridColIsAllowed.FieldName = "IsAllowed";
            this.gridColIsAllowed.Name = "gridColIsAllowed";
            // 
            // gridCntrSettings
            // 
            this.gridCntrSettings.Cursor = System.Windows.Forms.Cursors.Default;
            this.gridCntrSettings.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridCntrSettings.Location = new System.Drawing.Point(2, 21);
            this.gridCntrSettings.MainView = this.gridView1;
            this.gridCntrSettings.Name = "gridCntrSettings";
            this.gridCntrSettings.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit1,
            this.rLkpMode,
            this.rTxEdit});
            this.gridCntrSettings.Size = new System.Drawing.Size(426, 122);
            this.gridCntrSettings.TabIndex = 6;
            this.gridCntrSettings.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            // 
            // rLkpMode
            // 
            this.rLkpMode.AutoHeight = false;
            this.rLkpMode.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.rLkpMode.Name = "rLkpMode";
            // 
            // groupControl3
            // 
            this.groupControl3.Controls.Add(this.gridCntrSettings);
            this.groupControl3.Location = new System.Drawing.Point(7, 105);
            this.groupControl3.Name = "groupControl3";
            this.groupControl3.Size = new System.Drawing.Size(430, 145);
            this.groupControl3.TabIndex = 5;
            // 
            // checkTotals
            // 
            this.checkTotals.AutoSize = true;
            this.checkTotals.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.checkTotals.Location = new System.Drawing.Point(307, 82);
            this.checkTotals.Name = "checkTotals";
            this.checkTotals.Size = new System.Drawing.Size(126, 17);
            this.checkTotals.TabIndex = 4;
            this.checkTotals.Text = "Отображать итоги:";
            this.checkTotals.UseVisualStyleBackColor = true;
            // 
            // SettingsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(444, 301);
            this.Controls.Add(this.checkTotals);
            this.Controls.Add(this.groupControl3);
            this.Controls.Add(this.Отмена);
            this.Controls.Add(this.btnGenerate);
            this.Controls.Add(this.groupControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(460, 339);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(450, 326);
            this.Name = "SettingsForm";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Агентский проект";
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbxRegion.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbxPeriod.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbxCountry.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rTxEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridCntrSettings)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rLkpMode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).EndInit();
            this.groupControl3.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private GroupControl groupControl3;
        private ComboBoxEdit cbxPeriod;
        private System.Windows.Forms.CheckBox checkTotals;
        private CheckedComboBoxEdit cbxRegion;
        private RepositoryItemCheckEdit checkAllEdit;
        private GridColumn gridColReportId;
        private RepositoryItemLookUpEdit rLkpMode;
        private RepositoryItemTextEdit rTxEdit;
        private GridColumn gridColIsAllowed;
    }
}