﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SoftServe.Reports.AGNTProject
{
    public partial class ShowErrorForm : Form
    {
        public string ErrorList { get; set; }

        public ShowErrorForm()
        {
            InitializeComponent();
        }

        private void OKBtn_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void ShowErrorForm_Shown(object sender, EventArgs e)
        {
            memoEdit1.Text = ErrorList;
        }
    }
}
