﻿using SoftServe.Reports.AGNTProject.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SoftServe.Reports.AGNTProject.Models
{
    public class SimpleItemModel
    {
        public string Id { get; set; }
        public int? Status { get; set; }
        public decimal? Value { get; set; }
        public string Tag { get; set; }
        public string Tag2 { get; set; }
        public ItemState State { get; set; }
    }
}
