﻿using BLToolkit.Mapping;
using SoftServe.Reports.AGNTProject.Utils;
using System.Collections.Generic;
using System.ComponentModel;

namespace SoftServe.Reports.AGNTProject.Models
{
    public class LimitsOfSpendsModel : ISourceModel
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private bool _isNotifyOn = false;
        private LimitsOfSpendsModel _originalModel;

        private string _amortID;
        private int? _amortStatus;
        private decimal? _amortValue;

        private string _compensFoodId;
        private int? _compensFoodStatus;
        private decimal? _compensFoodValue;

        private string _compensLinkId;
        private int? _compensLinkStatus;
        private decimal? _compensLinkValue;

        private string _compensLinkPadId;
        private int? _compensLinkPadStatus;
        private decimal? _compensLinkPadValue;

        private string _compensOfficeId;
        private int? _compensOfficeStatus;
        private decimal? _compensOfficeValue;

        private string _compensTranspId;
        private int? _compensTranspStatus;
        private decimal? _compensTranspValue;

        [MapField("AMORT_ID")]
        public string AMORT_ID { get { return _amortID; } set { _amortID = value; } }
        [MapField("AMORT_STATUS")]
        public int? AMORT_STATUS { get { return _amortStatus; } set { SetField(ref _amortStatus, value, "AMORT_STATUS"); } }
        [MapField("AMORT")]
        public decimal? AMORT_VALUE { get { return _amortValue; } set { SetField(ref _amortValue, value, "AMORT_VALUE"); } }
        ///
        [MapField("COMPESFOOD_ID")]
        public string COMPESFOOD_ID { get { return _compensFoodId; } set { _compensFoodId = value; } }
        [MapField("COMPESFOOD_STATUS")]
        public int? COMPESFOOD_STATUS { get { return _compensFoodStatus; } set { SetField(ref _compensFoodStatus, value, "COMPESFOOD_STATUS"); } }
        [MapField("COMPESFOOD")]
        public decimal? COMPESFOOD_VALUE { get { return _compensFoodValue; } set { SetField(ref _compensFoodValue, value, "COMPESFOOD_VALUE"); } }
        //
        [MapField("COMPESLINK_ID")]
        public string COMPESLINK_ID { get { return _compensLinkId; } set { _compensLinkId = value; } }
        [MapField("COMPESLINK_STATUS")]
        public int? COMPESLINK_STATUS { get { return _compensLinkStatus; } set { SetField(ref _compensLinkStatus, value, "COMPESLINK_STATUS"); } }
        [MapField("COMPESLINK")]
        public decimal? COMPESLINK_VALUE { get { return _compensLinkValue; } set { SetField(ref _compensLinkValue, value, "COMPESLINK_VALUE"); } }
        //
        [MapField("COMPESLINKPAD_ID")]
        public string COMPESLINKPAD_ID { get { return _compensLinkPadId; } set { _compensLinkPadId = value; } }
        [MapField("COMPESLINKPAD_STATUS")]
        public int? COMPESLINKPAD_STATUS { get { return _compensLinkPadStatus; } set { SetField(ref _compensLinkPadStatus, value, "COMPESLINKPAD_STATUS"); } }
        [MapField("COMPESLINKPAD")]
        public decimal? COMPESLINKPAD_VALUE { get { return _compensLinkPadValue; } set { SetField(ref _compensLinkPadValue, value, "COMPESLINKPAD_VALUE"); } }
        //
        [MapField("COMPESOFFICE_ID")]
        public string COMPESOFFICE_ID { get { return _compensOfficeId; } set { _compensOfficeId = value; } }
        [MapField("COMPESOFFICE_STATUS")]
        public int? COMPESOFFICE_STATUS { get { return _compensOfficeStatus; } set { SetField(ref _compensOfficeStatus, value, "COMPESOFFICE_STATUS"); } }
        [MapField("COMPESOFFICE")]
        public decimal? COMPESOFFICE_VALUE { get { return _compensOfficeValue; } set { SetField(ref _compensOfficeValue, value, "COMPESOFFICE_VALUE"); } }
        //
        [MapField("COMPESTRANSP_ID")]
        public string COMPESTRANSP_ID { get { return _compensTranspId; } set { _compensTranspId = value; } }
        [MapField("COMPESTRANSP_STATUS")]
        public int? COMPESTRANSP_STATUS { get { return _compensTranspStatus; } set { SetField(ref _compensTranspStatus, value, "COMPESTRANSP_STATUS"); } }
        [MapField("COMPESTRANSP")]
        public decimal? COMPESTRANSP_VALUE { get { return _compensTranspValue; } set { SetField(ref _compensTranspValue, value, "COMPESTRANSP_VALUE"); } }


        public LimitsOfSpendsModel OriginalModel
        {
            get { return _originalModel; }
        }
        private void MakeCopy()
        {
            _originalModel = new LimitsOfSpendsModel()
            {
                AMORT_ID = this.AMORT_ID,
                AMORT_STATUS = this.AMORT_STATUS,
                AMORT_VALUE = this.AMORT_VALUE,
                COMPESFOOD_ID = this.COMPESFOOD_ID,
                COMPESFOOD_STATUS = this.COMPESFOOD_STATUS,
                COMPESFOOD_VALUE = this.COMPESFOOD_VALUE,
                COMPESLINK_ID = this.COMPESLINK_ID,
                COMPESLINK_STATUS = this.COMPESLINK_STATUS,
                COMPESLINK_VALUE = this.COMPESLINK_VALUE,
                COMPESLINKPAD_ID = this.COMPESLINKPAD_ID,
                COMPESLINKPAD_STATUS = this.COMPESLINKPAD_STATUS,
                COMPESLINKPAD_VALUE = this.COMPESLINKPAD_VALUE,
                COMPESOFFICE_ID = this.COMPESOFFICE_ID,
                COMPESOFFICE_STATUS = this.COMPESOFFICE_STATUS,
                COMPESOFFICE_VALUE = this.COMPESOFFICE_VALUE,
                COMPESTRANSP_ID = this.COMPESTRANSP_ID,
                COMPESTRANSP_STATUS = this.COMPESTRANSP_STATUS,
                COMPESTRANSP_VALUE = this.COMPESTRANSP_VALUE
            };
        }

        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
        private bool SetField<T>(ref T field, T value, string propertyName)
        {
            if (EqualityComparer<T>.Default.Equals(field, value))
                return false;
            field = value;
            if (_isNotifyOn)
                OnPropertyChanged(propertyName);
            return true;
        }
        public void OnSourceNotification()
        {
            MakeCopy();
            _isNotifyOn = true;
        }
        public int? GetStatus(string valueFieldName)
        {
            int st = 0;
            string core = valueFieldName.Replace("_VALUE", string.Empty);
            var val = this.GetType().GetProperty(core + "_STATUS").GetValue(this, null);

            if (val != null && int.TryParse(val.ToString(), out st))
            {
                return st;
            }
            return null;
        }
        public void SetStatus(string valueFieldName, int status)
        {
            string core = valueFieldName.Replace("_VALUE", string.Empty);
            this.GetType().GetProperty(core + "_STATUS").SetValue(this, status, null);           
        }
        public string GetId(string valueFieldName)
        {            
            string core = valueFieldName.Replace("_VALUE", string.Empty);
            var val = this.GetType().GetProperty(core + "_ID").GetValue(this, null);
            if (val != null)
            {
                return  val.ToString();
            }
            return null;
        }
        public void SetId(string valueFieldName, string id)
        {
            string core = valueFieldName.Replace("_VALUE", string.Empty);
            this.GetType().GetProperty(core + "_ID").SetValue(this, id, null);
        }
        public decimal? GetValue(string valueFieldName)
        {
            decimal v = 0;            
            var val = this.GetType().GetProperty(valueFieldName).GetValue(this, null);
            if (val != null && decimal.TryParse(val.ToString(), out v))
            {
                return v;
            }
            return null;
        }
        public string GetValue(string valueFieldName, bool isString)
        {
            return GetValue(valueFieldName).ToString();
        }
        public ItemState GetState (string valueFieldName)
        {
            if (this._originalModel == null)
            {
                return ItemState.Active;
            }

            long? id = GetStatus(valueFieldName);
            int? idOrg = _originalModel.GetStatus(valueFieldName);
            int? status = GetStatus(valueFieldName);
            int? statusOrg = _originalModel.GetStatus(valueFieldName);
            decimal? val = GetValue(valueFieldName);
            decimal? valOrg = _originalModel.GetValue(valueFieldName);

            if (val != null && idOrg == null)
                return ItemState.New;
            if (status == statusOrg && status == 2 && val != valOrg)
                return ItemState.Modified;
            if (status == statusOrg && status == 9 && val == valOrg)
                return ItemState.InActive;
            if (status == statusOrg && status == 2 && val == valOrg)
                return ItemState.Active;
            if (status != statusOrg && status == 2)
                return ItemState.Activated;
            if (status != statusOrg && status == 9)
                return ItemState.Deactivated;
            return ItemState.Empty;
        }
        public string Tag { get { return string.Empty; } }
    }
}