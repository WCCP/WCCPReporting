﻿using System;
using System.Collections.Generic;
using SoftServe.Reports.AGNTProject.Utils;

namespace SoftServe.Reports.AGNTProject.Models
{
    public class Settings
    {
        public event EventHandler StatusChanged;
        private Dictionary<string, string> _selectedReports;
        public int UserLevel { get; set; }
        public bool ShowTotals { get; set; }
        public int PeriodId { get; set; }
        public int Status { get; set; }
        public string PeriodName { get; set; }
        public int CountryId { get; set; }
        public List<string> RegionIDsList { get; set; }
        public Dictionary<string, string> SelectedReports
        {
            get { return _selectedReports ?? (_selectedReports = new Dictionary<string, string>()); }
        }
        public void OnStatusChanged()
        {
            StatusChanged(this, new EventArgs());
        }
    }
}
