﻿using BLToolkit.Mapping;
using SoftServe.Reports.AGNTProject.Utils;
using System.Collections.Generic;
using System.ComponentModel;

namespace SoftServe.Reports.AGNTProject.Models
{
    public class AmortBonusModel : ISourceModel
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private bool _isNotifyOn = false;
        private AmortBonusModel _originalModel;

        private string _m1Id;
        private int? _m1Status;
        private decimal? _m1Value;

        private string _m2Id;
        private int? _m2Status;
        private decimal? _m2Value;

        private string _m3Id;
        private int? _m3Status;
        private decimal? _m3Value;

        private string _m4Id;
        private int? _m4Status;
        private decimal? _m4Value;

        private string _m5Id;
        private int? _m5Status;
        private decimal? _m5Value;
      

        [MapField("UserType_id")]
        public int UserType_id { get; set; }
        [MapField("UserType_ShortName")]
        public string UserType_ShortName { get; set; }


        [MapField("M1_ID")]
        public string M1_ID { get { return _m1Id; } set {_m1Id = value; } }
        ///
        [MapField("M1_STATUS")]
        public int? M1_STATUS { get { return _m1Status; } set { SetField(ref _m1Status, value, "M1_STATUS"); } }
        [MapField("M1")]
        public decimal? M1_VALUE { get { return _m1Value; } set { SetField(ref _m1Value, value, "M1_VALUE"); } }

        [MapField("M2_ID")]
        public string M2_ID { get { return _m2Id; } set { _m2Id = value; } }
        ///
        [MapField("M2_STATUS")]
        public int? M2_STATUS { get { return _m2Status; } set { SetField(ref _m2Status, value, "M2_STATUS"); } }
        [MapField("M2")]
        public decimal? M2_VALUE { get { return _m2Value; } set { SetField(ref _m2Value, value, "M2_VALUE"); } }

        [MapField("M3_ID")]
        public string M3_ID { get { return _m3Id; } set { _m3Id = value; } }
        ///
        [MapField("M3_STATUS")]
        public int? M3_STATUS { get { return _m3Status; } set { SetField(ref _m3Status, value, "M3_STATUS"); } }
        [MapField("M3")]
        public decimal? M3_VALUE { get { return _m3Value; } set { SetField(ref _m3Value, value, "M3_VALUE"); } }

        [MapField("M4_ID")]
        public string M4_ID { get { return _m4Id; } set { _m4Id = value; } }
        ///
        [MapField("M4_STATUS")]
        public int? M4_STATUS { get { return _m4Status; } set { SetField(ref _m4Status, value, "M4_STATUS"); } }
        [MapField("M4")]
        public decimal? M4_VALUE { get { return _m4Value; } set { SetField(ref _m4Value, value, "M4_VALUE"); } }

        [MapField("M5_ID")]
        public string M5_ID { get { return _m5Id; } set { _m5Id = value; } }
        ///
        [MapField("M5_STATUS")]
        public int? M5_STATUS { get { return _m5Status; } set { SetField(ref _m5Status, value, "M5_STATUS"); } }
        [MapField("M5")]
        public decimal? M5_VALUE { get { return _m5Value; } set { SetField(ref _m5Value, value, "M5_VALUE"); } }
        public AmortBonusModel OriginalModel
        {
            get { return _originalModel; }
        }
        private void MakeCopy()
        {
            _originalModel = new AmortBonusModel()
            {
                UserType_id = this.UserType_id,
                UserType_ShortName = this.UserType_ShortName,
                M1_ID = this.M1_ID,
                M1_STATUS = this.M1_STATUS,
                M1_VALUE = this.M1_VALUE,
                M2_ID = this.M2_ID,
                M2_STATUS = this.M2_STATUS,
                M2_VALUE = this.M2_VALUE,
                M3_ID = this.M3_ID,
                M3_STATUS = this.M3_STATUS,
                M3_VALUE = this.M3_VALUE,
                M4_ID = this.M4_ID,
                M4_STATUS = this.M4_STATUS,
                M4_VALUE = this.M4_VALUE,
                M5_ID = this.M5_ID,
                M5_STATUS = this.M5_STATUS,
                M5_VALUE = this.M5_VALUE,
            };
        }

        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
        private bool SetField<T>(ref T field, T value, string propertyName)
        {
            if (EqualityComparer<T>.Default.Equals(field, value))
                return false;
            field = value;
            if (_isNotifyOn)
                OnPropertyChanged(propertyName);
            return true;
        }
        public void OnSourceNotification()
        {
            MakeCopy();
            _isNotifyOn = true;
        }
        public int? GetStatus(string valueFieldName)
        {
            int st = 0;
            string core = valueFieldName.Replace("_VALUE", string.Empty);
            var val = this.GetType().GetProperty(core + "_STATUS").GetValue(this, null);

            if (val != null && int.TryParse(val.ToString(), out st))
            {
                return st;
            }
            return null;
        }
        public void SetStatus(string valueFieldName, int status)
        {
            string core = valueFieldName.Replace("_VALUE", string.Empty);
            this.GetType().GetProperty(core + "_STATUS").SetValue(this, status, null);
        }
        public string GetId(string valueFieldName)
        {
            string core = valueFieldName.Replace("_VALUE", string.Empty);
            var val = this.GetType().GetProperty(core + "_ID").GetValue(this, null);
            if (val != null)
            {
                return val.ToString();
            }
            return null;
        }
        public void SetId(string valueFieldName, string id)
        {
            string core = valueFieldName.Replace("_VALUE", string.Empty);
            this.GetType().GetProperty(core + "_ID").SetValue(this, id, null);
        }
        public decimal? GetValue(string valueFieldName)
        {
            decimal v = 0;
            var val = this.GetType().GetProperty(valueFieldName).GetValue(this, null);
            if (val != null && decimal.TryParse(val.ToString(), out v))
            {
                return v;
            }
            return null;
        }
        public string GetValue(string valueFieldName, bool isString)
        {
            return GetValue(valueFieldName).ToString();
        }
        public ItemState GetState(string valueFieldName)
        {
            if (this._originalModel == null)
            {
                return ItemState.Active;
            }

            long? id = GetStatus(valueFieldName);
            int? idOrg = _originalModel.GetStatus(valueFieldName);
            int? status = GetStatus(valueFieldName);
            int? statusOrg = _originalModel.GetStatus(valueFieldName);
            decimal? val = GetValue(valueFieldName);
            decimal? valOrg = _originalModel.GetValue(valueFieldName);

            if (val != null && idOrg == null)
                return ItemState.New;
            if (status == statusOrg && status == 2 && val != valOrg)
                return ItemState.Modified;
            if (status == statusOrg && status == 9 && val == valOrg)
                return ItemState.InActive;
            if (status == statusOrg && status == 2 && val == valOrg)
                return ItemState.Active;
            if (status != statusOrg && status == 2)
                return ItemState.Activated;
            if (status != statusOrg && status == 9)
                return ItemState.Deactivated;
            return ItemState.Empty;
        }
        public string Tag { get { return UserType_id.ToString(); } }
    }
}
