﻿using GMap.NET.ObjectModel;
using SoftServe.Reports.AGNTProject.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SoftServe.Reports.AGNTProject.Models
{
    public interface ISourceModel: INotifyPropertyChanged
    {
        ItemState GetState(string valueFieldName);
        string GetId(string valueFieldName);
        void SetId(string valueFieldName, string id);
        decimal? GetValue(string valueFieldName);
        string GetValue(string valueFieldName, bool isString);
        void SetStatus(string valueFieldName, int status);
        void OnSourceNotification();
        string Tag { get; }
    }
}
