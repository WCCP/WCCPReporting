﻿using BLToolkit.Mapping;
using SoftServe.Reports.AGNTProject.Utils;
using System.Collections.Generic;
using System.ComponentModel;

namespace SoftServe.Reports.AGNTProject.Models
{
    public class CountrySettingsModel : ISourceModel
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private bool _isNotifyOn = false;
        private CountrySettingsModel _originalModel;

        private string _partdayofflimitId;
        private int? _partdayofflimitStatus;
        private decimal? _partdayofflimitValue;

        private string _echId;
        private int? _echIdStatus;
        private decimal? _echValue;

        private string _avgdayofmonthId;
        private int? _avgdayofmonthStatus;
        private decimal? _avgdayofmonthValue;

        private string _vatrateId;
        private int? _vatrateStatus;
        private decimal? _vatrateValue;    

        [MapField("PARTDAYOFFLIMIT_ID")]
        public string PARTDAYOFFLIMIT_ID { get { return _partdayofflimitId; } set { _partdayofflimitId = value; } }
        ///
        [MapField("PARTDAYOFFLIMIT_STATUS")]
        public int? PARTDAYOFFLIMIT_STATUS { get { return _partdayofflimitStatus; } set { SetField(ref _partdayofflimitStatus, value, "PARTDAYOFFLIMIT_STATUS"); } }
        [MapField("PARTDAYOFFLIMIT")]
        public decimal? PARTDAYOFFLIMIT_VALUE { get { return _partdayofflimitValue; } set { SetField(ref _partdayofflimitValue, value, "PARTDAYOFFLIMIT_VALUE"); } }

        [MapField("ECH_ID")]
        public string ECH_ID { get { return _echId; } set { _echId = value; } }
        ///
        [MapField("ECH_STATUS")]
        public int? ECH_STATUS { get { return _echIdStatus; } set { SetField(ref _echIdStatus, value, "ECH_STATUS"); } }
        [MapField("ECH")]
        public decimal? ECH_VALUE { get { return _echValue; } set { SetField(ref _echValue, value, "ECH_VALUE"); } }

        [MapField("AVGDAYOFMONTH_ID")]
        public string AVGDAYOFMONTH_ID { get { return _avgdayofmonthId; } set { _avgdayofmonthId = value; } }
        ///
        [MapField("AVGDAYOFMONTH_STATUS")]
        public int? AVGDAYOFMONTH_STATUS { get { return _avgdayofmonthStatus; } set { SetField(ref _avgdayofmonthStatus, value, "AVGDAYOFMONTH_STATUS"); } }
        [MapField("AVGDAYOFMONTH")]
        public decimal? AVGDAYOFMONTH_VALUE { get { return _avgdayofmonthValue; } set { SetField(ref _avgdayofmonthValue, value, "AVGDAYOFMONTH_VALUE"); } }

        [MapField("VATRATE_ID")]
        public string VATRATE_ID { get { return _vatrateId; } set { _vatrateId = value; } }
        ///
        [MapField("VATRATE_STATUS")]
        public int? VATRATE_STATUS { get { return _vatrateStatus; } set { SetField(ref _vatrateStatus, value, "VATRATE_STATUS"); } }
        [MapField("VATRATE")]
        public decimal? VATRATE_VALUE { get { return _vatrateValue; } set { SetField(ref _vatrateValue, value, "VATRATE_VALUE"); } }

        
        public CountrySettingsModel OriginalModel
        {
            get { return _originalModel; }
        }
        private void MakeCopy()
        {
            _originalModel = new CountrySettingsModel()
            {
                PARTDAYOFFLIMIT_ID = this.PARTDAYOFFLIMIT_ID,
                PARTDAYOFFLIMIT_STATUS = this.PARTDAYOFFLIMIT_STATUS,
                PARTDAYOFFLIMIT_VALUE = this.PARTDAYOFFLIMIT_VALUE,
                ECH_ID = this.ECH_ID,
                ECH_STATUS = this.ECH_STATUS,
                ECH_VALUE = this.ECH_VALUE,
                AVGDAYOFMONTH_ID = this.AVGDAYOFMONTH_ID,
                AVGDAYOFMONTH_STATUS = this.AVGDAYOFMONTH_STATUS,
                AVGDAYOFMONTH_VALUE = this.AVGDAYOFMONTH_VALUE,
                VATRATE_ID = this.VATRATE_ID,
                VATRATE_STATUS = this.VATRATE_STATUS,
                VATRATE_VALUE = this.VATRATE_VALUE
            };
        }

        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
        private bool SetField<T>(ref T field, T value, string propertyName)
        {
            if (EqualityComparer<T>.Default.Equals(field, value))
                return false;
            field = value;
            if (_isNotifyOn)
                OnPropertyChanged(propertyName);
            return true;
        }
        public void OnSourceNotification()
        {
            MakeCopy();
            _isNotifyOn = true;
        }
        public int? GetStatus(string valueFieldName)
        {
            int st = 0;
            string core = valueFieldName.Replace("_VALUE", string.Empty);
            var val = this.GetType().GetProperty(core + "_STATUS").GetValue(this, null);

            if (val != null && int.TryParse(val.ToString(), out st))
            {
                return st;
            }
            return null;
        }
        public void SetStatus(string valueFieldName, int status)
        {
            string core = valueFieldName.Replace("_VALUE", string.Empty);
            this.GetType().GetProperty(core + "_STATUS").SetValue(this, status, null);
        }
        public string GetId(string valueFieldName)
        {
            string core = valueFieldName.Replace("_VALUE", string.Empty);
            var val = this.GetType().GetProperty(core + "_ID").GetValue(this, null);
            if (val != null)
            {
                return val.ToString();
            }
            return null;
        }
        public void SetId(string valueFieldName, string id)
        {
            string core = valueFieldName.Replace("_VALUE", string.Empty);
            this.GetType().GetProperty(core + "_ID").SetValue(this, id, null);
        }
        public decimal? GetValue(string valueFieldName)
        {
            decimal v = 0;
            var val = this.GetType().GetProperty(valueFieldName).GetValue(this, null);
            if (val != null && decimal.TryParse(val.ToString(), out v))
            {
                return v;
            }
            return null;
        }
        public string GetValue(string valueFieldName, bool isString)
        {
            return GetValue(valueFieldName).ToString();
        }
        public ItemState GetState(string valueFieldName)
        {
            if (this._originalModel == null)
            {
                return ItemState.Active;
            }

            long? id = GetStatus(valueFieldName);
            int? idOrg = _originalModel.GetStatus(valueFieldName);
            int? status = GetStatus(valueFieldName);
            int? statusOrg = _originalModel.GetStatus(valueFieldName);
            decimal? val = GetValue(valueFieldName);
            decimal? valOrg = _originalModel.GetValue(valueFieldName);

            if (val != null && idOrg == null)
                return ItemState.New;
            if (status == statusOrg && status == 2 && val != valOrg)
                return ItemState.Modified;
            if (status == statusOrg && status == 9 && val == valOrg)
                return ItemState.InActive;
            if (status == statusOrg && status == 2 && val == valOrg)
                return ItemState.Active;
            if (status != statusOrg && status == 2)
                return ItemState.Activated;
            if (status != statusOrg && status == 9)
                return ItemState.Deactivated;
            return ItemState.Empty;
        }
        public string Tag { get { return string.Empty; } }
    }
}
