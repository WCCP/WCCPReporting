﻿using BLToolkit.DataAccess;
using BLToolkit.Mapping;


namespace SoftServe.Reports.AGNTProject.Models
{
    public class Country
    {
        [MapField("Country_id"), PrimaryKey]
        public int ID { get; set; }
        [MapField("Country_name")]
        public string Name { get; set; }

        public override string ToString()
        {
            return this.Name;
        }
    }
}