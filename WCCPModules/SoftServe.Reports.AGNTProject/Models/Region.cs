﻿using BLToolkit.DataAccess;
using BLToolkit.Mapping;

namespace SoftServe.Reports.AGNTProject.Models
{
    public class Region
    {
        [MapField("region_id"), PrimaryKey]
        public int ID { get; set; }
        [MapField("Region_name")]
        public string Name { get; set; }
        [MapField("Country_id")]
        public int CountryID { get; set; }

        public override string ToString()
        {
            return this.Name;
        }
    }
}
