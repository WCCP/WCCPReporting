﻿using BLToolkit.DataAccess;
using BLToolkit.Mapping;

namespace SoftServe.Reports.AGNTProject.Models
{
    public class Period
    {
        [MapField("Period"), PrimaryKey]
        public int ID { get; set; }
        [MapField("PeriodText")]
        public string Name { get; set; }

        public override string ToString()
        {
            return this.Name;
        }
    }
}
