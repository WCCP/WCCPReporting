﻿using System;
using System.Collections.Generic;
using System.Linq;
using SoftServe.Reports.AGNTProject.Utils;

namespace SoftServe.Reports.AGNTProject.Models
{
    class KeyColumnIntoXls
    {
        public int IdSector { get; set; }
        public int CodeSector { get; set; }
        public string ErrMess { get; set; }
    }

    class ValueCell
    {
        /// <summary>
        /// Значение из ячейки
        /// </summary>
        public object Value { get; private set; }
        /// <summary>
        /// Ошибка, которую можно определить читая данные из ячейки (не соответствие типов)
        /// </summary>
        public string ErrMess { get; private set; }

        public ValueCell(object value, string errMess)
        {
            Value = value;
            ErrMess = errMess;
        }
    }

    class BaseOfEmplFromXls
    {
        private string _errorMes;

        public BaseOfEmplFromXls(KeyColumnIntoXls key)
        {
            KeyColumn = key;
            ErrorMes = key.ErrMess;
        }

        public KeyColumnIntoXls KeyColumn { get; private set; }
        public bool HasError { get; private set; }

        public ColumnFromXls ColumnFromXls { get; set; }

        /// <summary>
        /// Устанавливаем значение из ячейки
        /// </summary>
        /// <param name="valueCell">Значение из ячейки</param>
        public void SetValue(ValueCell valueCell)
        {
            if (string.IsNullOrEmpty(valueCell.ErrMess))
            {
                CheckDataError(valueCell.Value);
                Value = GetValueFromValueCell(valueCell);
            }
            else
            {
                ErrorMes = valueCell.ErrMess;
                Value = null;
            }

        }

        public object Value { get; private set; }

        public string ErrorMes
        {
            get { return _errorMes; } 
            set
            {
                HasError = !string.IsNullOrEmpty(value);
                _errorMes = value;
            }
        }

        private object GetValueFromValueCell(ValueCell valueCell)
        {
            if (ColumnFromXls == null)
                throw new ArgumentException(
                    "Значения параметра Value можно определять после определения столбца ColumnFromXls");

            object returnVal = null;
            //если у нас нет ошибок, то можно определить значение
            if (!string.IsNullOrEmpty(ErrorMes))
                return null;

            if (ColumnFromXls.ColumnFildName == "WorkSchema")
            {
                string tmpStr = (string) valueCell.Value;
                WorkSchemaList wsl = new WorkSchemaList();
                foreach (WorkSchema schema in wsl.WorkSchemata.Where(schema => tmpStr == schema.WorkSchemaName))
                {
                    returnVal = schema.WorkSchemaId;
                }
            } else if (ColumnFromXls.ColumnFildName == "Amortization_norm" | ColumnFromXls.ColumnFildName == "FuelConsumption_norm"
                    | ColumnFromXls.ColumnFildName == "Fuel_price")
            {
                //округляем до 2-х знаков
                returnVal = Math.Round((double)valueCell.Value, 2);
            }
            else
            {
                returnVal = valueCell.Value;
            }
            return returnVal;
        }

        private void CheckDataError(object value)
        {
            if (ColumnFromXls == null)
                throw new ArgumentException(
                    "Значения параметра Value можно определять после определения столбца ColumnFromXls");

            if (ColumnFromXls.ColumnFildName == "WorkDays_total" | ColumnFromXls.ColumnFildName == "WorkedDays_num"
                | ColumnFromXls.ColumnFildName == "WorkSchema" | ColumnFromXls.ColumnFildName == "Salary1"
                | ColumnFromXls.ColumnFildName == "ConstPart_sum" | ColumnFromXls.ColumnFildName == "Amortization_norm"
                | ColumnFromXls.ColumnFildName == "MileagePerMonth_fact" | ColumnFromXls.ColumnFildName == "FuelConsumption_norm"
                | ColumnFromXls.ColumnFildName == "Fuel_price" | ColumnFromXls.ColumnFildName == "VariablePart_sum"
                | ColumnFromXls.ColumnFildName == "AddCompensation" | ColumnFromXls.ColumnFildName == "VariablePartClosingPerc_fact"
                | ColumnFromXls.ColumnFildName == "CorrectVarPartPrevP_sum")
            {
                if (value == null)
                {
                    ErrorMes = "Поле " + ColumnFromXls.ColumnName + " не может быть пустой!";
                }
                if (ColumnFromXls.ColumnFildName == "WorkDays_total" | ColumnFromXls.ColumnFildName == "WorkedDays_num")
                {
                    if (value != null)
                    {
                        int tmpWd = (int)value;
                        if (tmpWd < 0 | tmpWd > 31)
                        {
                            ErrorMes = "Значение " + ColumnFromXls.ColumnName + " должно быть в диапазоне от 0 до 31";
                        }
                    }
                }
                if (ColumnFromXls.ColumnFildName == "WorkSchema")
                {
                    if (value != null)
                    {
                        string tmpStr = (string)value;
                        WorkSchemaList wsl = new WorkSchemaList();
                        bool isExists = wsl.WorkSchemata.Any(schema => schema.WorkSchemaName == tmpStr);
                        if (!isExists)
                        {
                            ErrorMes = "Не корректные значения столбца " + ColumnFromXls.ColumnName +
                                       ", ожидаю 'С оптимизацией' или 'Без оптимизации'!";
                        }
                    }
                }

                if (ColumnFromXls.ColumnFildName == "Salary1" | ColumnFromXls.ColumnFildName == "ConstPart_sum"
                    | ColumnFromXls.ColumnFildName == "MileagePerMonth_fact" | ColumnFromXls.ColumnFildName == "VariablePart_sum"
                    | ColumnFromXls.ColumnFildName == "AddCompensation" | ColumnFromXls.ColumnFildName == "VariablePartClosingPerc_fact"
                    | ColumnFromXls.ColumnFildName == "CorrectVarPartPrevP_sum")
                {
                    if (value != null)
                    {
                        int tmpWd = (int)value;
                        if (tmpWd < 0)
                        {
                            ErrorMes = "Значение " + ColumnFromXls.ColumnName + " не может быть меньше 1-цы!";
                        }
                    }
                }

                if (ColumnFromXls.ColumnFildName == "Amortization_norm" | ColumnFromXls.ColumnFildName == "FuelConsumption_norm" 
                    | ColumnFromXls.ColumnFildName == "Fuel_price")
                {
                    if (value != null)
                    {
                        double tmpWd;
                        bool result = double.TryParse(value.ToString(),out tmpWd);
                        if (!result)
                        {                            
                            ErrorMes = "Значение поля " + ColumnFromXls.ColumnName + " не являеться допустимым типом!";                            
                        }
                        if (tmpWd > 999999.99)
                        {
                            ErrorMes = "Значение поля " + ColumnFromXls.ColumnName + " превышает допустимый диапазон (>999999.99)!";
                        }
                        if (tmpWd < 0)
                        {
                            ErrorMes = "Значение " + ColumnFromXls.ColumnName + " не может быть меньше 0-ля!";
                        }
                        //округляем до 2-х знаков
                    }
                }
            }
        }
    }

    class BaseOfEmplFromXlsLst
    {
        public BaseOfEmplFromXlsLst()
        {
            BaseOfEmplFromXls = new List<BaseOfEmplFromXls>();
        }
        public List<BaseOfEmplFromXls> BaseOfEmplFromXls { get; private set; }
    }
}
