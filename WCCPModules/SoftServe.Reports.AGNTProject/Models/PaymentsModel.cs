﻿using BLToolkit.Mapping;
using SoftServe.Reports.AGNTProject.Utils;
using System.Collections.Generic;
using System.ComponentModel;

namespace SoftServe.Reports.AGNTProject.Models
{
    public class PaymentsModel : ISourceModel
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private bool _isNotifyOn = false;
        private PaymentsModel _originalModel;

        private string _paymentId;
        private int? _paymentStatus;
        private string _paymentValue;



        [MapField("PAYMENT_ID")]
        public string PAYMENT_ID { get { return _paymentId; } set { _paymentId = value; } }
        ///
        [MapField("PAYMENT_STATUS")]
        public int? PAYMENT_STATUS { get { return _paymentStatus; } set { SetField(ref _paymentStatus, value, "PAYMENT_STATUS"); } }
        [MapField("PAYMENT")]
        public string PAYMENT_VALUE { get { return _paymentValue; } set { SetField(ref _paymentValue, value, "PAYMENT_VALUE"); } }

        public PaymentsModel OriginalModel
        {
            get { return _originalModel; }
        }
        private void MakeCopy()
        {
            _originalModel = new PaymentsModel()
            {
                PAYMENT_ID = this.PAYMENT_ID,
                PAYMENT_STATUS = this.PAYMENT_STATUS,
                PAYMENT_VALUE = this.PAYMENT_VALUE
            };
        }

        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
        private bool SetField<T>(ref T field, T value, string propertyName)
        {
            if (EqualityComparer<T>.Default.Equals(field, value))
                return false;
            field = value;
            if (_isNotifyOn)
                OnPropertyChanged(propertyName);
            return true;
        }
        public void OnSourceNotification()
        {
            MakeCopy();
            _isNotifyOn = true;
        }
        public int? GetStatus(string valueFieldName)
        {
            int st = 0;
            string core = valueFieldName.Replace("_VALUE", string.Empty);
            var val = this.GetType().GetProperty(core + "_STATUS").GetValue(this, null);

            if (val != null && int.TryParse(val.ToString(), out st))
            {
                return st;
            }
            return null;
        }
        public void SetStatus(string valueFieldName, int status)
        {
            string core = valueFieldName.Replace("_VALUE", string.Empty);
            this.GetType().GetProperty(core + "_STATUS").SetValue(this, status, null);
        }
        public string GetId(string valueFieldName)
        {
            string core = valueFieldName.Replace("_VALUE", string.Empty);
            var val = this.GetType().GetProperty(core + "_ID").GetValue(this, null);
            if (val != null)
            {
                return val.ToString();
            }
            return null;
        }
        public void SetId(string valueFieldName, string id)
        {
            string core = valueFieldName.Replace("_VALUE", string.Empty);
            this.GetType().GetProperty(core + "_ID").SetValue(this, id, null);
        }
        public decimal? GetValue(string valueFieldName)
        {
            return 0;
        }
        public string GetValue(string valueFieldName, bool isString)
        {
            var val = this.GetType().GetProperty(valueFieldName).GetValue(this, null);
            if (val != null)
            {
                return val.ToString();
            }
            return null;
        }
        public ItemState GetState(string valueFieldName)
        {
            if (this._originalModel == null)
            {
                return ItemState.Active;
            }

            long? id = GetStatus(valueFieldName);
            int? idOrg = _originalModel.GetStatus(valueFieldName);
            int? status = GetStatus(valueFieldName);
            int? statusOrg = _originalModel.GetStatus(valueFieldName);
            string val = GetValue(valueFieldName, true);
            string valOrg = _originalModel.GetValue(valueFieldName, true);

            if (val != null && idOrg == null)
                return ItemState.New;
            if (status == statusOrg && status == 2 && val != valOrg)
                return ItemState.Modified;
            if (status == statusOrg && status == 9 && val == valOrg)
                return ItemState.InActive;
            if (status == statusOrg && status == 2 && val == valOrg)
                return ItemState.Active;
            if (status != statusOrg && status == 2)
                return ItemState.Activated;
            if (status != statusOrg && status == 9)
                return ItemState.Deactivated;
            return ItemState.Empty;
        }
        public string Tag { get { return string.Empty; } }
    }
}
