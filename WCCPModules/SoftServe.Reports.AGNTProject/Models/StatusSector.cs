﻿using System.Collections.Generic;

namespace SoftServe.Reports.AGNTProject.Models
{
    internal class StatusSector
    {
        public int StatusSectorId { get; set; }
        public string StatusSectorName { get; set; }
    }

    class StatusSectorList
    {
        private readonly List<StatusSector> _statusSector = new List<StatusSector>();
        public List<StatusSector> StatusSector { get { return _statusSector; } }



        public StatusSectorList()
        {
            var statusActive = new StatusSector
            {
                StatusSectorId = 2,
                StatusSectorName = "Активный"
            };
            _statusSector.Add(statusActive);
            var statusNoActive = new StatusSector
            {
                StatusSectorId = 9,
                StatusSectorName = "Не активный"
            };
            _statusSector.Add(statusNoActive);
        }
    }
}
