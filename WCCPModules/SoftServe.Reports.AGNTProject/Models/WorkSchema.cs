﻿using System.Collections.Generic;

namespace SoftServe.Reports.AGNTProject.Models
{
    internal class WorkSchema
    {
        public int WorkSchemaId { get; set; }
        public  string WorkSchemaName { get; set; }
    }

    class WorkSchemaList
    {
        private readonly List<WorkSchema> _workSchemata = new List<WorkSchema>();
        public List<WorkSchema> WorkSchemata {get { return _workSchemata; } }
    


        public WorkSchemaList()
        {
            var schema1 = new WorkSchema
            {
                WorkSchemaId = 0,
                WorkSchemaName = "С оптимизацией"
            };
            _workSchemata.Add(schema1);
            var schema2 = new WorkSchema
            {
                WorkSchemaId = 1,
                WorkSchemaName = "Без оптимизации"
            };
            _workSchemata.Add(schema2);
        }
    }
}
