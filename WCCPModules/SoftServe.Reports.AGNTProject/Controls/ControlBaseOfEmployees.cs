﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using DevExpress.Data.Filtering;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraPrinting;
using Logica.Reports.Common.WaitWindow;
using SoftServe.Reports.AGNTProject.DataAccess;
using SoftServe.Reports.AGNTProject.Models;
using SoftServe.Reports.AGNTProject.Utils;
using DataTable = System.Data.DataTable;

namespace SoftServe.Reports.AGNTProject.Controls
{
    public partial class ControlBaseOfEmployees : UserControl, ITabReport, IEditableReport, IExportData
    {
        private DataTable _baseOfEmployees;
        private Settings _settings;
        private readonly BaseOfEmplFromXlsLst _baseOfEmplFromXlsLst = new BaseOfEmplFromXlsLst();
        readonly Stream _layoutToMem = new MemoryStream();

        public ControlBaseOfEmployees()
        {
            InitializeComponent();
            ReportText = Constants.CaptionEmployeesBase;
            SaveFilter = false;
        }

        public string ReportText { get; set ; }
        public bool SaveFilter { get; set; }
        private int PeriodId { get; set ; }
        private string RegionIdLst { get; set; }

        public void LoadData(Settings settings)
        {
            _settings = settings;
            try
            {
                PeriodId = _settings.PeriodId;
                RegionIdLst = string.Empty;
                foreach (string s in _settings.RegionIDsList)
                {
                    RegionIdLst = RegionIdLst + s + ",";
                }
                //убираем последнюю запятую
                RegionIdLst = RegionIdLst.Remove(RegionIdLst.Length - 1);
            }
            catch (Exception ex)

            {
                throw new ArgumentException("Ошибка получения парамтров: " + ex.Message);
            }

            _baseOfEmployees = DataProvider.GetEmployeesBase(PeriodId, RegionIdLst);
            gridControl1.DataSource = _baseOfEmployees;

            var workSchemaList = new WorkSchemaList();
            lueWorkSchema.DataSource = workSchemaList.WorkSchemata;

            luePublicTrCompensation_LimitID.DataSource = DataProvider.GetLimitsOfSpendsByCode(PeriodId, "COMPESTRANSP", null);
            lueConnectionCompensation_LimitID.DataSource = DataProvider.GetLimitsOfSpendsByCode(PeriodId, "COMPESLINK", null);
            lueFoodCompensation_LimitID.DataSource = DataProvider.GetLimitsOfSpendsByCode(PeriodId, "COMPESFOOD", null);
            lueStationeryCompensation_LimitID.DataSource = DataProvider.GetLimitsOfSpendsByCode(PeriodId, "COMPESOFFICE", null);
            lueConnectionTabletCompensation_LimitID.DataSource = DataProvider.GetLimitsOfSpendsByCode(PeriodId, "COMPESLINKPAD", null);

            var statusSectorList = new StatusSectorList();
            lueStatus.DataSource = statusSectorList.StatusSector;

            //права
            Access();
            //в зависимости от периода редактируем да/нет
            gridView1.OptionsBehavior.Editable = _settings.Status != 9;
            
            //
            if (!SaveFilter)
            {
                gridView1.ActiveFilterCriteria = null;
                gridView1.ApplyFindFilter("");
                gridView1.FindPanelVisible = false;
                gridView1.OptionsView.ShowAutoFilterRow = false;
            }
            SaveFilter = false;
        }

        private void Access()
        {
            AccessToApp accessToApp = AccessToApp.GetInstance();
            if (accessToApp.IsAdmin | accessToApp.IsReportEdit) return;
            if (accessToApp.IsBaseEmplCenter)
            {
                foreach (GridColumn column in gridView1.Columns)
                {
                    if (column.FieldName == "Sector_code" | column.FieldName == "Sector_name" |
                        column.FieldName == "UserLevel"
                        | column.FieldName == "SectorType_Name" | column.FieldName == "WorkedDays_num" |
                        column.FieldName == "AddCompensation"
                        | column.FieldName == "CorrectVarPartPrevP_sum")
                    {
                        if (column.FieldName != "WorkedDays_num")
                            column.OptionsColumn.AllowEdit = false;
                    }
                    else
                    {
                        column.Visible = false;
                        column.OptionsColumn.ShowInCustomizationForm = false;
                    }
                }
                return;
            }
            if (accessToApp.IsReport)
            {
                foreach (GridColumn column in gridView1.Columns)
                {
                    column.OptionsColumn.AllowEdit = false;
                }
                sbReCalc.Enabled = false;
                sbLoadFromXLS.Enabled = false;
            }
            else
            {
                throw new ApplicationException("Вы не имеете доступа к данной вкладке");
            }
        }

        private string EditLayoutFileName(string path)
        {
            string fileName = "ControlBaseOfEmployees_" + Path.GetFileName(path);
            string pathToDir = Path.GetDirectoryName(path);
            if (pathToDir != null) return Path.Combine(pathToDir, fileName);
            return path;
        }

        public void SaveLayout(string path)
        {
            //Добавим к имени имя компонента
            string fullPath = EditLayoutFileName(path);
            gridView1.SaveLayoutToXml(fullPath);
        }

        public void RestoreLayout(string path)
        {
            gridView1.SaveLayoutToStream(_layoutToMem);
            _layoutToMem.Seek(0, SeekOrigin.Begin);
            gridView1.BestFitColumns();

            //Добавим к имени имя компонента
            string fullPath = EditLayoutFileName(path);
            if (File.Exists(fullPath))
            {
                gridView1.RestoreLayoutFromXml(fullPath);
            }
        }

        private bool CheckAndSaveModifiedRow()
        {
            //сохраняем последние изменения в gridView1.GridControl.DataSource
            gridView1.CloseEditor();
            gridView1.UpdateCurrentRow();

            DataTable sourse = gridView1.GridControl.DataSource as DataTable;
            if (sourse == null) throw new NullReferenceException("Ошибка определения источника данных!");

            //смотрим есть не сохраненные данные
            bool isDirtyTrue = sourse.Rows.Cast<DataRow>().Any(row => row.RowState != DataRowState.Unchanged);
            if (isDirtyTrue)
            {
                DialogResult result = XtraMessageBox.Show(TextConstants.MSG_UNSAVED_DATA_FOUND, @"Сохранение",
                    MessageBoxButtons.YesNo);
                if (result == DialogResult.Yes)
                {
                    try
                    {
                        //Сохраним
                        return SaveData();
                    }
                    catch (Exception)
                    {
                        return false;
                    }
                }

                //обновить данные
                _baseOfEmployees = DataProvider.GetEmployeesBase(PeriodId, RegionIdLst);
                gridControl1.DataSource = _baseOfEmployees;
                return true;
            }
            return true;
        }

        private void gridView1_ShownEditor(object sender, EventArgs e)
        {
            ColumnView view = (ColumnView) sender;

            string fieldName = view.FocusedColumn.FieldName;
            string paramName = view.FocusedColumn.Name;
            //Фильтруем Выпадайки в зависимости от статуса (кроме Shadow столбцов)
            if (paramName.IndexOf("Shadow", StringComparison.Ordinal) == -1)
            {
                if (fieldName == "PublicTrCompensation_LimitID" | fieldName == "ConnectionCompensation_LimitID" |
                    fieldName == "FoodCompensation_LimitID"
                    | fieldName == "StationeryCompensation_LimitID" |
                    fieldName == "ConnectionTabletCompensation_LimitID")
                {
                    //Если включен автофильтр, то наполнение включаем все
                    if (view.FocusedRowHandle != GridControl.AutoFilterRowHandle)
                    {
                        var showOnlyOpen = 2;
                        LookUpEdit editor = (LookUpEdit)view.ActiveEditor;
                        editor.Properties.DataSource = DataProvider.GetLimitsOfSpendsByCode(PeriodId, paramName,
                            showOnlyOpen);
                    }


                }
            }
        }

        /// <summary>
        /// Проверяем валидность данных
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">e</param>
        private void gridView1_ValidatingEditor(object sender, BaseContainerValidateEditorEventArgs e)
        {
            var view = sender as GridView;

            if (view == null)
                return;

            //не проверяем ничего на строке активного фильтра
            if (view.FocusedRowHandle == GridControl.AutoFilterRowHandle) return;

            if (e == null) return;

            string fieldName = view.FocusedColumn.FieldName;

            if (fieldName == "WorkDays_total" | fieldName == "WorkedDays_num")
            {
                if (string.IsNullOrEmpty(e.Value.ToString()))
                {
                    e.Valid = false;
                    string strColumn = gridView1.Columns[fieldName].Caption;
                    e.ErrorText = "Поле " + strColumn + " не может быть пустой!";
                    return;
                }
                int val;
                bool result = Int32.TryParse(e.Value.ToString(), out val);
                if (!result)
                {
                    string strColumn = gridView1.Columns[fieldName].Caption;
                    e.Valid = false;
                    e.ErrorText = "Значение поля " + strColumn + " превышает допустимый диапазон!";
                    return;
                }
                if (val < 0 | val > 31)
                {
                    string strColumn = gridView1.Columns[fieldName].Caption;
                    e.Valid = false;
                    e.ErrorText = "Значение " + strColumn + " должно быть в диапазоне от 0 до 31";
                    return;
                }
            }

            //Поля с целыми числами
            if (fieldName == "Salary1" | fieldName == "ConstPart_sum" | fieldName == "MileagePerMonth_fact" 
                | fieldName == "AddCompensation" | fieldName == "VariablePartClosingPerc_fact" 
                | fieldName == "CorrectVarPartPrevP_sum")
            {
                if (e.Value == null)
                {
                    e.Valid = false;
                    string strColumn = gridView1.Columns[fieldName].Caption;
                    e.ErrorText = "Поле " + strColumn + " не может быть пустой!";
                    return;
                }
                int val;
                bool result = Int32.TryParse(e.Value.ToString(), out val);
                if (!result)
                {
                    string strColumn = gridView1.Columns[fieldName].Caption;
                    e.Valid = false;
                    e.ErrorText = "Значение поля " + strColumn + " превышает допустимый диапазон!";
                    return;
                }
                if (val < 0)
                {
                    string strColumn = gridView1.Columns[fieldName].Caption;
                    e.Valid = false;
                    e.ErrorText = "Значение поля " + strColumn + " не может быть меньше 1-цы";
                }
                if (fieldName == "VariablePartClosingPerc_fact")
                {
                    //ychu согласно письма от Savitskaya, Svetlana <Svetlana.Savitskaya@ab-inbev.com> от Вт 30.05.2017 20:23
                    if (val > int.MaxValue)
                    {
                        string strColumn = gridView1.Columns[fieldName].Caption;
                        e.Valid = false;
                        e.ErrorText = "Значение поля " + strColumn + " не может быть больше 100-а";
                        return;
                    }
                }
            }

            //Поля с дробными числами
            if (fieldName == "Amortization_norm" | fieldName == "FuelConsumption_norm" | fieldName == "Fuel_price")
            {
                if (e.Value == null)
                {
                    e.Valid = false;
                    string strColumn = gridView1.Columns[fieldName].Caption;
                    e.ErrorText = "Поле " + strColumn + " не может быть пустым!";
                    return;
                }
                double val;
                bool result = double.TryParse(e.Value.ToString(), out val);
                if (!result)
                {
                    string strColumn = gridView1.Columns[fieldName].Caption;
                    e.Valid = false;
                    e.ErrorText = "Значение поля " + strColumn + " превышает допустимый диапазон (>999999.99)!";
                    return;
                }
                if (val > 999999.99)
                {
                    string strColumn = gridView1.Columns[fieldName].Caption;
                    e.Valid = false;
                    e.ErrorText = "Значение поля " + strColumn + " превышает допустимый диапазон (>999999.99)!";
                    return;
                }
                if (val < 0)
                {
                    string strColumn = gridView1.Columns[fieldName].Caption;
                    e.Valid = false;
                    e.ErrorText = "Значение поля " + strColumn + " не может быть меньше 0-ля";
                }
            }

            //Убираем ошибку на поле
            int idxRh = view.FocusedRowHandle;
            if (idxRh > 0)
            {
                int idxDt = view.GetDataSourceRowIndex(idxRh);
                DataTable dt = gridView1.GridControl.DataSource as DataTable;
                if (dt == null) return;
                dt.Rows[idxDt].SetColumnError(fieldName, null);
            }
        }

        /// <summary>
        /// Кнопка "Сохранить"
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">EventArgs</param>
        private void sbSave_Click(object sender, EventArgs e)
        {
            var result = SaveData();
            if (!result)
            {
                splitContainerControl1.Text = @"Error!";
            }
            gridControl1.Focus();
        }

        /// <summary>
        /// проверяем измененные строки и сохраняем их
        /// </summary>
        /// <returns>True - нет ошибок</returns>ISNUMERIC(t.PublicTrCompensation_val)
        private bool SaveData()
        {
            WaitManager.StartWait();
            try
            {
                //сохраняем последние изменения в gridView1.GridControl.DataSource
                gridView1.CloseEditor();
                gridView1.UpdateCurrentRow();

                DataTable sourse = gridView1.GridControl.DataSource as DataTable;
                if (sourse == null) throw new NullReferenceException("Ошибка определения источника данных!");

                string firstErr = null;
                for (int i = 0; i < sourse.Rows.Count; i++)
                {
                    var rowChkErr = sourse.Rows[i];
                    //Данные могут только модифицироваться
                    if (rowChkErr.RowState == DataRowState.Modified)
                    {
                        string errMessage;
                        //проверить целостность
                        string errColumn;
                        ChekData(rowChkErr, out errMessage, out errColumn);
                        if (string.IsNullOrEmpty(firstErr)) firstErr = errColumn;
                    }
                }

                //поставим фокус на первую ошибку
                for (int i = 0; i < sourse.Rows.Count; i++)
                {
                    var rowChkErr = sourse.Rows[i];
                    if (rowChkErr.HasErrors)
                    {
                        //Поставим фокус на ошибочную строку
                        int idx = gridView1.GetRowHandle(i);
                        gridView1.FocusedRowHandle = idx;
                        //Поставим фокус на ошибочную ячейку
                        if (!string.IsNullOrEmpty(firstErr))
                        {
                            gridView1.FocusedColumn = gridView1.Columns[firstErr];
                        }
                        gridView1.ShowEditor();
                        return false;
                    }
                }

                if (sourse.Rows.Cast<DataRow>().Any(row => row.RowState == DataRowState.Modified))
                {
                    _settings.Status = DataProvider.GetPeriodStatus(PeriodId);
                    if (_settings.Status == 9)
                    {
                        AccessToApp access = AccessToApp.GetInstance();
                        if (access.IsReportEdit)
                        {
                            XtraMessageBox.Show("Период закрыт для редактирования!", "Период закрыт");
                            return true;
                        }
                        DialogResult result =
                            XtraMessageBox.Show(
                                "Период '" + _settings.PeriodName + "' закрыт. Открыть его для изменения данных?",
                                @"Период закрыт",
                                MessageBoxButtons.YesNo);

                        if (result == DialogResult.Yes)
                        {
                            //переоткрываем
                            string openStatus = "2";
                            DataProvider.EditPeriodStatus(PeriodId, openStatus);
                            _settings.Status = DataProvider.GetPeriodStatus(PeriodId);
                            gridView1.OptionsBehavior.Editable = _settings.Status != 9;
                        }
                        else
                        {
                            return true;
                        }
                    }
                }

                //Инициализация(создание) временной таблицы
                DataProvider.InitBeforeSaveEmployeesBase();

                for (int i = 0; i < sourse.Rows.Count; i++)
                {
                    var row = sourse.Rows[i];
                    if (row.RowState == DataRowState.Modified)
                    {
                        string errMessage = null;
                        try
                        {
                            //Записываем в табл для сохранения
                            DataProvider.SaveEmployeesBaseTmpTable(row);
                        }
                        catch (Exception ex)
                        {
                            errMessage = ex.Message;
                        }
                        finally
                        {
                            row.RowError = errMessage;
                        }
                    }
                    //смотрим есть ли ошибочные данные
                    if (row.HasErrors)
                    {
                        //Поставим фокус на ошибочную строку
                        gridView1.FocusedRowHandle = i;
                        gridView1.ShowEditor();
                        return false;
                    }
                }

                //сохраняем в постоянную таблицу
                DataSet resulExecute = DataProvider.SaveEmployeesBase();

                int countRow = resulExecute.Tables[0].Rows.Count;
                //если не было ошибок при сохранении то обновляем данные
                if (countRow == 0)
                {
                    //обновить данные
                    _baseOfEmployees = DataProvider.GetEmployeesBase(PeriodId, RegionIdLst);
                    gridControl1.DataSource = _baseOfEmployees;
                    gridView1.ActiveFilterCriteria = null;
                    return true;
                }

                //Получаем список ошибок
                DataTable errData = resulExecute.Tables[0];
                
                //Фильтруем ошибочные данные
                List<int> lst = errData.Rows.OfType<DataRow>()
                    .Select(dr => dr.Field<int>("Sector_id")).Distinct().ToList();
                gridView1.ActiveFilterCriteria = new InOperator("Sector_id", lst);

                for (int i = 0; i < errData.Rows.Count; i++)
                {
                    var rowErr = errData.Rows[i];
                    if (rowErr != null)
                    {
                        object[] findTheseVals = new object[2];
                        findTheseVals[0] = int.Parse(rowErr["Sector_id"].ToString());
                        findTheseVals[1] = int.Parse(rowErr["Period_id"].ToString());
                        string columnName = rowErr["ColumnName"].ToString();
                        string errMess = rowErr["FieldList"].ToString();

                        sourse.Rows.Find(findTheseVals).SetColumnError(columnName, errMess);
                    }
                }
                return false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            WaitManager.StopWait();
            return true;
        }

        /// <summary>
        /// Контроль на заполненность данных, при обнаружении ошибки устанавливает SetColumnError на row
        /// </summary>
        /// <param name="row">Проверяемая строка</param>
        /// <param name="errMessage">Сообщение об ошибке, если ошибки нет то строка пустая</param>
        /// <param name="errColumn">Имя столбца с ошибкой</param>
        private void ChekData(DataRow row, out string errMessage, out string errColumn)
        {
            errMessage = "";
            errColumn = "";
            AccessToApp accessToApp = AccessToApp.GetInstance();
            //Корректность полей
            foreach (DataColumn column in row.Table.Columns)
            {
                if (accessToApp.IsBaseEmplCenter)
                {
                    if (column.ColumnName == "WorkedDays_num")
                    {
                        if (string.IsNullOrEmpty(row[column.ColumnName].ToString()))
                        {
                            string strColumn = gridView1.Columns[column.ColumnName].Caption;
                            errMessage = "Поле '" + strColumn + "' не может быть пустое!";
                            errColumn = column.ColumnName;
                            row.SetColumnError(errColumn, errMessage);
                            return;
                        }
                    }
                }
                else
                {
                    if ((column.ColumnName == "ActiveFrom")
                        | (column.ColumnName == "WorkSchema")
                        | (column.ColumnName == "Salary1")
                        | (column.ColumnName == "ConstPart_sum")
                        | (column.ColumnName == "Amortization_norm")
                        | (column.ColumnName == "MileagePerMonth_fact")
                        | (column.ColumnName == "FuelConsumption_norm")
                        | (column.ColumnName == "Fuel_price")
                        | (column.ColumnName == "WorkDays_total")
                        | (column.ColumnName == "WorkedDays_num")
                        | (column.ColumnName == "AddCompensation")
                        | (column.ColumnName == "VariablePartClosingPerc_fact")
                        | (column.ColumnName == "CorrectVarPartPrevP_sum")
                        | (column.ColumnName == "Status")
                        )
                    {
                        if (string.IsNullOrEmpty(row[column.ColumnName].ToString()))
                        {
                            string strColumn = gridView1.Columns[column.ColumnName].Caption;
                            errMessage = "Поле '" + strColumn + "' не может быть пустое!";
                            errColumn = column.ColumnName;
                            row.SetColumnError(errColumn, errMessage);
                            return;
                        }
                    }
                }
                row.SetColumnError(column.ColumnName, null);
            }
        }

        /// <summary>
        /// Разукрашиваем в цвет при изменении данных
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">e</param>
        private void gridView1_RowCellStyle(object sender, RowCellStyleEventArgs e)
        {
            DataTable sourse = gridView1.GridControl.DataSource as DataTable;
            if (sourse == null) return;
            int idx = gridView1.GetDataSourceRowIndex(e.RowHandle);
            int maxRow = sourse.Rows.Count;
            if (idx >= 0 & idx < maxRow)
            {
                //Данные могуть только модифицироваться, но НЕ добавляться
                if (sourse.Rows[idx].RowState == DataRowState.Modified)
                {
                    e.Appearance.BackColor = Constants.ColorModified;
                }
                else
                {
                    GridView view = sender as GridView;
                    //красим не активные
                    if (view != null)
                    {
                        string notActive = view.GetRowCellDisplayText(e.RowHandle, view.Columns["Status"]);
                        if (notActive == "9")
                        {
                            e.Appearance.BackColor = Constants.ColorDiseble;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// При редактировании ячейки "передергиваем" строку для перерисовки
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">e</param>
        private void gridView1_CellValueChanged(object sender, CellValueChangedEventArgs e)
        {
            gridView1.PostEditor();
            gridView1.UpdateCurrentRow();
        }

        public bool IsDataValid
        {
            get
            {
                if (gridView1.ValidateEditor()) return true;
                return false;
            }
        }

        public bool HasChange
        {
            get
            {
                DataTable sourse = gridView1.GridControl.DataSource as DataTable;
                if (sourse != null)
                {
                    return sourse.Rows.Cast<DataRow>().Any(row => row.RowState == DataRowState.Modified);
                }
                return false;
            }
        }

        public void SetEditingMode(bool isOpened)
        {
            gridView1.OptionsBehavior.Editable = isOpened;
        }

        public bool SaveModifiedReportData()
        {
            return CheckAndSaveModifiedRow();
        }

        public bool ExportToExel(string path)
        {
            try
            {
                XlsxExportOptions option = new XlsxExportOptions { TextExportMode = TextExportMode.Text };
                gridView1.ExportToXlsx(path, option);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
            return true;
        }

        /// <summary>
        /// Пересчитать
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">e</param>
        private void sbReCalc_Click(object sender, EventArgs e)
        {
            WaitManager.StartWait();
            
            try
            {
                DataProvider.RecalcEmployeesBase(PeriodId);
                LoadData(_settings);
            }
            catch (Exception ex)
            {
                MessageBox.Show(@"Ошибка при расчетах данных: " + ex.Message);
            }

            WaitManager.StopWait();
            gridControl1.Focus();
        }

        private void sbLoadFromXLS_Click(object sender, EventArgs e)
        {
            _settings.Status = DataProvider.GetPeriodStatus(PeriodId);
            if (_settings.Status == 9)
            {
                XtraMessageBox.Show("Период '" + _settings.PeriodName + "' закрыт!", @"Период закрыт");
                return;
            }

            OpenFileDialog opFile = new OpenFileDialog()
            {
                Filter = @"xls files (*.xls)|*.xls;*.xlsx",
                RestoreDirectory = true
            };
            if (opFile.ShowDialog() == DialogResult.OK)
            {
                ReadDataFromExcel(opFile.FileName);
            }
            
        }

        /// <summary>
        /// Читаем данные их XLS
        /// </summary>
        /// <param name="path">путь к файлу</param>
        private void ReadDataFromExcel(string path)
        {
            _baseOfEmplFromXlsLst.BaseOfEmplFromXls.Clear();
            ExcelDataProvide excelDataProvide = new ExcelDataProvide(path, _baseOfEmplFromXlsLst);

            WaitManager.StartWait();
            try
            {
                excelDataProvide.TryToReadFile();
            }
            catch (IOException)
            {
                XtraMessageBox.Show("Файл уже открыт в другом приложении!", "Загрузка из EXCEL", MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
                WaitManager.StopWait();
                return;
            }
            catch (FormatException)
            {
                XtraMessageBox.Show("Некорректный формат файла! Нужно использовать xlsx, xls  форматы", "Загрузка данных",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
                WaitManager.StopWait();
                return;
            }

            //читаем собственно
            try
            {
                excelDataProvide.RaadFileData();
            }
            catch (Exception)
            {
                XtraMessageBox.Show("Некорректный формат файла!", "Загрузка из EXCEL", MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
                WaitManager.StopWait();
                return;
            }

            WaitManager.StopWait();
            if (excelDataProvide.IsStatusLoad)
            {
                ApllayChanges();
            }
        }

        private void ApllayChanges()
        {
            //Проверяем ошибки данных из таблицы лимитов, потому что там где читаем нет PeriodID
            foreach (BaseOfEmplFromXls baseOfEmplFromXlse in _baseOfEmplFromXlsLst.BaseOfEmplFromXls)
            {
                string filName = baseOfEmplFromXlse.ColumnFromXls.ColumnFildName;
                if (filName == "PublicTrCompensation_LimitID" | filName == "ConnectionCompensation_LimitID" |
                    filName == "FoodCompensation_LimitID" | filName == "StationeryCompensation_LimitID" |
                    filName == "ConnectionTabletCompensation_LimitID")
                {
                    if (!baseOfEmplFromXlse.HasError)
                    {
                        if (baseOfEmplFromXlse.Value != null)
                        {
                            string codeStr;
                            switch (filName)
                            {
                                case "PublicTrCompensation_LimitID":
                                    codeStr = "COMPESTRANSP";
                                    break;
                                case "ConnectionCompensation_LimitID":
                                    codeStr = "COMPESLINK";
                                    break;
                                case "FoodCompensation_LimitID":
                                    codeStr = "COMPESFOOD";
                                    break;
                                case "StationeryCompensation_LimitID":
                                    codeStr = "COMPESOFFICE";
                                    break;
                                case "ConnectionTabletCompensation_LimitID":
                                    codeStr = "COMPESLINKPAD";
                                    break;
                                default:
                                    codeStr = "-";
                                    break;
                            }
                            var idLimits = DataProvider.GetIdLimitsFromValue(PeriodId, codeStr,
                                (int) baseOfEmplFromXlse.Value);
                            if (idLimits == 0)
                            {
                                string mess = "Для значения столбца " + baseOfEmplFromXlse.Value +
                                                              " не могу найти id в базе!";
                                ValueCell valueCell = new ValueCell(null, mess);
                                baseOfEmplFromXlse.SetValue(valueCell);
                            }
                            else
                            {
                                ValueCell valueCell = new ValueCell(idLimits, "");
                                baseOfEmplFromXlse.SetValue(valueCell);
                            }
                        }
                    }
                }
            }

            string errorList = "";
            try
            {
                AccessToApp access = AccessToApp.GetInstance();

                WaitManager.StartWait();
                //как все готово применяем изменения собственно
                foreach (BaseOfEmplFromXls baseOfEmplFromXlse in _baseOfEmplFromXlsLst.BaseOfEmplFromXls)
                {
                    if (access.IsAdmin | access.IsReportEdit)
                    {
                        if (!baseOfEmplFromXlse.HasError)
                        {
                            int idxLoc = gridView1.LocateByValue("Sector_id", baseOfEmplFromXlse.KeyColumn.IdSector);
                            if (idxLoc != GridControl.InvalidRowHandle)
                            {
                                string status = gridView1.GetRowCellValue(idxLoc, "Status").ToString();
                                //не даем редактировать записи со статусом 9
                                if (status == "2")
                                {
                                    gridView1.SetRowCellValue(idxLoc, baseOfEmplFromXlse.ColumnFromXls.ColumnFildName,
                                        baseOfEmplFromXlse.Value);
                                }
                            }
                            else
                            {
                                if (!errorList.Contains(baseOfEmplFromXlse.KeyColumn.IdSector.ToString()))
                                {
                                    errorList = errorList + "Строка с 'ID сектора' - " +
                                                baseOfEmplFromXlse.KeyColumn.IdSector +
                                                ", 'Код сектора' - " +
                                                baseOfEmplFromXlse.KeyColumn.CodeSector + " не найдена на листе!" +
                                                Environment.NewLine;
                                }
                            }
                        }
                        else
                        {
                            errorList = errorList + "Строка с 'ID сектора' - " + baseOfEmplFromXlse.KeyColumn.IdSector +
                                        "; 'Код сектора' - " +
                                        baseOfEmplFromXlse.KeyColumn.CodeSector + "; В столбце: '" +
                                        baseOfEmplFromXlse.ColumnFromXls.ColumnName + "' ошибка - "
                                        + baseOfEmplFromXlse.ErrorMes + Environment.NewLine;
                        }
                    }
                    if (access.IsBaseEmplCenter)
                    {
                        if (!baseOfEmplFromXlse.HasError)
                        {
                            int idxLoc = gridView1.LocateByValue("Sector_id", baseOfEmplFromXlse.KeyColumn.IdSector);
                            if (idxLoc != GridControl.InvalidRowHandle)
                            {
                                string status = gridView1.GetRowCellValue(idxLoc, "Status").ToString();
                                //не даем редактировать записи со статусом 9
                                if (status == "2")
                                {
                                    if (baseOfEmplFromXlse.ColumnFromXls.ColumnFildName == "WorkedDays_num")
                                    {
                                        gridView1.SetRowCellValue(idxLoc,
                                            baseOfEmplFromXlse.ColumnFromXls.ColumnFildName,
                                            baseOfEmplFromXlse.Value);
                                    }
                                }
                            }
                            else
                            {
                                if (!errorList.Contains(baseOfEmplFromXlse.KeyColumn.IdSector.ToString()))
                                {
                                    errorList = errorList + "Строка с 'ID сектора' - " +
                                                baseOfEmplFromXlse.KeyColumn.IdSector +
                                                ", 'Код сектора' - " +
                                                baseOfEmplFromXlse.KeyColumn.CodeSector + " не найдена на листе!" +
                                                Environment.NewLine;
                                }
                            }
                        }
                        else
                        {
                            errorList = errorList + "Строка с 'ID сектора' - " + baseOfEmplFromXlse.KeyColumn.IdSector +
                                        "; 'Код сектора' - " +
                                        baseOfEmplFromXlse.KeyColumn.CodeSector + "; В столбце: '" +
                                        baseOfEmplFromXlse.ColumnFromXls.ColumnName + "' ошибка - "
                                        + baseOfEmplFromXlse.ErrorMes + Environment.NewLine;
                        }
                    }
                }
                WaitManager.StopWait();

                if (string.IsNullOrEmpty(errorList)) errorList = "Все записи загружены успешно!";
                ShowErrorForm errForm = new ShowErrorForm {ErrorList = errorList};
                errForm.ShowDialog();
                WaitManager.StopWait();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void bRestore_Click(object sender, EventArgs e)
        {
            try
            {
                gridView1.RestoreLayoutFromStream(_layoutToMem);
                _layoutToMem.Seek(0, SeekOrigin.Begin);
                gridView1.BestFitColumns();
                Access();
            }
            catch (Exception)
            {
                // ignored
            }
            gridControl1.Focus();
        }

        private void gridView1_ShowingEditor(object sender, System.ComponentModel.CancelEventArgs e)
        {
            //Не даем редактировать записи со статусом = 9
            GridView view = sender as GridView;
            if (view != null)
            {
                if (view.FocusedColumn.FieldName != "Status")
                {
                    string cellValue = view.GetRowCellValue(view.FocusedRowHandle, "Status").ToString();
                    if (cellValue == "9")
                        e.Cancel = true;
                }
                else
                {
                    AccessToApp access = AccessToApp.GetInstance();
                    if (!access.IsAdmin)
                    {
                        e.Cancel = true;
                    }
                }
            }
        }
    }
}
