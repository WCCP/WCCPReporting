﻿namespace SoftServe.Reports.AGNTProject.Controls
{
    partial class ControlCostsFact
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gcpart = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcpYear = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gc = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcWorkDays_total = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcWorkedDays_num = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcSector_code = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcSector_name = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcUserType_ShortName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcActiveFrom = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcDSM = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcCity = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcPartner = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcSalary = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcConstPartFact = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcVariablePartClosingPerc_fact = new DevExpress.XtraGrid.Columns.GridColumn();
            this.teVariablePartClosingPerc_fact = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gcVariablePart_sum = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcFuelCompensation_sumWt = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcFuelCompensationFact = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcAmortization_sum = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcAmortizationFact = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcPublicTrCompensation = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcConnectionCompensation = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcConnectionCompensFact = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcConnectionTabletCompensFact = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcFoodCompensation = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcFoodCompensFact = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcAddCompensation = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcCorrectVarPartPrevP_sum = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcAllSumWithoutPay = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcRegion_name = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcStationeryCompensation = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcVacationPayment = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcAdministration_sum = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcUST = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcPaymentWithPartners = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcAllSumWithoutFuel = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcAllSum = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teVariablePartClosingPerc_fact)).BeginInit();
            this.SuspendLayout();
            // 
            // gridControl1
            // 
            this.gridControl1.Cursor = System.Windows.Forms.Cursors.Default;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.Location = new System.Drawing.Point(0, 0);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.teVariablePartClosingPerc_fact});
            this.gridControl1.Size = new System.Drawing.Size(1399, 731);
            this.gridControl1.TabIndex = 0;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gcpart,
            this.gcpYear,
            this.gc,
            this.gcWorkDays_total,
            this.gcWorkedDays_num,
            this.gcSector_code,
            this.gcSector_name,
            this.gcUserType_ShortName,
            this.gcActiveFrom,
            this.gcDSM,
            this.gcCity,
            this.gcPartner,
            this.gcSalary,
            this.gcConstPartFact,
            this.gcVariablePartClosingPerc_fact,
            this.gcVariablePart_sum,
            this.gcFuelCompensation_sumWt,
            this.gcFuelCompensationFact,
            this.gcAmortization_sum,
            this.gcAmortizationFact,
            this.gcPublicTrCompensation,
            this.gcConnectionCompensation,
            this.gcConnectionCompensFact,
            this.gcConnectionTabletCompensFact,
            this.gcFoodCompensation,
            this.gcFoodCompensFact,
            this.gcAddCompensation,
            this.gcCorrectVarPartPrevP_sum,
            this.gcAllSumWithoutPay,
            this.gcRegion_name,
            this.gcStationeryCompensation,
            this.gcVacationPayment,
            this.gcAdministration_sum,
            this.gcUST,
            this.gcPaymentWithPartners,
            this.gcAllSumWithoutFuel,
            this.gcAllSum});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.GroupSummary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Salary", this.gcSalary, "(ИТОГО: {0})"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "ConstPartFact", this.gcConstPartFact, "(ИТОГО: {0})"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "VariablePart_sum", this.gcVariablePart_sum, "(ИТОГО: {0})"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "FuelCompensation_sumWt", this.gcFuelCompensation_sumWt, "(ИТОГО: {0})"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "FuelCompensationFact", this.gcFuelCompensationFact, "(ИТОГО: {0})"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Amortization_sum", this.gcAmortization_sum, "(ИТОГО: {0})"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "AmortizationFact", this.gcAmortizationFact, "(ИТОГО: {0})"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "PublicTrCompensation", this.gcPublicTrCompensation, "(ИТОГО: {0})"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "ConnectionCompensation", this.gcConnectionCompensation, "(ИТОГО: {0})"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "ConnectionCompensFact", this.gcConnectionCompensFact, "(ИТОГО: {0})"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "ConnectionTabletCompensFact", this.gcConnectionTabletCompensFact, "(ИТОГО: {0})"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "FoodCompensation", this.gcFoodCompensation, "(ИТОГО: {0})"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "FoodCompensFact", this.gcFoodCompensFact, "(ИТОГО: {0})"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "AddCompensation", this.gcAddCompensation, "(ИТОГО: {0})"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "CorrectVarPartPrevP_sum", this.gcCorrectVarPartPrevP_sum, "(ИТОГО: {0})"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "AllSumWithoutPay", this.gcAllSumWithoutPay, "(ИТОГО: {0})"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "StationeryCompensation", this.gcStationeryCompensation, "(ИТОГО: {0})"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "VacationPayment", this.gcVacationPayment, "(ИТОГО: {0})"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Administration_sum", this.gcAdministration_sum, "(ИТОГО: {0})"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "UST", this.gcUST, "(ИТОГО: {0})"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "PaymentWithPartners", this.gcPaymentWithPartners, "(ИТОГО: {0})"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "AllSumWithoutFuel", this.gcAllSumWithoutFuel, "(ИТОГО: {0})"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "AllSum", this.gcAllSum, "(ИТОГО: {0})")});
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsBehavior.Editable = false;
            this.gridView1.OptionsFind.ShowFindButton = false;
            this.gridView1.OptionsPrint.AutoWidth = false;
            this.gridView1.OptionsSelection.MultiSelect = true;
            this.gridView1.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CellSelect;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.ColumnHeaderAutoHeight = DevExpress.Utils.DefaultBoolean.True;
            this.gridView1.OptionsView.ShowFooter = true;
            // 
            // gcpart
            // 
            this.gcpart.Caption = "part";
            this.gcpart.FieldName = "part";
            this.gcpart.Name = "gcpart";
            this.gcpart.OptionsColumn.ShowInCustomizationForm = false;
            // 
            // gcpYear
            // 
            this.gcpYear.Caption = "Год";
            this.gcpYear.FieldName = "pYear";
            this.gcpYear.Name = "gcpYear";
            this.gcpYear.Visible = true;
            this.gcpYear.VisibleIndex = 0;
            // 
            // gc
            // 
            this.gc.Caption = "Месяц";
            this.gc.FieldName = "MonthNameRu";
            this.gc.Name = "gc";
            this.gc.Visible = true;
            this.gc.VisibleIndex = 1;
            // 
            // gcWorkDays_total
            // 
            this.gcWorkDays_total.Caption = "Кол-во рабочих дней";
            this.gcWorkDays_total.FieldName = "WorkDays_total";
            this.gcWorkDays_total.Name = "gcWorkDays_total";
            this.gcWorkDays_total.Visible = true;
            this.gcWorkDays_total.VisibleIndex = 2;
            // 
            // gcWorkedDays_num
            // 
            this.gcWorkedDays_num.Caption = "Кол-во отработанных дней";
            this.gcWorkedDays_num.FieldName = "WorkedDays_num";
            this.gcWorkedDays_num.Name = "gcWorkedDays_num";
            this.gcWorkedDays_num.Visible = true;
            this.gcWorkedDays_num.VisibleIndex = 3;
            // 
            // gcSector_code
            // 
            this.gcSector_code.Caption = "Код сектора";
            this.gcSector_code.FieldName = "Sector_code";
            this.gcSector_code.Name = "gcSector_code";
            this.gcSector_code.Visible = true;
            this.gcSector_code.VisibleIndex = 4;
            // 
            // gcSector_name
            // 
            this.gcSector_name.Caption = "Название сектора";
            this.gcSector_name.FieldName = "Sector_name";
            this.gcSector_name.Name = "gcSector_name";
            this.gcSector_name.Visible = true;
            this.gcSector_name.VisibleIndex = 5;
            // 
            // gcUserType_ShortName
            // 
            this.gcUserType_ShortName.Caption = "Тип сектора";
            this.gcUserType_ShortName.FieldName = "UserType_ShortName";
            this.gcUserType_ShortName.Name = "gcUserType_ShortName";
            this.gcUserType_ShortName.Visible = true;
            this.gcUserType_ShortName.VisibleIndex = 6;
            // 
            // gcActiveFrom
            // 
            this.gcActiveFrom.Caption = "Активен с";
            this.gcActiveFrom.FieldName = "ActiveFrom";
            this.gcActiveFrom.Name = "gcActiveFrom";
            this.gcActiveFrom.Visible = true;
            this.gcActiveFrom.VisibleIndex = 7;
            // 
            // gcDSM
            // 
            this.gcDSM.Caption = "DSM";
            this.gcDSM.FieldName = "DSM";
            this.gcDSM.Name = "gcDSM";
            this.gcDSM.Visible = true;
            this.gcDSM.VisibleIndex = 8;
            // 
            // gcCity
            // 
            this.gcCity.Caption = "Город";
            this.gcCity.FieldName = "City";
            this.gcCity.Name = "gcCity";
            this.gcCity.Visible = true;
            this.gcCity.VisibleIndex = 9;
            // 
            // gcPartner
            // 
            this.gcPartner.Caption = "Партнер";
            this.gcPartner.FieldName = "Partner";
            this.gcPartner.Name = "gcPartner";
            this.gcPartner.Visible = true;
            this.gcPartner.VisibleIndex = 10;
            // 
            // gcSalary
            // 
            this.gcSalary.Caption = "Постоянная часть, сумма";
            this.gcSalary.FieldName = "Salary";
            this.gcSalary.Name = "gcSalary";
            this.gcSalary.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Salary", "ИТОГО: {0}")});
            this.gcSalary.Visible = true;
            this.gcSalary.VisibleIndex = 11;
            // 
            // gcConstPartFact
            // 
            this.gcConstPartFact.Caption = "Постоянная часть, факт";
            this.gcConstPartFact.FieldName = "ConstPartFact";
            this.gcConstPartFact.Name = "gcConstPartFact";
            this.gcConstPartFact.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "ConstPartFact", "ИТОГО: {0}")});
            this.gcConstPartFact.Visible = true;
            this.gcConstPartFact.VisibleIndex = 12;
            // 
            // gcVariablePartClosingPerc_fact
            // 
            this.gcVariablePartClosingPerc_fact.Caption = "Фактическое закрытие бонуса, %";
            this.gcVariablePartClosingPerc_fact.ColumnEdit = this.teVariablePartClosingPerc_fact;
            this.gcVariablePartClosingPerc_fact.FieldName = "VariablePartClosingPerc_fact";
            this.gcVariablePartClosingPerc_fact.Name = "gcVariablePartClosingPerc_fact";
            this.gcVariablePartClosingPerc_fact.Visible = true;
            this.gcVariablePartClosingPerc_fact.VisibleIndex = 13;
            // 
            // teVariablePartClosingPerc_fact
            // 
            this.teVariablePartClosingPerc_fact.AutoHeight = false;
            this.teVariablePartClosingPerc_fact.Mask.EditMask = "P0";
            this.teVariablePartClosingPerc_fact.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.teVariablePartClosingPerc_fact.Mask.UseMaskAsDisplayFormat = true;
            this.teVariablePartClosingPerc_fact.Name = "teVariablePartClosingPerc_fact";
            // 
            // gcVariablePart_sum
            // 
            this.gcVariablePart_sum.Caption = "Переменная часть, сумма";
            this.gcVariablePart_sum.FieldName = "VariablePart_sum";
            this.gcVariablePart_sum.Name = "gcVariablePart_sum";
            this.gcVariablePart_sum.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "VariablePart_sum", "ИТОГО: {0}")});
            this.gcVariablePart_sum.Visible = true;
            this.gcVariablePart_sum.VisibleIndex = 14;
            // 
            // gcFuelCompensation_sumWt
            // 
            this.gcFuelCompensation_sumWt.Caption = "ГСМ лимиты";
            this.gcFuelCompensation_sumWt.FieldName = "FuelCompensation_sumWt";
            this.gcFuelCompensation_sumWt.Name = "gcFuelCompensation_sumWt";
            this.gcFuelCompensation_sumWt.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "FuelCompensation_sumWt", "ИТОГО: {0}")});
            this.gcFuelCompensation_sumWt.Visible = true;
            this.gcFuelCompensation_sumWt.VisibleIndex = 15;
            // 
            // gcFuelCompensationFact
            // 
            this.gcFuelCompensationFact.Caption = "ГСМ факт";
            this.gcFuelCompensationFact.FieldName = "FuelCompensationFact";
            this.gcFuelCompensationFact.Name = "gcFuelCompensationFact";
            this.gcFuelCompensationFact.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "FuelCompensationFact", "ИТОГО: {0}")});
            this.gcFuelCompensationFact.Visible = true;
            this.gcFuelCompensationFact.VisibleIndex = 16;
            // 
            // gcAmortization_sum
            // 
            this.gcAmortization_sum.Caption = "Амортизация лимит";
            this.gcAmortization_sum.FieldName = "Amortization_sum";
            this.gcAmortization_sum.Name = "gcAmortization_sum";
            this.gcAmortization_sum.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Amortization_sum", "ИТОГО: {0}")});
            this.gcAmortization_sum.Visible = true;
            this.gcAmortization_sum.VisibleIndex = 17;
            // 
            // gcAmortizationFact
            // 
            this.gcAmortizationFact.Caption = "Амортизация факт";
            this.gcAmortizationFact.FieldName = "AmortizationFact";
            this.gcAmortizationFact.Name = "gcAmortizationFact";
            this.gcAmortizationFact.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "AmortizationFact", "ИТОГО: {0}")});
            this.gcAmortizationFact.Visible = true;
            this.gcAmortizationFact.VisibleIndex = 18;
            // 
            // gcPublicTrCompensation
            // 
            this.gcPublicTrCompensation.Caption = "Компенсация общественного транспорта, сумма";
            this.gcPublicTrCompensation.FieldName = "PublicTrCompensation";
            this.gcPublicTrCompensation.Name = "gcPublicTrCompensation";
            this.gcPublicTrCompensation.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "PublicTrCompensation", "ИТОГО: {0}")});
            this.gcPublicTrCompensation.Visible = true;
            this.gcPublicTrCompensation.VisibleIndex = 19;
            // 
            // gcConnectionCompensation
            // 
            this.gcConnectionCompensation.Caption = "Телефон лимит";
            this.gcConnectionCompensation.FieldName = "ConnectionCompensation";
            this.gcConnectionCompensation.Name = "gcConnectionCompensation";
            this.gcConnectionCompensation.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "ConnectionCompensation", "ИТОГО: {0}")});
            this.gcConnectionCompensation.Visible = true;
            this.gcConnectionCompensation.VisibleIndex = 20;
            // 
            // gcConnectionCompensFact
            // 
            this.gcConnectionCompensFact.Caption = "Телефон факт";
            this.gcConnectionCompensFact.FieldName = "ConnectionCompensFact";
            this.gcConnectionCompensFact.Name = "gcConnectionCompensFact";
            this.gcConnectionCompensFact.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "ConnectionCompensFact", "ИТОГО: {0}")});
            this.gcConnectionCompensFact.Visible = true;
            this.gcConnectionCompensFact.VisibleIndex = 21;
            // 
            // gcConnectionTabletCompensFact
            // 
            this.gcConnectionTabletCompensFact.Caption = "Связь для планшетов (факт)";
            this.gcConnectionTabletCompensFact.FieldName = "ConnectionTabletCompensFact";
            this.gcConnectionTabletCompensFact.Name = "gcConnectionTabletCompensFact";
            this.gcConnectionTabletCompensFact.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "ConnectionTabletCompensFact", "ИТОГО: {0}")});
            this.gcConnectionTabletCompensFact.Visible = true;
            this.gcConnectionTabletCompensFact.VisibleIndex = 22;
            // 
            // gcFoodCompensation
            // 
            this.gcFoodCompensation.Caption = "Питание лимит";
            this.gcFoodCompensation.FieldName = "FoodCompensation";
            this.gcFoodCompensation.Name = "gcFoodCompensation";
            this.gcFoodCompensation.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "FoodCompensation", "ИТОГО: {0}")});
            this.gcFoodCompensation.Visible = true;
            this.gcFoodCompensation.VisibleIndex = 23;
            // 
            // gcFoodCompensFact
            // 
            this.gcFoodCompensFact.Caption = "Питание факт";
            this.gcFoodCompensFact.FieldName = "FoodCompensFact";
            this.gcFoodCompensFact.Name = "gcFoodCompensFact";
            this.gcFoodCompensFact.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "FoodCompensFact", "ИТОГО: {0}")});
            this.gcFoodCompensFact.Visible = true;
            this.gcFoodCompensFact.VisibleIndex = 24;
            // 
            // gcAddCompensation
            // 
            this.gcAddCompensation.Caption = "Дополнительные Компенсации";
            this.gcAddCompensation.FieldName = "AddCompensation";
            this.gcAddCompensation.Name = "gcAddCompensation";
            this.gcAddCompensation.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "AddCompensation", "ИТОГО: {0}")});
            this.gcAddCompensation.Visible = true;
            this.gcAddCompensation.VisibleIndex = 25;
            // 
            // gcCorrectVarPartPrevP_sum
            // 
            this.gcCorrectVarPartPrevP_sum.Caption = "Корректировка переменной части предыдущего периода, сумма";
            this.gcCorrectVarPartPrevP_sum.FieldName = "CorrectVarPartPrevP_sum";
            this.gcCorrectVarPartPrevP_sum.Name = "gcCorrectVarPartPrevP_sum";
            this.gcCorrectVarPartPrevP_sum.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "CorrectVarPartPrevP_sum", "ИТОГО: {0}")});
            this.gcCorrectVarPartPrevP_sum.Visible = true;
            this.gcCorrectVarPartPrevP_sum.VisibleIndex = 26;
            // 
            // gcAllSumWithoutPay
            // 
            this.gcAllSumWithoutPay.Caption = "Итого начислено (до вычетов)";
            this.gcAllSumWithoutPay.FieldName = "AllSumWithoutPay";
            this.gcAllSumWithoutPay.Name = "gcAllSumWithoutPay";
            this.gcAllSumWithoutPay.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "AllSumWithoutPay", "ИТОГО: {0}")});
            this.gcAllSumWithoutPay.Visible = true;
            this.gcAllSumWithoutPay.VisibleIndex = 27;
            // 
            // gcRegion_name
            // 
            this.gcRegion_name.Caption = "Регион";
            this.gcRegion_name.FieldName = "Region_name";
            this.gcRegion_name.Name = "gcRegion_name";
            this.gcRegion_name.Visible = true;
            this.gcRegion_name.VisibleIndex = 28;
            // 
            // gcStationeryCompensation
            // 
            this.gcStationeryCompensation.Caption = "Компенсация канцелярии, сумма";
            this.gcStationeryCompensation.FieldName = "StationeryCompensation";
            this.gcStationeryCompensation.Name = "gcStationeryCompensation";
            this.gcStationeryCompensation.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "StationeryCompensation", "ИТОГО: {0}")});
            this.gcStationeryCompensation.Visible = true;
            this.gcStationeryCompensation.VisibleIndex = 29;
            // 
            // gcVacationPayment
            // 
            this.gcVacationPayment.Caption = "Отпускные, сумма";
            this.gcVacationPayment.FieldName = "VacationPayment";
            this.gcVacationPayment.Name = "gcVacationPayment";
            this.gcVacationPayment.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "VacationPayment", "ИТОГО: {0}")});
            this.gcVacationPayment.Visible = true;
            this.gcVacationPayment.VisibleIndex = 30;
            // 
            // gcAdministration_sum
            // 
            this.gcAdministration_sum.Caption = "Администрирование, сумма";
            this.gcAdministration_sum.FieldName = "Administration_sum";
            this.gcAdministration_sum.Name = "gcAdministration_sum";
            this.gcAdministration_sum.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Administration_sum", "ИТОГО: {0}")});
            this.gcAdministration_sum.Visible = true;
            this.gcAdministration_sum.VisibleIndex = 31;
            // 
            // gcUST
            // 
            this.gcUST.Caption = "ЕСН, сумма";
            this.gcUST.FieldName = "UST";
            this.gcUST.Name = "gcUST";
            this.gcUST.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "UST", "ИТОГО: {0}")});
            this.gcUST.Visible = true;
            this.gcUST.VisibleIndex = 32;
            // 
            // gcPaymentWithPartners
            // 
            this.gcPaymentWithPartners.Caption = "Расчеты с партнерами, сумма";
            this.gcPaymentWithPartners.FieldName = "PaymentWithPartners";
            this.gcPaymentWithPartners.Name = "gcPaymentWithPartners";
            this.gcPaymentWithPartners.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "PaymentWithPartners", "ИТОГО: {0}")});
            this.gcPaymentWithPartners.Visible = true;
            this.gcPaymentWithPartners.VisibleIndex = 33;
            // 
            // gcAllSumWithoutFuel
            // 
            this.gcAllSumWithoutFuel.Caption = "Итого выплата сектору без топлива";
            this.gcAllSumWithoutFuel.FieldName = "AllSumWithoutFuel";
            this.gcAllSumWithoutFuel.Name = "gcAllSumWithoutFuel";
            this.gcAllSumWithoutFuel.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "AllSumWithoutFuel", "ИТОГО: {0}")});
            this.gcAllSumWithoutFuel.Visible = true;
            this.gcAllSumWithoutFuel.VisibleIndex = 34;
            // 
            // gcAllSum
            // 
            this.gcAllSum.Caption = "Итого затраты";
            this.gcAllSum.FieldName = "AllSum";
            this.gcAllSum.Name = "gcAllSum";
            this.gcAllSum.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "AllSum", "ИТОГО: {0}")});
            this.gcAllSum.Visible = true;
            this.gcAllSum.VisibleIndex = 35;
            // 
            // ControlCostsFact
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gridControl1);
            this.Name = "ControlCostsFact";
            this.Size = new System.Drawing.Size(1399, 731);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teVariablePartClosingPerc_fact)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn gcpart;
        private DevExpress.XtraGrid.Columns.GridColumn gcpYear;
        private DevExpress.XtraGrid.Columns.GridColumn gc;
        private DevExpress.XtraGrid.Columns.GridColumn gcWorkDays_total;
        private DevExpress.XtraGrid.Columns.GridColumn gcWorkedDays_num;
        private DevExpress.XtraGrid.Columns.GridColumn gcSector_code;
        private DevExpress.XtraGrid.Columns.GridColumn gcSector_name;
        private DevExpress.XtraGrid.Columns.GridColumn gcUserType_ShortName;
        private DevExpress.XtraGrid.Columns.GridColumn gcActiveFrom;
        private DevExpress.XtraGrid.Columns.GridColumn gcDSM;
        private DevExpress.XtraGrid.Columns.GridColumn gcCity;
        private DevExpress.XtraGrid.Columns.GridColumn gcPartner;
        private DevExpress.XtraGrid.Columns.GridColumn gcSalary;
        private DevExpress.XtraGrid.Columns.GridColumn gcConstPartFact;
        private DevExpress.XtraGrid.Columns.GridColumn gcVariablePartClosingPerc_fact;
        private DevExpress.XtraGrid.Columns.GridColumn gcVariablePart_sum;
        private DevExpress.XtraGrid.Columns.GridColumn gcFuelCompensation_sumWt;
        private DevExpress.XtraGrid.Columns.GridColumn gcFuelCompensationFact;
        private DevExpress.XtraGrid.Columns.GridColumn gcAmortization_sum;
        private DevExpress.XtraGrid.Columns.GridColumn gcAmortizationFact;
        private DevExpress.XtraGrid.Columns.GridColumn gcPublicTrCompensation;
        private DevExpress.XtraGrid.Columns.GridColumn gcConnectionCompensation;
        private DevExpress.XtraGrid.Columns.GridColumn gcConnectionCompensFact;
        private DevExpress.XtraGrid.Columns.GridColumn gcConnectionTabletCompensFact;
        private DevExpress.XtraGrid.Columns.GridColumn gcFoodCompensation;
        private DevExpress.XtraGrid.Columns.GridColumn gcFoodCompensFact;
        private DevExpress.XtraGrid.Columns.GridColumn gcAddCompensation;
        private DevExpress.XtraGrid.Columns.GridColumn gcCorrectVarPartPrevP_sum;
        private DevExpress.XtraGrid.Columns.GridColumn gcAllSumWithoutPay;
        private DevExpress.XtraGrid.Columns.GridColumn gcRegion_name;
        private DevExpress.XtraGrid.Columns.GridColumn gcStationeryCompensation;
        private DevExpress.XtraGrid.Columns.GridColumn gcVacationPayment;
        private DevExpress.XtraGrid.Columns.GridColumn gcAdministration_sum;
        private DevExpress.XtraGrid.Columns.GridColumn gcUST;
        private DevExpress.XtraGrid.Columns.GridColumn gcPaymentWithPartners;
        private DevExpress.XtraGrid.Columns.GridColumn gcAllSumWithoutFuel;
        private DevExpress.XtraGrid.Columns.GridColumn gcAllSum;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit teVariablePartClosingPerc_fact;
    }
}
