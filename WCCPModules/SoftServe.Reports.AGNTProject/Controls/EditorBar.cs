﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace SoftServe.Reports.AGNTProject.Controls
{
    public partial class EditorBar : UserControl
    {
        public SimpleButton BtnAddRow
        {
            get { return btnAddRow; }
        }
        public SimpleButton BtnActiv
        {
            get { return btnActiv; }            
        }

        public SimpleButton BtnDeactiv
        {
            get { return btnDeactiv; }            
        }

        public SimpleButton BtnSave
        {
            get { return btnSave; }            
        }

        public EditorBar()
        {
            InitializeComponent();
        }

        public void SetCaption(string captoin)
        {
            lblCaption.Text = captoin;
        }
     

        public void EnableEditButtons(bool enable)
        {
            btnAddRow.Enabled = 
            btnActiv.Enabled =
            btnDeactiv.Enabled = enable;
        }
        public void SetBtnAddRowVisible(bool visible)
        {
            btnAddRow.Visible = visible;
            if (!visible)
            {                
                tableLayoutPanel1.SetColumn(btnActiv, 0);
                tableLayoutPanel1.SetColumn(btnDeactiv, 1);
            }
            else
            {               
                tableLayoutPanel1.SetColumn(btnAddRow, 0);
                tableLayoutPanel1.SetColumn(btnActiv, 1);
                tableLayoutPanel1.SetColumn(btnDeactiv, 2);                                
            }
        }
    }
}
