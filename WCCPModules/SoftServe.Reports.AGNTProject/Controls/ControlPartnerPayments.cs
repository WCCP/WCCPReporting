﻿using System;
using System.Data;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraPrinting;
using SoftServe.Reports.AGNTProject.DataAccess;
using SoftServe.Reports.AGNTProject.Models;
using SoftServe.Reports.AGNTProject.Utils;

namespace SoftServe.Reports.AGNTProject.Controls
{
    public partial class ControlPartnerPayments : UserControl, ITabReport, IEditableReport, IExportData
    {

        private DataTable _partnerPayments;
        private int _paramQuery;
        private Settings _settings;
        public ControlPartnerPayments()
        {
            InitializeComponent();
            ReportText = Constants.CaptionPaymentWithPartner;
        }

        public string ReportText { get; set; }

        private int PerioId { get; set; }
        private string RegionIdLst { get; set; }
        private DateTime LastOpenPeriod {
            get { return DataProvider.LastOpenPeriod(PerioId); }
        }

        private string EditLayoutFileName(string path)
        {
            string fileName = "ControlPartnerPayments_" + Path.GetFileName(path);
            string pathToDir = Path.GetDirectoryName(path);
            if (pathToDir != null) return Path.Combine(pathToDir, fileName);
            return path;
        }

        public void LoadData(Settings settings)
        {
            _settings = settings;
            try
            {
                PerioId = _settings.PeriodId;
                RegionIdLst = "";
                foreach (string s in _settings.RegionIDsList)
                {
                    RegionIdLst = RegionIdLst + s + ",";
                }
                //убираем последнюю запятую
                RegionIdLst = RegionIdLst.Remove(RegionIdLst.Length - 1);
                _paramQuery = Convert.ToInt32(_settings.SelectedReports[Constants.CaptionPaymentWithPartner]);
            }
            catch (Exception ex)
            {
                throw new ArgumentException("Ошибка получения парамтров:" + ex.Message);
            }

            _partnerPayments = DataProvider.GetPartnerPayments(_paramQuery, PerioId, RegionIdLst);
            gridControl1.DataSource = _partnerPayments;

            //Доступ
            Access();

            lueDistr.DataSource = DataProvider.GetDistrib(PerioId, null, "");
            lurRegion.DataSource = DataProvider.GetRegionDistrib(null, null);
            lueAgentPayment.DataSource = DataProvider.GetAgentPaymentTypesDistrib(null);

            if (!SaveFilter)
            {
                gridView1.ActiveFilterCriteria = null;
                gridView1.ApplyFindFilter("");
                gridView1.FindPanelVisible = false;
                gridView1.OptionsView.ShowAutoFilterRow = false;
            }
            SaveFilter = false;
        }

        private void Access()
        {
            AccessToApp accessToApp = AccessToApp.GetInstance();
            if (accessToApp.IsAdmin) return;
            if (accessToApp.IsBaseEmplCenter)
            {
                throw new ApplicationException("Вы не имеете доступа к данной вкладке");
            }
            if (accessToApp.IsReport|accessToApp.IsReportEdit)
            {
                foreach (GridColumn column in gridView1.Columns)
                {
                    column.OptionsColumn.AllowEdit = false;
                }
            }
        }

        public void SaveLayout(string path)
        {
            //Добавим к имени имя компонента
            string fullPath = EditLayoutFileName(path);
            gridView1.SaveLayoutToXml(fullPath);
        }
        public void RestoreLayout(string path)
        {
            //Добавим к имени имя компонента
            string fullPath = EditLayoutFileName(path);
            if (File.Exists(fullPath))
            {
                gridView1.RestoreLayoutFromXml(fullPath);
            }
        }

        public bool SaveFilter { get; set; }

        private void LueDistrOnEditValueChanging(object sender, ChangingEventArgs changingEventArgs)
        {
            //при смене дистра на столбце, меняем его код
            gridView1.SetRowCellValue(gridView1.FocusedRowHandle, gridView1.Columns["distr_id2"],
                changingEventArgs.NewValue);
            //при смене дистра очистим поле регион
            gridView1.SetRowCellValue(gridView1.FocusedRowHandle, gridView1.Columns["Region_id"], null);
        }

        /// <summary>
        /// Кнопка "Сохранить"
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">EventArgs</param>
        private void simpleButton1_Click(object sender, EventArgs e)
        {
            var result = SaveData();
            if (!result)
            {
                splitContainerControl1.Text = @"Error!";
            }
        }

        /// <summary>
        /// Кнопка "Добавить"
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">EventArgs</param>
        private void simpleButton2_Click(object sender, EventArgs e)
        {
            DataTable sourse = gridView1.GridControl.DataSource as DataTable;
            if (sourse == null) throw new NullReferenceException("Ошибка определения источника данных!");

            gridView1.BeginUpdate();
            sourse.Rows.Add(sourse.NewRow());
            gridView1.RefreshData();
            gridView1.EndUpdate();
        }

        private void gridView1_ShownEditor(object sender, EventArgs e)
        {
            ColumnView view = (ColumnView)sender;
            int showOnlyOpen = 2;
            //Фильтруем lurRegion в зависимости от текущего региона
            if (view.FocusedColumn.FieldName == "Region_id")
            {
                //Если включен автофильтр, то наполнение включаем все
                if (view.FocusedRowHandle != GridControl.AutoFilterRowHandle)
                {
                    var distrIdStr = view.GetFocusedRowCellValue("distr_id").ToString();
                    int distrId;
                    bool result = int.TryParse(distrIdStr, out distrId);
                    if (result)
                    {
                        LookUpEdit editor = (LookUpEdit)view.ActiveEditor;
                        editor.Properties.DataSource = DataProvider.GetRegionDistrib(distrId, showOnlyOpen);
                    }
                    //если нет distr_id ничего не показываем
                    else
                    {
                        LookUpEdit editor = (LookUpEdit)view.ActiveEditor;
                        editor.Properties.DataSource = null;
                    }
                }
            }
            //Фильтруем lueAgentPayment в зависимости от статуса  "Вид агентского платежа"
            if (view.FocusedColumn.FieldName == "agentPayment_id")
            {
                //Если включен автофильтр, то наполнение включаем все
                if (view.FocusedRowHandle != GridControl.AutoFilterRowHandle)
                {
                    LookUpEdit editor = (LookUpEdit)view.ActiveEditor;
                    editor.Properties.DataSource = DataProvider.GetAgentPaymentTypesDistrib(showOnlyOpen);
                }
            }
            //Фильтруем lueDistr в зависимости от статуса  Дистра
            if (view.FocusedColumn.FieldName == "distr_id")
            {
                if (view.FocusedRowHandle != GridControl.AutoFilterRowHandle)
                {
                    LookUpEdit editor = (LookUpEdit)view.ActiveEditor;
                    editor.Properties.DataSource = DataProvider.GetDistrib(PerioId, showOnlyOpen, RegionIdLst);
                }
            }
        }

        /// <summary>
        /// Проверяем валидность данных
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">e</param>
        private void gridView1_ValidatingEditor(object sender, BaseContainerValidateEditorEventArgs e)
        {
            var view = sender as GridView;
            
            if (view == null)
                return;
            //не проверяем ничего на строке активного фильтра
            if (view.FocusedRowHandle == GridControl.AutoFilterRowHandle) return;

            object objDataFrom;
            if (view.FocusedColumn.FieldName == "date_from")
            {
                //Проверяем при редактировании столбца date_from
                objDataFrom = e.Value;
            }
            else
            {
                int currenRow = view.FocusedRowHandle;
                //проверяем при редактировании остальных столбцов
                objDataFrom = view.GetRowCellValue(currenRow, "date_from");
            }
            if (objDataFrom != null)
            {
                if (objDataFrom != DBNull.Value)
                {
                    var dateFrom = (DateTime)objDataFrom;
                    if (dateFrom < LastOpenPeriod)
                    {
                        //можно редактировать только столбец date_to
                        if (view.FocusedColumn.FieldName == "date_to") return;
                        e.Valid = false;
                        e.ErrorText = "Данные не могут быть измены/добавлены так как период закрыт!";
                        return;
                    }
                }
            }

            if (view.FocusedColumn.FieldName == "amount")
            {
                if(e.Value == null)
                {
                    e.Valid = false;
                    e.ErrorText = "Поле Сумма не может быть пустой!";
                    return;
                }
                if (string.IsNullOrEmpty(e.Value.ToString()))
                {
                    e.Valid = false;
                    e.ErrorText = "Поле Сумма не может быть пустой!";
                    return;
                }
                int val;
                bool resual = Int32.TryParse(e.Value.ToString(), out val);
                if (!resual)
                {
                    e.Valid = false;
                    e.ErrorText = "Значение превышает допустимый диапазон!";
                    return;
                }
                if(val <= 0)
                {
                    e.Valid = false;
                    e.ErrorText = "Значение не может быть меньше 1-цы";
                }
            }
        }

        /// <summary>
        /// Проверка и сохранение данных
        /// </summary>
        private bool CheckAndSaveModifiedRow()
        {
            //сохраняем последние изменения в gridView1.GridControl.DataSource
            gridView1.CloseEditor();
            gridView1.UpdateCurrentRow();

            DataTable sourse = gridView1.GridControl.DataSource as DataTable;
            if (sourse == null) throw new NullReferenceException("Ошибка определения источника данных!");
            
            //смотрим есть не сохраненные данные
            bool isDirtyTrue = sourse.Rows.Cast<DataRow>().Any(row => row.RowState != DataRowState.Unchanged);
            if (isDirtyTrue)
            {
                DialogResult result = XtraMessageBox.Show(TextConstants.MSG_UNSAVED_DATA_FOUND, @"Сохранение",
                    MessageBoxButtons.YesNo);
                if (result == DialogResult.Yes)
                {
                    try
                    {
                        //Сохраним
                        return SaveData();
                    }
                    catch (Exception)
                    {
                        return false;
                    }
                }
                
                //обновить данные
                _partnerPayments = DataProvider.GetPartnerPayments(_paramQuery, PerioId, RegionIdLst);
                gridControl1.DataSource = _partnerPayments;
                return true;
            }
            return true;
        }

        /// <summary>
        /// проверяем измененные строки и сохраняем их
        /// </summary>
        private bool SaveData()
        {
            try
            {
                //сохраняем последние изменения в gridView1.GridControl.DataSource
                gridView1.CloseEditor();
                gridView1.UpdateCurrentRow();

                DataTable sourse = gridView1.GridControl.DataSource as DataTable;
                if (sourse == null) throw new NullReferenceException("Ошибка определения источника данных!");

                //foreach (DataRow row in sourse.Rows)
                for (int i = 0; i < sourse.Rows.Count; i++)
                {
                    var rowChkErr = sourse.Rows[i];
                    string errColumn = null;
                    if (rowChkErr.RowState == DataRowState.Added | rowChkErr.RowState == DataRowState.Deleted |
                        rowChkErr.RowState == DataRowState.Modified)
                    {
                        //проверить целостность
                        string errMessage;
                        rowChkErr.RowError = ChekData(rowChkErr, out errMessage, out errColumn) == false
                            ? errMessage
                            : null;
                    }
                    //смотрим есть ли ошибочные данные
                    if (rowChkErr.HasErrors)
                    {
                        //Пишем о первой ошибке и валим не сохраняя
                        MessageBox.Show(rowChkErr.RowError);
                        //Поставим фокус на ошибочную строку
                        int idx = gridView1.GetRowHandle(i);
                        gridView1.FocusedRowHandle = idx;
                        //Поставим фокус на ошибочную ячейку
                        if (!string.IsNullOrEmpty(errColumn))
                        {
                            gridView1.FocusedColumn = gridView1.Columns[errColumn];
                        }
                        gridView1.ShowEditor();
                        return false;
                    }
                }

                //после переговоров с Оксаной Ф.
                //if (sourse.Rows.Cast<DataRow>().Any(row => row.RowState == DataRowState.Added | row.RowState == DataRowState.Deleted |
                //                                  row.RowState == DataRowState.Modified))
                //{
                //    _settings.Status = DataProvider.GetPeriodStatus(PerioId);
                //    if (_settings.Status == 9)
                //    {
                //        DialogResult result =
                //            XtraMessageBox.Show(
                //                "Период '" + _settings.PeriodName + "' закрыт. Открыть его для изменения данных?", @"Период закрыт",
                //                MessageBoxButtons.YesNo);

                //        if (result == DialogResult.Yes)
                //        {
                //            //переоткрываем
                //            string openStatus = "2";
                //            DataProvider.EditPeriodStatus(PerioId, openStatus);
                //            _settings.Status = DataProvider.GetPeriodStatus(PerioId);
                //        }
                //        else
                //        {
                //            return true;
                //        }
                //    }
                //}

                foreach (DataRow row in sourse.Rows)
                {
                    if (row.RowState == DataRowState.Added | row.RowState == DataRowState.Deleted |
                        row.RowState == DataRowState.Modified)
                    {
                        try
                        {
                            //Инициализация(создание) временной таблицы
                            DataProvider.InitBeforeSavePartnerPayment();
                            //Записываем в табл для сохранения
                            DataProvider.SavePartnerPaymentTmpTable(row);
                            //сохраняем в постоянную таблицу
                            DataProvider.SavePartnerPayment();
                            //Фиксируем изменения в строке
                            row.AcceptChanges();
                        }
                        catch (Exception ex)
                        {
                            row.RowError = ex.Message;
                        }
                    }
                }

                bool isErrTrue = sourse.Rows.Cast<DataRow>().Any(row => row.HasErrors);
                //если не было ошибок при сохранении то обновляем данные
                if (!isErrTrue)
                {
                    //обновить данные
                    _partnerPayments = DataProvider.GetPartnerPayments(_paramQuery, PerioId, RegionIdLst);
                    gridControl1.DataSource = _partnerPayments;
                    return true;
                }

                //тут нужно установить фокус на строку с ошибкой
                for (int i = 0; i < sourse.Rows.Count; i++)
                {
                    var rowChkErr = sourse.Rows[i];
                    //смотрим есть ли ошибочные данные
                    if (rowChkErr.HasErrors)
                    {
                        //Пишем о первой ошибке и валим не сохраняя
                        MessageBox.Show(rowChkErr.RowError);
                        //Поставим фокус на ошибочную строку
                        gridView1.FocusedRowHandle = i;
                        gridView1.ShowEditor();
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            return false;
        }

        /// <summary>
        /// Контроль на заполненность данных
        /// </summary>
        /// <param name="row">Проверяемая строка</param>
        /// <param name="errMessage">Сообщение об ошибке, если ошибки нет то строка пустая</param>
        /// <param name="errColumn">Имя столбца с ошибкой</param>
        /// <returns>true - ошибок нет; false - в строке ошибка</returns>
        private bool ChekData(DataRow row, out string errMessage, out string errColumn)
        {
            errMessage = "";
            errColumn = "";
            //Корректность полей
            foreach (DataColumn column in row.Table.Columns)
            {
                if (column.ColumnName == "paymentPartner_id" & row.RowState == DataRowState.Added) continue;
                if ((column.ColumnName == "distr_id")
                    | (column.ColumnName == "Region_id")
                    | (column.ColumnName == "distr_id2")
                    | (column.ColumnName == "agentPayment_id")
                    | (column.ColumnName == "date_from")
                    | (column.ColumnName == "amount"))
                {
                    if (string.IsNullOrEmpty(row[column.ColumnName].ToString()))
                    {
                        string strColumn = gridView1.Columns[column.ColumnName].Caption;
                        errMessage = "Поле '" + strColumn + "' не может быть пустое!";
                        errColumn = column.ColumnName;
                        return false;
                    }
                }
            }
            //Корректность дат
            DateTime dateFrom = Convert.ToDateTime(row["date_from"].ToString());
            DateTime dateTo = string.IsNullOrEmpty(row["date_to"].ToString())
                ? DateTime.MaxValue
                : Convert.ToDateTime(row["date_to"].ToString());
            if (dateFrom >= dateTo)
            {
                errMessage = "Дата 'Активен с' не может быть больше/равна дате 'Закрыт с'";
                errColumn = "date_from";
                return false;
            }
            return true;
        }

        /// <summary>
        /// Разукрашиваем в цвет при изменении данных
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">e</param>
        private void gridView1_RowCellStyle(object sender, RowCellStyleEventArgs e)
        {
            DataTable sourse = gridView1.GridControl.DataSource as DataTable;
            if (sourse == null) return;
            int idx = gridView1.GetDataSourceRowIndex(e.RowHandle);
            int maxRow = sourse.Rows.Count;
            if (idx >= 0 & idx < maxRow)
            {
                if (sourse.Rows[idx].RowState == DataRowState.Modified)
                {
                    e.Appearance.BackColor = Constants.ColorModified;
                }

                if (sourse.Rows[idx].RowState == DataRowState.Added)
                {
                    e.Appearance.BackColor = Constants.ColorAdded;
                }
            }
        }

        /// <summary>
        /// При редактировании ячейки "передергиваем" строку для перерисовки
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">e</param>
        private void gridView1_CellValueChanged(object sender, CellValueChangedEventArgs e)
        {
            gridView1.PostEditor();
            gridView1.UpdateCurrentRow();
        }

        private void gridControl1_ProcessGridKey(object sender, KeyEventArgs e)
        {
            ColumnView view = null;
            GridControl gridControl = sender as GridControl;
            
            if (gridControl != null)
            {
                view = gridControl.FocusedView as ColumnView;
            }
            if (view == null) return;
            var dataRowView = view.GetFocusedRow() as DataRowView;

            if (dataRowView == null) return;
            //если нажато CTRL+DELETE
            if (e.KeyCode == Keys.Delete && e.Control)
            {
                string paymentPartnerId = dataRowView.Row["paymentPartner_id"].ToString();
                //если нет paymentPartner_id, значит строка новая и ее можно удалить
                if (string.IsNullOrEmpty(paymentPartnerId))
                {
                    if (view.ActiveEditor != null) return;
                    try
                    {
                        view.DeleteSelectedRows();
                        e.Handled = true;
                    }
                    catch (Exception)
                    {
                        e.Handled = false;
                    }
                    
                }
            }
        }

        public bool IsDataValid
        {
            get
            {
                if (gridView1.ValidateEditor()) return true;
                return false;
            }
        }

        public bool HasChange
        {
            get
            {
                DataTable sourse = gridView1.GridControl.DataSource as DataTable;
                if (sourse != null)
                {
                    return sourse.Rows.Cast<DataRow>().Any(row => row.RowState == DataRowState.Modified);
                }
                return false;
            }
        }

        public void SetEditingMode(bool isOpened)
        {
            //throw new NotImplementedException();
        }

        public bool SaveModifiedReportData()
        {
            return CheckAndSaveModifiedRow();
        }

        public bool ExportToExel(string path)
        {
            try
            {
                XlsxExportOptions option = new XlsxExportOptions { TextExportMode = TextExportMode.Value };
                gridView1.ExportToXlsx(path, option);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
            return true;
        }
    }
}
