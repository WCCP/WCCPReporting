﻿namespace SoftServe.Reports.AGNTProject.Controls
{
    partial class EditorBar
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EditorBar));
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.btnAddRow = new DevExpress.XtraEditors.SimpleButton();
            this.imgColl = new DevExpress.Utils.ImageCollection();
            this.btnActiv = new DevExpress.XtraEditors.SimpleButton();
            this.btnDeactiv = new DevExpress.XtraEditors.SimpleButton();
            this.lblCaption = new System.Windows.Forms.Label();
            this.btnSave = new DevExpress.XtraEditors.SimpleButton();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgColl)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tableLayoutPanel1.ColumnCount = 5;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Controls.Add(this.btnAddRow, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.btnActiv, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.btnDeactiv, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.lblCaption, 3, 0);
            this.tableLayoutPanel1.Controls.Add(this.btnSave, 4, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(902, 30);
            this.tableLayoutPanel1.TabIndex = 4;
            // 
            // btnAddRow
            // 
            this.btnAddRow.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnAddRow.ImageIndex = 0;
            this.btnAddRow.ImageList = this.imgColl;
            this.btnAddRow.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnAddRow.Location = new System.Drawing.Point(3, 3);
            this.btnAddRow.Name = "btnAddRow";
            this.btnAddRow.Size = new System.Drawing.Size(24, 24);
            this.btnAddRow.TabIndex = 7;
            this.btnAddRow.Tag = "addRow";
            this.btnAddRow.ToolTip = "Добавить новую строку";
            // 
            // imgColl
            // 
            this.imgColl.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imgColl.ImageStream")));
            this.imgColl.Images.SetKeyName(0, "Add_16x16.png");
            this.imgColl.Images.SetKeyName(1, "Cancel_16x16.png");
            this.imgColl.Images.SetKeyName(2, "CheckBox_16x16.png");
            this.imgColl.Images.SetKeyName(3, "Save_16x16.png");
            // 
            // btnActiv
            // 
            this.btnActiv.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnActiv.ImageIndex = 2;
            this.btnActiv.ImageList = this.imgColl;
            this.btnActiv.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnActiv.Location = new System.Drawing.Point(33, 3);
            this.btnActiv.Name = "btnActiv";
            this.btnActiv.Size = new System.Drawing.Size(24, 24);
            this.btnActiv.TabIndex = 1;
            this.btnActiv.Tag = "activ";
            this.btnActiv.ToolTip = "Активировать значение";
            // 
            // btnDeactiv
            // 
            this.btnDeactiv.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnDeactiv.Enabled = false;
            this.btnDeactiv.ImageIndex = 1;
            this.btnDeactiv.ImageList = this.imgColl;
            this.btnDeactiv.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnDeactiv.Location = new System.Drawing.Point(63, 3);
            this.btnDeactiv.Name = "btnDeactiv";
            this.btnDeactiv.Size = new System.Drawing.Size(24, 24);
            this.btnDeactiv.TabIndex = 2;
            this.btnDeactiv.Tag = "deactiv";
            this.btnDeactiv.ToolTip = "Деактивировать значение";
            // 
            // lblCaption
            // 
            this.lblCaption.AutoSize = true;
            this.lblCaption.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lblCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblCaption.Location = new System.Drawing.Point(93, 0);
            this.lblCaption.Name = "lblCaption";
            this.lblCaption.Size = new System.Drawing.Size(706, 30);
            this.lblCaption.TabIndex = 6;
            this.lblCaption.Text = "Caption";
            this.lblCaption.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnSave
            // 
            this.btnSave.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnSave.ImageIndex = 3;
            this.btnSave.ImageList = this.imgColl;
            this.btnSave.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnSave.Location = new System.Drawing.Point(805, 3);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(94, 24);
            this.btnSave.TabIndex = 0;
            this.btnSave.Tag = "save";
            this.btnSave.Text = "Сохранить";
            this.btnSave.ToolTip = "Сохранить изменения в таблице";
            // 
            // EditorBar
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.WhiteSmoke;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "EditorBar";
            this.Size = new System.Drawing.Size(902, 30);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgColl)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private DevExpress.XtraEditors.SimpleButton btnActiv;
        private DevExpress.XtraEditors.SimpleButton btnDeactiv;
        private System.Windows.Forms.Label lblCaption;
        private DevExpress.XtraEditors.SimpleButton btnSave;
        private DevExpress.Utils.ImageCollection imgColl;
        private DevExpress.XtraEditors.SimpleButton btnAddRow;
    }
}
