﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Windows.Forms;
using BLToolkit.TypeBuilder;
using DevExpress.Utils.Menu;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Menu;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraEditors.Controls;
using SoftServe.Reports.AGNTProject.DataAccess;
using SoftServe.Reports.AGNTProject.Models;
using SoftServe.Reports.AGNTProject.Utils;
using SoftServe.Reports.AGNTProject.Utils.GridViewExtensions;

namespace SoftServe.Reports.AGNTProject.Controls
{
    public partial class ControlCommonSettings : UserControl, ITabReport, IEditableReport
    {
        ControllerCS _controlerCostsLimits = null;
        ControllerCS _controlerAmortization = null;
        ControllerCS _controlerBonus = null;
        ControllerCS _controlerCountrySettings = null;
        ControllerCS _controlerPayments = null;
        Settings _currentSettings = null;
        bool _isLoading = true;
        public ControlCommonSettings()
        {
            InitializeComponent();
            _controlerCostsLimits = new ControllerCS(grdViewCostsLimits, editBarCostsLimits);
            _controlerAmortization = new ControllerCS(grdViewAmortization, editBarAmortization);
            _controlerBonus = new ControllerCS(grdViewBonusSize, editBarBonus);
            _controlerCountrySettings = new ControllerCS(grdViewCountrySett, editBarCountrySet);
            _controlerPayments = new ControllerCS(grdViewTypeOfPayments, editBarPayments);

            _controlerCostsLimits.BecomeMain += _controlerCostsLimits_BecomeMain;
            _controlerAmortization.BecomeMain += _controlerCostsLimits_BecomeMain;
            _controlerBonus.BecomeMain += _controlerCostsLimits_BecomeMain;
            _controlerCountrySettings.BecomeMain += _controlerCostsLimits_BecomeMain;
            _controlerPayments.BecomeMain += _controlerCostsLimits_BecomeMain;


        }

        private void _controlerCostsLimits_BecomeMain(object sender, EventArgs e)
        {
            if (_isLoading)
                return;

            EditorBar bar = sender as EditorBar;
            if (bar != editBarCostsLimits)
            {
                editBarCostsLimits.EnableEditButtons(false);
                _controlerCostsLimits.ResetViewsCellFocus();
            }
            if (bar != editBarBonus)
            {
                editBarBonus.EnableEditButtons(false);
                _controlerBonus.ResetViewsCellFocus();
            }
            if (bar != editBarAmortization)
            {
                editBarAmortization.EnableEditButtons(false);
                _controlerAmortization.ResetViewsCellFocus();
            }
            if (bar != editBarCountrySet)
            {
                editBarCountrySet.EnableEditButtons(false);
                _controlerCountrySettings.ResetViewsCellFocus();
            }
            if (bar != editBarPayments)
            {
                editBarPayments.EnableEditButtons(false);
                _controlerPayments.ResetViewsCellFocus();
            }
        }

        #region Interfaces
        public bool IsDataValid
        {
            get
            {
                return
                    (_controlerCostsLimits.IsDataValid
                    && _controlerAmortization.IsDataValid
                    && _controlerBonus.IsDataValid
                    && _controlerCountrySettings.IsDataValid
                    && _controlerPayments.IsDataValid);
            }
        }

        public bool HasChange
        {
            get
            {
                return
                    (_controlerCostsLimits.IsModified
                     || _controlerAmortization.IsModified
                     || _controlerBonus.IsModified
                     || _controlerCountrySettings.IsModified
                     || _controlerPayments.IsModified);
            }
        }

        public string ReportText{ get; set;}
        public void LoadData(Settings settings)
        {
            _isLoading = true;
            _currentSettings = settings;
            _controlerCostsLimits.Load(settings);
            _controlerAmortization.Load(settings);
            _controlerBonus.Load(settings);
            _controlerCountrySettings.Load(settings);
            _controlerPayments.Load(settings);
            _isLoading = false;
        }
        public void RestoreLayout(string path)
        {
            throw new NotImplementedException();
        }

        public bool SaveFilter { get; set; }

        public void SaveLayout(string path)
        {
            throw new NotImplementedException();
        }
        public bool SaveModifiedReportData()
        {
            DialogResult res = DialogResult.None;

            if (_controlerCostsLimits.IsModified
                | _controlerAmortization.IsModified
                | _controlerBonus.IsModified
                | _controlerCountrySettings.IsModified
                | _controlerPayments.IsModified)

                res = XtraMessageBox.Show(TextConstants.MSG_UNSAVED_DATA_FOUND, @"Сохранение",
                MessageBoxButtons.YesNo);

            if (res == DialogResult.Yes)
            {
                _currentSettings.Status = DataProvider.GetPeriodStatus(_currentSettings.PeriodId);
                if (_currentSettings.Status == 9)
                {
                    XtraMessageBox.Show(
                                      "Период \"" + _currentSettings.PeriodName + "\" закрыт. Для того чтобы сохранить данные переоткройте период", @"Период закрыт",
                                      MessageBoxButtons.OK);
                    _currentSettings.OnStatusChanged();
                    return false;
                }
            }
            return (
                  _controlerCostsLimits.SaveModifiedReportData(res == DialogResult.Yes)
               && _controlerAmortization.SaveModifiedReportData(res == DialogResult.Yes)
               && _controlerBonus.SaveModifiedReportData(res == DialogResult.Yes)
               && _controlerCountrySettings.SaveModifiedReportData(res == DialogResult.Yes)
               && _controlerPayments.SaveModifiedReportData(res == DialogResult.Yes)
               );          
        }
        public void SetEditingMode(bool isOpened)
        {
            if(!isOpened)
            SaveModifiedReportData();

            _controlerCostsLimits.SetEditingMode(isOpened);
            _controlerAmortization.SetEditingMode(isOpened);
            _controlerBonus.SetEditingMode(isOpened);
            _controlerCountrySettings.SetEditingMode(isOpened);
            _controlerPayments.SetEditingMode(isOpened);
        }
        #endregion
        private void ControlCommonSettings_Load(object sender, EventArgs e)
        {
            editBarCostsLimits.SetBtnAddRowVisible(true);
            editBarPayments.SetBtnAddRowVisible(true);
            editBarBonus.SetBtnAddRowVisible(false);
            editBarAmortization.SetBtnAddRowVisible(false);
            editBarCountrySet.SetBtnAddRowVisible(false);

            // adjust controls sizes
            splitContainerControl1.FixedPanel = SplitFixedPanel.None;
            splitContainerControl2.FixedPanel = SplitFixedPanel.None;
            splitContainerControl3.FixedPanel = SplitFixedPanel.None;
            splitContainerControl4.FixedPanel = SplitFixedPanel.None;

            splitContainerControl2.SplitterPosition = Convert.ToInt32(splitContainerControl2.Height * 0.32);
            splitContainerControl4.SplitterPosition = Convert.ToInt32(splitContainerControl4.Height * 0.65);
            splitContainerControl3.SplitterPosition = Convert.ToInt32(splitContainerControl3.Height * 0.65);
            splitContainerControl1.SplitterPosition = splitContainerControl1.Width / 2;             
        }        
    }
}