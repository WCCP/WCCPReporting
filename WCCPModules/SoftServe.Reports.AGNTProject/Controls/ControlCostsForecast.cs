﻿using System;
using System.Data;
using System.IO;
using System.Windows.Forms;
using DevExpress.XtraPrinting;
using SoftServe.Reports.AGNTProject.DataAccess;
using SoftServe.Reports.AGNTProject.Models;
using SoftServe.Reports.AGNTProject.Utils;

namespace SoftServe.Reports.AGNTProject.Controls
{
    public partial class ControlCostsForecast : UserControl, ITabReport, IExportData
    {
        private DataTable _costsForecast;
        private decimal _paramQuery;
        private Settings _settings;
        private int PerioId { get; set; }
        private string RegionIdLst { get; set; }

        public ControlCostsForecast()
        {
            InitializeComponent();
            ReportText = Constants.CaptionCostsForecast;
        }

        public string ReportText { get; private set; }
        public void LoadData(Settings settings)
        {
            _settings = settings;
            try
            {
                PerioId = _settings.PeriodId;
                RegionIdLst = "";
                foreach (string s in _settings.RegionIDsList)
                {
                    RegionIdLst = RegionIdLst + s + ",";
                }
                //убираем последнюю запятую
                RegionIdLst = RegionIdLst.Remove(RegionIdLst.Length - 1);
                var percentStr = _settings.SelectedReports[Constants.CaptionCostsForecast];
                _paramQuery = string.IsNullOrEmpty(percentStr) ? 0 : Convert.ToDecimal(percentStr.Replace(".",","));
            }
            catch (Exception ex)
            {
                throw new ArgumentException("Ошибка получения парамтров:" + ex.Message);
            }
            _costsForecast = DataProvider.GetCostForecast(PerioId, _paramQuery, RegionIdLst);
            gridControl1.DataSource = _costsForecast;

            if (_settings.ShowTotals)
            {
                gridView1.BeginSort();
                try
                {
                    gridView1.ClearGrouping();
                    gridView1.Columns["Partner"].GroupIndex = 0;
                }
                finally
                {
                    gridView1.EndSort();
                }
            }
            else
            {
                gridView1.BeginSort();
                try
                {
                    gridView1.ClearGrouping();
                    gridView1.Columns["Partner"].GroupIndex = -1;
                }
                finally
                {
                    gridView1.EndSort();
                }
            }
            Access();

            if (!SaveFilter)
            {
                gridView1.ActiveFilterCriteria = null;
                gridView1.ApplyFindFilter("");
                gridView1.FindPanelVisible = false;
                gridView1.OptionsView.ShowAutoFilterRow = false;
            }
            SaveFilter = false;
        }

        private void Access()
        {
            AccessToApp accessToApp = AccessToApp.GetInstance();
            if (accessToApp.IsAdmin | accessToApp.IsReport | accessToApp.IsReportEdit) return;
            if (accessToApp.IsBaseEmplCenter)
            {
                throw new ApplicationException("Вы не имеете доступа к данной вкладке");
            }
        }

        private string EditLayoutFileName(string path)
        {
            string fileName = "ControlCostsForecast_" + Path.GetFileName(path);
            string pathToDir = Path.GetDirectoryName(path);
            if (pathToDir != null) return Path.Combine(pathToDir, fileName);
            return path;
        }

        public void SaveLayout(string path)
        {
            //Добавим к имени имя компонента
            string fullPath = EditLayoutFileName(path);
            gridView1.SaveLayoutToXml(fullPath);
        }

        public void RestoreLayout(string path)
        {
            gridView1.BestFitColumns();
            //Добавим к имени имя компонента
            string fullPath = EditLayoutFileName(path);
            if (File.Exists(fullPath))
            {
                gridView1.RestoreLayoutFromXml(fullPath);
            }
        }

        public bool SaveFilter { get; set; }

        public bool ExportToExel(string path)
        {
            try
            {
                gridView1.BestFitColumns();
                XlsxExportOptions option = new XlsxExportOptions { TextExportMode = TextExportMode.Text };
                gridView1.ExportToXlsx(path, option);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
            return true;
        }
    }
}
