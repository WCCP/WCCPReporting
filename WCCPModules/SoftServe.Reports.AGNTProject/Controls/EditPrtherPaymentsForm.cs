﻿using System;
using System.Windows.Forms;
using DevExpress.XtraEditors.Controls;
using SoftServe.Reports.AGNTProject.DataAccess;

namespace SoftServe.Reports.AGNTProject.Controls
{
    public partial class EditPrtherPaymentsForm : Form
    {
        public EditPrtherPaymentsForm(int periodId)
        {
            PeriodId = periodId;
            InitializeComponent();
            InitData();
        }

        private int PeriodId { get; set; }

        private void InitData()
        {
            #region lueDistr
            lueDistr.Properties.BeginUpdate();
            lueDistr.Properties.DataSource = DataProvider.GetDistrib(PeriodId);
            lueDistr.Properties.Columns.Add(new LookUpColumnInfo("DISTR_NAME", "Дистребьютер"));
            lueDistr.Properties.Columns.Add(new LookUpColumnInfo("DISTR_ID", "Код дистребьютера"));
            lueDistr.Properties.DisplayMember = "DISTR_NAME";
            lueDistr.Properties.ValueMember = "DISTR_ID";
            lueDistr.Properties.ShowFooter = false;
            lueDistr.Properties.NullText = String.Empty;
            lueDistr.Properties.EndUpdate();
            //Установить в нужного дистра
            #endregion

        }
    }
}
