﻿namespace SoftServe.Reports.AGNTProject.Controls
{
    partial class ControlCostsForecast
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gcpart = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcpYear = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcMonthNameRu = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcRegion_name = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcDSM = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcPartner = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcPayer_id = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcPartnerType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcCity = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcSector_id = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcSector_name = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcActiveFrom = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcUserType_ShortName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcWorkDays_total = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcSalary = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcBonus_sum = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcAmortization_sum = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcFuelCompensation_sumWt = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcPublicTrCompensation = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcConnectionCompensation = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcFoodCompensation = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcStationeryCompensation = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcConnectionTabletCompensation = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcVacationPayment_sum = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcUST_sum = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcagentPaymentSum = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcPaymentWithPartners = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcForecast = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // gridControl1
            // 
            this.gridControl1.Cursor = System.Windows.Forms.Cursors.Default;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.Location = new System.Drawing.Point(0, 0);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(1317, 631);
            this.gridControl1.TabIndex = 0;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gcpart,
            this.gcpYear,
            this.gcMonthNameRu,
            this.gcRegion_name,
            this.gcDSM,
            this.gcPartner,
            this.gcPayer_id,
            this.gcPartnerType,
            this.gcCity,
            this.gcSector_id,
            this.gcSector_name,
            this.gcActiveFrom,
            this.gcUserType_ShortName,
            this.gcWorkDays_total,
            this.gcSalary,
            this.gcBonus_sum,
            this.gcAmortization_sum,
            this.gcFuelCompensation_sumWt,
            this.gcPublicTrCompensation,
            this.gcConnectionCompensation,
            this.gcFoodCompensation,
            this.gcStationeryCompensation,
            this.gcConnectionTabletCompensation,
            this.gcVacationPayment_sum,
            this.gcUST_sum,
            this.gcagentPaymentSum,
            this.gcPaymentWithPartners,
            this.gcForecast});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.GroupSummary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Salary", this.gcSalary, "(ИТОГО: {0})"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Bonus_sum", this.gcBonus_sum, "(ИТОГО: {0})"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Amortization_sum", this.gcAmortization_sum, "(ИТОГО: {0})"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "FuelCompensation_sumWt", this.gcFuelCompensation_sumWt, "(ИТОГО: {0})"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "PublicTrCompensation", this.gcPublicTrCompensation, "(ИТОГО: {0})"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "ConnectionCompensation", this.gcConnectionCompensation, "(ИТОГО: {0})"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "FoodCompensation", this.gcFoodCompensation, "(ИТОГО: {0})"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "StationeryCompensation", this.gcStationeryCompensation, "(ИТОГО: {0})"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "ConnectionTabletCompensation", this.gcConnectionTabletCompensation, "(ИТОГО: {0})"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "VacationPayment_sum", this.gcVacationPayment_sum, "(ИТОГО: {0})"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "UST_sum", this.gcUST_sum, "(ИТОГО: {0})"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "agentPaymentSum", this.gcagentPaymentSum, "(ИТОГО: {0})"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "PaymentWithPartners", this.gcPaymentWithPartners, "(ИТОГО: {0})"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Forecast", this.gcForecast, "(ИТОГО: {0})")});
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsBehavior.Editable = false;
            this.gridView1.OptionsFind.ShowFindButton = false;
            this.gridView1.OptionsPrint.AutoWidth = false;
            this.gridView1.OptionsSelection.MultiSelect = true;
            this.gridView1.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CellSelect;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.ColumnHeaderAutoHeight = DevExpress.Utils.DefaultBoolean.True;
            this.gridView1.OptionsView.ShowFooter = true;
            // 
            // gcpart
            // 
            this.gcpart.Caption = "partition";
            this.gcpart.FieldName = "part";
            this.gcpart.Name = "gcpart";
            this.gcpart.OptionsColumn.ShowInCustomizationForm = false;
            // 
            // gcpYear
            // 
            this.gcpYear.Caption = "Год";
            this.gcpYear.FieldName = "pYear";
            this.gcpYear.Name = "gcpYear";
            this.gcpYear.Visible = true;
            this.gcpYear.VisibleIndex = 0;
            // 
            // gcMonthNameRu
            // 
            this.gcMonthNameRu.Caption = "Месяц";
            this.gcMonthNameRu.FieldName = "MonthNameRu";
            this.gcMonthNameRu.Name = "gcMonthNameRu";
            this.gcMonthNameRu.Visible = true;
            this.gcMonthNameRu.VisibleIndex = 1;
            // 
            // gcRegion_name
            // 
            this.gcRegion_name.Caption = "Регион";
            this.gcRegion_name.FieldName = "Region_name";
            this.gcRegion_name.Name = "gcRegion_name";
            this.gcRegion_name.Visible = true;
            this.gcRegion_name.VisibleIndex = 2;
            // 
            // gcDSM
            // 
            this.gcDSM.Caption = "Дивизион";
            this.gcDSM.FieldName = "DSM";
            this.gcDSM.Name = "gcDSM";
            this.gcDSM.Visible = true;
            this.gcDSM.VisibleIndex = 3;
            // 
            // gcPartner
            // 
            this.gcPartner.Caption = "Партнер";
            this.gcPartner.FieldName = "Partner";
            this.gcPartner.Name = "gcPartner";
            this.gcPartner.Visible = true;
            this.gcPartner.VisibleIndex = 4;
            // 
            // gcPayer_id
            // 
            this.gcPayer_id.Caption = "Payer ID";
            this.gcPayer_id.FieldName = "Payer_id";
            this.gcPayer_id.Name = "gcPayer_id";
            this.gcPayer_id.Visible = true;
            this.gcPayer_id.VisibleIndex = 5;
            // 
            // gcPartnerType
            // 
            this.gcPartnerType.Caption = "Тип партнера";
            this.gcPartnerType.FieldName = "PartnerType";
            this.gcPartnerType.Name = "gcPartnerType";
            this.gcPartnerType.Visible = true;
            this.gcPartnerType.VisibleIndex = 6;
            // 
            // gcCity
            // 
            this.gcCity.Caption = "Город";
            this.gcCity.FieldName = "City";
            this.gcCity.Name = "gcCity";
            this.gcCity.Visible = true;
            this.gcCity.VisibleIndex = 7;
            // 
            // gcSector_id
            // 
            this.gcSector_id.Caption = "Код сектора";
            this.gcSector_id.FieldName = "Sector_id";
            this.gcSector_id.Name = "gcSector_id";
            this.gcSector_id.Visible = true;
            this.gcSector_id.VisibleIndex = 8;
            // 
            // gcSector_name
            // 
            this.gcSector_name.Caption = "Название сектора";
            this.gcSector_name.FieldName = "Sector_name";
            this.gcSector_name.Name = "gcSector_name";
            this.gcSector_name.Visible = true;
            this.gcSector_name.VisibleIndex = 9;
            // 
            // gcActiveFrom
            // 
            this.gcActiveFrom.Caption = "Активен с";
            this.gcActiveFrom.FieldName = "ActiveFrom";
            this.gcActiveFrom.Name = "gcActiveFrom";
            this.gcActiveFrom.Visible = true;
            this.gcActiveFrom.VisibleIndex = 10;
            // 
            // gcUserType_ShortName
            // 
            this.gcUserType_ShortName.Caption = "Тип сектора";
            this.gcUserType_ShortName.FieldName = "UserType_ShortName";
            this.gcUserType_ShortName.Name = "gcUserType_ShortName";
            this.gcUserType_ShortName.Visible = true;
            this.gcUserType_ShortName.VisibleIndex = 11;
            // 
            // gcWorkDays_total
            // 
            this.gcWorkDays_total.Caption = "Количество рабочих дней";
            this.gcWorkDays_total.FieldName = "WorkDays_total";
            this.gcWorkDays_total.Name = "gcWorkDays_total";
            this.gcWorkDays_total.Visible = true;
            this.gcWorkDays_total.VisibleIndex = 12;
            // 
            // gcSalary
            // 
            this.gcSalary.Caption = "Постоянная часть, сумма";
            this.gcSalary.FieldName = "Salary";
            this.gcSalary.Name = "gcSalary";
            this.gcSalary.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Salary", "ИТОГО: {0}")});
            this.gcSalary.Visible = true;
            this.gcSalary.VisibleIndex = 13;
            // 
            // gcBonus_sum
            // 
            this.gcBonus_sum.Caption = "Переменная часть, сумма";
            this.gcBonus_sum.FieldName = "Bonus_sum";
            this.gcBonus_sum.Name = "gcBonus_sum";
            this.gcBonus_sum.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Bonus_sum", "ИТОГО: {0}")});
            this.gcBonus_sum.Visible = true;
            this.gcBonus_sum.VisibleIndex = 14;
            // 
            // gcAmortization_sum
            // 
            this.gcAmortization_sum.Caption = "Амортизация, сумма";
            this.gcAmortization_sum.FieldName = "Amortization_sum";
            this.gcAmortization_sum.Name = "gcAmortization_sum";
            this.gcAmortization_sum.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Amortization_sum", "ИТОГО: {0}")});
            this.gcAmortization_sum.Visible = true;
            this.gcAmortization_sum.VisibleIndex = 15;
            // 
            // gcFuelCompensation_sumWt
            // 
            this.gcFuelCompensation_sumWt.Caption = "Компенсация топлива, сумма";
            this.gcFuelCompensation_sumWt.FieldName = "FuelCompensation_sumWt";
            this.gcFuelCompensation_sumWt.Name = "gcFuelCompensation_sumWt";
            this.gcFuelCompensation_sumWt.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "FuelCompensation_sumWt", "ИТОГО: {0}")});
            this.gcFuelCompensation_sumWt.Visible = true;
            this.gcFuelCompensation_sumWt.VisibleIndex = 16;
            // 
            // gcPublicTrCompensation
            // 
            this.gcPublicTrCompensation.Caption = "Компенсация общественного транспорта, сумма";
            this.gcPublicTrCompensation.FieldName = "PublicTrCompensation";
            this.gcPublicTrCompensation.Name = "gcPublicTrCompensation";
            this.gcPublicTrCompensation.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "PublicTrCompensation", "ИТОГО: {0}")});
            this.gcPublicTrCompensation.Visible = true;
            this.gcPublicTrCompensation.VisibleIndex = 17;
            // 
            // gcConnectionCompensation
            // 
            this.gcConnectionCompensation.Caption = "Компенсация связи, сумма";
            this.gcConnectionCompensation.FieldName = "ConnectionCompensation";
            this.gcConnectionCompensation.Name = "gcConnectionCompensation";
            this.gcConnectionCompensation.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "ConnectionCompensation", "ИТОГО: {0}")});
            this.gcConnectionCompensation.Visible = true;
            this.gcConnectionCompensation.VisibleIndex = 18;
            // 
            // gcFoodCompensation
            // 
            this.gcFoodCompensation.Caption = "Компенсация питания, сумма";
            this.gcFoodCompensation.FieldName = "FoodCompensation";
            this.gcFoodCompensation.Name = "gcFoodCompensation";
            this.gcFoodCompensation.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "FoodCompensation", "ИТОГО: {0}")});
            this.gcFoodCompensation.Visible = true;
            this.gcFoodCompensation.VisibleIndex = 19;
            // 
            // gcStationeryCompensation
            // 
            this.gcStationeryCompensation.Caption = "Компенсация канцелярии, сумма";
            this.gcStationeryCompensation.FieldName = "StationeryCompensation";
            this.gcStationeryCompensation.Name = "gcStationeryCompensation";
            this.gcStationeryCompensation.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "StationeryCompensation", "ИТОГО: {0}")});
            this.gcStationeryCompensation.Visible = true;
            this.gcStationeryCompensation.VisibleIndex = 20;
            // 
            // gcConnectionTabletCompensation
            // 
            this.gcConnectionTabletCompensation.Caption = "Компенсация связи для планшета, сумма";
            this.gcConnectionTabletCompensation.FieldName = "ConnectionTabletCompensation";
            this.gcConnectionTabletCompensation.Name = "gcConnectionTabletCompensation";
            this.gcConnectionTabletCompensation.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "ConnectionTabletCompensation", "ИТОГО: {0}")});
            this.gcConnectionTabletCompensation.Visible = true;
            this.gcConnectionTabletCompensation.VisibleIndex = 21;
            // 
            // gcVacationPayment_sum
            // 
            this.gcVacationPayment_sum.Caption = "Отпускные, сумма";
            this.gcVacationPayment_sum.FieldName = "VacationPayment_sum";
            this.gcVacationPayment_sum.Name = "gcVacationPayment_sum";
            this.gcVacationPayment_sum.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "VacationPayment_sum", "ИТОГО: {0}")});
            this.gcVacationPayment_sum.Visible = true;
            this.gcVacationPayment_sum.VisibleIndex = 22;
            // 
            // gcUST_sum
            // 
            this.gcUST_sum.Caption = "ЕСН, сумма";
            this.gcUST_sum.FieldName = "UST_sum";
            this.gcUST_sum.Name = "gcUST_sum";
            this.gcUST_sum.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "UST_sum", "ИТОГО: {0}")});
            this.gcUST_sum.Visible = true;
            this.gcUST_sum.VisibleIndex = 23;
            // 
            // gcagentPaymentSum
            // 
            this.gcagentPaymentSum.Caption = "Агентский платёж, сумма";
            this.gcagentPaymentSum.FieldName = "agentPaymentSum";
            this.gcagentPaymentSum.Name = "gcagentPaymentSum";
            this.gcagentPaymentSum.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "agentPaymentSum", "ИТОГО: {0}")});
            this.gcagentPaymentSum.Visible = true;
            this.gcagentPaymentSum.VisibleIndex = 24;
            // 
            // gcPaymentWithPartners
            // 
            this.gcPaymentWithPartners.Caption = "Расчеты с партнерами, сумма";
            this.gcPaymentWithPartners.FieldName = "PaymentWithPartners";
            this.gcPaymentWithPartners.Name = "gcPaymentWithPartners";
            this.gcPaymentWithPartners.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "PaymentWithPartners", "ИТОГО: {0}")});
            this.gcPaymentWithPartners.Visible = true;
            this.gcPaymentWithPartners.VisibleIndex = 25;
            // 
            // gcForecast
            // 
            this.gcForecast.Caption = "Прогноз итого, сумма";
            this.gcForecast.FieldName = "Forecast";
            this.gcForecast.Name = "gcForecast";
            this.gcForecast.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Forecast", "ИТОГО: {0}")});
            this.gcForecast.Visible = true;
            this.gcForecast.VisibleIndex = 26;
            // 
            // ControlCostsForecast
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gridControl1);
            this.Name = "ControlCostsForecast";
            this.Size = new System.Drawing.Size(1317, 631);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn gcpart;
        private DevExpress.XtraGrid.Columns.GridColumn gcpYear;
        private DevExpress.XtraGrid.Columns.GridColumn gcMonthNameRu;
        private DevExpress.XtraGrid.Columns.GridColumn gcRegion_name;
        private DevExpress.XtraGrid.Columns.GridColumn gcDSM;
        private DevExpress.XtraGrid.Columns.GridColumn gcPartner;
        private DevExpress.XtraGrid.Columns.GridColumn gcPayer_id;
        private DevExpress.XtraGrid.Columns.GridColumn gcPartnerType;
        private DevExpress.XtraGrid.Columns.GridColumn gcCity;
        private DevExpress.XtraGrid.Columns.GridColumn gcSector_id;
        private DevExpress.XtraGrid.Columns.GridColumn gcSector_name;
        private DevExpress.XtraGrid.Columns.GridColumn gcActiveFrom;
        private DevExpress.XtraGrid.Columns.GridColumn gcUserType_ShortName;
        private DevExpress.XtraGrid.Columns.GridColumn gcWorkDays_total;
        private DevExpress.XtraGrid.Columns.GridColumn gcSalary;
        private DevExpress.XtraGrid.Columns.GridColumn gcBonus_sum;
        private DevExpress.XtraGrid.Columns.GridColumn gcAmortization_sum;
        private DevExpress.XtraGrid.Columns.GridColumn gcFuelCompensation_sumWt;
        private DevExpress.XtraGrid.Columns.GridColumn gcPublicTrCompensation;
        private DevExpress.XtraGrid.Columns.GridColumn gcConnectionCompensation;
        private DevExpress.XtraGrid.Columns.GridColumn gcFoodCompensation;
        private DevExpress.XtraGrid.Columns.GridColumn gcStationeryCompensation;
        private DevExpress.XtraGrid.Columns.GridColumn gcConnectionTabletCompensation;
        private DevExpress.XtraGrid.Columns.GridColumn gcVacationPayment_sum;
        private DevExpress.XtraGrid.Columns.GridColumn gcUST_sum;
        private DevExpress.XtraGrid.Columns.GridColumn gcagentPaymentSum;
        private DevExpress.XtraGrid.Columns.GridColumn gcPaymentWithPartners;
        private DevExpress.XtraGrid.Columns.GridColumn gcForecast;
    }
}
