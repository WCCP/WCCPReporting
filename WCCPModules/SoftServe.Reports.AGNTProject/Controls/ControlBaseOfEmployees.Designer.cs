﻿namespace SoftServe.Reports.AGNTProject.Controls
{
    partial class ControlBaseOfEmployees
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ControlBaseOfEmployees));
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.bRestore = new DevExpress.XtraEditors.SimpleButton();
            this.sbReCalc = new DevExpress.XtraEditors.SimpleButton();
            this.sbLoadFromXLS = new DevExpress.XtraEditors.SimpleButton();
            this.sbSave = new DevExpress.XtraEditors.SimpleButton();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gcSector_id = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcSector_code = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcSector_name = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcDLM = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcULM = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcActiveFrom = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcUserLevel = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcSectorType_id = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcSectorType_Name = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcChannel = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcDSM = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcCity = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcPartner_id = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcPartner_Name = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcWorkSchema = new DevExpress.XtraGrid.Columns.GridColumn();
            this.lueWorkSchema = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.gcSalary1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.iteSalary1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gcConstPart_sum = new DevExpress.XtraGrid.Columns.GridColumn();
            this.iteConstPart_sum = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gcAmortization_norm = new DevExpress.XtraGrid.Columns.GridColumn();
            this.iteAmortization_norm = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gcAmortization_sum = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcMileagePerDay_norm = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcMileagePerMonth_fact = new DevExpress.XtraGrid.Columns.GridColumn();
            this.iteMileagePerMonth_fact = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gcFuelConsumption_norm = new DevExpress.XtraGrid.Columns.GridColumn();
            this.iteFuelConsumption_norm = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gcFuel_price = new DevExpress.XtraGrid.Columns.GridColumn();
            this.iteFuel_price = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gcFuelCompensation_sumWt = new DevExpress.XtraGrid.Columns.GridColumn();
            this.COMPESTRANSP = new DevExpress.XtraGrid.Columns.GridColumn();
            this.luePublicTrCompensation_LimitID = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.COMPESLINK = new DevExpress.XtraGrid.Columns.GridColumn();
            this.lueConnectionCompensation_LimitID = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.COMPESFOOD = new DevExpress.XtraGrid.Columns.GridColumn();
            this.lueFoodCompensation_LimitID = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.COMPESOFFICE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.lueStationeryCompensation_LimitID = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.gcVacationPayment_sum = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcUST_sum = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcAdministration_sum = new DevExpress.XtraGrid.Columns.GridColumn();
            this.COMPESLINKPAD = new DevExpress.XtraGrid.Columns.GridColumn();
            this.lueConnectionTabletCompensation_LimitID = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.gcVariablePart_sum = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcWorkDays_total = new DevExpress.XtraGrid.Columns.GridColumn();
            this.iteWorkDays_total = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gcWorkedDays_num = new DevExpress.XtraGrid.Columns.GridColumn();
            this.iteWorkedDays_num = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gcAddCompensation = new DevExpress.XtraGrid.Columns.GridColumn();
            this.iteAddCompensation = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gcVariablePartClosingPerc_fact = new DevExpress.XtraGrid.Columns.GridColumn();
            this.iteVariablePartClosingPerc_fact = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gcCorrectVarPartPrevP_sum = new DevExpress.XtraGrid.Columns.GridColumn();
            this.iteCorrectVarPartPrevP_sum = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gcComment = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.lueStatus = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.gcWorkSchemaShadow = new DevExpress.XtraGrid.Columns.GridColumn();
            this.COMPESTRANSPShadow = new DevExpress.XtraGrid.Columns.GridColumn();
            this.COMPESLINKShadow = new DevExpress.XtraGrid.Columns.GridColumn();
            this.COMPESFOODShadow = new DevExpress.XtraGrid.Columns.GridColumn();
            this.COMPESOFFICEShadow = new DevExpress.XtraGrid.Columns.GridColumn();
            this.COMPESLINKPADShadow = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcStatusShadow = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueWorkSchema)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.iteSalary1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.iteConstPart_sum)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.iteAmortization_norm)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.iteMileagePerMonth_fact)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.iteFuelConsumption_norm)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.iteFuel_price)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.luePublicTrCompensation_LimitID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueConnectionCompensation_LimitID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueFoodCompensation_LimitID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueStationeryCompensation_LimitID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueConnectionTabletCompensation_LimitID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.iteWorkDays_total)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.iteWorkedDays_num)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.iteAddCompensation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.iteVariablePartClosingPerc_fact)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.iteCorrectVarPartPrevP_sum)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueStatus)).BeginInit();
            this.SuspendLayout();
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl1.Horizontal = false;
            this.splitContainerControl1.IsSplitterFixed = true;
            this.splitContainerControl1.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.Controls.Add(this.bRestore);
            this.splitContainerControl1.Panel1.Controls.Add(this.sbReCalc);
            this.splitContainerControl1.Panel1.Controls.Add(this.sbLoadFromXLS);
            this.splitContainerControl1.Panel1.Controls.Add(this.sbSave);
            this.splitContainerControl1.Panel1.Text = "Panel1";
            this.splitContainerControl1.Panel2.Controls.Add(this.gridControl1);
            this.splitContainerControl1.Panel2.Text = "Panel2";
            this.splitContainerControl1.Size = new System.Drawing.Size(1392, 447);
            this.splitContainerControl1.SplitterPosition = 33;
            this.splitContainerControl1.TabIndex = 0;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // bRestore
            // 
            this.bRestore.Image = ((System.Drawing.Image)(resources.GetObject("bRestore.Image")));
            this.bRestore.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.bRestore.Location = new System.Drawing.Point(93, 8);
            this.bRestore.Name = "bRestore";
            this.bRestore.Size = new System.Drawing.Size(24, 24);
            this.bRestore.TabIndex = 4;
            this.bRestore.Text = "Востановить настройки грида";
            this.bRestore.ToolTip = "Востановить настройки грида";
            this.bRestore.Click += new System.EventHandler(this.bRestore_Click);
            // 
            // sbReCalc
            // 
            this.sbReCalc.Image = ((System.Drawing.Image)(resources.GetObject("sbReCalc.Image")));
            this.sbReCalc.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.sbReCalc.Location = new System.Drawing.Point(63, 8);
            this.sbReCalc.Name = "sbReCalc";
            this.sbReCalc.Size = new System.Drawing.Size(24, 24);
            this.sbReCalc.TabIndex = 3;
            this.sbReCalc.Text = "Пересчитать";
            this.sbReCalc.ToolTip = "Пересчитать";
            this.sbReCalc.Click += new System.EventHandler(this.sbReCalc_Click);
            // 
            // sbLoadFromXLS
            // 
            this.sbLoadFromXLS.Image = ((System.Drawing.Image)(resources.GetObject("sbLoadFromXLS.Image")));
            this.sbLoadFromXLS.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.sbLoadFromXLS.Location = new System.Drawing.Point(33, 8);
            this.sbLoadFromXLS.Name = "sbLoadFromXLS";
            this.sbLoadFromXLS.Size = new System.Drawing.Size(24, 24);
            this.sbLoadFromXLS.TabIndex = 2;
            this.sbLoadFromXLS.Text = "Загрузить";
            this.sbLoadFromXLS.ToolTip = "Загрузить XLS";
            this.sbLoadFromXLS.Click += new System.EventHandler(this.sbLoadFromXLS_Click);
            // 
            // sbSave
            // 
            this.sbSave.Image = ((System.Drawing.Image)(resources.GetObject("sbSave.Image")));
            this.sbSave.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.sbSave.Location = new System.Drawing.Point(3, 8);
            this.sbSave.Name = "sbSave";
            this.sbSave.Size = new System.Drawing.Size(24, 24);
            this.sbSave.TabIndex = 0;
            this.sbSave.Text = "Сохранить";
            this.sbSave.ToolTip = "Сохранить";
            this.sbSave.Click += new System.EventHandler(this.sbSave_Click);
            // 
            // gridControl1
            // 
            this.gridControl1.Cursor = System.Windows.Forms.Cursors.Default;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.Location = new System.Drawing.Point(0, 0);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.lueWorkSchema,
            this.iteSalary1,
            this.iteConstPart_sum,
            this.iteAmortization_norm,
            this.iteMileagePerMonth_fact,
            this.iteFuelConsumption_norm,
            this.iteFuel_price,
            this.luePublicTrCompensation_LimitID,
            this.lueConnectionCompensation_LimitID,
            this.lueFoodCompensation_LimitID,
            this.lueStationeryCompensation_LimitID,
            this.lueConnectionTabletCompensation_LimitID,
            this.iteWorkDays_total,
            this.iteWorkedDays_num,
            this.iteAddCompensation,
            this.iteVariablePartClosingPerc_fact,
            this.iteCorrectVarPartPrevP_sum,
            this.lueStatus});
            this.gridControl1.Size = new System.Drawing.Size(1392, 409);
            this.gridControl1.TabIndex = 0;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gcSector_id,
            this.gcSector_code,
            this.gcSector_name,
            this.gcDLM,
            this.gcULM,
            this.gcActiveFrom,
            this.gcUserLevel,
            this.gcSectorType_id,
            this.gcSectorType_Name,
            this.gcChannel,
            this.gcDSM,
            this.gcCity,
            this.gcPartner_id,
            this.gcPartner_Name,
            this.gcWorkSchema,
            this.gcSalary1,
            this.gcConstPart_sum,
            this.gcAmortization_norm,
            this.gcAmortization_sum,
            this.gcMileagePerDay_norm,
            this.gcMileagePerMonth_fact,
            this.gcFuelConsumption_norm,
            this.gcFuel_price,
            this.gcFuelCompensation_sumWt,
            this.COMPESTRANSP,
            this.COMPESLINK,
            this.COMPESFOOD,
            this.COMPESOFFICE,
            this.gcVacationPayment_sum,
            this.gcUST_sum,
            this.gcAdministration_sum,
            this.COMPESLINKPAD,
            this.gcVariablePart_sum,
            this.gcWorkDays_total,
            this.gcWorkedDays_num,
            this.gcAddCompensation,
            this.gcVariablePartClosingPerc_fact,
            this.gcCorrectVarPartPrevP_sum,
            this.gcComment,
            this.gcStatus,
            this.gcWorkSchemaShadow,
            this.COMPESTRANSPShadow,
            this.COMPESLINKShadow,
            this.COMPESFOODShadow,
            this.COMPESOFFICEShadow,
            this.COMPESLINKPADShadow,
            this.gcStatusShadow});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsFind.ShowFindButton = false;
            this.gridView1.OptionsPrint.AutoWidth = false;
            this.gridView1.OptionsSelection.MultiSelect = true;
            this.gridView1.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CellSelect;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.ColumnHeaderAutoHeight = DevExpress.Utils.DefaultBoolean.True;
            this.gridView1.RowCellStyle += new DevExpress.XtraGrid.Views.Grid.RowCellStyleEventHandler(this.gridView1_RowCellStyle);
            this.gridView1.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.gridView1_ShowingEditor);
            this.gridView1.ShownEditor += new System.EventHandler(this.gridView1_ShownEditor);
            this.gridView1.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.gridView1_CellValueChanged);
            this.gridView1.ValidatingEditor += new DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventHandler(this.gridView1_ValidatingEditor);
            // 
            // gcSector_id
            // 
            this.gcSector_id.Caption = "ID сектора";
            this.gcSector_id.FieldName = "Sector_id";
            this.gcSector_id.Name = "gcSector_id";
            this.gcSector_id.OptionsColumn.AllowEdit = false;
            this.gcSector_id.Visible = true;
            this.gcSector_id.VisibleIndex = 0;
            // 
            // gcSector_code
            // 
            this.gcSector_code.Caption = "Код сектора";
            this.gcSector_code.FieldName = "Sector_code";
            this.gcSector_code.Name = "gcSector_code";
            this.gcSector_code.OptionsColumn.AllowEdit = false;
            this.gcSector_code.OptionsColumn.AllowFocus = false;
            this.gcSector_code.Visible = true;
            this.gcSector_code.VisibleIndex = 1;
            this.gcSector_code.Width = 71;
            // 
            // gcSector_name
            // 
            this.gcSector_name.Caption = "Название сектора";
            this.gcSector_name.FieldName = "Sector_name";
            this.gcSector_name.Name = "gcSector_name";
            this.gcSector_name.OptionsColumn.AllowEdit = false;
            this.gcSector_name.OptionsColumn.AllowFocus = false;
            this.gcSector_name.Visible = true;
            this.gcSector_name.VisibleIndex = 2;
            // 
            // gcDLM
            // 
            this.gcDLM.Caption = "Дата редактирования";
            this.gcDLM.FieldName = "DLM";
            this.gcDLM.Name = "gcDLM";
            this.gcDLM.OptionsColumn.AllowEdit = false;
            this.gcDLM.OptionsColumn.AllowFocus = false;
            this.gcDLM.Visible = true;
            this.gcDLM.VisibleIndex = 3;
            // 
            // gcULM
            // 
            this.gcULM.Caption = "Логин последнего редактирования";
            this.gcULM.FieldName = "ULM";
            this.gcULM.Name = "gcULM";
            this.gcULM.OptionsColumn.AllowEdit = false;
            this.gcULM.OptionsColumn.AllowFocus = false;
            this.gcULM.Visible = true;
            this.gcULM.VisibleIndex = 4;
            // 
            // gcActiveFrom
            // 
            this.gcActiveFrom.Caption = "Активен с";
            this.gcActiveFrom.FieldName = "ActiveFrom";
            this.gcActiveFrom.Name = "gcActiveFrom";
            this.gcActiveFrom.Visible = true;
            this.gcActiveFrom.VisibleIndex = 5;
            // 
            // gcUserLevel
            // 
            this.gcUserLevel.Caption = "Уровень";
            this.gcUserLevel.FieldName = "UserLevel";
            this.gcUserLevel.Name = "gcUserLevel";
            this.gcUserLevel.OptionsColumn.AllowEdit = false;
            this.gcUserLevel.OptionsColumn.AllowFocus = false;
            this.gcUserLevel.Visible = true;
            this.gcUserLevel.VisibleIndex = 6;
            // 
            // gcSectorType_id
            // 
            this.gcSectorType_id.Caption = "Тип сектора";
            this.gcSectorType_id.FieldName = "SectorType_id";
            this.gcSectorType_id.Name = "gcSectorType_id";
            this.gcSectorType_id.OptionsColumn.AllowEdit = false;
            this.gcSectorType_id.OptionsColumn.AllowFocus = false;
            this.gcSectorType_id.OptionsColumn.ShowInCustomizationForm = false;
            // 
            // gcSectorType_Name
            // 
            this.gcSectorType_Name.Caption = "Тип сектора";
            this.gcSectorType_Name.FieldName = "SectorType_Name";
            this.gcSectorType_Name.Name = "gcSectorType_Name";
            this.gcSectorType_Name.OptionsColumn.AllowEdit = false;
            this.gcSectorType_Name.OptionsColumn.AllowFocus = false;
            this.gcSectorType_Name.Visible = true;
            this.gcSectorType_Name.VisibleIndex = 7;
            // 
            // gcChannel
            // 
            this.gcChannel.Caption = "Канал";
            this.gcChannel.FieldName = "Channel";
            this.gcChannel.Name = "gcChannel";
            this.gcChannel.OptionsColumn.AllowEdit = false;
            this.gcChannel.OptionsColumn.AllowFocus = false;
            this.gcChannel.Visible = true;
            this.gcChannel.VisibleIndex = 8;
            // 
            // gcDSM
            // 
            this.gcDSM.Caption = "Дивизион";
            this.gcDSM.FieldName = "DSM";
            this.gcDSM.Name = "gcDSM";
            this.gcDSM.OptionsColumn.AllowEdit = false;
            this.gcDSM.OptionsColumn.AllowFocus = false;
            this.gcDSM.Visible = true;
            this.gcDSM.VisibleIndex = 9;
            // 
            // gcCity
            // 
            this.gcCity.Caption = "Город";
            this.gcCity.FieldName = "City";
            this.gcCity.Name = "gcCity";
            this.gcCity.OptionsColumn.AllowEdit = false;
            this.gcCity.OptionsColumn.AllowFocus = false;
            this.gcCity.Visible = true;
            this.gcCity.VisibleIndex = 10;
            // 
            // gcPartner_id
            // 
            this.gcPartner_id.Caption = "Партнер";
            this.gcPartner_id.FieldName = "Partner_id";
            this.gcPartner_id.Name = "gcPartner_id";
            this.gcPartner_id.OptionsColumn.ShowInCustomizationForm = false;
            // 
            // gcPartner_Name
            // 
            this.gcPartner_Name.Caption = "Партнер";
            this.gcPartner_Name.FieldName = "Partner_Name";
            this.gcPartner_Name.Name = "gcPartner_Name";
            this.gcPartner_Name.OptionsColumn.AllowEdit = false;
            this.gcPartner_Name.OptionsColumn.AllowFocus = false;
            this.gcPartner_Name.Visible = true;
            this.gcPartner_Name.VisibleIndex = 11;
            // 
            // gcWorkSchema
            // 
            this.gcWorkSchema.Caption = "Схема работы";
            this.gcWorkSchema.ColumnEdit = this.lueWorkSchema;
            this.gcWorkSchema.FieldName = "WorkSchema";
            this.gcWorkSchema.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.gcWorkSchema.Name = "gcWorkSchema";
            this.gcWorkSchema.Visible = true;
            this.gcWorkSchema.VisibleIndex = 12;
            // 
            // lueWorkSchema
            // 
            this.lueWorkSchema.AutoHeight = false;
            this.lueWorkSchema.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueWorkSchema.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("WorkSchemaId", "Id", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("WorkSchemaName", "Схема работы")});
            this.lueWorkSchema.DisplayMember = "WorkSchemaName";
            this.lueWorkSchema.Name = "lueWorkSchema";
            this.lueWorkSchema.NullText = "";
            this.lueWorkSchema.ValueMember = "WorkSchemaId";
            // 
            // gcSalary1
            // 
            this.gcSalary1.Caption = "Оклад 1, сумма";
            this.gcSalary1.ColumnEdit = this.iteSalary1;
            this.gcSalary1.FieldName = "Salary1";
            this.gcSalary1.Name = "gcSalary1";
            this.gcSalary1.Visible = true;
            this.gcSalary1.VisibleIndex = 13;
            // 
            // iteSalary1
            // 
            this.iteSalary1.AutoHeight = false;
            this.iteSalary1.Mask.EditMask = "\\d+";
            this.iteSalary1.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.iteSalary1.Name = "iteSalary1";
            // 
            // gcConstPart_sum
            // 
            this.gcConstPart_sum.Caption = "Постоянная часть, сумма";
            this.gcConstPart_sum.ColumnEdit = this.iteConstPart_sum;
            this.gcConstPart_sum.FieldName = "ConstPart_sum";
            this.gcConstPart_sum.Name = "gcConstPart_sum";
            this.gcConstPart_sum.Visible = true;
            this.gcConstPart_sum.VisibleIndex = 14;
            // 
            // iteConstPart_sum
            // 
            this.iteConstPart_sum.AutoHeight = false;
            this.iteConstPart_sum.Mask.EditMask = "\\d+";
            this.iteConstPart_sum.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.iteConstPart_sum.Name = "iteConstPart_sum";
            // 
            // gcAmortization_norm
            // 
            this.gcAmortization_norm.Caption = "Норма амортизации";
            this.gcAmortization_norm.ColumnEdit = this.iteAmortization_norm;
            this.gcAmortization_norm.FieldName = "Amortization_norm";
            this.gcAmortization_norm.Name = "gcAmortization_norm";
            this.gcAmortization_norm.Visible = true;
            this.gcAmortization_norm.VisibleIndex = 15;
            // 
            // iteAmortization_norm
            // 
            this.iteAmortization_norm.AutoHeight = false;
            this.iteAmortization_norm.Mask.EditMask = "f";
            this.iteAmortization_norm.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.iteAmortization_norm.Name = "iteAmortization_norm";
            // 
            // gcAmortization_sum
            // 
            this.gcAmortization_sum.Caption = "Амортизация, сумма";
            this.gcAmortization_sum.FieldName = "Amortization_sum";
            this.gcAmortization_sum.Name = "gcAmortization_sum";
            this.gcAmortization_sum.OptionsColumn.AllowEdit = false;
            this.gcAmortization_sum.OptionsColumn.AllowFocus = false;
            this.gcAmortization_sum.Visible = true;
            this.gcAmortization_sum.VisibleIndex = 16;
            // 
            // gcMileagePerDay_norm
            // 
            this.gcMileagePerDay_norm.Caption = "Нормативный пробег в день, км";
            this.gcMileagePerDay_norm.FieldName = "MileagePerDay_norm";
            this.gcMileagePerDay_norm.Name = "gcMileagePerDay_norm";
            this.gcMileagePerDay_norm.OptionsColumn.AllowEdit = false;
            this.gcMileagePerDay_norm.OptionsColumn.AllowFocus = false;
            this.gcMileagePerDay_norm.Visible = true;
            this.gcMileagePerDay_norm.VisibleIndex = 17;
            // 
            // gcMileagePerMonth_fact
            // 
            this.gcMileagePerMonth_fact.Caption = "Фактический пробег за месяц";
            this.gcMileagePerMonth_fact.ColumnEdit = this.iteMileagePerMonth_fact;
            this.gcMileagePerMonth_fact.FieldName = "MileagePerMonth_fact";
            this.gcMileagePerMonth_fact.Name = "gcMileagePerMonth_fact";
            this.gcMileagePerMonth_fact.Visible = true;
            this.gcMileagePerMonth_fact.VisibleIndex = 18;
            // 
            // iteMileagePerMonth_fact
            // 
            this.iteMileagePerMonth_fact.AutoHeight = false;
            this.iteMileagePerMonth_fact.Mask.EditMask = "\\d+";
            this.iteMileagePerMonth_fact.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.iteMileagePerMonth_fact.Name = "iteMileagePerMonth_fact";
            // 
            // gcFuelConsumption_norm
            // 
            this.gcFuelConsumption_norm.Caption = "Норма расхода топлива";
            this.gcFuelConsumption_norm.ColumnEdit = this.iteFuelConsumption_norm;
            this.gcFuelConsumption_norm.FieldName = "FuelConsumption_norm";
            this.gcFuelConsumption_norm.Name = "gcFuelConsumption_norm";
            this.gcFuelConsumption_norm.Visible = true;
            this.gcFuelConsumption_norm.VisibleIndex = 19;
            // 
            // iteFuelConsumption_norm
            // 
            this.iteFuelConsumption_norm.AutoHeight = false;
            this.iteFuelConsumption_norm.Mask.EditMask = "n";
            this.iteFuelConsumption_norm.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.iteFuelConsumption_norm.Name = "iteFuelConsumption_norm";
            // 
            // gcFuel_price
            // 
            this.gcFuel_price.Caption = "Цена топлива";
            this.gcFuel_price.ColumnEdit = this.iteFuel_price;
            this.gcFuel_price.FieldName = "Fuel_price";
            this.gcFuel_price.Name = "gcFuel_price";
            this.gcFuel_price.Visible = true;
            this.gcFuel_price.VisibleIndex = 20;
            // 
            // iteFuel_price
            // 
            this.iteFuel_price.AutoHeight = false;
            this.iteFuel_price.Mask.EditMask = "n";
            this.iteFuel_price.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.iteFuel_price.Name = "iteFuel_price";
            // 
            // gcFuelCompensation_sumWt
            // 
            this.gcFuelCompensation_sumWt.Caption = "Компенсация топлива, сумма без НДС";
            this.gcFuelCompensation_sumWt.FieldName = "FuelCompensation_sumWt";
            this.gcFuelCompensation_sumWt.Name = "gcFuelCompensation_sumWt";
            this.gcFuelCompensation_sumWt.OptionsColumn.AllowEdit = false;
            this.gcFuelCompensation_sumWt.OptionsColumn.AllowFocus = false;
            this.gcFuelCompensation_sumWt.Visible = true;
            this.gcFuelCompensation_sumWt.VisibleIndex = 21;
            // 
            // COMPESTRANSP
            // 
            this.COMPESTRANSP.Caption = "Компенсация общественного транспорта, сумма";
            this.COMPESTRANSP.ColumnEdit = this.luePublicTrCompensation_LimitID;
            this.COMPESTRANSP.FieldName = "PublicTrCompensation_LimitID";
            this.COMPESTRANSP.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.COMPESTRANSP.Name = "COMPESTRANSP";
            this.COMPESTRANSP.Visible = true;
            this.COMPESTRANSP.VisibleIndex = 22;
            // 
            // luePublicTrCompensation_LimitID
            // 
            this.luePublicTrCompensation_LimitID.AutoHeight = false;
            this.luePublicTrCompensation_LimitID.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.luePublicTrCompensation_LimitID.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("limits_id", "ID", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Code_name", "Параметр", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("value", "Значение")});
            this.luePublicTrCompensation_LimitID.DisplayMember = "value";
            this.luePublicTrCompensation_LimitID.Name = "luePublicTrCompensation_LimitID";
            this.luePublicTrCompensation_LimitID.NullText = "";
            this.luePublicTrCompensation_LimitID.ValueMember = "limits_id";
            // 
            // COMPESLINK
            // 
            this.COMPESLINK.Caption = "Компенсация связи, сумма";
            this.COMPESLINK.ColumnEdit = this.lueConnectionCompensation_LimitID;
            this.COMPESLINK.FieldName = "ConnectionCompensation_LimitID";
            this.COMPESLINK.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.COMPESLINK.Name = "COMPESLINK";
            this.COMPESLINK.Visible = true;
            this.COMPESLINK.VisibleIndex = 23;
            // 
            // lueConnectionCompensation_LimitID
            // 
            this.lueConnectionCompensation_LimitID.AutoHeight = false;
            this.lueConnectionCompensation_LimitID.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueConnectionCompensation_LimitID.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("limits_id", "ID", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Code_name", "Параметр", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("value", "Значение")});
            this.lueConnectionCompensation_LimitID.DisplayMember = "value";
            this.lueConnectionCompensation_LimitID.Name = "lueConnectionCompensation_LimitID";
            this.lueConnectionCompensation_LimitID.NullText = "";
            this.lueConnectionCompensation_LimitID.ValueMember = "limits_id";
            // 
            // COMPESFOOD
            // 
            this.COMPESFOOD.Caption = "Компенсация питания, сумма";
            this.COMPESFOOD.ColumnEdit = this.lueFoodCompensation_LimitID;
            this.COMPESFOOD.FieldName = "FoodCompensation_LimitID";
            this.COMPESFOOD.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.COMPESFOOD.Name = "COMPESFOOD";
            this.COMPESFOOD.Visible = true;
            this.COMPESFOOD.VisibleIndex = 24;
            // 
            // lueFoodCompensation_LimitID
            // 
            this.lueFoodCompensation_LimitID.AutoHeight = false;
            this.lueFoodCompensation_LimitID.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueFoodCompensation_LimitID.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("limits_id", "ID", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Code_name", "Параметр", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("value", "Значение")});
            this.lueFoodCompensation_LimitID.DisplayMember = "value";
            this.lueFoodCompensation_LimitID.Name = "lueFoodCompensation_LimitID";
            this.lueFoodCompensation_LimitID.NullText = "";
            this.lueFoodCompensation_LimitID.ValueMember = "limits_id";
            // 
            // COMPESOFFICE
            // 
            this.COMPESOFFICE.Caption = "Компенсация канцелярии, сумма";
            this.COMPESOFFICE.ColumnEdit = this.lueStationeryCompensation_LimitID;
            this.COMPESOFFICE.FieldName = "StationeryCompensation_LimitID";
            this.COMPESOFFICE.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.COMPESOFFICE.Name = "COMPESOFFICE";
            this.COMPESOFFICE.Visible = true;
            this.COMPESOFFICE.VisibleIndex = 25;
            // 
            // lueStationeryCompensation_LimitID
            // 
            this.lueStationeryCompensation_LimitID.AutoHeight = false;
            this.lueStationeryCompensation_LimitID.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueStationeryCompensation_LimitID.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("limits_id", "ID", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Code_name", "Параметр", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("value", "Значение")});
            this.lueStationeryCompensation_LimitID.DisplayMember = "value";
            this.lueStationeryCompensation_LimitID.Name = "lueStationeryCompensation_LimitID";
            this.lueStationeryCompensation_LimitID.NullText = "";
            this.lueStationeryCompensation_LimitID.ValueMember = "limits_id";
            // 
            // gcVacationPayment_sum
            // 
            this.gcVacationPayment_sum.Caption = "Отпускные, сумма";
            this.gcVacationPayment_sum.FieldName = "VacationPayment_sum";
            this.gcVacationPayment_sum.Name = "gcVacationPayment_sum";
            this.gcVacationPayment_sum.OptionsColumn.AllowEdit = false;
            this.gcVacationPayment_sum.OptionsColumn.AllowFocus = false;
            this.gcVacationPayment_sum.Visible = true;
            this.gcVacationPayment_sum.VisibleIndex = 26;
            // 
            // gcUST_sum
            // 
            this.gcUST_sum.Caption = "ЕСН, сумма";
            this.gcUST_sum.FieldName = "UST_sum";
            this.gcUST_sum.Name = "gcUST_sum";
            this.gcUST_sum.OptionsColumn.AllowEdit = false;
            this.gcUST_sum.OptionsColumn.AllowFocus = false;
            this.gcUST_sum.Visible = true;
            this.gcUST_sum.VisibleIndex = 27;
            // 
            // gcAdministration_sum
            // 
            this.gcAdministration_sum.Caption = "Администрирование, сумма";
            this.gcAdministration_sum.FieldName = "Administration_sum";
            this.gcAdministration_sum.Name = "gcAdministration_sum";
            this.gcAdministration_sum.OptionsColumn.AllowEdit = false;
            this.gcAdministration_sum.OptionsColumn.AllowFocus = false;
            this.gcAdministration_sum.Visible = true;
            this.gcAdministration_sum.VisibleIndex = 28;
            // 
            // COMPESLINKPAD
            // 
            this.COMPESLINKPAD.Caption = "Компенсация связи для планшета, сумма";
            this.COMPESLINKPAD.ColumnEdit = this.lueConnectionTabletCompensation_LimitID;
            this.COMPESLINKPAD.FieldName = "ConnectionTabletCompensation_LimitID";
            this.COMPESLINKPAD.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.COMPESLINKPAD.Name = "COMPESLINKPAD";
            this.COMPESLINKPAD.Visible = true;
            this.COMPESLINKPAD.VisibleIndex = 29;
            // 
            // lueConnectionTabletCompensation_LimitID
            // 
            this.lueConnectionTabletCompensation_LimitID.AutoHeight = false;
            this.lueConnectionTabletCompensation_LimitID.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueConnectionTabletCompensation_LimitID.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("limits_id", "ID", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Code_name", "Параметр", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("value", "Значение")});
            this.lueConnectionTabletCompensation_LimitID.DisplayMember = "value";
            this.lueConnectionTabletCompensation_LimitID.Name = "lueConnectionTabletCompensation_LimitID";
            this.lueConnectionTabletCompensation_LimitID.NullText = "";
            this.lueConnectionTabletCompensation_LimitID.ValueMember = "limits_id";
            // 
            // gcVariablePart_sum
            // 
            this.gcVariablePart_sum.Caption = "Переменная часть, сумма";
            this.gcVariablePart_sum.FieldName = "VariablePart_sum";
            this.gcVariablePart_sum.Name = "gcVariablePart_sum";
            this.gcVariablePart_sum.OptionsColumn.AllowEdit = false;
            this.gcVariablePart_sum.OptionsColumn.AllowFocus = false;
            this.gcVariablePart_sum.Visible = true;
            this.gcVariablePart_sum.VisibleIndex = 30;
            // 
            // gcWorkDays_total
            // 
            this.gcWorkDays_total.Caption = "Кол-во рабочих дней";
            this.gcWorkDays_total.ColumnEdit = this.iteWorkDays_total;
            this.gcWorkDays_total.FieldName = "WorkDays_total";
            this.gcWorkDays_total.Name = "gcWorkDays_total";
            this.gcWorkDays_total.Visible = true;
            this.gcWorkDays_total.VisibleIndex = 31;
            // 
            // iteWorkDays_total
            // 
            this.iteWorkDays_total.AutoHeight = false;
            this.iteWorkDays_total.DisplayFormat.FormatString = "#";
            this.iteWorkDays_total.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.iteWorkDays_total.EditFormat.FormatString = "#";
            this.iteWorkDays_total.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.iteWorkDays_total.Mask.EditMask = "\\d+";
            this.iteWorkDays_total.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.iteWorkDays_total.Name = "iteWorkDays_total";
            this.iteWorkDays_total.ValidateOnEnterKey = true;
            // 
            // gcWorkedDays_num
            // 
            this.gcWorkedDays_num.Caption = "Кол-во отработанных дней";
            this.gcWorkedDays_num.ColumnEdit = this.iteWorkedDays_num;
            this.gcWorkedDays_num.FieldName = "WorkedDays_num";
            this.gcWorkedDays_num.Name = "gcWorkedDays_num";
            this.gcWorkedDays_num.Visible = true;
            this.gcWorkedDays_num.VisibleIndex = 32;
            // 
            // iteWorkedDays_num
            // 
            this.iteWorkedDays_num.AutoHeight = false;
            this.iteWorkedDays_num.Mask.EditMask = "\\d+";
            this.iteWorkedDays_num.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.iteWorkedDays_num.Name = "iteWorkedDays_num";
            // 
            // gcAddCompensation
            // 
            this.gcAddCompensation.Caption = "Дополнительные Компенсации";
            this.gcAddCompensation.ColumnEdit = this.iteAddCompensation;
            this.gcAddCompensation.DisplayFormat.FormatString = "D";
            this.gcAddCompensation.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gcAddCompensation.FieldName = "AddCompensation";
            this.gcAddCompensation.Name = "gcAddCompensation";
            this.gcAddCompensation.Visible = true;
            this.gcAddCompensation.VisibleIndex = 33;
            // 
            // iteAddCompensation
            // 
            this.iteAddCompensation.AutoHeight = false;
            this.iteAddCompensation.Mask.EditMask = "\\d+";
            this.iteAddCompensation.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.iteAddCompensation.Name = "iteAddCompensation";
            // 
            // gcVariablePartClosingPerc_fact
            // 
            this.gcVariablePartClosingPerc_fact.Caption = "Фактическое закрытие переменной части, %";
            this.gcVariablePartClosingPerc_fact.ColumnEdit = this.iteVariablePartClosingPerc_fact;
            this.gcVariablePartClosingPerc_fact.FieldName = "VariablePartClosingPerc_fact";
            this.gcVariablePartClosingPerc_fact.Name = "gcVariablePartClosingPerc_fact";
            this.gcVariablePartClosingPerc_fact.Visible = true;
            this.gcVariablePartClosingPerc_fact.VisibleIndex = 34;
            // 
            // iteVariablePartClosingPerc_fact
            // 
            this.iteVariablePartClosingPerc_fact.AutoHeight = false;
            this.iteVariablePartClosingPerc_fact.Mask.EditMask = "P0";
            this.iteVariablePartClosingPerc_fact.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.iteVariablePartClosingPerc_fact.Name = "iteVariablePartClosingPerc_fact";
            // 
            // gcCorrectVarPartPrevP_sum
            // 
            this.gcCorrectVarPartPrevP_sum.Caption = "Корректировка переменной части предыдущего периода, сумма";
            this.gcCorrectVarPartPrevP_sum.ColumnEdit = this.iteCorrectVarPartPrevP_sum;
            this.gcCorrectVarPartPrevP_sum.FieldName = "CorrectVarPartPrevP_sum";
            this.gcCorrectVarPartPrevP_sum.Name = "gcCorrectVarPartPrevP_sum";
            this.gcCorrectVarPartPrevP_sum.Visible = true;
            this.gcCorrectVarPartPrevP_sum.VisibleIndex = 35;
            // 
            // iteCorrectVarPartPrevP_sum
            // 
            this.iteCorrectVarPartPrevP_sum.AutoHeight = false;
            this.iteCorrectVarPartPrevP_sum.Mask.EditMask = "D";
            this.iteCorrectVarPartPrevP_sum.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.iteCorrectVarPartPrevP_sum.Name = "iteCorrectVarPartPrevP_sum";
            this.iteCorrectVarPartPrevP_sum.ShowNullValuePromptWhenFocused = true;
            // 
            // gcComment
            // 
            this.gcComment.Caption = "Комментарий";
            this.gcComment.FieldName = "Comment";
            this.gcComment.Name = "gcComment";
            this.gcComment.Visible = true;
            this.gcComment.VisibleIndex = 36;
            // 
            // gcStatus
            // 
            this.gcStatus.Caption = "Статус сектора";
            this.gcStatus.ColumnEdit = this.lueStatus;
            this.gcStatus.FieldName = "Status";
            this.gcStatus.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.gcStatus.Name = "gcStatus";
            this.gcStatus.Visible = true;
            this.gcStatus.VisibleIndex = 37;
            // 
            // lueStatus
            // 
            this.lueStatus.AutoHeight = false;
            this.lueStatus.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueStatus.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("StatusSectorId", "Id", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("StatusSectorName", "Статус сектора")});
            this.lueStatus.DisplayMember = "StatusSectorName";
            this.lueStatus.Name = "lueStatus";
            this.lueStatus.ValueMember = "StatusSectorId";
            // 
            // gcWorkSchemaShadow
            // 
            this.gcWorkSchemaShadow.Caption = "Схема работы";
            this.gcWorkSchemaShadow.FieldName = "WorkSchema";
            this.gcWorkSchemaShadow.Name = "gcWorkSchemaShadow";
            this.gcWorkSchemaShadow.OptionsColumn.ShowInCustomizationForm = false;
            // 
            // COMPESTRANSPShadow
            // 
            this.COMPESTRANSPShadow.Caption = "Компенсация общественного транспорта, сумма";
            this.COMPESTRANSPShadow.FieldName = "PublicTrCompensation_LimitID";
            this.COMPESTRANSPShadow.Name = "COMPESTRANSPShadow";
            this.COMPESTRANSPShadow.OptionsColumn.ShowInCustomizationForm = false;
            // 
            // COMPESLINKShadow
            // 
            this.COMPESLINKShadow.Caption = "Компенсация связи, сумма";
            this.COMPESLINKShadow.FieldName = "ConnectionCompensation_LimitID";
            this.COMPESLINKShadow.Name = "COMPESLINKShadow";
            this.COMPESLINKShadow.OptionsColumn.ShowInCustomizationForm = false;
            // 
            // COMPESFOODShadow
            // 
            this.COMPESFOODShadow.Caption = "Компенсация питания, сумма";
            this.COMPESFOODShadow.FieldName = "FoodCompensation_LimitID";
            this.COMPESFOODShadow.Name = "COMPESFOODShadow";
            this.COMPESFOODShadow.OptionsColumn.ShowInCustomizationForm = false;
            // 
            // COMPESOFFICEShadow
            // 
            this.COMPESOFFICEShadow.Caption = "Компенсация канцелярии, сумма";
            this.COMPESOFFICEShadow.FieldName = "StationeryCompensation_LimitID";
            this.COMPESOFFICEShadow.Name = "COMPESOFFICEShadow";
            this.COMPESOFFICEShadow.OptionsColumn.ShowInCustomizationForm = false;
            // 
            // COMPESLINKPADShadow
            // 
            this.COMPESLINKPADShadow.Caption = "Компенсация связи для планшета, сумма";
            this.COMPESLINKPADShadow.FieldName = "ConnectionTabletCompensation_LimitID";
            this.COMPESLINKPADShadow.Name = "COMPESLINKPADShadow";
            this.COMPESLINKPADShadow.OptionsColumn.ShowInCustomizationForm = false;
            // 
            // gcStatusShadow
            // 
            this.gcStatusShadow.Caption = "Статус сектора";
            this.gcStatusShadow.FieldName = "Status";
            this.gcStatusShadow.Name = "gcStatusShadow";
            this.gcStatusShadow.OptionsColumn.ShowInCustomizationForm = false;
            // 
            // ControlBaseOfEmployees
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.splitContainerControl1);
            this.Name = "ControlBaseOfEmployees";
            this.Size = new System.Drawing.Size(1392, 447);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueWorkSchema)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.iteSalary1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.iteConstPart_sum)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.iteAmortization_norm)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.iteMileagePerMonth_fact)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.iteFuelConsumption_norm)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.iteFuel_price)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.luePublicTrCompensation_LimitID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueConnectionCompensation_LimitID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueFoodCompensation_LimitID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueStationeryCompensation_LimitID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueConnectionTabletCompensation_LimitID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.iteWorkDays_total)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.iteWorkedDays_num)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.iteAddCompensation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.iteVariablePartClosingPerc_fact)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.iteCorrectVarPartPrevP_sum)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueStatus)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraEditors.SimpleButton sbReCalc;
        private DevExpress.XtraEditors.SimpleButton sbLoadFromXLS;
        private DevExpress.XtraGrid.Columns.GridColumn gcSector_id;
        private DevExpress.XtraGrid.Columns.GridColumn gcSector_code;
        private DevExpress.XtraGrid.Columns.GridColumn gcSector_name;
        private DevExpress.XtraGrid.Columns.GridColumn gcDLM;
        private DevExpress.XtraGrid.Columns.GridColumn gcULM;
        private DevExpress.XtraGrid.Columns.GridColumn gcActiveFrom;
        private DevExpress.XtraGrid.Columns.GridColumn gcUserLevel;
        private DevExpress.XtraGrid.Columns.GridColumn gcSectorType_id;
        private DevExpress.XtraGrid.Columns.GridColumn gcSectorType_Name;
        private DevExpress.XtraGrid.Columns.GridColumn gcChannel;
        private DevExpress.XtraGrid.Columns.GridColumn gcDSM;
        private DevExpress.XtraGrid.Columns.GridColumn gcCity;
        private DevExpress.XtraGrid.Columns.GridColumn gcPartner_id;
        private DevExpress.XtraGrid.Columns.GridColumn gcWorkSchema;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit lueWorkSchema;
        private DevExpress.XtraGrid.Columns.GridColumn gcPartner_Name;
        private DevExpress.XtraGrid.Columns.GridColumn gcSalary1;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit iteSalary1;
        private DevExpress.XtraGrid.Columns.GridColumn gcConstPart_sum;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit iteConstPart_sum;
        private DevExpress.XtraGrid.Columns.GridColumn gcAmortization_norm;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit iteAmortization_norm;
        private DevExpress.XtraGrid.Columns.GridColumn gcAmortization_sum;
        private DevExpress.XtraGrid.Columns.GridColumn gcMileagePerDay_norm;
        private DevExpress.XtraGrid.Columns.GridColumn gcMileagePerMonth_fact;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit iteMileagePerMonth_fact;
        private DevExpress.XtraGrid.Columns.GridColumn gcFuelConsumption_norm;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit iteFuelConsumption_norm;
        private DevExpress.XtraGrid.Columns.GridColumn gcFuel_price;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit iteFuel_price;
        private DevExpress.XtraGrid.Columns.GridColumn gcFuelCompensation_sumWt;
        private DevExpress.XtraGrid.Columns.GridColumn COMPESTRANSP;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit luePublicTrCompensation_LimitID;
        private DevExpress.XtraGrid.Columns.GridColumn COMPESLINK;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit lueConnectionCompensation_LimitID;
        private DevExpress.XtraGrid.Columns.GridColumn COMPESFOOD;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit lueFoodCompensation_LimitID;
        private DevExpress.XtraGrid.Columns.GridColumn COMPESOFFICE;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit lueStationeryCompensation_LimitID;
        private DevExpress.XtraGrid.Columns.GridColumn gcVacationPayment_sum;
        private DevExpress.XtraGrid.Columns.GridColumn gcUST_sum;
        private DevExpress.XtraGrid.Columns.GridColumn gcAdministration_sum;
        private DevExpress.XtraGrid.Columns.GridColumn COMPESLINKPAD;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit lueConnectionTabletCompensation_LimitID;
        private DevExpress.XtraGrid.Columns.GridColumn gcVariablePart_sum;
        private DevExpress.XtraGrid.Columns.GridColumn gcWorkDays_total;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit iteWorkDays_total;
        private DevExpress.XtraGrid.Columns.GridColumn gcWorkedDays_num;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit iteWorkedDays_num;
        private DevExpress.XtraGrid.Columns.GridColumn gcAddCompensation;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit iteAddCompensation;
        private DevExpress.XtraGrid.Columns.GridColumn gcVariablePartClosingPerc_fact;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit iteVariablePartClosingPerc_fact;
        private DevExpress.XtraGrid.Columns.GridColumn gcCorrectVarPartPrevP_sum;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit iteCorrectVarPartPrevP_sum;
        private DevExpress.XtraGrid.Columns.GridColumn gcComment;
        private DevExpress.XtraGrid.Columns.GridColumn gcStatus;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit lueStatus;
        private DevExpress.XtraEditors.SimpleButton sbSave;
        private DevExpress.XtraGrid.Columns.GridColumn gcWorkSchemaShadow;
        private DevExpress.XtraGrid.Columns.GridColumn COMPESTRANSPShadow;
        private DevExpress.XtraGrid.Columns.GridColumn COMPESLINKShadow;
        private DevExpress.XtraGrid.Columns.GridColumn COMPESFOODShadow;
        private DevExpress.XtraGrid.Columns.GridColumn COMPESOFFICEShadow;
        private DevExpress.XtraGrid.Columns.GridColumn COMPESLINKPADShadow;
        private DevExpress.XtraGrid.Columns.GridColumn gcStatusShadow;
        private DevExpress.XtraEditors.SimpleButton bRestore;
    }
}
