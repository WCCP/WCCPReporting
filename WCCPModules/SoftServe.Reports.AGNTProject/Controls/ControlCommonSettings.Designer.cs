﻿namespace SoftServe.Reports.AGNTProject.Controls
{
    partial class ControlCommonSettings
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.group1 = new DevExpress.XtraEditors.GroupControl();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.editBarCostsLimits = new SoftServe.Reports.AGNTProject.Controls.EditorBar();
            this.gridCostsLimits = new DevExpress.XtraGrid.GridControl();
            this.grdViewCostsLimits = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colAmort = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colFood = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLink = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPad = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOffice = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTransport = new DevExpress.XtraGrid.Columns.GridColumn();
            this.group2 = new DevExpress.XtraEditors.GroupControl();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.gridbonusSize = new DevExpress.XtraGrid.GridControl();
            this.grdViewBonusSize = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColBonUserTypeShortName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColBonM1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColBonM2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColBonM3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColBonM4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColBonM5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.editBarBonus = new SoftServe.Reports.AGNTProject.Controls.EditorBar();
            this.group3 = new DevExpress.XtraEditors.GroupControl();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.gridAmortization = new DevExpress.XtraGrid.GridControl();
            this.grdViewAmortization = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColAmUserType_ShortName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColAmM1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColAmM2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColAmM3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColAmM4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColAmM5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.editBarAmortization = new SoftServe.Reports.AGNTProject.Controls.EditorBar();
            this.group4 = new DevExpress.XtraEditors.GroupControl();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.editBarCountrySet = new SoftServe.Reports.AGNTProject.Controls.EditorBar();
            this.gridCountrySettings = new DevExpress.XtraGrid.GridControl();
            this.grdViewCountrySett = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColCsPARTDAYOFFLIMIT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColCsECH = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColCsAVGDAYOFMONTH = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColCsVATRATE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.group5 = new DevExpress.XtraEditors.GroupControl();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.editBarPayments = new SoftServe.Reports.AGNTProject.Controls.EditorBar();
            this.gridTypeOfPayments = new DevExpress.XtraGrid.GridControl();
            this.grdViewTypeOfPayments = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColPaymentNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColtypeAgentPayment = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit5 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.splitContainerControl4 = new DevExpress.XtraEditors.SplitContainerControl();
            this.splitContainerControl3 = new DevExpress.XtraEditors.SplitContainerControl();
            this.splitContainerControl2 = new DevExpress.XtraEditors.SplitContainerControl();
            ((System.ComponentModel.ISupportInitialize)(this.group1)).BeginInit();
            this.group1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridCostsLimits)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdViewCostsLimits)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.group2)).BeginInit();
            this.group2.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridbonusSize)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdViewBonusSize)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.group3)).BeginInit();
            this.group3.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridAmortization)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdViewAmortization)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.group4)).BeginInit();
            this.group4.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridCountrySettings)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdViewCountrySett)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.group5)).BeginInit();
            this.group5.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridTypeOfPayments)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdViewTypeOfPayments)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl4)).BeginInit();
            this.splitContainerControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl3)).BeginInit();
            this.splitContainerControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).BeginInit();
            this.splitContainerControl2.SuspendLayout();
            this.SuspendLayout();
            // 
            // group1
            // 
            this.group1.Appearance.BackColor = System.Drawing.Color.WhiteSmoke;
            this.group1.Appearance.Options.UseBackColor = true;
            this.group1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.group1.Controls.Add(this.tableLayoutPanel1);
            this.group1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.group1.Location = new System.Drawing.Point(0, 0);
            this.group1.Name = "group1";
            this.group1.ShowCaption = false;
            this.group1.Size = new System.Drawing.Size(1233, 286);
            this.group1.TabIndex = 1;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.editBarCostsLimits, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.gridCostsLimits, 0, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(2, 2);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1229, 282);
            this.tableLayoutPanel1.TabIndex = 6;
            // 
            // editorBar1
            // 
            this.editBarCostsLimits.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.editBarCostsLimits.Dock = System.Windows.Forms.DockStyle.Fill;
            this.editBarCostsLimits.Location = new System.Drawing.Point(3, 3);
            this.editBarCostsLimits.Name = "editorBar1";
            this.editBarCostsLimits.Size = new System.Drawing.Size(1223, 29);
            this.editBarCostsLimits.TabIndex = 1;
            // 
            // gridCostsLimits
            // 
            this.gridCostsLimits.Cursor = System.Windows.Forms.Cursors.Default;
            this.gridCostsLimits.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridCostsLimits.Location = new System.Drawing.Point(3, 38);
            this.gridCostsLimits.MainView = this.grdViewCostsLimits;
            this.gridCostsLimits.Name = "gridCostsLimits";
            this.gridCostsLimits.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit1});
            this.gridCostsLimits.Size = new System.Drawing.Size(1223, 241);
            this.gridCostsLimits.TabIndex = 0;
            this.gridCostsLimits.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grdViewCostsLimits});
            // 
            // grdViewCostsLimits
            // 
            this.grdViewCostsLimits.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colAmort,
            this.colFood,
            this.colLink,
            this.colPad,
            this.colOffice,
            this.colTransport});
            this.grdViewCostsLimits.GridControl = this.gridCostsLimits;
            this.grdViewCostsLimits.Name = "grdViewCostsLimits";
            this.grdViewCostsLimits.OptionsBehavior.EditorShowMode = DevExpress.Utils.EditorShowMode.MouseDownFocused;
            this.grdViewCostsLimits.OptionsCustomization.AllowQuickHideColumns = false;
            this.grdViewCostsLimits.OptionsSelection.EnableAppearanceFocusedRow = false;
            this.grdViewCostsLimits.OptionsSelection.MultiSelect = true;
            this.grdViewCostsLimits.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CellSelect;
            this.grdViewCostsLimits.OptionsView.ShowGroupPanel = false;
            this.grdViewCostsLimits.OptionsView.ShowViewCaption = true;
            this.grdViewCostsLimits.ViewCaption = "Лимиты по затратам";
            // 
            // colAmort
            // 
            this.colAmort.AppearanceCell.Options.UseTextOptions = true;
            this.colAmort.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.colAmort.AppearanceHeader.Options.UseTextOptions = true;
            this.colAmort.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colAmort.ColumnEdit = this.repositoryItemTextEdit1;
            this.colAmort.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colAmort.FieldName = "AMORT_VALUE";
            this.colAmort.Name = "colAmort";
            this.colAmort.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this.colAmort.OptionsColumn.AllowMove = false;
            this.colAmort.OptionsColumn.AllowShowHide = false;
            this.colAmort.OptionsFilter.AllowAutoFilter = false;
            this.colAmort.OptionsFilter.AllowFilter = false;
            this.colAmort.ShowUnboundExpressionMenu = true;
            this.colAmort.Tag = "AMORT";
            this.colAmort.Visible = true;
            this.colAmort.VisibleIndex = 0;
            this.colAmort.Width = 174;
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AllowMouseWheel = false;
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.Mask.EditMask = "\\d{1,20}";
            this.repositoryItemTextEdit1.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.repositoryItemTextEdit1.Mask.ShowPlaceHolders = false;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // colFood
            // 
            this.colFood.AppearanceCell.Options.UseTextOptions = true;
            this.colFood.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.colFood.AppearanceHeader.Options.UseTextOptions = true;
            this.colFood.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colFood.ColumnEdit = this.repositoryItemTextEdit1;
            this.colFood.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colFood.FieldName = "COMPESFOOD_VALUE";
            this.colFood.Name = "colFood";
            this.colFood.OptionsColumn.AllowMove = false;
            this.colFood.OptionsColumn.AllowShowHide = false;
            this.colFood.OptionsFilter.AllowAutoFilter = false;
            this.colFood.OptionsFilter.AllowFilter = false;
            this.colFood.Tag = "COMPESFOOD";
            this.colFood.Visible = true;
            this.colFood.VisibleIndex = 1;
            this.colFood.Width = 174;
            // 
            // colLink
            // 
            this.colLink.AppearanceCell.Options.UseTextOptions = true;
            this.colLink.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.colLink.AppearanceHeader.Options.UseTextOptions = true;
            this.colLink.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colLink.ColumnEdit = this.repositoryItemTextEdit1;
            this.colLink.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colLink.FieldName = "COMPESLINK_VALUE";
            this.colLink.Name = "colLink";
            this.colLink.OptionsColumn.AllowMove = false;
            this.colLink.OptionsColumn.AllowShowHide = false;
            this.colLink.OptionsFilter.AllowAutoFilter = false;
            this.colLink.OptionsFilter.AllowFilter = false;
            this.colLink.Tag = "COMPESLINK";
            this.colLink.Visible = true;
            this.colLink.VisibleIndex = 2;
            this.colLink.Width = 174;
            // 
            // colPad
            // 
            this.colPad.AppearanceCell.Options.UseTextOptions = true;
            this.colPad.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.colPad.AppearanceHeader.Options.UseTextOptions = true;
            this.colPad.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colPad.ColumnEdit = this.repositoryItemTextEdit1;
            this.colPad.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colPad.FieldName = "COMPESLINKPAD_VALUE";
            this.colPad.Name = "colPad";
            this.colPad.OptionsColumn.AllowMove = false;
            this.colPad.OptionsColumn.AllowShowHide = false;
            this.colPad.OptionsFilter.AllowAutoFilter = false;
            this.colPad.OptionsFilter.AllowFilter = false;
            this.colPad.Tag = "COMPESLINKPAD";
            this.colPad.Visible = true;
            this.colPad.VisibleIndex = 3;
            this.colPad.Width = 174;
            // 
            // colOffice
            // 
            this.colOffice.AppearanceCell.Options.UseTextOptions = true;
            this.colOffice.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.colOffice.AppearanceHeader.Options.UseTextOptions = true;
            this.colOffice.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colOffice.ColumnEdit = this.repositoryItemTextEdit1;
            this.colOffice.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colOffice.FieldName = "COMPESOFFICE_VALUE";
            this.colOffice.Name = "colOffice";
            this.colOffice.OptionsColumn.AllowMove = false;
            this.colOffice.OptionsColumn.AllowShowHide = false;
            this.colOffice.OptionsFilter.AllowAutoFilter = false;
            this.colOffice.OptionsFilter.AllowFilter = false;
            this.colOffice.Tag = "COMPESOFFICE";
            this.colOffice.Visible = true;
            this.colOffice.VisibleIndex = 4;
            this.colOffice.Width = 174;
            // 
            // colTransport
            // 
            this.colTransport.AppearanceCell.Options.UseTextOptions = true;
            this.colTransport.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.colTransport.AppearanceHeader.Options.UseTextOptions = true;
            this.colTransport.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTransport.ColumnEdit = this.repositoryItemTextEdit1;
            this.colTransport.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colTransport.FieldName = "COMPESTRANSP_VALUE";
            this.colTransport.Name = "colTransport";
            this.colTransport.OptionsColumn.AllowMove = false;
            this.colTransport.OptionsColumn.AllowShowHide = false;
            this.colTransport.OptionsFilter.AllowAutoFilter = false;
            this.colTransport.OptionsFilter.AllowFilter = false;
            this.colTransport.Tag = "COMPESTRANSP";
            this.colTransport.Visible = true;
            this.colTransport.VisibleIndex = 5;
            this.colTransport.Width = 211;
            // 
            // group2
            // 
            this.group2.Appearance.BackColor = System.Drawing.Color.WhiteSmoke;
            this.group2.Appearance.Options.UseBackColor = true;
            this.group2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.group2.Controls.Add(this.tableLayoutPanel2);
            this.group2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.group2.Location = new System.Drawing.Point(0, 0);
            this.group2.Name = "group2";
            this.group2.ShowCaption = false;
            this.group2.Size = new System.Drawing.Size(631, 214);
            this.group2.TabIndex = 4;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Controls.Add(this.gridbonusSize, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.editBarBonus, 0, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(2, 2);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 2;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(627, 210);
            this.tableLayoutPanel2.TabIndex = 15;
            // 
            // gridbonusSize
            // 
            this.gridbonusSize.Cursor = System.Windows.Forms.Cursors.Default;
            this.gridbonusSize.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridbonusSize.Location = new System.Drawing.Point(3, 38);
            this.gridbonusSize.MainView = this.grdViewBonusSize;
            this.gridbonusSize.Name = "gridbonusSize";
            this.gridbonusSize.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit2});
            this.gridbonusSize.Size = new System.Drawing.Size(621, 169);
            this.gridbonusSize.TabIndex = 2;
            this.gridbonusSize.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grdViewBonusSize});
            // 
            // grdViewBonusSize
            // 
            this.grdViewBonusSize.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColBonUserTypeShortName,
            this.gridColBonM1,
            this.gridColBonM2,
            this.gridColBonM3,
            this.gridColBonM4,
            this.gridColBonM5});
            this.grdViewBonusSize.GridControl = this.gridbonusSize;
            this.grdViewBonusSize.Name = "grdViewBonusSize";
            this.grdViewBonusSize.OptionsBehavior.EditorShowMode = DevExpress.Utils.EditorShowMode.MouseDownFocused;
            this.grdViewBonusSize.OptionsCustomization.AllowQuickHideColumns = false;
            this.grdViewBonusSize.OptionsSelection.EnableAppearanceFocusedRow = false;
            this.grdViewBonusSize.OptionsSelection.MultiSelect = true;
            this.grdViewBonusSize.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CellSelect;
            this.grdViewBonusSize.OptionsView.ShowGroupPanel = false;
            this.grdViewBonusSize.OptionsView.ShowViewCaption = true;
            this.grdViewBonusSize.ViewCaption = "Размер бонусов";
            // 
            // gridColBonUserTypeShortName
            // 
            this.gridColBonUserTypeShortName.AppearanceCell.Options.UseTextOptions = true;
            this.gridColBonUserTypeShortName.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.gridColBonUserTypeShortName.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColBonUserTypeShortName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColBonUserTypeShortName.ColumnEdit = this.repositoryItemTextEdit2;
            this.gridColBonUserTypeShortName.FieldName = "UserType_ShortName";
            this.gridColBonUserTypeShortName.Name = "gridColBonUserTypeShortName";
            this.gridColBonUserTypeShortName.OptionsColumn.AllowEdit = false;
            this.gridColBonUserTypeShortName.OptionsColumn.AllowFocus = false;
            this.gridColBonUserTypeShortName.OptionsColumn.AllowMove = false;
            this.gridColBonUserTypeShortName.OptionsColumn.AllowShowHide = false;
            this.gridColBonUserTypeShortName.OptionsColumn.ReadOnly = true;
            this.gridColBonUserTypeShortName.OptionsFilter.AllowAutoFilter = false;
            this.gridColBonUserTypeShortName.OptionsFilter.AllowFilter = false;
            this.gridColBonUserTypeShortName.Tag = "UserType_ShortName";
            this.gridColBonUserTypeShortName.Visible = true;
            this.gridColBonUserTypeShortName.VisibleIndex = 0;
            // 
            // repositoryItemTextEdit2
            // 
            this.repositoryItemTextEdit2.AllowMouseWheel = false;
            this.repositoryItemTextEdit2.AutoHeight = false;
            this.repositoryItemTextEdit2.Mask.EditMask = "\\d{1,20}";
            this.repositoryItemTextEdit2.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.repositoryItemTextEdit2.Mask.ShowPlaceHolders = false;
            this.repositoryItemTextEdit2.Name = "repositoryItemTextEdit2";
            // 
            // gridColBonM1
            // 
            this.gridColBonM1.AppearanceCell.Options.UseTextOptions = true;
            this.gridColBonM1.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gridColBonM1.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColBonM1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColBonM1.ColumnEdit = this.repositoryItemTextEdit2;
            this.gridColBonM1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColBonM1.FieldName = "M1_VALUE";
            this.gridColBonM1.Name = "gridColBonM1";
            this.gridColBonM1.OptionsColumn.AllowMove = false;
            this.gridColBonM1.OptionsColumn.AllowShowHide = false;
            this.gridColBonM1.OptionsFilter.AllowAutoFilter = false;
            this.gridColBonM1.OptionsFilter.AllowFilter = false;
            this.gridColBonM1.Tag = "M1";
            this.gridColBonM1.Visible = true;
            this.gridColBonM1.VisibleIndex = 1;
            // 
            // gridColBonM2
            // 
            this.gridColBonM2.AppearanceCell.Options.UseTextOptions = true;
            this.gridColBonM2.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gridColBonM2.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColBonM2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColBonM2.ColumnEdit = this.repositoryItemTextEdit2;
            this.gridColBonM2.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColBonM2.FieldName = "M2_VALUE";
            this.gridColBonM2.Name = "gridColBonM2";
            this.gridColBonM2.OptionsColumn.AllowMove = false;
            this.gridColBonM2.OptionsColumn.AllowShowHide = false;
            this.gridColBonM2.OptionsFilter.AllowAutoFilter = false;
            this.gridColBonM2.OptionsFilter.AllowFilter = false;
            this.gridColBonM2.Tag = "M2";
            this.gridColBonM2.Visible = true;
            this.gridColBonM2.VisibleIndex = 2;
            // 
            // gridColBonM3
            // 
            this.gridColBonM3.AppearanceCell.Options.UseTextOptions = true;
            this.gridColBonM3.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gridColBonM3.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColBonM3.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColBonM3.ColumnEdit = this.repositoryItemTextEdit2;
            this.gridColBonM3.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColBonM3.FieldName = "M3_VALUE";
            this.gridColBonM3.Name = "gridColBonM3";
            this.gridColBonM3.OptionsColumn.AllowMove = false;
            this.gridColBonM3.OptionsColumn.AllowShowHide = false;
            this.gridColBonM3.OptionsFilter.AllowAutoFilter = false;
            this.gridColBonM3.OptionsFilter.AllowFilter = false;
            this.gridColBonM3.Tag = "M3";
            this.gridColBonM3.Visible = true;
            this.gridColBonM3.VisibleIndex = 3;
            // 
            // gridColBonM4
            // 
            this.gridColBonM4.AppearanceCell.Options.UseTextOptions = true;
            this.gridColBonM4.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gridColBonM4.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColBonM4.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColBonM4.ColumnEdit = this.repositoryItemTextEdit2;
            this.gridColBonM4.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColBonM4.FieldName = "M4_VALUE";
            this.gridColBonM4.Name = "gridColBonM4";
            this.gridColBonM4.OptionsColumn.AllowMove = false;
            this.gridColBonM4.OptionsColumn.AllowShowHide = false;
            this.gridColBonM4.OptionsFilter.AllowAutoFilter = false;
            this.gridColBonM4.OptionsFilter.AllowFilter = false;
            this.gridColBonM4.Tag = "M4";
            this.gridColBonM4.Visible = true;
            this.gridColBonM4.VisibleIndex = 4;
            // 
            // gridColBonM5
            // 
            this.gridColBonM5.AppearanceCell.Options.UseTextOptions = true;
            this.gridColBonM5.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gridColBonM5.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColBonM5.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColBonM5.ColumnEdit = this.repositoryItemTextEdit2;
            this.gridColBonM5.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColBonM5.FieldName = "M5_VALUE";
            this.gridColBonM5.Name = "gridColBonM5";
            this.gridColBonM5.OptionsColumn.AllowMove = false;
            this.gridColBonM5.OptionsColumn.AllowShowHide = false;
            this.gridColBonM5.OptionsFilter.AllowAutoFilter = false;
            this.gridColBonM5.OptionsFilter.AllowFilter = false;
            this.gridColBonM5.Tag = "M5";
            this.gridColBonM5.Visible = true;
            this.gridColBonM5.VisibleIndex = 5;
            // 
            // editorBar2
            // 
            this.editBarBonus.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.editBarBonus.Dock = System.Windows.Forms.DockStyle.Fill;
            this.editBarBonus.Location = new System.Drawing.Point(3, 3);
            this.editBarBonus.Name = "editorBar2";
            this.editBarBonus.Size = new System.Drawing.Size(621, 29);
            this.editBarBonus.TabIndex = 3;
            // 
            // group3
            // 
            this.group3.Appearance.BackColor = System.Drawing.Color.WhiteSmoke;
            this.group3.Appearance.Options.UseBackColor = true;
            this.group3.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.group3.Controls.Add(this.tableLayoutPanel3);
            this.group3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.group3.Location = new System.Drawing.Point(0, 0);
            this.group3.Name = "group3";
            this.group3.ShowCaption = false;
            this.group3.Size = new System.Drawing.Size(597, 213);
            this.group3.TabIndex = 6;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 1;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Controls.Add(this.gridAmortization, 0, 1);
            this.tableLayoutPanel3.Controls.Add(this.editBarAmortization, 0, 0);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(2, 2);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 2;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(593, 209);
            this.tableLayoutPanel3.TabIndex = 8;
            // 
            // gridAmortization
            // 
            this.gridAmortization.Cursor = System.Windows.Forms.Cursors.Default;
            this.gridAmortization.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridAmortization.Location = new System.Drawing.Point(3, 38);
            this.gridAmortization.MainView = this.grdViewAmortization;
            this.gridAmortization.Name = "gridAmortization";
            this.gridAmortization.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit3});
            this.gridAmortization.Size = new System.Drawing.Size(587, 168);
            this.gridAmortization.TabIndex = 4;
            this.gridAmortization.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grdViewAmortization});
            // 
            // grdViewAmortization
            // 
            this.grdViewAmortization.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColAmUserType_ShortName,
            this.gridColAmM1,
            this.gridColAmM2,
            this.gridColAmM3,
            this.gridColAmM4,
            this.gridColAmM5});
            this.grdViewAmortization.GridControl = this.gridAmortization;
            this.grdViewAmortization.Name = "grdViewAmortization";
            this.grdViewAmortization.OptionsBehavior.EditorShowMode = DevExpress.Utils.EditorShowMode.MouseDownFocused;
            this.grdViewAmortization.OptionsCustomization.AllowQuickHideColumns = false;
            this.grdViewAmortization.OptionsSelection.EnableAppearanceFocusedRow = false;
            this.grdViewAmortization.OptionsSelection.MultiSelect = true;
            this.grdViewAmortization.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CellSelect;
            this.grdViewAmortization.OptionsView.ShowGroupPanel = false;
            this.grdViewAmortization.OptionsView.ShowViewCaption = true;
            this.grdViewAmortization.ViewCaption = "Амортизация, сумма";
            // 
            // gridColAmUserType_ShortName
            // 
            this.gridColAmUserType_ShortName.AppearanceCell.Options.UseTextOptions = true;
            this.gridColAmUserType_ShortName.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.gridColAmUserType_ShortName.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColAmUserType_ShortName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColAmUserType_ShortName.ColumnEdit = this.repositoryItemTextEdit3;
            this.gridColAmUserType_ShortName.FieldName = "UserType_ShortName";
            this.gridColAmUserType_ShortName.Name = "gridColAmUserType_ShortName";
            this.gridColAmUserType_ShortName.OptionsColumn.AllowEdit = false;
            this.gridColAmUserType_ShortName.OptionsColumn.AllowFocus = false;
            this.gridColAmUserType_ShortName.OptionsColumn.AllowMove = false;
            this.gridColAmUserType_ShortName.OptionsColumn.AllowShowHide = false;
            this.gridColAmUserType_ShortName.OptionsColumn.ReadOnly = true;
            this.gridColAmUserType_ShortName.OptionsFilter.AllowAutoFilter = false;
            this.gridColAmUserType_ShortName.OptionsFilter.AllowFilter = false;
            this.gridColAmUserType_ShortName.Tag = "UserType_ShortName";
            this.gridColAmUserType_ShortName.Visible = true;
            this.gridColAmUserType_ShortName.VisibleIndex = 0;
            // 
            // repositoryItemTextEdit3
            // 
            this.repositoryItemTextEdit3.AllowMouseWheel = false;
            this.repositoryItemTextEdit3.AutoHeight = false;
            this.repositoryItemTextEdit3.Mask.EditMask = "\\d{1,20}";
            this.repositoryItemTextEdit3.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.repositoryItemTextEdit3.Mask.ShowPlaceHolders = false;
            this.repositoryItemTextEdit3.Name = "repositoryItemTextEdit3";
            // 
            // gridColAmM1
            // 
            this.gridColAmM1.AppearanceCell.Options.UseTextOptions = true;
            this.gridColAmM1.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gridColAmM1.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColAmM1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColAmM1.ColumnEdit = this.repositoryItemTextEdit3;
            this.gridColAmM1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColAmM1.FieldName = "M1_VALUE";
            this.gridColAmM1.Name = "gridColAmM1";
            this.gridColAmM1.OptionsColumn.AllowMove = false;
            this.gridColAmM1.OptionsColumn.AllowShowHide = false;
            this.gridColAmM1.OptionsFilter.AllowAutoFilter = false;
            this.gridColAmM1.OptionsFilter.AllowFilter = false;
            this.gridColAmM1.Tag = "M1";
            this.gridColAmM1.Visible = true;
            this.gridColAmM1.VisibleIndex = 1;
            // 
            // gridColAmM2
            // 
            this.gridColAmM2.AppearanceCell.Options.UseTextOptions = true;
            this.gridColAmM2.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gridColAmM2.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColAmM2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColAmM2.ColumnEdit = this.repositoryItemTextEdit3;
            this.gridColAmM2.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColAmM2.FieldName = "M2_VALUE";
            this.gridColAmM2.Name = "gridColAmM2";
            this.gridColAmM2.OptionsColumn.AllowMove = false;
            this.gridColAmM2.OptionsColumn.AllowShowHide = false;
            this.gridColAmM2.OptionsFilter.AllowAutoFilter = false;
            this.gridColAmM2.OptionsFilter.AllowFilter = false;
            this.gridColAmM2.Tag = "M2";
            this.gridColAmM2.Visible = true;
            this.gridColAmM2.VisibleIndex = 2;
            // 
            // gridColAmM3
            // 
            this.gridColAmM3.AppearanceCell.Options.UseTextOptions = true;
            this.gridColAmM3.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gridColAmM3.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColAmM3.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColAmM3.ColumnEdit = this.repositoryItemTextEdit3;
            this.gridColAmM3.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColAmM3.FieldName = "M3_VALUE";
            this.gridColAmM3.Name = "gridColAmM3";
            this.gridColAmM3.OptionsColumn.AllowMove = false;
            this.gridColAmM3.OptionsColumn.AllowShowHide = false;
            this.gridColAmM3.OptionsFilter.AllowAutoFilter = false;
            this.gridColAmM3.OptionsFilter.AllowFilter = false;
            this.gridColAmM3.Tag = "M3";
            this.gridColAmM3.Visible = true;
            this.gridColAmM3.VisibleIndex = 3;
            // 
            // gridColAmM4
            // 
            this.gridColAmM4.AppearanceCell.Options.UseTextOptions = true;
            this.gridColAmM4.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gridColAmM4.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColAmM4.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColAmM4.ColumnEdit = this.repositoryItemTextEdit3;
            this.gridColAmM4.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColAmM4.FieldName = "M4_VALUE";
            this.gridColAmM4.Name = "gridColAmM4";
            this.gridColAmM4.OptionsColumn.AllowMove = false;
            this.gridColAmM4.OptionsColumn.AllowShowHide = false;
            this.gridColAmM4.OptionsFilter.AllowAutoFilter = false;
            this.gridColAmM4.OptionsFilter.AllowFilter = false;
            this.gridColAmM4.Tag = "M4";
            this.gridColAmM4.Visible = true;
            this.gridColAmM4.VisibleIndex = 4;
            // 
            // gridColAmM5
            // 
            this.gridColAmM5.AppearanceCell.Options.UseTextOptions = true;
            this.gridColAmM5.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gridColAmM5.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColAmM5.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColAmM5.ColumnEdit = this.repositoryItemTextEdit3;
            this.gridColAmM5.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColAmM5.FieldName = "M5_VALUE";
            this.gridColAmM5.Name = "gridColAmM5";
            this.gridColAmM5.OptionsColumn.AllowMove = false;
            this.gridColAmM5.OptionsColumn.AllowShowHide = false;
            this.gridColAmM5.OptionsFilter.AllowAutoFilter = false;
            this.gridColAmM5.OptionsFilter.AllowFilter = false;
            this.gridColAmM5.Tag = "M5";
            this.gridColAmM5.Visible = true;
            this.gridColAmM5.VisibleIndex = 5;
            // 
            // editorBar3
            // 
            this.editBarAmortization.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.editBarAmortization.Dock = System.Windows.Forms.DockStyle.Fill;
            this.editBarAmortization.Location = new System.Drawing.Point(3, 3);
            this.editBarAmortization.Name = "editorBar3";
            this.editBarAmortization.Size = new System.Drawing.Size(587, 29);
            this.editBarAmortization.TabIndex = 5;
            // 
            // group4
            // 
            this.group4.Appearance.BackColor = System.Drawing.Color.WhiteSmoke;
            this.group4.Appearance.Options.UseBackColor = true;
            this.group4.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.group4.Controls.Add(this.tableLayoutPanel4);
            this.group4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.group4.Location = new System.Drawing.Point(0, 0);
            this.group4.Name = "group4";
            this.group4.ShowCaption = false;
            this.group4.Size = new System.Drawing.Size(631, 239);
            this.group4.TabIndex = 9;
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 1;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.Controls.Add(this.editBarCountrySet, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.gridCountrySettings, 0, 1);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(2, 2);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 2;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(627, 235);
            this.tableLayoutPanel4.TabIndex = 9;
            // 
            // editorBar4
            // 
            this.editBarCountrySet.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.editBarCountrySet.Dock = System.Windows.Forms.DockStyle.Fill;
            this.editBarCountrySet.Location = new System.Drawing.Point(3, 3);
            this.editBarCountrySet.Name = "editorBar4";
            this.editBarCountrySet.Size = new System.Drawing.Size(621, 29);
            this.editBarCountrySet.TabIndex = 7;
            // 
            // gridCountrySettings
            // 
            this.gridCountrySettings.Cursor = System.Windows.Forms.Cursors.Default;
            this.gridCountrySettings.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridCountrySettings.Location = new System.Drawing.Point(3, 38);
            this.gridCountrySettings.MainView = this.grdViewCountrySett;
            this.gridCountrySettings.Name = "gridCountrySettings";
            this.gridCountrySettings.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit4});
            this.gridCountrySettings.Size = new System.Drawing.Size(621, 194);
            this.gridCountrySettings.TabIndex = 6;
            this.gridCountrySettings.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grdViewCountrySett});
            // 
            // grdViewCountrySett
            // 
            this.grdViewCountrySett.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColCsPARTDAYOFFLIMIT,
            this.gridColCsECH,
            this.gridColCsAVGDAYOFMONTH,
            this.gridColCsVATRATE});
            this.grdViewCountrySett.GridControl = this.gridCountrySettings;
            this.grdViewCountrySett.Name = "grdViewCountrySett";
            this.grdViewCountrySett.OptionsBehavior.EditorShowMode = DevExpress.Utils.EditorShowMode.MouseDownFocused;
            this.grdViewCountrySett.OptionsCustomization.AllowQuickHideColumns = false;
            this.grdViewCountrySett.OptionsSelection.EnableAppearanceFocusedRow = false;
            this.grdViewCountrySett.OptionsSelection.MultiSelect = true;
            this.grdViewCountrySett.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CellSelect;
            this.grdViewCountrySett.OptionsView.ShowGroupPanel = false;
            this.grdViewCountrySett.OptionsView.ShowViewCaption = true;
            this.grdViewCountrySett.ViewCaption = "Настройка для страны";
            // 
            // gridColCsPARTDAYOFFLIMIT
            // 
            this.gridColCsPARTDAYOFFLIMIT.AppearanceCell.Options.UseTextOptions = true;
            this.gridColCsPARTDAYOFFLIMIT.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gridColCsPARTDAYOFFLIMIT.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColCsPARTDAYOFFLIMIT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColCsPARTDAYOFFLIMIT.ColumnEdit = this.repositoryItemTextEdit4;
            this.gridColCsPARTDAYOFFLIMIT.FieldName = "PARTDAYOFFLIMIT_VALUE";
            this.gridColCsPARTDAYOFFLIMIT.Name = "gridColCsPARTDAYOFFLIMIT";
            this.gridColCsPARTDAYOFFLIMIT.OptionsColumn.AllowMove = false;
            this.gridColCsPARTDAYOFFLIMIT.OptionsColumn.AllowShowHide = false;
            this.gridColCsPARTDAYOFFLIMIT.OptionsFilter.AllowAutoFilter = false;
            this.gridColCsPARTDAYOFFLIMIT.OptionsFilter.AllowFilter = false;
            this.gridColCsPARTDAYOFFLIMIT.Tag = "PARTDAYOFFLIMIT";
            this.gridColCsPARTDAYOFFLIMIT.Visible = true;
            this.gridColCsPARTDAYOFFLIMIT.VisibleIndex = 0;
            // 
            // repositoryItemTextEdit4
            // 
            this.repositoryItemTextEdit4.AllowMouseWheel = false;
            this.repositoryItemTextEdit4.AutoHeight = false;
            this.repositoryItemTextEdit4.Mask.EditMask = "#0.000;";
            this.repositoryItemTextEdit4.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit4.Mask.ShowPlaceHolders = false;
            this.repositoryItemTextEdit4.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit4.Name = "repositoryItemTextEdit4";
            // 
            // gridColCsECH
            // 
            this.gridColCsECH.AppearanceCell.Options.UseTextOptions = true;
            this.gridColCsECH.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gridColCsECH.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColCsECH.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColCsECH.ColumnEdit = this.repositoryItemTextEdit4;
            this.gridColCsECH.FieldName = "ECH_VALUE";
            this.gridColCsECH.Name = "gridColCsECH";
            this.gridColCsECH.OptionsColumn.AllowMove = false;
            this.gridColCsECH.OptionsColumn.AllowShowHide = false;
            this.gridColCsECH.OptionsFilter.AllowAutoFilter = false;
            this.gridColCsECH.OptionsFilter.AllowFilter = false;
            this.gridColCsECH.Tag = "ECH";
            this.gridColCsECH.Visible = true;
            this.gridColCsECH.VisibleIndex = 1;
            // 
            // gridColCsAVGDAYOFMONTH
            // 
            this.gridColCsAVGDAYOFMONTH.AppearanceCell.Options.UseTextOptions = true;
            this.gridColCsAVGDAYOFMONTH.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gridColCsAVGDAYOFMONTH.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColCsAVGDAYOFMONTH.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColCsAVGDAYOFMONTH.ColumnEdit = this.repositoryItemTextEdit4;
            this.gridColCsAVGDAYOFMONTH.FieldName = "AVGDAYOFMONTH_VALUE";
            this.gridColCsAVGDAYOFMONTH.Name = "gridColCsAVGDAYOFMONTH";
            this.gridColCsAVGDAYOFMONTH.OptionsColumn.AllowMove = false;
            this.gridColCsAVGDAYOFMONTH.OptionsColumn.AllowShowHide = false;
            this.gridColCsAVGDAYOFMONTH.OptionsFilter.AllowAutoFilter = false;
            this.gridColCsAVGDAYOFMONTH.OptionsFilter.AllowFilter = false;
            this.gridColCsAVGDAYOFMONTH.Tag = "AVGDAYOFMONTH";
            this.gridColCsAVGDAYOFMONTH.Visible = true;
            this.gridColCsAVGDAYOFMONTH.VisibleIndex = 2;
            // 
            // gridColCsVATRATE
            // 
            this.gridColCsVATRATE.AppearanceCell.Options.UseTextOptions = true;
            this.gridColCsVATRATE.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gridColCsVATRATE.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColCsVATRATE.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColCsVATRATE.ColumnEdit = this.repositoryItemTextEdit4;
            this.gridColCsVATRATE.FieldName = "VATRATE_VALUE";
            this.gridColCsVATRATE.Name = "gridColCsVATRATE";
            this.gridColCsVATRATE.OptionsColumn.AllowMove = false;
            this.gridColCsVATRATE.OptionsColumn.AllowShowHide = false;
            this.gridColCsVATRATE.OptionsFilter.AllowAutoFilter = false;
            this.gridColCsVATRATE.OptionsFilter.AllowFilter = false;
            this.gridColCsVATRATE.Tag = "VATRATE";
            this.gridColCsVATRATE.Visible = true;
            this.gridColCsVATRATE.VisibleIndex = 3;
            // 
            // group5
            // 
            this.group5.Appearance.BackColor = System.Drawing.Color.WhiteSmoke;
            this.group5.Appearance.Options.UseBackColor = true;
            this.group5.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.group5.Controls.Add(this.tableLayoutPanel5);
            this.group5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.group5.Location = new System.Drawing.Point(0, 0);
            this.group5.Name = "group5";
            this.group5.ShowCaption = false;
            this.group5.Size = new System.Drawing.Size(597, 240);
            this.group5.TabIndex = 11;
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.ColumnCount = 1;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel5.Controls.Add(this.editBarPayments, 0, 0);
            this.tableLayoutPanel5.Controls.Add(this.gridTypeOfPayments, 0, 1);
            this.tableLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel5.Location = new System.Drawing.Point(2, 2);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 2;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(593, 236);
            this.tableLayoutPanel5.TabIndex = 10;
            // 
            // editorBar5
            // 
            this.editBarPayments.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.editBarPayments.Dock = System.Windows.Forms.DockStyle.Fill;
            this.editBarPayments.Location = new System.Drawing.Point(3, 3);
            this.editBarPayments.Name = "editorBar5";
            this.editBarPayments.Size = new System.Drawing.Size(587, 29);
            this.editBarPayments.TabIndex = 9;
            // 
            // gridTypeOfPayments
            // 
            this.gridTypeOfPayments.Cursor = System.Windows.Forms.Cursors.Default;
            this.gridTypeOfPayments.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridTypeOfPayments.Location = new System.Drawing.Point(3, 38);
            this.gridTypeOfPayments.MainView = this.grdViewTypeOfPayments;
            this.gridTypeOfPayments.Name = "gridTypeOfPayments";
            this.gridTypeOfPayments.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit5});
            this.gridTypeOfPayments.Size = new System.Drawing.Size(587, 195);
            this.gridTypeOfPayments.TabIndex = 8;
            this.gridTypeOfPayments.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grdViewTypeOfPayments});
            // 
            // grdViewTypeOfPayments
            // 
            this.grdViewTypeOfPayments.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColPaymentNumber,
            this.gridColtypeAgentPayment});
            this.grdViewTypeOfPayments.GridControl = this.gridTypeOfPayments;
            this.grdViewTypeOfPayments.Name = "grdViewTypeOfPayments";
            this.grdViewTypeOfPayments.OptionsBehavior.EditorShowMode = DevExpress.Utils.EditorShowMode.MouseDownFocused;
            this.grdViewTypeOfPayments.OptionsCustomization.AllowQuickHideColumns = false;
            this.grdViewTypeOfPayments.OptionsSelection.EnableAppearanceFocusedRow = false;
            this.grdViewTypeOfPayments.OptionsSelection.MultiSelect = true;
            this.grdViewTypeOfPayments.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CellSelect;
            this.grdViewTypeOfPayments.OptionsView.ShowGroupPanel = false;
            this.grdViewTypeOfPayments.OptionsView.ShowViewCaption = true;
            this.grdViewTypeOfPayments.ViewCaption = "Виды агентского платежа";
            // 
            // gridColPaymentNumber
            // 
            this.gridColPaymentNumber.AppearanceCell.Options.UseTextOptions = true;
            this.gridColPaymentNumber.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.gridColPaymentNumber.Caption = "№ платежа";
            this.gridColPaymentNumber.Name = "gridColPaymentNumber";
            this.gridColPaymentNumber.OptionsColumn.AllowEdit = false;
            this.gridColPaymentNumber.OptionsColumn.AllowFocus = false;
            this.gridColPaymentNumber.OptionsColumn.AllowMove = false;
            this.gridColPaymentNumber.OptionsColumn.AllowShowHide = false;
            this.gridColPaymentNumber.OptionsColumn.AllowSize = false;
            this.gridColPaymentNumber.OptionsColumn.FixedWidth = true;
            this.gridColPaymentNumber.OptionsColumn.ReadOnly = true;
            this.gridColPaymentNumber.OptionsFilter.AllowAutoFilter = false;
            this.gridColPaymentNumber.OptionsFilter.AllowFilter = false;
            this.gridColPaymentNumber.Visible = true;
            this.gridColPaymentNumber.VisibleIndex = 0;
            // 
            // gridColtypeAgentPayment
            // 
            this.gridColtypeAgentPayment.AppearanceCell.Options.UseTextOptions = true;
            this.gridColtypeAgentPayment.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.gridColtypeAgentPayment.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColtypeAgentPayment.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.gridColtypeAgentPayment.Caption = "Виды агентских платежей";
            this.gridColtypeAgentPayment.ColumnEdit = this.repositoryItemTextEdit5;
            this.gridColtypeAgentPayment.FieldName = "PAYMENT_VALUE";
            this.gridColtypeAgentPayment.Name = "gridColtypeAgentPayment";
            this.gridColtypeAgentPayment.OptionsColumn.AllowMove = false;
            this.gridColtypeAgentPayment.OptionsColumn.AllowShowHide = false;
            this.gridColtypeAgentPayment.OptionsFilter.AllowAutoFilter = false;
            this.gridColtypeAgentPayment.OptionsFilter.AllowFilter = false;
            this.gridColtypeAgentPayment.Tag = "typeAgentPayment";
            this.gridColtypeAgentPayment.Visible = true;
            this.gridColtypeAgentPayment.VisibleIndex = 1;
            // 
            // repositoryItemTextEdit5
            // 
            this.repositoryItemTextEdit5.AllowMouseWheel = false;
            this.repositoryItemTextEdit5.AutoHeight = false;
            this.repositoryItemTextEdit5.Mask.AutoComplete = DevExpress.XtraEditors.Mask.AutoCompleteType.None;
            this.repositoryItemTextEdit5.Mask.ShowPlaceHolders = false;
            this.repositoryItemTextEdit5.Name = "repositoryItemTextEdit5";
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl1.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.Controls.Add(this.splitContainerControl4);
            this.splitContainerControl1.Panel1.Text = "Panel1";
            this.splitContainerControl1.Panel2.Controls.Add(this.splitContainerControl3);
            this.splitContainerControl1.Panel2.Text = "Panel2";
            this.splitContainerControl1.Size = new System.Drawing.Size(1233, 458);
            this.splitContainerControl1.SplitterPosition = 631;
            this.splitContainerControl1.TabIndex = 12;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // splitContainerControl4
            // 
            this.splitContainerControl4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl4.Horizontal = false;
            this.splitContainerControl4.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl4.Name = "splitContainerControl4";
            this.splitContainerControl4.Panel1.Controls.Add(this.group2);
            this.splitContainerControl4.Panel1.Text = "Panel1";
            this.splitContainerControl4.Panel2.Controls.Add(this.group4);
            this.splitContainerControl4.Panel2.Text = "Panel2";
            this.splitContainerControl4.Size = new System.Drawing.Size(631, 458);
            this.splitContainerControl4.SplitterPosition = 214;
            this.splitContainerControl4.TabIndex = 10;
            this.splitContainerControl4.Text = "splitContainerControl4";
            // 
            // splitContainerControl3
            // 
            this.splitContainerControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl3.Horizontal = false;
            this.splitContainerControl3.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl3.Name = "splitContainerControl3";
            this.splitContainerControl3.Panel1.Controls.Add(this.group3);
            this.splitContainerControl3.Panel1.Text = "Panel1";
            this.splitContainerControl3.Panel2.Controls.Add(this.group5);
            this.splitContainerControl3.Panel2.Text = "Panel2";
            this.splitContainerControl3.Size = new System.Drawing.Size(597, 458);
            this.splitContainerControl3.SplitterPosition = 213;
            this.splitContainerControl3.TabIndex = 12;
            this.splitContainerControl3.Text = "splitContainerControl3";
            // 
            // splitContainerControl2
            // 
            this.splitContainerControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl2.Horizontal = false;
            this.splitContainerControl2.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl2.Name = "splitContainerControl2";
            this.splitContainerControl2.Panel1.Controls.Add(this.group1);
            this.splitContainerControl2.Panel1.Text = "Panel1";
            this.splitContainerControl2.Panel2.Controls.Add(this.splitContainerControl1);
            this.splitContainerControl2.Panel2.Text = "Panel2";
            this.splitContainerControl2.Size = new System.Drawing.Size(1233, 749);
            this.splitContainerControl2.SplitterPosition = 286;
            this.splitContainerControl2.TabIndex = 13;
            this.splitContainerControl2.Text = "splitContainerControl2";
            // 
            // ControlCommonSettings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.splitContainerControl2);
            this.Name = "ControlCommonSettings";
            this.Size = new System.Drawing.Size(1233, 749);
            this.Load += new System.EventHandler(this.ControlCommonSettings_Load);
            ((System.ComponentModel.ISupportInitialize)(this.group1)).EndInit();
            this.group1.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridCostsLimits)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdViewCostsLimits)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.group2)).EndInit();
            this.group2.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridbonusSize)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdViewBonusSize)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.group3)).EndInit();
            this.group3.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridAmortization)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdViewAmortization)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.group4)).EndInit();
            this.group4.ResumeLayout(false);
            this.tableLayoutPanel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridCountrySettings)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdViewCountrySett)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.group5)).EndInit();
            this.group5.ResumeLayout(false);
            this.tableLayoutPanel5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridTypeOfPayments)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdViewTypeOfPayments)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl4)).EndInit();
            this.splitContainerControl4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl3)).EndInit();
            this.splitContainerControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).EndInit();
            this.splitContainerControl2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.GroupControl group1;
        private DevExpress.XtraEditors.GroupControl group2;
        private DevExpress.XtraEditors.GroupControl group3;
        private DevExpress.XtraEditors.GroupControl group4;
        private DevExpress.XtraEditors.GroupControl group5;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private Controls.EditorBar editBarCostsLimits;
        private DevExpress.XtraGrid.GridControl gridCostsLimits;
        private DevExpress.XtraGrid.Views.Grid.GridView grdViewCostsLimits;
        private DevExpress.XtraGrid.Columns.GridColumn colAmort;
        private DevExpress.XtraGrid.Columns.GridColumn colFood;
        private DevExpress.XtraGrid.Columns.GridColumn colLink;
        private DevExpress.XtraGrid.Columns.GridColumn colPad;
        private DevExpress.XtraGrid.Columns.GridColumn colOffice;
        private DevExpress.XtraGrid.Columns.GridColumn colTransport;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private DevExpress.XtraGrid.GridControl gridbonusSize;
        private DevExpress.XtraGrid.Views.Grid.GridView grdViewBonusSize;
        private Controls.EditorBar editBarBonus;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private DevExpress.XtraGrid.GridControl gridAmortization;
        private DevExpress.XtraGrid.Views.Grid.GridView grdViewAmortization;
        private Controls.EditorBar editBarAmortization;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private Controls.EditorBar editBarCountrySet;
        private DevExpress.XtraGrid.GridControl gridCountrySettings;
        private DevExpress.XtraGrid.Views.Grid.GridView grdViewCountrySett;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        private Controls.EditorBar editBarPayments;
        private DevExpress.XtraGrid.GridControl gridTypeOfPayments;
        private DevExpress.XtraGrid.Views.Grid.GridView grdViewTypeOfPayments;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColBonUserTypeShortName;
        private DevExpress.XtraGrid.Columns.GridColumn gridColBonM1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColBonM2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColBonM3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColBonM4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColBonM5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColAmUserType_ShortName;
        private DevExpress.XtraGrid.Columns.GridColumn gridColAmM1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColAmM2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColAmM3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColAmM4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColAmM5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColCsPARTDAYOFFLIMIT;
        private DevExpress.XtraGrid.Columns.GridColumn gridColCsECH;
        private DevExpress.XtraGrid.Columns.GridColumn gridColCsAVGDAYOFMONTH;
        private DevExpress.XtraGrid.Columns.GridColumn gridColCsVATRATE;
        private DevExpress.XtraGrid.Columns.GridColumn gridColtypeAgentPayment;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit3;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit4;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColPaymentNumber;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl2;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl4;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl3;
    }
}
