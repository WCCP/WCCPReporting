﻿namespace SoftServe.Reports.AGNTProject.Controls
{
    partial class ControlPartnerPayments
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ControlPartnerPayments));
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gcpaymentPartner_id = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcdistr_id = new DevExpress.XtraGrid.Columns.GridColumn();
            this.lueDistr = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.gcRegion_id = new DevExpress.XtraGrid.Columns.GridColumn();
            this.lurRegion = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.gcdistr_id2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcagentPayment_id = new DevExpress.XtraGrid.Columns.GridColumn();
            this.lueAgentPayment = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.gcComment = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcdate_from = new DevExpress.XtraGrid.Columns.GridColumn();
            this.dedate_from = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.gcdate_to = new DevExpress.XtraGrid.Columns.GridColumn();
            this.dedate_to = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.gcamount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.teamount = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueDistr)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lurRegion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueAgentPayment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dedate_from)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dedate_from.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dedate_to)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dedate_to.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teamount)).BeginInit();
            this.SuspendLayout();
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl1.Horizontal = false;
            this.splitContainerControl1.IsSplitterFixed = true;
            this.splitContainerControl1.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.splitContainerControl1.Panel1.Controls.Add(this.simpleButton2);
            this.splitContainerControl1.Panel1.Controls.Add(this.simpleButton1);
            this.splitContainerControl1.Panel1.Text = "Panel1";
            this.splitContainerControl1.Panel2.Controls.Add(this.gridControl1);
            this.splitContainerControl1.Panel2.Text = "Panel2";
            this.splitContainerControl1.Size = new System.Drawing.Size(560, 340);
            this.splitContainerControl1.SplitterPosition = 33;
            this.splitContainerControl1.TabIndex = 0;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // simpleButton2
            // 
            this.simpleButton2.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton2.Image")));
            this.simpleButton2.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButton2.Location = new System.Drawing.Point(33, 3);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(24, 24);
            this.simpleButton2.TabIndex = 1;
            this.simpleButton2.Text = "Добавить";
            this.simpleButton2.ToolTip = "Добавить";
            this.simpleButton2.Click += new System.EventHandler(this.simpleButton2_Click);
            // 
            // simpleButton1
            // 
            this.simpleButton1.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton1.Image")));
            this.simpleButton1.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButton1.Location = new System.Drawing.Point(3, 3);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(24, 24);
            this.simpleButton1.TabIndex = 0;
            this.simpleButton1.Text = "Редактировать";
            this.simpleButton1.ToolTip = "Сохранить";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // gridControl1
            // 
            this.gridControl1.Cursor = System.Windows.Forms.Cursors.Default;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.Location = new System.Drawing.Point(0, 0);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.lueDistr,
            this.lurRegion,
            this.lueAgentPayment,
            this.dedate_from,
            this.dedate_to,
            this.teamount});
            this.gridControl1.Size = new System.Drawing.Size(560, 302);
            this.gridControl1.TabIndex = 0;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            this.gridControl1.ProcessGridKey += new System.Windows.Forms.KeyEventHandler(this.gridControl1_ProcessGridKey);
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gcpaymentPartner_id,
            this.gcdistr_id,
            this.gcRegion_id,
            this.gcdistr_id2,
            this.gcagentPayment_id,
            this.gcComment,
            this.gcdate_from,
            this.gcdate_to,
            this.gcamount});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsBehavior.ImmediateUpdateRowPosition = false;
            this.gridView1.OptionsFind.ShowFindButton = false;
            this.gridView1.OptionsNavigation.AutoFocusNewRow = true;
            this.gridView1.OptionsSelection.MultiSelect = true;
            this.gridView1.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CellSelect;
            this.gridView1.RowCellStyle += new DevExpress.XtraGrid.Views.Grid.RowCellStyleEventHandler(this.gridView1_RowCellStyle);
            this.gridView1.ShownEditor += new System.EventHandler(this.gridView1_ShownEditor);
            this.gridView1.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.gridView1_CellValueChanged);
            this.gridView1.ValidatingEditor += new DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventHandler(this.gridView1_ValidatingEditor);
            // 
            // gcpaymentPartner_id
            // 
            this.gcpaymentPartner_id.Caption = "paymentPartner_id";
            this.gcpaymentPartner_id.FieldName = "paymentPartner_id";
            this.gcpaymentPartner_id.Name = "gcpaymentPartner_id";
            this.gcpaymentPartner_id.OptionsColumn.ShowInCustomizationForm = false;
            // 
            // gcdistr_id
            // 
            this.gcdistr_id.Caption = "Партнер";
            this.gcdistr_id.ColumnEdit = this.lueDistr;
            this.gcdistr_id.FieldName = "distr_id";
            this.gcdistr_id.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.gcdistr_id.Name = "gcdistr_id";
            this.gcdistr_id.Visible = true;
            this.gcdistr_id.VisibleIndex = 0;
            // 
            // lueDistr
            // 
            this.lueDistr.AutoHeight = false;
            this.lueDistr.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueDistr.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("DISTR_NAME", "Дистрибьютор")});
            this.lueDistr.DisplayMember = "DISTR_NAME";
            this.lueDistr.Name = "lueDistr";
            this.lueDistr.NullText = "";
            this.lueDistr.ValueMember = "DISTR_ID";
            this.lueDistr.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(this.LueDistrOnEditValueChanging);
            // 
            // gcRegion_id
            // 
            this.gcRegion_id.Caption = "Регион";
            this.gcRegion_id.ColumnEdit = this.lurRegion;
            this.gcRegion_id.FieldName = "Region_id";
            this.gcRegion_id.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.gcRegion_id.Name = "gcRegion_id";
            this.gcRegion_id.Visible = true;
            this.gcRegion_id.VisibleIndex = 1;
            // 
            // lurRegion
            // 
            this.lurRegion.AutoHeight = false;
            this.lurRegion.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lurRegion.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("GeographyName", "Регион")});
            this.lurRegion.DisplayMember = "GeographyName";
            this.lurRegion.Name = "lurRegion";
            this.lurRegion.NullText = "";
            this.lurRegion.ValueMember = "GeographyOldId";
            // 
            // gcdistr_id2
            // 
            this.gcdistr_id2.Caption = "Код партнера";
            this.gcdistr_id2.FieldName = "distr_id2";
            this.gcdistr_id2.Name = "gcdistr_id2";
            this.gcdistr_id2.OptionsColumn.AllowEdit = false;
            this.gcdistr_id2.Visible = true;
            this.gcdistr_id2.VisibleIndex = 2;
            // 
            // gcagentPayment_id
            // 
            this.gcagentPayment_id.Caption = "Вид агентского платежа";
            this.gcagentPayment_id.ColumnEdit = this.lueAgentPayment;
            this.gcagentPayment_id.FieldName = "agentPayment_id";
            this.gcagentPayment_id.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.gcagentPayment_id.Name = "gcagentPayment_id";
            this.gcagentPayment_id.Visible = true;
            this.gcagentPayment_id.VisibleIndex = 3;
            // 
            // lueAgentPayment
            // 
            this.lueAgentPayment.AutoHeight = false;
            this.lueAgentPayment.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueAgentPayment.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("typeAgentPayment", "Вид агентского платежа")});
            this.lueAgentPayment.DisplayMember = "typeAgentPayment";
            this.lueAgentPayment.Name = "lueAgentPayment";
            this.lueAgentPayment.NullText = "";
            this.lueAgentPayment.ValueMember = "agentPayment_id";
            // 
            // gcComment
            // 
            this.gcComment.Caption = "Комментарий";
            this.gcComment.FieldName = "Comment";
            this.gcComment.Name = "gcComment";
            this.gcComment.Visible = true;
            this.gcComment.VisibleIndex = 4;
            // 
            // gcdate_from
            // 
            this.gcdate_from.Caption = "Активен с";
            this.gcdate_from.ColumnEdit = this.dedate_from;
            this.gcdate_from.FieldName = "date_from";
            this.gcdate_from.Name = "gcdate_from";
            this.gcdate_from.Visible = true;
            this.gcdate_from.VisibleIndex = 5;
            // 
            // dedate_from
            // 
            this.dedate_from.AutoHeight = false;
            this.dedate_from.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dedate_from.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dedate_from.MaxValue = new System.DateTime(2100, 1, 1, 0, 0, 0, 0);
            this.dedate_from.MinValue = new System.DateTime(2000, 1, 1, 0, 0, 0, 0);
            this.dedate_from.Name = "dedate_from";
            // 
            // gcdate_to
            // 
            this.gcdate_to.Caption = "Закрыт с";
            this.gcdate_to.ColumnEdit = this.dedate_to;
            this.gcdate_to.FieldName = "date_to";
            this.gcdate_to.Name = "gcdate_to";
            this.gcdate_to.Visible = true;
            this.gcdate_to.VisibleIndex = 6;
            // 
            // dedate_to
            // 
            this.dedate_to.AutoHeight = false;
            this.dedate_to.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dedate_to.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dedate_to.MaxValue = new System.DateTime(2100, 1, 1, 0, 0, 0, 0);
            this.dedate_to.MinValue = new System.DateTime(2000, 1, 1, 0, 0, 0, 0);
            this.dedate_to.Name = "dedate_to";
            // 
            // gcamount
            // 
            this.gcamount.Caption = "Сумма Агентского платежа";
            this.gcamount.ColumnEdit = this.teamount;
            this.gcamount.FieldName = "amount";
            this.gcamount.Name = "gcamount";
            this.gcamount.Visible = true;
            this.gcamount.VisibleIndex = 7;
            // 
            // teamount
            // 
            this.teamount.AutoHeight = false;
            this.teamount.Mask.EditMask = "\\d+";
            this.teamount.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.teamount.Name = "teamount";
            // 
            // ControlPartnerPayments
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.splitContainerControl1);
            this.Name = "ControlPartnerPayments";
            this.Size = new System.Drawing.Size(560, 340);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueDistr)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lurRegion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueAgentPayment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dedate_from.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dedate_from)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dedate_to.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dedate_to)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teamount)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn gcdistr_id;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit lueDistr;
        private DevExpress.XtraGrid.Columns.GridColumn gcRegion_id;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit lurRegion;
        private DevExpress.XtraGrid.Columns.GridColumn gcpaymentPartner_id;
        private DevExpress.XtraGrid.Columns.GridColumn gcdistr_id2;
        private DevExpress.XtraGrid.Columns.GridColumn gcagentPayment_id;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit lueAgentPayment;
        private DevExpress.XtraGrid.Columns.GridColumn gcComment;
        private DevExpress.XtraGrid.Columns.GridColumn gcdate_from;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit dedate_from;
        private DevExpress.XtraGrid.Columns.GridColumn gcdate_to;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit dedate_to;
        private DevExpress.XtraGrid.Columns.GridColumn gcamount;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit teamount;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
    }
}
