﻿using System;
using System.ComponentModel;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading;
using Logica.Reports.Common;
using Logica.Reports.Common.WaitWindow;
using SoftServe.Core.Common.View;
using System.Windows.Forms;
using DevExpress.XtraBars;
using DevExpress.XtraTab;
using Logica.Reports.BaseReportControl;
using SoftServe.Reports.AGNTProject.DataAccess;
using SoftServe.Reports.AGNTProject.Models;
using SoftServe.Reports.AGNTProject.Utils;

namespace SoftServe.Reports.AGNTProject
{
    [ToolboxItem(false)]
    public partial class WccpUIControl : SimpleControlView
    {
        #region Fields
        private readonly SettingsForm _settingsForm;
        private Settings _currentSettings;
        private bool _isLoading;

        private const string COMPANY_DIRECTORY = "SoftServe";
        private const string PROGRAM_DIRECTORY = "WCCP Reporting";
        private const string LAYOUT_FILENAME = "AGNTProject.xml";
        #endregion

        #region Constructor

        public WccpUIControl()
        {
            InitializeComponent();
            string currentCulture = Thread.CurrentThread.CurrentCulture.Name;
            CultureInfo ci = new CultureInfo(currentCulture) {NumberFormat = {NumberDecimalSeparator = ","}};
            Thread.CurrentThread.CurrentCulture = ci;
            _settingsForm = new SettingsForm();
        }

        #endregion

        #region Methods
        public int InitReport()
        {
            WaitManager.StartWait();
            _settingsForm.LoadDataSources();
            AccessToApp access = AccessToApp.GetInstance();
            if (!access.IsAdmin)
            {
                chckIsPeriodClosed.Enabled = false;
            }
            WaitManager.StopWait();

            //RestoreLayout();
            return HandleSettingsForm() ? 0 : 1;
        }
        private void GenerateReport()
        {
            WaitManager.StartWait();
            //generate all selected reports
            foreach (XtraTabPage tab in tabControl.TabPages)
            {
                if (_currentSettings.SelectedReports.ContainsKey(tab.Text))
                {
                    if (tab.PageVisible == true)
                    {
                        foreach (var control in tab.Controls)
                        {
                            if (control is ITabReport)
                            {
                                ((ITabReport)control).LoadData(_currentSettings);
                            }
                        }
                    }
                    else tab.PageVisible = true;
                }
                else
                {
                    tab.PageVisible = false;
                }
            }
            HideExportButton(tabControl.SelectedTabPage);
            WaitManager.StopWait();
        }
        private bool HandleSettingsForm()
        {
            if (_settingsForm.ShowDialog(this) == DialogResult.OK)
            {
                _isLoading = true;
                _currentSettings = _settingsForm.Settings;
                lblPeriodCaptionControlSettings.Text = _currentSettings.PeriodName;

                string value;
                if (_currentSettings.SelectedReports.TryGetValue(Constants.CaptionPaymentWithPartner, out value))
                {
                    lbPeriodCaptionPartherPayment.Text = Int32.Parse(value) == 0
                        ? @"Все данные"
                        : _currentSettings.PeriodName;
                }

                lbPeriodCaptionEmploesBase.Text = _currentSettings.PeriodName;
                lbForecastPeriod.Text = _currentSettings.PeriodName;
                lbFactPeriod.Text = _currentSettings.PeriodName;
                
                //период открыт/закрыт
                chckIsPeriodClosed.Checked = _currentSettings.Status != 2;
                _currentSettings.StatusChanged += _currentSettings_StatusChanged;
                GenerateReport();
                _isLoading = !_isLoading;
                return true;
            }
            return false;
        }

        private void _currentSettings_StatusChanged(object sender, EventArgs e)
        {
            chckIsPeriodClosed.CheckedChanged -= chckIsPeriodClosed_CheckedChanged;
            chckIsPeriodClosed.Checked = _currentSettings.Status != 2;
            chckIsPeriodClosed.CheckedChanged += chckIsPeriodClosed_CheckedChanged;
        }

        private string SelectFilePath(string reportCaption, ExportToType exportType)
        {
            String res = String.Empty;
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Title = @"Сохранить";
            sfd.InitialDirectory = Assembly.GetExecutingAssembly().Location;
            sfd.FileName = reportCaption;
            sfd.Filter = String.Format("(*.{0})|*.{0}", exportType.ToString().ToLower());
            sfd.FilterIndex = 1;
            sfd.OverwritePrompt = true;
            sfd.RestoreDirectory = true;

            if (sfd.ShowDialog() != DialogResult.Cancel)
            {
                res = sfd.FileName;
            }

            return res;
        }
        private void ExportTab(ExportToType exportType)
        {
            string lPath = SelectFilePath(tabControl.SelectedTabPage.Text, exportType);

            if (!string.IsNullOrEmpty(lPath))
            {
                try
                {
                    //dataControl.Export(exportType, lPath);
                }
                catch (Exception)
                {
                    ErrorManager.ShowErrorBox("Ошибка при экспорте");
                }
            }
        }
        private void HideExportButton(XtraTabPage page)
        {
            if (page == tabCommonSettings)
                btnExportTo.Visibility = BarItemVisibility.Never;                    
            else            
                btnExportTo.Visibility = BarItemVisibility.Always;            
        }

        #endregion

        #region Event handlers
        private void exportToXlsxButton_ItemClick(object sender, ItemClickEventArgs e)
        {
            ExportTab(ExportToType.Xlsx);
        }

        private void exportToXlsButton_ItemClick(object sender, ItemClickEventArgs e)
        {
            ExportTab(ExportToType.Xls);
        }
        #endregion

        #region Save/restore layout

        private string GetLayoutPath()
        {
            string companyDirPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), COMPANY_DIRECTORY);

            if (!Directory.Exists(companyDirPath))
            {
                Directory.CreateDirectory(companyDirPath);
            }

            string programDirPath = Path.Combine(companyDirPath, PROGRAM_DIRECTORY);

            if (!Directory.Exists(programDirPath))
            {
                Directory.CreateDirectory(programDirPath);
            }

            return Path.Combine(programDirPath, LAYOUT_FILENAME);
        }

        public void SaveLayout()
        {
            string layoutPath = GetLayoutPath();
            foreach (XtraTabPage tab in tabControl.TabPages)
            {
                foreach (var control in tab.Controls)
                {
                    try
                    {
                        var report = control as ITabReport;
                        if (report != null)
                        {
                            report.SaveLayout(layoutPath);
                        }
                    }
                    catch (Exception)
                    {
                        // ignored
                    }
                }

            }
        }

        public void RestoreLayout()
        {
            string layoutPath = GetLayoutPath();
            foreach (XtraTabPage tab in tabControl.TabPages)
            {
                foreach (var control in tab.Controls)
                {
                    try
                    {
                        var report = control as ITabReport;
                        if (report != null)
                        {
                            report.RestoreLayout(layoutPath);
                        }
                    }
                    catch (Exception)
                    {
                        // ignored
                    }
                }
            }
        }

        #endregion

        public bool AllowClose()
        {
            IEditableReport editReport = (from object cont in tabControl.SelectedTabPage.Controls
                let report = cont as ITabReport
                where report != null
                select cont as IEditableReport).FirstOrDefault();

            if (editReport == null)
            {
                return true;
            }
            //If report is valid then try to save changes, otherwise do nothing till user correct it 
            if (editReport.IsDataValid)
            {
                if (editReport.SaveModifiedReportData())
                {
                    return true;
                }
                return false;
            }
            return true;
        }

        private void btnRefresh_ItemClick(object sender, ItemClickEventArgs e)
        {
            IEditableReport report = tabControl.SelectedTabPage.Controls.OfType<IEditableReport>().FirstOrDefault();

            if (report == null)
            {
                GenerateReport();
                return;
            }

            if (!report.IsDataValid)
                return;

            if (report.SaveModifiedReportData())
            {
                GenerateReport();
            }
        }
        private void btnSetting_ItemClick(object sender, ItemClickEventArgs e)
        {
            ITabReport tabReport = null;
            IEditableReport editReport = null;
            // Get tabReport report
            foreach (var cont in tabControl.SelectedTabPage.Controls)
            {
                ITabReport report = cont as ITabReport;
                if (report != null)
                {
                    tabReport = report;
                    editReport = cont as IEditableReport;
                    break;
                }
            }

            // Allow Settings form because report control has not implemented IEditableReport interface. 
            // So report is read only and has no data to save.
            if (editReport == null)
            {
                HandleSettingsForm();
                return;
            }
            //If report is valid then try to save changes, otherwise do nothing till user correct it 
            if (editReport.IsDataValid)
            {
                if (editReport.SaveModifiedReportData())
                {
                    if (HandleSettingsForm())
                        return;
                    tabReport.LoadData(_currentSettings);
                }
            }           
        }
        private void tabControl_SelectedPageChanged(object sender, TabPageChangedEventArgs e)
        {            
            HideExportButton(e.Page);
            if (e.Page == tabCommonSettings)
            {
                _currentSettings.Status = DataProvider.GetPeriodStatus(_currentSettings.PeriodId);
                _currentSettings_StatusChanged(null, null);
            }
            //Обновляем текущую страницу
            ITabReport tabReport = e.Page.Controls.OfType<ITabReport>().FirstOrDefault();
            if (e.Page.PageVisible)
            {
                if (tabReport != null)
                {
                    WaitManager.StartWait();
                    tabReport.SaveFilter = true;
                    tabReport.LoadData(_currentSettings);
                    WaitManager.StopWait();
                }
            }
        }

        private void tabControl_SelectedPageChanging(object sender, TabPageChangingEventArgs e)
        {
            ITabReport tabReport = null;
            IEditableReport editReport = null;
            // Get tabReport report
            foreach (var cont in e.PrevPage.Controls)
            {
                ITabReport report = cont as ITabReport;
                if (report != null)
                {
                    tabReport = report;
                    editReport = cont as IEditableReport;
                    break;
                }
            }
            // Return because report control has not implemented IEditableReport interface. 
            // So report is read only and has no data to save.
            if (editReport == null)
                return;

            if (!editReport.IsDataValid)
            {
                e.Cancel = true;
            }
            else if (editReport.HasChange)
            {
                if (editReport.SaveModifiedReportData())
                {
                    WaitManager.StartWait();
                    tabReport.SaveFilter = true;
                    tabReport.LoadData(_currentSettings);
                    e.Cancel = false;
                    WaitManager.StopWait();
                }
                else
                {
                    e.Cancel = true;
                }
            }
        }

        private void chckIsPeriodClosed_CheckedChanged(object sender, EventArgs e)
        {
            if (_isLoading)
                return;

                foreach (XtraTabPage page in tabControl.TabPages)
                {
                    foreach (var cont in page.Controls)
                    {
                        IEditableReport report = cont as IEditableReport;
                        if (report != null)
                        {
                            report.SetEditingMode(!chckIsPeriodClosed.Checked);
                        }
                    }
                }

            DataProvider.EditPeriodStatus(_currentSettings.PeriodId, chckIsPeriodClosed.Checked ? "9" : "2");
            _currentSettings.Status = chckIsPeriodClosed.Checked ? 9 : 2;
        }

        private void barLargeButtonItem1_ItemClick(object sender, ItemClickEventArgs e)
        {
            IExportData report = tabControl.SelectedTabPage.Controls.OfType<IExportData>().FirstOrDefault();
            if (report == null)
            {
                return;
            }
            
            SaveFileDialog saveFileDialog1 = new SaveFileDialog
            {
                Filter = @"xlsx files (*.xlsx)|*.xlsx|All files (*.*)|*.*",
                FilterIndex = 1,
                RestoreDirectory = true
            };
            if (tabControl.SelectedTabPage.Name == "tabCostsForecast")
            {
                string perc = _currentSettings.SelectedReports[Constants.CaptionCostsForecast];
                saveFileDialog1.FileName = "Прогноз_" + DateTime.Now.ToString("dd.MM.yy") + "_бонус=" + perc;
            }

            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                report.ExportToExel(saveFileDialog1.FileName);
            }
        }
    }
}
