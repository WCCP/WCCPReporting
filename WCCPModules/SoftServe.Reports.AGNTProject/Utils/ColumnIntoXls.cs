﻿using System.Collections.Generic;

namespace SoftServe.Reports.AGNTProject.Utils
{

    class ColumnIntoXlsLst
    {
        public ColumnIntoXlsLst()
        {
            ColumnIntoXls = new Dictionary<int, ColumnFromXls>();
        }
        public Dictionary<int, ColumnFromXls> ColumnIntoXls { get; private set; }
    }
}
