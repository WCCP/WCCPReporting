﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.XtraGrid.Views.Grid;
using SoftServe.Reports.AGNTProject.Models;

namespace SoftServe.Reports.AGNTProject.Utils.GridViewExtensions
{
    public static class GridViewExtender
    {
        public static bool IsSourceInputUnique(this GridView view, object val)
        {
            if (view.Name == "grdViewCostsLimits")
            {
                List<object> values = new List<object>();
                for (int i = 0; i < view.RowCount; i++)
                {
                    object celVal = view.GetRowCellValue(i, view.FocusedColumn);
                    if (celVal != null)
                        values.Add(view.GetRowCellValue(i, view.FocusedColumn));
                }
                List<object> uniq = values.Where(v => v.ToString() == val.ToString()).ToList();                
                
                return !(uniq.Count > 0);
            }
            return true;
        }
    }
}
namespace System.Runtime.CompilerServices
{
    public class ExtensionAttribute : Attribute { }
}