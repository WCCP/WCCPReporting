﻿namespace SoftServe.Reports.AGNTProject.Utils
{
    interface IExportData
    {
        /// <summary>
        /// Експорт в Excel
        /// </summary>
        /// <param name="path">Путь к файлу</param>
        /// <returns>true -все ок false - не все ок</returns>
        bool ExportToExel(string path);
    }
}


