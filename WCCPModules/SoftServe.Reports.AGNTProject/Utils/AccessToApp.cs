﻿using SoftServe.Reports.AGNTProject.DataAccess;

namespace SoftServe.Reports.AGNTProject.Utils
{
    public class AccessToApp
    {
        private static readonly AccessToApp Instance = new AccessToApp();
        private const int IsReportRole = 2;
        private const int IsReportEditEmpl = 4;
        private const int IsBaseEmplCenterRole = 8;
        private const int IsAdminRole = 16;

        public int GetAccess { get; private set; }
        public bool IsAdmin { get { return (GetAccess & IsAdminRole) == IsAdminRole; } }
        public bool IsBaseEmplCenter { get { return (GetAccess & IsBaseEmplCenterRole) == IsBaseEmplCenterRole; } }
        public bool IsReportEdit { get { return (GetAccess & IsReportEditEmpl) == IsReportEditEmpl; } }
        public bool IsReport { get { return (GetAccess & IsReportRole) == IsReportRole; } }


        private AccessToApp()
        {
            GetAccess = DataProvider.GetUserLevelAccess();
        }

        public static AccessToApp GetInstance()
        {
            return Instance;
        }
        public static void ResetAccess()
        {
            Instance.GetAccess = DataProvider.GetUserLevelAccess();
        }
    }
}
