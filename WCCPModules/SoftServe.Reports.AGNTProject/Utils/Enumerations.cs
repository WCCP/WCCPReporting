﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SoftServe.Reports.AGNTProject.Utils
{
    public enum CellStatus
    {
        New,
        InActive,
        Active,
        Modified,
        Empty,
        Unknown
    }

    public enum AccessLevel
    {
        ReportReader = 0,           //user has readolnly access for all reports.
        ReportReaderEmplEditor = 2, //user has readolnly access for all reports and can edit (write access) tab "База сотрудников".
        BaseEmplEditor = 4,         //user has read\write access for tab "База сотрудников". Remaining tabs are not accessible.
        ReportAdmin = 8             //user has full read\write access for all tabs.
    }

    public enum ItemState
    {
        Empty,
        New,
        Deactivated,
        Activated,
        InActive,
        Active,
        Modified
    }
}
