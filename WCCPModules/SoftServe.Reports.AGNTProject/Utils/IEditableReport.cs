﻿namespace SoftServe.Reports.AGNTProject.Utils
{
    public interface IEditableReport
    {
        /// <summary>
        /// Если на вкладке есть ошибочные данные
        /// </summary>
        bool IsDataValid { get; }
        /// <summary>
        /// Есть ли чего сохранять
        /// </summary>
        bool HasChange { get; }
        void SetEditingMode(bool isOpened);
        /// <summary>
        /// Сохраняем не сохраненное
        /// </summary>
        /// <returns>true - все хорошо; false - как-то не очень хорошо</returns>
        bool SaveModifiedReportData();
    }
}