﻿using System.Collections.Generic;

namespace SoftServe.Reports.AGNTProject.Utils
{
    /// <summary>
    /// Колонка в XML
    /// </summary>
    class ColumnFromXls
    {
        /// <summary>
        /// Колонка в XML
        /// </summary>
        /// <param name="columnName">Наименование колонки</param>
        /// <param name="columnFildName">Наименование в БД</param>
        /// <param name="columnXlsLiteral">Буква колонки в XLS файле</param>
        /// <param name="columnXlsNumber">Номер колнки в XLS файле</param>
        /// <param name="columnXlsRw">Редактируемая да/нет</param>
        /// <param name="columnType">Тип колонки</param>
        public ColumnFromXls(string columnName, string columnFildName, string columnXlsLiteral, int columnXlsNumber,
            bool columnXlsRw, string columnType)
        {
            ColumnName = columnName;
            ColumnXlsLiteral = columnXlsLiteral;
            ColumnXlsNumber = columnXlsNumber;
            ColumnXlsRw = columnXlsRw;
            ColumnFildName = columnFildName;
            ColumnType = columnType;
        }

        public string ColumnName { get; private set; }

        public string ColumnFildName { get; private set; }

        public string ColumnXlsLiteral { get; private set; }

        public int ColumnXlsNumber { get; private set; }

        public bool ColumnXlsRw { get; private set; }

        public string ColumnType { get; private set; }
    }

    static class ColumnFromXlsLst
    {
        public static Dictionary<string, ColumnFromXls> LstColumn
        {
            get
            {
                return new Dictionary<string, ColumnFromXls>()
                {
                    {"ID сектора", new ColumnFromXls("ID сектора", "Sector_id", "A", 0, false, "INT")},
                    {"Код сектора", new ColumnFromXls("Код сектора", "Sector_code", "B", 1, false, "INT")},
                    {"Название сектора", new ColumnFromXls("Название сектора", "Sector_name", "C", 2, false, "STRING")},
                    {"Дата редактирования", new ColumnFromXls("Дата редактирования", "DLM", "D", 3, false, "DATE")},
                    {
                        "Логин последнего редактирования",
                        new ColumnFromXls("Логин последнего редактирования", "ULM", "E", 4, false, "STRING")
                    },
                    {"Активен с", new ColumnFromXls("Активен с", "ActiveFrom", "F", 5, true, "DATE")},
                    {"Уровень", new ColumnFromXls("Уровень", "UserLevel", "G", 6, false, "STRING")},
                    {"Тип сектора", new ColumnFromXls("Тип сектора", "SectorType_Name", "H", 7, false, "STRING")},
                    {"Канал", new ColumnFromXls("Канал", "Channel", "I", 8, false, "STRING")},
                    {"Дивизион", new ColumnFromXls("Дивизион", "DSM", "J", 9, false, "STRING")},
                    {"Город", new ColumnFromXls("Город", "City", "C", 10, false, "STRING")},
                    {"Партнер", new ColumnFromXls("Партнер", "Partner_Name", "L", 11, false, "STRING")},
                    {"Схема работы", new ColumnFromXls("Схема работы", "WorkSchema", "M", 12, true, "STRING")},
                    {"Оклад 1, сумма", new ColumnFromXls("Оклад 1, сумма", "Salary1", "N", 13, true, "INT")},
                    {
                        "Постоянная часть, сумма",
                        new ColumnFromXls("Постоянная часть, сумма", "ConstPart_sum", "O", 14, true, "INT")
                    },
                    {
                        "Норма амортизации",
                        new ColumnFromXls("Норма амортизации", "Amortization_norm", "P", 15, true, "NUMERIC")
                    },
                    {
                        "Амортизация, сумма",
                        new ColumnFromXls("Амортизация, сумма", "Amortization_sum", "Q", 16, false, "NUMERIC")
                    },
                    {
                        "Нормативный пробег в день, км",
                        new ColumnFromXls("Нормативный пробег в день, км", "MileagePerDay_norm", "R", 17, false, "INT")
                    },
                    {
                        "Фактический пробег за месяц",
                        new ColumnFromXls("Фактический пробег за месяц", "MileagePerMonth_fact", "S", 18, true, "INT")
                    },
                    {
                        "Норма расхода топлива",
                        new ColumnFromXls("Норма расхода топлива", "FuelConsumption_norm", "T", 19, true, "NUMERIC")
                    },
                    {"Цена топлива", new ColumnFromXls("Цена топлива", "Fuel_price", "U", 20, true, "NUMERIC")},
                    {
                        "Компенсация топлива, сумма без НДС",
                        new ColumnFromXls("Компенсация топлива, сумма без НДС", "FuelCompensation_sumWt", "V", 21, false,
                            "NUMERIC")
                    },
                    {
                        "Компенсация общественного транспорта, сумма",
                        new ColumnFromXls("Компенсация общественного транспорта, сумма", "PublicTrCompensation_LimitID",
                            "W", 22, true, "INT")
                    },
                    {
                        "Компенсация связи, сумма",
                        new ColumnFromXls("Компенсация связи, сумма", "ConnectionCompensation_LimitID", "X", 23, true,
                            "INT")
                    },
                    {
                        "Компенсация питания, сумма",
                        new ColumnFromXls("Компенсация питания, сумма", "FoodCompensation_LimitID", "Y", 24, true, "INT")
                    },
                    {
                        "Компенсация канцелярии, сумма",
                        new ColumnFromXls("Компенсация канцелярии, сумма", "StationeryCompensation_LimitID", "Z", 25,
                            true, "INT")
                    },
                    {
                        "Отпускные, сумма",
                        new ColumnFromXls("Отпускные, сумма", "VacationPayment_sum", "AA", 26, false, "NUMERIC")
                    },
                    {"ЕСН, сумма", new ColumnFromXls("ЕСН, сумма", "UST_sum", "AB", 27, false, "NUMERIC")},
                    {
                        "Администрирование, сумма",
                        new ColumnFromXls("Администрирование, сумма", "Administration_sum", "AC", 28, false, "NUMERIC")
                    },
                    {
                        "Компенсация связи для планшета, сумма",
                        new ColumnFromXls("Компенсация связи для планшета, сумма",
                            "ConnectionTabletCompensation_LimitID", "AD", 29, true, "INT")
                    },
                    {
                        "Переменная часть, сумма",
                        new ColumnFromXls("Переменная часть, сумма", "VariablePart_sum", "AE", 30, false, "INT")
                    },
                    {
                        "Кол-во рабочих дней",
                        new ColumnFromXls("Кол-во рабочих дней", "WorkDays_total", "AF", 31, true, "INT")
                    },
                    {
                        "Кол-во отработанных дней",
                        new ColumnFromXls("Кол-во отработанных дней", "WorkedDays_num", "AG", 32, true, "INT")
                    },
                    {
                        "Дополнительные Компенсации",
                        new ColumnFromXls("Дополнительные Компенсации", "AddCompensation", "AH", 33, true, "INT")
                    },
                    {
                        "Фактическое закрытие переменной части, %",
                        new ColumnFromXls("Фактическое закрытие переменной части, %", "VariablePartClosingPerc_fact",
                            "AI", 34, true, "INT")
                    },
                    {
                        "Корректировка переменной части предыдущего периода, сумма",
                        new ColumnFromXls("Корректировка переменной части предыдущего периода, сумма",
                            "CorrectVarPartPrevP_sum", "AJ", 35, true, "INT")
                    },
                    {"Комментарий", new ColumnFromXls("Комментарий", "Comment", "AK", 36, true, "STRING")},
                    {"Статус сектора", new ColumnFromXls("Статус сектора", "Status", "AL", 37, false, "STRING")}
                };
            }
        }
    }
}
