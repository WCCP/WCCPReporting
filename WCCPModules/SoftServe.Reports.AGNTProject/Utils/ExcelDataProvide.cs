﻿using System;
using System.Globalization;
using System.IO;
using System.Windows.Forms;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using SoftServe.Reports.AGNTProject.Models;

namespace SoftServe.Reports.AGNTProject.Utils
{
    class ExcelDataProvide
    {
        private readonly string _filePath;
        private readonly BaseOfEmplFromXlsLst _baseOfEmplFromXlsLst;
        private ISheet _sheet;
        private readonly ColumnIntoXlsLst _columnIntoXlsLst = new ColumnIntoXlsLst();
        private int _countCol;
        
        public bool IsStatusLoad { get; private set; }

        public ExcelDataProvide(string path, BaseOfEmplFromXlsLst baseOfEmplFromXlsLst)
        {
            _filePath = path;
            _baseOfEmplFromXlsLst = baseOfEmplFromXlsLst;
            IsStatusLoad = true;
        }

        /// <summary>
        /// Открываем файл для чтения
        /// </summary>
        public void TryToReadFile()
        {
            _sheet = GetSheet(_filePath, 0);
        }
        
        /// <summary>
        /// Читаем данные
        /// </summary>
        public void RaadFileData()
        {
            ReadSheetDataIntoTable(_sheet, _baseOfEmplFromXlsLst);
        }

        private ISheet GetSheet(string path, int sheetNum)
        {
            IWorkbook book;

            using (FileStream file = new FileStream(path, FileMode.Open, FileAccess.Read))
            {
                if (path.Contains(@".xlsx"))
                    book = new XSSFWorkbook(file);
                else if (path.Contains(@".xls"))
                    book = new HSSFWorkbook(file);
                else throw new FormatException();
            }
            return book.GetSheetAt(sheetNum);
        }

        private void ReadSheetDataIntoTable(ISheet sheet, BaseOfEmplFromXlsLst baseOfEmplFromXlsLst)
        {
            string messageErr;
            //Проверяем заголовки
            if (!MapHeader(sheet, out messageErr))
            {
                MessageBox.Show(messageErr);
                IsStatusLoad = false;
                return;
            }
            try
            {
                //пропускаем первую строку
                for (int row = 1; row <= sheet.LastRowNum; row++)
                {
                    if (sheet.GetRow(row) != null)
                    {
                        string errMess;
                        IRow r = sheet.GetRow(row);
                        ICell cellIdSector = r.GetCell(0, MissingCellPolicy.CREATE_NULL_AS_BLANK);
                        ICell cellCodeSector = r.GetCell(1, MissingCellPolicy.CREATE_NULL_AS_BLANK);
                        KeyColumnIntoXls key = new KeyColumnIntoXls
                        {
                            IdSector = CreateKey(cellIdSector, out errMess),
                            ErrMess = errMess,
                            CodeSector = CreateKey(cellCodeSector, out errMess)
                        };
                        key.ErrMess = !string.IsNullOrEmpty(key.ErrMess) ? key.ErrMess + "; " + errMess : errMess;

                        //пропускаем первых 2-а столбца
                        for (int col = 2; col < _countCol; col++)
                        {
                            ColumnFromXls column;
                            //Если мы находимся на редактируемом солбце, то читаем его данные
                            if (_columnIntoXlsLst.ColumnIntoXls.TryGetValue(col, out column))
                            {
                                BaseOfEmplFromXls baseOfEmplFromXls = new BaseOfEmplFromXls(key);
                                //если при определении ключа была ошибка такую строку дальше не читаем
                                if (baseOfEmplFromXls.HasError)
                                    break;

                                //string errMessCell;
                                ICell cell = r.GetCell(col, MissingCellPolicy.CREATE_NULL_AS_BLANK);
                                baseOfEmplFromXls.ColumnFromXls = column;

                                ValueCell valueCell = GetValueFromCell(cell, column);
                                baseOfEmplFromXls.SetValue(valueCell);

                                baseOfEmplFromXlsLst.BaseOfEmplFromXls.Add(baseOfEmplFromXls);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                IsStatusLoad = false;
                MessageBox.Show(ex.Message);
            }
        }

        private ValueCell GetValueFromCell(ICell cell, ColumnFromXls column)
        {
            if (cell == null) return null;
            if (cell.CellType == CellType.Blank)
            {
                ValueCell valueCellBlank = new ValueCell(null, null);
                return valueCellBlank;
            }

            object val = null;
            string errMess = "";

            switch (cell.CellType)
            {
                case CellType.Numeric:
                    if (DateUtil.IsCellDateFormatted(cell))
                    {
                        if (column.ColumnType == "STRING" | column.ColumnType == "INT" | column.ColumnType == "NUMERIC")
                        {
                            errMess = "Не соответствие типов, ожидаем тип " + column.ColumnType;
                        }
                        else
                        {
                            val = cell.DateCellValue;
                        }
                    }
                    else
                    {
                        if (column.ColumnType == "DATE")
                        {
                            errMess = "Не соответствие типов, ожидаем тип DATE в формате dd.mm.yyyy";
                        }
                        if (column.ColumnType == "STRING")
                        {
                            val = cell.NumericCellValue.ToString(CultureInfo.CurrentCulture);
                        }
                        if (column.ColumnType == "INT")
                        {
                            int rezI;
                            if (int.TryParse(cell.NumericCellValue.ToString(CultureInfo.CurrentCulture), out rezI))
                            {
                                val = rezI;
                            }
                            else
                            {
                                val = null;
                                errMess = "Не соответствие типов, ожидаем тип " + column.ColumnType;
                            }
                        }
                        if (column.ColumnType == "NUMERIC")
                        {
                            double rezD;
                            if (double.TryParse(cell.NumericCellValue.ToString(CultureInfo.CurrentCulture), out rezD))
                            {
                                val = rezD;
                            }
                            else
                            {
                                val = null;
                                errMess = "Не соответствие типов, ожидаем тип " + column.ColumnType;
                            }
                        }
                    }
                    break;
                case CellType.String:
                    if (column.ColumnType == "STRING")
                    {
                        val = cell.StringCellValue;
                    }
                    if (column.ColumnType == "INT")
                    {
                        int rezI;
                        if (int.TryParse(cell.StringCellValue, out rezI))
                        {
                            val = rezI;
                        }
                        else
                        {
                            val = null;
                            errMess = "Не соответствие типов, ожидаем тип " + column.ColumnType;
                        }
                    }
                    if (column.ColumnType == "NUMERIC")
                    {
                        double rezD;
                        if (double.TryParse(cell.StringCellValue, out rezD))
                        {
                            val = rezD;
                        }
                        else
                        {
                            val = null;
                            errMess = "Не соответствие типов, ожидаем тип " + column.ColumnType;
                        }
                    }
                    if (column.ColumnType == "DATE")
                    {
                        DateTime rezDt;
                        if (DateTime.TryParse(cell.StringCellValue, out rezDt))
                        {
                            val = rezDt;
                        }
                        else
                        {
                            val = null;
                            errMess = "Не соответствие типов, ожидаем тип DATE в формате dd.mm.yyyy";
                        }

                    }
                    break;
            }

            ValueCell valueCell = new ValueCell(val, errMess);

            return valueCell;
        }

        //private object GetValueFromCell(ICell cell, ColumnFromXls column, out string errMess)
        //{
        //    errMess = "";
        //    if (cell == null) return null;
        //    if (cell.CellType == CellType.Blank) return null;

        //    object val = null;

        //    switch (cell.CellType)
        //    {
        //        case CellType.Numeric:
        //            if (DateUtil.IsCellDateFormatted(cell))
        //            {
        //                if (column.ColumnType == "STRING" | column.ColumnType == "INT" | column.ColumnType == "NUMERIC")
        //                {
        //                    errMess = "Не соответствие типов, ожидаем тип " + column.ColumnType;
        //                }
        //                else
        //                {
        //                    val = cell.DateCellValue;
        //                }
        //            }
        //            else
        //            {
        //                if (column.ColumnType == "DATE")
        //                {
        //                    errMess = "Не соответствие типов, ожидаем тип DATE в формате dd.mm.yyyy";
        //                }
        //                if (column.ColumnType == "STRING")
        //                {
        //                    val = cell.NumericCellValue.ToString(CultureInfo.CurrentCulture);
        //                }
        //                if (column.ColumnType == "INT")
        //                {
        //                    int rezI;
        //                    if (int.TryParse(cell.NumericCellValue.ToString(CultureInfo.CurrentCulture), out rezI))
        //                    {
        //                        val = rezI;
        //                    }
        //                    else
        //                    {
        //                        val = null;
        //                        errMess = "Не соответствие типов, ожидаем тип " + column.ColumnType;
        //                    }
        //                }
        //                if (column.ColumnType == "NUMERIC")
        //                {
        //                    double rezD;
        //                    if (double.TryParse(cell.NumericCellValue.ToString(CultureInfo.CurrentCulture), out rezD))
        //                    {
        //                        val = rezD;
        //                    }
        //                    else
        //                    {
        //                        val = null;
        //                        errMess = "Не соответствие типов, ожидаем тип " + column.ColumnType;
        //                    }
        //                }
        //            }
        //            break;
        //        case CellType.String:
        //            if (column.ColumnType == "STRING")
        //            {
        //                val = cell.StringCellValue;
        //            }
        //            if (column.ColumnType == "INT")
        //            {
        //                int rezI;
        //                if (int.TryParse(cell.StringCellValue, out rezI))
        //                {
        //                    val = rezI;
        //                }
        //                else
        //                {
        //                    val = null;
        //                    errMess = "Не соответствие типов, ожидаем тип " + column.ColumnType;
        //                }
        //            }
        //            if (column.ColumnType == "NUMERIC")
        //            {
        //                double rezD;
        //                if (double.TryParse(cell.StringCellValue, out rezD))
        //                {
        //                    val = rezD;
        //                }
        //                else
        //                {
        //                    val = null;
        //                    errMess = "Не соответствие типов, ожидаем тип " + column.ColumnType;
        //                }
        //            }
        //            if (column.ColumnType == "DATE")
        //            {
        //                DateTime rezDt;
        //                if (DateTime.TryParse(cell.StringCellValue, out rezDt))
        //                {
        //                    val = rezDt;
        //                }
        //                else
        //                {
        //                    val = null;
        //                    errMess = "Не соответствие типов, ожидаем тип DATE в формате dd.mm.yyyy";
        //                }

        //            }
        //            break;
        //    }
        //    return val;
        //}
        private int CreateKey(ICell cell, out string errMess)
        {
            errMess = null;
            if (cell.CellType == CellType.Blank)
            {
                errMess = "Ячейка в строке " + cell.RowIndex + " не может быть пустой!";
                return -1;
            }
            int res;
            switch (cell.CellType)
            {
                case CellType.Numeric:
                    if (DateUtil.IsCellDateFormatted(cell))
                    {
                        errMess = "Ячейка в строке " + cell.RowIndex + " не корректный тип данных!";
                        res = -1;
                    }
                    else
                    {
                        var resTmp = cell.NumericCellValue;
                        //проверим что-бы не было десятичных знаков
                        // ReSharper disable once CompareOfFloatsByEqualityOperator
                        // ReSharper disable once NegativeEqualityExpression
                        if (!(resTmp % 1 == 0))
                        {
                            errMess = "Ячейка в строке " + cell.RowIndex + " не корректный тип данных!";
                            res = -1;
                        }
                        else
                        {
                            res = Convert.ToInt32(resTmp);
                        }
                    }
                    break;
                case CellType.String:
                    if(!int.TryParse(cell.StringCellValue, out res))
                    {
                        errMess = "Ячейка в строке " + cell.RowIndex + " не корректный тип данных!";
                        res = -1;
                    }
                    break;
                default:
                    errMess = "Ячейка в строке " + cell.RowIndex + " не могу определить тип данных!";
                    res = -1;
                    break;
            }
            return res;
        }
        private bool MapHeader(ISheet sheet, out string messageErr)
        {
            messageErr = "OK";
            //первая строка -> заголовки
            IRow r = sheet.GetRow(0);
            _countCol = r.LastCellNum;
            //проверяем наличее обязательных полей ("ID сектора" и "Код сектора") они должны стоять первыми
            ICell idSectorCell = r.GetCell(0);
            if (idSectorCell.StringCellValue != "ID сектора")
            {
                messageErr = "Первым столбцом должно идти поле 'ID сектора'";
                return false;
            }
            ICell codeSectorCell = r.GetCell(1);
            if (codeSectorCell.StringCellValue != "Код сектора")
            {
                messageErr = "Вторым столбцом должно идти поле 'Код сектора'";
                return false;
            }
            for (int i = 0; i < _countCol; i++)
            {
                ICell cell = r.GetCell(i);
                if (cell != null)
                {
                    string cellVal = cell.StringCellValue;
                    ColumnFromXls column;
                    if (!ColumnFromXlsLst.LstColumn.TryGetValue(cellVal, out column))
                    {
                        messageErr = "Неизвестный столбец '" + cellVal + "'!";
                        return false;
                    }
                    if (column.ColumnXlsRw)
                    {
                        //Изменяемый столбец
                        _columnIntoXlsLst.ColumnIntoXls.Add(i, column);
                    }
                }
            }
            return true;
        }
    }
}
