﻿using System.Data;
using SoftServe.Reports.AGNTProject.DataAccess;

namespace SoftServe.Reports.AGNTProject.Utils
{
    public class StartCsParam
    {
        private readonly DataSet _data;
        public StartCsParam(int period, int countryId)
        {
            _data = DataProvider.GetPeriodId(period, countryId);
        }

        public int GetPeriodId()
        {
            var periodId = int.Parse(_data.Tables[0].Rows[0]["Period_id"].ToString());
            return periodId;
        }

        public int GetStatus()
        {
            var status = int.Parse(_data.Tables[0].Rows[0]["Status"].ToString());
            return status;
        }
    }
}
