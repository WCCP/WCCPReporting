﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SoftServe.Reports.AGNTProject.Utils
{
    public static class TextConstants
    {
      public const string  MSG_UNSAVED_DATA_FOUND = @"На форме есть не сохраненные данные, сохранить?";
    }
}
