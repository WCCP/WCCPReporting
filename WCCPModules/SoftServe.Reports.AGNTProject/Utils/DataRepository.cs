﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using SoftServe.Reports.AGNTProject.DataAccess;
using SoftServe.Reports.AGNTProject.Models;

namespace SoftServe.Reports.AGNTProject.Utils
{
    public static class DataRepository
    {
        private static List<ModeModel> _lookupSource;
        public static List<Region> Regions { get; set; }
        public static List<ModeModel> LookupSource
        {
            get
            {
                if (_lookupSource == null)
                {
                    _lookupSource = new List<ModeModel>() {new ModeModel() {ID = "0",Name = "Все"}, new ModeModel() { ID = "1", Name = "Текущий месяц" } };
                    return _lookupSource;
                }

                return _lookupSource;
            }
        }
        public static DataTable GetSettingsGridSource(int userLevel)
        {
            //bool isAllowed = userLevel == 4; //it means that user has only one role, that allow access only to the BaseOfEmployees tab page.
            bool isAllowed = AccessToApp.GetInstance().IsBaseEmplCenter ;

            DataTable t = new DataTable();
            t.Columns.Add("ReportId", typeof(int));
            t.Columns.Add("IsAllowed", typeof(bool));
            t.Columns.Add("ReportName", typeof(string));
            t.Columns.Add("Generate", typeof(bool));
            t.Columns.Add("ModeName", typeof(string));
            t.Columns.Add("ModeValue", typeof(string));
            DataRow r = t.NewRow();
            r["ReportId"] = 1;
            r["IsAllowed"] = !isAllowed;
            r["ReportName"] = Constants.CaptionCommonSetting;
            r["Generate"] = !isAllowed;
            r["ModeName"] = null;
            r["ModeValue"] = null;
            t.Rows.Add(r);
            r = t.NewRow();
            r["ReportId"] = 2;
            r["IsAllowed"] = !isAllowed;
            r["ReportName"] = Constants.CaptionPaymentWithPartner;
            r["Generate"] = !isAllowed;
            r["ModeName"] = "режим отображения";
            r["ModeValue"] = "0";
            t.Rows.Add(r);
            r = t.NewRow();
            r["ReportId"] = 3;
            r["IsAllowed"] = true;
            r["ReportName"] = Constants.CaptionEmployeesBase;
            r["Generate"] = true; //Always
            r["ModeName"] = null;
            r["ModeValue"] = null;
            t.Rows.Add(r);
            r = t.NewRow();
            r["ReportId"] = 4;
            r["IsAllowed"] = !isAllowed;
            r["ReportName"] = Constants.CaptionCostsForecast;
            r["Generate"] = !isAllowed;
            r["ModeName"] = "прогноз %";
            r["ModeValue"] = "0.00";
            t.Rows.Add(r);
            r = t.NewRow();
            r["ReportId"] = 5;
            r["IsAllowed"] = !isAllowed;
            r["ReportName"] = Constants.CaptionCostsFacts;
            r["Generate"] = !isAllowed;
            r["ModeName"] = null;
            r["ModeValue"] = null;
            t.Rows.Add(r);

            t.Select().ToList<DataRow>().ForEach(row => { row["Generate"] = false; });
            return t;
        }
    }

    public class ModeModel
    {
        public string ID { get; set; }
        public string Name { get; set; }

        public override string ToString()
        {
            return this.Name;
        }
    }
}