﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BLToolkit.Data.Sql;
using BLToolkit.Mapping;
using System.Data;
using SoftServe.Reports.AGNTProject.Models;

namespace SoftServe.Reports.AGNTProject.Utils
{
    public class ModelMapper
    {
        private static MappingSchema schema;

        public static List<T> MapTableToModel<T> (DataTable tblSource)
        {
            if (schema == null)
                schema = new MappingSchema();

            return schema.MapDataTableToList<T>(tblSource);
            
        }
    }
}
