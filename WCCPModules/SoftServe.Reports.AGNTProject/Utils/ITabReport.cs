﻿using SoftServe.Reports.AGNTProject.Models;

namespace SoftServe.Reports.AGNTProject
{
    interface ITabReport
    {
        string ReportText { get; }

        void LoadData(Settings settings);

        void SaveLayout(string path);

        void RestoreLayout(string path);

        bool SaveFilter { get; set; }
    }
}
