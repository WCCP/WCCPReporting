﻿using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Grid;
using SoftServe.Reports.AGNTProject.DataAccess;
using SoftServe.Reports.AGNTProject.Models;
using SoftServe.Reports.AGNTProject.Controls;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using DevExpress.XtraEditors;
using System.Windows.Forms;
using System.Drawing;
using System.Globalization;
using DevExpress.XtraGrid.Menu;
using DevExpress.Utils.Menu;
using DevExpress.XtraVerticalGrid.Events;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraEditors.Controls;
using SoftServe.Reports.AGNTProject.Utils.GridViewExtensions;

namespace SoftServe.Reports.AGNTProject.Utils
{
    class ControllerCS
    {
        AccessToApp access;
        int counter = 0;
        private bool _isPaymentView = false;
        private GridColumn gridColFake = new GridColumn() { Visible = false, OptionsColumn = { ShowInCustomizationForm = false } };
        private DataSet _source;
        private List<ISourceModel> _soutceRows;
        private GridView _view = null;
        private EditorBar _bar = null;

        private bool _isLoading = true;
        private bool _isEnterPressed;
        private bool _isPeriodClosed;
        private bool _isInputDataValid = true;
        private Dictionary<string, SimpleItemModel> _buffer = new Dictionary<string, SimpleItemModel>();

        public event EventHandler BecomeMain;       

        private Settings _currentSettings;
        public string ReportText { get; set; }
        public bool IsDataValid
        {
            get
            {
                var isValid = _view.ValidateEditor();

                if (!_isInputDataValid && isValid)
                    _isInputDataValid = isValid;

                return _isInputDataValid;
            }
            private set { _isInputDataValid = value; }
        }
        public ControllerCS(GridView view, EditorBar bar)
        {
            _view = view;
            _isPaymentView = view.Name == "grdViewTypeOfPayments";
            _bar = bar;
            _view.Columns.Add(gridColFake);
            AssignHendlers();
            _soutceRows = new List<ISourceModel>();
            access = AccessToApp.GetInstance();
        }

        #region Data loading methods
        public void Load(Settings settings)
        {
            _isLoading = true;
            _isPeriodClosed = settings.Status != 2;
            _currentSettings = settings;
            LoadData(_currentSettings.PeriodId);

            if(!_isPaymentView)
            SetGrigColumnsCaptions(_source.Tables[1]);
            SetGridsCaptions();
            _view.FocusedColumn = gridColFake;           
            _bar.BtnSave.Enabled = false;
            _bar.EnableEditButtons(false);
            _isLoading = !_isLoading;
        }
        private void LoadData(int periodId)
        {         
            switch (_view.Name)
            {
                case "grdViewCostsLimits":
                    _source = DataProvider.GetCostsLimits(periodId);
                    _soutceRows = ModelMapper.MapTableToModel<LimitsOfSpendsModel>(_source.Tables[0]).ConvertAll(r => (ISourceModel)r); break;
                case "grdViewBonusSize":
                    _source = DataProvider.GetBonusAmount(periodId);
                    _soutceRows = ModelMapper.MapTableToModel<AmortBonusModel>(_source.Tables[0]).ConvertAll(r => (ISourceModel)r); break;
                case "grdViewAmortization":
                    _source = DataProvider.GetAmortization(periodId);
                    _soutceRows = ModelMapper.MapTableToModel<AmortBonusModel>(_source.Tables[0]).ConvertAll(r => (ISourceModel)r); break;
                case "grdViewCountrySett":
                    _source = DataProvider.GetCountrySettings(periodId);
                    _soutceRows = ModelMapper.MapTableToModel<CountrySettingsModel>(_source.Tables[0]).ConvertAll(r => (ISourceModel)r); break;
                case "grdViewTypeOfPayments":
                    _source = new DataSet(); _source.Tables.Add(DataProvider.GetAgentPaymentTypes());
                    _soutceRows = ModelMapper.MapTableToModel<PaymentsModel>(_source.Tables[0]).ConvertAll(r => (ISourceModel)r); break;
            }
            AssignDataSource();   
        }
        private void AssignDataSource()
        {
            _soutceRows.ForEach(row => row.OnSourceNotification());
            _soutceRows.ForEach(row => row.PropertyChanged += PropertyChanged);
            switch (_view.Name)
            {
                case "grdViewCostsLimits":_view.GridControl.DataSource = _soutceRows.ConvertAll(r=>(LimitsOfSpendsModel)r);break;
                case "grdViewBonusSize":_view.GridControl.DataSource = _soutceRows.ConvertAll(r => (AmortBonusModel)r); break;
                case "grdViewAmortization":_view.GridControl.DataSource = _soutceRows.ConvertAll(r => (AmortBonusModel)r); break;
                case "grdViewCountrySett": _view.GridControl.DataSource = _soutceRows.ConvertAll(r => (CountrySettingsModel)r); break; break;
                case "grdViewTypeOfPayments": _view.GridControl.DataSource = _soutceRows.ConvertAll(r => (PaymentsModel)r); break; break; break;                    
            }
        }
        private void SaveData()
        {
            try
            {
                switch (_view.Name)
                {
                    case "grdViewCostsLimits": DataProvider.SaveCostsLimChanges(_currentSettings.PeriodId, _buffer); break;
                    case "grdViewBonusSize": DataProvider.SaveBonusSizeChanges(_currentSettings.PeriodId, _buffer); break;
                    case "grdViewAmortization": DataProvider.SaveAmortizationChanges(_currentSettings.PeriodId, _buffer); break;
                    case "grdViewCountrySett": DataProvider.SaveCountrySettingChanges(_currentSettings.PeriodId, _buffer); break;
                    case "grdViewTypeOfPayments": DataProvider.SavePaymentTypeChanges(_buffer); break;
                }
                _bar.BtnSave.Enabled = false;
            }
            catch (Exception e)
            {
                XtraMessageBox.Show(e.Message);
            }
            ClrearBuffer();
        }
        public bool IsModified
        {
            get
            {
                _view.CloseEditor();
                return _buffer.Count > 0;
            }
        }        
        public bool SaveModifiedReportData(bool trySave)
        {
            if (trySave)
            {
                try
                {
                    SaveData();
                }
                catch (Exception e)
                {
                    return false;
                }
            }
            ClrearBuffer();
            return true;
        }
        private void ClrearBuffer()
        {
            _buffer.Clear();
        }
        private void PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            string corePropName = string.Empty;
            string valuePropName = string.Empty;

            if (e.PropertyName.Contains("_STATUS"))
            {
                corePropName = e.PropertyName.Replace("_STATUS", string.Empty);
            }
            else if (e.PropertyName.Contains("_VALUE"))
            {
                corePropName = e.PropertyName.Replace("_VALUE", string.Empty);
            }
            if (string.IsNullOrEmpty(corePropName))
                return;
            valuePropName = string.Concat(corePropName, "_VALUE");

            ISourceModel mod = (ISourceModel)sender;
            SimpleItemModel m = null;
            ItemState state = mod.GetState(valuePropName);

            string id = mod.GetId(valuePropName);

            if (id != null && _buffer.ContainsKey(id))
                _buffer.Remove(id);

            if (state == ItemState.New)
            {
                string newId = counter.ToString() + "_NEW";
                counter++;
                mod.SetId(valuePropName, newId);
                m = new SimpleItemModel { Id = newId, Status = null, Value = mod.GetValue(valuePropName), Tag = corePropName, Tag2 = mod.Tag, State = state };
                if (_isPaymentView)
                {
                    m.Tag = mod.GetValue(valuePropName, _isPaymentView);
                }

            }
            else if (state == ItemState.Modified|| state == ItemState.Deactivated || state == ItemState.Activated)
            {
                m = new SimpleItemModel() { Id = mod.GetId(valuePropName), Value = mod.GetValue(valuePropName), Tag = corePropName, Tag2 = mod.Tag, State = state };
                if (_isPaymentView)
                {
                    m.Tag = mod.GetValue(valuePropName, _isPaymentView);
                }
            }           

            if (m != null)
            {
                _buffer.Add(m.Id, m);
            }

            _bar.BtnSave.Enabled = _buffer.Count > 0;
            HandleButtons(_view);
        }
        public void SaveLayout(string path)
        {
            //throw new NotImplementedException();
        }
        public void RestoreLayout(string path)
        {
            //throw new NotImplementedException();
        }
        public void SetEditingMode(bool isOpened)
        {
            _isPeriodClosed = !isOpened;
            if (_isPeriodClosed)
            {                
                _currentSettings.Status = 9;
                Load(_currentSettings);
            }
            else
            {
                _currentSettings.Status = 2;
            }
        }
        #endregion

        #region Event handlers        
        private void AddButton_Click(object sender, EventArgs e)
        {           
            ViewAddNewRow();
        }
        private void DelButtonClick(object sender, EventArgs e)
        {
            ISourceModel mod = (ISourceModel)_view.GetRow(_view.FocusedRowHandle);

            ItemState state = mod.GetState(_view.FocusedColumn.FieldName);

            if (state == ItemState.Active || state == ItemState.Activated)
            {
                _view.BeginUpdate();
                mod.SetStatus(_view.FocusedColumn.FieldName, 9);
                _view.EndUpdate();
            }

            if (sender is SimpleButton)
            {
                ((SimpleButton)sender).Enabled = false;
            }
            HandleButtons(_view);
        }
        private void SaveButtonClick(object sender, EventArgs e)
        {
            _currentSettings.Status = DataProvider.GetPeriodStatus(_currentSettings.PeriodId);            

            if (_currentSettings.Status == 9)
            { XtraMessageBox.Show(
                                "Период \"" + _currentSettings.PeriodName + "\" закрыт. Для того чтобы сохранить данные переоткройте период", @"Период закрыт",
                                MessageBoxButtons.OK);                          
                _currentSettings.OnStatusChanged();
                return;
            }
            
            _isLoading = true;            
            _view.CloseEditor();
            _view.FocusedColumn = gridColFake;

            try
            {
                SaveData();
                LoadData(_currentSettings.PeriodId);
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Сохранение", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            finally
            {
                _isLoading = false;
            }
        }
        private void ActivateButtonClick(object sender, EventArgs e)
        {
            ISourceModel mod = (ISourceModel)_view.GetRow(_view.FocusedRowHandle);

            ItemState state = mod.GetState(_view.FocusedColumn.FieldName);

            if (state == ItemState.InActive || state == ItemState.Deactivated)
            {
                _view.BeginUpdate();
                mod.SetStatus(_view.FocusedColumn.FieldName, 2);
                _view.EndUpdate();
            }

            if (sender is SimpleButton)
            {
                ((SimpleButton)sender).Enabled = false;
            }
            HandleButtons(_view);
        }
        private void grdView_CustomDrawCell(object sender, RowCellCustomDrawEventArgs e)
        {
            if (!e.Column.Visible)
                return;

            if (e.Column.Name == "gridColPaymentNumber")
            {
                e.DisplayText = (e.RowHandle + 1).ToString();
            }

            GridView view = sender as GridView;
            if (view == null)
                return;

            if (e.Column.FieldName == "UserType_ShortName"
                || e.Column.Name == "gridColPaymentNumber")
                return;

            ISourceModel mod = (ISourceModel)view.GetRow(e.RowHandle);

            string fieldNAme = e.Column.FieldName;

            ItemState state = mod.GetState(fieldNAme);
            if (state == ItemState.InActive || state == ItemState.Deactivated)
            {
                e.Appearance.BackColor = Color.FromArgb(169, 169, 169);
            }
            else if (state == ItemState.Modified)
            {
                e.Appearance.BackColor = Constants.ColorModified;
            }
            else if (state == ItemState.New)
            {
                e.Appearance.BackColor = Constants.ColorAdded;
            }
        }
        private void grdView_KeyDown(object sender, KeyEventArgs e)
        {
            GridView view = sender as GridView;
            if (view == null)
                return;

            switch (e.KeyCode)
            {
                case Keys.Enter:
                    _isEnterPressed = true;
                    HandleGridEnterKey();
                    e.Handled = true;
                    break;
                default:
                    break;
            }
        }
        private void grdView_ShowingEditor(object sender, CancelEventArgs e)
        {
            // If period is closed we do not show editor, so report is read only
            if (_isPeriodClosed || !access.IsAdmin)
            {
                e.Cancel = true;
                return;
            }
            // If Enter key was pressed, must go below to the next cell
            if (_isEnterPressed)
            {
                e.Cancel = true;
                _isEnterPressed = !_isEnterPressed;
                return;
            }

            GridView view = sender as GridView;
            if (view == null)
                return;

            ISourceModel mod = (ISourceModel)view.GetRow(view.FocusedRowHandle);

            string fieldNAme = view.FocusedColumn.FieldName;

            ItemState state = mod.GetState(fieldNAme);

            // Do not show editor if cell is not active
            e.Cancel = state == ItemState.InActive || state == ItemState.Deactivated;            
        }
        private void _view_ShownEditor(object sender, EventArgs e)
        {
            _bar.BtnDeactiv.Enabled = false;
        }
        private void grdView_ValidatingEditor(object sender, BaseContainerValidateEditorEventArgs e)
        {
            _isInputDataValid = true;

            GridView view = sender as GridView;
            if (view == null)
                return;
            string message = string.Empty;

            ISourceModel mod = (ISourceModel)view.GetRow(view.FocusedRowHandle);

            string fieldNAme = view.FocusedColumn.FieldName;

            ItemState state = mod.GetState(fieldNAme);
                        
            if ((state == ItemState.Active || state == ItemState.Modified) && e.Value.ToString() == string.Empty)
            {
                e.ErrorText = "Нельзя стереть активное значение";
                e.Valid = false;
                _isInputDataValid = e.Valid;
                return;
            }            
            try
            {
                if (view.Name == "grdViewCountrySett")
                    ValidateCountrySettings(e.Value, out message);
                else if (_isPaymentView)
                    return;
                else
                {
                    ValidateSimilarViews(e.Value, out message);
                    message = string.IsNullOrEmpty(message)&& !view.IsSourceInputUnique(e.Value) ?
                        string.Format("Значение {0} уже присутствует в столбце", e.Value.ToString()) : message;
                }
            }
            catch (Exception)
            {
                e.ErrorText = "Не корректный ввод";
                e.Valid = false;
                _isInputDataValid = e.Valid;
            }


            if (!string.IsNullOrEmpty(message))
            {
                e.ErrorText = message;
                e.Valid = false;
                _isInputDataValid = e.Valid;
            }            
            _isEnterPressed = false;                         
        }
        
        private void grdView_CellValueChanging(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            _bar.BtnSave.Enabled = true;
        }
        private void grdView_FocusedColumnChanged(object sender, FocusedColumnChangedEventArgs e)
        {
            HandleButtons(sender as GridView);
        }
        private void grdView_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            HandleButtons(sender as GridView);
        }
        private void grdView_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            if (e.MenuType == GridMenuType.Column)
            {
                GridViewColumnMenu menu = e.Menu as GridViewColumnMenu;
                if (menu == null)
                    return;

                foreach (DXMenuItem item in menu.Items)
                {
                    if (!item.Caption.StartsWith("Best Fit"))
                        item.Visible = false;
                }
            }
        }
        #endregion

        #region private methods                         
        private void AssignHendlers()
        {
            //AddRow button
            if (_view.Name == "grdViewCostsLimits" || _view.Name == "grdViewTypeOfPayments")
                _bar.BtnAddRow.Click += AddButton_Click;
            //Activate button
            _bar.BtnActiv.Click += ActivateButtonClick;
            //Deactivate button
            _bar.BtnDeactiv.Click += DelButtonClick;
            //Save button
            _bar.BtnSave.Click += SaveButtonClick;

            _view.CustomDrawCell += new RowCellCustomDrawEventHandler(this.grdView_CustomDrawCell);
            _view.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.grdView_PopupMenuShowing);
            _view.ShowingEditor += new CancelEventHandler(this.grdView_ShowingEditor);
            _view.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.grdView_FocusedRowChanged);
            _view.FocusedColumnChanged += new FocusedColumnChangedEventHandler(this.grdView_FocusedColumnChanged);
            _view.CellValueChanging += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.grdView_CellValueChanging);
            _view.KeyDown += new KeyEventHandler(this.grdView_KeyDown);
            _view.ValidatingEditor += new BaseContainerValidateEditorEventHandler(this.grdView_ValidatingEditor);
            _view.ShownEditor += _view_ShownEditor;
        }

        

        private void SetGridsCaptions()
        {
            _bar.SetCaption(_view.ViewCaption);           
            _view.OptionsView.ShowViewCaption = false;
        }
        private void SourseListAddRow<T>(object source) where T : ISourceModel
        {
            var sourceList = source as List<T>;
            if (sourceList == null)
                return;
            ISourceModel row = Activator.CreateInstance<T>();
            row.OnSourceNotification();
            row.PropertyChanged += PropertyChanged;
            sourceList.Add((T)row);
        }
        private void ViewAddNewRow()
        {
            _view.BeginUpdate();
            if (_view.GridControl.DataSource is List<LimitsOfSpendsModel>)
                SourseListAddRow<LimitsOfSpendsModel>(_view.GridControl.DataSource);
            else if (_view.GridControl.DataSource is List<PaymentsModel>)
                SourseListAddRow<PaymentsModel>(_view.GridControl.DataSource);
            _view.FocusedRowHandle = _view.RowCount - 1;
            _view.EndUpdate();
        }
        private void HandleGridEnterKey()
        {
            try
            {
                _view.CloseEditor();
            }
            catch
            {
                return;
            }
            var hasAnotherRow = _view.RowCount > _view.FocusedRowHandle + 1;
            int nextRowHandle = hasAnotherRow ? _view.FocusedRowHandle + 1 : _view.FocusedRowHandle;
            _view.FocusedRowHandle = nextRowHandle;
            _view.ClearSelection();
            _view.SelectCell(_view.FocusedRowHandle, _view.FocusedColumn);
        }
        public void ResetViewsCellFocus()
        {
            if (_view.ValidateEditor())
            {
                _view.FocusedColumnChanged -= grdView_FocusedColumnChanged;
                _view.FocusedRowChanged -= grdView_FocusedRowChanged;
                _view.FocusedColumn = gridColFake;
                _view.FocusedRowHandle = 0;
                _view.MoveFirst();
                _view.FocusedColumnChanged += grdView_FocusedColumnChanged;
                _view.FocusedRowChanged += grdView_FocusedRowChanged;
            }
        }
        private void SetGrigColumnsCaptions(DataTable table)
        {
            if (table == null) throw new NullReferenceException("Ошибка определения источника данных!");
            foreach (GridColumn col in _view.Columns)
            {
                if (col.Visible)
                {
                    col.Caption =
                        table.AsEnumerable()
                            .FirstOrDefault(r => r.Field<string>("Code") == col.Tag.ToString()).Field<string>("Code_name") ??
                        string.Empty;
                }
            }
        }
        private bool ValidateSimilarViews(object val, out string msg)
        {
            msg = string.Empty;
            if (string.IsNullOrEmpty(val.ToString()))
            {
                return true;
            }
            try
            {
                decimal res = Convert.ToDecimal(val);

                if (res.CompareTo(0) < 0)
                {
                    msg = "Значение не может быть меньше 1-цы";
                }
                /*else if (res.CompareTo(0) == 0)
                {
                    msg = "Значение не может быть равно 0";
                }*/
                else if (res.CompareTo(9999999999) > 0)
                {
                    msg = "Значение превышает допустимый диапазон!";
                }
            }
            catch (Exception)
            {
                msg = string.Format("Значеные за пределами допустимого диапазона. \nМаксимальное значение = {0}", decimal.MaxValue);
            }

            return msg == string.Empty;
        }
        private bool ValidateCountrySettings(object val, out string msg)
        {
            msg = string.Empty;

            try
            {
                decimal res = Convert.ToDecimal(val);

                if (_view.FocusedColumn.Name == "gridColCsAVGDAYOFMONTH" && (res < 0 || res > 31))
                {
                    msg = "Некорректное значение. Введите от 0.. до 31";
                    return false;
                }
                if (!(_view.FocusedColumn.Name == "gridColCsAVGDAYOFMONTH") && (res < 0 || res > 1))
                {
                    msg = "Некорректное значение. Введите от 0.. до 1";
                    return false;
                }                
            }
            catch (Exception)
            {
                msg = "Значение превышает допустимый диапазон!";
            }
            return msg == string.Empty;
        }
        private EditorBar GetBarByButton(object button)
        {
            EditorBar bar = null;

            if (button is SimpleButton)
            {
                SimpleButton b = button as SimpleButton;

                Control cnt = b.Parent;
                bar = cnt as EditorBar;
                int level = 1;
                while (bar == null && level < 4)
                {
                    cnt = cnt.Parent;
                    bar = cnt as EditorBar;
                    level++;
                }
            }
            return bar;
        }       
        private GroupControl GetGroupByView(GridView view)
        {
            GroupControl group = null;
            if (view != null)
            {
                group = view.GridControl.Parent.Parent as GroupControl;
            }
            return group;
        }
        private void HandleButtons(GridView view)
        {
            BecomeMain(_bar, new EventArgs());

            if (_isPeriodClosed || !access.IsAdmin || view == null)
                return;

            if (!view.FocusedColumn.Visible 
                || view.FocusedColumn.FieldName == "UserType_ShortName" 
                || view.FocusedColumn.FieldName == "gridColPaymentNumber")
            {
                _bar.BtnActiv.Enabled = false;
                _bar.BtnDeactiv.Enabled = false;
                return;
            }

            ISourceModel mod = (ISourceModel)view.GetRow(view.FocusedRowHandle);

            if (mod == null)
                return;
                            
            string fieldNAme = view.FocusedColumn.FieldName;

            ItemState state = mod.GetState(fieldNAme);
            if (state == ItemState.InActive || state == ItemState.Deactivated)
            {
                _bar.BtnActiv.Enabled = true;
                _bar.BtnDeactiv.Enabled = false;
            }
            else if (state == ItemState.Modified)
            {
                _bar.BtnActiv.Enabled = false;
                _bar.BtnDeactiv.Enabled = false;
            }
            else if (state == ItemState.New)
            {
                _bar.BtnActiv.Enabled = false;
                _bar.BtnDeactiv.Enabled = false;
            }
            else if (state == ItemState.Activated || state == ItemState.Active)
            {
                _bar.BtnActiv.Enabled = false;
                _bar.BtnDeactiv.Enabled = true;
            }
            else
            {
                _bar.BtnActiv.Enabled = false;
                _bar.BtnDeactiv.Enabled = false;
            }
            if (_view.Name == "grdViewCostsLimits" || _view.Name == "grdViewTypeOfPayments")
                _bar.BtnAddRow.Enabled = true;
        }
        #endregion
    }
}          
