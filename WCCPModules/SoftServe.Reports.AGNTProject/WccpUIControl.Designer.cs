﻿using SoftServe.Reports.AGNTProject.Controls;

namespace SoftServe.Reports.AGNTProject
{
    partial class WccpUIControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(WccpUIControl));
            this.barManager = new DevExpress.XtraBars.BarManager(this.components);
            this.menuBar = new DevExpress.XtraBars.Bar();
            this.btnRefresh = new DevExpress.XtraBars.BarLargeButtonItem();
            this.btnSetting = new DevExpress.XtraBars.BarLargeButtonItem();
            this.btnExportTo = new DevExpress.XtraBars.BarLargeButtonItem();
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.barEditItem1 = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            this.imageCollection = new DevExpress.Utils.ImageCollection(this.components);
            this.tabControl = new DevExpress.XtraTab.XtraTabControl();
            this.tabCommonSettings = new DevExpress.XtraTab.XtraTabPage();
            this.controlCommonSettings1 = new SoftServe.Reports.AGNTProject.Controls.ControlCommonSettings();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.lblReportName = new DevExpress.XtraEditors.LabelControl();
            this.chckIsPeriodClosed = new DevExpress.XtraEditors.CheckEdit();
            this.lblPeriodCaptionControlSettings = new System.Windows.Forms.Label();
            this.tabPartnerPayments = new DevExpress.XtraTab.XtraTabPage();
            this.controlPartnerPayments1 = new SoftServe.Reports.AGNTProject.Controls.ControlPartnerPayments();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.lbPeriodCaptionPartherPayment = new System.Windows.Forms.Label();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.tabBaseOfEmployees = new DevExpress.XtraTab.XtraTabPage();
            this.controlBaseOfEmployees1 = new SoftServe.Reports.AGNTProject.Controls.ControlBaseOfEmployees();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.lbPeriodCaptionEmploesBase = new System.Windows.Forms.Label();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.tabCostsForecast = new DevExpress.XtraTab.XtraTabPage();
            this.controlCostsForecast1 = new SoftServe.Reports.AGNTProject.Controls.ControlCostsForecast();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.lbForecastPeriod = new System.Windows.Forms.Label();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.tabCostsFact = new DevExpress.XtraTab.XtraTabPage();
            this.controlCostsFact1 = new SoftServe.Reports.AGNTProject.Controls.ControlCostsFact();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.lbFactPeriod = new System.Windows.Forms.Label();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.btnExport = new DevExpress.XtraBars.BarSubItem();
            ((System.ComponentModel.ISupportInitialize)(this.barManager)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabControl)).BeginInit();
            this.tabControl.SuspendLayout();
            this.tabCommonSettings.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chckIsPeriodClosed.Properties)).BeginInit();
            this.tabPartnerPayments.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.tabBaseOfEmployees.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.tabCostsForecast.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.tabCostsFact.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            this.SuspendLayout();
            // 
            // barManager
            // 
            this.barManager.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.menuBar,
            this.bar2,
            this.bar3});
            this.barManager.DockControls.Add(this.barDockControlTop);
            this.barManager.DockControls.Add(this.barDockControlBottom);
            this.barManager.DockControls.Add(this.barDockControlLeft);
            this.barManager.DockControls.Add(this.barDockControlRight);
            this.barManager.Form = this;
            this.barManager.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.btnRefresh,
            this.btnSetting,
            this.barEditItem1,
            this.barButtonItem1,
            this.btnExportTo});
            this.barManager.LargeImages = this.imageCollection;
            this.barManager.MainMenu = this.bar2;
            this.barManager.MaxItemId = 8;
            this.barManager.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit1});
            this.barManager.StatusBar = this.bar3;
            // 
            // menuBar
            // 
            this.menuBar.BarName = "Tools";
            this.menuBar.DockCol = 0;
            this.menuBar.DockRow = 0;
            this.menuBar.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.menuBar.FloatLocation = new System.Drawing.Point(730, 120);
            this.menuBar.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.btnRefresh),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnSetting),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnExportTo)});
            this.menuBar.OptionsBar.AllowQuickCustomization = false;
            this.menuBar.OptionsBar.DisableClose = true;
            this.menuBar.OptionsBar.DisableCustomization = true;
            this.menuBar.OptionsBar.UseWholeRow = true;
            this.menuBar.Text = "Tools";
            // 
            // btnRefresh
            // 
            this.btnRefresh.Caption = "Обновить";
            this.btnRefresh.Id = 2;
            this.btnRefresh.LargeImageIndex = 0;
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.ShowCaptionOnBar = false;
            this.btnRefresh.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnRefresh_ItemClick);
            // 
            // btnSetting
            // 
            this.btnSetting.Caption = "Настройка параметров";
            this.btnSetting.Id = 3;
            this.btnSetting.LargeImageIndex = 2;
            this.btnSetting.Name = "btnSetting";
            this.btnSetting.ShowCaptionOnBar = false;
            this.btnSetting.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnSetting_ItemClick);
            // 
            // btnExportTo
            // 
            this.btnExportTo.Caption = "Експорт в Excel";
            this.btnExportTo.Id = 7;
            this.btnExportTo.LargeImageIndex = 1;
            this.btnExportTo.Name = "btnExportTo";
            this.btnExportTo.ShowCaptionOnBar = false;
            this.btnExportTo.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barLargeButtonItem1_ItemClick);
            // 
            // bar2
            // 
            this.bar2.BarName = "Main menu";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 1;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar2.OptionsBar.MultiLine = true;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Main menu";
            this.bar2.Visible = false;
            // 
            // bar3
            // 
            this.bar3.BarName = "Status bar";
            this.bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar3.OptionsBar.AllowQuickCustomization = false;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Status bar";
            this.bar3.Visible = false;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(1405, 59);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 563);
            this.barDockControlBottom.Size = new System.Drawing.Size(1405, 23);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 59);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 504);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(1405, 59);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 504);
            // 
            // barEditItem1
            // 
            this.barEditItem1.Caption = "barEditItem1";
            this.barEditItem1.Edit = this.repositoryItemTextEdit1;
            this.barEditItem1.Id = 4;
            this.barEditItem1.Name = "barEditItem1";
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // barButtonItem1
            // 
            this.barButtonItem1.Caption = "Експорт в Excel";
            this.barButtonItem1.Id = 6;
            this.barButtonItem1.LargeImageIndex = 1;
            this.barButtonItem1.Name = "barButtonItem1";
            // 
            // imageCollection
            // 
            this.imageCollection.ImageSize = new System.Drawing.Size(24, 24);
            this.imageCollection.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection.ImageStream")));
            this.imageCollection.Images.SetKeyName(0, "refresh_24.png");
            this.imageCollection.Images.SetKeyName(1, "Excel.png");
            this.imageCollection.Images.SetKeyName(2, "briefcase_ok.png");
            this.imageCollection.Images.SetKeyName(3, "image_refresh.png");
            this.imageCollection.Images.SetKeyName(4, "csv.png");
            // 
            // tabControl
            // 
            this.tabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl.HeaderLocation = DevExpress.XtraTab.TabHeaderLocation.Bottom;
            this.tabControl.Location = new System.Drawing.Point(0, 59);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedTabPage = this.tabCommonSettings;
            this.tabControl.Size = new System.Drawing.Size(1405, 504);
            this.tabControl.TabIndex = 4;
            this.tabControl.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.tabCommonSettings,
            this.tabPartnerPayments,
            this.tabBaseOfEmployees,
            this.tabCostsForecast,
            this.tabCostsFact});
            this.tabControl.SelectedPageChanged += new DevExpress.XtraTab.TabPageChangedEventHandler(this.tabControl_SelectedPageChanged);
            this.tabControl.SelectedPageChanging += new DevExpress.XtraTab.TabPageChangingEventHandler(this.tabControl_SelectedPageChanging);
            // 
            // tabCommonSettings
            // 
            this.tabCommonSettings.Controls.Add(this.controlCommonSettings1);
            this.tabCommonSettings.Controls.Add(this.tableLayoutPanel1);
            this.tabCommonSettings.Name = "tabCommonSettings";
            this.tabCommonSettings.PageVisible = false;
            this.tabCommonSettings.Size = new System.Drawing.Size(1399, 476);
            this.tabCommonSettings.Text = "Общие настройки";
            // 
            // controlCommonSettings1
            // 
            this.controlCommonSettings1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.controlCommonSettings1.Location = new System.Drawing.Point(0, 27);
            this.controlCommonSettings1.Name = "controlCommonSettings1";
            this.controlCommonSettings1.ReportText = "Общие настройки";
            this.controlCommonSettings1.SaveFilter = false;
            this.controlCommonSettings1.Size = new System.Drawing.Size(1399, 449);
            this.controlCommonSettings1.TabIndex = 1;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.Controls.Add(this.lblReportName, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.chckIsPeriodClosed, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.lblPeriodCaptionControlSettings, 1, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1399, 27);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // lblReportName
            // 
            this.lblReportName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.lblReportName.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.lblReportName.Location = new System.Drawing.Point(3, 3);
            this.lblReportName.Name = "lblReportName";
            this.lblReportName.Size = new System.Drawing.Size(132, 19);
            this.lblReportName.TabIndex = 2;
            this.lblReportName.Text = "Общие настройки";
            // 
            // chckIsPeriodClosed
            // 
            this.chckIsPeriodClosed.Dock = System.Windows.Forms.DockStyle.Right;
            this.chckIsPeriodClosed.Location = new System.Drawing.Point(1096, 3);
            this.chckIsPeriodClosed.MenuManager = this.barManager;
            this.chckIsPeriodClosed.Name = "chckIsPeriodClosed";
            this.chckIsPeriodClosed.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.chckIsPeriodClosed.Properties.Appearance.Options.UseFont = true;
            this.chckIsPeriodClosed.Properties.Caption = "Закрыть период для редактирования";
            this.chckIsPeriodClosed.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.chckIsPeriodClosed.Size = new System.Drawing.Size(300, 23);
            this.chckIsPeriodClosed.TabIndex = 0;
            this.chckIsPeriodClosed.CheckedChanged += new System.EventHandler(this.chckIsPeriodClosed_CheckedChanged);
            // 
            // lblPeriodCaptionControlSettings
            // 
            this.lblPeriodCaptionControlSettings.AutoSize = true;
            this.lblPeriodCaptionControlSettings.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblPeriodCaptionControlSettings.Font = new System.Drawing.Font("Tahoma", 12F);
            this.lblPeriodCaptionControlSettings.Location = new System.Drawing.Point(469, 0);
            this.lblPeriodCaptionControlSettings.Name = "lblPeriodCaptionControlSettings";
            this.lblPeriodCaptionControlSettings.Size = new System.Drawing.Size(460, 27);
            this.lblPeriodCaptionControlSettings.TabIndex = 3;
            this.lblPeriodCaptionControlSettings.Text = "PeriodName";
            this.lblPeriodCaptionControlSettings.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tabPartnerPayments
            // 
            this.tabPartnerPayments.Controls.Add(this.controlPartnerPayments1);
            this.tabPartnerPayments.Controls.Add(this.tableLayoutPanel2);
            this.tabPartnerPayments.Name = "tabPartnerPayments";
            this.tabPartnerPayments.PageVisible = false;
            this.tabPartnerPayments.Size = new System.Drawing.Size(1399, 476);
            this.tabPartnerPayments.Text = "Расчеты с партнером";
            // 
            // controlPartnerPayments1
            // 
            this.controlPartnerPayments1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.controlPartnerPayments1.Location = new System.Drawing.Point(0, 27);
            this.controlPartnerPayments1.Name = "controlPartnerPayments1";
            this.controlPartnerPayments1.ReportText = "Рачеты с партнером";
            this.controlPartnerPayments1.SaveFilter = false;
            this.controlPartnerPayments1.Size = new System.Drawing.Size(1399, 449);
            this.controlPartnerPayments1.TabIndex = 2;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 3;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel2.Controls.Add(this.lbPeriodCaptionPartherPayment, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.labelControl1, 0, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(1399, 27);
            this.tableLayoutPanel2.TabIndex = 1;
            // 
            // lbPeriodCaptionPartherPayment
            // 
            this.lbPeriodCaptionPartherPayment.AutoSize = true;
            this.lbPeriodCaptionPartherPayment.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbPeriodCaptionPartherPayment.Font = new System.Drawing.Font("Tahoma", 12F);
            this.lbPeriodCaptionPartherPayment.Location = new System.Drawing.Point(469, 0);
            this.lbPeriodCaptionPartherPayment.Name = "lbPeriodCaptionPartherPayment";
            this.lbPeriodCaptionPartherPayment.Size = new System.Drawing.Size(460, 27);
            this.lbPeriodCaptionPartherPayment.TabIndex = 4;
            this.lbPeriodCaptionPartherPayment.Text = "PeriodName";
            this.lbPeriodCaptionPartherPayment.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelControl1
            // 
            this.labelControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.labelControl1.Location = new System.Drawing.Point(3, 3);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(156, 19);
            this.labelControl1.TabIndex = 2;
            this.labelControl1.Text = "Расчеты с партнером";
            // 
            // tabBaseOfEmployees
            // 
            this.tabBaseOfEmployees.Controls.Add(this.controlBaseOfEmployees1);
            this.tabBaseOfEmployees.Controls.Add(this.tableLayoutPanel3);
            this.tabBaseOfEmployees.Name = "tabBaseOfEmployees";
            this.tabBaseOfEmployees.PageVisible = false;
            this.tabBaseOfEmployees.Size = new System.Drawing.Size(1399, 476);
            this.tabBaseOfEmployees.Text = "База сотрудников";
            // 
            // controlBaseOfEmployees1
            // 
            this.controlBaseOfEmployees1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.controlBaseOfEmployees1.Location = new System.Drawing.Point(0, 27);
            this.controlBaseOfEmployees1.Name = "controlBaseOfEmployees1";
            this.controlBaseOfEmployees1.ReportText = "База сотрудников";
            this.controlBaseOfEmployees1.SaveFilter = false;
            this.controlBaseOfEmployees1.Size = new System.Drawing.Size(1399, 449);
            this.controlBaseOfEmployees1.TabIndex = 2;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 3;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel3.Controls.Add(this.lbPeriodCaptionEmploesBase, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.labelControl3, 0, 0);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 1;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(1399, 27);
            this.tableLayoutPanel3.TabIndex = 1;
            // 
            // lbPeriodCaptionEmploesBase
            // 
            this.lbPeriodCaptionEmploesBase.AutoSize = true;
            this.lbPeriodCaptionEmploesBase.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbPeriodCaptionEmploesBase.Font = new System.Drawing.Font("Tahoma", 12F);
            this.lbPeriodCaptionEmploesBase.Location = new System.Drawing.Point(469, 0);
            this.lbPeriodCaptionEmploesBase.Name = "lbPeriodCaptionEmploesBase";
            this.lbPeriodCaptionEmploesBase.Size = new System.Drawing.Size(460, 27);
            this.lbPeriodCaptionEmploesBase.TabIndex = 4;
            this.lbPeriodCaptionEmploesBase.Text = "PeriodName";
            this.lbPeriodCaptionEmploesBase.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelControl3
            // 
            this.labelControl3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.labelControl3.Location = new System.Drawing.Point(3, 3);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(130, 19);
            this.labelControl3.TabIndex = 2;
            this.labelControl3.Text = "База сотрудников";
            // 
            // tabCostsForecast
            // 
            this.tabCostsForecast.Controls.Add(this.controlCostsForecast1);
            this.tabCostsForecast.Controls.Add(this.tableLayoutPanel4);
            this.tabCostsForecast.Name = "tabCostsForecast";
            this.tabCostsForecast.PageVisible = false;
            this.tabCostsForecast.Size = new System.Drawing.Size(1399, 476);
            this.tabCostsForecast.Text = "Прогноз затрат";
            // 
            // controlCostsForecast1
            // 
            this.controlCostsForecast1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.controlCostsForecast1.Location = new System.Drawing.Point(0, 27);
            this.controlCostsForecast1.Name = "controlCostsForecast1";
            this.controlCostsForecast1.SaveFilter = false;
            this.controlCostsForecast1.Size = new System.Drawing.Size(1399, 449);
            this.controlCostsForecast1.TabIndex = 2;
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 3;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel4.Controls.Add(this.lbForecastPeriod, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.labelControl5, 0, 0);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 1;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(1399, 27);
            this.tableLayoutPanel4.TabIndex = 1;
            // 
            // lbForecastPeriod
            // 
            this.lbForecastPeriod.AutoSize = true;
            this.lbForecastPeriod.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbForecastPeriod.Font = new System.Drawing.Font("Tahoma", 12F);
            this.lbForecastPeriod.Location = new System.Drawing.Point(469, 0);
            this.lbForecastPeriod.Name = "lbForecastPeriod";
            this.lbForecastPeriod.Size = new System.Drawing.Size(460, 27);
            this.lbForecastPeriod.TabIndex = 4;
            this.lbForecastPeriod.Text = "PeriodName";
            this.lbForecastPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelControl5
            // 
            this.labelControl5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.labelControl5.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.labelControl5.Location = new System.Drawing.Point(3, 3);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(114, 19);
            this.labelControl5.TabIndex = 2;
            this.labelControl5.Text = "Прогноз затрат";
            // 
            // tabCostsFact
            // 
            this.tabCostsFact.Controls.Add(this.controlCostsFact1);
            this.tabCostsFact.Controls.Add(this.tableLayoutPanel5);
            this.tabCostsFact.Name = "tabCostsFact";
            this.tabCostsFact.PageVisible = false;
            this.tabCostsFact.Size = new System.Drawing.Size(1399, 476);
            this.tabCostsFact.Text = "Факт затрат";
            // 
            // controlCostsFact1
            // 
            this.controlCostsFact1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.controlCostsFact1.Location = new System.Drawing.Point(0, 27);
            this.controlCostsFact1.Name = "controlCostsFact1";
            this.controlCostsFact1.SaveFilter = false;
            this.controlCostsFact1.Size = new System.Drawing.Size(1399, 449);
            this.controlCostsFact1.TabIndex = 2;
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.ColumnCount = 3;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel5.Controls.Add(this.lbFactPeriod, 0, 0);
            this.tableLayoutPanel5.Controls.Add(this.labelControl7, 0, 0);
            this.tableLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel5.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 1;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(1399, 27);
            this.tableLayoutPanel5.TabIndex = 1;
            // 
            // lbFactPeriod
            // 
            this.lbFactPeriod.AutoSize = true;
            this.lbFactPeriod.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbFactPeriod.Font = new System.Drawing.Font("Tahoma", 12F);
            this.lbFactPeriod.Location = new System.Drawing.Point(469, 0);
            this.lbFactPeriod.Name = "lbFactPeriod";
            this.lbFactPeriod.Size = new System.Drawing.Size(460, 27);
            this.lbFactPeriod.TabIndex = 4;
            this.lbFactPeriod.Text = "PeriodName";
            this.lbFactPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelControl7
            // 
            this.labelControl7.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.labelControl7.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.labelControl7.Location = new System.Drawing.Point(3, 3);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(89, 19);
            this.labelControl7.TabIndex = 2;
            this.labelControl7.Text = "Факт затрат";
            // 
            // btnExport
            // 
            this.btnExport.Glyph = ((System.Drawing.Image)(resources.GetObject("btnExport.Glyph")));
            this.btnExport.Id = 5;
            this.btnExport.LargeImageIndex = 3;
            this.btnExport.Name = "btnExport";
            this.btnExport.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // WccpUIControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tabControl);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Name = "WccpUIControl";
            this.Size = new System.Drawing.Size(1405, 586);
            ((System.ComponentModel.ISupportInitialize)(this.barManager)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabControl)).EndInit();
            this.tabControl.ResumeLayout(false);
            this.tabCommonSettings.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chckIsPeriodClosed.Properties)).EndInit();
            this.tabPartnerPayments.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.tabBaseOfEmployees.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.tabCostsForecast.ResumeLayout(false);
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel4.PerformLayout();
            this.tabCostsFact.ResumeLayout(false);
            this.tableLayoutPanel5.ResumeLayout(false);
            this.tableLayoutPanel5.PerformLayout();
            this.ResumeLayout(false);

        }


        #endregion

        private DevExpress.XtraBars.BarManager barManager;
        private DevExpress.XtraBars.Bar menuBar;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.Utils.ImageCollection imageCollection;
        private DevExpress.XtraBars.BarLargeButtonItem btnRefresh;
        private DevExpress.XtraBars.BarLargeButtonItem btnSetting;
        private DevExpress.XtraBars.BarEditItem barEditItem1;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
        private DevExpress.XtraTab.XtraTabControl tabControl;
        private DevExpress.XtraTab.XtraTabPage tabPartnerPayments;
        private DevExpress.XtraTab.XtraTabPage tabBaseOfEmployees;
        private DevExpress.XtraTab.XtraTabPage tabCostsForecast;
        private DevExpress.XtraTab.XtraTabPage tabCostsFact;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private DevExpress.XtraEditors.LabelControl lblReportName;
        private DevExpress.XtraEditors.CheckEdit chckIsPeriodClosed;
        private DevExpress.XtraTab.XtraTabPage tabCommonSettings;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private System.Windows.Forms.Label lblPeriodCaptionControlSettings;
        private System.Windows.Forms.Label lbPeriodCaptionPartherPayment;
        private System.Windows.Forms.Label lbPeriodCaptionEmploesBase;
        private System.Windows.Forms.Label lbForecastPeriod;
        private System.Windows.Forms.Label lbFactPeriod;
        private ControlCommonSettings controlCommonSettings1;
        private Controls.ControlPartnerPayments controlPartnerPayments1;
        private Controls.ControlBaseOfEmployees controlBaseOfEmployees1;
        private DevExpress.XtraBars.BarSubItem btnExport;
        private DevExpress.XtraBars.BarLargeButtonItem btnExportTo;
        private DevExpress.XtraBars.BarButtonItem barButtonItem1;
        private ControlCostsForecast controlCostsForecast1;
        private ControlCostsFact controlCostsFact1;
    }
}
