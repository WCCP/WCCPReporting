﻿namespace SoftServe.Reports.ThR.SummaryReport.Forms
{
    partial class SelectGroupsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SelectGroupsForm));
            this.panelControlButtons = new DevExpress.XtraEditors.PanelControl();
            this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.btnOk = new DevExpress.XtraEditors.SimpleButton();
            this.chkSelDeselAll = new DevExpress.XtraEditors.CheckEdit();
            this.panelControlMainArea = new DevExpress.XtraEditors.PanelControl();
            this.gridControl = new DevExpress.XtraGrid.GridControl();
            this.groupVisibilityBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gcGroup = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcVisible = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.panelControlButtons)).BeginInit();
            this.panelControlButtons.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkSelDeselAll.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControlMainArea)).BeginInit();
            this.panelControlMainArea.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupVisibilityBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControlButtons
            // 
            this.panelControlButtons.Controls.Add(this.btnCancel);
            this.panelControlButtons.Controls.Add(this.btnOk);
            this.panelControlButtons.Controls.Add(this.chkSelDeselAll);
            this.panelControlButtons.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControlButtons.Location = new System.Drawing.Point(0, 338);
            this.panelControlButtons.Name = "panelControlButtons";
            this.panelControlButtons.Size = new System.Drawing.Size(332, 64);
            this.panelControlButtons.TabIndex = 0;
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Image = global::SoftServe.Reports.ThR.SummaryReport.Properties.Resources.Close_16;
            this.btnCancel.Location = new System.Drawing.Point(252, 31);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 2;
            this.btnCancel.Text = "Нет";
            // 
            // btnOk
            // 
            this.btnOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOk.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnOk.Image = global::SoftServe.Reports.ThR.SummaryReport.Properties.Resources.Check_16;
            this.btnOk.Location = new System.Drawing.Point(171, 31);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(75, 23);
            this.btnOk.TabIndex = 1;
            this.btnOk.Text = "Да";
            // 
            // chkSelDeselAll
            // 
            this.chkSelDeselAll.Location = new System.Drawing.Point(12, 6);
            this.chkSelDeselAll.Name = "chkSelDeselAll";
            this.chkSelDeselAll.Properties.Caption = "Отобразить/скрыть для  &всех";
            this.chkSelDeselAll.Size = new System.Drawing.Size(234, 19);
            this.chkSelDeselAll.TabIndex = 0;
            this.chkSelDeselAll.CheckedChanged += new System.EventHandler(this.chkSelDeselAll_CheckedChanged);
            // 
            // panelControlMainArea
            // 
            this.panelControlMainArea.Controls.Add(this.gridControl);
            this.panelControlMainArea.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControlMainArea.Location = new System.Drawing.Point(0, 0);
            this.panelControlMainArea.Name = "panelControlMainArea";
            this.panelControlMainArea.Size = new System.Drawing.Size(332, 338);
            this.panelControlMainArea.TabIndex = 1;
            // 
            // gridControl
            // 
            this.gridControl.DataSource = this.groupVisibilityBindingSource;
            this.gridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl.Location = new System.Drawing.Point(2, 2);
            this.gridControl.MainView = this.gridView;
            this.gridControl.Name = "gridControl";
            this.gridControl.Size = new System.Drawing.Size(328, 334);
            this.gridControl.TabIndex = 0;
            this.gridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView});
            // 
            // groupVisibilityBindingSource
            // 
            this.groupVisibilityBindingSource.DataSource = typeof(SoftServe.Reports.ThR.SummaryReport.DataProvider.GroupVisibility);
            // 
            // gridView
            // 
            this.gridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gcGroup,
            this.gcVisible});
            this.gridView.GridControl = this.gridControl;
            this.gridView.Name = "gridView";
            this.gridView.OptionsBehavior.EditorShowMode = DevExpress.Utils.EditorShowMode.MouseDown;
            this.gridView.OptionsView.ShowGroupPanel = false;
            this.gridView.OptionsView.ShowIndicator = false;
            this.gridView.RowCellClick += new DevExpress.XtraGrid.Views.Grid.RowCellClickEventHandler(this.gridView_RowCellClick);
            this.gridView.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.gridView_CellValueChanged);
            // 
            // gcGroup
            // 
            this.gcGroup.AppearanceHeader.Options.UseTextOptions = true;
            this.gcGroup.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gcGroup.Caption = "Группа";
            this.gcGroup.FieldName = "GroupName";
            this.gcGroup.Name = "gcGroup";
            this.gcGroup.OptionsColumn.AllowEdit = false;
            this.gcGroup.OptionsColumn.ReadOnly = true;
            this.gcGroup.Visible = true;
            this.gcGroup.VisibleIndex = 0;
            this.gcGroup.Width = 212;
            // 
            // gcVisible
            // 
            this.gcVisible.AppearanceHeader.Options.UseTextOptions = true;
            this.gcVisible.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gcVisible.Caption = "Отобразить";
            this.gcVisible.FieldName = "IsVisible";
            this.gcVisible.Name = "gcVisible";
            this.gcVisible.OptionsColumn.AllowEdit = false;
            this.gcVisible.Visible = true;
            this.gcVisible.VisibleIndex = 1;
            this.gcVisible.Width = 88;
            // 
            // SelectGroupsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(332, 402);
            this.Controls.Add(this.panelControlMainArea);
            this.Controls.Add(this.panelControlButtons);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "SelectGroupsForm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Отображение/скрытие групп";
            ((System.ComponentModel.ISupportInitialize)(this.panelControlButtons)).EndInit();
            this.panelControlButtons.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chkSelDeselAll.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControlMainArea)).EndInit();
            this.panelControlMainArea.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupVisibilityBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControlButtons;
        private DevExpress.XtraEditors.SimpleButton btnCancel;
        private DevExpress.XtraEditors.SimpleButton btnOk;
        private DevExpress.XtraEditors.CheckEdit chkSelDeselAll;
        private DevExpress.XtraEditors.PanelControl panelControlMainArea;
        private DevExpress.XtraGrid.GridControl gridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView;
        private DevExpress.XtraGrid.Columns.GridColumn gcGroup;
        private DevExpress.XtraGrid.Columns.GridColumn gcVisible;
        private System.Windows.Forms.BindingSource groupVisibilityBindingSource;

    }
}