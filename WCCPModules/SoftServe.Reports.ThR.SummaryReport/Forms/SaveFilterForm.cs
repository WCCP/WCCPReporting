﻿using System;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Logica.Reports.Common.WaitWindow;
using SoftServe.Reports.ThR.SummaryReport.DataProvider;
using SoftServe.Reports.ThR.SummaryReport.Properties;

namespace SoftServe.Reports.ThR.SummaryReport.Forms
{
    public partial class SaveFilterForm : XtraForm
    {
        public FilterInfo Filter { get; set; }

        public SaveFilterForm()
        {
            InitializeComponent();
        }

        private void SaveFilterForm_Load(object sender, EventArgs e)
        {
            txtFilterName.EditValue = Filter == null ? Resources.txtWithoutFilter : Filter.Filter_Name;
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            Filter.Filter_Name = txtFilterName.EditValue.ToString();
            WaitManager.StartWait();
            try
            {
                DataAccessProvider.SaveThRFilter(Filter);
            }
            finally
            {
                WaitManager.StopWait();
            }
            DialogResult = DialogResult.OK;
        }

        private void btnSelectFilter_Click(object sender, EventArgs e)
        {
            var form = new SelectFilterForm();
            if (form.ShowDialog() == DialogResult.OK)
            {
                var filter = form.CurrentFilter;
                Filter.Filter_Name = filter.Filter_Name;
                Filter.Filter_ID = filter.Filter_ID;
            }
        }
    }
}
