namespace SoftServe.Reports.ThR.SummaryReport.Forms
{
    partial class SelectFilterForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SelectFilterForm));
            this.xtraTabControl = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPageOwnFilters = new DevExpress.XtraTab.XtraTabPage();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.gridControlOwnFilters = new DevExpress.XtraGrid.GridControl();
            this.ownFilterInfoBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.bandedGridViewOwnFilters = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridView();
            this.gridBand1 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bgcOfFilter_Name = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bgcOfUserName = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bgcOfFilter_date = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bgcOfFilter_text = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand2 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bgcOfWaveName = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bgcOfChannel = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bgcOfRegion = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bgcOfFilter_ID = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bgcOfFilter_str = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bgcOfUser_ID = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bgcOfWave_ID = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.panelControlOwnMemo = new DevExpress.XtraEditors.PanelControl();
            this.memoEditOwnFilterCriterium = new DevExpress.XtraEditors.MemoEdit();
            this.panelControlDelete = new DevExpress.XtraEditors.PanelControl();
            this.btnDelete = new DevExpress.XtraEditors.SimpleButton();
            this.xtraTabPageAllFilters = new DevExpress.XtraTab.XtraTabPage();
            this.panelControl4 = new DevExpress.XtraEditors.PanelControl();
            this.gridControlAllFilters = new DevExpress.XtraGrid.GridControl();
            this.allFilterInfoBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.bandedGridViewAllFilters = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridView();
            this.gridBand3 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bgcAfFilter_Name = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bgcAfUserName = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bgcAfFilter_date = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bgcAfFilter_text = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand4 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bgcAfWaveName = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bgcAfChannel = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bgcAfRegion = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bgcAfFilter_ID = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bgcAfFilter_str = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bgcAfUser_ID = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bgcAfWave_ID = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.memoEditAllFilterCriterium = new DevExpress.XtraEditors.MemoEdit();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.btnOk = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl)).BeginInit();
            this.xtraTabControl.SuspendLayout();
            this.xtraTabPageOwnFilters.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlOwnFilters)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ownFilterInfoBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bandedGridViewOwnFilters)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControlOwnMemo)).BeginInit();
            this.panelControlOwnMemo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.memoEditOwnFilterCriterium.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControlDelete)).BeginInit();
            this.panelControlDelete.SuspendLayout();
            this.xtraTabPageAllFilters.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).BeginInit();
            this.panelControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlAllFilters)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.allFilterInfoBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bandedGridViewAllFilters)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.memoEditAllFilterCriterium.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            this.SuspendLayout();
            // 
            // xtraTabControl
            // 
            this.xtraTabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraTabControl.Location = new System.Drawing.Point(2, 2);
            this.xtraTabControl.Name = "xtraTabControl";
            this.xtraTabControl.SelectedTabPage = this.xtraTabPageOwnFilters;
            this.xtraTabControl.Size = new System.Drawing.Size(868, 508);
            this.xtraTabControl.TabIndex = 0;
            this.xtraTabControl.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPageOwnFilters,
            this.xtraTabPageAllFilters});
            this.xtraTabControl.SelectedPageChanged += new DevExpress.XtraTab.TabPageChangedEventHandler(this.xtraTabControl_SelectedPageChanged);
            // 
            // xtraTabPageOwnFilters
            // 
            this.xtraTabPageOwnFilters.Controls.Add(this.panelControl3);
            this.xtraTabPageOwnFilters.Controls.Add(this.groupControl1);
            this.xtraTabPageOwnFilters.Image = global::SoftServe.Reports.ThR.SummaryReport.Properties.Resources.OwnFilters_24;
            this.xtraTabPageOwnFilters.Name = "xtraTabPageOwnFilters";
            this.xtraTabPageOwnFilters.Size = new System.Drawing.Size(861, 469);
            this.xtraTabPageOwnFilters.Text = "����";
            // 
            // panelControl3
            // 
            this.panelControl3.Controls.Add(this.gridControlOwnFilters);
            this.panelControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl3.Location = new System.Drawing.Point(0, 0);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(861, 311);
            this.panelControl3.TabIndex = 1;
            // 
            // gridControlOwnFilters
            // 
            this.gridControlOwnFilters.DataSource = this.ownFilterInfoBindingSource;
            this.gridControlOwnFilters.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlOwnFilters.Location = new System.Drawing.Point(2, 2);
            this.gridControlOwnFilters.MainView = this.bandedGridViewOwnFilters;
            this.gridControlOwnFilters.Name = "gridControlOwnFilters";
            this.gridControlOwnFilters.Size = new System.Drawing.Size(857, 307);
            this.gridControlOwnFilters.TabIndex = 0;
            this.gridControlOwnFilters.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.bandedGridViewOwnFilters});
            // 
            // ownFilterInfoBindingSource
            // 
            this.ownFilterInfoBindingSource.DataSource = typeof(SoftServe.Reports.ThR.SummaryReport.DataProvider.FilterInfo);
            // 
            // bandedGridViewOwnFilters
            // 
            this.bandedGridViewOwnFilters.Bands.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand1,
            this.gridBand2});
            this.bandedGridViewOwnFilters.Columns.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn[] {
            this.bgcOfFilter_ID,
            this.bgcOfFilter_Name,
            this.bgcOfFilter_str,
            this.bgcOfFilter_text,
            this.bgcOfFilter_date,
            this.bgcOfUser_ID,
            this.bgcOfUserName,
            this.bgcOfWave_ID,
            this.bgcOfWaveName,
            this.bgcOfRegion,
            this.bgcOfChannel});
            this.bandedGridViewOwnFilters.GridControl = this.gridControlOwnFilters;
            this.bandedGridViewOwnFilters.Name = "bandedGridViewOwnFilters";
            this.bandedGridViewOwnFilters.OptionsBehavior.Editable = false;
            this.bandedGridViewOwnFilters.OptionsView.ColumnAutoWidth = false;
            this.bandedGridViewOwnFilters.OptionsView.HeaderFilterButtonShowMode = DevExpress.XtraEditors.Controls.FilterButtonShowMode.Button;
            // 
            // gridBand1
            // 
            this.gridBand1.Caption = "������";
            this.gridBand1.Columns.Add(this.bgcOfFilter_Name);
            this.gridBand1.Columns.Add(this.bgcOfUserName);
            this.gridBand1.Columns.Add(this.bgcOfFilter_date);
            this.gridBand1.Columns.Add(this.bgcOfFilter_text);
            this.gridBand1.Name = "gridBand1";
            this.gridBand1.Width = 831;
            // 
            // bgcOfFilter_Name
            // 
            this.bgcOfFilter_Name.Caption = "��������";
            this.bgcOfFilter_Name.FieldName = "Filter_Name";
            this.bgcOfFilter_Name.Name = "bgcOfFilter_Name";
            this.bgcOfFilter_Name.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.bgcOfFilter_Name.Visible = true;
            this.bgcOfFilter_Name.Width = 314;
            // 
            // bgcOfUserName
            // 
            this.bgcOfUserName.Caption = "������������";
            this.bgcOfUserName.FieldName = "UserName";
            this.bgcOfUserName.Name = "bgcOfUserName";
            this.bgcOfUserName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.bgcOfUserName.Visible = true;
            this.bgcOfUserName.Width = 112;
            // 
            // bgcOfFilter_date
            // 
            this.bgcOfFilter_date.Caption = "���� ��������";
            this.bgcOfFilter_date.FieldName = "Filter_date";
            this.bgcOfFilter_date.Name = "bgcOfFilter_date";
            this.bgcOfFilter_date.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.bgcOfFilter_date.Visible = true;
            this.bgcOfFilter_date.Width = 105;
            // 
            // bgcOfFilter_text
            // 
            this.bgcOfFilter_text.Caption = "�������� ����������";
            this.bgcOfFilter_text.FieldName = "Filter_text";
            this.bgcOfFilter_text.Name = "bgcOfFilter_text";
            this.bgcOfFilter_text.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.bgcOfFilter_text.Visible = true;
            this.bgcOfFilter_text.Width = 300;
            // 
            // gridBand2
            // 
            this.gridBand2.Caption = "����� ������";
            this.gridBand2.Columns.Add(this.bgcOfWaveName);
            this.gridBand2.Columns.Add(this.bgcOfChannel);
            this.gridBand2.Columns.Add(this.bgcOfRegion);
            this.gridBand2.Name = "gridBand2";
            this.gridBand2.Width = 329;
            // 
            // bgcOfWaveName
            // 
            this.bgcOfWaveName.Caption = "�����";
            this.bgcOfWaveName.FieldName = "WaveName";
            this.bgcOfWaveName.Name = "bgcOfWaveName";
            this.bgcOfWaveName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.bgcOfWaveName.Visible = true;
            this.bgcOfWaveName.Width = 164;
            // 
            // bgcOfChannel
            // 
            this.bgcOfChannel.Caption = "�����";
            this.bgcOfChannel.FieldName = "Channel";
            this.bgcOfChannel.Name = "bgcOfChannel";
            this.bgcOfChannel.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.bgcOfChannel.Visible = true;
            this.bgcOfChannel.Width = 60;
            // 
            // bgcOfRegion
            // 
            this.bgcOfRegion.Caption = "������";
            this.bgcOfRegion.FieldName = "Region";
            this.bgcOfRegion.Name = "bgcOfRegion";
            this.bgcOfRegion.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.bgcOfRegion.Visible = true;
            this.bgcOfRegion.Width = 105;
            // 
            // bgcOfFilter_ID
            // 
            this.bgcOfFilter_ID.Caption = "bandedGridColumn1";
            this.bgcOfFilter_ID.FieldName = "Filter_ID";
            this.bgcOfFilter_ID.Name = "bgcOfFilter_ID";
            this.bgcOfFilter_ID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.bgcOfFilter_ID.Visible = true;
            // 
            // bgcOfFilter_str
            // 
            this.bgcOfFilter_str.Caption = "bandedGridColumn3";
            this.bgcOfFilter_str.FieldName = "Filter_str";
            this.bgcOfFilter_str.Name = "bgcOfFilter_str";
            this.bgcOfFilter_str.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.bgcOfFilter_str.Visible = true;
            // 
            // bgcOfUser_ID
            // 
            this.bgcOfUser_ID.Caption = "bandedGridColumn1";
            this.bgcOfUser_ID.FieldName = "User_ID";
            this.bgcOfUser_ID.Name = "bgcOfUser_ID";
            this.bgcOfUser_ID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.bgcOfUser_ID.Visible = true;
            // 
            // bgcOfWave_ID
            // 
            this.bgcOfWave_ID.Caption = "bandedGridColumn3";
            this.bgcOfWave_ID.FieldName = "Wave_ID";
            this.bgcOfWave_ID.Name = "bgcOfWave_ID";
            this.bgcOfWave_ID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.bgcOfWave_ID.Visible = true;
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.panelControlOwnMemo);
            this.groupControl1.Controls.Add(this.panelControlDelete);
            this.groupControl1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.groupControl1.Location = new System.Drawing.Point(0, 311);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(861, 158);
            this.groupControl1.TabIndex = 0;
            this.groupControl1.Text = "�������� ����������";
            // 
            // panelControlOwnMemo
            // 
            this.panelControlOwnMemo.Controls.Add(this.memoEditOwnFilterCriterium);
            this.panelControlOwnMemo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControlOwnMemo.Location = new System.Drawing.Point(2, 22);
            this.panelControlOwnMemo.Name = "panelControlOwnMemo";
            this.panelControlOwnMemo.Size = new System.Drawing.Size(857, 97);
            this.panelControlOwnMemo.TabIndex = 2;
            // 
            // memoEditOwnFilterCriterium
            // 
            this.memoEditOwnFilterCriterium.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.ownFilterInfoBindingSource, "Filter_text", true));
            this.memoEditOwnFilterCriterium.Dock = System.Windows.Forms.DockStyle.Fill;
            this.memoEditOwnFilterCriterium.Location = new System.Drawing.Point(2, 2);
            this.memoEditOwnFilterCriterium.Name = "memoEditOwnFilterCriterium";
            this.memoEditOwnFilterCriterium.Properties.ReadOnly = true;
            this.memoEditOwnFilterCriterium.Size = new System.Drawing.Size(853, 93);
            this.memoEditOwnFilterCriterium.TabIndex = 0;
            // 
            // panelControlDelete
            // 
            this.panelControlDelete.Controls.Add(this.btnDelete);
            this.panelControlDelete.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControlDelete.Location = new System.Drawing.Point(2, 119);
            this.panelControlDelete.Name = "panelControlDelete";
            this.panelControlDelete.Size = new System.Drawing.Size(857, 37);
            this.panelControlDelete.TabIndex = 1;
            // 
            // btnDelete
            // 
            this.btnDelete.Enabled = false;
            this.btnDelete.Image = global::SoftServe.Reports.ThR.SummaryReport.Properties.Resources.DeleteFilter_16;
            this.btnDelete.Location = new System.Drawing.Point(6, 6);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(75, 23);
            this.btnDelete.TabIndex = 0;
            this.btnDelete.Text = "�������";
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // xtraTabPageAllFilters
            // 
            this.xtraTabPageAllFilters.Controls.Add(this.panelControl4);
            this.xtraTabPageAllFilters.Controls.Add(this.groupControl2);
            this.xtraTabPageAllFilters.Image = global::SoftServe.Reports.ThR.SummaryReport.Properties.Resources.AllFilters_24;
            this.xtraTabPageAllFilters.Name = "xtraTabPageAllFilters";
            this.xtraTabPageAllFilters.Size = new System.Drawing.Size(861, 469);
            this.xtraTabPageAllFilters.Text = "���";
            // 
            // panelControl4
            // 
            this.panelControl4.Controls.Add(this.gridControlAllFilters);
            this.panelControl4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl4.Location = new System.Drawing.Point(0, 0);
            this.panelControl4.Name = "panelControl4";
            this.panelControl4.Size = new System.Drawing.Size(861, 369);
            this.panelControl4.TabIndex = 3;
            // 
            // gridControlAllFilters
            // 
            this.gridControlAllFilters.DataSource = this.allFilterInfoBindingSource;
            this.gridControlAllFilters.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlAllFilters.Location = new System.Drawing.Point(2, 2);
            this.gridControlAllFilters.MainView = this.bandedGridViewAllFilters;
            this.gridControlAllFilters.Name = "gridControlAllFilters";
            this.gridControlAllFilters.Size = new System.Drawing.Size(857, 365);
            this.gridControlAllFilters.TabIndex = 1;
            this.gridControlAllFilters.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.bandedGridViewAllFilters});
            // 
            // allFilterInfoBindingSource
            // 
            this.allFilterInfoBindingSource.DataSource = typeof(SoftServe.Reports.ThR.SummaryReport.DataProvider.FilterInfo);
            // 
            // bandedGridViewAllFilters
            // 
            this.bandedGridViewAllFilters.Bands.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand3,
            this.gridBand4});
            this.bandedGridViewAllFilters.Columns.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn[] {
            this.bgcAfFilter_ID,
            this.bgcAfFilter_Name,
            this.bgcAfFilter_str,
            this.bgcAfFilter_text,
            this.bgcAfFilter_date,
            this.bgcAfUser_ID,
            this.bgcAfUserName,
            this.bgcAfWave_ID,
            this.bgcAfWaveName,
            this.bgcAfRegion,
            this.bgcAfChannel});
            this.bandedGridViewAllFilters.GridControl = this.gridControlAllFilters;
            this.bandedGridViewAllFilters.Name = "bandedGridViewAllFilters";
            this.bandedGridViewAllFilters.OptionsBehavior.Editable = false;
            this.bandedGridViewAllFilters.OptionsView.ColumnAutoWidth = false;
            this.bandedGridViewAllFilters.OptionsView.HeaderFilterButtonShowMode = DevExpress.XtraEditors.Controls.FilterButtonShowMode.Button;
            // 
            // gridBand3
            // 
            this.gridBand3.Caption = "������";
            this.gridBand3.Columns.Add(this.bgcAfFilter_Name);
            this.gridBand3.Columns.Add(this.bgcAfUserName);
            this.gridBand3.Columns.Add(this.bgcAfFilter_date);
            this.gridBand3.Columns.Add(this.bgcAfFilter_text);
            this.gridBand3.Name = "gridBand3";
            this.gridBand3.Width = 831;
            // 
            // bgcAfFilter_Name
            // 
            this.bgcAfFilter_Name.Caption = "��������";
            this.bgcAfFilter_Name.FieldName = "Filter_Name";
            this.bgcAfFilter_Name.Name = "bgcAfFilter_Name";
            this.bgcAfFilter_Name.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.bgcAfFilter_Name.Visible = true;
            this.bgcAfFilter_Name.Width = 314;
            // 
            // bgcAfUserName
            // 
            this.bgcAfUserName.Caption = "������������";
            this.bgcAfUserName.FieldName = "UserName";
            this.bgcAfUserName.Name = "bgcAfUserName";
            this.bgcAfUserName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.bgcAfUserName.Visible = true;
            this.bgcAfUserName.Width = 112;
            // 
            // bgcAfFilter_date
            // 
            this.bgcAfFilter_date.Caption = "���� ��������";
            this.bgcAfFilter_date.FieldName = "Filter_date";
            this.bgcAfFilter_date.Name = "bgcAfFilter_date";
            this.bgcAfFilter_date.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.bgcAfFilter_date.Visible = true;
            this.bgcAfFilter_date.Width = 105;
            // 
            // bgcAfFilter_text
            // 
            this.bgcAfFilter_text.Caption = "�������� ����������";
            this.bgcAfFilter_text.FieldName = "Filter_text";
            this.bgcAfFilter_text.Name = "bgcAfFilter_text";
            this.bgcAfFilter_text.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.bgcAfFilter_text.Visible = true;
            this.bgcAfFilter_text.Width = 300;
            // 
            // gridBand4
            // 
            this.gridBand4.Caption = "����� ������";
            this.gridBand4.Columns.Add(this.bgcAfWaveName);
            this.gridBand4.Columns.Add(this.bgcAfChannel);
            this.gridBand4.Columns.Add(this.bgcAfRegion);
            this.gridBand4.Name = "gridBand4";
            this.gridBand4.Width = 326;
            // 
            // bgcAfWaveName
            // 
            this.bgcAfWaveName.Caption = "�����";
            this.bgcAfWaveName.FieldName = "WaveName";
            this.bgcAfWaveName.Name = "bgcAfWaveName";
            this.bgcAfWaveName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.bgcAfWaveName.Visible = true;
            this.bgcAfWaveName.Width = 164;
            // 
            // bgcAfChannel
            // 
            this.bgcAfChannel.Caption = "�����";
            this.bgcAfChannel.FieldName = "Channel";
            this.bgcAfChannel.Name = "bgcAfChannel";
            this.bgcAfChannel.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.bgcAfChannel.Visible = true;
            this.bgcAfChannel.Width = 60;
            // 
            // bgcAfRegion
            // 
            this.bgcAfRegion.Caption = "������";
            this.bgcAfRegion.FieldName = "Regions";
            this.bgcAfRegion.Name = "bgcAfRegion";
            this.bgcAfRegion.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.bgcAfRegion.Visible = true;
            this.bgcAfRegion.Width = 102;
            // 
            // bgcAfFilter_ID
            // 
            this.bgcAfFilter_ID.Caption = "bandedGridColumn1";
            this.bgcAfFilter_ID.FieldName = "Filter_ID";
            this.bgcAfFilter_ID.Name = "bgcAfFilter_ID";
            this.bgcAfFilter_ID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.bgcAfFilter_ID.Visible = true;
            // 
            // bgcAfFilter_str
            // 
            this.bgcAfFilter_str.Caption = "bandedGridColumn3";
            this.bgcAfFilter_str.FieldName = "Filter_str";
            this.bgcAfFilter_str.Name = "bgcAfFilter_str";
            this.bgcAfFilter_str.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.bgcAfFilter_str.Visible = true;
            // 
            // bgcAfUser_ID
            // 
            this.bgcAfUser_ID.Caption = "bandedGridColumn1";
            this.bgcAfUser_ID.FieldName = "User_ID";
            this.bgcAfUser_ID.Name = "bgcAfUser_ID";
            this.bgcAfUser_ID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.bgcAfUser_ID.Visible = true;
            // 
            // bgcAfWave_ID
            // 
            this.bgcAfWave_ID.Caption = "bandedGridColumn3";
            this.bgcAfWave_ID.FieldName = "Wave_ID";
            this.bgcAfWave_ID.Name = "bgcAfWave_ID";
            this.bgcAfWave_ID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.bgcAfWave_ID.Visible = true;
            // 
            // groupControl2
            // 
            this.groupControl2.Controls.Add(this.memoEditAllFilterCriterium);
            this.groupControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.groupControl2.Location = new System.Drawing.Point(0, 369);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(861, 100);
            this.groupControl2.TabIndex = 2;
            this.groupControl2.Text = "�������� ����������";
            // 
            // memoEditAllFilterCriterium
            // 
            this.memoEditAllFilterCriterium.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.allFilterInfoBindingSource, "Filter_text", true));
            this.memoEditAllFilterCriterium.Dock = System.Windows.Forms.DockStyle.Fill;
            this.memoEditAllFilterCriterium.Location = new System.Drawing.Point(2, 22);
            this.memoEditAllFilterCriterium.Name = "memoEditAllFilterCriterium";
            this.memoEditAllFilterCriterium.Properties.ReadOnly = true;
            this.memoEditAllFilterCriterium.Size = new System.Drawing.Size(857, 76);
            this.memoEditAllFilterCriterium.TabIndex = 0;
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.btnCancel);
            this.panelControl1.Controls.Add(this.btnOk);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl1.Location = new System.Drawing.Point(0, 512);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(872, 46);
            this.panelControl1.TabIndex = 1;
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Image = global::SoftServe.Reports.ThR.SummaryReport.Properties.Resources.Close_16;
            this.btnCancel.Location = new System.Drawing.Point(785, 11);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 1;
            this.btnCancel.Text = "���";
            // 
            // btnOk
            // 
            this.btnOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOk.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnOk.Image = global::SoftServe.Reports.ThR.SummaryReport.Properties.Resources.Check_16;
            this.btnOk.Location = new System.Drawing.Point(704, 11);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(75, 23);
            this.btnOk.TabIndex = 0;
            this.btnOk.Text = "��";
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.xtraTabControl);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl2.Location = new System.Drawing.Point(0, 0);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(872, 512);
            this.panelControl2.TabIndex = 2;
            // 
            // SelectFilterForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(872, 558);
            this.Controls.Add(this.panelControl2);
            this.Controls.Add(this.panelControl1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "SelectFilterForm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "���������� ���������";
            this.Load += new System.EventHandler(this.SelectFilterForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl)).EndInit();
            this.xtraTabControl.ResumeLayout(false);
            this.xtraTabPageOwnFilters.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlOwnFilters)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ownFilterInfoBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bandedGridViewOwnFilters)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControlOwnMemo)).EndInit();
            this.panelControlOwnMemo.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.memoEditOwnFilterCriterium.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControlDelete)).EndInit();
            this.panelControlDelete.ResumeLayout(false);
            this.xtraTabPageAllFilters.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).EndInit();
            this.panelControl4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlAllFilters)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.allFilterInfoBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bandedGridViewAllFilters)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.memoEditAllFilterCriterium.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraTab.XtraTabControl xtraTabControl;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageOwnFilters;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageAllFilters;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.SimpleButton btnCancel;
        private DevExpress.XtraEditors.SimpleButton btnOk;
        private DevExpress.XtraEditors.PanelControl panelControl3;
        private DevExpress.XtraGrid.GridControl gridControlOwnFilters;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.MemoEdit memoEditOwnFilterCriterium;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridView bandedGridViewOwnFilters;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bgcOfFilter_ID;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bgcOfFilter_Name;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bgcOfFilter_str;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bgcOfFilter_text;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bgcOfFilter_date;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bgcOfUser_ID;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bgcOfUserName;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bgcOfWave_ID;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bgcOfWaveName;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bgcOfRegion;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bgcOfChannel;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand1;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand2;
        private DevExpress.XtraEditors.PanelControl panelControl4;
        private DevExpress.XtraGrid.GridControl gridControlAllFilters;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridView bandedGridViewAllFilters;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand3;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bgcAfFilter_Name;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bgcAfUserName;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bgcAfFilter_date;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bgcAfFilter_text;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand4;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bgcAfWaveName;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bgcAfChannel;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bgcAfRegion;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bgcAfFilter_ID;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bgcAfFilter_str;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bgcAfUser_ID;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bgcAfWave_ID;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraEditors.MemoEdit memoEditAllFilterCriterium;
        private DevExpress.XtraEditors.PanelControl panelControlOwnMemo;
        private DevExpress.XtraEditors.PanelControl panelControlDelete;
        private DevExpress.XtraEditors.SimpleButton btnDelete;
        private System.Windows.Forms.BindingSource ownFilterInfoBindingSource;
        private System.Windows.Forms.BindingSource allFilterInfoBindingSource;
    }
}