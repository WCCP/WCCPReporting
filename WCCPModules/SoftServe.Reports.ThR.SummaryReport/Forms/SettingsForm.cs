#region

using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Threading;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using Logica.Reports.BaseReportControl.Utility;
using Logica.Reports.Common.WaitWindow;
using SoftServe.Reports.ThR.SummaryReport.DataProvider;
using SoftServe.Reports.ThR.SummaryReport.Utility;
using WccpReporting;

#endregion

namespace SoftServe.Reports.ThR.SummaryReport.Forms
{
    public partial class SettingsForm : XtraForm
    {
        #region Fields

        private bool isDataLoaded;
        private Countries _currentCountry;
        private List<Countries> _userCountries = new List<Countries>(); 
        #endregion

        #region Constructors

        public SettingsForm()
        {
            InitializeComponent();
        }

        #endregion

        #region Instance Properties

        private FilterInfo Filter { get; set; }

        public SettingsSnapshot Settings { get; set; }

        #endregion

        #region Instance Methods

        private void LoadData() {
            Enabled = false;
            WaitManager.StartWait();
            try
            {
                _userCountries = DataAccessProvider.GetUserCountries();

                if (_userCountries.Count == 0)
                {
                    XtraMessageBox.Show("�� ���������� ������ ������������", "Summary Report", MessageBoxButtons.OK,
                                        MessageBoxIcon.Error);
                    return;
                }

                DataTable lWavesTable = DataAccessProvider.GetWavesByCountries(_userCountries);
                DataSet lRegions = DataAccessProvider.GetRegions();

                Text = WccpUI.ReportName;
                lkpWave.Properties.DataSource = lWavesTable;
                if (lWavesTable.Rows.Count > 0)
                    lkpWave.EditValue = lWavesTable.Rows[lWavesTable.Rows.Count - 1][lkpWave.Properties.ValueMember];

                DataTable lRegionsUkr = lRegions.Tables["UkrainianRegions"];
                lkpRegionUkraine.Properties.DataSource = lRegionsUkr;
                if (lRegionsUkr.Rows.Count > 0)
                {
                    lkpRegionUkraine.Properties.DropDownRows = lRegionsUkr.Rows.Count;
                    lkpRegionUkraine.EditValue = lRegionsUkr.Rows[0][lkpRegionUkraine.Properties.ValueMember];
                }

                if (!DataAccessProvider.IsLDB)
                {
                    DataTable lRegionsRu = lRegions.Tables["RussianRegions"];
                    if (lRegionsRu.Rows.Count > 0)
                    {
                        cRegionsRussia.LoadDataSource(lRegionsRu);
                        cRegionsRussia.Properties.DropDownRows = lRegionsRu.Rows.Count + 1;
                    }
                }
            }
            catch (Exception e)
            {
            }
            finally
            {
                Enabled = true;
                WaitManager.StopWait();
            }
        }

        private bool ValidateData()
        {
            if (_currentCountry == Countries.Russia && cRegionsRussia.GetComboBoxCheckedValues().Count == 0)
            {
                XtraMessageBox.Show("����� ������� ������", "Summary Report", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }

            return true;
        }

        #endregion

        #region Event Handling

        private void SettingsForm_Load(object sender, EventArgs e)
        {
            if (isDataLoaded) return;

            LoadData();
            isDataLoaded = true;
        }

        private void btnSelectFilter_Click(object sender, EventArgs e)
        {
            var form = new SelectFilterForm();
            if (form.ShowDialog() != DialogResult.OK) return;

            Filter = form.CurrentFilter;
            txtFilterName.EditValue = Filter.Filter_Name;
        }

        private void btnYes_Click(object sender, EventArgs e)
        {
            if (ValidateData())
            {
                Settings = new SettingsSnapshot
                               {
                                   Filter = chkWithoutFilter.Checked ? null : Filter,
                                   WaveId = ConvertEx.ToInt(lkpWave.EditValue),
                                   WaveName = lkpWave.Properties.GetDisplayText(lkpWave.EditValue),
                                   ChannelId = ConvertEx.ToInt(lkpChannel.EditValue),
                                   ChannelName = lkpChannel.Properties.GetDisplayText(lkpChannel.EditValue),
                                   RegionIds = GetRegionIds(),
                                   RegionNames = GetRegionNames()
                               };
                DialogResult = DialogResult.OK;
            }
        }

        private void chkWithoutFilter_CheckedChanged(object sender, EventArgs e)
        {
            btnSelectFilter.Enabled = !chkWithoutFilter.Checked;
            txtFilterName.Enabled = !chkWithoutFilter.Checked;
            lkpChannel.Enabled = chkWithoutFilter.Checked;
        }

        private List<int> GetRegionIds()
        {
            List<int> result = new List<int>();

            if (_currentCountry == Countries.Ukraine)
            {
                result.Add(ConvertEx.ToInt(lkpRegionUkraine.EditValue));
            }
            else
            {
                result = (from string regionId in cRegionsRussia.GetComboBoxCheckedValues() select int.Parse(regionId)).ToList();
            }

            return result;
        }

        private List<string> GetRegionNames()
        {
            List<string> result = new List<string>();

            if (_currentCountry == Countries.Ukraine)
            {
                result.Add(lkpRegionUkraine.Properties.GetDisplayText(lkpRegionUkraine.EditValue));
            }
            else
            {
                result =
                    (from CheckedListBoxItem item in cRegionsRussia.Properties.Items where item.CheckState == CheckState.Checked select item.Description).ToList();
            }

            return result;
        }

        #endregion

        private void lkpWave_EditValueChanged(object sender, EventArgs e)
        {
            if(lkpWave.EditValue == null)
                return;

            DataRowView row = lkpWave.Properties.GetDataSourceRowByKeyValue(lkpWave.EditValue) as DataRowView;
            Countries lWaveCountry = DataAccessProvider.IsLDB ? Countries.Ukraine : (Countries) Convert.ToInt32(row["Country_id"]);

           if(lWaveCountry == _currentCountry)
               return;

           _currentCountry = lWaveCountry;
            
            LoadRegionsByWave();
            LoadChannelsByWave();
        }

        private void LoadChannelsByWave()
        {
            DataTable lChannelsTable = DataAccessProvider.GetChannelsByCountry(_currentCountry, ConvertEx.ToInt(lkpWave.EditValue));

            lkpChannel.Properties.DataSource = lChannelsTable;
            if (lChannelsTable.Rows.Count > 0)
                lkpChannel.EditValue = lChannelsTable.Rows[0][lkpChannel.Properties.ValueMember];
        }

        private void LoadRegionsByWave()
        {
            if (_currentCountry == Countries.Russia)
            {
                lkpRegionUkraine.Visible = false;
                lkpRegionUkraine.Enabled = false;

                cRegionsRussia.Enabled = true;
                cRegionsRussia.Visible = true;
                cRegionsRussia.Size = new Size(275, 20);
                cRegionsRussia.Location = new Point(5, 134);
            }
            else
            {
                cRegionsRussia.Visible = false;
                cRegionsRussia.Enabled = false;

                lkpRegionUkraine.Enabled = true;
                lkpRegionUkraine.Visible = true;
                lkpRegionUkraine.Size = new Size(275, 20);
                lkpRegionUkraine.Location = new Point(5, 134);
            }
        }
    }
}