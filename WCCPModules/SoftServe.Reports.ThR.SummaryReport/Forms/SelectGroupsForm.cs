﻿using System;
using System.Collections.Generic;
using System.Linq;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using SoftServe.Reports.ThR.SummaryReport.DataProvider;

namespace SoftServe.Reports.ThR.SummaryReport.Forms
{
    public partial class SelectGroupsForm : XtraForm
    {
        #region Fields

        private bool programmUpdate;

        #endregion

        #region Constructors

        public SelectGroupsForm()
        {
            InitializeComponent();
        }

        #endregion

        #region Instance Properties

        public IEnumerable<GroupVisibility> Groups
        {
            get { return (IEnumerable<GroupVisibility>)groupVisibilityBindingSource.DataSource; }
            set
            {
                groupVisibilityBindingSource.DataSource = value;
                gridView_CellValueChanged(null, null);
            }
        }

        #endregion

        #region Event Handling

        private void chkSelDeselAll_CheckedChanged(object sender, EventArgs e)
        {
            if (programmUpdate) return;
            gridControl.BeginUpdate();
            gridView.BeginUpdate();
            foreach (var groupVisibility in Groups)
            {
                groupVisibility.IsVisible = chkSelDeselAll.Checked;
            }
            gridControl.RefreshDataSource();
            gridView.EndUpdate();
            gridControl.EndUpdate();
            btnOk.Enabled = Groups.Any(x => x.IsVisible);
        }

        private void gridView_CellValueChanged(object sender, CellValueChangedEventArgs e)
        {
            programmUpdate = true;
            if (Groups.All(x => x.IsVisible))
                chkSelDeselAll.Checked = true;
            if (Groups.All(x => !x.IsVisible))
                chkSelDeselAll.Checked = false;
            programmUpdate = false;
            btnOk.Enabled = Groups.Any(x => x.IsVisible);
        }

        private void gridView_RowCellClick(object sender, RowCellClickEventArgs e)
        {
            if (e.Column == gcVisible)
                gridView.SetRowCellValue(e.RowHandle, e.Column, !(bool)e.CellValue);
        }

        #endregion
    }
}