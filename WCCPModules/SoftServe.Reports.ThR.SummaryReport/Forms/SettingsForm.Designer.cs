namespace SoftServe.Reports.ThR.SummaryReport.Forms
{
    partial class SettingsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btnYes = new DevExpress.XtraEditors.SimpleButton();
            this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.cRegionsRussia = new Logica.Reports.BaseReportControl.CommonControls.ComboboxExtended(this.components);
            this.btnSelectFilter = new DevExpress.XtraEditors.SimpleButton();
            this.chkWithoutFilter = new DevExpress.XtraEditors.CheckEdit();
            this.txtFilterName = new DevExpress.XtraEditors.TextEdit();
            this.lkpRegionUkraine = new DevExpress.XtraEditors.LookUpEdit();
            this.lkpChannel = new DevExpress.XtraEditors.LookUpEdit();
            this.lkpWave = new DevExpress.XtraEditors.LookUpEdit();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cRegionsRussia.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkWithoutFilter.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFilterName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lkpRegionUkraine.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lkpChannel.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lkpWave.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // btnYes
            // 
            this.btnYes.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnYes.Image = global::SoftServe.Reports.ThR.SummaryReport.Properties.Resources.Check_16;
            this.btnYes.ImageIndex = 0;
            this.btnYes.Location = new System.Drawing.Point(147, 260);
            this.btnYes.Name = "btnYes";
            this.btnYes.Size = new System.Drawing.Size(72, 23);
            this.btnYes.TabIndex = 1;
            this.btnYes.Text = "��";
            this.btnYes.Click += new System.EventHandler(this.btnYes_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Image = global::SoftServe.Reports.ThR.SummaryReport.Properties.Resources.Close_16;
            this.btnCancel.ImageIndex = 1;
            this.btnCancel.Location = new System.Drawing.Point(225, 260);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(72, 23);
            this.btnCancel.TabIndex = 2;
            this.btnCancel.Text = "���";
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.cRegionsRussia);
            this.groupControl1.Controls.Add(this.btnSelectFilter);
            this.groupControl1.Controls.Add(this.chkWithoutFilter);
            this.groupControl1.Controls.Add(this.txtFilterName);
            this.groupControl1.Controls.Add(this.lkpRegionUkraine);
            this.groupControl1.Controls.Add(this.lkpChannel);
            this.groupControl1.Controls.Add(this.lkpWave);
            this.groupControl1.Controls.Add(this.labelControl4);
            this.groupControl1.Controls.Add(this.labelControl3);
            this.groupControl1.Controls.Add(this.labelControl2);
            this.groupControl1.Controls.Add(this.labelControl1);
            this.groupControl1.Location = new System.Drawing.Point(12, 12);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(285, 238);
            this.groupControl1.TabIndex = 3;
            this.groupControl1.Text = "����� ������:";
            // 
            // cRegionsRussia
            // 
            this.cRegionsRussia.Delimiter = ",";
            this.cRegionsRussia.Location = new System.Drawing.Point(154, 134);
            this.cRegionsRussia.Name = "cRegionsRussia";
            this.cRegionsRussia.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cRegionsRussia.Properties.DisplayMember = "NAME";
            this.cRegionsRussia.Properties.SelectAllItemCaption = "(���)";
            this.cRegionsRussia.Properties.ShowButtons = false;
            this.cRegionsRussia.Properties.ShowPopupCloseButton = false;
            this.cRegionsRussia.Properties.ValueMember = "ID";
            this.cRegionsRussia.Size = new System.Drawing.Size(126, 20);
            this.cRegionsRussia.TabIndex = 11;
            // 
            // btnSelectFilter
            // 
            this.btnSelectFilter.Enabled = false;
            this.btnSelectFilter.Image = global::SoftServe.Reports.ThR.SummaryReport.Properties.Resources.FiltersManage_16;
            this.btnSelectFilter.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnSelectFilter.Location = new System.Drawing.Point(245, 178);
            this.btnSelectFilter.Name = "btnSelectFilter";
            this.btnSelectFilter.Size = new System.Drawing.Size(35, 23);
            this.btnSelectFilter.TabIndex = 9;
            this.btnSelectFilter.Click += new System.EventHandler(this.btnSelectFilter_Click);
            // 
            // chkWithoutFilter
            // 
            this.chkWithoutFilter.EditValue = true;
            this.chkWithoutFilter.Location = new System.Drawing.Point(5, 205);
            this.chkWithoutFilter.Name = "chkWithoutFilter";
            this.chkWithoutFilter.Properties.Caption = "��� �������";
            this.chkWithoutFilter.Size = new System.Drawing.Size(143, 19);
            this.chkWithoutFilter.TabIndex = 8;
            this.chkWithoutFilter.CheckedChanged += new System.EventHandler(this.chkWithoutFilter_CheckedChanged);
            // 
            // txtFilterName
            // 
            this.txtFilterName.Enabled = false;
            this.txtFilterName.Location = new System.Drawing.Point(5, 179);
            this.txtFilterName.Name = "txtFilterName";
            this.txtFilterName.Size = new System.Drawing.Size(234, 20);
            this.txtFilterName.TabIndex = 7;
            // 
            // lkpRegionUkraine
            // 
            this.lkpRegionUkraine.Location = new System.Drawing.Point(5, 134);
            this.lkpRegionUkraine.Name = "lkpRegionUkraine";
            this.lkpRegionUkraine.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.lkpRegionUkraine.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lkpRegionUkraine.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("NAME", "Region_Name")});
            this.lkpRegionUkraine.Properties.DisplayMember = "NAME";
            this.lkpRegionUkraine.Properties.NullText = "";
            this.lkpRegionUkraine.Properties.ShowFooter = false;
            this.lkpRegionUkraine.Properties.ShowHeader = false;
            this.lkpRegionUkraine.Properties.ValueMember = "ID";
            this.lkpRegionUkraine.Size = new System.Drawing.Size(132, 20);
            this.lkpRegionUkraine.TabIndex = 6;
            // 
            // lkpChannel
            // 
            this.lkpChannel.Location = new System.Drawing.Point(5, 89);
            this.lkpChannel.Name = "lkpChannel";
            this.lkpChannel.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.lkpChannel.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lkpChannel.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("ChanelType", "ChanelType")});
            this.lkpChannel.Properties.DisplayMember = "ChanelType";
            this.lkpChannel.Properties.NullText = "";
            this.lkpChannel.Properties.ShowFooter = false;
            this.lkpChannel.Properties.ShowHeader = false;
            this.lkpChannel.Properties.ValueMember = "ChanelType_id";
            this.lkpChannel.Size = new System.Drawing.Size(275, 20);
            this.lkpChannel.TabIndex = 5;
            // 
            // lkpWave
            // 
            this.lkpWave.Location = new System.Drawing.Point(5, 44);
            this.lkpWave.Name = "lkpWave";
            this.lkpWave.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.lkpWave.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lkpWave.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("WaveName", "WaveName")});
            this.lkpWave.Properties.DisplayMember = "WaveName";
            this.lkpWave.Properties.NullText = "";
            this.lkpWave.Properties.ShowFooter = false;
            this.lkpWave.Properties.ShowHeader = false;
            this.lkpWave.Properties.ValueMember = "Wave_ID";
            this.lkpWave.Size = new System.Drawing.Size(275, 20);
            this.lkpWave.TabIndex = 4;
            this.lkpWave.EditValueChanged += new System.EventHandler(this.lkpWave_EditValueChanged);
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(5, 160);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(99, 13);
            this.labelControl4.TabIndex = 3;
            this.labelControl4.Text = "�������� �������:";
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(5, 115);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(39, 13);
            this.labelControl3.TabIndex = 2;
            this.labelControl3.Text = "������:";
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(5, 70);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(35, 13);
            this.labelControl2.TabIndex = 1;
            this.labelControl2.Text = "�����:";
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(5, 25);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(34, 13);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "�����:";
            // 
            // SettingsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(309, 295);
            this.Controls.Add(this.groupControl1);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnYes);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "SettingsForm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Summary report";
            this.Load += new System.EventHandler(this.SettingsForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cRegionsRussia.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkWithoutFilter.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFilterName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lkpRegionUkraine.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lkpChannel.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lkpWave.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton btnYes;
        private DevExpress.XtraEditors.SimpleButton btnCancel;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.SimpleButton btnSelectFilter;
        private DevExpress.XtraEditors.CheckEdit chkWithoutFilter;
        private DevExpress.XtraEditors.TextEdit txtFilterName;
        private DevExpress.XtraEditors.LookUpEdit lkpRegionUkraine;
        private DevExpress.XtraEditors.LookUpEdit lkpChannel;
        private DevExpress.XtraEditors.LookUpEdit lkpWave;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private Logica.Reports.BaseReportControl.CommonControls.ComboboxExtended cRegionsRussia;
    }
}