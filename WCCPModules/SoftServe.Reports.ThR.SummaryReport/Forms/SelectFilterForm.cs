using System;
using System.Linq;
using System.Threading;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Logica.Reports.BaseReportControl.CommonFunctionality;
using Logica.Reports.Common.WaitWindow;
using SoftServe.Reports.ThR.SummaryReport.DataProvider;

namespace SoftServe.Reports.ThR.SummaryReport.Forms
{
    public partial class SelectFilterForm : XtraForm
    {
        #region Fields

        private bool isDataLoaded;

        #endregion

        #region Constructors

        public SelectFilterForm()
        {
            InitializeComponent();
        }

        #endregion

        public FilterInfo CurrentFilter
        {
            get
            {
                if (xtraTabControl.SelectedTabPage == xtraTabPageOwnFilters)
                {
                    return ((FilterInfo)(ownFilterInfoBindingSource.Current));
                }
                return ((FilterInfo)allFilterInfoBindingSource.Current);
            }
        }

        #region Instance Methods

        private void LoadData()
        {
            new Thread(() =>
                           {
                               Invoke(new Action(() =>
                                                     {
                                                         Enabled = false;
                                                         WaitManager.StartWait();
                                                     }));
                               try
                               {
                                   var ownFilters = DataAccessProvider.GetThRFilters(false);
                                   var allFilters = DataAccessProvider.GetThRFilters(true);
                                   Invoke(new Action(() =>
                                                         {
                                                             ownFilterInfoBindingSource.DataSource = ownFilters;
                                                             allFilterInfoBindingSource.DataSource = allFilters;

                                                             btnDelete.Enabled = ownFilters.Any();
                                                             xtraTabControl_SelectedPageChanged(null, null);
                                                         }));
                               }
                               finally
                               {
                                   Invoke(new Action(() =>
                                                         {
                                                             Enabled = true;
                                                             WaitManager.StopWait();
                                                         }));
                               }
                           }).Start();
        }

        #endregion

        #region Event Handling

        private void SelectFilterForm_Load(object sender, EventArgs e)
        {
            btnDelete.Visible = !UserType.Current.IsM2;
            
            if (!isDataLoaded)
            {
                LoadData();
                isDataLoaded = true;
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            var confirmationResult =
                XtraMessageBox.Show("������� ������ ����� ������." + Environment.NewLine + "������� ������ ?",
                                    "�������������",
                                    MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (confirmationResult == DialogResult.Yes)
            {
                var filterId = ((FilterInfo)ownFilterInfoBindingSource.Current).Filter_ID;
                new Thread(() =>
                               {
                                   Invoke(new Action(() =>
                                                         {
                                                             Enabled = false;
                                                             WaitManager.StartWait();
                                                         }));
                                   try
                                   {
                                       DataAccessProvider.DeleteThRFilter(filterId);
                                   }
                                   finally
                                   {
                                       Invoke(new Action(() =>
                                                             {
                                                                 Enabled = true; 
                                                                 WaitManager.StopWait();
                                                                 LoadData();
                                                             }));
                                   }
                               }).Start();
            }
        }

        #endregion

        private void xtraTabControl_SelectedPageChanged(object sender, DevExpress.XtraTab.TabPageChangedEventArgs e)
        {
            if (xtraTabControl.SelectedTabPage == xtraTabPageOwnFilters)
                btnOk.Enabled = ownFilterInfoBindingSource.Current != null;
            else
                btnOk.Enabled = allFilterInfoBindingSource.Current != null;
        }
    }
}