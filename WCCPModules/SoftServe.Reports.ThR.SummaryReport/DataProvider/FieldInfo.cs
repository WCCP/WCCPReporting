﻿namespace SoftServe.Reports.ThR.SummaryReport.DataProvider
{
    public class FieldInfo
    {
        public string Field_Name { get; set; }
        public int Group_Id { get; set; }
        public int SortOrder { get; set; }
        public int Width { get; set; }
        public bool IsPercent { get; set; }
    }
}
