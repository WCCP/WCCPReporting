﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Windows.Forms;
using Logica.Reports.BaseReportControl.Helpers;
using Logica.Reports.DataAccess;
using SoftServe.Reports.ThR.SummaryReport.Utility;

namespace SoftServe.Reports.ThR.SummaryReport.DataProvider
{
    internal static class DataAccessProvider
    {
        public const string SQL_GET_Countries = @"select * from dbo.fn_CountryByUser(system_user)";
        public const string SQL_GET_AccessibleRegions = @"SELECT r.Region_id as [ID], r.Region_name as [NAME], r.Country_id
                                                        FROM dbo.tblRegions r
                                                        WHERE r.Region_ID > 0 and r.[Status] = 2
                                                        and r.Region_id in 
                                                            (SELECT rg.Region_id
                                                            FROM dbo.SVbyUser(system_user) urm
                                                                join dbo.tblRM rm on rm.RM_ID = urm.RM_ID
                                                                join dbo.tblRegions rg on rg.RM_ID = rm.RM_ID
                                                            )
                                                        ORDER BY r.Region_name
                                                    ";

        public const string SQL_GET_AccessibleRegions_LDB = "spDW_GetUserAccesibleRegions";

        public const string SQL_GET_Channels = @"SELECT DISTINCT 
                                                                g.OLGroup_id AS ChanelType_id,
                                                                g.OLGroup_name AS ChanelType
                                                    FROM 
                                                                tblOutLetGroups                           AS g
                                                                INNER JOIN DW_TomasResearch_ChannelTypes  AS t ON t.OLGroup_ID = g.OLGroup_ID
                                                                INNER JOIN DW_TomasResearch_Wave          AS w ON w.ThR_ChannelType_ID = t.ThR_ChannelType_ID
                                                    WHERE
                                                                w.Wave_ID = 
                                                    ";
        private const string SQL_GET_WAVES = @"SELECT Wave_ID, WaveName, Country_id FROM DW_TomasResearch_Wave WHERE (isHidden = 0) ORDER BY WaveName";
        private const string SQL_GET_WAVES_LDB = @"SELECT Wave_ID, WaveName FROM DW_TomasResearch_Wave WHERE (isHidden = 0) and Country_id = 1 ORDER BY WaveName";
        
        private const string SQL_GET_FIELD_GROUPS = @"SELECT Group_ID,Group_Name,SortOrder, IsPercent FROM DW_TomasResearch_Group ORDER BY SortOrder";
        private const string SQL_GET_FILTERS = "DW_TomasResearch_ListFilters";
        private const string SQL_SET_FILTER = "DW_TomasResearch_PutFilter";
        private const string SQL_DEL_FILTER = "DW_TomasResearch_DelFilter";
        private const string SQL_GET_FIELDS = "DW_TomasResearch_GetFieldsByGroup";
        private const string SpDwTrCalcFl = "spDW_TR_Calc_FL";
        private const string SpDwTrCalcInconsistency = "spDW_TR_Calc_Inconsistency";
        private const string SpDwTrCalcSr = "spDW_TR_Calc_SR";
        private const string SQL_GET_FILTERS_PRM_1 = "@User_ID";
        private const string SQL_GET_FILTERS_PRM_2 = "@Wave_ID";
        private const string SQL_GET_FILTERS_PRM_3 = "@Region";
        private const string SQL_GET_FILTERS_PRM_4 = "@Channel";
        private const string SQL_DEL_FILTER_PRM_1 = "@Filter_ID";
        private const string SQL_GET_FIELDS_PRM_1 = "@Wave_ID";
        private const string SQL_SET_FILTER_PRM_1 = "@Filter_ID";
        private const string SQL_SET_FILTER_PRM_2 = "@Filter_Name";
        private const string SQL_SET_FILTER_PRM_3 = "@Filter_str";
        private const string SQL_SET_FILTER_PRM_4 = "@Filter_text";
        private const string SQL_SET_FILTER_PRM_5 = "@Filter_date";
        private const string SQL_SET_FILTER_PRM_6 = "@Wave_ID";
        private const string SQL_SET_FILTER_PRM_7 = "@Region";
        private const string SQL_SET_FILTER_PRM_8 = "@Channel";
        private const string SQL_SET_FILTER_PRM_9 = "@User_ID";

        public static string MsgError = "Ошибка";
        static public string MsgErrorPrepare = "Не могу сформировать предварительный набор данных.";

        #region SettingsForm

        public static List<Countries> GetUserCountries()
        {
            List<Countries> countries = new List<Countries>();

            if (DataAccessLayer.IsLDB)
            {
                countries.Add(Countries.Ukraine);
            }
            else
            {
                DataSet lDataSet = DataAccessLayer.ExecuteQuery(SQL_GET_Countries, new SqlParameter[] { });
                if (lDataSet != null && lDataSet.Tables.Count > 0)
                {
                    foreach (DataRow country in lDataSet.Tables[0].Rows)
                    {
                        countries.Add(country.Field<Countries>("CountryID"));
                    }
                }
            }

            return countries;
        }

        public static DataSet GetRegions()
        {
            if (IsLDB)
            {
                DataSet lDataSetLdb = DataAccessLayer.ExecuteQuery(SQL_GET_AccessibleRegions_LDB);

                if (lDataSetLdb != null && lDataSetLdb.Tables.Count > 0)
                {
                    DataSet lRegionSet = new DataSet();

                    DataView lRegionView = lDataSetLdb.Tables[0].DefaultView;
                    lRegionSet.Tables.Add(lRegionView.ToTable("UkrainianRegions"));
                    
                    return lRegionSet;
                }

                return null;
            }


            DataSet lDataSet = DataAccessLayer.ExecuteQuery(SQL_GET_AccessibleRegions);

            if (lDataSet != null && lDataSet.Tables.Count > 0)
            {
                DataSet lRegionSet = new DataSet();

                DataView lRegionView = lDataSet.Tables[0].DefaultView;
                lRegionView.RowFilter = "Country_id = 1";

                lRegionSet.Tables.Add(lRegionView.ToTable("UkrainianRegions"));

                lRegionView.RowFilter = "Country_id = 2";

                lRegionSet.Tables.Add(lRegionView.ToTable("RussianRegions"));

                return lRegionSet;
            }

            return null;
        }

        public static DataTable GetWavesByCountries(List<Countries> countries)
        {
            if (IsLDB)
            {
                DataSet lDataSetLdb = DataAccessLayer.ExecuteQuery(SQL_GET_WAVES_LDB);

                if (lDataSetLdb != null && lDataSetLdb.Tables.Count > 0)
                {
                    return lDataSetLdb.Tables[0];
                }

                return null;
            }

            DataSet lDataSet = DataAccessLayer.ExecuteQuery(SQL_GET_WAVES);

            if (lDataSet != null && lDataSet.Tables.Count > 0)
            {
                if (countries.Count == 1)
                {
                    DataView lWavesView = lDataSet.Tables[0].DefaultView;
                    lWavesView.RowFilter = "Country_id = " + (int)countries[0];
                    return lWavesView.ToTable();
                }

                return lDataSet.Tables[0];
            }
            return null;
        }

        public static DataTable GetChannelsByCountry(Countries country, int waveId)
        {
            if (country == Countries.Ukraine)
            {
                var dt = new DataTable();
                dt.Columns.Add("ChanelType_id", typeof(int));
                dt.Columns.Add("ChanelType", typeof(string));
                dt.Rows.Add(new object[] { 2, "Off-Trade" });
                dt.Rows.Add(new object[] { 1, "On-Trade" });
                return dt;
            }

            DataSet lDataSet = DataAccessLayer.ExecuteQuery(SQL_GET_Channels + waveId);

            if (lDataSet != null && lDataSet.Tables.Count > 0)
            {
                return lDataSet.Tables[0];
            }

            return null;
        }

        #endregion

        #region SelectFilterForm

        public static IEnumerable<FilterInfo> GetThRFilters(bool isAllFilters)
        {
            var dataSet = DataAccessLayer.ExecuteStoredProcedure(SQL_GET_FILTERS,
                                                                 new[]
                                                                     {
                                                                         new SqlParameter(SQL_GET_FILTERS_PRM_1,
                                                                                          isAllFilters
                                                                                              ? (object) DBNull.Value
                                                                                              : -1),
                                                                         new SqlParameter(SQL_GET_FILTERS_PRM_2, DBNull.Value),
                                                                         new SqlParameter(SQL_GET_FILTERS_PRM_3, DBNull.Value),
                                                                         new SqlParameter(SQL_GET_FILTERS_PRM_4, DBNull.Value)
                                                                     });
            if (dataSet != null && dataSet.Tables.Count > 0)
            {
                return dataSet.Tables[0].AsEnumerable().Select(r => new FilterInfo
                                                                        {
                                                                            Filter_ID = r.Field<long>("Filter_ID"),
                                                                            Filter_Name = r.Field<string>("Filter_Name"),
                                                                            Filter_text = r.Field<string>("Filter_text"),
                                                                            Filter_date = r.Field<DateTime>("Filter_date"),
                                                                            User_ID = r.Field<int?>("User_ID"),
                                                                            UserName = r.Field<string>("UserName"),
                                                                            Wave_ID = r.Field<int>("Wave_ID"),
                                                                            WaveName = r.Field<string>("WaveName"),
                                                                            Regions = r.Field<string>("Region"),
                                                                            Channel = r.Field<string>("Channel")
                                                                        }).ToArray();
            }
            return new List<FilterInfo>();
        }

        public static void DeleteThRFilter(long filterId)
        {
            DataAccessLayer.ExecuteNonQueryStoredProcedure(SQL_DEL_FILTER,
                                                           new[]
                                                               {
                                                                   new SqlParameter(SQL_DEL_FILTER_PRM_1, filterId)
                                                               });
        }

        #endregion

        #region Report

        public static DataTable GetPriceUnits()
        {
            var result = new DataTable();
            result.Columns.Add("UnitId", typeof(bool));
            result.Columns.Add("UnitName", typeof(string));
            result.Rows.Add(new object[] { false, "UAH/DAL" });
            result.Rows.Add(new object[] { true, "UAH/SKU" });
            return result;
        }

        public static DataTable GetSummaryReport(SettingsSnapshot settings, bool skipOutlets)
        {
            var listOutlets = (skipOutlets || settings.SelectedPOC.Count() == 0)
                                  ? (object)DBNull.Value
                                  : settings.SelectedPOC.ToString(",");
            DataSet dataSet;
            if (DataAccessLayer.IsLDB)
            {
                dataSet = DataAccessLayer.ExecuteStoredProcedure(@"spDW_Tr_SR_Total",
                                                                     new[]
                                                                     {
                                                                         new SqlParameter("@ParRegion", settings.RegionsDescription),
                                                                         new SqlParameter("@ParChanel", settings.ChannelName),
                                                                         new SqlParameter("@listOutlets", listOutlets)
                                                                     });
            }
            else
            {
                dataSet = DataAccessLayer.ExecuteStoredProcedure(@"DW_TomasResearch_SummaryTotal",
                                                                     new[]
                                                                     {
                                                                         new SqlParameter("@ParRegion", settings.RegionsDescription),
                                                                         new SqlParameter("@ParChanel", settings.ChannelName),
                                                                         new SqlParameter("@Wave_ID", settings.WaveId),
                                                                         new SqlParameter("@isBySKU", settings.IsBySKU),
                                                                         new SqlParameter("@listOutlets", listOutlets)
                                                                     });
            }
            if (dataSet != null && dataSet.Tables.Count > 0)
                return dataSet.Tables[0];
            return null;
        }

        public static DataTable GetDataReport(SettingsSnapshot settings)
        {
            DataSet dataSet;
            if (DataAccessLayer.IsLDB)
            {
                dataSet = DataAccessLayer.ExecuteStoredProcedure(@"spDW_TR_SummaryData",
                                                                 new[]
                                                                 {
                                                                     new SqlParameter("@ParRegion", settings.RegionsDescription),
                                                                     new SqlParameter("@ParSV", DBNull.Value),
                                                                     new SqlParameter("@ParChanel", settings.ChannelName),
                                                                     new SqlParameter("@wave_id",settings.WaveId)
                                                                 });
            }
            else
            {
                dataSet = DataAccessLayer.ExecuteStoredProcedure(@"DW_TomasResearch_SummaryReport",
                                                                 new[]
                                                                 {
                                                                     new SqlParameter("@ParRegion", settings.RegionsDescription),
                                                                     new SqlParameter("@ParChanel", settings.ChannelName),
                                                                     new SqlParameter("@wave_id", settings.WaveId)
                                                                 });
            }
            if (dataSet != null && dataSet.Tables.Count > 0)
                return dataSet.Tables[0];
            return null;
        }

        public static IEnumerable<GroupInfo> GetFieldGroups(SettingsSnapshot settings)
        {
            var dataSet = DataAccessLayer.ExecuteQuery(SQL_GET_FIELD_GROUPS);
            if (dataSet != null && dataSet.Tables.Count > 0)
                return dataSet.Tables[0].AsEnumerable().Select(r => new GroupInfo
                                                                        {
                                                                            Group_ID = r.Field<int>("Group_ID"),
                                                                            Group_Name = r.Field<string>("Group_Name"),
                                                                            SortOrder = r.Field<int>("SortOrder"),
                                                                            IsPercent = r.Field<bool>("IsPercent")
                                                                        }).ToArray();
            return new List<GroupInfo>();
        }

        public static IEnumerable<FieldInfo> GetFields(SettingsSnapshot settings)
        {
            var dataSet = DataAccessLayer.ExecuteStoredProcedure(SQL_GET_FIELDS, new[] { new SqlParameter(SQL_GET_FIELDS_PRM_1, settings.WaveId), });
            if (dataSet != null && dataSet.Tables.Count > 0)
                return dataSet.Tables[0].AsEnumerable().Select(r => new FieldInfo
                {
                    Field_Name = r.Field<string>("Field_Name"),
                    Group_Id = r.Field<int>("Group_Id"),
                    SortOrder = r.Field<int>("SortOrder"),
                    Width = r.Field<int>("Width"),
                    IsPercent = r.Field<bool>("IsPercent")
                }).ToArray();
            return new List<FieldInfo>();
        }

        public static void SaveThRFilter(FilterInfo filter)
        {
            DataAccessLayer.ExecuteNonQueryStoredProcedure(SQL_SET_FILTER,
                                                           new[]
                                                               {
                                                                   new SqlParameter(SQL_SET_FILTER_PRM_1, (filter.Filter_ID > 0) ? (object)filter.Filter_ID : DBNull.Value),
                                                                   new SqlParameter(SQL_SET_FILTER_PRM_2, filter.Filter_Name),
                                                                   new SqlParameter(SQL_SET_FILTER_PRM_3, new byte[]{}),
                                                                   new SqlParameter(SQL_SET_FILTER_PRM_4, filter.Filter_text),
                                                                   new SqlParameter(SQL_SET_FILTER_PRM_5, filter.Filter_date),
                                                                   new SqlParameter(SQL_SET_FILTER_PRM_6, filter.Wave_ID),
                                                                   new SqlParameter(SQL_SET_FILTER_PRM_7, filter.Regions),
                                                                   new SqlParameter(SQL_SET_FILTER_PRM_8, filter.Channel),
                                                                   new SqlParameter(SQL_SET_FILTER_PRM_9, DBNull.Value)
                                                               });
        }

        public static bool PrepareM2Data(SettingsSnapshot settings)
        {
            try
            {
                DataAccessLayer.ExecuteNonQueryStoredProcedure(SpDwTrCalcFl);
                DataAccessLayer.ExecuteNonQueryStoredProcedure(SpDwTrCalcInconsistency);
                DataAccessLayer.ExecuteNonQueryStoredProcedure(SpDwTrCalcSr);
            }
            catch (Exception lException)
            {
                MessageBox.Show(MsgErrorPrepare + "\n" + lException.Message, MsgError,
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            return true;
        }

        #endregion

        public static bool IsLDB
        {
            get { return DataAccessLayer.IsLDB; }
        }
    }

}