﻿namespace SoftServe.Reports.ThR.SummaryReport.DataProvider
{
    public class GroupInfo
    {
        public int Group_ID { get; set; }
        public string Group_Name { get; set; }
        public int SortOrder { get; set; }
        public bool IsPercent { get; set; }
    }
}
