﻿namespace SoftServe.Reports.ThR.SummaryReport.DataProvider
{
    public class GroupVisibility
    {
        public string GroupName { get; set; }
        public bool IsVisible { get; set; }
    }
}