﻿using System;

namespace SoftServe.Reports.ThR.SummaryReport.DataProvider
{
    public class FilterInfo
    {
        public long Filter_ID { get; set; }
        public string Filter_Name { get; set; }
        /// <summary>
        /// Filter criteria represented as string
        /// </summary>
        public string Filter_text { get; set; }
        public DateTime Filter_date { get; set; }
        public int? User_ID { get; set; }
        public string UserName { get; set; }
        public int Wave_ID { get; set; }
        public string WaveName { get; set; }
        public string Regions { get; set; }
        public string Channel { get; set; }
    }
}