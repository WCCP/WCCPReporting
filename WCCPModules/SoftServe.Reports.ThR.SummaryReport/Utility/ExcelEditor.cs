﻿using System;
using System.Collections.Generic;
using System.Linq;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;

namespace SoftServe.Reports.ThR.SummaryReport.Utility
{
    public class ExcelEditor
    {
        private SpreadsheetDocument _excelFile;
        private Worksheet _worksheet;

        private string _fileName;

        public ExcelEditor(string fileName)
        {
            _fileName = fileName;
        }

        public void StartWriting()
        {
            _excelFile = SpreadsheetDocument.Open(_fileName, true);
        }

        public void FinishWriting()
        {
            try
            {
                _worksheet.Save();
                _excelFile.Close();
                FreeResourses();
            }
            catch (Exception)
            {
            }
        }

        public void UpdateCell(string text, uint rowIndex, string columnName, CellValues cellValue)
        {
            if (_worksheet != null)
            {
                Cell lCell = GetCell(columnName, rowIndex);

                lCell.CellValue = new CellValue(text);
                lCell.DataType = new EnumValue<CellValues>(cellValue);

                //_worksheet.Save();
            }
        }

        public void SetWorksheetForEditing(string sheetName)
        {
            IEnumerable<Sheet> lSheets =
                _excelFile.WorkbookPart.Workbook.GetFirstChild<Sheets>()
                          .Elements<Sheet>()
                          .Where(s => s.Name == sheetName);
            string lRelationshipId = lSheets.First().Id.Value;
            WorksheetPart lWorksheetPart = (WorksheetPart) _excelFile.WorkbookPart.GetPartById(lRelationshipId);

            _worksheet = lWorksheetPart.Worksheet;
        }

        private Cell GetCell(string columnName, uint rowIndex)
        {
            Row lRow = GetRow(rowIndex);
            Cell lCell =
                lRow.Elements<Cell>()
                    .FirstOrDefault(c => string.Compare(c.CellReference.Value, columnName + rowIndex, true) == 0);

            if (lCell == null)
            {
                lCell = InsertCell(rowIndex, columnName);
            }

            return lCell;
        }

        private Row GetRow(uint rowIndex)
        {
            Row lRow = _worksheet.GetFirstChild<SheetData>().Elements<Row>().FirstOrDefault(r => r.RowIndex == rowIndex);

            if (lRow == null)
            {
                lRow = InsertRow(rowIndex);
            }

            return lRow;
        }

        public Row InsertRow(uint rowIndex)
        {
            SheetData lSheetData = _worksheet.GetFirstChild<SheetData>();

            Row lRow = new Row() {RowIndex = rowIndex};
            lSheetData.Append(lRow);

            //_worksheet.Save();

            return lRow;
        }

        public void InsertCell(string value, Row row)
        {
            Cell lNewCell = new Cell() {DataType = CellValues.String, CellValue = new CellValue(value)};
            row.Append(lNewCell);
        }

        private Cell InsertCell(uint rowIndex, string columnName)
        {
            Row lRow = GetRow(rowIndex);
            string lCellReference = columnName + rowIndex;

            // Cells must be in sequential order according to CellReference. Determine where to insert the new cell.
            //Cell lRefCell = null;
            //foreach (Cell cell in lRow.Elements<Cell>())
            //{
            //    if (string.Compare(cell.CellReference.Value, lCellReference, true) > 0)
            //    {
            //        lRefCell = cell;
            //        break;
            //    }
            //}

            Cell lNewCell = new Cell()
                {
                    CellReference = lCellReference,
                    DataType = CellValues.String,
                    CellValue = new CellValue(string.Empty)
                };
            lRow.Append(lNewCell);
            //lRow.InsertBefore(lNewCell, lRefCell);

            //_worksheet.Save();

            return lNewCell;
        }

        private void FreeResourses()
        {
            _excelFile.Close();
            ReleaseObject(_excelFile);
        }

        private void ReleaseObject(object obj)
        {
            if (obj != null)
            {
                GC.Collect();
                GC.WaitForPendingFinalizers();
            }
        }
    }
}
