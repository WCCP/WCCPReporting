using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Grid;

namespace SoftServe.Reports.ThR.SummaryReport.Utility
{
    public interface IReportControl
    {
        void LoadData(SettingsSnapshot settings);

        GridView View { get; }
        GridColumn[] GetColumnsForExport(int maxColumnCount);
    }
}