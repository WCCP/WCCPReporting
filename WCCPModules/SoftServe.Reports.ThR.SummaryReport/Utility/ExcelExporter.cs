﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using DevExpress.Utils;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DocumentFormat.OpenXml.Spreadsheet;
using SoftServe.Reports.ThR.SummaryReport.Utility.WCG;
using DataTable = System.Data.DataTable;

namespace SoftServe.Reports.ThR.SummaryReport.Utility
{
    public class ExcelExporter
    {
        private ExcelEditor _excelEditor;
        private SettingsSnapshot _settings { get; set; }

        public ExcelExporter(string filePath, SettingsSnapshot settings)
        {
            _settings = settings;

            _excelEditor = new ExcelEditor(filePath);
            //_excelEditor.StartWriting();
            //_excelEditor.SetWorksheetForEditing("Data");
            //_excelEditor.UpdateCell("The best product of the year", 10, "D", CellValues.String);
            //_excelEditor.UpdateCell("80", 3, "B", CellValues.Number);
            //_excelEditor.UpdateCell("80", 2, "C", CellValues.Number);
            //_excelEditor.UpdateCell("20", 3, "C", CellValues.Number);
            //_excelEditor.FinishWriting();
        }

        public void ExportDataTab(GridView view, GridColumn[] columns)
        {
            _excelEditor.StartWriting();
            _excelEditor.SetWorksheetForEditing("Data");

            _excelEditor.UpdateCell(_settings.Filter == null ? string.Empty : _settings.Filter.Filter_text, 6, "C", CellValues.String);
            _excelEditor.UpdateCell(_settings.FilterName, 8, "C", CellValues.String);

            InsertGridView(view, columns, 12, "A");

            _excelEditor.FinishWriting();
        }

        public void ExportSummaryTab(GridView view, GridColumn[] columns, List<string> labels)
        {
            _excelEditor.StartWriting();
            _excelEditor.SetWorksheetForEditing("SummaryReport");

            _excelEditor.UpdateCell(_settings.WaveName ?? string.Empty, 2, "B", CellValues.String);
            _excelEditor.UpdateCell(_settings.ChannelName ?? string.Empty, 3, "B", CellValues.String);
            _excelEditor.UpdateCell(_settings.RegionsDescriptionForDisplay ?? string.Empty, 4, "B", CellValues.String);

            for (int i = 0; i < labels.Count; i++)
            {
                _excelEditor.UpdateCell(labels[i], (uint)(5 + i), "C", CellValues.String);
            }

            ExportGridView(view, columns, 10, "A");

            _excelEditor.FinishWriting();
        }

        public void ExportFilterTab(GridView view, GridColumn[] columns, List<string> labels)
        {
            _excelEditor.StartWriting();
            _excelEditor.SetWorksheetForEditing("SummaryReport");

            _excelEditor.UpdateCell(_settings.Filter == null ? string.Empty : _settings.Filter.Filter_text, 30, "E", CellValues.String);
            _excelEditor.UpdateCell(_settings.FilterName, 32, "E", CellValues.String);

            for (int i = 0; i < labels.Count; i++)
            {
                _excelEditor.UpdateCell(labels[i], (uint)(34 + i), "C", CellValues.String);
            }

            ExportGridView(view, columns, 42, "A");

            _excelEditor.FinishWriting();
        }


        public void ExportGridView(GridView view, GridColumn[] columns, uint rowIndex, string columnName)
        {
            if (columns.Length == 0)
                return;

            PrintHeader(columns, rowIndex, columnName);

            DataTable data = GetFilteredData(view).ToTable();
            UpdateGridBody(data, columns, rowIndex + 1, columnName);
        }

        public void InsertGridView(GridView view, GridColumn[] columns, uint rowIndex, string columnName)
        {
            if (columns.Length == 0)
                return;

            PrintHeader(columns, rowIndex, columnName);

            DataTable data = GetFilteredData(view).ToTable();
            InsertGridBody(data, columns, rowIndex + 1);
        }

        private void UpdateGridBody(DataTable data, GridColumn[] columns, uint rowIndex, string columnName)
        {
            string lColumnName = columnName;

            for (int j = 0; j < columns.Length; j++)
            {
                for (int i = 0; i < data.Rows.Count; i++)
                {
                    var val = data.Rows[i][columns[j].FieldName];
                    string lValueForExport = string.Empty;

                    if (columns[j].DisplayFormat.FormatType == FormatType.Numeric && columns[j].DisplayFormat.Format is PercentFormat)
                    {
                        lValueForExport = (((double)val) * 100).ToString("n2");
                    }
                    else if (val is bool)
                    {
                        lValueForExport = ((bool)val) ? "Да" : "Нет";
                    }
                    else
                    {
                        lValueForExport = val.ToString();
                    }
                   
                    _excelEditor.UpdateCell(lValueForExport, (uint)(rowIndex + i), lColumnName, CellValues.String);
                }

                lColumnName = GetNextColumn(lColumnName);
            }
        }

        private void InsertGridBody(DataTable data, GridColumn[] columns, uint rowIndex)
        {
            for (int i = 0; i < data.Rows.Count; i++)
            {
                Row lRow = _excelEditor.InsertRow((uint)i + rowIndex);

                for (int j = 0; j < columns.Length; j++)
                {
                    var val = data.Rows[i][columns[j].FieldName];
                    string lValueForExport = string.Empty;

                    if (columns[j].DisplayFormat.FormatType == FormatType.Numeric && columns[j].DisplayFormat.Format is PercentFormat)
                    {
                        lValueForExport = (((double)val) * 100).ToString("n2");
                    }
                    else if (val is bool)
                    {
                        lValueForExport = ((bool)val) ? "Да" : "Нет";
                    }
                    else
                    {
                        lValueForExport = val.ToString();
                    }

                    _excelEditor.InsertCell(lValueForExport, lRow);
                }
            }
        }

        private void PrintHeader(GridColumn[] columns, uint rowIndex, string columnName)
        {
            string lColumnName = columnName;

            for (int j = 0; j < columns.Length; j++)
            {
                string lColumnHeader = string.IsNullOrEmpty(columns[j].Caption)
                                          ? columns[j].FieldName
                                          : columns[j].Caption;
                _excelEditor.UpdateCell(lColumnHeader, rowIndex, lColumnName, CellValues.String);
                lColumnName = GetNextColumn(lColumnName);
            }
        }

        private string GetNextColumn(string columnName)
        {
            List<char> lChars = columnName.Reverse().ToList();

            uint lCurValue, lNewValue;
            bool lOverflow = false;

            uint LettersCount = 26;

            for (int i = 0; i < lChars.Count; i++)
            {
                lCurValue = (uint)lChars[i] - (uint)'A' + 1;
                lNewValue = ++lCurValue;
                lOverflow = lNewValue > LettersCount;

                if (!lOverflow)
                {
                    lChars[i] = (char)(lNewValue + (uint)'A' - 1);
                    break;
                }
                else
                {
                    lChars[i] = 'A';
                }
            }

            if (lOverflow) { lChars.Add('A'); }

            lChars.Reverse();

            return new string(lChars.ToArray());
        }

        static DataView GetFilteredData(ColumnView view)
        {
            if (view == null)
                return null;
            if (view.ActiveFilter == null || !view.ActiveFilterEnabled || view.ActiveFilter.Expression == "")
                return view.DataSource as DataView;

            string lDataSetWhereText = CriteriaToWhereClauseHelper.GetDataSetWhere(view.ActiveFilterCriteria);
            DataTable lTable = ((DataView)view.DataSource).Table;
            DataView lFilteredDataView = new DataView(lTable);
            lFilteredDataView.RowFilter = lDataSetWhereText;
            return lFilteredDataView;
        }

    }
}