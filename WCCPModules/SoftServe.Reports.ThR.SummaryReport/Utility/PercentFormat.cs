using System;

namespace SoftServe.Reports.ThR.SummaryReport.Utility
{
    public class PercentFormat : IFormatProvider, ICustomFormatter
    {
        public object GetFormat(Type formatType)
        {
            if (formatType == typeof(ICustomFormatter))
                return this;
            return null;
        }

        public string Format(string format, object arg, IFormatProvider formatProvider)
        {
            if (arg is double)
            {
                return (((double)arg) * 100).ToString(format);
            }
            if (arg is decimal)
            {
                return (((decimal)arg) * 100).ToString(format);
            }
            if (arg is float)
            {
                return (((float)arg) * 100).ToString(format);
            }
            return string.Empty;
        }
    }
}