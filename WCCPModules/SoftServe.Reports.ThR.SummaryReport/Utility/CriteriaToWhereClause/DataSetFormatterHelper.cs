using System;
using System.Globalization;
using DevExpress.Data.Filtering;

namespace SoftServe.Reports.ThR.SummaryReport.Utility.WCG
{
    public static class DataSetFormatterHelper
    {
        // Methods
        public static string FormatBinary(BinaryOperatorType operatorType, string leftOperand, string rightOperand)
        {
            switch (operatorType)
            {
                case BinaryOperatorType.BitwiseAnd:
                case BinaryOperatorType.BitwiseOr:
                case BinaryOperatorType.BitwiseXor:
                    throw new NotSupportedException();
            }
            return BaseFormatterHelper.DefaultFormatBinary(operatorType, leftOperand, rightOperand);
        }

        public static string FormatColumn(string columnName)
        {
            return string.Format(CultureInfo.InvariantCulture, "[{0}]", new object[] { columnName });
        }

        public static string FormatColumn(string columnName, string tableAlias)
        {
            return string.Format(CultureInfo.InvariantCulture, "{1}.[{0}]", new object[] { columnName, tableAlias });
        }

        public static string FormatFunction(FunctionOperatorType operatorType, params string[] operands)
        {
            if (operatorType == FunctionOperatorType.IsNull)
            {
                switch (operands.Length)
                {
                    case 1:
                        return string.Format(CultureInfo.InvariantCulture, "(({0}) is null)", new object[] { operands[0] });

                    case 2:
                        return string.Format(CultureInfo.InvariantCulture, "iif(({0}) is null, {1}, {0})", new object[] { operands[0], operands[1] });
                }
            }
            else
            {
                if (operatorType == FunctionOperatorType.IsNullOrEmpty)
                {
                    return string.Format(CultureInfo.InvariantCulture, "(({0}) is null or (len({0}) = 0))", new object[] { operands[0] });
                }
                if (operatorType == FunctionOperatorType.Iif)
                {
                    return string.Format(CultureInfo.InvariantCulture, "iif({0}, {1}, {2})", new object[] { operands[0], operands[1], operands[2] });
                }
                if (operatorType == FunctionOperatorType.Trim)
                {
                    return string.Format(CultureInfo.InvariantCulture, "trim({0})", new object[] { operands[0] });
                }
                if (operatorType == FunctionOperatorType.Concat)
                {
                    if (operands.Length < 2)
                    {
                        throw new InvalidOperationException();
                    }
                    string str = string.Empty;
                    for (int i = 0; i < operands.Length; i++)
                    {
                        if (i != 0)
                        {
                            str = str + " + ";
                        }
                        str = str + operands[i];
                    }
                    return str;
                }
                if (operatorType == FunctionOperatorType.Substring)
                {
                    if (operands.Length == 3)
                    {
                        return string.Format(CultureInfo.InvariantCulture, "({0}).Substring({1}, {2})", new object[] { operands[0], operands[1], operands[2] });
                    }
                }
            }
            throw new NotSupportedException();
        }

        public static string FormatTable(string schema, string tableName)
        {
            return string.Format(CultureInfo.InvariantCulture, "{0}", new object[] { tableName });
        }

        public static string FormatTable(string schema, string tableName, string tableAlias)
        {
            return string.Format(CultureInfo.InvariantCulture, "{0} {1}", new object[] { tableName, tableAlias });
        }
    }
}