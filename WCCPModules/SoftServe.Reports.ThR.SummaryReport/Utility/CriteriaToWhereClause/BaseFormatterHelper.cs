using System;
using System.Globalization;
using DevExpress.Data.Filtering;

namespace SoftServe.Reports.ThR.SummaryReport.Utility.WCG
{
    public static class BaseFormatterHelper
    {
        // Methods
        public static string DefaultFormatBinary(BinaryOperatorType operatorType, string leftOperand, string rightOperand)
        {
            string str;
            switch (operatorType)
            {
                case BinaryOperatorType.Equal:
                    str = "=";
                    break;

                case BinaryOperatorType.NotEqual:
                    str = "<>";
                    break;

                case BinaryOperatorType.Greater:
                    str = ">";
                    break;

                case BinaryOperatorType.Less:
                    str = "<";
                    break;

                case BinaryOperatorType.LessOrEqual:
                    str = "<=";
                    break;

                case BinaryOperatorType.GreaterOrEqual:
                    str = ">=";
                    break;

                case BinaryOperatorType.Like:
                    str = "like";
                    break;

                case BinaryOperatorType.BitwiseAnd:
                    str = "&";
                    break;

                case BinaryOperatorType.BitwiseOr:
                    str = "|";
                    break;

                case BinaryOperatorType.BitwiseXor:
                    str = "^";
                    break;

                case BinaryOperatorType.Divide:
                    str = "/";
                    break;

                case BinaryOperatorType.Modulo:
                    str = "%";
                    break;

                case BinaryOperatorType.Multiply:
                    str = "*";
                    break;

                case BinaryOperatorType.Plus:
                    str = "+";
                    break;

                case BinaryOperatorType.Minus:
                    str = "-";
                    break;

                default:
                    throw new NotImplementedException("DefaultFormatBunary('" + operatorType.ToString() + "', '" + leftOperand + "', '" + rightOperand + "')");
            }
            return ("(" + leftOperand + " " + str + " " + rightOperand + ")");
        }

        public static string DefaultFormatFunction(FunctionOperatorType operatorType, params string[] operands)
        {
            string str;
            switch (operatorType)
            {
                case FunctionOperatorType.Iif:
                    return string.Format(CultureInfo.InvariantCulture, "case when {0} then {1} else {2} end", new object[] { operands[0], operands[1], operands[2] });

                case FunctionOperatorType.IsNull:
                case FunctionOperatorType.Len:
                case FunctionOperatorType.Upper:
                case FunctionOperatorType.Lower:
                    str = string.Empty;
                    foreach (string str4 in operands)
                    {
                        if (str.Length > 0)
                        {
                            str = str + ", ";
                        }
                        str = str + str4;
                    }
                    return string.Concat(new object[] { operatorType.ToString(), '(', str, ')' });

                case FunctionOperatorType.IsNullOrEmpty:
                    return string.Format(CultureInfo.InvariantCulture, "(({0}) is null or (Len({0}) = 0))", new object[] { operands[0] });

                case FunctionOperatorType.Trim:
                    return ("ltrim(rtrim(" + operands[0] + "))");

                case FunctionOperatorType.Substring:
                    {
                        string str3 = (operands.Length < 3) ? ("Len(" + operands[0] + ") - " + operands[1]) : operands[2];
                        return ("Substring(" + operands[0] + ", (" + operands[1] + ") + 1, " + str3 + ")");
                    }
                case FunctionOperatorType.Concat:
                    str = string.Empty;
                    foreach (string str2 in operands)
                    {
                        if (str.Length > 0)
                        {
                            str = str + " || ";
                        }
                        str = str + str2;
                    }
                    return str;

                case FunctionOperatorType.Ascii:
                    return string.Format(CultureInfo.InvariantCulture, "Ascii({0})", new object[] { operands[0] });

                case FunctionOperatorType.Char:
                    return string.Format(CultureInfo.InvariantCulture, "Char({0})", new object[] { operands[0] });

                case FunctionOperatorType.ToStr:
                    return string.Format(CultureInfo.InvariantCulture, "Str({0})", new object[] { operands[0] });

                case FunctionOperatorType.Replace:
                    return string.Format(CultureInfo.InvariantCulture, "Replace({0}, {1}, {2})", new object[] { operands[0], operands[1], operands[2] });

                case FunctionOperatorType.Reverse:
                    return string.Format(CultureInfo.InvariantCulture, "Reverse({0})", new object[] { operands[0] });

                case FunctionOperatorType.Insert:
                    return string.Format(CultureInfo.InvariantCulture, "Stuff({0}, {1} + 1, 0, {2})", new object[] { operands[0], operands[1], operands[2] });

                case FunctionOperatorType.CharIndex:
                    return string.Format(CultureInfo.InvariantCulture, "(Charindex({0}, {1}) - 1)", new object[] { operands[0], operands[1] });

                case FunctionOperatorType.Remove:
                    return string.Format(CultureInfo.InvariantCulture, "Stuff({0}, {1} + 1, {2}, '')", new object[] { operands[0], operands[1], operands[2] });

                case FunctionOperatorType.Abs:
                    return string.Format(CultureInfo.InvariantCulture, "Abs({0})", new object[] { operands[0] });

                case FunctionOperatorType.Sqr:
                    return string.Format(CultureInfo.InvariantCulture, "Sqr({0})", new object[] { operands[0] });

                case FunctionOperatorType.Cos:
                    return string.Format(CultureInfo.InvariantCulture, "Cos({0})", new object[] { operands[0] });

                case FunctionOperatorType.Sin:
                    return string.Format(CultureInfo.InvariantCulture, "Sin({0})", new object[] { operands[0] });

                case FunctionOperatorType.Atn:
                    return string.Format(CultureInfo.InvariantCulture, "Atn({0})", new object[] { operands[0] });

                case FunctionOperatorType.Exp:
                    return string.Format(CultureInfo.InvariantCulture, "Exp({0})", new object[] { operands[0] });

                case FunctionOperatorType.Log:
                    return string.Format(CultureInfo.InvariantCulture, "Log({0})", new object[] { operands[0] });

                case FunctionOperatorType.Rnd:
                    return "Rnd()";

                case FunctionOperatorType.Tan:
                    return string.Format(CultureInfo.InvariantCulture, "Tan({0})", new object[] { operands[0] });

                case FunctionOperatorType.Power:
                    return string.Format(CultureInfo.InvariantCulture, "Power({0}, {1})", new object[] { operands[0], operands[1] });

                case FunctionOperatorType.Sign:
                    return string.Format(CultureInfo.InvariantCulture, "Sign({0})", new object[] { operands[0] });

                case FunctionOperatorType.Round:
                    return string.Format(CultureInfo.InvariantCulture, "Round({0})", new object[] { operands[0] });

                case FunctionOperatorType.Ceiling:
                    return string.Format(CultureInfo.InvariantCulture, "Ceiling({0})", new object[] { operands[0] });

                case FunctionOperatorType.Floor:
                    return string.Format(CultureInfo.InvariantCulture, "Floor({0})", new object[] { operands[0] });

                case FunctionOperatorType.GetDate:
                    return string.Format(CultureInfo.InvariantCulture, "CONVERT(DATE, {0})", new object[] { operands[0] });

                case FunctionOperatorType.GetMilliSecond:
                    return string.Format(CultureInfo.InvariantCulture, "DATEPART(Millisecond, {0})", new object[] { operands[0] });

                case FunctionOperatorType.GetSecond:
                    return string.Format(CultureInfo.InvariantCulture, "DATEPART(Second, {0})", new object[] { operands[0] });

                case FunctionOperatorType.GetMinute:
                    return string.Format(CultureInfo.InvariantCulture, "DATEPART(Minute, {0})", new object[] { operands[0] });

                case FunctionOperatorType.GetHour:
                    return string.Format(CultureInfo.InvariantCulture, "DATEPART(Hour, {0})", new object[] { operands[0] });

                case FunctionOperatorType.GetDay:
                    return string.Format(CultureInfo.InvariantCulture, "DATEPART(Day, {0})", new object[] { operands[0] });

                case FunctionOperatorType.GetMonth:
                    return string.Format(CultureInfo.InvariantCulture, "DATEPART(Month, {0})", new object[] { operands[0] });

                case FunctionOperatorType.GetYear:
                    return string.Format(CultureInfo.InvariantCulture, "DATEPART(Year, {0})", new object[] { operands[0] });

                case FunctionOperatorType.GetDayOfWeek:
                    return string.Format(CultureInfo.InvariantCulture, "CONVERT(Int,(DATEPART(dw, {0}) + (@@DATEFIRST) + 6) % 7)", new object[] { operands[0] });

                case FunctionOperatorType.GetDayOfYear:
                    return string.Format(CultureInfo.InvariantCulture, "DATEPART(DayOfYear, {0})", new object[] { operands[0] });

                case FunctionOperatorType.GetTimeOfDay:
                    return string.Format(CultureInfo.InvariantCulture, "(CONVERT(BigInt,((CONVERT(BigInt,DATEPART(HOUR, {0}))) * 36000000000) + ((CONVERT(BigInt,DATEPART(MINUTE, {0}))) * 600000000) + ((CONVERT(BigInt,DATEPART(SECOND, {0}))) * 10000000) + ((CONVERT(BigInt,DATEPART(MILLISECOND, {0}))) * 10000)))", new object[] { operands[0] });

                case FunctionOperatorType.Now:
                    return "NOW()";

                case FunctionOperatorType.UtcNow:
                    return "UTCNOW()";

                case FunctionOperatorType.Today:
                    return "DATE()";

                case FunctionOperatorType.AddDays:
                    return string.Format(CultureInfo.InvariantCulture, "AddDays({0})", new object[] { operands[0], operands[1] });
            }
            throw new NotImplementedException("DefaultFormatFunction for " + operatorType.ToString());
        }

        public static string DefaultFormatUnary(UnaryOperatorType operatorType, string operand)
        {
            string str;
            switch (operatorType)
            {
                case UnaryOperatorType.BitwiseNot:
                    str = "~({0})";
                    break;

                case UnaryOperatorType.Plus:
                    str = "+{0}";
                    break;

                case UnaryOperatorType.Minus:
                    str = "-{0}";
                    break;

                case UnaryOperatorType.Not:
                    str = "not ({0})";
                    break;

                case UnaryOperatorType.IsNull:
                    str = "{0} is null";
                    break;

                default:
                    throw new NotImplementedException("DefaultFormatUnary('" + operatorType.ToString() + "', '" + operand + "')");
            }
            return string.Format(CultureInfo.InvariantCulture, str, new object[] { operand });
        }
    }
}