using System;
using System.Collections.Specialized;
using System.Globalization;
using System.Text;
using System.Text.RegularExpressions;
using DevExpress.Data.Filtering;
using DevExpress.Xpo.DB.Helpers;

namespace SoftServe.Reports.ThR.SummaryReport.Utility.WCG
{
    public static class CriteriaToWhereClauseHelper
    {
        // Methods
        public static string GetDataSetWhere(CriteriaOperator op)
        {
            return new DataSetWhereGenerator().Process(op);
        }

        public static string GetDynamicLinqWhere(CriteriaOperator op)
        {
            return new DynamicLinqWhereGenerator().Process(op);
        }

        public static string GetMsSqlWhere(string criteriaExpression) {
            string lSqlWhereText = criteriaExpression;
            // replace decimal separator "," with "."
            lSqlWhereText = Regex.Replace(lSqlWhereText, @"(\d),(\d)", @"$1.$2");
            // convert DevEx "Between (<expr1>, <expr2>)" to SQL syntax "between <expr1> and <expr2>"
            lSqlWhereText = Regex.Replace(lSqlWhereText, @"BETWEEN\s*\(\s*(\d+(?:\.\d+)?)\s*,\s*(\d+(?:\.\d+)?)\s*\)", @"BETWEEN $1 AND $2", RegexOptions.IgnoreCase);
            // convert DevEx "?" to SQL "NULL"
            lSqlWhereText = Regex.Replace(lSqlWhereText, @"\s*=\s*\?", @" = NULL", RegexOptions.IgnoreCase);
            return lSqlWhereText;
        }

        public static string GetFilterExpressionBySqlWhere(string sqlWhereText) {
            string lFilterText = sqlWhereText;
            // replace decimal separator "," with "."
            lFilterText = Regex.Replace(lFilterText, @"(\d),(\d)", @"$1.$2");
            // convert SQL "between <expr1> and <expr2>" to DevEx syntax "Between (<expr1>, <expr2>)"
            lFilterText = Regex.Replace(lFilterText, @"BETWEEN\s+?(\d+(?:\.\d+)?)\s+?AND\s+?(\d+(?:\.\d+)?)", @"BETWEEN ($1, $2)", RegexOptions.IgnoreCase);
            // convert SQL "NULL" to DevEx "?"
            lFilterText = Regex.Replace(lFilterText, @"\s*=\s*NULL", @" = ?", RegexOptions.IgnoreCase);
            return lFilterText;
        }

        // Nested Types
        private abstract class BaseWhereGenerator : IClientCriteriaVisitor
        {
            // Methods
            protected BaseWhereGenerator()
            {
            }

            object IClientCriteriaVisitor.Visit(AggregateOperand theOperand)
            {
                throw new NotSupportedException("AggregateOperand");
            }

            object IClientCriteriaVisitor.Visit(OperandProperty theOperand)
            {
                return this.FormatOperandProperty(theOperand);
            }

            object ICriteriaVisitor.Visit(BetweenOperator theOperator)
            {
                return this.Process(CriteriaOperator.And(new BinaryOperator(theOperator.BeginExpression, theOperator.TestExpression, BinaryOperatorType.LessOrEqual), new BinaryOperator(theOperator.TestExpression, theOperator.EndExpression, BinaryOperatorType.LessOrEqual)));
            }

            object ICriteriaVisitor.Visit(BinaryOperator theOperator)
            {
                throw new NotImplementedException();
            }

            object ICriteriaVisitor.Visit(FunctionOperator theOperator)
            {
                throw new NotImplementedException();
            }

            object ICriteriaVisitor.Visit(GroupOperator theOperator)
            {
                StringCollection collection = new StringCollection();
                foreach (CriteriaOperator @operator in theOperator.Operands)
                {
                    string str = this.Process(@operator);
                    if (str != null)
                    {
                        collection.Add(str);
                    }
                }
                return ("(" + StringListHelper.DelimitedText(collection, " " + theOperator.OperatorType.ToString() + " ") + ")");
            }

            object ICriteriaVisitor.Visit(InOperator theOperator)
            {
                string str = this.Process(theOperator.LeftOperand);
                StringBuilder builder = new StringBuilder();
                foreach (CriteriaOperator @operator in theOperator.Operands)
                {
                    if (builder.Length > 0)
                    {
                        builder.Append(',');
                    }
                    builder.Append(this.Process(@operator));
                }
                return string.Format(CultureInfo.InvariantCulture, "{0} in ({1})", new object[] { str, builder.ToString() });
            }

            object ICriteriaVisitor.Visit(OperandValue theOperand)
            {
                return theOperand.ToString();
            }

            object ICriteriaVisitor.Visit(UnaryOperator theOperator)
            {
                return BaseFormatterHelper.DefaultFormatUnary(theOperator.OperatorType, this.Process(theOperator.Operand));
            }

            protected abstract string FormatOperandProperty(OperandProperty operand);
            public string Process(CriteriaOperator operand)
            {
                if (object.ReferenceEquals(operand, null))
                {
                    return string.Empty;
                }
                return (string)operand.Accept(this);
            }

            public object Visit(JoinOperand theOperand)
            {
                throw new NotImplementedException();
            }
        }

        private class DataSetWhereGenerator : BaseWhereGenerator, ICriteriaVisitor
        {
            // Fields
            private const string nullString = "null";

            // Methods
            private static string AsString(object value)
            {
                return ("'" + value.ToString().Replace("'", "''") + "'");
            }

            object ICriteriaVisitor.Visit(BinaryOperator theOperator)
            {
                string leftOperand = base.Process(theOperator.LeftOperand);
                string rightOperand = base.Process(theOperator.RightOperand);
                return DataSetFormatterHelper.FormatBinary(theOperator.OperatorType, leftOperand, rightOperand);
            }

            object ICriteriaVisitor.Visit(FunctionOperator theOperator)
            {
                string[] operands = new string[theOperator.Operands.Count];
                for (int i = 0; i < theOperator.Operands.Count; i++)
                {
                    operands[i] = base.Process(theOperator.Operands[i]);
                }
                return DataSetFormatterHelper.FormatFunction(theOperator.OperatorType, operands);
            }

            object ICriteriaVisitor.Visit(OperandValue theOperand)
            {
                DateTime time;
                string str;
                object obj2 = theOperand.Value;
                if (obj2 == null)
                {
                    return "null";
                }
                switch (Type.GetTypeCode(obj2.GetType()))
                {
                    case TypeCode.Empty:
                    case TypeCode.DBNull:
                        return "null";

                    case TypeCode.Boolean:
                        if ((bool)obj2)
                        {
                            return "true";
                        }
                        return "false";

                    case TypeCode.Char:
                        return ("'" + ((char)obj2) + "'");

                    case TypeCode.SByte:
                    case TypeCode.Byte:
                    case TypeCode.Int16:
                    case TypeCode.UInt16:
                    case TypeCode.Int32:
                    case TypeCode.UInt32:
                    case TypeCode.Int64:
                        if (obj2 is Enum)
                        {
                            return Convert.ToInt64(obj2).ToString();
                        }
                        return obj2.ToString();

                    case TypeCode.UInt64:
                        if (obj2 is Enum)
                        {
                            return Convert.ToUInt64(obj2).ToString();
                        }
                        return obj2.ToString();

                    case TypeCode.Single:
                        {
                            float num3 = (float)obj2;
                            return FixNonFixedText(num3.ToString("r", CultureInfo.InvariantCulture));
                        }
                    case TypeCode.Double:
                        {
                            double num2 = (double)obj2;
                            return FixNonFixedText(num2.ToString("r", CultureInfo.InvariantCulture));
                        }
                    case TypeCode.Decimal:
                        {
                            decimal num = (decimal)obj2;
                            return FixNonFixedText(num.ToString(CultureInfo.InvariantCulture));
                        }
                    case TypeCode.DateTime:
                        time = (DateTime)obj2;
                        if (!(time.TimeOfDay == TimeSpan.Zero))
                        {
                            str = "MM/dd/yyyy HH:mm:ss";
                            break;
                        }
                        str = "MM/dd/yyyy";
                        break;

                    case TypeCode.String:
                        return AsString(obj2);

                    default:
                        {
                            if (obj2 is Guid)
                            {
                                Guid guid = (Guid)obj2;
                                return ("CONVERT('{" + guid.ToString() + "}','System.Guid')");
                            }
                            if (!(obj2 is TimeSpan))
                            {
                                throw new ArgumentException(obj2.ToString());
                            }
                            TimeSpan span = (TimeSpan)obj2;
                            return FixNonFixedText(span.TotalSeconds.ToString("r", CultureInfo.InvariantCulture));
                        }
                }
                return ("#" + time.ToString(str, CultureInfo.InvariantCulture) + "#");
            }

            private static string FixNonFixedText(string toFix)
            {
                if (toFix.IndexOfAny(new char[] { '.', 'e', 'E' }) < 0)
                {
                    toFix = toFix + ".0";
                }
                return toFix;
            }

            protected override string FormatOperandProperty(OperandProperty operand)
            {
                return string.Format("[{0}]", operand.PropertyName);
            }
        }

        private class DynamicLinqWhereGenerator : BaseWhereGenerator, ICriteriaVisitor
        {
            // Methods
            object ICriteriaVisitor.Visit(BinaryOperator theOperator)
            {
                string leftOperand = base.Process(theOperator.LeftOperand);
                string rightOperand = base.Process(theOperator.RightOperand);
                return DynamicLinqFormatterHelper.FormatBinary(theOperator.OperatorType, leftOperand, rightOperand);
            }

            object ICriteriaVisitor.Visit(FunctionOperator theOperator)
            {
                string[] operands = new string[theOperator.Operands.Count];
                for (int i = 0; i < theOperator.Operands.Count; i++)
                {
                    operands[i] = base.Process(theOperator.Operands[i]);
                }
                return DynamicLinqFormatterHelper.FormatFunction(theOperator.OperatorType, operands);
            }

            object ICriteriaVisitor.Visit(OperandValue theOperand)
            {
                return string.Format("\"{0}\"", theOperand.Value);
            }

            object ICriteriaVisitor.Visit(UnaryOperator theOperator)
            {
                return DynamicLinqFormatterHelper.FormatUnary(theOperator.OperatorType, base.Process(theOperator.Operand));
            }

            protected override string FormatOperandProperty(OperandProperty operand)
            {
                return operand.PropertyName;
            }
        }
    }
}