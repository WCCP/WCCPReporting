﻿using System;
using System.Globalization;
using DevExpress.Data.Filtering;
using DevExpress.Xpo.DB;

namespace SoftServe.Reports.ThR.SummaryReport.Utility.WCG
{
    public static class DynamicLinqFormatterHelper
    {
        // Methods
        public static string FormatBinary(BinaryOperatorType operatorType, string leftOperand, string rightOperand)
        {
            switch (operatorType)
            {
                case BinaryOperatorType.Like:
                case BinaryOperatorType.BitwiseAnd:
                case BinaryOperatorType.BitwiseOr:
                case BinaryOperatorType.BitwiseXor:
                    throw new NotSupportedException();
            }
            return BaseFormatterHelper.DefaultFormatBinary(operatorType, leftOperand, rightOperand);
        }

        public static string FormatColumn(string columnName)
        {
            return columnName;
        }

        public static string FormatColumn(string columnName, string tableAlias)
        {
            return string.Format(CultureInfo.InvariantCulture, "{1}.{0}", new object[] { columnName, tableAlias });
        }

        public static string FormatFunction(FunctionOperatorType operatorType, params string[] operands)
        {
            switch (operatorType)
            {
                case FunctionOperatorType.Iif:
                    return string.Format(CultureInfo.InvariantCulture, "iif({0}, {1}, {2})", new object[] { operands[0], operands[1], operands[2] });

                case FunctionOperatorType.IsNull:
                    switch (operands.Length)
                    {
                        case 1:
                            return string.Format(CultureInfo.InvariantCulture, "(({0}) == null)", new object[] { operands[0] });

                        case 2:
                            return string.Format(CultureInfo.InvariantCulture, "iif(({0}) == null, {1}, {0})", new object[] { operands[0], operands[1] });
                    }
                    break;

                case FunctionOperatorType.IsNullOrEmpty:
                    return string.Format(CultureInfo.InvariantCulture, "(({0}) == null or ({0}).Length = 0)", new object[] { operands[0] });

                case FunctionOperatorType.Trim:
                    return string.Format(CultureInfo.InvariantCulture, "({0}).Trim()", new object[] { operands[0] });

                case FunctionOperatorType.Substring:
                    switch (operands.Length)
                    {
                        case 2:
                            return string.Format(CultureInfo.InvariantCulture, "({0}).Substring({1})", new object[] { operands[0], operands[1] });

                        case 3:
                            return string.Format(CultureInfo.InvariantCulture, "({0}).Substring({1}, {2})", new object[] { operands[0], operands[1], operands[2] });
                    }
                    throw new NotSupportedException();

                case FunctionOperatorType.Upper:
                    return string.Format(CultureInfo.InvariantCulture, "({0}).ToUpper()", new object[] { operands[0] });

                case FunctionOperatorType.Lower:
                    return string.Format(CultureInfo.InvariantCulture, "({0}).ToLower()", new object[] { operands[0] });

                case FunctionOperatorType.Concat:
                    {
                        if (operands.Length < 2)
                        {
                            throw new InvalidOperationException();
                        }
                        string str = string.Empty;
                        for (int i = 0; i < operands.Length; i++)
                        {
                            if (i != 0)
                            {
                                str = str + " + ";
                            }
                            str = str + operands[i];
                        }
                        return str;
                    }
                case FunctionOperatorType.Ascii:
                case FunctionOperatorType.Char:
                case FunctionOperatorType.Reverse:
                case FunctionOperatorType.Rnd:
                case FunctionOperatorType.Now:
                case FunctionOperatorType.Today:
                    throw new NotSupportedException();

                case FunctionOperatorType.ToStr:
                    return string.Format(CultureInfo.InvariantCulture, "({0}).ToString()", new object[] { operands[0] });

                case FunctionOperatorType.Replace:
                    return string.Format(CultureInfo.InvariantCulture, "({0}).Replace({0}, {1})", new object[] { operands[0], operands[1] });

                case FunctionOperatorType.Insert:
                    return string.Format(CultureInfo.InvariantCulture, "({0}).Insert({1}, {2})", new object[] { operands[0], operands[1], operands[2] });

                case FunctionOperatorType.CharIndex:
                    switch (operands.Length)
                    {
                        case 2:
                            return string.Format(CultureInfo.InvariantCulture, "({0}).IndexOf({1})", new object[] { operands[0], operands[1] });

                        case 3:
                            return string.Format(CultureInfo.InvariantCulture, "({0}).IndexOf({1}, {2})", new object[] { operands[0], operands[1], operands[2] });

                        case 4:
                            return string.Format(CultureInfo.InvariantCulture, "({0}).IndexOf({1}, {2}, {3})", new object[] { operands[0], operands[1], operands[2], operands[3] });
                    }
                    throw new NotSupportedException();

                case FunctionOperatorType.Remove:
                    switch (operands.Length)
                    {
                        case 2:
                            return string.Format(CultureInfo.InvariantCulture, "({0}).Remove({1})", new object[] { operands[0], operands[1] });

                        case 3:
                            return string.Format(CultureInfo.InvariantCulture, "({0}).Remove({1}, {2})", new object[] { operands[0], operands[1], operands[2] });
                    }
                    throw new NotSupportedException();

                case FunctionOperatorType.Abs:
                    return string.Format(CultureInfo.InvariantCulture, "Math.Abs({0})", new object[] { operands[0] });

                case FunctionOperatorType.Sqr:
                    return string.Format(CultureInfo.InvariantCulture, "Math.Sqrt({0})", new object[] { operands[0] });

                case FunctionOperatorType.Cos:
                    return string.Format(CultureInfo.InvariantCulture, "Math.Cos({0})", new object[] { operands[0] });

                case FunctionOperatorType.Sin:
                    return string.Format(CultureInfo.InvariantCulture, "Math.Sin({0})", new object[] { operands[0] });

                case FunctionOperatorType.Atn:
                    return string.Format(CultureInfo.InvariantCulture, "Math.Atan({0})", new object[] { operands[0] });

                case FunctionOperatorType.Exp:
                    return string.Format(CultureInfo.InvariantCulture, "Math.Exp({0})", new object[] { operands[0] });

                case FunctionOperatorType.Log:
                    switch (operands.Length)
                    {
                        case 1:
                            return string.Format(CultureInfo.InvariantCulture, "Math.Log({0})", new object[] { operands[0] });

                        case 2:
                            return string.Format(CultureInfo.InvariantCulture, "(Math.Log({0}, {1}))", new object[] { operands[0], operands[1] });
                    }
                    break;

                case FunctionOperatorType.Tan:
                    return string.Format(CultureInfo.InvariantCulture, "Math.Tan({0})", new object[] { operands[0] });

                case FunctionOperatorType.Power:
                    return string.Format(CultureInfo.InvariantCulture, "Math.Pow({0}, {1})", new object[] { operands[0], operands[1] });

                case FunctionOperatorType.Sign:
                    return string.Format(CultureInfo.InvariantCulture, "Math.Sign({0})", new object[] { operands[0] });

                case FunctionOperatorType.Round:
                    switch (operands.Length)
                    {
                        case 1:
                            return string.Format(CultureInfo.InvariantCulture, "Math.Round({0}, 0)", new object[] { operands[0] });

                        case 2:
                            return string.Format(CultureInfo.InvariantCulture, "Math.Round({0}, {1})", new object[] { operands[0], operands[1] });
                    }
                    break;

                case FunctionOperatorType.Ceiling:
                    return string.Format(CultureInfo.InvariantCulture, "Math.Ceiling({0})", new object[] { operands[0] });

                case FunctionOperatorType.Floor:
                    return string.Format(CultureInfo.InvariantCulture, "Math.Floor({0})", new object[] { operands[0] });

                //case FunctionOperatorType.Max:
                //    return string.Format(CultureInfo.InvariantCulture, "Math.Max({0}, {1})", new object[] { operands[0], operands[1] });

                //case FunctionOperatorType.Min:
                //    return string.Format(CultureInfo.InvariantCulture, "Math.Min({0}, {1})", new object[] { operands[0], operands[1] });

                case FunctionOperatorType.Acos:
                    return string.Format(CultureInfo.InvariantCulture, "Math.Acos({0})", new object[] { operands[0] });

                case FunctionOperatorType.Asin:
                    return string.Format(CultureInfo.InvariantCulture, "Math.Asin({0})", new object[] { operands[0] });

                case FunctionOperatorType.Atn2:
                    return string.Format(CultureInfo.InvariantCulture, "Math.Atan2({0}, {1})", new object[] { operands[0], operands[1] });

                case FunctionOperatorType.Cosh:
                    return string.Format(CultureInfo.InvariantCulture, "Math.Cosh({0})", new object[] { operands[0] });

                case FunctionOperatorType.Log10:
                    return string.Format(CultureInfo.InvariantCulture, "(Math.Log10({0}))", new object[] { operands[0] });

                case FunctionOperatorType.Sinh:
                    return string.Format(CultureInfo.InvariantCulture, "Math.Sinh({0})", new object[] { operands[0] });

                case FunctionOperatorType.Tanh:
                    return string.Format(CultureInfo.InvariantCulture, "Math.Tanh({0})", new object[] { operands[0] });

                case FunctionOperatorType.PadLeft:
                    if (operands.Length != 2)
                    {
                        throw new NotSupportedException();
                    }
                    return string.Format(CultureInfo.InvariantCulture, "({0}).PadLeft({1})", new object[] { operands[0], operands[1] });

                case FunctionOperatorType.PadRight:
                    if (operands.Length != 2)
                    {
                        throw new NotSupportedException();
                    }
                    return string.Format(CultureInfo.InvariantCulture, "({0}).PadRight({1})", new object[] { operands[0], operands[1] });

                //case FunctionOperatorType.DateDiffTick:
                //    return string.Format(CultureInfo.InvariantCulture, "(Math.Ceiling((({0}).Value - ({1}).Value).TotalSeconds) * 10000000)", new object[] { operands[0], operands[1] });

                //case FunctionOperatorType.DateDiffSecond:
                //    return string.Format(CultureInfo.InvariantCulture, "Math.Ceiling((({0}).Value - ({1}).Value).TotalSeconds)", new object[] { operands[0], operands[1] });

                //case FunctionOperatorType.DateDiffMilliSecond:
                //    return string.Format(CultureInfo.InvariantCulture, "Math.Ceiling((({0}).Value - ({1}).Value).TotalMilliseconds)", new object[] { operands[0], operands[1] });

                //case FunctionOperatorType.DateDiffMinute:
                //    return string.Format(CultureInfo.InvariantCulture, "Math.Ceiling((({0}).Value - ({1}).Value).TotalMinutes)", new object[] { operands[0], operands[1] });

                //case FunctionOperatorType.DateDiffHour:
                //    return string.Format(CultureInfo.InvariantCulture, "Math.Ceiling((({0}).Value - ({1}).Value).TotalHours)", new object[] { operands[0], operands[1] });

                //case FunctionOperatorType.DateDiffDay:
                //    return string.Format(CultureInfo.InvariantCulture, "Math.Ceiling((({0}).Value - ({1}).Value).TotalDays)", new object[] { operands[0], operands[1] });

                //case FunctionOperatorType.DateDiffMonth:
                //    return string.Format(CultureInfo.InvariantCulture, "Math.Ceiling((({0}).Value - ({1}).Value).TotalMonths)", new object[] { operands[0], operands[1] });

                //case FunctionOperatorType.DateDiffYear:
                //    return string.Format(CultureInfo.InvariantCulture, "Math.Ceiling((({0}).Value - ({1}).Value).TotalYears)", new object[] { operands[0], operands[1] });

                case FunctionOperatorType.GetDate:
                    return string.Format(CultureInfo.InvariantCulture, "({0}).Value.Date", new object[] { operands[0] });

                case FunctionOperatorType.GetMilliSecond:
                    return string.Format(CultureInfo.InvariantCulture, "({0}).Value.Millisecond", new object[] { operands[0] });

                case FunctionOperatorType.GetSecond:
                    return string.Format(CultureInfo.InvariantCulture, "({0}).Value.Second", new object[] { operands[0] });

                case FunctionOperatorType.GetMinute:
                    return string.Format(CultureInfo.InvariantCulture, "({0}).Value.Minute", new object[] { operands[0] });

                case FunctionOperatorType.GetHour:
                    return string.Format(CultureInfo.InvariantCulture, "({0}).Value.Hour", new object[] { operands[0] });

                case FunctionOperatorType.GetDay:
                    return string.Format(CultureInfo.InvariantCulture, "({0}).Value.Day", new object[] { operands[0] });

                case FunctionOperatorType.GetMonth:
                    return string.Format(CultureInfo.InvariantCulture, "({0}).Value.Month", new object[] { operands[0] });

                case FunctionOperatorType.GetYear:
                    return string.Format(CultureInfo.InvariantCulture, "({0}).Value.Year", new object[] { operands[0] });

                case FunctionOperatorType.GetDayOfWeek:
                    return string.Format(CultureInfo.InvariantCulture, "({0}).Value.DayOfWeek", new object[] { operands[0] });

                case FunctionOperatorType.GetDayOfYear:
                    return string.Format(CultureInfo.InvariantCulture, "({0}).Value.DayOfYear", new object[] { operands[0] });

                case FunctionOperatorType.GetTimeOfDay:
                    return string.Format(CultureInfo.InvariantCulture, "({0}).Value.TimeOfDay", new object[] { operands[0] });

                case FunctionOperatorType.AddTimeSpan:
                case FunctionOperatorType.AddSeconds:
                    return string.Format(CultureInfo.InvariantCulture, "({0}).Value.AddSeconds({1})", new object[] { operands[0], operands[1] });

                case FunctionOperatorType.AddTicks:
                    return string.Format(CultureInfo.InvariantCulture, "({0}).Value.AddTicks({1})", new object[] { operands[0], operands[1] });

                case FunctionOperatorType.AddMilliSeconds:
                    return string.Format(CultureInfo.InvariantCulture, "({0}).Value.AddMilliSeconds({1})", new object[] { operands[0], operands[1] });

                case FunctionOperatorType.AddMinutes:
                    return string.Format(CultureInfo.InvariantCulture, "({0}).Value.AddMinutes({1})", new object[] { operands[0], operands[1] });

                case FunctionOperatorType.AddHours:
                    return string.Format(CultureInfo.InvariantCulture, "({0}).Value.AddHours({1})", new object[] { operands[0], operands[1] });

                case FunctionOperatorType.AddDays:
                    return string.Format(CultureInfo.InvariantCulture, "({0}).Value.AddDays({1})", new object[] { operands[0], operands[1] });

                case FunctionOperatorType.AddMonths:
                    return string.Format(CultureInfo.InvariantCulture, "({0}).Value.AddMonths({1})", new object[] { operands[0], operands[1] });

                case FunctionOperatorType.AddYears:
                    return string.Format(CultureInfo.InvariantCulture, "({0}).Value.AddYears({1})", new object[] { operands[0], operands[1] });
            }
            return null;
        }

        public static string FormatTable(string schema, string tableName)
        {
            return string.Format(CultureInfo.InvariantCulture, "{0}", new object[] { tableName });
        }

        public static string FormatTable(string schema, string tableName, string tableAlias)
        {
            return string.Format(CultureInfo.InvariantCulture, "{0} {1}", new object[] { tableName, tableAlias });
        }

        public static string FormatUnary(UnaryOperatorType operatorType, string operand)
        {
            if (operatorType == UnaryOperatorType.IsNull)
            {
                return string.Format(CultureInfo.InvariantCulture, "({0} == null)", new object[] { operand });
            }
            return BaseFormatterHelper.DefaultFormatUnary(operatorType, operand);
        }
    }

}