using System;

namespace SoftServe.Reports.ThR.SummaryReport.Utility
{
    public class GridViewCommandEventArgs : EventArgs
    {
        public GridViewCommandEventArgs(string command)
        {
            Command = command;
        }

        public string Command { get; private set; }
    }
}