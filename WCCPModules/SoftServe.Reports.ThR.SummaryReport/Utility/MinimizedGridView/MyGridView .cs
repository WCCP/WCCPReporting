﻿namespace SoftServe.Reports.ThR.SummaryReport.Utility.MinimizedGridView
{
    public class MinimizedGridView : DevExpress.XtraGrid.Views.Grid.GridView
    {
        public MinimizedGridView() : this(null) { }
        public MinimizedGridView(DevExpress.XtraGrid.GridControl grid)
            : base(grid)
        {
            // put your initialization code here
        }
        protected override string ViewName { get { return "MyGridView"; } }
    }
}

