﻿using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Base.Handler;
using DevExpress.XtraGrid.Views.Base.ViewInfo;
using DevExpress.XtraGrid.Registrator;

namespace SoftServe.Reports.ThR.SummaryReport.Utility.MinimizedGridView
{
    public class MinimizedGridViewInfoRegistrator : GridInfoRegistrator
    {
        public override string ViewName { get { return "MyGridView"; } }
        public override BaseView CreateView(GridControl grid) { return new MinimizedGridView(grid as GridControl); }
        public override BaseViewInfo CreateViewInfo(BaseView view) { return new MinimizedGridViewInfo(view as MinimizedGridView); }
        public override BaseViewHandler CreateHandler(BaseView view) { return new MinimizedGridHandler(view as MinimizedGridView); }
    }
}

