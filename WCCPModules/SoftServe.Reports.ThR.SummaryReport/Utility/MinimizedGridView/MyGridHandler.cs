﻿using System.Windows.Forms;
using DevExpress.XtraGrid.Views.Grid;

namespace SoftServe.Reports.ThR.SummaryReport.Utility.MinimizedGridView
{
    public class MinimizedGridHandler : DevExpress.XtraGrid.Views.Grid.Handler.GridHandler
    {
        public MinimizedGridHandler(GridView gridView) : base(gridView) { }

        //protected override void OnKeyDown(KeyEventArgs e)
        //{
        //    base.OnKeyDown(e);
        //    if (e.KeyData == Keys.Delete && View.State == GridState.Normal)
        //        View.DeleteRow(View.FocusedRowHandle);
        //}
    }
}

