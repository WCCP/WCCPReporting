﻿using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Registrator;

namespace SoftServe.Reports.ThR.SummaryReport.Utility.MinimizedGridView
{
    public class MinimizedGridControl : GridControl
    {
        protected override BaseView CreateDefaultView()
        {
            return CreateView("MinimizedGridView");
        }
        protected override void RegisterAvailableViewsCore(InfoCollection collection)
        {
            base.RegisterAvailableViewsCore(collection);
            collection.Add(new MinimizedGridViewInfoRegistrator());
        }
    }
}

