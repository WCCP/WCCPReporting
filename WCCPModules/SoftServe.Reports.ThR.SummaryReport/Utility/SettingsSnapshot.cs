using System.Collections.Generic;
using SoftServe.Reports.ThR.SummaryReport.DataProvider;
using SoftServe.Reports.ThR.SummaryReport.Properties;

namespace SoftServe.Reports.ThR.SummaryReport.Utility
{
    public class SettingsSnapshot
    {
        public SettingsSnapshot()
        {
            SelectedPOC = new long[]{};
        }

        public int WaveId { get; set; }
        public string WaveName { get; set; }
        public int ChannelId { get; set; }
        public string ChannelName { get; set; }
        public List<int> RegionIds { get; set; }
        public List<string> RegionNames { get; set; }
        public FilterInfo Filter { get; set; }
        public bool IsBySKU { get; set; }
        public bool IsM2 { get; set; }

        public string FilterName
        {
            get { return Filter != null ? Filter.Filter_Name : Resources.txtWithoutFilter; }
        }

        public string RegionsDescription {
            get { return string.Join(",", RegionNames.ToArray()); }
        }

        public string RegionsDescriptionForDisplay
        {
            get { return string.Join(", ", RegionNames.ToArray()); }
        }

        public IEnumerable<long> SelectedPOC { get; set; }

    }
}