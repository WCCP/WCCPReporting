﻿using System;
using DevExpress.Utils.Menu;
using DevExpress.XtraGrid.Menu;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.BandedGrid;

namespace SoftServe.Reports.ThR.SummaryReport.Utility
{
    public class GridViewColumnButtonMenu : GridViewMenu
    {
        public delegate void ExecuteMenuCommandDelegate(object sender, GridViewCommandEventArgs e);

        public event ExecuteMenuCommandDelegate MenuCommand;

        public GridViewColumnButtonMenu(DevExpress.XtraGrid.Views.Grid.GridView view) : base(view) { }
        // Create menu items. 
        // This method is automatically called by the menu's public Init method. 
        protected override void CreateItems()
        {
            Items.Clear();
            DXSubMenuItem columnsItem = new DXSubMenuItem("Группы полей");
            Items.Add(columnsItem);
            Items.Add(CreateMenuItem("Открыть настройки групп", GridMenuImages.Column.Images[3],
                                     "Customization", true));

            var gridView = View as BandedGridView;
            if (gridView != null)
                foreach (GridBand band in gridView.Bands)
                {
                    columnsItem.Items.Add(CreateMenuCheckItem(band.Caption, band.Visible,
                                                                  null, band, true));
                }
        }

        protected override void OnMenuItemClick(object sender, EventArgs e)
        {
            if (RaiseClickEvent(sender, null)) return;
            DXMenuItem item = sender as DXMenuItem;
            if (item == null || item.Tag == null)
                return;
            if (item.Tag is GridBand)
            {
                var band = item.Tag as GridBand;
                band.Visible = !band.Visible;
            }
            else if (MenuCommand != null)
            {
                MenuCommand(item, new GridViewCommandEventArgs(item.Tag.ToString()));
            }
        }
    }
}