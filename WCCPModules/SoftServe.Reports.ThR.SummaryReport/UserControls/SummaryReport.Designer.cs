﻿namespace SoftServe.Reports.ThR.SummaryReport.UserControls
{
    partial class SummaryReport
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelControlHeader = new DevExpress.XtraEditors.PanelControl();
            this.lblAveragePOCVolume = new DevExpress.XtraEditors.LabelControl();
            this.lblPocSample = new DevExpress.XtraEditors.LabelControl();
            this.lblIndustryVolume = new DevExpress.XtraEditors.LabelControl();
            this.lblAveragePOCVolumeUnit = new DevExpress.XtraEditors.LabelControl();
            this.lblPocSampleUnit = new DevExpress.XtraEditors.LabelControl();
            this.lblIndustryVolumeUnit = new DevExpress.XtraEditors.LabelControl();
            this.lblAveragePOCVolumeName = new DevExpress.XtraEditors.LabelControl();
            this.lblPocSampleName = new DevExpress.XtraEditors.LabelControl();
            this.lblIndustryVolumeName = new DevExpress.XtraEditors.LabelControl();
            this.panelControlGrid = new DevExpress.XtraEditors.PanelControl();
            this.gridControl = new SoftServe.Reports.ThR.SummaryReport.Utility.MinimizedGridView.MinimizedGridControl();
            this.gridView = new SoftServe.Reports.ThR.SummaryReport.Utility.MinimizedGridView.MinimizedGridView();
            ((System.ComponentModel.ISupportInitialize)(this.panelControlHeader)).BeginInit();
            this.panelControlHeader.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControlGrid)).BeginInit();
            this.panelControlGrid.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControlHeader
            // 
            this.panelControlHeader.Controls.Add(this.lblAveragePOCVolume);
            this.panelControlHeader.Controls.Add(this.lblPocSample);
            this.panelControlHeader.Controls.Add(this.lblIndustryVolume);
            this.panelControlHeader.Controls.Add(this.lblAveragePOCVolumeUnit);
            this.panelControlHeader.Controls.Add(this.lblPocSampleUnit);
            this.panelControlHeader.Controls.Add(this.lblIndustryVolumeUnit);
            this.panelControlHeader.Controls.Add(this.lblAveragePOCVolumeName);
            this.panelControlHeader.Controls.Add(this.lblPocSampleName);
            this.panelControlHeader.Controls.Add(this.lblIndustryVolumeName);
            this.panelControlHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControlHeader.Location = new System.Drawing.Point(0, 0);
            this.panelControlHeader.Name = "panelControlHeader";
            this.panelControlHeader.Size = new System.Drawing.Size(1016, 64);
            this.panelControlHeader.TabIndex = 0;
            // 
            // lblAveragePOCVolume
            // 
            this.lblAveragePOCVolume.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.lblAveragePOCVolume.Appearance.Options.UseFont = true;
            this.lblAveragePOCVolume.Location = new System.Drawing.Point(203, 43);
            this.lblAveragePOCVolume.Name = "lblAveragePOCVolume";
            this.lblAveragePOCVolume.Size = new System.Drawing.Size(7, 13);
            this.lblAveragePOCVolume.TabIndex = 8;
            this.lblAveragePOCVolume.Text = "0";
            // 
            // lblPocSample
            // 
            this.lblPocSample.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.lblPocSample.Appearance.Options.UseFont = true;
            this.lblPocSample.Location = new System.Drawing.Point(203, 24);
            this.lblPocSample.Name = "lblPocSample";
            this.lblPocSample.Size = new System.Drawing.Size(7, 13);
            this.lblPocSample.TabIndex = 7;
            this.lblPocSample.Text = "0";
            // 
            // lblIndustryVolume
            // 
            this.lblIndustryVolume.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.lblIndustryVolume.Appearance.Options.UseFont = true;
            this.lblIndustryVolume.Location = new System.Drawing.Point(203, 5);
            this.lblIndustryVolume.Name = "lblIndustryVolume";
            this.lblIndustryVolume.Size = new System.Drawing.Size(7, 13);
            this.lblIndustryVolume.TabIndex = 6;
            this.lblIndustryVolume.Text = "0";
            // 
            // lblAveragePOCVolumeUnit
            // 
            this.lblAveragePOCVolumeUnit.Location = new System.Drawing.Point(129, 43);
            this.lblAveragePOCVolumeUnit.Name = "lblAveragePOCVolumeUnit";
            this.lblAveragePOCVolumeUnit.Size = new System.Drawing.Size(68, 13);
            this.lblAveragePOCVolumeUnit.TabIndex = 5;
            this.lblAveragePOCVolumeUnit.Text = "dal/POC/week";
            // 
            // lblPocSampleUnit
            // 
            this.lblPocSampleUnit.Location = new System.Drawing.Point(147, 24);
            this.lblPocSampleUnit.Name = "lblPocSampleUnit";
            this.lblPocSampleUnit.Size = new System.Drawing.Size(50, 13);
            this.lblPocSampleUnit.TabIndex = 4;
            this.lblPocSampleUnit.Text = "# of POCs";
            // 
            // lblIndustryVolumeUnit
            // 
            this.lblIndustryVolumeUnit.Location = new System.Drawing.Point(154, 5);
            this.lblIndustryVolumeUnit.Name = "lblIndustryVolumeUnit";
            this.lblIndustryVolumeUnit.Size = new System.Drawing.Size(43, 13);
            this.lblIndustryVolumeUnit.TabIndex = 3;
            this.lblIndustryVolumeUnit.Text = "dal/week";
            // 
            // lblAveragePOCVolumeName
            // 
            this.lblAveragePOCVolumeName.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.lblAveragePOCVolumeName.Appearance.Options.UseFont = true;
            this.lblAveragePOCVolumeName.Location = new System.Drawing.Point(5, 43);
            this.lblAveragePOCVolumeName.Name = "lblAveragePOCVolumeName";
            this.lblAveragePOCVolumeName.Size = new System.Drawing.Size(118, 13);
            this.lblAveragePOCVolumeName.TabIndex = 2;
            this.lblAveragePOCVolumeName.Text = "Average POC volume";
            // 
            // lblPocSampleName
            // 
            this.lblPocSampleName.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.lblPocSampleName.Appearance.Options.UseFont = true;
            this.lblPocSampleName.Location = new System.Drawing.Point(57, 24);
            this.lblPocSampleName.Name = "lblPocSampleName";
            this.lblPocSampleName.Size = new System.Drawing.Size(66, 13);
            this.lblPocSampleName.TabIndex = 1;
            this.lblPocSampleName.Text = "POC sample";
            // 
            // lblIndustryVolumeName
            // 
            this.lblIndustryVolumeName.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.lblIndustryVolumeName.Appearance.Options.UseFont = true;
            this.lblIndustryVolumeName.Location = new System.Drawing.Point(29, 5);
            this.lblIndustryVolumeName.Name = "lblIndustryVolumeName";
            this.lblIndustryVolumeName.Size = new System.Drawing.Size(94, 13);
            this.lblIndustryVolumeName.TabIndex = 0;
            this.lblIndustryVolumeName.Text = "Industry Volume";
            // 
            // panelControlGrid
            // 
            this.panelControlGrid.Controls.Add(this.gridControl);
            this.panelControlGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControlGrid.Location = new System.Drawing.Point(0, 64);
            this.panelControlGrid.Name = "panelControlGrid";
            this.panelControlGrid.Size = new System.Drawing.Size(1016, 530);
            this.panelControlGrid.TabIndex = 1;
            // 
            // gridControl
            // 
            this.gridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl.Location = new System.Drawing.Point(2, 2);
            this.gridControl.MainView = this.gridView;
            this.gridControl.Name = "gridControl";
            this.gridControl.Size = new System.Drawing.Size(1012, 526);
            this.gridControl.TabIndex = 0;
            this.gridControl.UseEmbeddedNavigator = true;
            this.gridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView});
            // 
            // gridView
            // 
            this.gridView.ColumnPanelRowHeight = 45;
            this.gridView.GridControl = this.gridControl;
            this.gridView.Name = "gridView";
            this.gridView.OptionsBehavior.Editable = false;
            this.gridView.OptionsCustomization.AllowColumnMoving = false;
            this.gridView.OptionsCustomization.AllowGroup = false;
            this.gridView.OptionsCustomization.AllowQuickHideColumns = false;
            this.gridView.OptionsCustomization.AllowSort = false;
            this.gridView.OptionsView.ColumnAutoWidth = false;
            this.gridView.OptionsView.ShowGroupPanel = false;
            this.gridView.OptionsView.ShowIndicator = false;
            // 
            // SummaryReport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panelControlGrid);
            this.Controls.Add(this.panelControlHeader);
            this.Name = "SummaryReport";
            this.Size = new System.Drawing.Size(1016, 594);
            ((System.ComponentModel.ISupportInitialize)(this.panelControlHeader)).EndInit();
            this.panelControlHeader.ResumeLayout(false);
            this.panelControlHeader.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControlGrid)).EndInit();
            this.panelControlGrid.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControlHeader;
        private DevExpress.XtraEditors.PanelControl panelControlGrid;
        private DevExpress.XtraEditors.LabelControl lblAveragePOCVolume;
        private DevExpress.XtraEditors.LabelControl lblPocSample;
        private DevExpress.XtraEditors.LabelControl lblIndustryVolume;
        private DevExpress.XtraEditors.LabelControl lblAveragePOCVolumeUnit;
        private DevExpress.XtraEditors.LabelControl lblPocSampleUnit;
        private DevExpress.XtraEditors.LabelControl lblIndustryVolumeUnit;
        private DevExpress.XtraEditors.LabelControl lblAveragePOCVolumeName;
        private DevExpress.XtraEditors.LabelControl lblPocSampleName;
        private DevExpress.XtraEditors.LabelControl lblIndustryVolumeName;
        private SoftServe.Reports.ThR.SummaryReport.Utility.MinimizedGridView.MinimizedGridControl gridControl;
        private SoftServe.Reports.ThR.SummaryReport.Utility.MinimizedGridView.MinimizedGridView gridView;
    }
}
