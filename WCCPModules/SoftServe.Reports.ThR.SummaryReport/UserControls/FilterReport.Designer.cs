﻿namespace SoftServe.Reports.ThR.SummaryReport.UserControls
{
    partial class FilterReport
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelControlHeader = new DevExpress.XtraEditors.PanelControl();
            this.txtFilterName = new DevExpress.XtraEditors.TextEdit();
            this.txtFilterCriteria = new DevExpress.XtraEditors.TextEdit();
            this.lblMarketProgram = new DevExpress.XtraEditors.LabelControl();
            this.lblFilterCriteria = new DevExpress.XtraEditors.LabelControl();
            this.lblCompetitorVolume = new DevExpress.XtraEditors.LabelControl();
            this.lblInBevVolume = new DevExpress.XtraEditors.LabelControl();
            this.lblPOCs = new DevExpress.XtraEditors.LabelControl();
            this.lblFilteredIndustryVolume = new DevExpress.XtraEditors.LabelControl();
            this.lblAveragePocVolume = new DevExpress.XtraEditors.LabelControl();
            this.lblPOCSample = new DevExpress.XtraEditors.LabelControl();
            this.lblIndustryVolume = new DevExpress.XtraEditors.LabelControl();
            this.lblCompetitorVolumeUnit = new DevExpress.XtraEditors.LabelControl();
            this.lblInBevVolumeUnit = new DevExpress.XtraEditors.LabelControl();
            this.lblPOCsUnit = new DevExpress.XtraEditors.LabelControl();
            this.lblFilteredIndustryVolumeUnit = new DevExpress.XtraEditors.LabelControl();
            this.lblAveragePocVolumeUnit = new DevExpress.XtraEditors.LabelControl();
            this.lblPOCSampleUnit = new DevExpress.XtraEditors.LabelControl();
            this.lblIndustryVolumeUnit = new DevExpress.XtraEditors.LabelControl();
            this.lblCompetitorVolumeName = new DevExpress.XtraEditors.LabelControl();
            this.lblInBevVolumeName = new DevExpress.XtraEditors.LabelControl();
            this.lblPOCsName = new DevExpress.XtraEditors.LabelControl();
            this.lblFilteredIndustryVolumeName = new DevExpress.XtraEditors.LabelControl();
            this.lblAveragePocVolumeName = new DevExpress.XtraEditors.LabelControl();
            this.lblPOCSampleName = new DevExpress.XtraEditors.LabelControl();
            this.lblIndustryVolumeName = new DevExpress.XtraEditors.LabelControl();
            this.lblInformation = new DevExpress.XtraEditors.LabelControl();
            this.panelControlGrid = new DevExpress.XtraEditors.PanelControl();
            this.gridControl = new SoftServe.Reports.ThR.SummaryReport.Utility.MinimizedGridView.MinimizedGridControl();
            this.gridView = new SoftServe.Reports.ThR.SummaryReport.Utility.MinimizedGridView.MinimizedGridView();
            ((System.ComponentModel.ISupportInitialize)(this.panelControlHeader)).BeginInit();
            this.panelControlHeader.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtFilterName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFilterCriteria.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControlGrid)).BeginInit();
            this.panelControlGrid.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControlHeader
            // 
            this.panelControlHeader.Controls.Add(this.txtFilterName);
            this.panelControlHeader.Controls.Add(this.txtFilterCriteria);
            this.panelControlHeader.Controls.Add(this.lblMarketProgram);
            this.panelControlHeader.Controls.Add(this.lblFilterCriteria);
            this.panelControlHeader.Controls.Add(this.lblCompetitorVolume);
            this.panelControlHeader.Controls.Add(this.lblInBevVolume);
            this.panelControlHeader.Controls.Add(this.lblPOCs);
            this.panelControlHeader.Controls.Add(this.lblFilteredIndustryVolume);
            this.panelControlHeader.Controls.Add(this.lblAveragePocVolume);
            this.panelControlHeader.Controls.Add(this.lblPOCSample);
            this.panelControlHeader.Controls.Add(this.lblIndustryVolume);
            this.panelControlHeader.Controls.Add(this.lblCompetitorVolumeUnit);
            this.panelControlHeader.Controls.Add(this.lblInBevVolumeUnit);
            this.panelControlHeader.Controls.Add(this.lblPOCsUnit);
            this.panelControlHeader.Controls.Add(this.lblFilteredIndustryVolumeUnit);
            this.panelControlHeader.Controls.Add(this.lblAveragePocVolumeUnit);
            this.panelControlHeader.Controls.Add(this.lblPOCSampleUnit);
            this.panelControlHeader.Controls.Add(this.lblIndustryVolumeUnit);
            this.panelControlHeader.Controls.Add(this.lblCompetitorVolumeName);
            this.panelControlHeader.Controls.Add(this.lblInBevVolumeName);
            this.panelControlHeader.Controls.Add(this.lblPOCsName);
            this.panelControlHeader.Controls.Add(this.lblFilteredIndustryVolumeName);
            this.panelControlHeader.Controls.Add(this.lblAveragePocVolumeName);
            this.panelControlHeader.Controls.Add(this.lblPOCSampleName);
            this.panelControlHeader.Controls.Add(this.lblIndustryVolumeName);
            this.panelControlHeader.Controls.Add(this.lblInformation);
            this.panelControlHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControlHeader.Location = new System.Drawing.Point(0, 0);
            this.panelControlHeader.Name = "panelControlHeader";
            this.panelControlHeader.Size = new System.Drawing.Size(934, 123);
            this.panelControlHeader.TabIndex = 0;
            // 
            // txtFilterName
            // 
            this.txtFilterName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtFilterName.Location = new System.Drawing.Point(311, 69);
            this.txtFilterName.Name = "txtFilterName";
            this.txtFilterName.Size = new System.Drawing.Size(618, 20);
            this.txtFilterName.TabIndex = 25;
            // 
            // txtFilterCriteria
            // 
            this.txtFilterCriteria.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtFilterCriteria.Location = new System.Drawing.Point(311, 24);
            this.txtFilterCriteria.Name = "txtFilterCriteria";
            this.txtFilterCriteria.Size = new System.Drawing.Size(618, 20);
            this.txtFilterCriteria.TabIndex = 24;
            // 
            // lblMarketProgram
            // 
            this.lblMarketProgram.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.lblMarketProgram.Appearance.Options.UseFont = true;
            this.lblMarketProgram.Location = new System.Drawing.Point(311, 50);
            this.lblMarketProgram.Name = "lblMarketProgram";
            this.lblMarketProgram.Size = new System.Drawing.Size(96, 13);
            this.lblMarketProgram.TabIndex = 23;
            this.lblMarketProgram.Text = "Market Program:";
            // 
            // lblFilterCriteria
            // 
            this.lblFilterCriteria.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.lblFilterCriteria.Appearance.Options.UseFont = true;
            this.lblFilterCriteria.Location = new System.Drawing.Point(311, 5);
            this.lblFilterCriteria.Name = "lblFilterCriteria";
            this.lblFilterCriteria.Size = new System.Drawing.Size(77, 13);
            this.lblFilterCriteria.TabIndex = 22;
            this.lblFilterCriteria.Text = "Filter Criteria:";
            // 
            // lblCompetitorVolume
            // 
            this.lblCompetitorVolume.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.lblCompetitorVolume.Appearance.Options.UseFont = true;
            this.lblCompetitorVolume.Location = new System.Drawing.Point(218, 103);
            this.lblCompetitorVolume.Name = "lblCompetitorVolume";
            this.lblCompetitorVolume.Size = new System.Drawing.Size(7, 13);
            this.lblCompetitorVolume.TabIndex = 21;
            this.lblCompetitorVolume.Text = "0";
            // 
            // lblInBevVolume
            // 
            this.lblInBevVolume.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.lblInBevVolume.Appearance.Options.UseFont = true;
            this.lblInBevVolume.Location = new System.Drawing.Point(218, 89);
            this.lblInBevVolume.Name = "lblInBevVolume";
            this.lblInBevVolume.Size = new System.Drawing.Size(7, 13);
            this.lblInBevVolume.TabIndex = 20;
            this.lblInBevVolume.Text = "0";
            // 
            // lblPOCs
            // 
            this.lblPOCs.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.lblPOCs.Appearance.Options.UseFont = true;
            this.lblPOCs.Location = new System.Drawing.Point(218, 75);
            this.lblPOCs.Name = "lblPOCs";
            this.lblPOCs.Size = new System.Drawing.Size(7, 13);
            this.lblPOCs.TabIndex = 19;
            this.lblPOCs.Text = "0";
            // 
            // lblFilteredIndustryVolume
            // 
            this.lblFilteredIndustryVolume.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.lblFilteredIndustryVolume.Appearance.Options.UseFont = true;
            this.lblFilteredIndustryVolume.Location = new System.Drawing.Point(218, 61);
            this.lblFilteredIndustryVolume.Name = "lblFilteredIndustryVolume";
            this.lblFilteredIndustryVolume.Size = new System.Drawing.Size(7, 13);
            this.lblFilteredIndustryVolume.TabIndex = 18;
            this.lblFilteredIndustryVolume.Text = "0";
            // 
            // lblAveragePocVolume
            // 
            this.lblAveragePocVolume.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.lblAveragePocVolume.Appearance.Options.UseFont = true;
            this.lblAveragePocVolume.Location = new System.Drawing.Point(218, 47);
            this.lblAveragePocVolume.Name = "lblAveragePocVolume";
            this.lblAveragePocVolume.Size = new System.Drawing.Size(7, 13);
            this.lblAveragePocVolume.TabIndex = 17;
            this.lblAveragePocVolume.Text = "0";
            // 
            // lblPOCSample
            // 
            this.lblPOCSample.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.lblPOCSample.Appearance.Options.UseFont = true;
            this.lblPOCSample.Location = new System.Drawing.Point(218, 33);
            this.lblPOCSample.Name = "lblPOCSample";
            this.lblPOCSample.Size = new System.Drawing.Size(7, 13);
            this.lblPOCSample.TabIndex = 16;
            this.lblPOCSample.Text = "0";
            // 
            // lblIndustryVolume
            // 
            this.lblIndustryVolume.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.lblIndustryVolume.Appearance.Options.UseFont = true;
            this.lblIndustryVolume.Location = new System.Drawing.Point(218, 19);
            this.lblIndustryVolume.Name = "lblIndustryVolume";
            this.lblIndustryVolume.Size = new System.Drawing.Size(7, 13);
            this.lblIndustryVolume.TabIndex = 15;
            this.lblIndustryVolume.Text = "0";
            // 
            // lblCompetitorVolumeUnit
            // 
            this.lblCompetitorVolumeUnit.Location = new System.Drawing.Point(163, 103);
            this.lblCompetitorVolumeUnit.Name = "lblCompetitorVolumeUnit";
            this.lblCompetitorVolumeUnit.Size = new System.Drawing.Size(49, 13);
            this.lblCompetitorVolumeUnit.TabIndex = 14;
            this.lblCompetitorVolumeUnit.Text = "% of total";
            // 
            // lblInBevVolumeUnit
            // 
            this.lblInBevVolumeUnit.Location = new System.Drawing.Point(163, 89);
            this.lblInBevVolumeUnit.Name = "lblInBevVolumeUnit";
            this.lblInBevVolumeUnit.Size = new System.Drawing.Size(49, 13);
            this.lblInBevVolumeUnit.TabIndex = 13;
            this.lblInBevVolumeUnit.Text = "% of total";
            // 
            // lblPOCsUnit
            // 
            this.lblPOCsUnit.Location = new System.Drawing.Point(163, 75);
            this.lblPOCsUnit.Name = "lblPOCsUnit";
            this.lblPOCsUnit.Size = new System.Drawing.Size(49, 13);
            this.lblPOCsUnit.TabIndex = 12;
            this.lblPOCsUnit.Text = "% of total";
            // 
            // lblFilteredIndustryVolumeUnit
            // 
            this.lblFilteredIndustryVolumeUnit.Location = new System.Drawing.Point(169, 61);
            this.lblFilteredIndustryVolumeUnit.Name = "lblFilteredIndustryVolumeUnit";
            this.lblFilteredIndustryVolumeUnit.Size = new System.Drawing.Size(43, 13);
            this.lblFilteredIndustryVolumeUnit.TabIndex = 11;
            this.lblFilteredIndustryVolumeUnit.Text = "dal/week";
            // 
            // lblAveragePocVolumeUnit
            // 
            this.lblAveragePocVolumeUnit.Location = new System.Drawing.Point(144, 47);
            this.lblAveragePocVolumeUnit.Name = "lblAveragePocVolumeUnit";
            this.lblAveragePocVolumeUnit.Size = new System.Drawing.Size(68, 13);
            this.lblAveragePocVolumeUnit.TabIndex = 10;
            this.lblAveragePocVolumeUnit.Text = "dal/POC/week";
            // 
            // lblPOCSampleUnit
            // 
            this.lblPOCSampleUnit.Location = new System.Drawing.Point(162, 33);
            this.lblPOCSampleUnit.Name = "lblPOCSampleUnit";
            this.lblPOCSampleUnit.Size = new System.Drawing.Size(50, 13);
            this.lblPOCSampleUnit.TabIndex = 9;
            this.lblPOCSampleUnit.Text = "# of POCs";
            // 
            // lblIndustryVolumeUnit
            // 
            this.lblIndustryVolumeUnit.Location = new System.Drawing.Point(169, 19);
            this.lblIndustryVolumeUnit.Name = "lblIndustryVolumeUnit";
            this.lblIndustryVolumeUnit.Size = new System.Drawing.Size(43, 13);
            this.lblIndustryVolumeUnit.TabIndex = 8;
            this.lblIndustryVolumeUnit.Text = "dal/week";
            // 
            // lblCompetitorVolumeName
            // 
            this.lblCompetitorVolumeName.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.lblCompetitorVolumeName.Appearance.Options.UseFont = true;
            this.lblCompetitorVolumeName.Location = new System.Drawing.Point(29, 103);
            this.lblCompetitorVolumeName.Name = "lblCompetitorVolumeName";
            this.lblCompetitorVolumeName.Size = new System.Drawing.Size(109, 13);
            this.lblCompetitorVolumeName.TabIndex = 7;
            this.lblCompetitorVolumeName.Text = "Competitor volume";
            // 
            // lblInBevVolumeName
            // 
            this.lblInBevVolumeName.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.lblInBevVolumeName.Appearance.Options.UseFont = true;
            this.lblInBevVolumeName.Location = new System.Drawing.Point(60, 89);
            this.lblInBevVolumeName.Name = "lblInBevVolumeName";
            this.lblInBevVolumeName.Size = new System.Drawing.Size(78, 13);
            this.lblInBevVolumeName.TabIndex = 6;
            this.lblInBevVolumeName.Text = "InBev volume";
            // 
            // lblPOCsName
            // 
            this.lblPOCsName.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.lblPOCsName.Appearance.Options.UseFont = true;
            this.lblPOCsName.Location = new System.Drawing.Point(110, 75);
            this.lblPOCsName.Name = "lblPOCsName";
            this.lblPOCsName.Size = new System.Drawing.Size(28, 13);
            this.lblPOCsName.TabIndex = 5;
            this.lblPOCsName.Text = "POCs";
            // 
            // lblFilteredIndustryVolumeName
            // 
            this.lblFilteredIndustryVolumeName.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.lblFilteredIndustryVolumeName.Appearance.Options.UseFont = true;
            this.lblFilteredIndustryVolumeName.Location = new System.Drawing.Point(5, 61);
            this.lblFilteredIndustryVolumeName.Name = "lblFilteredIndustryVolumeName";
            this.lblFilteredIndustryVolumeName.Size = new System.Drawing.Size(133, 13);
            this.lblFilteredIndustryVolumeName.TabIndex = 4;
            this.lblFilteredIndustryVolumeName.Text = "Filtred Industry Volume";
            // 
            // lblAveragePocVolumeName
            // 
            this.lblAveragePocVolumeName.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.lblAveragePocVolumeName.Appearance.Options.UseFont = true;
            this.lblAveragePocVolumeName.Location = new System.Drawing.Point(20, 47);
            this.lblAveragePocVolumeName.Name = "lblAveragePocVolumeName";
            this.lblAveragePocVolumeName.Size = new System.Drawing.Size(118, 13);
            this.lblAveragePocVolumeName.TabIndex = 3;
            this.lblAveragePocVolumeName.Text = "Average POC volume";
            // 
            // lblPOCSampleName
            // 
            this.lblPOCSampleName.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.lblPOCSampleName.Appearance.Options.UseFont = true;
            this.lblPOCSampleName.Location = new System.Drawing.Point(72, 33);
            this.lblPOCSampleName.Name = "lblPOCSampleName";
            this.lblPOCSampleName.Size = new System.Drawing.Size(66, 13);
            this.lblPOCSampleName.TabIndex = 2;
            this.lblPOCSampleName.Text = "POC sample";
            // 
            // lblIndustryVolumeName
            // 
            this.lblIndustryVolumeName.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.lblIndustryVolumeName.Appearance.Options.UseFont = true;
            this.lblIndustryVolumeName.Location = new System.Drawing.Point(44, 19);
            this.lblIndustryVolumeName.Name = "lblIndustryVolumeName";
            this.lblIndustryVolumeName.Size = new System.Drawing.Size(94, 13);
            this.lblIndustryVolumeName.TabIndex = 1;
            this.lblIndustryVolumeName.Text = "Industry Volume";
            // 
            // lblInformation
            // 
            this.lblInformation.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))));
            this.lblInformation.Appearance.Options.UseFont = true;
            this.lblInformation.Location = new System.Drawing.Point(5, 5);
            this.lblInformation.Name = "lblInformation";
            this.lblInformation.Size = new System.Drawing.Size(180, 13);
            this.lblInformation.TabIndex = 0;
            this.lblInformation.Text = "Information from selected POCs";
            // 
            // panelControlGrid
            // 
            this.panelControlGrid.Controls.Add(this.gridControl);
            this.panelControlGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControlGrid.Location = new System.Drawing.Point(0, 123);
            this.panelControlGrid.Name = "panelControlGrid";
            this.panelControlGrid.Size = new System.Drawing.Size(934, 481);
            this.panelControlGrid.TabIndex = 1;
            // 
            // gridControl
            // 
            this.gridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl.Location = new System.Drawing.Point(2, 2);
            this.gridControl.MainView = this.gridView;
            this.gridControl.Name = "gridControl";
            this.gridControl.Size = new System.Drawing.Size(930, 477);
            this.gridControl.TabIndex = 0;
            this.gridControl.UseEmbeddedNavigator = true;
            this.gridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView});
            // 
            // gridView
            // 
            this.gridView.ColumnPanelRowHeight = 45;
            this.gridView.GridControl = this.gridControl;
            this.gridView.Name = "gridView";
            this.gridView.OptionsBehavior.Editable = false;
            this.gridView.OptionsCustomization.AllowColumnMoving = false;
            this.gridView.OptionsCustomization.AllowQuickHideColumns = false;
            this.gridView.OptionsCustomization.AllowSort = false;
            this.gridView.OptionsView.ColumnAutoWidth = false;
            this.gridView.OptionsView.ShowGroupPanel = false;
            this.gridView.OptionsView.ShowIndicator = false;
            // 
            // FilterReport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panelControlGrid);
            this.Controls.Add(this.panelControlHeader);
            this.Name = "FilterReport";
            this.Size = new System.Drawing.Size(934, 604);
            ((System.ComponentModel.ISupportInitialize)(this.panelControlHeader)).EndInit();
            this.panelControlHeader.ResumeLayout(false);
            this.panelControlHeader.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtFilterName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFilterCriteria.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControlGrid)).EndInit();
            this.panelControlGrid.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControlHeader;
        private DevExpress.XtraEditors.PanelControl panelControlGrid;
        private SoftServe.Reports.ThR.SummaryReport.Utility.MinimizedGridView.MinimizedGridControl gridControl;
        private SoftServe.Reports.ThR.SummaryReport.Utility.MinimizedGridView.MinimizedGridView gridView;
        private DevExpress.XtraEditors.TextEdit txtFilterName;
        private DevExpress.XtraEditors.TextEdit txtFilterCriteria;
        private DevExpress.XtraEditors.LabelControl lblMarketProgram;
        private DevExpress.XtraEditors.LabelControl lblFilterCriteria;
        private DevExpress.XtraEditors.LabelControl lblCompetitorVolume;
        private DevExpress.XtraEditors.LabelControl lblInBevVolume;
        private DevExpress.XtraEditors.LabelControl lblPOCs;
        private DevExpress.XtraEditors.LabelControl lblFilteredIndustryVolume;
        private DevExpress.XtraEditors.LabelControl lblAveragePocVolume;
        private DevExpress.XtraEditors.LabelControl lblPOCSample;
        private DevExpress.XtraEditors.LabelControl lblIndustryVolume;
        private DevExpress.XtraEditors.LabelControl lblCompetitorVolumeUnit;
        private DevExpress.XtraEditors.LabelControl lblInBevVolumeUnit;
        private DevExpress.XtraEditors.LabelControl lblPOCsUnit;
        private DevExpress.XtraEditors.LabelControl lblFilteredIndustryVolumeUnit;
        private DevExpress.XtraEditors.LabelControl lblAveragePocVolumeUnit;
        private DevExpress.XtraEditors.LabelControl lblPOCSampleUnit;
        private DevExpress.XtraEditors.LabelControl lblIndustryVolumeUnit;
        private DevExpress.XtraEditors.LabelControl lblCompetitorVolumeName;
        private DevExpress.XtraEditors.LabelControl lblInBevVolumeName;
        private DevExpress.XtraEditors.LabelControl lblPOCsName;
        private DevExpress.XtraEditors.LabelControl lblFilteredIndustryVolumeName;
        private DevExpress.XtraEditors.LabelControl lblAveragePocVolumeName;
        private DevExpress.XtraEditors.LabelControl lblPOCSampleName;
        private DevExpress.XtraEditors.LabelControl lblIndustryVolumeName;
        private DevExpress.XtraEditors.LabelControl lblInformation;
    }
}
