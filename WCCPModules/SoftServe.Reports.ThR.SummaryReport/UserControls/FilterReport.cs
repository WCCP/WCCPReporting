﻿using System;
using System.Collections.Generic;
using System.Linq;
using DevExpress.Utils;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Grid;
using Logica.Reports.Common.WaitWindow;
using SoftServe.Reports.ThR.SummaryReport.DataProvider;
using SoftServe.Reports.ThR.SummaryReport.Utility;
using DataTable = System.Data.DataTable;

namespace SoftServe.Reports.ThR.SummaryReport.UserControls
{
    public partial class FilterReport : XtraUserControl//, IReportControl
    {
        private readonly LabelControl[] labelControlsValues;

        public FilterReport()
        {
            InitializeComponent();
            labelControlsValues = new[]
                                      {
                                          lblIndustryVolume, lblPOCSample, lblAveragePocVolume, lblFilteredIndustryVolume,
                                          lblPOCs, lblInBevVolume, lblCompetitorVolume
                                      };

        }

        public void LoadData(SettingsSnapshot settings)
        {
            DataTable summaryReport = null;
            gridView.Columns.Clear();
            WaitManager.StartWait();
            try
            {
                summaryReport = DataAccessProvider.GetSummaryReport(settings, false);
            }
            finally
            {
                WaitManager.StopWait();
            }

            foreach (var control in labelControlsValues)
            {
                control.Text = @"0";
            }

            txtFilterName.EditValue = settings.FilterName;
            txtFilterCriteria.EditValue = settings.Filter == null ? string.Empty : settings.Filter.Filter_text;

            var dataRows = summaryReport.Select("isOut = 1");
            foreach (var dataRow in dataRows)
            {
                switch ((int)dataRow["id"])
                {// from SP dbo.DW_TomasResearch_SummaryTotal
                    case 1: //Industry Volume, dal/week
                        lblIndustryVolume.Text = dataRow[4].ToString();
                        break;
                    case 2: //POC sample, # of POCs
                        lblPOCSample.Text = String.Format("{0:G0}", dataRow[4]);
                        break;
                    case 3: //Average POC volume, dal/POC/week
                        lblAveragePocVolume.Text = dataRow[4].ToString();
                        break;
                    //                    case 14:// bug: not exists id = 14
                    //                        lblFilteredIndustryVolume.Text = dataRow[4].ToString();
                    //                        break;
                    case 15: //Filtered Industry Volume, dal/week
                        lblFilteredIndustryVolume.Text = dataRow[4].ToString();
                        break;
                    case 16: //POCs, %
                        lblPOCs.Text = dataRow[4].ToString();
                        break;
                    case 17: //InBev volume, %
                        lblInBevVolume.Text = dataRow[4].ToString();
                        break;
                    case 18: //Competitors volume, %
                        lblCompetitorVolume.Text = dataRow[4].ToString();
                        break;
                }
                summaryReport.Rows.Remove(dataRow);
            }
            gridControl.DataSource = summaryReport;


            foreach (var column in gridView.Columns.Cast<GridColumn>().Take(2).ToArray())
            {
                column.Visible = false;
            }

            foreach (GridColumn column in gridView.Columns)
            {
                if (column.Visible)
                {
                    if (column.VisibleIndex == 0)
                        column.Width = 150;
                    else if (column.VisibleIndex == 1)
                        column.Width = 100;
                    else
                        column.Width = 72;

                    if (column.VisibleIndex < 2)
                    {
                        column.Caption = @" ";
                    }

                    column.AppearanceHeader.TextOptions.WordWrap = WordWrap.Wrap;
                    column.AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Center;
                }
            }

        }

        public string CurrentFilter
        {
            get { return (string)txtFilterCriteria.EditValue; }
            set { txtFilterCriteria.EditValue = value; }
        }

        public GridColumn[] GetColumnsForExport(int maxColumnCount)
        {
            GridColumn[] bandedGridColumns = gridView.Columns.Cast<GridColumn>().Where(c => c.Visible).OrderBy(c => c.VisibleIndex).Take(maxColumnCount).ToArray();
            return bandedGridColumns;
        }

        public GridView View
        {
            get { return gridView; }
        }

        public List<string> GetLabels()
        {
            List<string> labels = new List<string>();

            for (int i = 0; i < labelControlsValues.Length; i++)
            {
                labels.Add(labelControlsValues[i].Text);
            }

            return labels;
        }
    }
}
