﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using DevExpress.Utils;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.BandedGrid;
using DevExpress.XtraGrid.Views.Grid;
using Logica.Reports.BaseReportControl.Utility;
using Logica.Reports.Common.WaitWindow;
using SoftServe.Reports.ThR.SummaryReport.DataProvider;
using SoftServe.Reports.ThR.SummaryReport.Forms;
using SoftServe.Reports.ThR.SummaryReport.Utility;
using DevExpress.Data.Filtering;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraEditors.Repository;
using DataTable = System.Data.DataTable;
using Point = System.Drawing.Point;

namespace SoftServe.Reports.ThR.SummaryReport.UserControls
{
    public partial class DataReport : XtraUserControl, IReportControl
    {
        public DataReport()
        {
            InitializeComponent();
        }

        public IEnumerable<GroupVisibility> Groups
        {
            get
            {
                return
                    bandedGridView.Bands.Cast<GridBand>().Select(
                        b => new GroupVisibility { GroupName = b.Caption, IsVisible = b.Visible }).ToArray();
            }
            set
            {
                foreach (var groupVisibility in value)
                {
                    var band = bandedGridView.Bands.Cast<GridBand>().FirstOrDefault(
                        x => x.Caption.Equals(groupVisibility.GroupName));
                    if (band != null)
                    {
                        band.Visible = groupVisibility.IsVisible;
                    }
                }
            }
        }

        public IEnumerable<long> SelectedPOC
        {
            get
            {
                for (int i = 0; i < bandedGridView.DataRowCount; i++)
                {
                    yield return ConvertEx.ToLongInt(bandedGridView.GetRowCellValue(i, "POC Code"));
                }
            }
        }


        public GridColumn[] GetColumnsForExport(int maxColumnCount)
        {
            GridColumn[] bandedGridColumns = bandedGridView.Columns.Cast<BandedGridColumn>().Where(c => c.Visible && c.OwnerBand != null && c.OwnerBand.Visible).OrderBy(c => c.VisibleIndex).Take(maxColumnCount).ToArray();
            return bandedGridColumns;
        }

        public GridView View {
            get { return bandedGridView; }
        }

        public void LoadData(SettingsSnapshot settings)
        {
            
            IEnumerable<GroupInfo> groups;
            IEnumerable<FieldInfo> fields;
            DataTable report;
            WaitManager.StartWait();
            try
            {
                groups = DataAccessProvider.GetFieldGroups(settings);
                fields = DataAccessProvider.GetFields(settings);
                report = DataAccessProvider.GetDataReport(settings);
            }
            finally
            {
                WaitManager.StopWait();
            }
            
            bandedGridView.BeginUpdate();
            
            bandedGridView.OptionsFilter.MaxCheckedListItemCount = Int32.MaxValue;
            bandedGridView.Columns.Clear();
            bandedGridView.Bands.Clear();

            bool isGreen = true;
            foreach (DataColumn dataColumn in report.Columns)
            {
                var fieldInfo = fields.FirstOrDefault(x => x.Field_Name.Equals(dataColumn.ColumnName, StringComparison.InvariantCultureIgnoreCase));
                if (fieldInfo != null)
                {
                    var groupInfo = groups.First(x => x.Group_ID == fieldInfo.Group_Id);
                    var band = bandedGridView.Bands.Cast<GridBand>().FirstOrDefault(x => x.Caption.Equals(groupInfo.Group_Name));
                    if (band == null)
                    {
                        isGreen = !isGreen;
                        band = new GridBand { Caption = groupInfo.Group_Name };
                        band.AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Center;
                        band.AppearanceHeader.Options.UseTextOptions = true;
                        bandedGridView.Bands.Add(band);
                    }
                    var column = new BandedGridColumn
                                     {
                                         Caption = fieldInfo.Field_Name,
                                         FieldName = fieldInfo.Field_Name,
                                         Width = fieldInfo.Width,
                                         Visible = !(settings.IsM2 && (fieldInfo.Field_Name == "Region" || fieldInfo.Field_Name == "M3" || fieldInfo.Field_Name == "M4"))
                                     };
                    column.AppearanceHeader.TextOptions.WordWrap = WordWrap.Wrap;
                    column.AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Center;
                    column.AppearanceHeader.Options.UseTextOptions = true;
                    column.OptionsFilter.FilterPopupMode = FilterPopupMode.CheckedList;
                    if (fieldInfo.IsPercent)
                    {
                        column.DisplayFormat.FormatType = FormatType.Numeric;
                        column.DisplayFormat.FormatString = "N2";
                        column.DisplayFormat.Format = new PercentFormat();
                    }
                    if (isGreen)
                    {
                        column.AppearanceCell.BackColor = Color.FromArgb(0xC0, 0xDC, 0xC0);
                        column.AppearanceCell.Options.UseBackColor = true;
                    }
                    band.Columns.Add(column);
                    //bandedGridView.Columns.Add(column);
                }
            }
            bandedGridView.EndUpdate();
            gridControl.DataSource = report;


            if (settings.Filter != null)
            {
                CurrentFilter = settings.Filter.Filter_text;
            }
        }

        public string CurrentFilter
        {
            get { return bandedGridView.ActiveFilterEnabled? bandedGridView.ActiveFilter.Expression : string.Empty; }
            set {
                string lCriteriaText = SoftServe.Reports.ThR.SummaryReport.Utility.WCG.CriteriaToWhereClauseHelper.GetFilterExpressionBySqlWhere(value);
                bandedGridView.ActiveFilterCriteria = CriteriaOperator.Parse(lCriteriaText, new object[] { });
                bandedGridView.ActiveFilterEnabled = true;
            }
        }

        void DoShowMenu(DevExpress.XtraGrid.Views.Grid.ViewInfo.GridHitInfo hi)
        {
            // Create the menu. 
            // Check whether the header panel button has been clicked. 
            if (hi.HitTest == DevExpress.XtraGrid.Views.Grid.ViewInfo.GridHitTest.ColumnButton)
            {
                var menu = new GridViewColumnButtonMenu(bandedGridView);
                menu.Init(hi);
                menu.MenuCommand += (sender, e) =>
                                        {
                                            if (e.Command.Equals("Customization"))
                                            {
                                                var form = new SelectGroupsForm();
                                                form.Groups = Groups;
                                                if (form.ShowDialog() == DialogResult.OK)
                                                {
                                                    Groups = form.Groups;
                                                }
                                            }
                                        };
                // Display the menu. 
                menu.Show(hi.HitPoint);
            }
        }

        private void bandedGridView_MouseDown(object sender, MouseEventArgs e)
        {
            // Check if the end-user has right clicked the grid control. 
            if (e.Button == MouseButtons.Right)
                DoShowMenu(bandedGridView.CalcHitInfo(new Point(e.X, e.Y)));
        }

        private void bandedGridView_CustomColumnDisplayText(object sender, DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventArgs e)
        {
            if (e.Column == null)
            {
                return;
            }
            if (e.Column.FieldName == "ТТ на маршруте")
            {
                e.DisplayText = (bool) e.Value ? "Да" : "Нет";
            }
        }

       private void bandedGridView_CustomRowCellEdit(object sender, CustomRowCellEditEventArgs e)
        {
            if (e.Column == null)
            {
                return;
            }
            if (e.Column.FieldName == "ТТ на маршруте")
            {
                e.RepositoryItem = new RepositoryItemTextEdit();
            }
        }
        }
    }
