﻿using System;
using System.Collections.Generic;
using System.Linq;
using DevExpress.Utils;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Grid;
using Logica.Reports.Common.WaitWindow;
using SoftServe.Reports.ThR.SummaryReport.DataProvider;
using SoftServe.Reports.ThR.SummaryReport.Utility;
using DataTable = System.Data.DataTable;

namespace SoftServe.Reports.ThR.SummaryReport.UserControls
{
    public partial class SummaryReport : XtraUserControl, IReportControl
    {
        private readonly LabelControl[] labelControlsValues;

        public SummaryReport()
        {
            InitializeComponent();

            labelControlsValues = new[]
                                      {
                                          lblIndustryVolume, lblPocSample, lblAveragePOCVolume
                                      };
        }

        public void LoadData(SettingsSnapshot settings)
        {
            DataTable summaryReport;
            gridView.Columns.Clear();
            WaitManager.StartWait();
            try
            {
                summaryReport = DataAccessProvider.GetSummaryReport(settings, true);
            }
            finally
            {
                WaitManager.StopWait();
            }

            var dataRows = summaryReport.Select("isOut = 1");
            foreach (var dataRow in dataRows)
            {
                switch ((int)dataRow["id"])
                {
                    case 1:
                        lblIndustryVolume.Text = dataRow[4].ToString();
                        break;
                    case 2:
                        lblPocSample.Text = String.Format("{0:G0}", dataRow[4]);
                        break;
                    case 3:
                        lblAveragePOCVolume.Text = dataRow[4].ToString();
                        break;
                }
                summaryReport.Rows.Remove(dataRow);
            }
            gridControl.DataSource = summaryReport;


            foreach (var column in gridView.Columns.Cast<GridColumn>().Take(2).ToArray())
            {
                column.Visible = false;
            }

            foreach (GridColumn column in gridView.Columns)
            {
                if (column.Visible)
                {
                    if (column.VisibleIndex == 0)
                        column.Width = 150;
                    else if (column.VisibleIndex == 1)
                        column.Width = 100;
                    else
                        column.Width = 72;

                    if (column.VisibleIndex < 2)
                    {
                        column.Caption = @" ";
                    }

                    column.AppearanceHeader.TextOptions.WordWrap = WordWrap.Wrap;
                    column.AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Center;
                }
            }

            //gridView.BestFitColumns();
        }

        public GridColumn[] GetColumnsForExport(int maxColumnCount)
        {
            GridColumn[] bandedGridColumns = gridView.Columns.Cast<GridColumn>().Where(c => c.Visible).OrderBy(c => c.VisibleIndex).Take(maxColumnCount).ToArray();
            return bandedGridColumns;
        }

        public GridView View
        {
            get { return gridView; }
        }

        public List<string> GetLabels()
        {
            List<string> labels = new List<string>();

            for (int i = 0; i < labelControlsValues.Length; i++)
            {
                labels.Add(labelControlsValues[i].Text);
            }

            return labels;
        }
    }
}
