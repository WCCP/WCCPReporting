﻿using System;
using System.Collections;
using System.Globalization;
using System.IO;
using System.Reflection;
using System.Resources;
using System.Windows.Forms;
using DevExpress.Utils;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraPrinting;
using DevExpress.XtraPrintingLinks;
using DevExpress.XtraTab;
using Logica.Reports.BaseReportControl;
using Logica.Reports.BaseReportControl.Utility;
using Logica.Reports.Common;
using Logica.Reports.Common.WaitWindow;
using Logica.Reports.DataAccess;

using SoftServe.Reports.ThR.SummaryReport.DataProvider;
using SoftServe.Reports.ThR.SummaryReport.Forms;
using SoftServe.Reports.ThR.SummaryReport.Properties;
using SoftServe.Reports.ThR.SummaryReport.Utility;
using SoftServe.Reports.ThR.SummaryReport.Utility.WCG;

using Logica.Reports.BaseReportControl.CommonFunctionality;


namespace WccpReporting
{
    public partial class WccpUIControl : XtraUserControl
    {
        #region Fields

        private readonly bool _isLkpPriceUnitUpdate;
        private readonly bool _isM2;

        #endregion

        #region Constructors

        public WccpUIControl()
        {
            InitializeComponent();
            /*
            btnExportToExcel.Tag = btnExportToExcel.Tag = ExportToType.Xls;
            btnExportToHtml.Tag = btnExportToHtml.Tag = ExportToType.Html;
            btnExportToMht.Tag = btnExportToMht.Tag = ExportToType.Mht;
            btnExportToPdf.Tag = btnExportToPdf.Tag = ExportToType.Pdf;
            btnExportToRtf.Tag = btnExportToRtf.Tag = ExportToType.Rtf;
            btnExportToText.Tag = btnExportToText.Tag = ExportToType.Txt;
            btnExportToImage.Tag = btnExportToImage.Tag = ExportToType.Bmp;
            btnExportToCsv.Tag = btnExportToCsv.Tag = ExportToType.Csv;
            btnExportToXlsx.Tag = btnExportToXlsx.Tag = ExportToType.Xlsx;
            */

            _isM2 = UserType.Current.IsM2;
            try
            {
                _isLkpPriceUnitUpdate = true;
                var priceUnits = DataAccessProvider.GetPriceUnits();
                lkpPriceUnit.Properties.DataSource = priceUnits;
                lkpPriceUnit.EditValue = priceUnits.Rows[0][lkpPriceUnit.Properties.ValueMember];
                lkpPriceUnit.Visible = !_isM2;
                lblPriceUnit.Visible = !_isM2;
            }
            finally
            {
                _isLkpPriceUnitUpdate = false;
            }
            barButtonItemSaveFilter.Visibility = _isM2 ? BarItemVisibility.Never : BarItemVisibility.Always;
            Settings = SettingsForm.Settings;
            ShowReport();
        }

        #endregion

        #region Instance Properties

        private string LastProcessedFilter { get; set; }
        private SettingsSnapshot Settings { get; set; }

        #endregion

        #region Instance Methods

        private void CallSettingsForm()
        {
            if (SettingsForm.ShowDialog() != DialogResult.OK) return;
            Settings = SettingsForm.Settings;
            ShowReport();
        }

        private void CreateFilterReport()
        {
            Settings.SelectedPOC = dataReport.SelectedPOC;
            LastProcessedFilter = dataReport.CurrentFilter;
            filterReport.LoadData(Settings);
            filterReport.CurrentFilter = dataReport.CurrentFilter;
        }

        private CompositeLink PrepareCompositeLink()
        {
            CompositeLink compositeLink = new CompositeLink(new PrintingSystem());
            //foreach (PrintableComponentLink printableComponentLink in smashReport1.PrintableComponents)
            //{
            //  compositeLink.Links.Add(printableComponentLink);
            //}
            //compositeLink.PaperKind = PaperKind.A4Rotated;
            //PageHeaderFooter phf = compositeLink.PageHeaderFooter as PageHeaderFooter;
            //phf.Header.Content.Clear();
            //phf.Header.Content.AddRange(new[] { String.Empty, ((ComboBoxItem)barEditItem1.EditValue).Text, String.Empty });
            //phf.Header.Font = new Font(phf.Header.Font.FontFamily, phf.Header.Font.Size + 2, FontStyle.Bold);
            //phf.Header.LineAlignment = BrickAlignment.Center;

            compositeLink.CreateDocument();
            return compositeLink;
        }



        //private Range PrepareFilteredSheet(Worksheet sheet)
        //{
        //    var startRange = sheet.Range["A28"];
        //    ExcelExporter.ApplyCellValue(startRange, "FILTER REPORT", 14, true);
        //    AddGradientLine(sheet, startRange);
        //    return startRange.Offset[2];
        //}

        //private Range PrepareSummarySheet(Worksheet sheet)
        //{
        //    sheet.Name = tpSummaryReport.Text;

        //    var startRange = sheet.Range["A1"];
        //    ExcelExporter.ApplyCellValue(startRange, "SUMMARY REPORT", 14, true);


        //    AddGradientLine(sheet, startRange);

        //    var range = startRange.Offset[1].Resize[3];
        //    ExcelExporter.ApplyCellValue(range, new[,]
        //                                      {
        //                                          {lblWave.Text},
        //                                          {lblChannel.Text},
        //                                          {lblRegion.Text}
        //                                      }, 10, true);
        //    range.HorizontalAlignment = XlHAlign.xlHAlignRight;

        //    range = startRange.Offset[1, 1].Resize[3];
        //    ExcelExporter.ApplyCellValue(range, new[,]
        //                                      {
        //                                          {txtWave.Text},
        //                                          {txtChannel.Text},
        //                                          {txtRegion.Text}
        //                                      }, 10);
        //    range.HorizontalAlignment = XlHAlign.xlHAlignLeft;

        //    string d = Settings.FilterName;

        //    return startRange.Offset[4];
        //}

        private string SelectFilePath(ExportToType type)
        {
            String res = String.Empty;
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Title = "Сохранить";
            sfd.InitialDirectory = Assembly.GetExecutingAssembly().Location;
            sfd.FileName = "Report";
            sfd.Filter = String.Format("(*.{0})|*.{0}", type.ToString().ToLower());
            sfd.FilterIndex = 1;
            sfd.OverwritePrompt = true;
            sfd.RestoreDirectory = true;

            if (sfd.ShowDialog() != DialogResult.Cancel)
            {
                res = sfd.FileName;
            }
            return res;
        }

        private void ShowReport(bool keepCurrentFilter = false)
        {
            Settings.IsBySKU = (bool)lkpPriceUnit.EditValue;
            Settings.IsM2 = _isM2;
            txtWave.EditValue = Settings.WaveName;
            txtChannel.EditValue = Settings.ChannelName;
            txtRegion.EditValue = Settings.RegionsDescriptionForDisplay;
            txtFilterName.EditValue = Settings.FilterName;

            if (DataAccessLayer.IsLDB)
            {
                WaitManager.StartWait();
                try
                {
                    bool lResult = DataAccessProvider.PrepareM2Data(Settings);
                    if (!lResult)
                        return;
                }
                finally
                {
                    WaitManager.StopWait();
                }
            }
            dataReport.LoadData(Settings);
            if (keepCurrentFilter)
                dataReport.CurrentFilter = LastProcessedFilter;

            CreateFilterReport();
            summaryReport.LoadData(Settings);
        }

        #endregion

        #region Event Handling

        private void barButtonItemExport_ItemClick(object sender, ItemClickEventArgs e)
        {
            string pathToFile = SelectFilePath(ExportToType.Xlsx);

            if (string.IsNullOrEmpty(pathToFile))
            {
                return;
            }

            try
            {
                WaitManager.StartWait();
                CreateXslxDocument(pathToFile);
            }
            catch (Exception ex)
            {
                ErrorManager.ShowErrorBox("Ошибка при экспорте файла");
            }
            finally
            {
                WaitManager.StopWait();
            }
        }

        private void barButtonItemFilterManage_ItemClick(object sender, ItemClickEventArgs e)
        {
            var form = new SelectFilterForm();
            if (form.ShowDialog() == DialogResult.OK)
            {
                Settings.Filter = form.CurrentFilter;
                txtFilterName.EditValue = Settings.FilterName;
                dataReport.CurrentFilter = Settings.Filter.Filter_text;
                CreateFilterReport();
            }
        }

        private void barButtonItemManageGroups_ItemClick(object sender, ItemClickEventArgs e)
        {
            var form = new SelectGroupsForm();
            form.Groups = dataReport.Groups;
            if (form.ShowDialog() == DialogResult.OK)
            {
                dataReport.Groups = form.Groups;
            }
        }

        private void btnPrint_ItemClick(object sender, ItemClickEventArgs e)
        {
            PrepareCompositeLink().PrintingSystem.PreviewFormEx.Show();
        }

        private void btnRefresh_ItemClick(object sender, ItemClickEventArgs e)
        {
            ShowReport();
        }

        private void btnSettings_ItemClick(object sender, ItemClickEventArgs e)
        {
            CallSettingsForm();
        }

        private void lkpPriceUnit_EditValueChanged(object sender, EventArgs e)
        {
            if (_isLkpPriceUnitUpdate) return;
            ShowReport(true);
        }

        private void xtraTabControl_SelectedPageChanged(object sender, TabPageChangedEventArgs e)
        {
            if (e.Page == tpFilterReport && dataReport.CurrentFilter != LastProcessedFilter)
            {
                CreateFilterReport();
            }
        }

        #endregion

        #region Class Properties

        public static SettingsForm SettingsForm { get; set; }

        #endregion

        private void barButtonItemSaveFilter_ItemClick(object sender, ItemClickEventArgs e)
        {
            SaveFilterForm lSaveFilterForm = new SaveFilterForm();
            string lFilterText = CriteriaToWhereClauseHelper.GetMsSqlWhere(dataReport.CurrentFilter);

            lSaveFilterForm.Filter = new FilterInfo
            {
                //Filter_ID = null,
                Filter_Name = Settings.FilterName,
                Filter_text = lFilterText,
                Filter_date = DateTime.Now,
                User_ID = null,
                UserName = null,
                Wave_ID = Settings.WaveId,
                WaveName = Settings.WaveName,
                Regions = Settings.RegionsDescriptionForDisplay,
                Channel = Settings.ChannelName
            };

            if (lSaveFilterForm.ShowDialog() == DialogResult.OK)
                XtraMessageBox.Show("Фильтр был успешно сохранен", "Сохранение", MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
        }

        private void toolTipRegions_GetActiveObjectInfo(object sender, DevExpress.Utils.ToolTipControllerGetActiveObjectInfoEventArgs e)
        {
            if (e.SelectedControl != txtRegion) return;

            object o = 1;
            string text = txtRegion.Text;

            if (string.IsNullOrEmpty(text))
                return;

            ToolTipControlInfo info = null;
            info = new ToolTipControlInfo(o, text);

            if (info != null)
                e.Info = info;
        }

        private void CreateXslxDocument(string filePath)
        {
            WriteTemplate(filePath);

            ExcelExporter exporter = new ExcelExporter(filePath, Settings);
            exporter.ExportSummaryTab(summaryReport.View, summaryReport.GetColumnsForExport(290), summaryReport.GetLabels());
            exporter.ExportFilterTab(filterReport.View, filterReport.GetColumnsForExport(290), filterReport.GetLabels());
            exporter.ExportDataTab(dataReport.View, dataReport.GetColumnsForExport(290));
        }

        private void WriteTemplate(string path)
        {
            ResourceManager res = new ResourceManager(Resources.ResourceManager.BaseName, Assembly.GetExecutingAssembly());

            using (ResourceSet lResourceSet = res.GetResourceSet(CultureInfo.InvariantCulture, true, false))
            {
                if (null != lResourceSet)
                {
                    object lValue = res.GetObject("TRSummaryReport_template");

                    if (File.Exists(path))
                    {
                        File.Delete(path);
                    }

                    WriteFile(path, (byte[])lValue);
                }
            }
            res.ReleaseAllResources();
        }

        private void WriteFile(string fileName, byte[] fileContent)
        {
            FileStream fs = new FileStream(fileName, FileMode.OpenOrCreate, FileAccess.Write, FileShare.None);
            fs.Write(fileContent, 0, fileContent.Length);
            fs.Flush();
            fs.Close();
            fs.Dispose();
        }

    }
}