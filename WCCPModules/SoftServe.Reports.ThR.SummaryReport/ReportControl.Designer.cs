﻿using Logica.Reports.BaseReportControl;
namespace WccpReporting
{
    partial class WccpUIControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(WccpUIControl));
            this.barMenuManager = new DevExpress.XtraBars.BarManager(this.components);
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.btnRefresh = new DevExpress.XtraBars.BarButtonItem();
            this.btnSettings = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonExportToExcel = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemManageGroups = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemSaveFilter = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemFilterManage = new DevExpress.XtraBars.BarButtonItem();
            this.standaloneBarDockControl1 = new DevExpress.XtraBars.StandaloneBarDockControl();
            this.barAndDockingController1 = new DevExpress.XtraBars.BarAndDockingController(this.components);
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.btnPrint = new DevExpress.XtraBars.BarButtonItem();
            this.imCollection = new DevExpress.Utils.ImageCollection(this.components);
            this.repositoryItemComboBox1 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.panelControlHeader = new DevExpress.XtraEditors.PanelControl();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.panelControlInformation = new DevExpress.XtraEditors.PanelControl();
            this.lkpPriceUnit = new DevExpress.XtraEditors.LookUpEdit();
            this.txtFilterName = new DevExpress.XtraEditors.TextEdit();
            this.txtRegion = new DevExpress.XtraEditors.TextEdit();
            this.toolTipRegions = new DevExpress.Utils.ToolTipController(this.components);
            this.txtChannel = new DevExpress.XtraEditors.TextEdit();
            this.txtWave = new DevExpress.XtraEditors.TextEdit();
            this.lblPriceUnit = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.lblRegion = new DevExpress.XtraEditors.LabelControl();
            this.lblChannel = new DevExpress.XtraEditors.LabelControl();
            this.lblWave = new DevExpress.XtraEditors.LabelControl();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.lblHeader = new DevExpress.XtraLayout.SimpleLabelItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.panelControlTabs = new DevExpress.XtraEditors.PanelControl();
            this.xtraTabControl = new DevExpress.XtraTab.XtraTabControl();
            this.tpSummaryReport = new DevExpress.XtraTab.XtraTabPage();
            this.summaryReport = new SoftServe.Reports.ThR.SummaryReport.UserControls.SummaryReport();
            this.tpFilterReport = new DevExpress.XtraTab.XtraTabPage();
            this.filterReport = new SoftServe.Reports.ThR.SummaryReport.UserControls.FilterReport();
            this.tpData = new DevExpress.XtraTab.XtraTabPage();
            this.dataReport = new SoftServe.Reports.ThR.SummaryReport.UserControls.DataReport();
            this.barButtonItemExport = new DevExpress.XtraBars.BarButtonItem();
            ((System.ComponentModel.ISupportInitialize)(this.barMenuManager)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barAndDockingController1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imCollection)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControlHeader)).BeginInit();
            this.panelControlHeader.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControlInformation)).BeginInit();
            this.panelControlInformation.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lkpPriceUnit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFilterName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRegion.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtChannel.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWave.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblHeader)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControlTabs)).BeginInit();
            this.panelControlTabs.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl)).BeginInit();
            this.xtraTabControl.SuspendLayout();
            this.tpSummaryReport.SuspendLayout();
            this.tpFilterReport.SuspendLayout();
            this.tpData.SuspendLayout();
            this.SuspendLayout();
            // 
            // barMenuManager
            // 
            this.barMenuManager.AllowCustomization = false;
            this.barMenuManager.AllowMoveBarOnToolbar = false;
            this.barMenuManager.AllowQuickCustomization = false;
            this.barMenuManager.AllowShowToolbarsPopup = false;
            this.barMenuManager.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1});
            this.barMenuManager.Controller = this.barAndDockingController1;
            this.barMenuManager.DockControls.Add(this.barDockControlTop);
            this.barMenuManager.DockControls.Add(this.barDockControlBottom);
            this.barMenuManager.DockControls.Add(this.barDockControlLeft);
            this.barMenuManager.DockControls.Add(this.barDockControlRight);
            this.barMenuManager.DockControls.Add(this.standaloneBarDockControl1);
            this.barMenuManager.Form = this;
            this.barMenuManager.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.btnSettings,
            this.btnPrint,
            this.btnRefresh,
            this.barButtonItemManageGroups,
            this.barButtonItemSaveFilter,
            this.barButtonItemFilterManage,
            this.barButtonExportToExcel,
            this.barButtonItemExport});
            this.barMenuManager.LargeImages = this.imCollection;
            this.barMenuManager.MaxItemId = 49;
            this.barMenuManager.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemComboBox1});
            // 
            // bar1
            // 
            this.bar1.BarName = "menuBar";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Standalone;
            this.bar1.FloatLocation = new System.Drawing.Point(74, 165);
            this.bar1.FloatSize = new System.Drawing.Size(48, 32);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btnRefresh, "", true, true, true, 0, null, DevExpress.XtraBars.BarItemPaintStyle.Standard),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnSettings),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonExportToExcel),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItemManageGroups, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItemSaveFilter),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItemFilterManage),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItemExport)});
            this.bar1.OptionsBar.AllowQuickCustomization = false;
            this.bar1.OptionsBar.DisableClose = true;
            this.bar1.OptionsBar.MultiLine = true;
            this.bar1.StandaloneBarDockControl = this.standaloneBarDockControl1;
            this.bar1.Text = "menuBar";
            // 
            // btnRefresh
            // 
            this.btnRefresh.Caption = "Обновить";
            this.btnRefresh.Hint = "Обновить";
            this.btnRefresh.Id = 6;
            this.btnRefresh.LargeImageIndex = 0;
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnRefresh_ItemClick);
            // 
            // btnSettings
            // 
            this.btnSettings.Caption = "Параметры отчета";
            this.btnSettings.Hint = "Параметры отчета";
            this.btnSettings.Id = 3;
            this.btnSettings.LargeGlyph = global::SoftServe.Reports.ThR.SummaryReport.Properties.Resources.ReportSettings_24;
            this.btnSettings.Name = "btnSettings";
            this.btnSettings.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnSettings_ItemClick);
            // 
            // barButtonItemManageGroups
            // 
            this.barButtonItemManageGroups.Caption = "Скрыть/показать группы колонок";
            this.barButtonItemManageGroups.Hint = "Скрыть/показать группы колонок";
            this.barButtonItemManageGroups.Id = 44;
            this.barButtonItemManageGroups.LargeGlyph = global::SoftServe.Reports.ThR.SummaryReport.Properties.Resources.ShowHideGroups_24;
            this.barButtonItemManageGroups.Name = "barButtonItemManageGroups";
            this.barButtonItemManageGroups.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemManageGroups_ItemClick);
            // 
            // barButtonItemSaveFilter
            // 
            this.barButtonItemSaveFilter.Caption = "Сохранить фильтр...";
            this.barButtonItemSaveFilter.Hint = "Сохранить фильтр...";
            this.barButtonItemSaveFilter.Id = 45;
            this.barButtonItemSaveFilter.LargeGlyph = global::SoftServe.Reports.ThR.SummaryReport.Properties.Resources.FilterSave_24;
            this.barButtonItemSaveFilter.Name = "barButtonItemSaveFilter";
            this.barButtonItemSaveFilter.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemSaveFilter_ItemClick);
            // 
            // barButtonItemFilterManage
            // 
            this.barButtonItemFilterManage.Caption = "Выбрать фильтр...";
            this.barButtonItemFilterManage.Hint = "Выбрать фильтр...";
            this.barButtonItemFilterManage.Id = 46;
            this.barButtonItemFilterManage.LargeGlyph = global::SoftServe.Reports.ThR.SummaryReport.Properties.Resources.FiltersManage_24;
            this.barButtonItemFilterManage.Name = "barButtonItemFilterManage";
            this.barButtonItemFilterManage.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemFilterManage_ItemClick);
            // 
            // standaloneBarDockControl1
            // 
            this.standaloneBarDockControl1.AutoSize = true;
            this.standaloneBarDockControl1.CausesValidation = false;
            this.standaloneBarDockControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.standaloneBarDockControl1.Location = new System.Drawing.Point(0, 0);
            this.standaloneBarDockControl1.Name = "standaloneBarDockControl1";
            this.standaloneBarDockControl1.Size = new System.Drawing.Size(956, 32);
            this.standaloneBarDockControl1.Text = "standaloneBarDockControl1";
            // 
            // barAndDockingController1
            // 
            this.barAndDockingController1.AppearancesBar.ItemsFont = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.barAndDockingController1.PaintStyleName = "WindowsXP";
            this.barAndDockingController1.PropertiesBar.AllowLinkLighting = false;
            this.barAndDockingController1.PropertiesBar.DefaultGlyphSize = new System.Drawing.Size(16, 16);
            this.barAndDockingController1.PropertiesBar.DefaultLargeGlyphSize = new System.Drawing.Size(32, 32);
            this.barAndDockingController1.PropertiesBar.LargeIcons = true;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(956, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 476);
            this.barDockControlBottom.Size = new System.Drawing.Size(956, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 0);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 476);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(956, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 476);
            // 
            // btnPrint
            // 
            this.btnPrint.Caption = "Печать";
            this.btnPrint.Id = 5;
            this.btnPrint.LargeImageIndex = 19;
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnPrint_ItemClick);
            // 
            // imCollection
            // 
            this.imCollection.ImageSize = new System.Drawing.Size(24, 24);
            this.imCollection.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imCollection.ImageStream")));
            this.imCollection.Images.SetKeyName(0, "refresh_24.png");
            this.imCollection.Images.SetKeyName(1, "briefcase_prev_24.png");
            this.imCollection.Images.SetKeyName(2, "del_24.png");
            this.imCollection.Images.SetKeyName(3, "Excel_42.bmp");
            this.imCollection.Images.SetKeyName(4, "ExcelAll_42.bmp");
            this.imCollection.Images.SetKeyName(5, "print_24.png");
            this.imCollection.Images.SetKeyName(6, "briefcase_ok_24.png");
            this.imCollection.Images.SetKeyName(7, "briefcase_save_24.png");
            this.imCollection.Images.SetKeyName(15, "xlsx.png");
            this.imCollection.Images.SetKeyName(16, "bmp.PNG");
            this.imCollection.Images.SetKeyName(17, "csv.PNG");
            this.imCollection.Images.SetKeyName(18, "copy_prev_24.png");
            this.imCollection.Images.SetKeyName(19, "doc_prev_24.png");
            // 
            // repositoryItemComboBox1
            // 
            this.repositoryItemComboBox1.AutoHeight = false;
            this.repositoryItemComboBox1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox1.Name = "repositoryItemComboBox1";
            this.repositoryItemComboBox1.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            // 
            // panelControlHeader
            // 
            this.panelControlHeader.Controls.Add(this.layoutControl1);
            this.panelControlHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControlHeader.Location = new System.Drawing.Point(0, 32);
            this.panelControlHeader.Name = "panelControlHeader";
            this.panelControlHeader.Size = new System.Drawing.Size(956, 89);
            this.panelControlHeader.TabIndex = 5;
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.panelControlInformation);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(2, 2);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(952, 85);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // panelControlInformation
            // 
            this.panelControlInformation.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControlInformation.Controls.Add(this.lkpPriceUnit);
            this.panelControlInformation.Controls.Add(this.txtFilterName);
            this.panelControlInformation.Controls.Add(this.txtRegion);
            this.panelControlInformation.Controls.Add(this.txtChannel);
            this.panelControlInformation.Controls.Add(this.txtWave);
            this.panelControlInformation.Controls.Add(this.lblPriceUnit);
            this.panelControlInformation.Controls.Add(this.labelControl4);
            this.panelControlInformation.Controls.Add(this.lblRegion);
            this.panelControlInformation.Controls.Add(this.lblChannel);
            this.panelControlInformation.Controls.Add(this.lblWave);
            this.panelControlInformation.Location = new System.Drawing.Point(12, 29);
            this.panelControlInformation.Name = "panelControlInformation";
            this.panelControlInformation.Size = new System.Drawing.Size(928, 54);
            this.panelControlInformation.TabIndex = 4;
            // 
            // lkpPriceUnit
            // 
            this.lkpPriceUnit.Location = new System.Drawing.Point(803, 5);
            this.lkpPriceUnit.MenuManager = this.barMenuManager;
            this.lkpPriceUnit.Name = "lkpPriceUnit";
            this.lkpPriceUnit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lkpPriceUnit.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("UnitName", "UnitName")});
            this.lkpPriceUnit.Properties.DisplayMember = "UnitName";
            this.lkpPriceUnit.Properties.ShowFooter = false;
            this.lkpPriceUnit.Properties.ShowHeader = false;
            this.lkpPriceUnit.Properties.ValueMember = "UnitId";
            this.lkpPriceUnit.Size = new System.Drawing.Size(122, 20);
            this.lkpPriceUnit.TabIndex = 9;
            this.lkpPriceUnit.EditValueChanged += new System.EventHandler(this.lkpPriceUnit_EditValueChanged);
            // 
            // txtFilterName
            // 
            this.txtFilterName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtFilterName.Location = new System.Drawing.Point(108, 29);
            this.txtFilterName.MenuManager = this.barMenuManager;
            this.txtFilterName.Name = "txtFilterName";
            this.txtFilterName.Properties.ReadOnly = true;
            this.txtFilterName.Size = new System.Drawing.Size(817, 20);
            this.txtFilterName.TabIndex = 8;
            // 
            // txtRegion
            // 
            this.txtRegion.Location = new System.Drawing.Point(390, 5);
            this.txtRegion.MenuManager = this.barMenuManager;
            this.txtRegion.Name = "txtRegion";
            this.txtRegion.Properties.ReadOnly = true;
            this.txtRegion.Size = new System.Drawing.Size(350, 20);
            this.txtRegion.TabIndex = 7;
            this.txtRegion.ToolTipController = this.toolTipRegions;
            // 
            // toolTipRegions
            // 
            this.toolTipRegions.GetActiveObjectInfo += new DevExpress.Utils.ToolTipControllerGetActiveObjectInfoEventHandler(this.toolTipRegions_GetActiveObjectInfo);
            // 
            // txtChannel
            // 
            this.txtChannel.Location = new System.Drawing.Point(214, 5);
            this.txtChannel.MenuManager = this.barMenuManager;
            this.txtChannel.Name = "txtChannel";
            this.txtChannel.Properties.ReadOnly = true;
            this.txtChannel.Size = new System.Drawing.Size(100, 20);
            this.txtChannel.TabIndex = 6;
            // 
            // txtWave
            // 
            this.txtWave.Location = new System.Drawing.Point(45, 5);
            this.txtWave.MenuManager = this.barMenuManager;
            this.txtWave.Name = "txtWave";
            this.txtWave.Properties.ReadOnly = true;
            this.txtWave.Size = new System.Drawing.Size(100, 20);
            this.txtWave.TabIndex = 5;
            // 
            // lblPriceUnit
            // 
            this.lblPriceUnit.Location = new System.Drawing.Point(758, 8);
            this.lblPriceUnit.Name = "lblPriceUnit";
            this.lblPriceUnit.Size = new System.Drawing.Size(39, 13);
            this.lblPriceUnit.TabIndex = 4;
            this.lblPriceUnit.Text = "Цена в:";
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(3, 32);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(99, 13);
            this.labelControl4.TabIndex = 3;
            this.labelControl4.Text = "Название фильтра:";
            // 
            // lblRegion
            // 
            this.lblRegion.Location = new System.Drawing.Point(345, 8);
            this.lblRegion.Name = "lblRegion";
            this.lblRegion.Size = new System.Drawing.Size(39, 13);
            this.lblRegion.TabIndex = 2;
            this.lblRegion.Text = "Регион:";
            // 
            // lblChannel
            // 
            this.lblChannel.Location = new System.Drawing.Point(173, 8);
            this.lblChannel.Name = "lblChannel";
            this.lblChannel.Size = new System.Drawing.Size(35, 13);
            this.lblChannel.TabIndex = 1;
            this.lblChannel.Text = "Канал:";
            // 
            // lblWave
            // 
            this.lblWave.Location = new System.Drawing.Point(5, 8);
            this.lblWave.Name = "lblWave";
            this.lblWave.Size = new System.Drawing.Size(34, 13);
            this.lblWave.TabIndex = 0;
            this.lblWave.Text = "Волна:";
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.lblHeader,
            this.layoutControlItem1});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(10, 10, 0, 0);
            this.layoutControlGroup1.Size = new System.Drawing.Size(952, 85);
            this.layoutControlGroup1.Text = "Root";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // lblHeader
            // 
            this.lblHeader.AllowHotTrack = false;
            this.lblHeader.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblHeader.AppearanceItemCaption.Options.UseFont = true;
            this.lblHeader.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lblHeader.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lblHeader.ControlAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblHeader.CustomizationFormText = "Summary Report";
            this.lblHeader.Location = new System.Drawing.Point(0, 0);
            this.lblHeader.Name = "lblHeader";
            this.lblHeader.Size = new System.Drawing.Size(932, 27);
            this.lblHeader.Text = "Summary Report";
            this.lblHeader.TextSize = new System.Drawing.Size(142, 23);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.panelControlInformation;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 27);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(932, 58);
            this.layoutControlItem1.Text = "layoutControlItem1";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
            // 
            // panelControlTabs
            // 
            this.panelControlTabs.Controls.Add(this.xtraTabControl);
            this.panelControlTabs.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControlTabs.Location = new System.Drawing.Point(0, 121);
            this.panelControlTabs.Name = "panelControlTabs";
            this.panelControlTabs.Size = new System.Drawing.Size(956, 355);
            this.panelControlTabs.TabIndex = 6;
            // 
            // xtraTabControl
            // 
            this.xtraTabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraTabControl.HeaderLocation = DevExpress.XtraTab.TabHeaderLocation.Bottom;
            this.xtraTabControl.Location = new System.Drawing.Point(2, 2);
            this.xtraTabControl.Name = "xtraTabControl";
            this.xtraTabControl.SelectedTabPage = this.tpSummaryReport;
            this.xtraTabControl.Size = new System.Drawing.Size(952, 351);
            this.xtraTabControl.TabIndex = 0;
            this.xtraTabControl.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.tpSummaryReport,
            this.tpFilterReport,
            this.tpData});
            this.xtraTabControl.SelectedPageChanged += new DevExpress.XtraTab.TabPageChangedEventHandler(this.xtraTabControl_SelectedPageChanged);
            // 
            // tpSummaryReport
            // 
            this.tpSummaryReport.Controls.Add(this.summaryReport);
            this.tpSummaryReport.Name = "tpSummaryReport";
            this.tpSummaryReport.Size = new System.Drawing.Size(946, 323);
            this.tpSummaryReport.Text = "Summary report";
            // 
            // summaryReport
            // 
            this.summaryReport.Dock = System.Windows.Forms.DockStyle.Fill;
            this.summaryReport.Location = new System.Drawing.Point(0, 0);
            this.summaryReport.Name = "summaryReport";
            this.summaryReport.Size = new System.Drawing.Size(946, 323);
            this.summaryReport.TabIndex = 0;
            // 
            // tpFilterReport
            // 
            this.tpFilterReport.Controls.Add(this.filterReport);
            this.tpFilterReport.Name = "tpFilterReport";
            this.tpFilterReport.Size = new System.Drawing.Size(946, 313);
            this.tpFilterReport.Text = "Filter report";
            // 
            // filterReport
            // 
            this.filterReport.CurrentFilter = null;
            this.filterReport.Dock = System.Windows.Forms.DockStyle.Fill;
            this.filterReport.Location = new System.Drawing.Point(0, 0);
            this.filterReport.Name = "filterReport";
            this.filterReport.Size = new System.Drawing.Size(946, 313);
            this.filterReport.TabIndex = 0;
            // 
            // tpData
            // 
            this.tpData.Controls.Add(this.dataReport);
            this.tpData.Name = "tpData";
            this.tpData.Size = new System.Drawing.Size(946, 313);
            this.tpData.Text = "Data";
            // 
            // dataReport
            // 
            this.dataReport.CurrentFilter = "";
            this.dataReport.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataReport.Groups = new SoftServe.Reports.ThR.SummaryReport.DataProvider.GroupVisibility[0];
            this.dataReport.Location = new System.Drawing.Point(0, 0);
            this.dataReport.Name = "dataReport";
            this.dataReport.Size = new System.Drawing.Size(946, 313);
            this.dataReport.TabIndex = 0;
            // 
            // barButtonItemExport
            // 
            this.barButtonItemExport.Caption = "Экспорт в xslx";
            this.barButtonItemExport.Hint = "Экспорт в xslx";
            this.barButtonItemExport.Id = 48;
            this.barButtonItemExport.LargeGlyph = global::SoftServe.Reports.ThR.SummaryReport.Properties.Resources.Excel_24;
            this.barButtonItemExport.Name = "barButtonItemExport";
            this.barButtonItemExport.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemExport_ItemClick);
            // 
            // WccpUIControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panelControlTabs);
            this.Controls.Add(this.panelControlHeader);
            this.Controls.Add(this.standaloneBarDockControl1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Name = "WccpUIControl";
            this.Size = new System.Drawing.Size(956, 476);
            ((System.ComponentModel.ISupportInitialize)(this.barMenuManager)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barAndDockingController1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imCollection)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControlHeader)).EndInit();
            this.panelControlHeader.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControlInformation)).EndInit();
            this.panelControlInformation.ResumeLayout(false);
            this.panelControlInformation.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lkpPriceUnit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFilterName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRegion.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtChannel.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWave.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblHeader)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControlTabs)).EndInit();
            this.panelControlTabs.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl)).EndInit();
            this.xtraTabControl.ResumeLayout(false);
            this.tpSummaryReport.ResumeLayout(false);
            this.tpFilterReport.ResumeLayout(false);
            this.tpData.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarManager barMenuManager;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.StandaloneBarDockControl standaloneBarDockControl1;
        private DevExpress.XtraBars.BarAndDockingController barAndDockingController1;
        private DevExpress.XtraBars.BarButtonItem btnSettings;
        private DevExpress.XtraBars.BarButtonItem btnRefresh;
        //private DevExpress.XtraBars.BarSubItem btnExportTo;
        //private DevExpress.XtraBars.BarButtonItem btnExportToExcel;
        //private DevExpress.XtraBars.BarButtonItem btnExportToPdf;
        //private DevExpress.XtraBars.BarButtonItem btnExportToHtml;
        //private DevExpress.XtraBars.BarButtonItem btnExportToMht;
        //private DevExpress.XtraBars.BarButtonItem btnExportToRtf;
        //private DevExpress.XtraBars.BarButtonItem btnExportToText;
        //private DevExpress.XtraBars.BarButtonItem btnExportToImage;
        //private DevExpress.XtraBars.BarButtonItem btnExportToCsv;
        //private DevExpress.XtraBars.BarButtonItem btnExportToXlsx;
        //private DevExpress.XtraBars.BarButtonItem barButtonItem1;
        public DevExpress.XtraBars.BarButtonItem btnPrint;
        //private DevExpress.XtraBars.BarStaticItem barStaticItem1;
        //private DevExpress.XtraBars.BarEditItem barEditItem1;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox1;
        private DevExpress.Utils.ImageCollection imCollection;
        private DevExpress.XtraEditors.PanelControl panelControlTabs;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl;
        private DevExpress.XtraTab.XtraTabPage tpSummaryReport;
        private DevExpress.XtraTab.XtraTabPage tpFilterReport;
        private DevExpress.XtraTab.XtraTabPage tpData;
        private DevExpress.XtraEditors.PanelControl panelControlHeader;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraEditors.PanelControl panelControlInformation;
        private DevExpress.XtraEditors.LookUpEdit lkpPriceUnit;
        private DevExpress.XtraEditors.TextEdit txtFilterName;
        private DevExpress.XtraEditors.TextEdit txtRegion;
        private DevExpress.XtraEditors.TextEdit txtChannel;
        private DevExpress.XtraEditors.TextEdit txtWave;
        private DevExpress.XtraEditors.LabelControl lblPriceUnit;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl lblRegion;
        private DevExpress.XtraEditors.LabelControl lblChannel;
        private DevExpress.XtraEditors.LabelControl lblWave;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.SimpleLabelItem lblHeader;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private SoftServe.Reports.ThR.SummaryReport.UserControls.SummaryReport summaryReport;
        private SoftServe.Reports.ThR.SummaryReport.UserControls.FilterReport filterReport;
        private SoftServe.Reports.ThR.SummaryReport.UserControls.DataReport dataReport;
        private DevExpress.XtraBars.BarButtonItem barButtonItemManageGroups;
        private DevExpress.XtraBars.BarButtonItem barButtonItemSaveFilter;
        private DevExpress.XtraBars.BarButtonItem barButtonItemFilterManage;
        private DevExpress.XtraBars.BarButtonItem barButtonExportToExcel;
        private DevExpress.Utils.ToolTipController toolTipRegions;
        private DevExpress.XtraBars.BarButtonItem barButtonItemExport;

    }
}
