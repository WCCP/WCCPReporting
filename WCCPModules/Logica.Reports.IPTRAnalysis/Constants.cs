﻿using Logica.Reports.Common;

namespace Logica.Reports.IPTRA
{
    /// <summary>
    /// 
    /// </summary>
    public static class Constants
    {
        // Setting Parameters
        public const string SETTING_PARAM_DATE = "@Date";
        public const string SETTING_PARAM_CHANNEL_TYPE_ID = "@ChanelType_id";
        public const string SETTING_PARAM_STAFF_LEVEL_ID = "@StaffLevel_ID";
        public const string SETTING_PARAM_STAFF_ID = "@Staff_ID";
        public const string SETTING_PARAM_EQUIPMENT_CLASS_ID = "@EquipmentClass_ID";
        public const string SETTING_PARAM_DEBUG = "@debug";

        // Field Names
        public const string FIELD_POC_ID = "ID";
        public const string FIELD_SUPERVISER_ID = "Supervisor_id";
        public const string FIELD_POC_NAME = "POCName";
        public const string FIELD_STAFF_TREE_PARENT_ID = "Parent_ID";
        public const string FIELD_STAFF_TREE_NAME = "Name";
        public const string FIELD_STAFF_TREE_ITEM_ID = "Item_ID";
        public const string FIELD_STAFF_TREE_LEVEL_ID = "StaffLevel_ID";
        public const string FIELD_STAFF_TREE_ID = "ID";
        public const string FIELD_CHANNEL_TYPE_ID = "ChanelType_id";
        public const string FIELD_CHANNEL_TYPE = "ChanelType";
        public const string FIELD_EQUIPMENT_CLASS_ID = "EquipmentClass_ID";
        public const string FIELD_EQUIPMENT_CLASS = "ClassName";
        public const string FIELD_REALLOCATION_POC_ID = "ID";

        // Field Prefixes
        public const string FIELD_PREFIX_EQUIPMENT = "EquipmentSize:";
        public const string FIELD_EQUIPMENT_SMALL = "Small";
        public const string FIELD_PREFIX_REALLOCATION_EQUIPMENT = "ReallocateEquipmentSize:";
        public const string FIELD_SUPERVISER = "Supervisor_name";
        public const string FIELD_MERCH = "MerchName";

        // Stored Procedure Names
        public const string SP_GET_ACTION_PLAN_SUMMARY = "spDW_IPTRA_GetActionPlanSummary";
        public const string SP_GET_SUPERVISORS_LIST = "spDW_SupervisorsGetList";
        public const string SP_GET_ACTION_LIST = "spDW_IPTRA_GetActionList";
        public const string SP_GET_STATUS_LIST = "spDW_IPTRA_GetStatusList";
        public const string SP_GET_REALLOCATION_POC_LIST = "spDW_IPTRA_GetReallocationPOCList";
        public const string SP_GET_ACCESS_LEVEL = "spDW_IPTRA_GetAccessLevel";
        public const string SP_SET_ACTION_PLAN_SUMMARY = "spDW_IPTRA_SetActionPlanSummary";
        public const string SP_GET_CHANNEL_TYPE_LIST = "spDW_ChanelTypeList";
        public const string SP_GET_EQUIPMENT_CLASS_LIST = "spDW_IPTR_EquipmentClassList";
        public const string SP_GET_STAFF_TREE = "spDW_StaffTree";

        // Stored Procedure Parameters
        public const string SP_PARAM_REALLOCATE_EQUIPMENTS = "@ReallocateEquipments";
        public const string SP_PARAM_REPORT_ID = "@ReportId";
        public const string SP_PARAM_CHANNEL_TYPE_ID = "@ChanelType_ID";
        public const string SP_PARAM_ACCESS_LEVEL_DATE = "@Date";
    }

    /// <summary>
    /// 
    /// </summary>
    public enum Action
    {
        /// <summary>
        /// 
        /// </summary>
        Unknown = 0,
        /// <summary>
        /// 
        /// </summary>
        RelocateToPoc = 1,
        /// <summary>
        /// 
        /// </summary>
        RelocateToPocAndStock = 2,
        /// <summary>
        /// 
        /// </summary>
        ReplaceWithOneDoorFridge = 3,
        /// <summary>
        /// 
        /// </summary>
        IncreasePerfomance = 4,
        /// <summary>
        /// 
        /// </summary>
        LeaveAndRepair = 5,
        /// <summary>
        /// 
        /// </summary>
        LeaveAndDZ = 6
    }

    /// <summary>
    /// 
    /// </summary>
    public enum ActionStatus
    {
        /// <summary>
        /// 
        /// </summary>
        InProgress = 1,
        /// <summary>
        /// 
        /// </summary>
        Delayed = 2,
        /// <summary>
        /// 
        /// </summary>
        Complete = 3
    }

    /// <summary>
    /// 
    /// </summary>
    public enum AccessLevel
    {
        /// <summary>
        /// 
        /// </summary>
        None = 1,
        /// <summary>
        /// 
        /// </summary>
        M2 = 2,
        /// <summary>
        /// 
        /// </summary>
        M3 = 3,
        /// <summary>
        /// 
        /// </summary>
        M4 = 4
    }

    /// <summary>
    /// 
    /// </summary>
    public enum ApproveStatus
    {
        /// <summary>
        /// 
        /// </summary>
        Error,
        /// <summary>
        /// 
        /// </summary>
        CanApprove,
        /// <summary>
        /// 
        /// </summary>
        ActionIsNotSet,
        /// <summary>
        /// 
        /// </summary>
        ResponsibleIsNotSet
    }
}
