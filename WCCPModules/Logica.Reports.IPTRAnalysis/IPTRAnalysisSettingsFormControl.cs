﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using Logica.Reports.BaseReportControl;
using Logica.Reports;

namespace WccpReporting
{
    public partial class WccpUIControl : BaseReportUserControl
    {
        private const int _currentReportId = 117;
        IPTRAnalysisSettingsForm setForm;

        public WccpUIControl() : base(_currentReportId)
        {
            InitializeComponent();
            this.SettingsFormClick += new MenuClickHandler(WccpUIControl_SettingsFormClick);
            setForm = new IPTRAnalysisSettingsForm(HasWriteAccess, Report.Tabs[0].Id);
            ShowSettingsForm();
        }

        private void WccpUIControl_SettingsFormClick(object sender, DevExpress.XtraTab.XtraTabPage selectedPage)
        {
            ShowSettingsForm();
        }

        private void ShowSettingsForm()
        {
            if (setForm.ShowDialog() == DialogResult.OK)
            {
                this.UpdateSheets(new List<SheetParamCollection>() { setForm.ParameterCollection });
            }
        }
    }
}
