﻿using System;
using System.Collections.Generic;
using System.Text;
using DevExpress.XtraTab;
using Logica.Reports.BaseReportControl;
using Logica.Reports.Common.WaitWindow;
using System.Windows.Forms;
using System.Data.SqlClient;
using DevExpress.XtraEditors;
using System.Data;
using DevExpress.XtraPrintingLinks;
using DevExpress.XtraPrinting;

namespace Logica.Reports.IPTRA
{
    /// <summary>
    /// 
    /// </summary>
    public class ActionPlanTab : XtraTabPage, IPrint
    {
        private static Guid tabId = Guid.NewGuid();

        private BaseReportUserControl parent;
        private ActionPlanControl actionPlanControl;

        private int _reportId;
        private SheetParamCollection sheetSettings;


        /// <summary>
        /// Gets the tab id.
        /// </summary>
        /// <value>The tab id.</value>
        public static Guid TabId
        {
            get{ return tabId; }
        }

        /// <summary>
        /// Gets the name of the tab.
        /// </summary>
        /// <value>The name of the tab.</value>
        public static string TabName
        {
            get { return Resource.ActionPlan; }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ActionPlanTab"/> class.
        /// </summary>
        /// <param name="parent">The parent.</param>
        public ActionPlanTab(BaseReportUserControl parent, int reportId) 
        {
            if (!this.DesignMode && parent != null)
            {
                this.parent = parent;
                _reportId = reportId;
                Text = TabName;

                parent.RefreshClick += new BaseReportUserControl.MenuClickHandler(parent_RefreshClick);
                parent.SaveClick += new BaseReportUserControl.MenuClickHandler(parent_SaveClick);
                parent.ExportClick += new BaseReportUserControl.ExportMenuClickHandler(parent_ExportClick);
                parent.CloseClick += new BaseReportUserControl.MenuClickHandler(parent_CloseClick);
                parent.MenuButtonsRendering += parent_MenuButtonsRendering;

                actionPlanControl = new ActionPlanControl();
                actionPlanControl.Dock = DockStyle.Fill;
                Controls.Add(actionPlanControl);
            }
        }

        /// <summary>
        /// Parent_s the tool button visibility check.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="selectedPage">The selected page.</param>
        private void parent_MenuButtonsRendering(object sender, BaseReportUserControl.MenuButtonsRenderingEventArgs args)
        {
            if (this == args.SelectedPage)
            {
                args.ShowSaveBtn = true;
            }
        }

        /// <summary>
        /// Updates the sheet.
        /// </summary>
        /// <param name="settings">The settings.</param>
        public void UpdateSheet(List<SheetParamCollection> listSettings)
        {
            SheetParamCollection settings = listSettings.Find(p => p.TabId == ActionPlanTab.TabId);
            if (settings != null)
            {
                if (!parent.TabControl.TabPages.Contains(this))
                {
                    parent.TabControl.TabPages.Add(this);
                }
                UpdateData(settings, false);
            }
            else if (parent.TabControl.TabPages.Contains(this))
            {
                parent.TabControl.TabPages.Remove(this);
            }
        }

        /// <summary>
        /// Updates the data.
        /// </summary>
        /// <param name="parameters">The parameters.</param>
        /// <param name="isForce">if set to <c>true</c> [is force].</param>
        public void UpdateData(SheetParamCollection parameters, bool isForce)
        {
            if (parameters == null || (!isForce && CompareSettings(parameters)))
            {
                return;
            }

            sheetSettings = parameters;

            WaitManager.StartWait();
            parent.ConstructTemporaryData(parameters);

            LoadData();

            parent.DestructTemporaryData();
            WaitManager.StopWait();
        }

        /// <summary>
        /// Loads the settings.
        /// </summary>
        /// <returns></returns>
        private List<SqlParameter> LoadSettings()
        {
            List<SqlParameter> list = new List<SqlParameter>();
            AddSqlParameter(list, Constants.SETTING_PARAM_DATE);
            AddSqlParameter(list, Constants.SETTING_PARAM_CHANNEL_TYPE_ID);
            AddSqlParameter(list, Constants.SETTING_PARAM_STAFF_LEVEL_ID);
            AddSqlParameter(list, Constants.SETTING_PARAM_STAFF_ID);
            AddSqlParameter(list, Constants.SETTING_PARAM_EQUIPMENT_CLASS_ID);
            AddSqlParameter(list, Constants.SETTING_PARAM_DEBUG);
            return list;
        }

        /// <summary>
        /// Adds the SQL parameter.
        /// </summary>
        /// <param name="list">The list.</param>
        /// <param name="paramName">Name of the param.</param>
        private void AddSqlParameter(List<SqlParameter> list, string paramName)
        {
            if (list != null && sheetSettings != null)
            {
                if (sheetSettings.Exists(p => p.SqlParamName == paramName))
                {
                    list.Add(new SqlParameter(paramName, sheetSettings.Find(p => p.SqlParamName == paramName).Value));
                }
            }
        }

        /// <summary>
        /// Adds the SQL parameter.
        /// </summary>
        /// <param name="list">The list.</param>
        /// <param name="paramName">Name of the param.</param>
        /// <param name="value">The value.</param>
        private static void AddSqlParameter(List<SqlParameter> list, string paramName, object value)
        {
            if (list != null)
            {
                list.Add(new SqlParameter(paramName, value));
            }
        }

        /// <summary>
        /// Loads the data.
        /// </summary>
        private void LoadData()
        {
            List<SqlParameter> list = LoadSettings();
            if (list != null && list.Count > 0)
            {
                // main table
                DateTime dateTime = (DateTime)sheetSettings.Find(p => p.SqlParamName == Constants.SETTING_PARAM_DATE).Value;
                actionPlanControl.AccessLevel = DataProvider.GetAccessLevel(dateTime);
                DataTable actionPlanTable = DataProvider.GetActionPlanTable(list);
                actionPlanControl.ActionPlanTable = actionPlanTable;
                if (actionPlanTable != null && actionPlanTable.Rows.Count > 0)
                {
                    // other tables
                    actionPlanControl.EquipmentSizeTable = actionPlanControl.ActionPlanTable;
                    actionPlanControl.ReallocationPOCTable = DataProvider.ReallocationPOCTable;

                    // lookups
                    actionPlanControl.ActionListLookUp = DataProvider.ActionListLookUp;
                    actionPlanControl.ReallocationPOCTableLookUp = actionPlanControl.ReallocationPOCTable;
                    actionPlanControl.M2ListLokUp = DataProvider.M2ListLookUp(_reportId);
                    actionPlanControl.StatusListLookUp = DataProvider.StatusListLookUp;
                }
            }
        }

        /// <summary>
        /// Saves the changes.
        /// </summary>
        private void SaveChanges()
        {
            DataTable dataTable = (DataTable)actionPlanControl.ActionPlanTable;
            if (dataTable != null)
            {
                DataTable tableChanges = dataTable.GetChanges();
                if (tableChanges != null)
                {
                    foreach (DataRow row in tableChanges.Rows)
                    {
                        SaveRow(dataTable.Columns, row);
                    }

                    dataTable.AcceptChanges();

                    XtraMessageBox.Show(Resource.SaveSucceeded, Application.ProductName);
                }
            }
        }

        /// <summary>
        /// Saves the row.
        /// </summary>
        /// <param name="row">The row.</param>
        private static void SaveRow(DataColumnCollection columnCollection, DataRow row) {
            if (row != null && row.ItemArray.Length > 0) {
                List<SqlParameter> paramsList = new List<SqlParameter>();
                foreach (DataColumn column in columnCollection) {
                    if (IsModifyStoredProcParameter(column.ColumnName)) {
                        object lValue = row[column.ColumnName];
                        if ((column.ColumnName == "SupervisorID") && (lValue.Equals(DBNull.Value)))
                            lValue = row["Supervisor_id"];

                        AddSqlParameter(paramsList, column.ColumnName, lValue);
                    }
                }

                string xmlReallocateEquipment = BuildXmlParameter(row, Constants.FIELD_PREFIX_REALLOCATION_EQUIPMENT);
                AddSqlParameter(paramsList, Constants.SP_PARAM_REALLOCATE_EQUIPMENTS, xmlReallocateEquipment);

                DataProvider.UpdateActionPlanRecord(paramsList);
            }
        }

        /// <summary>
        /// Builds the XML parameter.
        /// </summary>
        /// <param name="row">The row.</param>
        /// <param name="columnPrefix">The column prefix.</param>
        /// <returns></returns>
        private static string BuildXmlParameter(DataRow row, string columnPrefix)
        {
            StringBuilder builder = new StringBuilder();
            builder.Append("<ROOT>");
            foreach (DataColumn column in row.Table.Columns)
            {
                if (column.ColumnName.StartsWith(columnPrefix)
                    && row[column.ColumnName] != null && row[column.ColumnName] != DBNull.Value)
                {
                    builder.Append("<EquipmentSize>");
                    builder.Append("<Name>" + column.ColumnName + "</Name>");
                    builder.Append("<Count>" + row[column.ColumnName].ToString() + "</Count>");
                    builder.Append("</EquipmentSize>");
                }
            }
            builder.Append("</ROOT>");
            return builder.ToString();
        }

        /// <summary>
        /// Determines whether [is modify stored proc parameter] [the specified column name].
        /// </summary>
        /// <param name="columnName">Name of the column.</param>
        /// <returns>
        /// 	<c>true</c> if [is modify stored proc parameter] [the specified column name]; otherwise, <c>false</c>.
        /// </returns>
        private static bool IsModifyStoredProcParameter(string columnName)
        {
            if (!string.IsNullOrEmpty(columnName))
            {
                return columnName != Constants.FIELD_POC_NAME
                       && !columnName.StartsWith(Constants.FIELD_PREFIX_EQUIPMENT)
                       && !columnName.StartsWith(Constants.FIELD_PREFIX_REALLOCATION_EQUIPMENT)
                       && columnName != Constants.FIELD_SUPERVISER
                       && columnName != Constants.FIELD_MERCH
                       && columnName != Constants.FIELD_SUPERVISER_ID;
            }
            return false;
        }

        /// <summary>
        /// Prepare container of elements for exporting
        /// </summary>
        /// <returns></returns>
        public CompositeLink PrepareCompositeLink()
        {
            CompositeLink compositeLink = new CompositeLink(new PrintingSystem());
            compositeLink.Links.Add(new PrintableComponentLink() { Component = actionPlanControl.GridControl });
            compositeLink.CreateDocument();
            return compositeLink;
        }

        /// <summary>
        /// Compares the settings.
        /// </summary>
        /// <param name="settings">The settings.</param>
        /// <returns></returns>
        private bool CompareSettings(SheetParamCollection settings)
        {
            if (null == sheetSettings || sheetSettings.Count != settings.Count)
            {
                return false;
            }
            foreach (SheetParam param in settings)
            {
                if (!sheetSettings.Exists(p => p.SqlParamName.Equals(param.SqlParamName) && p.Value.Equals(param.Value)))
                {
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// Parent_s the export click.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="selectedPage">The selected page.</param>
        /// <param name="type">The type.</param>
        /// <param name="fName">Name of the f.</param>
        private void parent_ExportClick(object sender, XtraTabPage selectedPage, ExportToType type, string fName)
        {
            if (this == selectedPage)
            {
                switch (type)
                {
                    case ExportToType.Html:
                        PrepareCompositeLink().PrintingSystem.ExportToHtml(fName);
                        break;
                    case ExportToType.Mht:
                        PrepareCompositeLink().PrintingSystem.ExportToMht(fName);
                        break;
                    case ExportToType.Pdf:
                        PrepareCompositeLink().PrintingSystem.ExportToPdf(fName, new PdfExportOptions() { Compressed = true });
                        break;
                    case ExportToType.Rtf:
                        PrepareCompositeLink().PrintingSystem.ExportToRtf(fName);
                        break;
                    case ExportToType.Txt:
                        PrepareCompositeLink().PrintingSystem.ExportToText(fName);
                        break;
                    case ExportToType.Xls:
                        PrepareCompositeLink().PrintingSystem.ExportToXls(fName, new XlsExportOptions() { SheetName = TabName });
                        break;
                    case ExportToType.Xlsx:
                        PrepareCompositeLink().PrintingSystem.ExportToXlsx(fName);
                        break;
                    case ExportToType.Bmp:
                        PrepareCompositeLink().PrintingSystem.ExportToImage(fName);
                        break;
                    case ExportToType.Csv:
                        PrepareCompositeLink().PrintingSystem.ExportToCsv(fName);
                        break;
                }
            }
        }

        /// <summary>
        /// Parent_s the refresh click.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="selectedPage">The selected page.</param>
        private void parent_RefreshClick(object sender, XtraTabPage selectedPage)
        {
            if (this == selectedPage)
            {
                UpdateData(sheetSettings, true);
            }
        }

        /// <summary>
        /// Parent_s the save click.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="selectedPage">The selected page.</param>
        private void parent_SaveClick(object sender, XtraTabPage selectedPage)
        {
            if (this == selectedPage)
            {
                SaveChanges();
            }
        }

        /// <summary>
        /// Parent_s the close click.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="selectedPage">The selected page.</param>
        private void parent_CloseClick(object sender, XtraTabPage selectedPage)
        {
            if (this == selectedPage)
            {
                if (parent.TabControl.TabPages.Contains(this))
                {
                    if (actionPlanControl.IsModified)
                    {
                        if (XtraMessageBox.Show(Resource.AskSaveChanges, Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                        {
                            SaveChanges();
                        }
                    }
                    parent.TabControl.TabPages.Remove(this);
                }
            }
        }
    }
}
