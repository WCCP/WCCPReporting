﻿using DevExpress.XtraEditors.Repository;
using DevExpress.XtraGrid.Views.BandedGrid;
using Logica.Reports.BaseReportControl;
using Logica.Reports.ConfigXmlParser.GridExtension;
using Logica.Reports.IPTRA;
using System.Drawing;
using System.Windows.Forms;

namespace WccpReporting
{
    /// <summary>
    /// 
    /// </summary>
    public partial class WccpUIControl : BaseReportUserControl
    {
        private IPTRASettingsForm formSettings;
        private ActionPlanTab customTabActionPlan;

        /// <summary>
        /// Initializes a new instance of the <see cref="WccpUIControl"/> class.
        /// </summary>
        public WccpUIControl(int reportId)
            : base(reportId)
        {
            InitializeComponent();

            formSettings = new IPTRASettingsForm(Report);
            customTabActionPlan = new ActionPlanTab(this, reportId);

            SettingsFormClick += WccpUIControl_SettingsFormClick;
        }

        public int ReportInit()
        {
            if (formSettings.ShowDialog() == DialogResult.OK)
            {
                UpdateAllSheets();
                return 0;
            }

            return 1;
        }

        /// <summary>
        /// WCCPs the UI control_ settings form click.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="selectedPage">The selected page.</param>
        private void WccpUIControl_SettingsFormClick(object sender, DevExpress.XtraTab.XtraTabPage selectedPage)
        {
            if (formSettings.ShowDialog() == DialogResult.OK)
            {
                UpdateAllSheets();
            }
        }

        /// <summary>
        /// Updates all sheets.
        /// </summary>
        private void UpdateAllSheets()
        {
            UpdateSheets(formSettings.ListSheetParams);
            customTabActionPlan.UpdateSheet(formSettings.ListSheetParams);

            SetExportOptionsForAnalisysTab();
        }

        private void SetExportOptionsForAnalisysTab()
        {
            for (int tabindex = 0; tabindex < tabManager.TabPages.Count; tabindex++)
            {
               if(tabManager.TabPages[tabindex].Text != "Анализ")
                   continue;

                BaseTab baseTabPage = (tabManager.TabPages[tabindex] as BaseTab);
                if (baseTabPage == null) continue;
                
                for (int gridIndex = 0; gridIndex < baseTabPage.Length; gridIndex++)
                {
                    BaseGridControl lGrid = baseTabPage.GetGrid(gridIndex);

                    var lTextEdit = new RepositoryItemTextEdit();
                    lTextEdit.BeginInit();
                    lTextEdit.ExportMode = ExportMode.DisplayText;
                    lGrid.GridControl.RepositoryItems.Add(lTextEdit);
                    lTextEdit.EndInit();

                    GroupBandedView lView = (GroupBandedView)lGrid.GridControl.DefaultView;
                    
                    for (int colIndex = 0; colIndex < lView.Columns.Count; colIndex++)
                    {
                        var lColumn = lView.Columns[colIndex];
                        if (lColumn.Visible)
                        {
                            lColumn.ColumnEdit = lTextEdit;
                        }
                    }
                }
            }
        }
    }
}
