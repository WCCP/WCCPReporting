﻿namespace Logica.Reports
{
    partial class IPTRAnalysisSettingsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(IPTRAnalysisSettingsForm));
            this.panel1 = new DevExpress.XtraEditors.PanelControl();
            this.btnHelp = new DevExpress.XtraEditors.SimpleButton();
            this.imCollection = new DevExpress.Utils.ImageCollection(this.components);
            this.btnNo = new DevExpress.XtraEditors.SimpleButton();
            this.btnYes = new DevExpress.XtraEditors.SimpleButton();
            this.splitContainer1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.uiTlStaffLevel = new DevExpress.XtraTreeList.TreeList();
            this.treeListColumn1 = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.uiCbxEquipmentClass = new DevExpress.XtraEditors.ComboBoxEdit();
            this.lbEquipmentClass = new DevExpress.XtraEditors.LabelControl();
            this.uiCbxChannelType = new DevExpress.XtraEditors.ComboBoxEdit();
            this.lbChannelType = new DevExpress.XtraEditors.LabelControl();
            this.lbDateTime = new DevExpress.XtraEditors.LabelControl();
            this.uiCbxDate = new DevExpress.XtraEditors.DateEdit();
            ((System.ComponentModel.ISupportInitialize)(this.panel1)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imCollection)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiTlStaffLevel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiCbxEquipmentClass.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiCbxChannelType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiCbxDate.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiCbxDate.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.panel1.Appearance.Options.UseBackColor = true;
            this.panel1.Controls.Add(this.btnHelp);
            this.panel1.Controls.Add(this.btnNo);
            this.panel1.Controls.Add(this.btnYes);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 446);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(478, 36);
            this.panel1.TabIndex = 0;
            // 
            // btnHelp
            // 
            this.btnHelp.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnHelp.Enabled = false;
            this.btnHelp.ImageIndex = 0;
            this.btnHelp.ImageList = this.imCollection;
            this.btnHelp.Location = new System.Drawing.Point(397, 6);
            this.btnHelp.Name = "btnHelp";
            this.btnHelp.Size = new System.Drawing.Size(75, 25);
            this.btnHelp.TabIndex = 2;
            this.btnHelp.Text = "&Справка";
            // 
            // imCollection
            // 
            this.imCollection.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imCollection.ImageStream")));
            // 
            // btnNo
            // 
            this.btnNo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnNo.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnNo.ImageIndex = 2;
            this.btnNo.ImageList = this.imCollection;
            this.btnNo.Location = new System.Drawing.Point(316, 6);
            this.btnNo.Name = "btnNo";
            this.btnNo.Size = new System.Drawing.Size(75, 25);
            this.btnNo.TabIndex = 1;
            this.btnNo.Text = "Н&ет";
            // 
            // btnYes
            // 
            this.btnYes.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnYes.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnYes.ImageIndex = 1;
            this.btnYes.ImageList = this.imCollection;
            this.btnYes.Location = new System.Drawing.Point(235, 6);
            this.btnYes.Name = "btnYes";
            this.btnYes.Size = new System.Drawing.Size(75, 25);
            this.btnYes.TabIndex = 0;
            this.btnYes.Text = "&Да";
            this.btnYes.Click += new System.EventHandler(this.btnYes_Click);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Panel1.Controls.Add(this.uiTlStaffLevel);
            this.splitContainer1.Panel2.Controls.Add(this.uiCbxEquipmentClass);
            this.splitContainer1.Panel2.Controls.Add(this.lbEquipmentClass);
            this.splitContainer1.Panel2.Controls.Add(this.uiCbxChannelType);
            this.splitContainer1.Panel2.Controls.Add(this.lbChannelType);
            this.splitContainer1.Panel2.Controls.Add(this.lbDateTime);
            this.splitContainer1.Panel2.Controls.Add(this.uiCbxDate);
            this.splitContainer1.Size = new System.Drawing.Size(478, 446);
            this.splitContainer1.SplitterPosition = 210;
            this.splitContainer1.TabIndex = 1;
            // 
            // uiTlStaffLevel
            // 
            this.uiTlStaffLevel.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.treeListColumn1});
            this.uiTlStaffLevel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiTlStaffLevel.Location = new System.Drawing.Point(0, 0);
            this.uiTlStaffLevel.Name = "uiTlStaffLevel";
            this.uiTlStaffLevel.OptionsSelection.InvertSelection = true;
            this.uiTlStaffLevel.OptionsSelection.MultiSelect = true;
            this.uiTlStaffLevel.OptionsSelection.UseIndicatorForSelection = true;
            this.uiTlStaffLevel.OptionsView.ShowColumns = false;
            this.uiTlStaffLevel.OptionsView.ShowIndicator = false;
            this.uiTlStaffLevel.Size = new System.Drawing.Size(210, 446);
            this.uiTlStaffLevel.TabIndex = 0;
            this.uiTlStaffLevel.SelectionChanged += new System.EventHandler(this.staffLevelTree_SelectionChanged);
            // 
            // treeListColumn1
            // 
            this.treeListColumn1.Caption = "treeListColumn1";
            this.treeListColumn1.FieldName = "treeListColumn1";
            this.treeListColumn1.Name = "treeListColumn1";
            this.treeListColumn1.OptionsColumn.AllowEdit = false;
            this.treeListColumn1.OptionsColumn.AllowMove = false;
            this.treeListColumn1.OptionsColumn.AllowMoveToCustomizationForm = false;
            this.treeListColumn1.OptionsColumn.ReadOnly = true;
            this.treeListColumn1.OptionsColumn.ShowInCustomizationForm = false;
            this.treeListColumn1.Visible = true;
            this.treeListColumn1.VisibleIndex = 0;
            // 
            // uiCbxEquipmentClass
            // 
            this.uiCbxEquipmentClass.Location = new System.Drawing.Point(125, 95);
            this.uiCbxEquipmentClass.Name = "uiCbxEquipmentClass";
            this.uiCbxEquipmentClass.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.uiCbxEquipmentClass.Size = new System.Drawing.Size(116, 20);
            this.uiCbxEquipmentClass.TabIndex = 12;
            // 
            // lbEquipmentClass
            // 
            this.lbEquipmentClass.Location = new System.Drawing.Point(13, 97);
            this.lbEquipmentClass.Name = "lbEquipmentClass";
            this.lbEquipmentClass.Size = new System.Drawing.Size(63, 13);
            this.lbEquipmentClass.TabIndex = 11;
            this.lbEquipmentClass.Text = "labelControl1";
            // 
            // uiCbxChannelType
            // 
            this.uiCbxChannelType.Location = new System.Drawing.Point(125, 52);
            this.uiCbxChannelType.Name = "uiCbxChannelType";
            this.uiCbxChannelType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.uiCbxChannelType.Size = new System.Drawing.Size(116, 20);
            this.uiCbxChannelType.TabIndex = 4;
            this.uiCbxChannelType.SelectedIndexChanged += new System.EventHandler(this.cbChannelType_SelectedIndexChanged);
            // 
            // lbChannelType
            // 
            this.lbChannelType.Location = new System.Drawing.Point(13, 54);
            this.lbChannelType.Name = "lbChannelType";
            this.lbChannelType.Size = new System.Drawing.Size(63, 13);
            this.lbChannelType.TabIndex = 3;
            this.lbChannelType.Text = "labelControl1";
            // 
            // lbDateTime
            // 
            this.lbDateTime.Location = new System.Drawing.Point(13, 16);
            this.lbDateTime.Name = "lbDateTime";
            this.lbDateTime.Size = new System.Drawing.Size(63, 13);
            this.lbDateTime.TabIndex = 2;
            this.lbDateTime.Text = "labelControl1";
            // 
            // uiCbxDate
            // 
            this.uiCbxDate.EditValue = null;
            this.uiCbxDate.Location = new System.Drawing.Point(125, 13);
            this.uiCbxDate.Name = "uiCbxDate";
            this.uiCbxDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.uiCbxDate.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.uiCbxDate.Size = new System.Drawing.Size(116, 20);
            this.uiCbxDate.TabIndex = 1;
            // 
            // IPTRAnalysisSettingsForm
            // 
            this.AcceptButton = this.btnYes;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(478, 482);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.panel1);
            this.MaximizeBox = false;
            this.Name = "IPTRAnalysisSettingsForm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Свойства: IPTR Analysis";
            ((System.ComponentModel.ISupportInitialize)(this.panel1)).EndInit();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.imCollection)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiTlStaffLevel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiCbxEquipmentClass.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiCbxChannelType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiCbxDate.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiCbxDate.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        
        #endregion

        private DevExpress.XtraEditors.SplitContainerControl splitContainer1;
        private DevExpress.XtraTreeList.Columns.TreeListColumn treeListColumn1;
        private DevExpress.XtraEditors.LabelControl lbDateTime;
        private DevExpress.XtraEditors.DateEdit uiCbxDate;
        private DevExpress.XtraEditors.LabelControl lbChannelType;
        private DevExpress.XtraEditors.ComboBoxEdit uiCbxChannelType;
        private DevExpress.XtraTreeList.TreeList uiTlStaffLevel;
        private DevExpress.XtraEditors.SimpleButton btnHelp;
        private DevExpress.XtraEditors.SimpleButton btnNo;
        private DevExpress.XtraEditors.SimpleButton btnYes;
        private DevExpress.XtraEditors.PanelControl panel1;
        private DevExpress.Utils.ImageCollection imCollection;
        private DevExpress.XtraEditors.ComboBoxEdit uiCbxEquipmentClass;
        private DevExpress.XtraEditors.LabelControl lbEquipmentClass;
    }
}