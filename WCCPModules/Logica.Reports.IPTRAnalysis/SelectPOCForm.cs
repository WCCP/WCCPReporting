using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraTab;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraGrid.Views.Grid;
using Logica.Reports.DataAccess;
using System.Data.SqlClient;
using DevExpress.Utils;

namespace Logica.Reports.IPTRA
{
    /// <summary>
    /// 
    /// </summary>
    public partial class SelectPOCForm : DevExpress.XtraEditors.XtraForm
    {
        private DataTable _equipmentSizeTable;
        private DataTable _reallocationPOCTable;
        private int _currentPocId;

        /// <summary>
        /// Initializes a new instance of the <see cref="SelectPOCForm"/> class.
        /// </summary>
        public SelectPOCForm()
        {
            InitializeComponent();
        }


        /// <summary>
        /// Gets the selected POC id.
        /// </summary>
        /// <value>The selected POC id.</value>
        public int SelectedPOCId
        {
            get
            {
                object objPOCId = gridViewPOC.GetFocusedRowCellValue(columnId);
                if (objPOCId != null && objPOCId != DBNull.Value)
                {
                    return Convert.ToInt32(objPOCId);
                }
                return 0;
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SelectPOCForm"/> class.
        /// </summary>
        /// <param name="dataTable">The data table.</param>
        /// <param name="idToExclude">The id to exclude.</param>
        public SelectPOCForm(DataTable reallocationPOCTable, DataTable equipmentSizeTable, int currentPocId)
        {
            InitializeComponent();

            _reallocationPOCTable = reallocationPOCTable;
            _equipmentSizeTable = equipmentSizeTable;
            _currentPocId = currentPocId;

            LoadReallocationTable();
            LoadEquipmentTable();

            UpdateButtonsState();
        }

        /// <summary>
        /// Loads the reallocation table.
        /// </summary>
        private void LoadReallocationTable()
        {
            if (_reallocationPOCTable != null)
            {
                // Set table
                gridControlPOC.DataSource = _reallocationPOCTable.Copy();

                // Filter rows
                DataView dataView = (DataView)gridViewPOC.DataSource;
                if (dataView != null)
                {
                    dataView.RowFilter = string.Format("{0} <> {1}", 
                        Constants.FIELD_REALLOCATION_POC_ID, _currentPocId);
                }
            }
        }

        /// <summary>
        /// Loads the equipment table.
        /// </summary>
        private void LoadEquipmentTable()
        {
            if (_equipmentSizeTable != null)
            {
                // Set copy of the table
                DataTable equipmentSizeTableCopy = _equipmentSizeTable.Copy();
                gridControlEquipment.DataSource = equipmentSizeTableCopy;

                // Filter rows to include only current POC
                DataView dataView = (DataView)gridViewEquipment.DataSource;
                if (dataView != null)
                {
                    dataView.RowFilter = string.Format("{0} = {1}", Constants.FIELD_POC_ID, _currentPocId);
                }

                // Add columns
                foreach (GridColumn column in gridViewEquipment.Columns)
                {
                    bool showColumn = false;

                    if (IsReallocationColumn(column.FieldName))
                    {
                        column.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
                        column.OptionsColumn.AllowShowHide = false;
                        
                        RepositoryItemSpinEdit repositoryColumnEdit = new RepositoryItemSpinEdit();

                        // Set max value
                        repositoryColumnEdit.MaxValue = 0;
                        GridColumn colMaxValue = gridViewEquipment.Columns.ColumnByFieldName(column.FieldName.Replace("Reallocate", string.Empty));
                        if (colMaxValue != null)
                        {
                            object objMaxValue = gridViewEquipment.GetRowCellValue(0, colMaxValue);
                            if (objMaxValue != null && objMaxValue != DBNull.Value)
                            {
                                repositoryColumnEdit.MaxValue = Convert.ToInt32(objMaxValue);
                            }
                        }

                        if (repositoryColumnEdit.MaxValue > 0)
                        {
                            repositoryColumnEdit.MaxLength = repositoryColumnEdit.MaxValue.ToString().Length;
                            repositoryColumnEdit.MinValue = 0;
                            repositoryColumnEdit.ValueChanged += new EventHandler(repositoryColumnEdit_ValueChanged);

                            repositoryColumnEdit.Mask.EditMask = "N00";
                            repositoryColumnEdit.Mask.SaveLiteral = false;
                            repositoryColumnEdit.Mask.ShowPlaceHolders = false;

                            column.ColumnEdit = repositoryColumnEdit;

                            gridViewEquipment.SetRowCellValue(0, column, 0);                   
                        }
                        else
                        {
                            column.OptionsColumn.AllowEdit = false;
                        }

                        column.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Custom;
                        column.SummaryItem.DisplayFormat = string.Format("{0}: {1}",
                            Resource.Available, repositoryColumnEdit.MaxValue);

                        showColumn = true;
                    }

                    column.Visible = showColumn;
                }
            }
        }

        /// <summary>
        /// Determines whether [is reallocation column] [the specified column name].
        /// </summary>
        /// <param name="columnName">Name of the column.</param>
        /// <returns>
        /// 	<c>true</c> if [is reallocation column] [the specified column name]; otherwise, <c>false</c>.
        /// </returns>
        private bool IsReallocationColumn(string columnName)
        {
            if(!string.IsNullOrEmpty(columnName))
            {
                return columnName.StartsWith(Constants.FIELD_PREFIX_REALLOCATION_EQUIPMENT);
            }
            return false;
        }

        /// <summary>
        /// Saves the changes.
        /// </summary>
        private void SaveChanges()
        {
            if (gridViewEquipment != null && gridViewEquipment.RowCount == 1)
            {
                gridViewEquipment.PostEditor();
                gridViewEquipment.UpdateCurrentRow();

                DataView dataView = (DataView)gridViewEquipment.DataSource;
                if (dataView != null)
                {
                    DataTable dataTableChanges = dataView.ToTable();
                    if (_equipmentSizeTable != null && dataTableChanges != null && dataTableChanges.Rows.Count == 1)
                    {
                        DataRow rowToUpdate = _equipmentSizeTable.Rows.Find(_currentPocId);
                        if (rowToUpdate != null)
                        {
                            foreach (DataColumn column in dataTableChanges.Columns)
                            {
                                if (IsReallocationColumn(column.ColumnName))
                                {
                                    rowToUpdate[column.ColumnName] = dataTableChanges.Rows[0][column.ColumnName];
                                }
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Updates the state of the buttons.
        /// </summary>
        private void UpdateButtonsState()
        {
            int sumEquipCount = 0;

            gridViewEquipment.PostEditor();
            gridViewEquipment.UpdateCurrentRow();

            if (gridViewPOC.FocusedRowHandle >= 0)
            {
                foreach (GridColumn column in gridViewEquipment.Columns)
                {
                    if (column.Visible)
                    {
                        sumEquipCount += Convert.ToInt32(gridViewEquipment.GetRowCellValue(0, column));
                    }
                }
            }

            btnSave.Enabled = sumEquipCount > 0;
        }

        /// <summary>
        /// Handles the ValueChanged event of the repositoryColumnEdit control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void repositoryColumnEdit_ValueChanged(object sender, EventArgs e)
        {
            UpdateButtonsState();
        }

        /// <summary>
        /// Handles the Click event of the btnSave control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void btnSave_Click(object sender, EventArgs e)
        {
            SaveChanges();
        }

        /// <summary>
        /// Handles the FocusedRowChanged event of the gridViewPOC control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs"/> instance containing the event data.</param>
        private void gridViewPOC_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            UpdateButtonsState();
        }
    }
}