﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Logica.Reports.BaseReportControl;
using Logica.Reports.DataAccess;
using DevExpress.XtraTreeList.Nodes;
using Logica.Reports.ConfigXmlParser.Model;
using DevExpress.XtraEditors.Controls;
using System.Data.SqlClient;
using Logica.Reports.Common;
using DevExpress.XtraEditors;

namespace Logica.Reports.IPTRA
{
    /// <summary>
    /// 
    /// </summary>
    public partial class IPTRASettingsForm : XtraForm
    {
        private List<SheetParamCollection> _listSheetParams = new List<SheetParamCollection>();
        private Report _report;

        /// <summary>
        /// Gets the list sheet params.
        /// </summary>
        /// <value>The list sheet params.</value>
        public List<SheetParamCollection> ListSheetParams
        {
            get { return _listSheetParams; }
        }

        /// <summary>
        /// Gets the channel type id.
        /// </summary>
        /// <value>The channel type id.</value>
        private int ChannelTypeId
        {
            get
            {
                ComboBoxItem comboBoxItem = (ComboBoxItem)uiCbxChannelType.SelectedItem;
                if (comboBoxItem != null)
                {
                    return comboBoxItem.Id;
                }
                return 0;
            }
        }

        /// <summary>
        /// Gets the equipment class id.
        /// </summary>
        /// <value>The equipment class id.</value>
        private int EquipmentClassId
        {
            get
            {
                ComboBoxItem comboBoxItem = (ComboBoxItem)uiCbxEquipmentClass.SelectedItem;
                if (comboBoxItem != null)
                {
                    return comboBoxItem.Id;
                }
                return 0;
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="IPTRASettingsForm"/> class.
        /// </summary>
        /// <param name="hasWriteAccess">if set to <c>true</c> [has write access].</param>
        /// <param name="tabId">The tab id.</param>
        public IPTRASettingsForm(Report report)
        {
            InitializeComponent();

            _report = report;

            DataAccessLayer.OpenConnection();

            BuildTree(null);

            LoadDate();          
            LoadComboboxChannel();            
            LoadComboboxEquipment();

            DataAccessLayer.CloseConnection();
        }

        /// <summary>
        /// Loads the date.
        /// </summary>
        private void LoadDate()
        {
            lbDateTime.Text = Resource.Date;
            uiCbxDate.DateTime = DateTime.Now;
        }

        /// <summary>
        /// Loads the combobox channel.
        /// </summary>
        private void LoadComboboxChannel()
        {
            lbChannelType.Text = Resource.ChannelType;
            DataTable dataTable = DataProvider.ChannelTypeList;
            if(dataTable != null)
            {
                FillComboBox(uiCbxChannelType, dataTable,
                    Constants.FIELD_CHANNEL_TYPE_ID, Constants.FIELD_CHANNEL_TYPE);

                // remove item 'Все'
                foreach (ComboBoxItem item in uiCbxChannelType.Properties.Items)
                {
                    if (item.Id == 0)
                    {
                        uiCbxChannelType.Properties.Items.Remove(item);
                        break;
                    }
                }
                uiCbxChannelType.SelectedIndex = 0;
            }
        }

        /// <summary>
        /// Loads the combobox equipment.
        /// </summary>
        private void LoadComboboxEquipment()
        {
            lbEquipmentClass.Text = Resource.EquipmentClass;
            DataTable dataTable = DataProvider.EquipmentClassList;
            if (dataTable != null)
            {
                FillComboBox(uiCbxEquipmentClass, dataTable,
                    Constants.FIELD_EQUIPMENT_CLASS_ID, Constants.FIELD_EQUIPMENT_CLASS);
                uiCbxEquipmentClass.SelectedIndex = 0;
            }
        }

        /// <summary>
        /// Builds the tree.
        /// </summary>
        /// <param name="id">The id.</param>
        private void BuildTree(object id)
        {
            uiTlStaffLevel.ClearNodes();

            DataTable dataTable = null;

            if (id != null)
            {
                dataTable = DataProvider.GetStaffTree(new SqlParameter[] { new SqlParameter(Constants.SP_PARAM_CHANNEL_TYPE_ID, id) });
            }
            else
            {
                dataTable = DataProvider.GetStaffTree(null);
            }

            AddChildNodes(null, dataTable);
        }
        

        /// <summary>
        /// Fills the combo box.
        /// </summary>
        /// <param name="comboBox">The combo box.</param>
        /// <param name="table">The table.</param>
        /// <param name="displayMember">The display member.</param>
        /// <param name="valueMember">The value member.</param>
        private void FillComboBox(ComboBoxEdit comboBox, DataTable table, string displayMember, string valueMember)
        {
            if (comboBox != null && table != null)
            {
                foreach (DataRow dataRow in table.Rows)
                {
                    if (dataRow[valueMember] != null)
                    {
                        int id = 0;
                        if (dataRow[displayMember] != null && dataRow[displayMember] != DBNull.Value &&
                            dataRow[valueMember] != null && dataRow[valueMember] != DBNull.Value)
                        {
                            id = Convert.ToInt32(dataRow[displayMember]);
                        }
                        comboBox.Properties.Items.Add(new ComboBoxItem(id, dataRow[valueMember]));
                    }
                }
            }
        }

        /// <summary>
        /// Adds the child nodes.
        /// </summary>
        /// <param name="parent">The parent.</param>
        /// <param name="table">The table.</param>
        private void AddChildNodes(TreeListNode parent, DataTable table)
        {
            foreach (DataRow dataRow in table.Rows)
            {
                if ((null == parent && dataRow[Constants.FIELD_STAFF_TREE_PARENT_ID].Equals(0))
                    || (null != parent && dataRow[Constants.FIELD_STAFF_TREE_PARENT_ID].Equals(((IList<object>)parent.Tag)[0])))
                {
                    TreeListNode node = uiTlStaffLevel.AppendNode(
                        new object[] { dataRow[Constants.FIELD_STAFF_TREE_NAME] }, parent);
                    
                    node.Tag = new List<object>() { 
                        dataRow[Constants.FIELD_STAFF_TREE_ITEM_ID],
                        dataRow[Constants.FIELD_STAFF_TREE_LEVEL_ID], 
                        dataRow[Constants.FIELD_STAFF_TREE_ID] };
                    
                    AddChildNodes(node, table);
                }
            }
        }

        /// <summary>
        /// Handles the SelectedIndexChanged event of the cbChannelType control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void cbChannelType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(uiCbxChannelType.SelectedText))
            {
                BuildTree(ChannelTypeId);
            }
        }

        /// <summary>
        /// Gets the param collection.
        /// </summary>
        /// <param name="tabId">The tab id.</param>
        /// <returns></returns>
        private SheetParamCollection GetParamCollection(Guid tabId)
        {
            SheetParamCollection parameterCollection = new SheetParamCollection();

            parameterCollection.TabId = tabId;
            parameterCollection.TableDataType = TableType.Fact;

            parameterCollection.Add(new SheetParam()
            {
                SqlParamName = Constants.SETTING_PARAM_DATE
                ,
                DisplayParamName = Resource.SettingsDate
                ,
                DisplayValue = uiCbxDate.DateTime.Date.ToShortDateString()
                ,
                Value = uiCbxDate.DateTime.Date
            });
            parameterCollection.Add(new SheetParam()
            {
                SqlParamName = Constants.SETTING_PARAM_CHANNEL_TYPE_ID
                ,
                DisplayParamName = Resource.SettingsChannelType
                ,
                DisplayValue = uiCbxChannelType.Text
                ,
                Value = ChannelTypeId
            });
            parameterCollection.Add(new SheetParam()
            {
                SqlParamName = Constants.SETTING_PARAM_STAFF_LEVEL_ID
                ,
                Value = ((IList<object>)uiTlStaffLevel.Selection[0].Tag)[1]
            });
            parameterCollection.Add(new SheetParam()
            {
                SqlParamName = Constants.SETTING_PARAM_STAFF_ID
                ,
                Value = ((IList<object>)uiTlStaffLevel.Selection[0].Tag)[2]
            });
            parameterCollection.Add(new SheetParam()
            {
                SqlParamName = Constants.SETTING_PARAM_EQUIPMENT_CLASS_ID
                ,
                DisplayParamName = Resource.SettingsEquipmentClass
                ,
                DisplayValue = uiCbxEquipmentClass.Text
                ,
                Value = EquipmentClassId
            });
            parameterCollection.Add(new SheetParam()
            {
                SqlParamName = Constants.SETTING_PARAM_DEBUG
                ,
                Value = 0
            });
            return parameterCollection;
        }

        /// <summary>
        /// Handles the Click evenbt of the btnYes control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void btnYes_Click(object sender, EventArgs e)
        {
            if (Validate())
            {
                BuildSheetSettings();
            }
        }

        /// <summary>
        /// Builds the sheet settings.
        /// </summary>
        /// <param name="report">The report.</param>
        private void BuildSheetSettings()
        {
            if (_report != null)
            {
                _listSheetParams.Clear();

                _listSheetParams.Add(GetParamCollection(ActionPlanTab.TabId));
                foreach (Tab tab in _report.Tabs)
                {
                    _listSheetParams.Add(GetParamCollection(tab.Id));
                }
            }
        }

        /// <summary>
        /// Verifies the value of the control losing focus by causing the <see cref="E:System.Windows.Forms.Control.Validating"/> and <see cref="E:System.Windows.Forms.Control.Validated"/> events to occur, in that order.
        /// </summary>
        /// <returns>
        /// true if validation is successful; otherwise, false. If called from the <see cref="E:System.Windows.Forms.Control.Validating"/> or <see cref="E:System.Windows.Forms.Control.Validated"/> event handlers, this method will always return false.
        /// </returns>
        public new bool Validate()
        {
            bool isValid = false;

            if (uiCbxChannelType.SelectedItem == null || string.IsNullOrEmpty(uiCbxChannelType.SelectedItem.ToString()))
            {
                MessageBox.Show(Resource.IncorrectChannel, Resource.Error, MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else if (uiTlStaffLevel.Selection == null || 0 == uiTlStaffLevel.Selection.Count || string.IsNullOrEmpty(uiTlStaffLevel.Selection[0].ToString()))
            {
                MessageBox.Show(Resource.IncorrectStaffLevel, Resource.Error, MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else if (uiCbxEquipmentClass.SelectedItem == null || string.IsNullOrEmpty(uiCbxEquipmentClass.SelectedItem.ToString()))
            {
                MessageBox.Show(Resource.IncorrectEquipmentClass, Resource.Error, MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                isValid = true;
            }

            return isValid;
        }

        /// <summary>
        /// 
        /// </summary>
        private class ComboBoxItem
        {
            private int id;
            private object value;

            /// <summary>
            /// Initializes a new instance of the <see cref="ComboBoxItem"/> class.
            /// </summary>
            /// <param name="id">The id.</param>
            /// <param name="value">The value.</param>
            public ComboBoxItem(int id, object value)
            {
                this.id = id;
                this.value = value;
            }

            /// <summary>
            /// Gets the id.
            /// </summary>
            /// <value>The id.</value>
            public int Id
            {
                get { return id; }
            }

            /// <summary>
            /// Gets or sets the value.
            /// </summary>
            /// <value>The value.</value>
            public object Value
            {
                get { return this.value; }
            }

            /// <summary>
            /// Returns a <see cref="System.String"/> that represents this instance.
            /// </summary>
            /// <returns>
            /// A <see cref="System.String"/> that represents this instance.
            /// </returns>
            public override string ToString()
            {
                return value.ToString();
            }
        }
    }
}

