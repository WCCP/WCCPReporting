﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraEditors.Controls;
using Logica.Reports.DataAccess;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraEditors;

namespace Logica.Reports.IPTRA
{
    /// <summary>
    /// 
    /// </summary>
    public partial class ActionPlanControl : UserControl
    {
        private AccessLevel _accessLevel = AccessLevel.None;

        /// <summary>
        /// Initializes a new instance of the <see cref="ActionPlanControl"/> class.
        /// </summary>
        public ActionPlanControl()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Gets the grid control.
        /// </summary>
        /// <value>The grid control.</value>
        public GridControl GridControl
        {
            get
            {
                return gridControl;
            }
        }

        /// <summary>
        /// Gets the grid view.
        /// </summary>
        /// <value>The grid view.</value>
        public GridView GridView
        {
            get
            {
                return gridView;
            }
        }

        /// <summary>
        /// Sets the action plan table.
        /// </summary>
        /// <value>The action plan table.</value>
        public DataTable ActionPlanTable
        {
            set
            {
                gridControl.DataSource = value;
                UpdateColumnAccessLevel();
            }
            get 
            {
                gridView.PostEditor();
                gridView.UpdateCurrentRow();
                return (DataTable)gridControl.DataSource;
            }
        }

        /// <summary>
        /// Sets the access level.
        /// </summary>
        /// <value>The access level.</value>
        public AccessLevel AccessLevel
        {
            set
            {
                _accessLevel = value;
            }
        }

        /// <summary>
        /// Sets the action list.
        /// </summary>
        /// <value>The action list.</value>
        public DataTable ActionListLookUp
        {
            set { repositoryActionLookUp.DataSource = value; }
        }

        /// <summary>
        /// Sets the reallocation POC look up.
        /// </summary>
        /// <value>The reallocation POC look up.</value>
        public DataTable ReallocationPOCTableLookUp
        {
            set
            {
                repositoryPOCMoveToookUp.DataSource = value;
            }
        }

        /// <summary>
        /// Sets the responsible list.
        /// </summary>
        /// <value>The responsible list.</value>
        public DataTable M2ListLokUp
        {
            set
            {
                repositoryResponsibleLookUp.DataSource = value;
            }
        }

        /// <summary>
        /// Sets the status list.
        /// </summary>
        /// <value>The status list.</value>
        public DataTable StatusListLookUp
        {
            set
            {
                repositoryStatusLookUp.DataSource = value;
            }
        }

        /// <summary>
        /// Gets or sets the reallocation POC table.
        /// </summary>
        /// <value>The reallocation POC table.</value>
        public DataTable ReallocationPOCTable
        {
            get;
            set;
        }

        /// <summary>
        /// Gets the equipment size table.
        /// </summary>
        /// <value>The equipment size table.</value>
        public DataTable EquipmentSizeTable
        {
            get;
            set;
        }

        /// <summary>
        /// Gets a value indicating whether this instance is modified.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance is modified; otherwise, <c>false</c>.
        /// </value>
        public bool IsModified
        {
            get
            {
                bool isModified = false;
                DataTable dataTable = ActionPlanTable;
                if (dataTable != null)
                {
                    DataTable changesTable = dataTable.GetChanges();
                    if (changesTable != null)
                    {
                        isModified = changesTable.Rows.Count > 0;
                    }
                }
                return isModified;
            }
        }

        /// <summary>
        /// Gets the column equipment small.
        /// </summary>
        /// <value>The column equipment small.</value>
        private string ColumnEquipmentSmall
        {
            get
            {
                return string.Concat(Constants.FIELD_PREFIX_EQUIPMENT, Constants.FIELD_EQUIPMENT_SMALL);
            }
        }

        /// <summary>
        /// Updates the column access level.
        /// </summary>
        private void UpdateColumnAccessLevel()
        {
            columnTopic.OptionsColumn.AllowEdit = (_accessLevel == AccessLevel.M2 || _accessLevel == AccessLevel.M3);
            columnAction.OptionsColumn.AllowEdit = (_accessLevel == AccessLevel.M2 || _accessLevel == AccessLevel.M3);
            columnCommentAction.OptionsColumn.AllowEdit = (_accessLevel == AccessLevel.M2 || _accessLevel == AccessLevel.M3);
            columnCommentStatus.OptionsColumn.AllowEdit = (_accessLevel == AccessLevel.M2 || _accessLevel == AccessLevel.M3);

            columnM2.OptionsColumn.AllowEdit = (_accessLevel == AccessLevel.M3);

            columnApproved.OptionsColumn.AllowEdit = (_accessLevel == AccessLevel.M3 || _accessLevel == AccessLevel.M4);
            standaloneTool.Visible = columnApproved.OptionsColumn.AllowEdit;

            // Set cell appearance
            foreach (GridColumn column in gridView.Columns)
            {
                 if (column.OptionsColumn.AllowEdit)
                 {
                     column.AppearanceCell.BackColor = Color.LightPink;
                 }
                 column.AppearanceCell.Options.UseBackColor = column.OptionsColumn.AllowEdit;
            }
        }

        /// <summary>
        /// Handles the EditValueChanging event of the repositoryActionLookUp control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="DevExpress.XtraEditors.Controls.ChangingEventArgs"/> instance containing the event data.</param>
        private void repositoryActionLookUp_EditValueChanging(object sender, ChangingEventArgs e)
        {
            if (gridView.FocusedRowHandle >= 0)
            {
                bool resetPocMoveTo = true;

                Action newAction = (Action)e.NewValue;
                if (newAction == Action.RelocateToPoc || newAction == Action.RelocateToPocAndStock)
                {
                    int currentId = Convert.ToInt32(gridView.GetFocusedRowCellValue(columnId));

                    SelectPOCForm selectPOCForm = new SelectPOCForm(
                        ReallocationPOCTable, EquipmentSizeTable, currentId);

                    if (selectPOCForm.ShowDialog() == DialogResult.OK)
                    {
                        object value = null;
                        if(selectPOCForm.SelectedPOCId > 0)
                        {
                            value = selectPOCForm.SelectedPOCId;
                        }
                        gridView.SetFocusedRowCellValue(columPOCMoveTo, value);
                        resetPocMoveTo = false;
                    }
                    else
                    {
                        e.Cancel = true;
                    }
                }
                else if (newAction == Action.ReplaceWithOneDoorFridge)
                {
                    e.Cancel = CheckPOCHasEquipmentSmallOnly(gridView.FocusedRowHandle);
                    if (e.Cancel)
                    {
                        XtraMessageBox.Show(Resource.HasEquipmentSmallOnly, Application.ProductName);
                    }
                }
                else if (newAction == Action.LeaveAndDZ || newAction == Action.LeaveAndRepair)
                {
                    gridView.SetFocusedRowCellValue(columnStatus, (int)ActionStatus.Complete);
                }

                if (resetPocMoveTo)
                {
                    gridView.SetFocusedRowCellValue(columPOCMoveTo, null);
                }

                if (!e.Cancel)
                {
                    gridView.SetFocusedRowCellValue(columnDate, DateTime.Now);
                    gridView.SetFocusedRowCellValue(columnApproved, false);
                    DateTime dueDate = new DateTime(DateTime.Today.Year, DateTime.Today.AddMonths(1).Month, 1);
                    gridView.SetFocusedRowCellValue(columnDueDate, dueDate);
                }
            }
        }

        /// <summary>
        /// Handles the ItemClick event of the barButtonApproveAll control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="DevExpress.XtraBars.ItemClickEventArgs"/> instance containing the event data.</param>
        private void barButtonApproveAll_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            gridView.PostEditor();
            gridView.UpdateCurrentRow();

            for (int rowHandle = 0; rowHandle < gridView.RowCount; rowHandle++)
            {
                if (GetCanApprovePOC(rowHandle) == ApproveStatus.CanApprove)
                {
                    gridView.SetRowCellValue(rowHandle, columnApproved, true);
                }
            }
        }

        /// <summary>
        /// Handles the ShowingEditor event of the gridView control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.ComponentModel.CancelEventArgs"/> instance containing the event data.</param>
        private void gridView_ShowingEditor(object sender, CancelEventArgs e)
        {
            // if user cannot approve and current POC is approved then cancel editing any field
            object objApproved = gridView.GetFocusedRowCellValue(columnApproved);
            if (objApproved != null && objApproved != DBNull.Value)
            {
                if (!columnApproved.OptionsColumn.AllowEdit)
                {
                    e.Cancel = Convert.ToBoolean(objApproved);
                }
            }
        }

        /// <summary>
        /// Handles the EditValueChanging event of the repositoryApproveCheck control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="DevExpress.XtraEditors.Controls.ChangingEventArgs"/> instance containing the event data.</param>
        private void repositoryApproveCheck_EditValueChanging(object sender, ChangingEventArgs e)
        {
            if (gridView.FocusedRowHandle >= 0)
            {
                // do not allow approving while action is not set
                bool approve = Convert.ToBoolean(e.NewValue);
                if (approve)
                {
                    ApproveStatus canApprove = GetCanApprovePOC(gridView.FocusedRowHandle);
                    if (canApprove == ApproveStatus.ActionIsNotSet)
                    {
                        e.Cancel = true;
                        XtraMessageBox.Show(Resource.SelectActionFirst, Application.ProductName);
                    }
                    else if (canApprove == ApproveStatus.ResponsibleIsNotSet)
                    {
                        e.Cancel = true;
                        XtraMessageBox.Show(Resource.SelectResponsibleFirst, Application.ProductName);
                    }
                    else if (canApprove == ApproveStatus.Error)
                    {
                        e.Cancel = true;
                        XtraMessageBox.Show(Resource.Error, Application.ProductName);
                    }
                }
            }
        }

        /// <summary>
        /// Gets the can approve POC.
        /// </summary>
        /// <value>The can approve POC.</value>
        private ApproveStatus GetCanApprovePOC(int rowHandle)
        {
            if(rowHandle >= 0)
            {
                object objAction = gridView.GetRowCellValue(rowHandle, columnAction);
                if (objAction == null || objAction == DBNull.Value || Convert.ToInt32(objAction) == (int)Action.Unknown)
                {
                    return ApproveStatus.ActionIsNotSet;
                }

                object objM2 = gridView.GetRowCellValue(rowHandle, columnM2);
                if (objM2 == null || objM2 == DBNull.Value)
                {
                    return ApproveStatus.ResponsibleIsNotSet;
                }

                return ApproveStatus.CanApprove;
            }

            return ApproveStatus.Error;
        }

        /// <summary>
        /// Checks the POC has equipment small only.
        /// </summary>
        /// <param name="rowHandle">The row handle.</param>
        /// <returns></returns>
        private bool CheckPOCHasEquipmentSmallOnly(int rowHandle)
        {
            int nonEquipmentSmallCount = 0;
            int equipmentSmallCount = 0;
            int currentId = Convert.ToInt32(gridView.GetFocusedRowCellValue(columnId));

            DataTable dataTable = ActionPlanTable;

            if (dataTable != null && dataTable.Rows.Count > 0)
            {
                DataView dataView = dataTable.AsDataView();
                dataView.Sort = Constants.FIELD_POC_ID;
                DataRowView[] rows = dataView.FindRows(currentId);

                if (rows.Length == 1)
                {
                    DataRowView row = rows[0];

                    foreach (DataColumn column in dataTable.Columns)
                    {
                        // find EquipmentSize: fields
                        if (column.ColumnName.StartsWith(Constants.FIELD_PREFIX_EQUIPMENT))
                        {
                            object value = row[column.ColumnName];
                            if (value != null && value != DBNull.Value)
                            {
                                if (column.ColumnName == ColumnEquipmentSmall)
                                {
                                    equipmentSmallCount += Convert.ToInt32(value);
                                }
                                else
                                {
                                    nonEquipmentSmallCount += Convert.ToInt32(value);
                                }
                            }
                        }
                    }
                }
            }

            return equipmentSmallCount > 0 && nonEquipmentSmallCount == 0;
        }
    }
}
