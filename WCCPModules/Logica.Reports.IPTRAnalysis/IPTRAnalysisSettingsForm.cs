﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Logica.Reports.BaseReportControl;
using Logica.Reports.DataAccess;
using DevExpress.XtraTreeList.Nodes;
using Logica.Reports.ConfigXmlParser.Model;
using DevExpress.XtraEditors.Controls;
using System.Data.SqlClient;
using Logica.Reports.Common;
using DevExpress.XtraEditors;

namespace Logica.Reports
{
    /// <summary>
    /// 
    /// </summary>
    public partial class IPTRAnalysisSettingsForm : XtraForm
    {
        #region Private variables

        private Dictionary<object, object> _channelTypeCollection = new Dictionary<object, object>();
        private Dictionary<object, object> _equipmentCollection = new Dictionary<object, object>();
        private SheetParamCollection _parameterCollection;
        private Guid _tabId;
        private bool _hasWriteAccess;

        #endregion 

        #region Public properties

        /// <summary>
        /// Gets or sets the parameter collection.
        /// </summary>
        /// <value>The parameter collection.</value>
        public SheetParamCollection ParameterCollection
        {
            get { return _parameterCollection; }
            set { _parameterCollection = value; }
        }

        /// <summary>
        /// Gets or sets the tab id.
        /// </summary>
        /// <value>The tab id.</value>
        public Guid TabId
        {
            get { return _tabId; }
            set { _tabId = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this instance has write access.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance has write access; otherwise, <c>false</c>.
        /// </value>
        public bool HasWriteAccess
        {
            get { return _hasWriteAccess; }
            set { _hasWriteAccess = value; }
        }

        /// <summary>
        /// Gets a value indicating whether need to byild a fact report.
        /// </summary>
        /// <value><c>true</c> if this instance is fact; otherwise, <c>false</c>.</value>
        public bool IsFact
        {
            get { return !_hasWriteAccess; }
        }

        #endregion

        #region Constructors

        public IPTRAnalysisSettingsForm(bool hasWriteAccess, Guid tabId)
        {
            InitializeComponent();

            //Date
            lbDateTime.Text = Resource.Date;
            uiCbxDate.DateTime = DateTime.Now;
            //Staff tree
            BuildTree(null);
            //Chanel type
            lbChannelType.Text = Resource.ChannelType;
            DataSet ds2 = DataAccessLayer.ExecuteStoredProcedure("spDW_ChanelTypeList", new SqlParameter[] { });
            foreach (DataRow dr in ds2.Tables[0].Rows)
            {
                if (null == dr["ChanelType_id"] || String.IsNullOrEmpty(dr["ChanelType_id"].ToString()))
                {
                    continue;
                }
                _channelTypeCollection.Add(dr["ChanelType"], dr["ChanelType_id"]);
                uiCbxChannelType.Properties.Items.Add(dr["ChanelType"]);
            }
            uiCbxChannelType.SelectedIndex = 0;
            //Equipment
            lbEquipmentClass.Text = Resource.EquipmentClass;
            DataSet ds3 = DataAccessLayer.ExecuteStoredProcedure("spDW_IPTR_EquipmentClassList", new SqlParameter[] { });
            foreach (DataRow dr in ds3.Tables[0].Rows)
            {
                _equipmentCollection.Add(dr["ClassName"], dr["EquipmentClass_ID"]);
                uiCbxEquipmentClass.Properties.Items.Add(dr["ClassName"]);
            }
            uiCbxEquipmentClass.SelectedIndex = 0;
            HasWriteAccess = hasWriteAccess;
            TabId = tabId;
            //uiRgPlanFact.Visible = hasWriteAccess;
        }

        #endregion // Constructors

        #region Build tree

        private void BuildTree(object id)
        {
            uiTlStaffLevel.ClearNodes();
            DataSet ds1;
            DataAccessLayer.OpenConnection();
            if(null != id)
                ds1 = DataAccessLayer.ExecuteStoredProcedure("spDW_StaffTree", new SqlParameter[] { new SqlParameter("@ChanelType_ID", id) });
            else
                ds1 = DataAccessLayer.ExecuteStoredProcedure("spDW_StaffTree", new SqlParameter[] { });
            DataAccessLayer.CloseConnection();
            AddChildNodes(null, ds1.Tables[0]);
        }

        private void AddChildNodes(TreeListNode parent,DataTable table)
        {
            foreach(DataRow dr in table.Rows)
            {
                if ((null == parent && dr["Parent_ID"].Equals(0)) || (null != parent && dr["Parent_ID"].Equals(((IList<object>)parent.Tag)[0])))
                {
                    TreeListNode node = uiTlStaffLevel.AppendNode(new object[] { dr["Name"] }, parent);
                    node.Tag = new List<object>() { dr["Item_ID"], dr["StaffLevel_ID"], dr["ID"] };
                    AddChildNodes(node, table);
                }
            }
        }

        #endregion // Build tree

        #region Event handlers

        private void cbChannelType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(uiCbxChannelType.SelectedText))
            {
                BuildTree(_channelTypeCollection[uiCbxChannelType.SelectedText]);
            }
        }

        private void uiRgPlanFact_SelectedIndexChanged(object sender, EventArgs e)
        {
            bool enabled = true;//uiRgPlanFact.SelectedIndex != 0;
            uiTlStaffLevel.Enabled = enabled;
            uiCbxChannelType.Enabled = enabled;
        }

        private void btnYes_Click(object sender, EventArgs e)
        {
            if (!Validate())
            {
                return;
            }

            _parameterCollection = new SheetParamCollection();
            _parameterCollection.TabId = TabId;
            _parameterCollection.TableDataType = IsFact ? TableType.Fact : TableType.Plan;
            _parameterCollection.Add(new SheetParam() { SqlParamName = "@Date"
                                    , DisplayParamName = "Дата"
                                    , DisplayValue = uiCbxDate.DateTime.Date.ToShortDateString()
                                    , Value = uiCbxDate.DateTime.Date
            });
            _parameterCollection.Add(new SheetParam() { SqlParamName = "@ChanelType_id"
                                    , DisplayParamName = IsFact ? "Тип канала" : null
                                    , DisplayValue = uiCbxChannelType.Text
                                    , Value = _channelTypeCollection[uiCbxChannelType.SelectedItem.ToString()]
            });
            _parameterCollection.Add(new SheetParam() { SqlParamName = "@StaffLevel_ID"
                                    , Value = ((IList<object>)uiTlStaffLevel.Selection[0].Tag)[1]
            });
            _parameterCollection.Add(new SheetParam() { SqlParamName = "@ID"
                                    , Value = ((IList<object>)uiTlStaffLevel.Selection[0].Tag)[2]
            });
            _parameterCollection.Add(new SheetParam() { SqlParamName = "@EquipmentClass_ID"
                                    , Value = _equipmentCollection[uiCbxEquipmentClass.SelectedItem.ToString()]
                                    , DisplayParamName = "Тип оборудования" 
                                    , DisplayValue = uiCbxEquipmentClass.Text
            });
            _parameterCollection.Add(new SheetParam() { SqlParamName = "@debug"
                                    , Value = 0
            });
        }

        #endregion // Event handlers

        #region Staff tree

        private void staffLevelTree_SelectionChanged(object sender, EventArgs e)
        {
            if (null != uiTlStaffLevel.Selection[0] && null != uiTlStaffLevel.Selection[0].Tag)
            {
                this.Cursor = System.Windows.Forms.Cursors.WaitCursor;
                this.Cursor = System.Windows.Forms.Cursors.Default;
            }
        }
        #endregion

        #region Public methods

        /// <summary>
        /// Verifies the value of the control losing focus by causing the <see cref="E:System.Windows.Forms.Control.Validating"/> and <see cref="E:System.Windows.Forms.Control.Validated"/> events to occur, in that order.
        /// </summary>
        /// <returns>
        /// true if validation is successful; otherwise, false. If called from the <see cref="E:System.Windows.Forms.Control.Validating"/> or <see cref="E:System.Windows.Forms.Control.Validated"/> event handlers, this method will always return false.
        /// </returns>
        /// <PermissionSet>
        /// 	<IPermission class="System.Security.Permissions.EnvironmentPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true"/>
        /// 	<IPermission class="System.Security.Permissions.FileIOPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true"/>
        /// 	<IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="UnmanagedCode, ControlEvidence"/>
        /// 	<IPermission class="System.Diagnostics.PerformanceCounterPermission, System, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true"/>
        /// </PermissionSet>
        public new bool Validate()
        {
            bool isValid = false;

            if (uiCbxChannelType.SelectedItem == null || string.IsNullOrEmpty(uiCbxChannelType.SelectedItem.ToString()))
            {
                MessageBox.Show(Resource.IncorrectChannel, Resource.Error, MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else if (uiTlStaffLevel.Selection == null || 0 == uiTlStaffLevel.Selection.Count || string.IsNullOrEmpty(uiTlStaffLevel.Selection[0].ToString()))
            {
                MessageBox.Show(Resource.IncorrectStaffLevel, Resource.Error, MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else if (uiCbxEquipmentClass.SelectedItem == null || string.IsNullOrEmpty(uiCbxEquipmentClass.SelectedItem.ToString()))
            {
                MessageBox.Show(Resource.IncorrectEquipmentClass, Resource.Error, MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                isValid = true;
            }

            return isValid;
        }

        #endregion //Public methods
    }
}

