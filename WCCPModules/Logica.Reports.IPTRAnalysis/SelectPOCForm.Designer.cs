namespace Logica.Reports.IPTRA
{
    partial class SelectPOCForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnSave = new DevExpress.XtraEditors.SimpleButton();
            this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.groupControlPOC = new DevExpress.XtraEditors.GroupControl();
            this.gridControlPOC = new DevExpress.XtraGrid.GridControl();
            this.gridViewPOC = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.columnM2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnM1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnPOCName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnPOCAddress = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnPOCRegion = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnPOCChannel = new DevExpress.XtraGrid.Columns.GridColumn();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.gridControlEquipment = new DevExpress.XtraGrid.GridControl();
            this.gridViewEquipment = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.repositoryEquipmentCountEdit = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControlPOC)).BeginInit();
            this.groupControlPOC.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlPOC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewPOC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlEquipment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewEquipment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryEquipmentCountEdit)).BeginInit();
            this.SuspendLayout();
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnSave.Location = new System.Drawing.Point(627, 528);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(100, 23);
            this.btnSave.TabIndex = 1;
            this.btnSave.Text = "�������";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.CausesValidation = false;
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(733, 528);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(100, 23);
            this.btnCancel.TabIndex = 1;
            this.btnCancel.Text = "��������";
            // 
            // groupControlPOC
            // 
            this.groupControlPOC.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControlPOC.Controls.Add(this.gridControlPOC);
            this.groupControlPOC.Location = new System.Drawing.Point(12, 12);
            this.groupControlPOC.Name = "groupControlPOC";
            this.groupControlPOC.Size = new System.Drawing.Size(821, 412);
            this.groupControlPOC.TabIndex = 6;
            this.groupControlPOC.Text = "�� � ����������:";
            // 
            // gridControlPOC
            // 
            this.gridControlPOC.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlPOC.Location = new System.Drawing.Point(2, 22);
            this.gridControlPOC.MainView = this.gridViewPOC;
            this.gridControlPOC.Name = "gridControlPOC";
            this.gridControlPOC.Size = new System.Drawing.Size(817, 388);
            this.gridControlPOC.TabIndex = 5;
            this.gridControlPOC.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewPOC});
            // 
            // gridViewPOC
            // 
            this.gridViewPOC.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.columnM2,
            this.columnM1,
            this.columnPOCName,
            this.columnId,
            this.columnPOCAddress,
            this.columnPOCRegion,
            this.columnPOCChannel});
            this.gridViewPOC.GridControl = this.gridControlPOC;
            this.gridViewPOC.Name = "gridViewPOC";
            this.gridViewPOC.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridViewPOC.OptionsView.ShowAutoFilterRow = true;
            this.gridViewPOC.OptionsView.ShowGroupPanel = false;
            this.gridViewPOC.OptionsView.ShowIndicator = false;
            this.gridViewPOC.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridViewPOC_FocusedRowChanged);
            // 
            // columnM2
            // 
            this.columnM2.AppearanceHeader.Options.UseTextOptions = true;
            this.columnM2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnM2.Caption = "M2";
            this.columnM2.FieldName = "M2";
            this.columnM2.Name = "columnM2";
            this.columnM2.OptionsColumn.AllowEdit = false;
            this.columnM2.OptionsColumn.AllowShowHide = false;
            this.columnM2.Visible = true;
            this.columnM2.VisibleIndex = 0;
            // 
            // columnM1
            // 
            this.columnM1.AppearanceHeader.Options.UseTextOptions = true;
            this.columnM1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnM1.Caption = "M1";
            this.columnM1.FieldName = "M1";
            this.columnM1.Name = "columnM1";
            this.columnM1.OptionsColumn.AllowEdit = false;
            this.columnM1.OptionsColumn.AllowShowHide = false;
            this.columnM1.Visible = true;
            this.columnM1.VisibleIndex = 1;
            // 
            // columnPOCName
            // 
            this.columnPOCName.AppearanceHeader.Options.UseTextOptions = true;
            this.columnPOCName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnPOCName.Caption = "POC Name";
            this.columnPOCName.FieldName = "POCName";
            this.columnPOCName.Name = "columnPOCName";
            this.columnPOCName.OptionsColumn.AllowEdit = false;
            this.columnPOCName.OptionsColumn.AllowShowHide = false;
            this.columnPOCName.Visible = true;
            this.columnPOCName.VisibleIndex = 2;
            // 
            // columnId
            // 
            this.columnId.AppearanceHeader.Options.UseTextOptions = true;
            this.columnId.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnId.Caption = "POC ID";
            this.columnId.FieldName = "ID";
            this.columnId.Name = "columnId";
            this.columnId.OptionsColumn.AllowEdit = false;
            this.columnId.OptionsColumn.AllowShowHide = false;
            this.columnId.Visible = true;
            this.columnId.VisibleIndex = 3;
            // 
            // columnPOCAddress
            // 
            this.columnPOCAddress.AppearanceHeader.Options.UseTextOptions = true;
            this.columnPOCAddress.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnPOCAddress.Caption = "Address";
            this.columnPOCAddress.FieldName = "Address";
            this.columnPOCAddress.Name = "columnPOCAddress";
            this.columnPOCAddress.OptionsColumn.AllowEdit = false;
            this.columnPOCAddress.OptionsColumn.AllowShowHide = false;
            this.columnPOCAddress.Visible = true;
            this.columnPOCAddress.VisibleIndex = 4;
            // 
            // columnPOCRegion
            // 
            this.columnPOCRegion.AppearanceHeader.Options.UseTextOptions = true;
            this.columnPOCRegion.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnPOCRegion.Caption = "Region";
            this.columnPOCRegion.FieldName = "Region";
            this.columnPOCRegion.Name = "columnPOCRegion";
            this.columnPOCRegion.OptionsColumn.AllowEdit = false;
            this.columnPOCRegion.OptionsColumn.AllowShowHide = false;
            this.columnPOCRegion.Visible = true;
            this.columnPOCRegion.VisibleIndex = 5;
            // 
            // columnPOCChannel
            // 
            this.columnPOCChannel.AppearanceHeader.Options.UseTextOptions = true;
            this.columnPOCChannel.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnPOCChannel.Caption = "Channel";
            this.columnPOCChannel.FieldName = "Channel";
            this.columnPOCChannel.Name = "columnPOCChannel";
            this.columnPOCChannel.OptionsColumn.AllowEdit = false;
            this.columnPOCChannel.OptionsColumn.AllowShowHide = false;
            this.columnPOCChannel.Visible = true;
            this.columnPOCChannel.VisibleIndex = 6;
            // 
            // groupControl2
            // 
            this.groupControl2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl2.Controls.Add(this.gridControlEquipment);
            this.groupControl2.Location = new System.Drawing.Point(14, 430);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(821, 92);
            this.groupControl2.TabIndex = 7;
            this.groupControl2.Text = "���������� � ��� ������������ � �����������:";
            // 
            // gridControlEquipment
            // 
            this.gridControlEquipment.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlEquipment.Location = new System.Drawing.Point(2, 22);
            this.gridControlEquipment.MainView = this.gridViewEquipment;
            this.gridControlEquipment.Name = "gridControlEquipment";
            this.gridControlEquipment.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryEquipmentCountEdit});
            this.gridControlEquipment.Size = new System.Drawing.Size(817, 68);
            this.gridControlEquipment.TabIndex = 6;
            this.gridControlEquipment.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewEquipment});
            // 
            // gridViewEquipment
            // 
            this.gridViewEquipment.Appearance.FocusedCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.gridViewEquipment.Appearance.FocusedCell.Options.UseBackColor = true;
            this.gridViewEquipment.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.gridViewEquipment.Appearance.Row.Options.UseBackColor = true;
            this.gridViewEquipment.GridControl = this.gridControlEquipment;
            this.gridViewEquipment.Name = "gridViewEquipment";
            this.gridViewEquipment.OptionsCustomization.AllowColumnMoving = false;
            this.gridViewEquipment.OptionsCustomization.AllowFilter = false;
            this.gridViewEquipment.OptionsCustomization.AllowGroup = false;
            this.gridViewEquipment.OptionsCustomization.AllowSort = false;
            this.gridViewEquipment.OptionsSelection.EnableAppearanceFocusedRow = false;
            this.gridViewEquipment.OptionsView.ShowFooter = true;
            this.gridViewEquipment.OptionsView.ShowGroupPanel = false;
            this.gridViewEquipment.OptionsView.ShowIndicator = false;
            // 
            // repositoryEquipmentCountEdit
            // 
            this.repositoryEquipmentCountEdit.AutoHeight = false;
            this.repositoryEquipmentCountEdit.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.repositoryEquipmentCountEdit.Mask.EditMask = "f0";
            this.repositoryEquipmentCountEdit.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryEquipmentCountEdit.Name = "repositoryEquipmentCountEdit";
            // 
            // SelectPOCForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(844, 562);
            this.Controls.Add(this.groupControl2);
            this.Controls.Add(this.groupControlPOC);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnSave);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(562, 329);
            this.Name = "SelectPOCForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Show;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "����� �� ��� ����������� ������������";
            ((System.ComponentModel.ISupportInitialize)(this.groupControlPOC)).EndInit();
            this.groupControlPOC.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlPOC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewPOC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlEquipment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewEquipment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryEquipmentCountEdit)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton btnSave;
        private DevExpress.XtraEditors.SimpleButton btnCancel;
        private DevExpress.XtraEditors.GroupControl groupControlPOC;
        private DevExpress.XtraGrid.GridControl gridControlPOC;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewPOC;
        private DevExpress.XtraGrid.Columns.GridColumn columnM2;
        private DevExpress.XtraGrid.Columns.GridColumn columnM1;
        private DevExpress.XtraGrid.Columns.GridColumn columnPOCName;
        private DevExpress.XtraGrid.Columns.GridColumn columnId;
        private DevExpress.XtraGrid.Columns.GridColumn columnPOCAddress;
        private DevExpress.XtraGrid.Columns.GridColumn columnPOCRegion;
        private DevExpress.XtraGrid.Columns.GridColumn columnPOCChannel;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraGrid.GridControl gridControlEquipment;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewEquipment;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryEquipmentCountEdit;


    }
}