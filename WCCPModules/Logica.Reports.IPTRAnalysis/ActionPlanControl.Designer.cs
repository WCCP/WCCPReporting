﻿namespace Logica.Reports.IPTRA
{
    partial class ActionPlanControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gridControl = new DevExpress.XtraGrid.GridControl();
            this.gridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.columnSuperviser = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnMerch = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnPOCName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnTopic = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryTopicEdit = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.columnAction = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryActionLookUp = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.columPOCMoveTo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryPOCMoveToookUp = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.columnApproved = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryApproveCheck = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.columnCommentAction = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryCommentActionEdit = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.columnM2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryResponsibleLookUp = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.columnDueDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryStatusLookUp = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.columnCommentStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryCommentStatusEdit = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.barManager = new DevExpress.XtraBars.BarManager();
            this.standaloneTool = new DevExpress.XtraBars.Bar();
            this.barButtonApproveAll = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryTopicEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryActionLookUp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryPOCMoveToookUp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryApproveCheck)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryCommentActionEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryResponsibleLookUp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryStatusLookUp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryCommentStatusEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager)).BeginInit();
            this.SuspendLayout();
            // 
            // gridControl
            // 
            this.gridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl.Location = new System.Drawing.Point(0, 24);
            this.gridControl.MainView = this.gridView;
            this.gridControl.Name = "gridControl";
            this.gridControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryTopicEdit,
            this.repositoryActionLookUp,
            this.repositoryApproveCheck,
            this.repositoryCommentActionEdit,
            this.repositoryResponsibleLookUp,
            this.repositoryStatusLookUp,
            this.repositoryCommentStatusEdit,
            this.repositoryPOCMoveToookUp});
            this.gridControl.Size = new System.Drawing.Size(769, 365);
            this.gridControl.TabIndex = 1;
            this.gridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView});
            // 
            // gridView
            // 
            this.gridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.columnSuperviser,
            this.columnMerch,
            this.columnId,
            this.columnPOCName,
            this.columnDate,
            this.columnTopic,
            this.columnAction,
            this.columPOCMoveTo,
            this.columnApproved,
            this.columnCommentAction,
            this.columnM2,
            this.columnDueDate,
            this.columnStatus,
            this.columnCommentStatus});
            this.gridView.GridControl = this.gridControl;
            this.gridView.Name = "gridView";
            this.gridView.OptionsView.ColumnAutoWidth = false;
            this.gridView.OptionsView.ShowAutoFilterRow = true;
            this.gridView.OptionsView.ShowGroupPanel = false;
            this.gridView.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.gridView_ShowingEditor);
            // 
            // columnSuperviser
            // 
            this.columnSuperviser.AppearanceHeader.Options.UseTextOptions = true;
            this.columnSuperviser.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnSuperviser.Caption = "M2";
            this.columnSuperviser.FieldName = "Supervisor_name";
            this.columnSuperviser.Name = "columnSuperviser";
            this.columnSuperviser.OptionsColumn.AllowEdit = false;
            this.columnSuperviser.OptionsColumn.ReadOnly = true;
            this.columnSuperviser.Visible = true;
            this.columnSuperviser.VisibleIndex = 0;
            this.columnSuperviser.Width = 110;
            // 
            // columnMerch
            // 
            this.columnMerch.AppearanceHeader.Options.UseTextOptions = true;
            this.columnMerch.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnMerch.Caption = "M1";
            this.columnMerch.FieldName = "MerchName";
            this.columnMerch.Name = "columnMerch";
            this.columnMerch.OptionsColumn.AllowEdit = false;
            this.columnMerch.OptionsColumn.ReadOnly = true;
            this.columnMerch.Visible = true;
            this.columnMerch.VisibleIndex = 1;
            this.columnMerch.Width = 110;
            // 
            // columnId
            // 
            this.columnId.AppearanceHeader.Options.UseTextOptions = true;
            this.columnId.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnId.Caption = "POC ID";
            this.columnId.FieldName = "ID";
            this.columnId.Name = "columnId";
            this.columnId.OptionsColumn.AllowEdit = false;
            this.columnId.OptionsColumn.AllowShowHide = false;
            this.columnId.OptionsColumn.ReadOnly = true;
            this.columnId.Visible = true;
            this.columnId.VisibleIndex = 2;
            this.columnId.Width = 100;
            // 
            // columnPOCName
            // 
            this.columnPOCName.AppearanceHeader.Options.UseTextOptions = true;
            this.columnPOCName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnPOCName.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.columnPOCName.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.columnPOCName.Caption = "POC Name";
            this.columnPOCName.FieldName = "POCName";
            this.columnPOCName.MinWidth = 100;
            this.columnPOCName.Name = "columnPOCName";
            this.columnPOCName.OptionsColumn.AllowEdit = false;
            this.columnPOCName.OptionsColumn.AllowShowHide = false;
            this.columnPOCName.OptionsColumn.ReadOnly = true;
            this.columnPOCName.Visible = true;
            this.columnPOCName.VisibleIndex = 3;
            this.columnPOCName.Width = 100;
            // 
            // columnDate
            // 
            this.columnDate.AppearanceHeader.Options.UseTextOptions = true;
            this.columnDate.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnDate.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.columnDate.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.columnDate.Caption = "Дата";
            this.columnDate.FieldName = "Date";
            this.columnDate.MinWidth = 100;
            this.columnDate.Name = "columnDate";
            this.columnDate.OptionsColumn.AllowEdit = false;
            this.columnDate.OptionsColumn.AllowShowHide = false;
            this.columnDate.OptionsColumn.ReadOnly = true;
            this.columnDate.Visible = true;
            this.columnDate.VisibleIndex = 4;
            this.columnDate.Width = 100;
            // 
            // columnTopic
            // 
            this.columnTopic.AppearanceHeader.Options.UseTextOptions = true;
            this.columnTopic.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnTopic.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.columnTopic.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.columnTopic.Caption = "Тема";
            this.columnTopic.ColumnEdit = this.repositoryTopicEdit;
            this.columnTopic.FieldName = "Topic";
            this.columnTopic.MinWidth = 100;
            this.columnTopic.Name = "columnTopic";
            this.columnTopic.OptionsColumn.AllowShowHide = false;
            this.columnTopic.Visible = true;
            this.columnTopic.VisibleIndex = 5;
            this.columnTopic.Width = 100;
            // 
            // repositoryTopicEdit
            // 
            this.repositoryTopicEdit.AutoHeight = false;
            this.repositoryTopicEdit.Name = "repositoryTopicEdit";
            // 
            // columnAction
            // 
            this.columnAction.AppearanceHeader.Options.UseTextOptions = true;
            this.columnAction.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnAction.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.columnAction.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.columnAction.Caption = "Действие";
            this.columnAction.ColumnEdit = this.repositoryActionLookUp;
            this.columnAction.FieldName = "ActionID";
            this.columnAction.MinWidth = 100;
            this.columnAction.Name = "columnAction";
            this.columnAction.OptionsColumn.AllowShowHide = false;
            this.columnAction.Visible = true;
            this.columnAction.VisibleIndex = 6;
            this.columnAction.Width = 100;
            // 
            // repositoryActionLookUp
            // 
            this.repositoryActionLookUp.AutoHeight = false;
            this.repositoryActionLookUp.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryActionLookUp.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("ID", "ID", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("ActionName", "Action")});
            this.repositoryActionLookUp.DisplayMember = "ActionName";
            this.repositoryActionLookUp.Name = "repositoryActionLookUp";
            this.repositoryActionLookUp.NullText = "Не задано";
            this.repositoryActionLookUp.ValueMember = "ID";
            this.repositoryActionLookUp.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(this.repositoryActionLookUp_EditValueChanging);
            // 
            // columPOCMoveTo
            // 
            this.columPOCMoveTo.AppearanceHeader.Options.UseTextOptions = true;
            this.columPOCMoveTo.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columPOCMoveTo.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.columPOCMoveTo.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.columPOCMoveTo.Caption = "Переместить в (ТТ)";
            this.columPOCMoveTo.ColumnEdit = this.repositoryPOCMoveToookUp;
            this.columPOCMoveTo.FieldName = "MoveToPOCID";
            this.columPOCMoveTo.MinWidth = 100;
            this.columPOCMoveTo.Name = "columPOCMoveTo";
            this.columPOCMoveTo.OptionsColumn.AllowEdit = false;
            this.columPOCMoveTo.OptionsColumn.AllowShowHide = false;
            this.columPOCMoveTo.OptionsColumn.ReadOnly = true;
            this.columPOCMoveTo.Visible = true;
            this.columPOCMoveTo.VisibleIndex = 7;
            this.columPOCMoveTo.Width = 100;
            // 
            // repositoryPOCMoveToookUp
            // 
            this.repositoryPOCMoveToookUp.AutoHeight = false;
            this.repositoryPOCMoveToookUp.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryPOCMoveToookUp.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("ID", "ID", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("POCName", "ТТ")});
            this.repositoryPOCMoveToookUp.DisplayMember = "POCName";
            this.repositoryPOCMoveToookUp.Name = "repositoryPOCMoveToookUp";
            this.repositoryPOCMoveToookUp.NullText = "Без перемещения";
            this.repositoryPOCMoveToookUp.ValueMember = "ID";
            // 
            // columnApproved
            // 
            this.columnApproved.AppearanceHeader.Options.UseTextOptions = true;
            this.columnApproved.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnApproved.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.columnApproved.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.columnApproved.Caption = "Утверждено";
            this.columnApproved.ColumnEdit = this.repositoryApproveCheck;
            this.columnApproved.FieldName = "Approved";
            this.columnApproved.MinWidth = 100;
            this.columnApproved.Name = "columnApproved";
            this.columnApproved.OptionsColumn.AllowShowHide = false;
            this.columnApproved.Visible = true;
            this.columnApproved.VisibleIndex = 8;
            this.columnApproved.Width = 100;
            // 
            // repositoryApproveCheck
            // 
            this.repositoryApproveCheck.AutoHeight = false;
            this.repositoryApproveCheck.Name = "repositoryApproveCheck";
            this.repositoryApproveCheck.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(this.repositoryApproveCheck_EditValueChanging);
            // 
            // columnCommentAction
            // 
            this.columnCommentAction.AppearanceHeader.Options.UseTextOptions = true;
            this.columnCommentAction.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnCommentAction.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.columnCommentAction.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.columnCommentAction.Caption = "Комментарий";
            this.columnCommentAction.ColumnEdit = this.repositoryCommentActionEdit;
            this.columnCommentAction.FieldName = "CommentAction";
            this.columnCommentAction.MinWidth = 100;
            this.columnCommentAction.Name = "columnCommentAction";
            this.columnCommentAction.OptionsColumn.AllowShowHide = false;
            this.columnCommentAction.Visible = true;
            this.columnCommentAction.VisibleIndex = 9;
            this.columnCommentAction.Width = 100;
            // 
            // repositoryCommentActionEdit
            // 
            this.repositoryCommentActionEdit.AutoHeight = false;
            this.repositoryCommentActionEdit.Name = "repositoryCommentActionEdit";
            // 
            // columnM2
            // 
            this.columnM2.AppearanceHeader.Options.UseTextOptions = true;
            this.columnM2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnM2.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.columnM2.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.columnM2.Caption = "Ответсвенный";
            this.columnM2.ColumnEdit = this.repositoryResponsibleLookUp;
            this.columnM2.FieldName = "SupervisorID";
            this.columnM2.MinWidth = 100;
            this.columnM2.Name = "columnM2";
            this.columnM2.OptionsColumn.AllowShowHide = false;
            this.columnM2.Visible = true;
            this.columnM2.VisibleIndex = 10;
            this.columnM2.Width = 100;
            // 
            // repositoryResponsibleLookUp
            // 
            this.repositoryResponsibleLookUp.AutoHeight = false;
            this.repositoryResponsibleLookUp.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryResponsibleLookUp.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Supervisor_ID", "ID", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Supervisor_name", "M2")});
            this.repositoryResponsibleLookUp.DisplayMember = "Supervisor_name";
            this.repositoryResponsibleLookUp.Name = "repositoryResponsibleLookUp";
            this.repositoryResponsibleLookUp.NullText = "Не задан";
            this.repositoryResponsibleLookUp.ValueMember = "Supervisor_ID";
            // 
            // columnDueDate
            // 
            this.columnDueDate.AppearanceHeader.Options.UseTextOptions = true;
            this.columnDueDate.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnDueDate.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.columnDueDate.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.columnDueDate.Caption = "Выполнить до";
            this.columnDueDate.FieldName = "DueDate";
            this.columnDueDate.MinWidth = 100;
            this.columnDueDate.Name = "columnDueDate";
            this.columnDueDate.OptionsColumn.AllowEdit = false;
            this.columnDueDate.OptionsColumn.AllowShowHide = false;
            this.columnDueDate.OptionsColumn.ReadOnly = true;
            this.columnDueDate.Visible = true;
            this.columnDueDate.VisibleIndex = 11;
            this.columnDueDate.Width = 100;
            // 
            // columnStatus
            // 
            this.columnStatus.AppearanceHeader.Options.UseTextOptions = true;
            this.columnStatus.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnStatus.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.columnStatus.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.columnStatus.Caption = "Статус";
            this.columnStatus.ColumnEdit = this.repositoryStatusLookUp;
            this.columnStatus.FieldName = "StatusID";
            this.columnStatus.MinWidth = 100;
            this.columnStatus.Name = "columnStatus";
            this.columnStatus.OptionsColumn.AllowEdit = false;
            this.columnStatus.OptionsColumn.AllowShowHide = false;
            this.columnStatus.Visible = true;
            this.columnStatus.VisibleIndex = 12;
            this.columnStatus.Width = 100;
            // 
            // repositoryStatusLookUp
            // 
            this.repositoryStatusLookUp.AutoHeight = false;
            this.repositoryStatusLookUp.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryStatusLookUp.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("ID", "ID", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("StatusName", "Status")});
            this.repositoryStatusLookUp.DisplayMember = "StatusName";
            this.repositoryStatusLookUp.Name = "repositoryStatusLookUp";
            this.repositoryStatusLookUp.NullText = "";
            this.repositoryStatusLookUp.ValueMember = "ID";
            // 
            // columnCommentStatus
            // 
            this.columnCommentStatus.AppearanceHeader.Options.UseTextOptions = true;
            this.columnCommentStatus.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnCommentStatus.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.columnCommentStatus.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.columnCommentStatus.Caption = "Комментарий";
            this.columnCommentStatus.ColumnEdit = this.repositoryCommentStatusEdit;
            this.columnCommentStatus.FieldName = "CommentStatus";
            this.columnCommentStatus.MinWidth = 100;
            this.columnCommentStatus.Name = "columnCommentStatus";
            this.columnCommentStatus.OptionsColumn.AllowShowHide = false;
            this.columnCommentStatus.Visible = true;
            this.columnCommentStatus.VisibleIndex = 13;
            this.columnCommentStatus.Width = 100;
            // 
            // repositoryCommentStatusEdit
            // 
            this.repositoryCommentStatusEdit.AutoHeight = false;
            this.repositoryCommentStatusEdit.Name = "repositoryCommentStatusEdit";
            // 
            // barManager
            // 
            this.barManager.AllowCustomization = false;
            this.barManager.AllowMoveBarOnToolbar = false;
            this.barManager.AllowQuickCustomization = false;
            this.barManager.AllowShowToolbarsPopup = false;
            this.barManager.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.standaloneTool});
            this.barManager.DockControls.Add(this.barDockControlTop);
            this.barManager.DockControls.Add(this.barDockControlBottom);
            this.barManager.DockControls.Add(this.barDockControlLeft);
            this.barManager.DockControls.Add(this.barDockControlRight);
            this.barManager.Form = this;
            this.barManager.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barButtonApproveAll});
            this.barManager.MaxItemId = 1;
            // 
            // standaloneTool
            // 
            this.standaloneTool.BarName = "Tools";
            this.standaloneTool.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Top;
            this.standaloneTool.DockCol = 0;
            this.standaloneTool.DockRow = 0;
            this.standaloneTool.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.standaloneTool.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonApproveAll, true)});
            this.standaloneTool.Text = "Tools";
            // 
            // barButtonApproveAll
            // 
            this.barButtonApproveAll.Caption = "Утвердить все ТТ";
            this.barButtonApproveAll.Hint = "Утвердить все ТТ";
            this.barButtonApproveAll.Id = 0;
            this.barButtonApproveAll.Name = "barButtonApproveAll";
            this.barButtonApproveAll.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonApproveAll_ItemClick);
            // 
            // ActionPlanControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.Controls.Add(this.gridControl);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Name = "ActionPlanControl";
            this.Size = new System.Drawing.Size(769, 389);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryTopicEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryActionLookUp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryPOCMoveToookUp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryApproveCheck)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryCommentActionEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryResponsibleLookUp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryStatusLookUp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryCommentStatusEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView;
        private DevExpress.XtraGrid.Columns.GridColumn columnPOCName;
        private DevExpress.XtraGrid.Columns.GridColumn columnDate;
        private DevExpress.XtraGrid.Columns.GridColumn columnTopic;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryTopicEdit;
        private DevExpress.XtraGrid.Columns.GridColumn columnAction;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryActionLookUp;
        private DevExpress.XtraGrid.Columns.GridColumn columPOCMoveTo;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryPOCMoveToookUp;
        private DevExpress.XtraGrid.Columns.GridColumn columnApproved;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryApproveCheck;
        private DevExpress.XtraGrid.Columns.GridColumn columnCommentAction;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryCommentActionEdit;
        private DevExpress.XtraGrid.Columns.GridColumn columnM2;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryResponsibleLookUp;
        private DevExpress.XtraGrid.Columns.GridColumn columnDueDate;
        private DevExpress.XtraGrid.Columns.GridColumn columnStatus;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryStatusLookUp;
        private DevExpress.XtraGrid.Columns.GridColumn columnCommentStatus;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryCommentStatusEdit;
        private DevExpress.XtraGrid.Columns.GridColumn columnId;
        private DevExpress.XtraBars.BarManager barManager;
        private DevExpress.XtraBars.Bar standaloneTool;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarButtonItem barButtonApproveAll;
        private DevExpress.XtraGrid.Columns.GridColumn columnSuperviser;
        private DevExpress.XtraGrid.Columns.GridColumn columnMerch;


    }
}
