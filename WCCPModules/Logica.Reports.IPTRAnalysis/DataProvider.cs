﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Logica.Reports.DataAccess;

namespace Logica.Reports.IPTRA
{
    /// <summary>
    /// 
    /// </summary>
    internal static class DataProvider
    {
        /// <summary>
        /// Gets the action plan table.
        /// </summary>
        /// <value>The action plan table.</value>
        public static DataTable GetActionPlanTable(List<SqlParameter> list)
        {
            if (list != null && list.Count > 0)
            {
                DataSet dataSet = DataAccessLayer.ExecuteStoredProcedure(Constants.SP_GET_ACTION_PLAN_SUMMARY,
                    list.ToArray());

                if (dataSet.Tables.Count > 0 && dataSet.Tables[0].Columns.Count > 0)
                {
                    DataTable dataTable = dataSet.Tables[0];
                    dataTable.PrimaryKey = new DataColumn[] { dataTable.Columns[0] }; // ID column
                    return dataTable;
                }
            }
            return null;
        }

        /// <summary>
        /// Gets the access level.
        /// </summary>
        /// <value>The access level.</value>
        public static AccessLevel GetAccessLevel(DateTime date)
        {
            return (AccessLevel)DataAccessLayer.ExecuteScalarStoredProcedure(
                Constants.SP_GET_ACCESS_LEVEL,
                new SqlParameter[] 
                {
                    new SqlParameter(Constants.SP_PARAM_ACCESS_LEVEL_DATE, date) 
                }
            );
        }

        /// <summary>
        /// Gets the m2 list look up.
        /// </summary>
        /// <value>The m2 list look up.</value>
        public static DataTable M2ListLookUp(int reportId)
        {
            DataSet dataSet = DataAccessLayer.ExecuteStoredProcedure(Constants.SP_GET_SUPERVISORS_LIST,
                    new SqlParameter[] { new SqlParameter(Constants.SP_PARAM_REPORT_ID, reportId) });
            if (dataSet.Tables.Count > 0)
            {
                return dataSet.Tables[0];
            }
            return null;
        }

        /// <summary>
        /// Gets the action list look up.
        /// </summary>
        /// <value>The action list look up.</value>
        public static DataTable ActionListLookUp
        {
            get
            {
                DataSet dataSet = DataAccessLayer.ExecuteStoredProcedure(Constants.SP_GET_ACTION_LIST);
                if (dataSet.Tables.Count > 0)
                {
                    return dataSet.Tables[0];
                }
                return null;
            }
        }

        /// <summary>
        /// Gets the status list look up.
        /// </summary>
        /// <value>The status list look up.</value>
        public static DataTable StatusListLookUp
        {
            get
            {
                DataSet dataSet = DataAccessLayer.ExecuteStoredProcedure(Constants.SP_GET_STATUS_LIST);
                if (dataSet.Tables.Count > 0)
                {
                    return dataSet.Tables[0];
                }
                return null;
            }
        }

        /// <summary>
        /// Gets the reallocation POC table.
        /// </summary>
        /// <value>The reallocation POC table.</value>
        public static DataTable ReallocationPOCTable
        {
            get
            {
                DataSet dataSet = DataAccessLayer.ExecuteStoredProcedure(Constants.SP_GET_REALLOCATION_POC_LIST);
                if (dataSet.Tables.Count > 0)
                {
                    return dataSet.Tables[0];
                }
                return null;
            }
        }

        /// <summary>
        /// Updates the action plan record.
        /// </summary>
        /// <param name="paramsList">The params list.</param>
        public static void UpdateActionPlanRecord(List<SqlParameter> paramsList)
        {
            if (paramsList != null)
            {
                DataAccessLayer.ExecuteNonQueryStoredProcedure(Constants.SP_SET_ACTION_PLAN_SUMMARY, paramsList.ToArray());
            }
        }

        /// <summary>
        /// Gets the channel type list.
        /// </summary>
        /// <value>The channel type list.</value>
        public static DataTable ChannelTypeList
        {
            get
            {
                DataTable dataTable = null;
                DataSet dataSet = DataAccessLayer.ExecuteStoredProcedure(Constants.SP_GET_CHANNEL_TYPE_LIST,
                    new SqlParameter[] { });
                if (dataSet != null && dataSet.Tables.Count > 0)
                {
                    dataTable = dataSet.Tables[0];
                }
                return dataTable;
            }
        }

        /// <summary>
        /// Gets the equipment class list.
        /// </summary>
        /// <value>The equipment class list.</value>
        public static DataTable EquipmentClassList
        {
            get
            {
                DataTable dataTable = null;
                DataSet dataSet = DataAccessLayer.ExecuteStoredProcedure(Constants.SP_GET_EQUIPMENT_CLASS_LIST, new SqlParameter[] { });
                if (dataSet != null && dataSet.Tables.Count > 0)
                {
                    dataTable = dataSet.Tables[0];
                }
                return dataTable;
            }
        }

        /// <summary>
        /// Gets the staff tree.
        /// </summary>
        /// <param name="sqlParams">The SQL params.</param>
        /// <returns></returns>
        public static DataTable GetStaffTree(SqlParameter[] sqlParams)
        {
            DataTable dataTable = null;
            DataSet dataSet = null;

            if (sqlParams != null)
            {
                dataSet = DataAccessLayer.ExecuteStoredProcedure(Constants.SP_GET_STAFF_TREE, sqlParams);
            }
            else
            {
                dataSet = DataAccessLayer.ExecuteStoredProcedure(Constants.SP_GET_STAFF_TREE, new SqlParameter[] { });
            }

            if (dataSet != null && dataSet.Tables.Count > 0)
            {
                dataTable = dataSet.Tables[0];
            }
            return dataTable;
        }
    }
}
