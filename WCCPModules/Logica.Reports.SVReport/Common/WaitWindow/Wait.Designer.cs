namespace Logica.Reports.Common.WaitWindow
{
    partial class Wait
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Wait));
            this.pnClient = new DevExpress.XtraEditors.PanelControl();
            this.pbWait = new System.Windows.Forms.PictureBox();
            this.lbMessage = new DevExpress.XtraEditors.LabelControl();
            this.lbElapsedTimeValue = new DevExpress.XtraEditors.LabelControl();
            this.lbSpentTime = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.pnClient)).BeginInit();
            this.pnClient.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbWait)).BeginInit();
            this.SuspendLayout();
            // 
            // pnClient
            // 
            this.pnClient.Controls.Add(this.lbElapsedTimeValue);
            this.pnClient.Controls.Add(this.lbSpentTime);
            this.pnClient.Controls.Add(this.pbWait);
            this.pnClient.Controls.Add(this.lbMessage);
            this.pnClient.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnClient.Location = new System.Drawing.Point(0, 0);
            this.pnClient.Name = "pnClient";
            this.pnClient.Size = new System.Drawing.Size(345, 59);
            this.pnClient.TabIndex = 0;
            // 
            // pbWait
            // 
            this.pbWait.Image = ((System.Drawing.Image)(resources.GetObject("pbWait.Image")));
            this.pbWait.Location = new System.Drawing.Point(4, 17);
            this.pbWait.Name = "pbWait";
            this.pbWait.Size = new System.Drawing.Size(24, 24);
            this.pbWait.TabIndex = 1;
            this.pbWait.TabStop = false;
            // 
            // lbMessage
            // 
            this.lbMessage.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lbMessage.Appearance.Options.UseFont = true;
            this.lbMessage.ImageAlignToText = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this.lbMessage.Location = new System.Drawing.Point(37, 18);
            this.lbMessage.Name = "lbMessage";
            this.lbMessage.Size = new System.Drawing.Size(308, 16);
            this.lbMessage.TabIndex = 0;
            this.lbMessage.Text = "���������, ����������.  ���� ��������� ������...";
            // 
            // lbElapsedTimeValue
            // 
            this.lbElapsedTimeValue.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lbElapsedTimeValue.Appearance.Options.UseFont = true;
            this.lbElapsedTimeValue.Location = new System.Drawing.Point(292, 40);
            this.lbElapsedTimeValue.Name = "lbElapsedTimeValue";
            this.lbElapsedTimeValue.Size = new System.Drawing.Size(48, 13);
            this.lbElapsedTimeValue.TabIndex = 3;
            this.lbElapsedTimeValue.Text = "00:00:00";
            this.lbElapsedTimeValue.Visible = false;
            // 
            // lbSpentTime
            // 
            this.lbSpentTime.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lbSpentTime.Appearance.ForeColor = System.Drawing.Color.DarkRed;
            this.lbSpentTime.Appearance.Options.UseFont = true;
            this.lbSpentTime.Appearance.Options.UseForeColor = true;
            this.lbSpentTime.Location = new System.Drawing.Point(240, 40);
            this.lbSpentTime.Name = "lbSpentTime";
            this.lbSpentTime.Size = new System.Drawing.Size(48, 13);
            this.lbSpentTime.TabIndex = 2;
            this.lbSpentTime.Text = "������:";
            this.lbSpentTime.Visible = false;
            // 
            // Wait
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(345, 59);
            this.Controls.Add(this.pnClient);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Wait";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "���������";
            ((System.ComponentModel.ISupportInitialize)(this.pnClient)).EndInit();
            this.pnClient.ResumeLayout(false);
            this.pnClient.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbWait)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl pnClient;
        private DevExpress.XtraEditors.LabelControl lbMessage;
        private System.Windows.Forms.PictureBox pbWait;
        private DevExpress.XtraEditors.LabelControl lbElapsedTimeValue;
        private DevExpress.XtraEditors.LabelControl lbSpentTime;
    }
}