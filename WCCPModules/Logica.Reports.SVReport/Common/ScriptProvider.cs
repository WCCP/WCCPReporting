namespace Logica.Reports.Common
{
    public static class ScriptProvider
    {
        #region Class Properties

        public static string ActiveOutletsTOP10
        {
            get
            {
                return
                    @"
-- =============================================
-- Author:		Developer 1
-- Create date: 18-11-2009
-- Description:	Selects all needed data for 'Failed visits analysis' report.
-- =============================================

-- Input parameters
-- ******************* Data range for the report *******************
-- @lowerDate as datetime 
-- @upperDate as datetime
-- *****************************************************************

-- ********************* Supervisor filter ID **********************
-- @supervisorId as INTEGER
-- *****************************************************************

-- ********************* Merchandisers ID list *********************
-- @merchandisersIDs as nvarchar
-- @merchandisersCnt as INTEGER
-- It is a list of IDs. The list is a fixed width (10 characters) 
-- string like '         2,     12345,1234567898'
-- the function dbo.fnDW_Common_SplitText must be used to transform 
-- the string IDs into a table representation
-- @merchandisersCnt - count of mrecandisers should be returned
-- *****************************************************************

--DECLARE @lowerDate AS DATETIME
--DECLARE @upperDate AS DATETIME
--DECLARE @supervisorId AS INTEGER
--DECLARE @merchandisersIDs AS NVARCHAR(4000)
--DECLARE @merchandisersCnt AS INTEGER

--set @merchandisersIDs = '  14500001,  14500002,  14500003,  14500004'
--set @merchandisersCnt = 0
--set @supervisorId = NULL
--set @lowerDate = '2009-10-01'
--set @upperDate = '2009-10-30'

SELECT pt.ProductCn_ID 
	 , pt.Region_ID
	 , pt.StartDate
	 , pt.EndDate 
INTO #top10
FROM [LDB].[dbo].DW_ProductCnTop pt

	WHERE (@lowerDate BETWEEN StartDate AND EndDate) OR
		  (@upperDate BETWEEN StartDate AND EndDate)


-- Define Merchandisers IDs list
CREATE TABLE #merchandisersIDs (id INT NOT NULL)
BEGIN
	DECLARE @s BIGINT, @e INT, @id INT
	DECLARE @nv NVARCHAR(10), @cnt INT
	DECLARE @string NVARCHAR(4000)
    DECLARE @merch_type INT
	
	If (@merchandisersCnt <> 0 )
		BEGIN
			SET @string = @merchandisersIDs
			SET @s = 1
			SET @cnt = @merchandisersCnt
			WHILE @cnt > 0
			BEGIN
				SET @nv = SUBSTRING(@string, @s, 10)
				SET @s = @s + 11
				SET @id  = cast(@nv as int )
				SELECT @merch_type = t.UserType_id 
                FROM dbo.tblMerchandisers  m 
                     INNER JOIN tblMerchandiserUserTypes t ON t.merch_id = m.merch_id AND m.Merch_id = @id and t.status = 2
				IF @merch_type IN (SELECT UserType FROM DW_FSM_UserTypes dfut WHERE GETDATE() BETWEEN dfut.StartDate AND dfut.EndDate)  -- Added by Huch R 
                BEGIN                
                    INSERT #merchandisersIDs VALUES (@id) 
                END
				SET @cnt = @cnt -1
			END
		END
	ELSE	
		BEGIN 
			INSERT #merchandisersIDs VALUES (-1) 
		END

END

-- �������� ��, ������ ����������� �� � ��������� � ������
SELECT ROW_NUMBER() OVER (PARTITION BY OL_id ORDER BY (SELECT 1))  RelatedMerch_CNT -- ��������� �� �-�� ��, ������������� ����� �� 
    , OL_id
    , Merch_Id
    , ActivityType  
INTO #OL_Merch_Chanal
FROM
(
	SELECT ol.OL_id 
		, MAX(r.Merch_Id) Merch_Id -- �������� �� � ������� ID
		, ActivityType
	FROM  dbo.tblRoutes r 
		INNER JOIN dbo.tblOutletRoutes olr ON r.Route_id=olr.Route_id 
											AND olr.Status = 2
		INNER JOIN dbo.tblOutlets ol ON olr.OL_id=ol.OL_id
		INNER JOIN 
		( -- ���������  ���� ������� ����� ��
			SELECT  ROW_NUMBER() OVER (PARTITION BY Merch_Id ORDER BY (SELECT 1)) MerchActivity_ID
				 , Merch_ID
				 , ActivityType
			FROM tblSyncStatusActTypesLinks 
			INNER JOIN #merchandisersIDs AS IDs ON IDs.id = 
						CASE WHEN @merchandisersCnt = 0 THEN -1 ELSE Merch_ID END
		) matl ON matl.MerchActivity_ID = 1  -- �������� ������ ���������� Activity (�����) � ��
			  AND r.Merch_Id = matl.Merch_Id 
		GROUP BY ol.OL_id, ActivityType
			 
) AS OL_Merch_Chanal 


SELECT OL_ID
--	, Merch_Id
     , COUNT(OL_ID) as CNT
INTO #tmpOch
FROM (
		SELECT och.OL_ID
--			 , och.Merch_Id
			 , ProductCn_Id
			, pt10.StartDate
			, pt10.EndDate
			 
--		   , ROW_NUMBER() OVER (PARTITION BY och.OL_ID,Merch_Id,ProductCn_Id  ORDER BY (SELECT 1)) Cnt
		FROM tblOutletCardH och
		INNER JOIN tblOutlets ol ON och.OL_id = ol.OL_id 
			   AND och.OLCardDate BETWEEN @lowerDate AND @upperDate 
		INNER JOIN tblOutletOrderH ooh ON och.OLCard_id = ooh.OLCard_id
		INNER JOIN tblOutletOrderD ood ON ood.OrderNo = ooh.OrderNo 
		INNER JOIN tblProducts p ON p.Product_Id = ood.Product_Id 
		INNER JOIN tblSalOutH soh ON soh.OrderNo = ooh.OrderNo AND soh.Status <> 9 
		INNER JOIN tblSalOutD sod ON sod.Invoice_Id = soh.Invoice_Id 
							AND sod.Product_Id = ood.Product_Id
		INNER JOIN #top10 pt10 ON CAST(p.ProductCn_Id AS INT) = pt10.ProductCn_ID
								AND och.OLCardDate BETWEEN pt10.StartDate AND pt10.EndDate

		GROUP BY och.OL_ID
--				, och.Merch_Id 
				, ProductCn_Id
				, pt10.StartDate
				, pt10.EndDate
			
) AS d
GROUP BY OL_ID
--, Merch_Id
HAVING COUNT(*) >= (SELECT COUNT(*) FROM #top10)


SELECT sp.Cust_Name -- ����� �������������
 	 , s.Supervisor_name -- ����������
	 , c.City_Name	 -- �����
	 , d.District_Name -- �������
	 , NULL AS Region_Name -- ����� (��) TODO: join to tblCountryArea
	 , a.Area_Name  -- ����� � ������ (��) 
	 , NULL AS Territory_Type -- ��� ���������� TODO: join to  tblSettlement
	 , olmc.Merch_Id
     , m.MerchName -- ��
	 , ms.LValue AS MerchandiserStatus -- ������ ��
	 , och.OL_id AS  OL_Has -- Outlet is visited by Merchandiser 
     , och.OL_id -- ��� ��
 	 , ol.OL_Code -- ������� �����
	 , ols.LValue as OutletsSatus -- ������ ��
	 , olt.OLtype_name --�����
	 , olst.OLSubTypeName -- ��������
	 , ol.OLName --���� ���
	 , ol.OLDeliveryAddress -- ����. �����
	 , ol.OLTradingName -- ��. ���
	 , ol.OLAddress --��. �����
	 , pf.LValue AS ProximityFactor -- ������ ��������
FROM #OL_Merch_Chanal olmc 
INNER JOIN 	#tmpOch  AS  och  ON och.OL_id = olmc.OL_id	
					-- AND olmc.Merch_Id = 
					--(
					--CASE 
					--	WHEN (NOT EXISTS( -- ��������� ��-�� ��, ���������� �� ��
					--					SELECT 1 
					--					FROM #OL_Merch_Chanal olmc1 
					--					WHERE olmc1.OL_id = olmc.OL_id 
					--					  AND olmc1.RelatedMerch_CNT = 2 -- ���������� ��������� �� ��� ������ ��
					--					)
					--		  ) 
					--	THEN -- �� �� ������� ������ ���� ��.
					--		 -- �� ����������� ��, ���������� �����, ���������� ����� �� ���� ��, ������� ������� �� �� ����� �������
					--		olmc.Merch_ID 
					--	ELSE -- �� �� �������� ��������� ��.
					--		CASE
					--			WHEN ( -- ��������� ��, �������� �� �� � ��
					--				  EXISTS(
					--						SELECT 1 
					--						FROM #OL_Merch_Chanal olmc2
					--						WHERE olmc2.OL_id = olmc.OL_id 
					--							AND olmc2.Merch_Id = och.Merch_Id -- ��, ��������
					--						)
					--				 )
					--			THEN --  
					--				och.Merch_Id 
					--			ELSE -- �������� �� �� '�������' � ���� ��� ����� � �������� �� �� '���������� ����� ��������'. 
					--				 -- �������� ������ ���������� ����� � ��, ������� �������� �����
					--				(SELECT TOP 1 olmc3.Merch_ID
					--				FROM #OL_Merch_Chanal olmc3
					--				INNER JOIN tblSyncStatusActTypesLinks matl ON olmc3.ActivityType = matl.ActivityType 
					--														  AND matl.Merch_id = och.Merch_id
					--														  AND olmc3.OL_id = och.OL_id
					--				)
					--		END
					--END
					--)

INNER JOIN tblMerchandisers m ON m.Merch_id = olmc.Merch_id
INNER JOIN tblGloballookup ms ON ms.LKey = m.Status 
							  AND ms.TableName = 'tblMerchandisers' 
							  AND ms.FieldName = 'Status'
INNER JOIN tblOutlets ol ON olmc.OL_id = ol.OL_id
INNER JOIN tblOutletSubTypes olst ON ol.OLSubType_id = olst.OLSubType_id 
INNER JOIN tblOutLetTypes olt ON olst.OLType_ID = olt.OLType_id 
INNER JOIN tblGloballookup ols ON ols.LKey = ol.Status 
							  AND ols.TableName = 'tblOutlets' 
							  AND ols.FieldName = 'Status'
INNER JOIN tblSupervisors s ON m.Supervisor_ID = s.Supervisor_id AND s.Status <> 9
						  AND (@supervisorId is NULL OR @supervisorId = m.Supervisor_ID) 
INNER JOIN 
	(
		SELECT  ROW_NUMBER() OVER (PARTITION BY Supervisor_id ORDER BY (SELECT 1)) CustomerOrder
			 ,  Supervisor_id
			 ,  Cust_Id
		FROM tblCustomerSupervisors tcs
		WHERE (@supervisorId is NULL OR @supervisorId = tcs.Supervisor_ID)  
	) AS cs ON cs.Supervisor_id = s.Supervisor_id	
			AND cs.CustomerOrder = 1	-- �.�. ��� ����������� ����� ������� ��, 
										-- �������� ������ ���������� �� ������������� ��
INNER JOIN tblCustomers sp ON cs.Cust_id = sp.Cust_Id 
INNER JOIN tblAreas a ON ol.Area_id = a.Area_Id
INNER JOIN tblCities c ON c.City_id = a.City_Id
INNER JOIN tblDistricts d ON d.District_id = c.District_id
INNER JOIN tblGloballookup pf ON  (pf.LKey = CASE WHEN ol.Proximity IS NULL 
									THEN -3 
									ELSE ol.Proximity
									END
								   ) 
								AND pf.TableName = 'tblOutlets' 
								AND pf.FieldName = 'Proximity'


DROP TABLE #OL_Merch_Chanal
DROP TABLE #tmpOch
DROP TABLE #merchandisersIDs
DROP TABLE #top10
";
            }
        }

        public static string CoverageAnalysis
        {
            get
            {
                return
                    @"
-- =============================================
-- Author:		Developer 2
-- Create date: 18-11-2009
-- Description:	Selects all needed data for 'Failed visits analysis' report.
-- 
-- Changed
-- Author:		Developer 1
-- Create date: 24-11-2009
-- Description:	Selects all data for 'Coverage analysis' report.
--
-- Changed
-- Author:		Developer 6
-- Create date: 19-03-2010
-- Description:	Add some tmp tables and indexes on it to improve performance.
--              The following fiwld were removed from report result set:
--              ��, District_Name, SKU, SKU Code
-- Changed
-- Author:		Developer 6
-- Create date: 09-04-2010
-- Description:	add new selection for not visited OL and add filter 'WHERE OLSchedule IS NOT NULL' to result query
-- =============================================

-- Input parameters
-- ******************* Data range for the report *******************
-- @lowerDate as datetime 
-- @upperDate as datetime
-- *****************************************************************

-- ********************* Supervisor filter ID **********************
-- @supervisorId as INTEGER
-- *****************************************************************

-- ********************* Merchandisers ID list *********************
-- @merchandisersIDs as nvarchar
-- @merchandisersCnt as INTEGER
-- It is a list of IDs. The list is a fixed width (10 characters) 
-- string like '         2,     12345,1234567898'
-- the function dbo.fnDW_Common_SplitText must be used to transform 
-- the string IDs into a table representation
-- @merchandisersCnt - count of mrecandisers should be returned
-- *****************************************************************

/*-------------------------------------------*/
--DECLARE @lowerDate AS DATETIME
--DECLARE @upperDate AS DATETIME
----
--DECLARE @supervisorId AS INTEGER
--DECLARE @merchandisersIDs AS NVARCHAR(4000)
--DECLARE @merchandisersCnt AS INTEGER
----
--SET @merchandisersIDs = '        33,        35,        36,        37,        40,        41,       128,    600007'
--SET @merchandisersCnt = 1
--SET @supervisorId = NULL
--SET @lowerDate = '2009-10-01'
--SET @upperDate = '2009-10-31'
/*-------------------------------------------*/

set datefirst 1
set dateformat ymd

DECLARE @currentDate AS DATETIME
----Define planed visits by day
SET @currentDate = @lowerDate
CREATE TABLE #WeekDays (date DateTime NOT NULL, [weekDay] tinyInt )

WHILE @currentDate < @upperDate
BEGIN
INSERT INTO #WeekDays (date, [weekDay])
	VALUES (@currentDate, datePart(dw, @currentDate))
	SET @currentDate = @currentDate + 1
END 

SELECT m.*, s.Supervisor_name
INTO #merch
FROM 
tblMerchandisers m 
INNER JOIN tblSupervisors s ON s.Supervisor_id=m.Supervisor_ID AND s.Status <> 9 AND m.[Status]<>9 AND (@supervisorId is NULL OR @supervisorId = m.Supervisor_ID)

-- Define Merchandisers IDs list
CREATE TABLE #merchandisersIDs (id INT NOT NULL PRIMARY KEY)

If (@merchandisersCnt <> 0 )
	BEGIN
		INSERT INTO #merchandisersIDs
		SELECT ID from dbo.Parse_IntList(@merchandisersIDs)
		
		DELETE #merchandisersIDs WHERE id NOT IN 
		(SELECT m.Merch_id
		FROM dbo.tblMerchandisers  m 
			INNER JOIN tblMerchandiserUserTypes t ON t.merch_id = m.merch_id AND t.status = 2 
			INNER JOIN DW_FSM_UserTypes dfut ON t.UserType_id = dfut.UserType
		WHERE GETDATE() BETWEEN dfut.StartDate AND dfut.EndDate)			
	END
ELSE	
	BEGIN 
		INSERT #merchandisersIDs VALUES (-1) 
	END


-- �������� ��, ������ ����������� �� � ��������� � ������
SELECT ROW_NUMBER() OVER (PARTITION BY OL_id ORDER BY (SELECT 1))  RelatedMerch_CNT -- ��������� �� �-�� ��, ������������� ����� �� 
    , OL_id
    , Merch_Id
    , ActivityType  
INTO #OL_Merch_Chanal
FROM
(
	SELECT olr.OL_id 
		, MAX(r.Merch_Id) Merch_Id -- �������� �� � ������� ID
		, ActivityType
	FROM  dbo.tblRoutes r 
		INNER JOIN dbo.tblOutletRoutes olr ON r.Route_id=olr.Route_id 
											AND olr.Status = 2 AND r.Status = 2
		INNER JOIN 
		( -- ���������  ���� ������� ����� ��
			SELECT  ROW_NUMBER() OVER (PARTITION BY Merch_Id ORDER BY (SELECT 1)) MerchActivity_ID
				 , Merch_ID
				 , ActivityType
			FROM tblSyncStatusActTypesLinks 
			    INNER JOIN #merchandisersIDs AS IDs ON IDs.id = 
						CASE 
						    WHEN @merchandisersCnt = 0 THEN -1 
						    ELSE Merch_ID 
						END

		) matl ON matl.MerchActivity_ID = 1  -- �������� ������ ���������� Activity (�����) � ��
			  AND r.Merch_Id = matl.Merch_Id 
		GROUP BY olr.OL_id, ActivityType
			 
) AS OL_Merch_Chanal 

-- Define schedules for merchandisers
SELECT DISTINCT olr.OL_id
	 , r.Merch_Id 
	 , wd.Date AS OLCardDate
INTO #Schedule
FROM #WeekDays wd
    INNER JOIN dbo.tblRoutes r ON ISNUMERIC(SUBSTRING(r.RouteName, 1, 1)) <> 0 
               AND (wd.[weekDay] = SUBSTRING(r.RouteName, 1, 1) or (wd.[weekDay] = case when SUBSTRING(r.RouteName, 1, 1) in ('8','9') then 7 else null end) )
               --AND (wd.[weekDay] = SUBSTRING(r.RouteName, 1, 1) or SUBSTRING(r.RouteName, 1, 1) in ('8','9'))
               AND r.Status = 2
    INNER JOIN dbo.tblOutletRoutes olr ON r.Route_id = olr.Route_id 
									    AND olr.Status = 2
    INNER JOIN #OL_Merch_Chanal olmc ON olmc.Merch_Id = r.Merch_Id AND olmc.OL_Id = olr.OL_Id
    INNER JOIN tbloutlets o ON olr.ol_id = o.ol_id AND o.status <> 9
    INNER JOIN #merchandisersIDs AS IDs ON IDs.id = 
			    CASE 
			        WHEN @merchandisersCnt = 0 THEN -1 
			        ELSE r.Merch_id 
			    END
        
CREATE UNIQUE CLUSTERED INDEX [PK_#Schedule] ON #Schedule(OL_id, Merch_Id, OLCardDate)
-- �������� ��������� � ��� ���������� � ��� ������

SELECT p.Product_Id
	 , p.ProductName -- ���
	 , pt.ProdTypeName -- ����
	 , pg.ProdGroupName -- �����
	 , u.Unit_Name -- ��� ��������
	 , p.productVolume * 10 AS Packing_Capacity -- ������� ��������
	 , CASE 
	        WHEN ptp.ProductName LIKE '%BNR%'  THEN  CASE 
	                                                    WHEN ptp.ProductName LIKE '% ��%' THEN 'BNR ��' 
			                                            ELSE CASE 
			                                                    WHEN ptp.ProductName LIKE '% InBev%' THEN 'BNR InBev' 
				                                                ELSE 'BNR' 
				                                             END 
			                                         END
		    ELSE 'Other' 
		END  AS Packing_Type -- ��� ��������
	 , pc.ProductCnName -- ������������ ��������������� ���������
	 , p.ProductCn_Id -- Combined product Id
	 , 0 AS BelongingTOPSKU 
INTO #products
FROM tblProducts p 
    INNER JOIN tblProductTypes pt ON pt.ProductType_id = p.ProductType_id 
    INNER JOIN tblProductGroups pg ON pt.ProdGroup_ID = pg.ProdGroup_ID 
    INNER JOIN tblUnits u ON p.Unit_ID = u.Unit_ID 
    INNER JOIN tblProductCombine pc ON p.ProductCn_Id = pc.ProductCn_Id 
    LEFT  JOIN tblProducts ptp ON p.Tare_Id = ptp.Product_Id

CREATE UNIQUE CLUSTERED INDEX [PK_#PRODUCT_PRODUCT_ID] ON #products (Product_id)
CREATE INDEX [IX_#PRODUCT_PRODUCTCN_ID] ON #products (ProductCn_ID)

SELECT och.OL_ID
       , och.Merch_ID 
       , och.OLCard_Id
       , och.OLCardDate
       , od.Product_Id
INTO #od
  FROM tblOutletDistribution od
      INNER JOIN tblOutletCardH och ON och.OLCard_id = od.OLCard_id 
                 AND od.IsPresent > 0 
		         AND CONVERT(datetime,CONVERT(VARCHAR(10),och.OLCardDate,104),104) BETWEEN @lowerDate AND @upperDate
 CREATE INDEX [IX_#od] ON #od (OLCard_Id,Product_Id)


-- �������� �������� ������ � �������� ��������� � ���� ������ �� ���������
SELECT 	DISTINCT
        olmc.OL_ID
      , olmc.Merch_ID
      , CASE 
            WHEN och.OLCard_Id IS NULL THEN  od.OLCard_Id 
            ELSE och.OLCard_Id 
        END OLCard_Id
      , CASE 
            WHEN och.OLCardDate IS NULL THEN od.OLCardDate 
            ELSE och.OLCardDate 
        END OLCardDate
      , CASE 
            WHEN och.Product_Id IS NULL THEN od.Product_Id 
            ELSE och.Product_Id 
        END Product_Id
      , och.OL_ID AS OL_IsActive
      , od.OL_ID AS OL_HasRemains
      , p.ProductName      -- ���
      , p.ProdTypeName     -- ����
      , p.ProdGroupName    -- �����
      , p.Unit_Name        -- ��� ��������
      , p.Packing_Capacity -- ������� ��������
      , p.Packing_Type     -- ��� ��������
      , p.ProductCnName    -- ������������ ��������������� ���������
INTO #tmpOch
FROM 
#od AS od
FULL OUTER JOIN 
(SELECT och.OL_ID
   , och.Merch_ID
   , och.OLCard_Id
   , och.OLCardDate
   , sod.Product_Id
 FROM tblOutletCardH och
	INNER JOIN tblOutletOrderH ooh ON och.OLCard_id = ooh.OLCard_id
								  AND CONVERT(datetime,CONVERT(VARCHAR(10),och.OLCardDate,104),104) BETWEEN @lowerDate AND @upperDate								  
	INNER JOIN tblOutletOrderD ood ON ood.OrderNo = ooh.OrderNo 
	INNER JOIN tblSalOutH soh ON soh.OrderNo = ooh.OrderNo AND och.ol_id = soh.ol_id
    inner join #merchandisersIDs m on m.id = och.Merch_id
	INNER JOIN tblSalOutD sod ON sod.Invoice_Id = soh.Invoice_Id AND							
                            (select top 1 productcn_id from tblProducts where product_id = sod.Product_Id) =
                            (select top 1 productcn_id from tblProducts where product_id = ood.Product_Id)							                          
							AND ood.Product_qty <> 0 
							AND sod.Product_qty <> 0
							AND soh.Status <> 9
) AS och 
ON od.OlCard_id = och.OLCard_ID  AND (och.Product_Id IS NULL OR  och.Product_Id  = od.Product_Id)
    INNER JOIN #products p ON p.Product_id = CASE 
											    WHEN och.Product_Id IS NULL THEN od.Product_Id 
											    ELSE och.Product_Id 
                                             END
    INNER JOIN #OL_Merch_Chanal olmc ON olmc.OL_ID = CASE 
                                                        WHEN och.OL_ID IS NULL THEN  od.OL_ID 
                                                        ELSE och.OL_ID 
                                                     END 
								AND olmc.Merch_ID = 
(
		CASE 
			WHEN (NOT EXISTS( -- ��������� ��-�� ��, ���������� �� ��
							SELECT 1 
							FROM #OL_Merch_Chanal olmc1 
							WHERE olmc1.OL_id = olmc.OL_id 
							  AND olmc1.RelatedMerch_CNT = 2 -- ���������� ��������� �� ��� ������ ��
							)
				  ) 
			THEN -- �� �� ������� ������ ���� ��.
				 -- �� ����������� ��, ���������� �����, ���������� ����� �� ���� ��, ������� ������� �� �� ����� �������
				olmc.Merch_ID 
			ELSE -- �� �� �������� ��������� ��.
				CASE
					WHEN ( -- ��������� ��, �������� �� �� � ��
						  EXISTS(
								SELECT 1 
								FROM #OL_Merch_Chanal olmc2
								WHERE olmc2.OL_id = olmc.OL_id 
									AND olmc2.Merch_Id = CASE 
									                        WHEN och.Merch_ID IS NULL THEN  od.Merch_ID 
									                        ELSE och.Merch_ID 
									                     END  -- ��, ��������
								)
						 )
					THEN --  
						och.Merch_Id 
					ELSE -- �������� �� �� '�������' � ���� ��� ����� � �������� �� �� '���������� ����� ��������'. 
						 -- �������� ������ ���������� ����� � ��, ������� �������� �����
						(SELECT TOP 1 olmc3.Merch_ID
						FROM #OL_Merch_Chanal olmc3
						    INNER JOIN tblSyncStatusActTypesLinks matl ON olmc3.ActivityType = matl.ActivityType 
								      AND matl.Merch_id = CASE 
								                              WHEN och.Merch_ID IS NULL THEN  od.Merch_ID 
								                              ELSE och.Merch_ID 
								                          END
								      AND olmc3.OL_id = CASE 
								                            WHEN och.OL_ID IS NULL THEN  od.OL_ID 
								                            ELSE och.OL_ID 
								                        END 
						)
				END
		END
)

-- �������� ������, ������� �� ���� ���������, ������ �� ������ � ���������� ������
SELECT * 
INTO #tmpOch1
FROM
(
	SELECT Merch_ID
         , OLCardDate
         , OL_ID
         , OL_IsActive
         , OL_HasRemains
         , Product_Id
         , OLCard_Id
         , ProductName      -- ���
         , ProdTypeName     -- ����
         , ProdGroupName    -- �����
         , Unit_Name        -- ��� ��������
         , Packing_Capacity -- ������� ��������
         , Packing_Type     -- ��� ��������
         , ProductCnName 
	FROM #tmpOch
	UNION
	SELECT un.Merch_ID
         , un.OLCardDate
         , un.OL_id
         , NULL AS OL_IsActive
         , NULL AS OL_HasRemains
         , ps.Product_Id
         , un.OLCard_Id
         , ps.ProductName      -- ���
         , ps.ProdTypeName     -- ����         
         , ps.ProdGroupName    -- �����         
         , ps.Unit_Name        -- ��� ��������
         , ps.Packing_Capacity -- ������� ��������
         , ps.Packing_Type     -- ��� ��������
         , ps.ProductCnName
	FROM
	(
		SELECT OL_Id
			 , Merch_id 
			 , OLCardDate
			 , Product_Id
			 , ProductName
			 , ProdGroupName
			 , ProdTypeName
			 , Unit_Name        -- ��� ��������
			 , Packing_Capacity -- ������� ��������
			 , Packing_Type     -- ��� ��������
			 , ProductCnName
		FROM #Schedule
		CROSS JOIN #products
	) AS ps 
	INNER JOIN
	(
		SELECT Merch_ID
			 , OLCardDate
			 , OL_id
			 , NULL AS OL_IsActive
			 , NULL AS OL_HasRemains
			 , NULL AS Product_Id
			 , OLCard_Id
			 , NULL AS ProductName      -- ���
             , NULL AS ProdTypeName     -- ����			 
             , NULL AS ProdGroupName    -- �����
			 , NULL AS Unit_Name        -- ��� ��������
			 , NULL AS Packing_Capacity -- ������� ��������
			 , NULL AS Packing_Type     -- ��� ��������
			 , NULL AS ProductCnName
		FROM 
		(
			SELECT DISTINCT OLCard_Id, olmc.OL_Id, olmc.Merch_id, OLCardDate 
			FROM tblOutletCardH och
    			INNER JOIN #OL_Merch_Chanal olmc ON olmc.OL_id = och.OL_id 
											AND olmc.Merch_Id = 
			(
			CASE 
				WHEN (NOT EXISTS( -- ��������� ��-�� ��, ���������� �� ��
								SELECT 1 
								FROM #OL_Merch_Chanal olmc1 
								WHERE olmc1.OL_id = olmc.OL_id 
								  AND olmc1.RelatedMerch_CNT = 2 -- ���������� ��������� �� ��� ������ ��
								)
					  ) 
				THEN -- �� �� ������� ������ ���� ��.
					 -- �� ����������� ��, ���������� �����, ���������� ����� �� ���� ��, ������� ������� �� �� ����� �������
					olmc.Merch_ID 
				ELSE -- �� �� �������� ��������� ��.
					CASE
						WHEN ( -- ��������� ��, �������� �� �� � ��
							  EXISTS(
									SELECT 1 
									FROM #OL_Merch_Chanal olmc2
									WHERE olmc2.OL_id = olmc.OL_id 
										AND olmc2.Merch_Id = och.Merch_Id -- ��, ��������
									)
							 )
						THEN --  
							och.Merch_Id 
						ELSE -- �������� �� �� '�������' � ���� ��� ����� � �������� �� �� '���������� ����� ��������'. 
							 -- �������� ������ ���������� ����� � ��, ������� �������� �����
							(SELECT TOP 1 olmc3.Merch_ID
							 FROM #OL_Merch_Chanal olmc3
	    						INNER JOIN tblSyncStatusActTypesLinks matl ON olmc3.ActivityType = matl.ActivityType 
											  AND matl.Merch_id = och.Merch_id
											  AND olmc3.OL_id = och.OL_id
							)
					END
			END
			)
			WHERE CONVERT(datetime,CONVERT(VARCHAR(10),och.OLCardDate,104),104) BETWEEN @lowerDate AND @upperDate 
			EXCEPT
			SELECT DISTINCT OLCard_Id
			      , OL_Id
			      , Merch_id
			      , OLCardDate 
			FROM #tmpOch
		) AS D  	
	) AS un ON un.OL_Id = ps.OL_Id AND un.Merch_id = ps.Merch_id --AND CONVERT(datetime,CONVERT(VARCHAR(10),un.OLCardDate,104),104) = CONVERT(datetime,CONVERT(VARCHAR(10),ps.OLCardDate,104),104)
)AS DD

-- ��������� ��, � ������� �� ���� �������
SELECT tr.*
INTO #tempResult 
FROM
(SELECT DISTINCT s.ol_id
     , s.merch_id
     , s.olcarddate
     , p.Product_Id
FROM #Schedule s
CROSS JOIN vwDW_Products p
EXCEPT
	SELECT DISTINCT OL_Id
				  , Merch_id
				  , OLCardDate
				  , Product_Id	              
	FROM #tmpOch1
) as tr

--SELECT 
--        t2.OL_Id
--      , t2.Merch_id 
--      , t2.OLCardDate
--      , t1.Product_Id
--INTO #tempResult 
--FROM
--(
--    SELECT  
--            Merch_id 
--          , OLCardDate
--          , Product_Id
--    from #tmpOch1
--    GROUP BY Merch_id 
--          , OLCardDate
--          , Product_Id
--) t1
--RIGHT JOIN(
--    SELECT s.OL_Id
--         , s.Merch_id 
--         , s.OLCardDate
--    FROM  #Schedule s
--    LEFT JOIN  #tmpOch1 och
--     ON   och.OL_Id    = s.OL_Id
--      AND och.Merch_id  = s.Merch_id 
--      AND och.OLCardDate = s.OLCardDate
--    where och.OL_Id is null
--    GROUP BY s.OL_Id
--           , s.Merch_id 
--           , s.OLCardDate
--) t2 on     
--       t2.Merch_id   = t1.Merch_id 
--      AND t2.OLCardDate = t1.OLCardDate

/* /* OLD */
SELECT  OL_Id
      , Merch_id 
      , OLCardDate
      , Product_Id
INTO #tempResult 
FROM (
    SELECT OL_Id
         , Merch_id 
         , OLCardDate
         , Product_Id 
    FROM #Schedule
        CROSS JOIN #products
    EXCEPT
    SELECT DISTINCT OL_Id
                  , Merch_id 
                  , OLCardDate
                  , Product_Id 
    FROM #tmpOch1
    )tt
*/
CREATE UNIQUE CLUSTERED INDEX [PK_#tempResult]  ON #tempResult(
        OL_Id
      , Merch_id 
      , Product_Id
      , OLCardDate
      )
CREATE  INDEX [IX_#tempResult]  ON #tempResult(Product_Id)
--CREATE  INDEX [IX_#tempResult_2] ON #tempResult(Merch_ID)

/*------------------------------------------------------------------------------------------------------*/

SELECT * 
INTO #tempResult2
FROM (
    SELECT    Merch_ID
		    , OLCardDate
		    , OL_ID
		    , OL_ID AS OL_Visited
		    , OL_IsActive
		    , OL_HasRemains
		    , Product_Id
		    , OLCard_Id
		    , ProductName -- ���
		    , ProdTypeName -- ����
		    , ProdGroupName -- �����
		    , Unit_Name -- ��� ��������
		    , Packing_Capacity -- ������� ��������
		    , Packing_Type -- ��� ��������
		    , ProductCnName 
	    FROM #tmpOch1
	    UNION
	    SELECT Merch_ID
		     , OLCardDate
		     , OL_id
		     , NULL AS OL_Visited
		     , NULL AS OL_IsActive
		     , NULL AS OL_HasRemains
		     , t.Product_Id
		     , NULL AS OLCard_Id
		     , p.ProductName -- ���
		     , p.ProdTypeName -- ����
             , p.ProdGroupName -- �����		     
		     , p.Unit_Name -- ��� ��������
		     , p.Packing_Capacity -- ������� ��������
		     , p.Packing_Type -- ��� ��������
		     , p.ProductCnName
        FROM #tempResult t
	    LEFT JOIN #products p ON p.Product_Id = t.Product_Id
)tt	    
CREATE UNIQUE CLUSTERED INDEX [PK_#tempResult2] ON #tempResult2 (
        OL_ID
      , Merch_ID
      , OLCard_Id
      , Product_Id
      , OLCardDate
    )
CREATE  INDEX [IX_#tempResult2] ON #tempResult2(Product_Id)
--CREATE  INDEX [IX_#tempResult3] ON #tempResult2(OL_id)
/*------------------------------------------------------------------------------------------------------*/
/*------------------------------------------------------------------------------------------------------*/
/*------------------------------------------------------------------------------------------------------*/
     
SELECT  res.Merch_ID
      , res.OL_HasRemains
      , res.OL_ID
      , res.OL_IsActive
      , res.OL_Visited
      , res.OLCard_Id
      , res.OLCardDate
      , res.Packing_Capacity
      , res.Packing_Type
      , res.ProdGroupName
      , res.ProdTypeName
      , res.ProductCnName
      , res.Product_Id
      , res.Unit_Name
      , d.Region_id -- indicate region to choose TOP SKU
INTO #Result
FROM #tempResult2 res
    INNER JOIN tblOutlets ol ON res.OL_id = ol.OL_id
    INNER JOIN tblAreas a ON ol.Area_id = a.Area_Id
    INNER JOIN tblCities c ON c.City_id = a.City_Id
    INNER JOIN tblDistricts d ON d.District_id = c.District_id


CREATE UNIQUE CLUSTERED INDEX [PK_#Result] ON #Result (
        OL_ID
      , Merch_ID
      , OLCard_Id
      , Product_Id
      , OLCardDate
    )

--CREATE  INDEX [IX_#Result]          ON #Result(Product_Id)
--CREATE  INDEX [IX_#Result_OL_id]    ON #Result(OL_id)

SELECT DISTINCT
                 r.merch_id
               , r.ol_id
               , Schedule.OL_id  AS OLSchedule
               , r.Product_Id 
	           , r.OL_Visited 
	           , r.OL_IsActive 
			   , r.OL_HasRemains 
	           , r.ProdTypeName 
               , r.ProdGroupName 
	           , r.Unit_Name 
	           , r.Packing_Capacity
	           , r.Packing_Type 
	           , r.ProductCnName  
	           , r.OLCardDate
INTO #result_tst --drop table #result_tst
--SELECT *          
FROM #Result r
         LEFT JOIN #Schedule Schedule ON r.OL_Id=Schedule.OL_Id  							     
                                     AND Schedule.OLCardDate between @lowerDate and @upperDate 
							         AND r.Merch_Id=Schedule.Merch_Id        

CREATE INDEX [IX_#result_tst] ON #result_tst (merch_id)

--Update belonging product to topSKU 
UPDATE #products
SET BelongingTOPSKU = 1
FROM #products p 
INNER JOIN 
(
	SELECT DISTINCT ProductCn_ID 
	FROM dbo.DW_ProductCnTop pt
	    INNER JOIN ( SELECT TOP 1 Region_id FROM #Result ) AS regId ON  regId.Region_ID = pt.Region_ID
	WHERE @upperDate BETWEEN StartDate 
	  AND EndDate 
	  OR (StartDate <= @upperDate AND EndDate IS NULL)
) tp ON tp.ProductCn_ID = p.ProductCn_Id

SELECT OL_IsActive AS OL_ID
INTO #OlCompletedPlan
FROM 
(
	SELECT OL_IsActive, ROW_NUMBER() OVER (PARTITION BY OL_IsActive ORDER BY (SELECT 1))  CmbCd 
		FROM #Result r
		    INNER JOIN #products p ON r.Product_id = p.Product_id
		WHERE BelongingTOPSKU = 1
		GROUP BY OL_IsActive, p.ProductCn_Id
) ActiveOutlets
WHERE CmbCd = (SELECT count(DISTINCT ProductCn_Id) FROM #products WHERE BelongingTOPSKU = 1)
 AND OL_IsActive IS NOT NULL

CREATE  INDEX [IX_#Schedule] ON #Schedule(OL_Id,OLCardDate,Merch_Id)
CREATE  INDEX [IX_#OlCompletedPlan] ON #OlCompletedPlan(OL_id)

IF OBJECT_ID('tempdb..#tblOutlets','U') !=0
    DROP TABLE #tblOutlets
SELECT * 
INTO #tblOutlets 
FROM tblOutlets
WHERE status = 2

CREATE UNIQUE CLUSTERED INDEX [UCI_tblOutlets] ON #tblOutlets (OL_id)
CREATE  INDEX [IX_tblOutlets] ON #tblOutlets (Proximity)
UPDATE #tblOutlets
SET Proximity = -3
WHERE Proximity IS NULL

/*-----------------------------------------------------------------------------------------------------------------*/
-- ��������� �������������� �������
/*-----------------------------------------------------------------------------------------------------------------*/


SELECT merch.Supervisor_name -- ����������
     , merch.MerchName -- ��
	 , och.OL_Visited -- Outlet is visited by Merchandiser 
	 , och.OL_IsActive AS OL_IsActive -- Outlet is Active
	 , och.OL_HasRemains AS OL_HasRemains --OL with remains
--     , Schedule.OL_id  AS OLSchedule-- Outlets on schedule
         , och.OLSchedule-- 
	 , ol.OL_id -- ��� ��
 	 , ol.OL_Code -- ������� �����
	 , olt.OLtype_name  --�����
	 , olst.OLSubTypeName  -- ��������
	 , ol.OLName  --���� ���
	 , ol.OLDeliveryAddress  -- ����. �����
	 , convert(datetime,CONVERT(VARCHAR(10),och.OLCardDate,104),104) as OLCardDate  -- �����
	 , och.ProdGroupName -- �����	 
     , och.ProdTypeName -- ����     
     , och.Unit_Name -- ��� ��������
	 , och.Packing_Capacity -- ������� ��������
	 , och.Packing_Type -- ��� ��������
	 , och.ProductCnName -- ������������ ��������������� ���������
	 , pFactor.ProximityName AS ProximityFactor -- ������ ��������
	 , ISNULL(BelongingTOPSKU, 0) AS BelongingTOPSKU
	 , olcp.OL_Id AS OlCompletedPlan
FROM #Result_tst och
        INNER JOIN #merch merch ON merch.Merch_id = och.Merch_id 
--        INNER JOIN tblMerchandisers m ON m.Merch_id = och.Merch_id       
    INNER JOIN #tblOutlets ol ON och.OL_id = ol.OL_id
    INNER JOIN tblOutletSubTypes olst ON ol.OLSubType_id = olst.OLSubType_id 
    INNER JOIN tblOutLetTypes olt ON olst.OLType_ID = olt.OLType_id 
--    INNER JOIN tblSupervisors s ON m.Supervisor_ID = s.Supervisor_id AND s.Status <> 9
--						      AND (@supervisorId is NULL OR @supervisorId = m.Supervisor_ID) 
    LEFT JOIN tblProximityFactors pFactor ON pFactor.Proximity_id = ol.Proximity
--    LEFT JOIN #Schedule Schedule ON Schedule.OL_Id = och.OL_Id 
--							    AND Schedule.OLCardDate between @lowerDate and @upperDate                              
--							    AND Schedule.Merch_Id = och.Merch_Id
    LEFT JOIN #products topSKU ON och.Product_Id = topSKU.Product_Id
    LEFT JOIN #OlCompletedPlan olcp ON och.OL_Id = olcp.OL_Id
--WHERE Schedule.OL_id IS NOT NULL
--AND (och.OL_Visited IS NOT NULL OR och.OL_IsActive IS NOT NULL OR och.OL_HasRemains IS NOT NULL)

DROP TABLE #OL_Merch_Chanal
DROP TABLE #tmpOch
DROP TABLE #tmpOch1
DROP TABLE #merchandisersIDs
DROP TABLE #Result
DROP TABLE #products
DROP TABLE #WeekDays
DROP TABLE #Schedule
DROP TABLE #OlCompletedPlan
DROP TABLE #tempResult
DROP TABLE #tempResult2
DROP TABLE #tblOutlets
DROP TABLE #merch
DROP TABLE #od
DROP TABLE #Result_tst
";
            }
        }

        public static string CoverageAnalysisNew
        {
            get
            {
                return
                    @"
-- =============================================
-- Author:		Developer 2
-- Create date: 18-11-2009
-- Description:	Selects all needed data for 'Failed visits analysis' report.
-- 
-- Changed
-- Author:		Developer 1
-- Create date: 24-11-2009
-- Description:	Selects all data for 'Coverage analysis' report.
--
-- Changed
-- Author:		Developer 6
-- Create date: 19-03-2010
-- Description:	Add some tmp tables and indexes on it to improve performance.
--              The following fiwld were removed from report result set:
--              ��, District_Name, SKU, SKU Code
-- Changed
-- Author:		Developer 6
-- Create date: 09-04-2010
-- Description:	add new selection for not visited OL and add filter 'WHERE OLSchedule IS NOT NULL' to result query
-- =============================================

-- Input parameters
-- ******************* Data range for the report *******************
-- @lowerDate as datetime 
-- @upperDate as datetime
-- *****************************************************************

-- ********************* Supervisor filter ID **********************
-- @supervisorId as INTEGER
-- *****************************************************************

-- ********************* Merchandisers ID list *********************
-- @merchandisersIDs as nvarchar
-- @merchandisersCnt as INTEGER
-- It is a list of IDs. The list is a fixed width (10 characters) 
-- string like '         2,     12345,1234567898'
-- the function dbo.fnDW_Common_SplitText must be used to transform 
-- the string IDs into a table representation
-- @merchandisersCnt - count of mrecandisers should be returned
-- *****************************************************************

/*--------------------------------------------*/
--DECLARE @lowerDate AS DATETIME
--DECLARE @upperDate AS DATETIME
----
--DECLARE @supervisorId AS INTEGER
--DECLARE @merchandisersIDs AS NVARCHAR(4000)
--DECLARE @merchandisersCnt AS INTEGER
----
--SET @merchandisersIDs = '        33,        35,        36,        37,        40,        41,       128,    600007'
--SET @merchandisersCnt = 1
--SET @supervisorId = NULL
--SET @lowerDate = '2009-10-01'
--SET @upperDate = '2009-10-31'
/*--------------------------------------------*/

set datefirst 1    -- �������� ������ ���� � �����
set dateformat ymd -- ������ ���� yyymmdd
SET NOCOUNT ON

DECLARE @currentDate AS DATETIME
----Define planed visits by day
SET @currentDate = @lowerDate
CREATE TABLE #WeekDays (date DateTime NOT NULL, [weekDay] tinyInt )

WHILE @currentDate < @upperDate
BEGIN
INSERT INTO #WeekDays (date, [weekDay])
	VALUES (@currentDate, datePart(dw, @currentDate))
	SET @currentDate = @currentDate + 1
END 

-- Define Merchandisers IDs list
SELECT m.*, s.Supervisor_name
INTO #merch
FROM 
tblMerchandisers m 
INNER JOIN tblSupervisors s ON s.Supervisor_id=m.Supervisor_ID AND s.Status <> 9 AND m.[Status]<>9 AND (@supervisorId is NULL OR @supervisorId = m.Supervisor_ID)

CREATE TABLE #merchandisersIDs (id INT NOT NULL PRIMARY KEY)

If (@merchandisersCnt <> 0 )
	BEGIN
		INSERT INTO #merchandisersIDs
		SELECT ID from dbo.Parse_IntList(@merchandisersIDs)
		
		DELETE #merchandisersIDs WHERE id NOT IN 
		(SELECT m.Merch_id
		FROM dbo.tblMerchandisers  m 
			INNER JOIN tblMerchandiserUserTypes t ON t.merch_id = m.merch_id AND t.status = 2 
			INNER JOIN DW_FSM_UserTypes dfut ON t.UserType_id = dfut.UserType
		WHERE GETDATE() BETWEEN dfut.StartDate AND dfut.EndDate)			
	END
ELSE	
	BEGIN 
		INSERT #merchandisersIDs VALUES (-1) 
	END

-- �������� ��, ������ ����������� �� � ��������� � ������
SELECT ROW_NUMBER() OVER (PARTITION BY OL_id ORDER BY (SELECT 1))  RelatedMerch_CNT -- ��������� �� �-�� ��, ������������� ����� �� 
    , OL_id
    , Merch_Id
    , ActivityType  
INTO #OL_Merch_Chanal
FROM
(
	SELECT olr.OL_id 
		, MAX(r.Merch_Id) Merch_Id -- �������� �� � ������� ID
		, ActivityType
	FROM  dbo.tblRoutes r 
		INNER JOIN dbo.tblOutletRoutes olr ON r.Route_id=olr.Route_id 
											AND olr.Status = 2 AND r.Status = 2
		INNER JOIN 
		( -- ���������  ���� ������� ����� ��
			SELECT  ROW_NUMBER() OVER (PARTITION BY Merch_Id ORDER BY (SELECT 1)) MerchActivity_ID
				 , Merch_ID
				 , ActivityType
			FROM tblSyncStatusActTypesLinks 
			    INNER JOIN #merchandisersIDs AS IDs ON IDs.id = 
						CASE 
						    WHEN @merchandisersCnt = 0 THEN -1 
						    ELSE Merch_ID 
						END

		) matl ON matl.MerchActivity_ID = 1  -- �������� ������ ���������� Activity (�����) � ��
			  AND r.Merch_Id = matl.Merch_Id 
		GROUP BY olr.OL_id, ActivityType
			 
) AS OL_Merch_Chanal 

-- Define schedules for merchandisers
SELECT DISTINCT olr.OL_id
	 , r.Merch_Id 
	 , wd.Date AS OLCardDate
INTO #Schedule--drop table #Schedule
FROM #WeekDays wd
    INNER JOIN dbo.tblRoutes r ON ISNUMERIC(SUBSTRING(r.RouteName, 1, 1)) <> 0 
                 AND (wd.[weekDay] = SUBSTRING(r.RouteName, 1, 1) OR (wd.[weekDay] = CASE 
                                                                                         WHEN SUBSTRING(r.RouteName, 1, 1) IN ('8','9')
                                                                                         THEN 7
                                                                                         ELSE NULL 
                                                                                     END) )                 
                 AND r.Status = 2
    INNER JOIN dbo.tblOutletRoutes olr ON r.Route_id = olr.Route_id 
									    AND olr.Status = 2
    INNER JOIN #OL_Merch_Chanal olmc ON olmc.Merch_Id = r.Merch_Id AND olmc.OL_Id = olr.OL_Id
    INNER JOIN tbloutlets o ON olr.ol_id = o.ol_id AND o.status <> 9
    INNER JOIN #merchandisersIDs AS IDs ON IDs.id = 
			    CASE 
			        WHEN @merchandisersCnt = 0 THEN -1 
			        ELSE r.Merch_id 
			    END
        
CREATE UNIQUE CLUSTERED INDEX [PK_#Schedule] ON #Schedule(OL_id, Merch_Id, OLCardDate)
CREATE INDEX [IX_#Schedule] ON #Schedule (ol_id,merch_id)
-- �������� ��������� � ��� ���������� � ��� ������

SELECT p.Product_Id
	 , p.ProductName      -- ���
	 , pt.ProdTypeName    -- ����
	 , pg.ProdGroupName   -- �����
	 , u.Unit_Name        -- ��� ��������
	 , p.productVolume * 10 AS Packing_Capacity -- ������� ��������
	 , CASE 
	        WHEN ptp.ProductName LIKE '%BNR%'  THEN  CASE 
	                                                    WHEN ptp.ProductName LIKE '% ��%' THEN 'BNR ��' 
			                                            ELSE CASE 
			                                                    WHEN ptp.ProductName LIKE '% InBev%' THEN 'BNR InBev' 
				                                                ELSE 'BNR' 
				                                             END 
			                                         END
		    ELSE 'Other' 
		END  AS Packing_Type -- ��� ��������
	 , pc.ProductCnName      -- ������������ ��������������� ���������
	 , p.ProductCn_Id        -- Combined product Id
	 , 0 AS BelongingTOPSKU 
INTO #products
FROM tblProducts p 
    INNER JOIN tblProductTypes pt ON pt.ProductType_id = p.ProductType_id 
    INNER JOIN tblProductGroups pg ON pt.ProdGroup_ID = pg.ProdGroup_ID 
    INNER JOIN tblUnits u ON p.Unit_ID = u.Unit_ID 
    INNER JOIN tblProductCombine pc ON p.ProductCn_Id = pc.ProductCn_Id 
    LEFT  JOIN tblProducts ptp ON p.Tare_Id = ptp.Product_Id


CREATE UNIQUE CLUSTERED INDEX [PK_#PRODUCT_PRODUCT_ID] ON #products (Product_id)
CREATE INDEX [IX_#PRODUCT_PRODUCTCN_ID] ON #products (ProductCn_ID)

SELECT och.OL_ID
       , och.Merch_ID 
       , och.OLCard_Id
       , och.OLCardDate
       , od.Product_Id
INTO #od
  FROM tblOutletDistribution od
      INNER JOIN tblOutletCardH och ON och.OLCard_id = od.OLCard_id 
                 AND od.IsPresent > 0 
		         AND CONVERT(datetime,CONVERT(VARCHAR(10),och.OLCardDate,104),104) BETWEEN @lowerDate AND @upperDate
 CREATE INDEX [IX_#od] ON #od (OLCard_Id,Product_Id)
 
-- �������� �������� ������ � �������� ��������� � ���� ������ �� ���������
SELECT 	DISTINCT
        olmc.OL_ID
      , olmc.Merch_ID
      , CASE 
            WHEN och.OLCard_Id IS NULL THEN  od.OLCard_Id 
            ELSE och.OLCard_Id 
        END OLCard_Id
      , CASE 
            WHEN och.OLCardDate IS NULL THEN od.OLCardDate 
            ELSE och.OLCardDate 
        END OLCardDate
      , CASE 
            WHEN och.Product_Id IS NULL THEN od.Product_Id 
            ELSE och.Product_Id 
        END Product_Id
      , och.OL_ID AS OL_IsActive
      , od.OL_ID AS OL_HasRemains
      , p.ProductName       -- ���
      , p.ProdTypeName      -- ����
      , p.ProdGroupName     -- �����
      , p.Unit_Name         -- ��� ��������
      , p.Packing_Capacity  -- ������� ��������
      , p.Packing_Type      -- ��� ��������
      , p.ProductCnName     -- ������������ ��������������� ���������
INTO #tmpOch
FROM 
#od AS od
FULL OUTER JOIN 
(SELECT och.OL_ID
   , och.Merch_ID
   , och.OLCard_Id
   , och.OLCardDate
   , sod.Product_Id
 FROM tblOutletCardH och
	INNER JOIN tblOutletOrderH ooh ON och.OLCard_id = ooh.OLCard_id--
								  AND CONVERT(datetime,CONVERT(VARCHAR(10),och.OLCardDate,104),104) BETWEEN @lowerDate AND @upperDate
	INNER JOIN tblOutletOrderD ood ON ood.OrderNo = ooh.OrderNo 
	INNER JOIN tblSalOutH soh ON soh.OrderNo = ooh.OrderNo AND och.ol_id = soh.ol_id
    inner join #merchandisersIDs m on m.id = och.Merch_id
	INNER JOIN tblSalOutD sod ON sod.Invoice_Id = soh.Invoice_Id AND							
                            (select top 1 productcn_id from tblProducts where product_id = sod.Product_Id) =
                            (select top 1 productcn_id from tblProducts where product_id = ood.Product_Id)							
                            AND ood.Product_qty <> 0 
							AND sod.Product_qty <> 0
							AND soh.Status <> 9
) AS och 
ON od.OlCard_id = och.OLCard_ID  AND (och.Product_Id IS NULL OR  och.Product_Id  = od.Product_Id)--och.Product_Id =isnull(och.Product_Id,od.Product_Id)
    INNER JOIN #products p ON p.Product_id = CASE                                                      
											    WHEN och.Product_Id IS NULL THEN od.Product_Id 
											    ELSE och.Product_Id 
                                             END
    INNER JOIN #OL_Merch_Chanal olmc ON olmc.OL_ID = CASE                                              
                                                        WHEN och.OL_ID IS NULL THEN  od.OL_ID 
                                                        ELSE och.OL_ID 
                                                     END 
								AND olmc.Merch_ID = 
(
		CASE 
			WHEN (NOT EXISTS( -- ��������� ��-�� ��, ���������� �� ��
							SELECT 1 
							FROM #OL_Merch_Chanal olmc1 
							WHERE olmc1.OL_id = olmc.OL_id 
							  AND olmc1.RelatedMerch_CNT = 2 -- ���������� ��������� �� ��� ������ ��
							)
				  ) 
			THEN -- �� �� ������� ������ ���� ��.
				 -- �� ����������� ��, ���������� �����, ���������� ����� �� ���� ��, ������� ������� �� �� ����� �������
				olmc.Merch_ID 
			ELSE -- �� �� �������� ��������� ��.
				CASE
					WHEN ( -- ��������� ��, �������� �� �� � ��
						  EXISTS(
								SELECT 1 
								FROM #OL_Merch_Chanal olmc2
								WHERE olmc2.OL_id = olmc.OL_id 
									AND olmc2.Merch_Id = CASE 
									                        WHEN och.Merch_ID IS NULL THEN  od.Merch_ID 
									                        ELSE och.Merch_ID 
									                     END  -- ��, ��������
								)
						 )
					THEN --  
						och.Merch_Id 
					ELSE -- �������� �� �� '�������' � ���� ��� ����� � �������� �� �� '���������� ����� ��������'. 
						 -- �������� ������ ���������� ����� � ��, ������� �������� �����
						(SELECT TOP 1 olmc3.Merch_ID
						FROM #OL_Merch_Chanal olmc3
						    INNER JOIN tblSyncStatusActTypesLinks matl ON olmc3.ActivityType = matl.ActivityType 
								      AND matl.Merch_id = CASE 
								                              WHEN och.Merch_ID IS NULL THEN  od.Merch_ID 
								                              ELSE och.Merch_ID 
								                          END
								      AND olmc3.OL_id = CASE 
								                            WHEN och.OL_ID IS NULL THEN  od.OL_ID 
								                            ELSE och.OL_ID 
								                        END 
						)
				END
		END
)

-- �������� ������, ������� �� ���� ���������, ������ �� ������ � ���������� ������
SELECT * 
INTO #tmpOch1
FROM
(
	SELECT Merch_ID
         , OLCardDate
         , OL_ID
         , OL_IsActive
         , OL_HasRemains
         , Product_Id
         , OLCard_Id
         , ProductName          -- ���
         , ProdTypeName         -- ����
         , ProdGroupName        -- �����
         , Unit_Name            -- ��� ��������
         , Packing_Capacity     -- ������� ��������
         , Packing_Type         -- ��� ��������
         , ProductCnName 
	FROM #tmpOch
	UNION
	SELECT un.Merch_ID
         , un.OLCardDate
         , un.OL_id
         , NULL AS OL_IsActive
         , NULL AS OL_HasRemains
         , ps.Product_Id
         , un.OLCard_Id
         , ps.ProductName        -- ���
         , ps.ProdTypeName       -- ���� 
         , ps.ProdGroupName      -- �����         
         , ps.Unit_Name          -- ��� ��������
         , ps.Packing_Capacity   -- ������� ��������
         , ps.Packing_Type       -- ��� ��������
         , ps.ProductCnName
	FROM
	(
		SELECT OL_Id
			 , Merch_id 
			 , OLCardDate
			 , Product_Id
			 , ProductName
			 , ProdGroupName
			 , ProdTypeName
			 , Unit_Name         -- ��� ��������
			 , Packing_Capacity  -- ������� ��������
			 , Packing_Type      -- ��� ��������
			 , ProductCnName
		FROM #Schedule
		CROSS JOIN #products
	) AS ps 
	INNER JOIN
	(
		SELECT Merch_ID
			 , OLCardDate
			 , OL_id
			 , NULL AS OL_IsActive
			 , NULL AS OL_HasRemains
			 , NULL AS Product_Id
			 , OLCard_Id
			 , NULL AS ProductName      -- ���
			 , NULL AS ProdTypeName     -- ����
             , NULL AS ProdGroupName    -- �����			 
			 , NULL AS Unit_Name        -- ��� ��������
			 , NULL AS Packing_Capacity -- ������� ��������
			 , NULL AS Packing_Type     -- ��� ��������
			 , NULL AS ProductCnName
		FROM 
		(
			SELECT DISTINCT OLCard_Id, olmc.OL_Id, olmc.Merch_id, OLCardDate 
			FROM tblOutletCardH och
    			INNER JOIN #OL_Merch_Chanal olmc ON olmc.OL_id = och.OL_id 
											AND olmc.Merch_Id = 
			(
			CASE 
				WHEN (NOT EXISTS( -- ��������� ��-�� ��, ���������� �� ��
								SELECT 1 
								FROM #OL_Merch_Chanal olmc1 
								WHERE olmc1.OL_id = olmc.OL_id 
								  AND olmc1.RelatedMerch_CNT = 2 -- ���������� ��������� �� ��� ������ ��
								)
					  ) 
				THEN -- �� �� ������� ������ ���� ��.
					 -- �� ����������� ��, ���������� �����, ���������� ����� �� ���� ��, ������� ������� �� �� ����� �������
					olmc.Merch_ID 
				ELSE -- �� �� �������� ��������� ��.
					CASE
						WHEN ( -- ��������� ��, �������� �� �� � ��
							  EXISTS(
									SELECT 1 
									FROM #OL_Merch_Chanal olmc2
									WHERE olmc2.OL_id = olmc.OL_id 
										AND olmc2.Merch_Id = och.Merch_Id -- ��, ��������
									)
							 )
						THEN --  
							och.Merch_Id 
						ELSE -- �������� �� �� '�������' � ���� ��� ����� � �������� �� �� '���������� ����� ��������'. 
							 -- �������� ������ ���������� ����� � ��, ������� �������� �����
							(SELECT TOP 1 olmc3.Merch_ID
							 FROM #OL_Merch_Chanal olmc3
	    						INNER JOIN tblSyncStatusActTypesLinks matl ON olmc3.ActivityType = matl.ActivityType 
											  AND matl.Merch_id = och.Merch_id
											  AND olmc3.OL_id = och.OL_id
							)
					END
			END
			)
			WHERE CONVERT(datetime,CONVERT(VARCHAR(10),och.OLCardDate,104),104) BETWEEN @lowerDate AND @upperDate 
			EXCEPT
			SELECT DISTINCT OLCard_Id
			      , OL_Id
			      , Merch_id
			      , OLCardDate 
			FROM #tmpOch
		) AS D  
	
	) AS un ON un.OL_Id = ps.OL_Id AND un.Merch_id = ps.Merch_id --AND CONVERT(datetime,CONVERT(VARCHAR(10),un.OLCardDate,104),104) = CONVERT(datetime,CONVERT(VARCHAR(10),ps.OLCardDate,104),104)
)AS DD

-- ��������� ��, � ������� �� ���� �������
SELECT tr.*
INTO #tempResult 
FROM
(SELECT DISTINCT s.ol_id
     , s.merch_id
     , s.olcarddate
     , p.Product_Id
FROM #Schedule s
CROSS JOIN vwDW_Products p
EXCEPT
	SELECT DISTINCT OL_Id
				  , Merch_id
				  , OLCardDate
				  , Product_Id	              
	FROM #tmpOch1
) as tr


CREATE UNIQUE CLUSTERED INDEX [PK_#tempResult]  ON #tempResult(
        OL_Id
      , Merch_id 
      , Product_Id
      , OLCardDate
      )
CREATE  INDEX [IX_#tempResult]  ON #tempResult(Product_Id)

--------------------------------------------------------------------------------------------
SELECT * 
INTO #tempResult2
FROM (
    SELECT    Merch_ID
		    , OLCardDate
		    , OL_ID
		    , OL_ID AS OL_Visited
		    , OL_IsActive
		    , OL_HasRemains
		    , Product_Id
		    , OLCard_Id
		    , ProductName -- ���
		    , ProdTypeName -- ����
		    , ProdGroupName -- �����
		    , Unit_Name -- ��� ��������
		    , Packing_Capacity -- ������� ��������
		    , Packing_Type -- ��� ��������
		    , ProductCnName 
	    FROM #tmpOch1
	    UNION
	    SELECT Merch_ID
		     , OLCardDate
		     , OL_id
		     , NULL AS OL_Visited
		     , NULL AS OL_IsActive
		     , NULL AS OL_HasRemains
		     , t.Product_Id
		     , NULL AS OLCard_Id
		     , p.ProductName      -- ���
		     , p.ProdTypeName     -- ����
             , p.ProdGroupName    -- �����		     
		     , p.Unit_Name        -- ��� ��������
		     , p.Packing_Capacity -- ������� ��������
		     , p.Packing_Type     -- ��� ��������
		     , p.ProductCnName
        FROM #tempResult t
	    LEFT JOIN #products p ON p.Product_Id = t.Product_Id   
)tt	 

SELECT  res.Merch_ID
      , res.OL_HasRemains
      , res.OL_ID
      , res.OL_IsActive
      , res.OL_Visited
      , res.OLCard_Id
      , res.OLCardDate
      , res.Packing_Capacity
      , res.Packing_Type
      , res.ProdGroupName
      , res.ProdTypeName
      , res.ProductCnName
      , res.Product_Id   
      , res.Unit_Name
      , d.Region_id -- indicate region to choose TOP SKU
INTO #Result
FROM #tempResult2 res
    INNER JOIN tblOutlets ol  ON res.OL_id = ol.OL_id
    INNER JOIN tblAreas a     ON ol.Area_id = a.Area_Id
    INNER JOIN tblCities c    ON c.City_id = a.City_Id
    INNER JOIN tblDistricts d ON d.District_id = c.District_id

SELECT DISTINCT
                 r.merch_id
               , r.ol_id
               , Schedule.OL_id  AS OLSchedule
               , r.Product_Id 
	           , r.OL_Visited 
	           , r.OL_IsActive 
			   , r.OL_HasRemains 
	           , r.ProdTypeName 
               , r.ProdGroupName 
	           , r.Unit_Name 
	           , r.Packing_Capacity
	           , r.Packing_Type 
	           , r.ProductCnName  
INTO #result_tst --drop table #result_tst
--SELECT *          
FROM #Result r
         LEFT JOIN #Schedule Schedule ON r.OL_Id=Schedule.OL_Id  							     
                                     AND Schedule.OLCardDate between @lowerDate and @upperDate 
							         AND r.Merch_Id=Schedule.Merch_Id        

CREATE INDEX [IX_#result_tst] ON #result_tst (merch_id)
--CREATE INDEX [IX_#result_tst1] ON #result_tst (ol_id,merch_id)

--Update belonging product to topSKU 
UPDATE #products
SET BelongingTOPSKU = 1
FROM #products p 
INNER JOIN 
(
	SELECT DISTINCT ProductCn_ID 
	FROM dbo.DW_ProductCnTop pt
	    INNER JOIN ( SELECT TOP 1 Region_id FROM #Result ) AS regId ON  regId.Region_ID = pt.Region_ID
	WHERE @upperDate BETWEEN StartDate 
	  AND EndDate 
	  OR (StartDate <= @upperDate AND EndDate IS NULL)
) tp ON tp.ProductCn_ID = p.ProductCn_Id

SELECT OL_IsActive AS OL_ID
INTO #OlCompletedPlan
FROM 
(
	SELECT OL_IsActive, ROW_NUMBER() OVER (PARTITION BY OL_IsActive ORDER BY (SELECT 1))  CmbCd 
		FROM #Result r
		    INNER JOIN #products p ON r.Product_id = p.Product_id
		WHERE BelongingTOPSKU = 1
		GROUP BY OL_IsActive, p.ProductCn_Id
) ActiveOutlets
WHERE CmbCd = (SELECT count(DISTINCT ProductCn_Id) FROM #products WHERE BelongingTOPSKU = 1)
 AND OL_IsActive IS NOT NULL

--CREATE  INDEX [IX_#Schedule]        ON #Schedule(OL_Id,OLCardDate,Merch_Id)
CREATE  INDEX [IX_#OlCompletedPlan] ON #OlCompletedPlan(OL_id)

IF OBJECT_ID('tempdb..#tblOutlets','U') !=0
    DROP TABLE #tblOutlets

SELECT * 
INTO #tblOutlets 
FROM tblOutlets
WHERE [Status] = 2 --<>9--igetman

CREATE UNIQUE CLUSTERED INDEX [UCI_tblOutlets] ON #tblOutlets (OL_id)
CREATE  INDEX [IX_tblOutlets] ON #tblOutlets (Proximity)
UPDATE #tblOutlets
SET Proximity = -3
WHERE Proximity IS NULL

/*-----------------------------------------------------------------------------------------------------------------*/
-- ������� ����������� �������
/*-----------------------------------------------------------------------------------------------------------------*/
SELECT 
       Supervisor_name   -- ���������
     , MerchName         -- �� 
     , OL_Visited        -- ������ ���������� �� �������� ������������� (���� �� ���� ������ �������� OL_ID ������� ����� �����������)
     , OL_IsActive       -- ������ ��������� ����� (���� �� ���� ������ �������� OL_ID ������� ����� �������)
     , OL_HasRemains     -- ������ ����� � ��������� (���� �� ���� ������ �������� OL_ID ������� ����� �� �������)
     , OLSchedule        -- ����� �� �������
     , OL_id             -- ��� ��  
     , OL_Code           -- ��������� ��� ��  
     , OLtype_name       -- ��� �� 
     , OLSubTypeName     -- ϳ���� ��
     , OLName            -- �������� ��� ��
     , OLDeliveryAddress -- ��������� ����� �� 
     , ProdGroupName     -- �����
     , ProdTypeName      -- ����
     , Unit_Name         -- ��� ��������
     , Packing_Capacity  -- ������� ��������
     , Packing_Type      -- ��� �������� 
     , ProductCnName     -- ������������ ���������� ���������
     , ProximityFactor   -- ������ ���������
     , BelongingTOPSKU   -- ������ �� �������� ������� �� TOP SKU (���� ������ �������� 1 ��� 0)
     , OlCompletedPlan   
FROM (
    SELECT --s.Supervisor_name 
         --, m.MerchName 
           merch.Supervisor_name 
         , merch.MerchName          
	     , och.OL_Visited 
	     , och.OL_IsActive AS OL_IsActive 
	     , och.OL_HasRemains AS OL_HasRemains 
    --     , Schedule.OL_id  AS OLSchedule
         , och.OLSchedule
	     , ol.OL_id 
 	     , ol.OL_Code 
	     , olt.OLtype_name  
	     , olst.OLSubTypeName 
	     , ol.OLName  
	     , ol.OLDeliveryAddress  
	     , och.ProdTypeName 
         , och.ProdGroupName 
	     , och.Unit_Name 
	     , och.Packing_Capacity
	     , och.Packing_Type 
	     , och.ProductCnName
	     , pf.ProximityName AS ProximityFactor 
	     , ISNULL(BelongingTOPSKU, 0) AS BelongingTOPSKU
	     , olcp.OL_Id AS OlCompletedPlan
    FROM #result_tst och 
        INNER JOIN #merch merch ON merch.Merch_id = och.Merch_id --AND (@supervisorId is NULL OR @supervisorId = m.Supervisor_ID)               
--        INNER JOIN tblMerchandisers m ON m.Merch_id = och.Merch_id       
        INNER JOIN #tblOutlets ol ON och.OL_id = ol.OL_id       
        INNER JOIN tblOutletSubTypes olst ON ol.OLSubType_id = olst.OLSubType_id 
        INNER JOIN tblOutLetTypes olt ON olst.OLType_ID = olt.OLType_id        
--        INNER JOIN tblSupervisors s ON m.Supervisor_ID = s.Supervisor_id AND s.Status <> 9
--						          AND (@supervisorId is NULL OR @supervisorId = m.Supervisor_ID)        
        LEFT JOIN tblProximityFactors pf ON pf.Proximity_id = ol.Proximity        
--        LEFT JOIN #Schedule Schedule /*WITH (INDEX (IX_#Schedule))   */ON och.OL_Id=Schedule.OL_Id  							     
                                    --AND Schedule.OLCardDate between @lowerDate and @upperDate 
							        --AND och.Merch_Id=Schedule.Merch_Id        
        LEFT JOIN #products topSKU ON och.Product_Id = topSKU.Product_Id
        LEFT JOIN #OlCompletedPlan olcp ON och.OL_Id = olcp.OL_Id
)T
GROUP BY 
       Supervisor_name
     , MerchName
	 , OL_Visited
	 , OL_IsActive 
	 , OL_HasRemains
     , OLSchedule
	 , OL_id
 	 , OL_Code
	 , OLtype_name
	 , OLSubTypeName
	 , OLName
	 , OLDeliveryAddress
	 , ProdTypeName 
     , ProdGroupName	 
	 , Unit_Name
	 , Packing_Capacity
	 , Packing_Type
	 , ProductCnName
	 , ProximityFactor
	 , BelongingTOPSKU
	 , OlCompletedPlan

DROP TABLE #OL_Merch_Chanal
DROP TABLE #tmpOch
DROP TABLE #tmpOch1
DROP TABLE #merchandisersIDs
DROP TABLE #Result
DROP TABLE #Result_tst
DROP TABLE #products
DROP TABLE #WeekDays
DROP TABLE #Schedule
DROP TABLE #OlCompletedPlan
DROP TABLE #tempResult
DROP TABLE #tempResult2
DROP TABLE #tblOutlets
DROP TABLE #merch
DROP TABLE #od
";
            }
        }

        public static string FailedVisits
        {
            get
            {
                return
                    @"
-- =============================================
-- Author:		Semyon Krotkih
-- Create date: 18-11-2009
-- Description:	Selects all needed data for 'Failed visits analysis' report.
-- Input parameters:
--		Data range for the report:
--			@lowerDate as datetime - 
--			@upperDate as datetime

--		Supervisor filter ID:
--			@supervisorId as INTEGER
--		Merchandisers ID list:
--			@merchandisersIDs as nvarchar
--		Count of mrecandisers should be returned
--			@merchandisersCnt as INTEGER

--		It is a list of IDs. The list is a fixed width (10 characters) 
--		string like '         2,     12345,1234567898'
--		the function dbo.fnDW_Common_SplitText must be used to transform 
--		the string IDs into a table representation
-- 
-- =============================================

/**/
--DECLARE @lowerDate as datetime
--DECLARE @upperDate as datetime
--
--set @lowerDate = '2009-10-01'
--set @upperDate = '2009-10-31'
--
--
--DECLARE @supervisorId AS INTEGER
--DECLARE @merchandisersIDs AS NVARCHAR(4000)
--DECLARE @merchandisersCnt AS INTEGER
--
--set @merchandisersIDs = '  14500001,  14500002,  14500003,  14500004'
--set @merchandisersCnt = 0
--set @supervisorId = NULL


-- Define Merchandisers IDs list
CREATE TABLE #merchandisersIDs (id INT NOT NULL)
BEGIN
	DECLARE @s BIGINT, @e INT, @id INT
	DECLARE @nv NVARCHAR(10), @cnt INT
	DECLARE @string NVARCHAR(4000)
    DECLARE @merch_type INT
	
	If (@merchandisersCnt <> 0 )
		BEGIN
			SET @string = @merchandisersIDs
			SET @s = 1
			SET @cnt = @merchandisersCnt
			WHILE @cnt > 0
			BEGIN
				SET @nv = SUBSTRING(@string, @s, 10)
				SET @s = @s + 11
				SET @id  = cast(@nv as int )
				SELECT @merch_type = t.UserType_id 
                FROM dbo.tblMerchandisers  m 
                     INNER JOIN tblMerchandiserUserTypes t ON t.merch_id = m.merch_id AND m.Merch_id = @id and t.status = 2
				IF @merch_type IN (SELECT UserType FROM DW_FSM_UserTypes dfut WHERE GETDATE() BETWEEN dfut.StartDate AND dfut.EndDate)  -- Added by Huch R 
                BEGIN                
                    INSERT #merchandisersIDs VALUES (@id) 
                END
				SET @cnt = @cnt -1
			END
		END
	ELSE	
		BEGIN 
			INSERT #merchandisersIDs VALUES (-1) 
		END

END

-- �������� ��, ������ ����������� �� � ��������� � ������
SELECT ROW_NUMBER() OVER (PARTITION BY OL_id ORDER BY (SELECT 1))  RelatedMerch_CNT -- ��������� �� �-�� ��, ������������� ����� �� 
    , OL_id
    , Merch_Id
    , ActivityType  
INTO #OL_Merch_Chanal
FROM
(
	SELECT ol.OL_id 
		, MAX(r.Merch_Id) Merch_Id -- �������� �� � ������� ID
		, ActivityType
	FROM  dbo.tblRoutes r 
		INNER JOIN dbo.tblOutletRoutes olr ON r.Route_id=olr.Route_id 
		INNER JOIN dbo.tblOutlets ol ON olr.OL_id=ol.OL_id
		INNER JOIN 
		( -- ���������  ���� ������� ����� ��
			SELECT  ROW_NUMBER() OVER (PARTITION BY Merch_Id ORDER BY (SELECT 1)) MerchActivity_ID
				 ,  Merch_ID
				 , ActivityType
			FROM tblSyncStatusActTypesLinks
			INNER JOIN #merchandisersIDs AS IDs ON IDs.id = 
						CASE WHEN @merchandisersCnt = 0 THEN -1 ELSE Merch_ID END
		) matl ON matl.MerchActivity_ID = 1 -- �������� ������ ���������� Activity (�����) � ��
			  AND r.Merch_Id = matl.Merch_Id  
	WHERE olr.Status = 2		
	GROUP BY ol.OL_id
		, ActivityType			 
) AS OL_Merch_Chanal 

-- �������� �������������� ������ �� ����
SELECT 
	  tblSupervisors.Supervisor_name
	, tblSyncStatus.MerchName
	, COUNT(*) AS FailedVisitsCount
	, tblOutlets.OL_id
	, tblOutlets.OL_Code
	, tblInaccessibilityReasons.Reason
	, tblOutlets.OLDeliveryAddress + ', ' + tblOutlets.OLName AS FactTT
	, tblOutlets.OLAddress + ', ' + tblOutlets.OLTradingName AS LawTT
	, tblOutletSubTypes.OLSubTypeName
	, tblOutletTypes.OLtype_name
	, tblOutlets.OLName
	, tblOutlets.OLDeliveryAddress
	, tblOutlets.OLTradingName
	, tblOutlets.OLAddress
	, ols.LValue AS OutletsSatus
	, mers.LValue AS MerchStatus
	, tblCustomers.Cust_Name
	, tblCities.City_Name
	, och.Comments
	, och.OLCardDate
    , a.Area_Name -- ����� � ������ (��) 
	
FROM tblOutletCardH och
INNER JOIN #OL_Merch_Chanal olmc ON olmc.OL_id = och.OL_id AND olmc.Merch_ID = 
		(
		CASE 
			WHEN (NOT EXISTS( -- ��������� ��-�� ��, ���������� �� ��
							SELECT 1 
							FROM #OL_Merch_Chanal olmc1 
							WHERE olmc1.OL_id = olmc.OL_id 
							  AND olmc1.RelatedMerch_CNT = 2 -- ���������� ��������� �� ��� ������ ��
							)
				  ) 
			THEN -- �� �� ������� ������ ���� ��.
				 -- �� ����������� ��, ���������� �����, ���������� ����� �� ���� ��, ������� ������� �� �� ����� �������
				olmc.Merch_ID 
			ELSE -- �� �� �������� ��������� ��.
				CASE
					WHEN ( -- ��������� ��, �������� �� �� � ��
						  EXISTS(
								SELECT 1 
								FROM #OL_Merch_Chanal olmc2
								WHERE olmc2.OL_id = olmc.OL_id 
									AND olmc2.Merch_Id = och.Merch_Id -- ��, ��������
								)
						 )
					THEN --  
						och.Merch_Id 
					ELSE -- �������� �� �� '�������' � ���� ��� ����� � �������� �� �� '���������� ����� ��������'. 
						 -- �������� ������ ���������� ����� � ��, ������� �������� �����
						(SELECT TOP 1 olmc3.Merch_ID
						FROM #OL_Merch_Chanal olmc3
						INNER JOIN tblSyncStatusActTypesLinks matl ON olmc3.ActivityType = matl.ActivityType 
																  AND matl.Merch_id = och.Merch_id
																  AND olmc3.OL_id = och.OL_id
						)
				END
		END
		)
INNER JOIN tblSyncStatus ON tblSyncStatus.Merch_id = olmc.Merch_ID
LEFT JOIN tblInaccessibilityReasons ON tblInaccessibilityReasons.Reason_id = och.Reason_id
INNER JOIN tblSupervisors ON tblSyncStatus.Supervisor_ID = tblSupervisors.Supervisor_id AND tblSupervisors.Status <> 9
INNER JOIN 
	(
		SELECT  ROW_NUMBER() OVER (PARTITION BY Supervisor_id ORDER BY (SELECT 1)) CustomerOrder
			 ,  Supervisor_id
			 ,  Cust_Id
		FROM tblCustomerSupervisors tcs
		WHERE (@supervisorId is NULL OR @supervisorId = tcs.Supervisor_ID)  
	) AS CustomerSupervisors ON CustomerSupervisors.Supervisor_id = tblSupervisors.Supervisor_id	
							AND CustomerSupervisors.CustomerOrder = 1	-- �.�. ��� ����������� ����� ������� ��, 
																		-- �������� ������ ���������� �� ������������� ��
INNER JOIN tblCustomers ON tblCustomers.Cust_Id = CustomerSupervisors.Cust_id
INNER JOIN tblOutlets ON tblOutlets.OL_id = olmc.OL_id
INNER JOIN tblOutletSubTypes ON tblOutletSubTypes.OLSubType_id = tblOutlets.OLSubType_id
INNER JOIN tblOutletTypes ON tblOutletTypes.OLType_ID = tblOutletSubTypes.OLType_ID
INNER JOIN tblAreas a ON tblOutlets.Area_id = a.Area_Id
INNER JOIN tblCities ON tblCities.City_id = a.City_Id
INNER JOIN tblGloballookup ols ON ols.LKey = tblOutlets.Status 
				AND ols.TableName = 'tblOutlets'
				AND ols.FieldName = 'Status'
INNER JOIN tblGloballookup mers ON mers.LKey = tblSyncStatus.Status 
				AND mers.TableName = 'tblSyncStatus'
				AND mers.FieldName = 'Status'
	
GROUP BY 
	  tblSupervisors.Supervisor_name
	, tblSyncStatus.MerchName
	, tblOutlets.OL_id
	, tblOutlets.OL_Code
	, tblInaccessibilityReasons.Reason
	, tblOutlets.OLDeliveryAddress
	, tblOutlets.OLAddress
	, tblOutletSubTypes.OLSubTypeName
	, tblOutletTypes.OLtype_name
	, tblOutlets.OLName
	, tblOutlets.OLDeliveryAddress
	, tblOutlets.OLTradingName
	, tblOutlets.OLAddress
	, tblSyncStatus.MerchStatus
	, tblOutlets.Status
	, tblCustomers.Cust_name
	, tblCities.City_name
	, och.Comments
	, och.OLCardDate
	, ols.LValue
	, mers.LValue
	, och.Inaccessible
	, a.Area_Name	
HAVING och.OLCardDate BETWEEN @lowerDate AND @upperDate	AND och.Inaccessible = 1

DROP TABLE #OL_Merch_Chanal
DROP TABLE #merchandisersIDs


";
            }
        }

        public static string OutletBase
        {
            get
            {
                return
                    @"
-- =============================================
-- Author:		Semyon Krotkih
-- Create date: 18-11-2009
-- Description:	Selects all needed data for 'Outlet base' report.
-- Input parameters:
--		Data range for the report:
--		Supervisor filter ID:
--			@supervisorId as INTEGER
--		Merchandisers ID list:
--			@merchandisersIDs as nvarchar
--		Count of mrecandisers should be returned
--			@merchandisersCnt as INTEGER

--		It is a list of IDs. The list is a fixed width (10 characters) 
--		string like '         2,     12345,1234567898'
--		the string IDs into a table representation
-- 
-- =============================================

SET DATEFIRST 1

--DECLARE @supervisorId AS INTEGER
--DECLARE @merchandisersIDs AS NVARCHAR(4000)
--DECLARE @merchandisersCnt AS INTEGER
DECLARE @currentDate as datetime
DECLARE @currentMonthStart as datetime
DECLARE @previousMonthStart as datetime
DECLARE @previousMonthEnd as datetime

SET @currentDate = CAST(ROUND(CAST(getdate() AS float), 0, 1) AS datetime)
SET @previousMonthEnd = DATEADD(dd, -DATEPART(dd, @currentDate), @currentDate)
SET @currentMonthStart = DATEADD(dd, 1, @previousMonthEnd)
SET @previousMonthStart = DATEADD(mm, -1, @currentMonthStart)
--set @merchandisersIDs = '        28,        29,        30,        31'
--set @merchandisersCnt = 0
--set @supervisorId = NULL

-- Define Merchandisers IDs list
CREATE TABLE #merchandisersIDs (id INT NOT NULL)
BEGIN
	DECLARE @s BIGINT, @e INT, @id INT
	DECLARE @nv NVARCHAR(10), @cnt INT
	DECLARE @string NVARCHAR(4000)
    DECLARE @merch_type INT
	
	If (@merchandisersCnt <> 0 )
		BEGIN
			SET @string = @merchandisersIDs
			SET @s = 1
			SET @cnt = @merchandisersCnt
			WHILE @cnt > 0
			BEGIN
				SET @nv = SUBSTRING(@string, @s, 10)
				SET @s = @s + 11
				SET @id  = cast(@nv as int )
				SELECT @merch_type = t.UserType_id 
                FROM dbo.tblMerchandisers  m 
                     INNER JOIN tblMerchandiserUserTypes t ON t.merch_id = m.merch_id AND m.Merch_id = @id and t.status = 2
				IF @merch_type IN (SELECT UserType FROM DW_FSM_UserTypes dfut WHERE GETDATE() BETWEEN dfut.StartDate AND dfut.EndDate)  -- Added by Huch R
                BEGIN                
                    INSERT #merchandisersIDs VALUES (@id) 
                END
				SET @cnt = @cnt -1
			END
		END
	ELSE	
		BEGIN 
			INSERT #merchandisersIDs VALUES (-1) 
		END
END


-- �������� ��, ������ ����������� �� � ��������� � ������
SELECT ROW_NUMBER() OVER (PARTITION BY OL_id ORDER BY (SELECT 1))  RelatedMerch_CNT -- ��������� �� �-�� ��, ������������� ����� �� 
    , OL_id
    , Merch_Id
    , ActivityType  
INTO #OL_Merch_Chanal
FROM
(
	SELECT ol.OL_id 
		, MAX(r.Merch_Id) Merch_Id -- �������� �� � ������� ID
		, ActivityType
	FROM  tblRoutes r 
		INNER JOIN dbo.tblOutletRoutes olr ON r.Route_id=olr.Route_id 
		INNER JOIN dbo.tblOutlets ol ON olr.OL_id=ol.OL_id
		INNER JOIN 
		( -- ���������  ���� ������� ����� ��
			SELECT  ROW_NUMBER() OVER (PARTITION BY Merch_Id ORDER BY (SELECT 1)) MerchActivity_ID
				 ,  Merch_ID
				 ,  ActivityType
			FROM tblSyncStatusActTypesLinks 
			INNER JOIN #merchandisersIDs AS IDs ON IDs.id = 
						CASE WHEN @merchandisersCnt = 0 THEN -1 ELSE Merch_ID END
		) matl ON matl.MerchActivity_ID = 1  -- �������� ������ ���������� Activity (�����) � ��
			  AND r.Merch_Id = matl.Merch_Id 
	--WHERE olr.Status = 2
	GROUP BY ol.OL_id
		, ActivityType
) AS OL_Merch_Chanal 

-- �������� �������������� ������ �� ����
SELECT 
	  tblOutlets.OL_id
	, tblOutlets.OL_Code
	, tblOutletSubTypes.OLSubTypeName
	, tblOutletTypes.OLtype_name
	, tblOutlets.OLName
	, tblOutlets.OLDeliveryAddress
	, tblOutlets.OLTradingName
	, tblOutlets.OLAddress
	, ols.LValue as OutletsSatus
	, tblDistricts.District_Name
	, tblCities.City_Name
	, tblOutlets.OLDirector
	, ISNULL(Sales.PrevBottledSales, 0) AS PreviousBottledSales
	, ISNULL(Sales.PrevKegSales, 0) AS PreviousKegSales
	, ISNULL(Sales.CurrBottledSales, 0) AS CurrentBottledSales

	, ISNULL(Sales.CurrKegSales, 0) AS CurrentKegSales
	, ISNULL(Fridges.Quantity, 0) AS FridgeNumber
	, ISNULL(Coolers.Quantity, 0) AS CoolerNumber
	, ISNULL(PlanedVisits.VisitsCount, 0) AS PlanedVisitsCount
	, ISNULL(FactVisits.VisitsCount, 0) AS TotalVisitsCount
	, pFactor.ProximityName as ProximityFactor
	, tblOutlets.OLTelephone
	, tblOutlets.OLFax
	, tblOutlets.OLEmail
	, tblOutlets.OLPurchManager
	, tblOutlets.OLMarkManager
	, tblOutlets.OLMarkManagerPhone
	, tblOutlets.OLAccountant
	, tblOutlets.OLAccountantPhone
	, tblOutlets.ContractNumber
	, tblOutlets.ContractDate
	, tblOutlets.RR
	, tblOutlets.BankCode
	, tblOutlets.BankName
	, tblOutlets.BankAddress
	, tblOutlets.ZKPO
	, tblOutlets.VATN
	, tblOutlets.IPN
	, tblOutlets.OLSize
	, tblOutlets.OLWHSize
	, tblAreas.Area_Name
	, tblSupervisors.Supervisor_name
	, tblSyncStatus.MerchName
--TODO : �-�� ����������� ���, Urban/Rural, ���-�� ���������, ����� ��.

--FROM 
--	(--������� ���������� � ���������, ��� ��������� ������������
	 
	 
--	 SELECT DISTINCT o.OL_id, 
--					 o.Owner_id AS merch_id
FROM
(SELECT DISTINCT o.ol_id, m.id AS merch_id
 FROM tblOutlets o inner join #merchandisersIDs m ON m.id =o.Owner_id ) AS vwMerch_OL
--(--SELECT DISTINCT ol_id,owner_id FROM tblOutlets
 --UNION
 --SELECT DISTINCT tor.ol_id, tot.Owner_id FROM tblOutletRoutes tor INNER JOIN tblOutlets tot ON tor.OL_id = tot.OL_id) o
 --LEFT JOIN tblOutletRoutes olr ON o.OL_id = olr.OL_id
 --LEFT JOIN #merchandisersIDs AS IDs ON o.Owner_id = IDs.id --OR @merchandisersCnt = 0     
 --   ) AS vwMerch_OL
LEFT JOIN tblOutlets ON tblOutlets.OL_id = vwMerch_OL.OL_id
LEFT JOIN tblSyncStatus ON tblSyncStatus.Merch_id = vwMerch_OL.Merch_ID
LEFT JOIN tblSupervisors ON tblSyncStatus.Supervisor_ID = tblSupervisors.Supervisor_id 
		AND tblSupervisors.Status <> 9
LEFT JOIN tblAreas ON tblOutlets.Area_id = tblAreas.Area_Id
LEFT JOIN tblCities ON tblCities.City_id = tblAreas.City_Id
LEFT JOIN tblOutletSubTypes ON tblOutletSubTypes.OLSubType_id = tblOutlets.OLSubType_id --!
LEFT JOIN tblOutletTypes ON tblOutletTypes.OLType_ID = tblOutletSubTypes.OLType_ID --!
LEFT JOIN tblDistricts ON tblDistricts.District_id = tblCities.District_id
LEFT JOIN tblGloballookup ols ON ols.LKey = tblOutlets.Status  --!
							  AND ols.TableName = 'tblOutlets'
							  AND ols.FieldName = 'Status' 
LEFT JOIN -- ������������
	(
		SELECT OL_ID
			 , SUM(Quantity) AS Quantity 
		FROM DW_IPTR_EquipmentOutlet
		WHERE EquipmentSize_ID > 0
		  AND EquipmentClass_ID = 1
		  AND yMonth = DATEPART(yyyy, @currentDate) * 100 + DATEPART(mm, @currentDate)
		GROUP BY OL_ID
	) Fridges ON Fridges.OL_id = vwMerch_OL.OL_id 
LEFT JOIN -- ����������
	(
		SELECT OL_ID
			 , SUM(Quantity) AS Quantity 
		FROM DW_IPTR_EquipmentOutlet
		WHERE EquipmentSize_ID > 0
		  AND EquipmentClass_ID = 2
		  AND yMonth = DATEPART(yyyy, @currentDate) * 100 + DATEPART(mm, @currentDate)
		GROUP BY OL_ID
	) Coolers ON Coolers.OL_id = vwMerch_OL.OL_id 
LEFT JOIN -- �������.
   (
	   SELECT tblSalOutH.OL_ID
			, olmc.Merch_ID
			, SUM(CASE 
				  WHEN tblSalOutH.[Date] BETWEEN @previousMonthStart AND @previousMonthEnd 
					   AND tblProducts.Unit_ID <> 3
				  THEN tblSalOutD.Product_Qty * tblProducts.ProductVolume
				  ELSE 0 END) AS PrevBottledSales
			, SUM(CASE 
				  WHEN tblSalOutH.[Date] BETWEEN @previousMonthStart AND @previousMonthEnd 
					   AND tblProducts.Unit_ID = 3
				  THEN tblSalOutD.Product_Qty * tblProducts.ProductVolume
				  ELSE 0 END) AS PrevKegSales
			, SUM(CASE 
				  WHEN tblSalOutH.[Date] BETWEEN @currentMonthStart AND @currentDate
					   AND tblProducts.Unit_ID <> 3
				  THEN tblSalOutD.Product_Qty * tblProducts.ProductVolume
				  ELSE 0 END) AS CurrBottledSales
			, SUM(CASE 
				  WHEN tblSalOutH.[Date] BETWEEN @currentMonthStart AND @currentDate 
					   AND tblProducts.Unit_ID = 3
				  THEN tblSalOutD.Product_Qty * tblProducts.ProductVolume
				  ELSE 0 END) AS CurrKegSales
		FROM tblSalOutH 
		INNER JOIN tblSalOutD ON tblSalOutH.Invoice_id = tblSalOutD.Invoice_id 
		INNER JOIN tblProducts ON tblSalOutD.Product_id = tblProducts.Product_id
		INNER JOIN tblProductTypes ON tblProducts.ProductType_id = tblProductTypes.ProductType_id 
		INNER JOIN tblProductGroups ON tblProductTypes.ProdGroup_ID = tblProductGroups.ProdGroup_ID 
			AND tblProductGroups.ProdGroup_ID NOT IN (6, 31, 32, 33, 35, 36, 37) 
		INNER JOIN #OL_Merch_Chanal olmc ON olmc.OL_id = tblSalOutH.OL_id AND olmc.Merch_ID = 
		(
		CASE 
			WHEN (NOT EXISTS( -- ��������� ��-�� ��, ���������� �� ��
							SELECT 1 
							FROM #OL_Merch_Chanal olmc1 
							WHERE olmc1.OL_id = olmc.OL_id 
							  AND olmc1.RelatedMerch_CNT = 2 -- ���������� ��������� �� ��� ������ ��
							)
				  ) 
			THEN -- �� �� ������� ������ ���� ��.
				 -- �� ����������� ��, ���������� �����, ���������� ����� �� ���� ��, ������� ������� �� �� ����� �������
				olmc.Merch_ID 
			ELSE -- �� �� �������� ��������� ��.
				CASE
					WHEN ( -- ��������� ��, �������� �� �� � ��
						  EXISTS(
								SELECT 1 
								FROM #OL_Merch_Chanal olmc2
								WHERE olmc2.OL_id = olmc.OL_id 
									AND olmc2.Merch_Id = tblSalOutH.Merch_Id -- ��, ��������
								)
						 )
					THEN --  
						tblSalOutH.Merch_Id 
					ELSE -- �������� �� �� ������� � ���� ��� ����� � �������� �� �� ���������� ����� ��������. 
						 -- �������� ������ ���������� ����� � ��, ������� �������� �����
						(SELECT TOP 1 olmc3.Merch_ID
						FROM #OL_Merch_Chanal olmc3
						INNER JOIN tblSyncStatusActTypesLinks matl ON olmc3.ActivityType = matl.ActivityType 
																  AND matl.Merch_id = tblSalOutH.Merch_id
																  AND olmc3.OL_id = tblSalOutH.OL_id
						)
				END
		END
		)
		WHERE tblSalOutH.[Date] BETWEEN @previousMonthStart AND @currentDate
			  AND tblSalOutH.Status <> 9
	    GROUP BY tblSalOutH.OL_ID
			  ,  olmc.Merch_ID
   ) Sales ON Sales.OL_ID = vwMerch_OL.OL_id 
			  AND Sales.Merch_id = vwMerch_OL.Merch_id
LEFT JOIN -- �������� ���������� �������.
	(
		SELECT OL_id
			 , Merch_id
			 , SUM(VisitsCount) AS VisitsCount
		FROM 
		(
			SELECT 
				  ol.OL_id
				, r.Merch_id
				, DATEDIFF(WEEK, @currentMonthStart, @currentDate)
				  + (CASE 
					WHEN (SUBSTRING(r.RouteName, 1, 1) % 8 >= DATEPART(dw, @currentMonthStart)
						  AND SUBSTRING(r.RouteName, 1, 1) % 8 <= DATEPART(dw, @currentDate))
					THEN 
						1
					WHEN (SUBSTRING(r.RouteName, 1, 1) % 8 < DATEPART(dw, @currentMonthStart)
						  AND SUBSTRING(r.RouteName, 1, 1) % 8 > DATEPART(dw, @currentDate))
					THEN 
						-1
					ELSE
						0
				 END) AS VisitsCount
			 FROM tblRoutes r
    			INNER JOIN tblOutletRoutes olr ON r.Route_id = olr.Route_id 
	    		INNER JOIN tblOutlets ol ON olr.OL_id = ol.OL_id
		        INNER JOIN #merchandisersIDs m ON m.id = r.Merch_id
             WHERE r.Status = 2 
               AND olr.Status = 2
		) t
		GROUP BY OL_id
			   , Merch_id
	) PlanedVisits ON PlanedVisits.OL_id = vwMerch_OL.OL_id 
				  AND PlanedVisits.Merch_id = vwMerch_OL.Merch_ID
LEFT JOIN -- ����������� ���������� �������.
	(
		SELECT och.OL_id
			 , olmc.Merch_ID
			 , COUNT(DISTINCT och.OLCard_id) AS VisitsCount
		FROM tblOutletCardH och
		INNER JOIN #OL_Merch_Chanal olmc ON olmc.OL_id = och.OL_id AND olmc.Merch_ID = 
		(
		CASE 
			WHEN (NOT EXISTS( -- ��������� ��-�� ��, ���������� �� ��
							SELECT 1 
							FROM #OL_Merch_Chanal olmc1 
							WHERE olmc1.OL_id = olmc.OL_id 
							  AND olmc1.RelatedMerch_CNT = 2 -- ���������� ��������� �� ��� ������ ��
							)
				  ) 
			THEN -- �� �� ������� ������ ���� ��.
				 -- �� ����������� ��, ���������� �����, ���������� ����� �� ���� ��, ������� ������� �� �� ����� �������
				olmc.Merch_ID 
			ELSE -- �� �� �������� ��������� ��.
				CASE
					WHEN ( -- ��������� ��, �������� �� �� � ��
						  EXISTS(
								SELECT 1 
								FROM #OL_Merch_Chanal olmc2
								WHERE olmc2.OL_id = olmc.OL_id 
									AND olmc2.Merch_Id = och.Merch_Id -- ��, ��������
								)
						 )
					THEN --  
						och.Merch_Id 
					ELSE -- �������� �� �� ������� � ���� ��� ����� � �������� �� �� ���������� ����� ��������. 
						 -- �������� ������ ���������� ����� � ��, ������� �������� �����
						(SELECT TOP 1 olmc3.Merch_ID
						FROM #OL_Merch_Chanal olmc3
						INNER JOIN tblSyncStatusActTypesLinks matl ON olmc3.ActivityType = matl.ActivityType 
																  AND matl.Merch_id = och.Merch_id
																  AND olmc3.OL_id = och.OL_id
						)
				END
		END
		)
		WHERE
			och.OLCardDate BETWEEN @currentMonthStart AND @currentDate
		GROUP BY och.OL_id
			   , olmc.Merch_ID
	) FactVisits ON FactVisits.OL_id = vwMerch_OL.OL_id 
				AND FactVisits.Merch_id = vwMerch_OL.Merch_id
LEFT JOIN tblProximityFactors pFactor ON pFactor.Proximity_id = tblOutlets.Proximity
								
ORDER BY tblOutlets.OL_id

DROP TABLE #merchandisersIDs
DROP TABLE #OL_Merch_Chanal
";
            }
        }

        public static string ProductsMotion
        {
            get
            {
                return
                    @"
-- =============================================
-- Author:		Developer 1
-- Create date: 24-11-2009
-- Description:	Selects all data for 'Product motion' report.
-- =============================================

-- Input parameters
-- ******************* Data range for the report *******************
-- @lowerDate as datetime - 
-- @upperDate as datetime
-- *****************************************************************

-- ********************* Supervisor filter ID **********************
-- @supervisorId as INTEGER
-- *****************************************************************

-- ********************* Merchandisers ID list *********************
-- @merchandisersIDs as nvarchar
-- @merchandisersCnt as INTEGER
-- It is a list of IDs. The list is a fixed width (10 characters) 
-- string like '         2,     12345,1234567898'
-- the function dbo.fnDW_Common_SplitText must be used to transform 
-- the string IDs into a table representation
-- @merchandisersCnt - count of mrecandisers should be returned
-- *****************************************************************

--DECLARE @lowerDate AS DATETIME
--DECLARE @upperDate AS DATETIME
--DECLARE @supervisorId AS INTEGER
--DECLARE @merchandisersIDs AS NVARCHAR(4000)
--DECLARE @merchandisersCnt AS INTEGER
--
--set @merchandisersIDs = '  14500001,  14500002,  14500003,  14500004'
--set @merchandisersCnt = 0
--set @supervisorId = NULL
--set @lowerDate = '2009-08-01'
--set @upperDate = '2009-10-30'

-- Define MErchandisers IDs list
CREATE TABLE #merchandisersIDs (id INT NOT NULL PRIMARY KEY)

If (@merchandisersCnt <> 0 )
	BEGIN
		INSERT INTO #merchandisersIDs
		SELECT ID from dbo.Parse_IntList(@merchandisersIDs)
		
		DELETE #merchandisersIDs WHERE id NOT IN 
		(SELECT m.Merch_id
		FROM dbo.tblMerchandisers  m 
			INNER JOIN tblMerchandiserUserTypes t ON t.merch_id = m.merch_id AND t.status = 2 
			INNER JOIN DW_FSM_UserTypes dfut ON t.UserType_id = dfut.UserType
		WHERE GETDATE() BETWEEN dfut.StartDate AND dfut.EndDate)			
	END
ELSE	
	BEGIN 
		INSERT #merchandisersIDs VALUES (-1) 
	END

CREATE TABLE #products
(
  Product_Id		INT NOT NULL PRIMARY KEY,
  ProductName		VARCHAR(50), 
  ProdTypeName		VARCHAR(50),
  ProdGroupName		VARCHAR(50),
  Unit_Name			VARCHAR(5),
  Packing_Capacity  NUMERIC(10,3),
  productVolume		NUMERIC(7,3),
  Packing_Type		VARCHAR(9),
  ProductCnName		VARCHAR(50)
)


-- �������� ��������� � ��� ���������� � ��� ������
INSERT INTO #products
SELECT p.Product_Id
	 , p.ProductName     -- ���
	 , pt.ProdTypeName   -- ����
	 , pg.ProdGroupName  -- �����
	 , u.Unit_Name       -- ��� ��������
	 , p.productVolume * 10 AS Packing_Capacity -- ������� ��������
	 , p.productVolume 
	 , CASE WHEN ptp.ProductName LIKE '%BNR%' 
	   THEN 
			CASE WHEN ptp.ProductName LIKE '% ��%' 
			THEN 
				'BNR ��' 
			ELSE  
				CASE WHEN ptp.ProductName LIKE '% InBev%' 
				THEN 
					'BNR InBev' 
				ELSE 'BNR' END 
			 END
		ELSE 'Other' 
		END  AS Packing_Type -- ��� ��������
	 , pc.ProductCnName      -- ������������ ��������������� ���������	
FROM tblProducts p 
INNER JOIN tblProductTypes pt ON pt.ProductType_id = p.ProductType_id 
INNER JOIN tblProductGroups pg ON pt.ProdGroup_ID = pg.ProdGroup_ID 
INNER JOIN tblUnits u ON p.Unit_ID = u.Unit_ID 
INNER JOIN tblProductCombine pc ON p.ProductCn_Id = pc.ProductCn_Id 
LEFT JOIN tblProducts ptp ON p.Tare_Id = ptp.Product_Id

-- �������� ������ �� �������� ������� ������.
SELECT CONVERT(datetime,CONVERT(VARCHAR(10),och.OLCardDate,104),104) AS OrderDate -- ����    
     , och.OL_id
     , och.Merch_id
	 , ooh.OrderNo	 
	 , SUM(ood.Product_qty) AS OrderedProductVolume 
	 , MAX(ood.Product_id)  AS Product_id
	 , p.ProductCn_Id	 
into #Orders	 
FROM tblOutletCardH och
	INNER JOIN tblOutletOrderH ooh ON och.OLCard_id = ooh.OLCard_id
	INNER JOIN #merchandisersIDs m ON m.id = och.Merch_id
	INNER JOIN tblOutletOrderD ood ON ood.OrderNo = ooh.OrderNo 
	INNER JOIN tblProducts p ON p.Product_id = ood.Product_id	
WHERE CONVERT(datetime,CONVERT(VARCHAR(10),och.OLCardDate,104),104) BETWEEN @lowerDate AND @upperDate 
  AND ooh.OrderNo <> CAST(0 AS BIGINT)
GROUP BY och.OLCardDate
       , och.OL_id
       , och.Merch_id
       , ooh.OrderNo
       , p.ProductCn_Id	

--�������� ������ �� ���� �������� ���� ��������� ����� ����
SELECT soh.OrderNo     
     , SUM(sod.Product_qty) AS SaledOrderedProductVolume
     , MAX(sod.Product_Id) as Product_Id
     , p.ProductCn_Id	
	 , CONVERT(datetime,CONVERT(VARCHAR(10),soh.Date, 104),104) AS Date
INTO #Sales	 		 
FROM tblSalOutH soh
	INNER JOIN tblSalOutD sod ON sod.Invoice_Id = soh.Invoice_Id AND soh.Status <> 9 
	INNER JOIN tblProducts p ON p.Product_Id = sod.Product_Id 
--	INNER JOIN #merchandisersIDs m ON m.id = soh.Merch_id--igetman 20111024 � ����� ������ �� �������� �� �� ������ �1 
    INNER JOIN vwDW_OutletsMerchandiserActivity_Link voml ON soh.ol_id=voml.ol_id  	
	INNER JOIN #merchandisersIDs m ON m.id = soh.Merch_id
WHERE soh.Status <> 9
  AND soh.Date >= @lowerDate
  AND soh.OrderNo <> CAST(0 AS BIGINT)
GROUP BY soh.OrderNo, p.ProductCn_Id, soh.Date	

SELECT OrderNo     
     , SUM(SaledOrderedProductVolume) AS SaledOrderedProductVolume
     , Product_Id Product_Id
     , ProductCn_Id	
	 , min(CONVERT(datetime,CONVERT(VARCHAR(10),Date, 104),104)) AS Date
INTO #sales1	 
FROM #Sales	
GROUP BY OrderNo, Product_Id, ProductCn_Id  


SELECT  ROW_NUMBER() OVER (PARTITION BY OrderNo, ProductCn_Id  ORDER BY (SELECT 1)) AS Part
	, olmc.OL_id
	, olmc.Merch_Id
	, OrderNo
	, OrderedProductVolume                                                  -- ������ �����
	, SaledOrderedProductVolume												-- ��������
	, RemainsProductVolume												    -- ��������
	, WithoutOrder                                                          -- ��� ������
	, CONVERT(datetime,CONVERT(VARCHAR(10),OrderDate,104),104) as OrderDate -- ����	
	, ToTakeOffProductVolume                                                -- �� �����	 			 	 
    , OverOrdered                                                           -- ����� ������	 		 
	, Product_id                                                            --�������
	, ProductCn_Id                                                          --�����. �������
INTO #pm0
FROM 
(
-- ������
SELECT CONVERT(datetime,CONVERT(VARCHAR(10),och.OLCardDate,104),104) AS OrderDate -- ����
     , och.OL_id
	 , och.Merch_Id
	 , ooh.OrderNo
	 , ood.Product_qty AS OrderedProductVolume -- ������ �����		  
	 , NULL AS RemainsProductVolume            -- ��������
	 , NULL AS SaledOrderedProductVolume       -- ��������
	 , NULL AS ToTakeOffProductVolume          -- �� �����	 
	 , NULL AS WithoutOrder                    -- ��� ������
	 , NULL AS OverOrdered                     -- ����� ������	 		 
	 , ood.Product_id		 
	 , p.ProductCn_Id	 
FROM tblOutletCardH och
	INNER JOIN tblOutletOrderH ooh ON och.OLCard_id = ooh.OLCard_id
	INNER JOIN #merchandisersIDs m ON m.id = och.Merch_id
	INNER JOIN tblOutletOrderD ood ON ood.OrderNo = ooh.OrderNo 
	INNER JOIN tblProducts p ON p.Product_id = ood.Product_id	
WHERE CONVERT(datetime,CONVERT(VARCHAR(10),och.OLCardDate,104),104) BETWEEN @lowerDate AND @upperDate 

-- �������
UNION ALL  
SELECT CONVERT(datetime,CONVERT(VARCHAR(10),och.olcarddate,104),104) AS OrderDate             -- ����		 
     , och.OL_id
     , och.Merch_Id
	 , ooh.OrderNo	 
	 , NULL AS OrderedProductVolume            -- ������ �����	
     , od.IsPresent AS RemainsProductVolume    -- �������
     , NULL AS SaledOrderedProductVolume       -- ��������
     , NULL AS ToTakeOffProductVolume          -- �� �����	 			 
	 , NULL AS WithoutOrder                    -- ��� ������
	 , NULL AS OverOrdered                     -- ����� ������	 		 
     , p.Product_Id		 
	 , p.ProductCn_Id			
FROM tblOutletCardH och		
	LEFT JOIN tblOutletDistribution od ON och.OLCard_id = od.OLCard_id  
	INNER JOIN #merchandisersIDs m ON m.id = och.Merch_id
	LEFT JOIN tblOutletOrderH ooh ON od.OLCard_id = ooh.OLCard_id         	 						         	 						 
	LEFT JOIN tblProducts p ON p.Product_id = od.Product_id
WHERE CONVERT(datetime,CONVERT(VARCHAR(10),och.OLCardDate,104),104) BETWEEN @lowerDate AND @upperDate 

-- ³�������
UNION ALL
SELECT CONVERT(datetime,CONVERT(VARCHAR(10),soh.Date, 104),104) AS OrderDate   -- ����	
	 , soh.ol_id 
	 , vwi.Merch_Id_Assigned --soh.merch_id
	 , soh.OrderNo 
	 , NULL AS OrderedProductVolume                 -- ������ �����		 
	 , NULL AS RemainsProductVolume                 -- ��������    
     , SUM(sod.Product_qty) AS SaledOrderedProductVolume
     , NULL AS ToTakeOffProductVolume               -- �� �����	 			 
	 , NULL AS WithoutOrder                         -- ��� ������
	 , NULL AS OverOrdered                          -- ����� ������	 	
     , (sod.Product_Id) as Product_Id
     , p.ProductCn_Id	
FROM tblSalOutH soh
	inner join tblSalOutD sod on soh.Invoice_id = sod.Invoice_id
	inner join tblProducts p on sod.product_id = p.product_id
	inner join dbo.DW_Visits_With_Invoices vwi on vwi.Invoice_id = soh.Invoice_id
	inner join #merchandisersIDs m ON m.id = vwi.Merch_Id_Assigned		
where vwi.Date BETWEEN @lowerDate AND @upperDate --dateadd(dd, 1, @upperDate)
GROUP BY soh.Date
       , soh.ol_id 
	   , vwi.Merch_Id_Assigned--soh.merch_id
	   , soh.OrderNo 	       
       , p.ProductCn_Id   
       , sod.product_id 

-- ³������� ��� ������
UNION ALL 
SELECT CONVERT(datetime,CONVERT(VARCHAR(10),soh.Date,104),104) as OrderDate
	 , soh.OL_Id
	 , soh.Merch_id
	 , soh.OrderNo
	 , NULL AS OrderedProductVolume                   -- ������ ����� 
	 , NULL AS RemainsProductVolume                   -- ��������
	 , NULL AS SaledOrderedProductVolume              -- ��������
	 , NULL AS ToTakeOffProductVolume                 -- �� �����	 			 
	 , CASE WHEN sod.Product_qty < 0 
				THEN NULL
				ELSE sod.Product_qty
		   END AS WithoutOrder                        -- ��� ������                   
	 , NULL AS OverOrdered                            -- ����� ������					  
	 , sod.Product_id
	 , p.ProductCn_Id	    	      
FROM tblSalOutH soh 	
	INNER JOIN tblSaloutD sod      ON soh.Invoice_Id = sod.Invoice_Id AND soh.Status <> 9
	INNER JOIN #merchandisersIDs m ON m.id = soh.Merch_id
	INNER JOIN tblProducts p       ON p.Product_id = sod.Product_id
	LEFT JOIN tblOutletOrderD ood  ON soh.OrderNo = ood.OrderNo AND sod.Product_id = ood.Product_id
	LEFT JOIN tblOutletOrderH ooh  ON soh.OrderNo = ooh.OrderNo
	LEFT JOIN tblOutletCardH och   ON och.OLCard_id = ooh.OLCard_id
WHERE CONVERT(datetime,CONVERT(VARCHAR(10),soh.Date,104),104) BETWEEN @lowerDate AND @upperDate 
  AND soh.OrderNo = CAST(0 AS BIGINT)
  
-- �� �����, ����� ������
UNION ALL 
SELECT case 
          when o.OrderDate is null 
          then CONVERT(datetime,CONVERT(VARCHAR(10),s.Date,104),104)
          else CONVERT(datetime,CONVERT(VARCHAR(10),o.OrderDate ,104),104) 
       end as OrderDate                    -- ���� ������
     , o.OL_Id                        -- ��
     , o.Merch_id                     -- ��
     , case 
           when o.OrderNo is null
           then s.orderno
           else o.OrderNo
       end as OrderNo                      -- ����� ������  
     , null OrderedProductVolume         -- ������ �����
     , null RemainsProductVolume         -- �������
     , null SaledOrderedProductVolume    -- ��������
    , (CASE
            WHEN isnull(o.OrderedProductVolume,0) > ISNULL(s.SaledOrderedProductVolume,0) 
			THEN isnull(o.OrderedProductVolume,0) - ISNULL(s.SaledOrderedProductVolume,0)  
			ELSE NULL
		END ) as ToTakeOffProductVolume       -- �� ����� 
     , null WithoutOrder                      -- ��� ������
     , ( CASE
             WHEN isnull(o.OrderedProductVolume,0) < ISNULL(s.SaledOrderedProductVolume,0) 
			 THEN ISNULL(s.SaledOrderedProductVolume,0) - ISNULL(o.OrderedProductVolume,0)
			 ELSE NULL
		 END ) as OverOrdered                 -- ����� ������
     , case 
           when o.Product_id is null
           then s.product_id
           else o.Product_id
       end as Product_id                  -- �������
     , case 
           when o.Productcn_id is null
           then s.productcn_id
           else o.Productcn_id
       end as Productcn_id                 -- ����������� ������� 
FROM  #Sales1 s 
	inner join #Orders o ON o.orderno = s.orderno and o.product_id = s.product_id
) AS os
INNER JOIN dbo.vwDW_OutletsMerchandiserActivity_Link AS olmc ON olmc.OL_id = os.OL_id 
		AND olmc.Merch_ID = 
		(
		CASE 
			WHEN (NOT EXISTS( -- ��������� ��-�� ��, ���������� �� ��
							SELECT 1 
							FROM dbo.vwDW_OutletsMerchandiserActivity_Link olmc1 
							WHERE olmc1.OL_id = olmc.OL_id 
							  AND olmc1.RelatedMerch_CNT = 2 -- ���������� ��������� �� ��� ������ ��
							)
				  ) 
			THEN -- �� �� ������� ������ ���� ��.
				 -- �� ����������� ��, ���������� �����, ���������� ����� �� ���� ��, ������� ������� �� �� ����� �������
					olmc.Merch_ID 
			ELSE -- �� �� �������� ��������� ��.
				CASE
					WHEN ( -- ��������� ��, �������� �� �� � ��
						  EXISTS(
								SELECT 1 
								FROM dbo.vwDW_OutletsMerchandiserActivity_Link olmc2
								WHERE olmc2.OL_id = olmc.OL_id 
									AND olmc2.Merch_Id = os.Merch_Id -- ��, ��������
								)
						 )
					THEN --  
						os.Merch_Id 
					ELSE -- �������� �� �� '�������' � ���� ��� ����� � �������� �� �� '���������� ����� ��������'. 
						 -- �������� ������ ���������� ����� � ��, ������� �������� �����
						(SELECT TOP 1 olmc3.Merch_ID
						FROM dbo.vwDW_OutletsMerchandiserActivity_Link olmc3
						INNER JOIN tblSyncStatusActTypesLinks matl ON olmc3.ActivityType = matl.ActivityType 
																  AND matl.Merch_id = os.Merch_id
																  AND olmc3.OL_id = os.OL_id
						)
				END
		END
		)


SELECT sp.Cust_Name                                                                              -- ����� �������������
	  , s.Supervisor_name																	     -- ����������
	  , c.City_Name																		         -- �����
	  , d.District_Name																			 -- �������	  
	  , NULL AS Region_Name																		 -- ����� (��) TODO: join to tblCountryArea
	  , a.Area_Name																				 -- ����� � ������ (��) 
	  , st.Settlement AS Territory_Type                                                          -- ��� ���������� TODO: join to  tblSettlement
	  , m.MerchName																				 -- ��	  
	  , ms.LValue AS MerchandiserStatus															 -- ������ ��
	  , CONVERT(datetime,CONVERT(VARCHAR(10),os.OrderDate,104),104) as OrderDate                 -- ����
	  , ISNULL(os.OrderedProductVolume, 0) * p.ProductVolume AS OrderedProductVolume	         -- ������ ����� 		  
	  , ISNULL(os.SaledOrderedProductVolume, 0) * p.ProductVolume AS SaledOrderedProductVolume   -- �������� 	  
	  , ISNULL(os.ToTakeOffProductVolume, 0) * p.ProductVolume AS ToTakeOffProductVolume         -- �� �����      
	  , ISNULL(os.RemainsProductVolume, 0)  * p.ProductVolume AS RemainsProductVolume            -- �������� TODO:
      , ISNULL(os.WithoutOrder, 0) * p.ProductVolume AS WithoutOrder						     -- ��� ������
	  , ISNULL(os.OverOrdered, 0) * p.ProductVolume AS OverOrdered							     -- ����� ������	  
      , os.OL_id																				 -- ��� ��
	  ,	ol.OL_Code																				 -- ������� �����
	  , ols.LValue as OutletsSatus																 -- ������ ��
	  , olt.OLtype_name																			 --�����
	  , olst.OLSubTypeName																		 -- ��������
	  , ol.OLName																				 --���� ���
	  , ol.OLDeliveryAddress																	 -- ����. �����
	  , ol.OLTradingName																		 -- ��. ���
	  , ol.OLAddress																			 --��. �����
	  , p.Product_Id
	  , p.ProductName																			 -- ���
	  , p.ProdGroupName																			 -- �����	  
	  , p.ProdTypeName																			 -- ����
	  , p.Unit_Name																				 -- ��� ��������
	  , p.Packing_Capacity																         -- ������� ��������
	  , p.Packing_Type							          					         -- ��� ��������
	  , p.ProductCnName														 -- ������������ ��������������� ���������
	  , pf.ProximityName AS ProximityFactor												 -- ������ ��������
	  , ppl.Population														 -- ���������	      
FROM #pm0 os
INNER JOIN tblMerchandisers m ON m.Merch_id = os.Merch_id
INNER JOIN #merchandisersIDs m1 ON m1.id = m.merch_id
INNER JOIN tblGloballookup ms ON ms.LKey = m.Status 
							  AND ms.TableName = 'tblMerchandisers' 
							  AND ms.FieldName = 'Status'
INNER JOIN tblOutlets ol ON os.OL_id = ol.OL_id
INNER JOIN tblOutletSubTypes olst ON ol.OLSubType_id = olst.OLSubType_id 
INNER JOIN tblOutLetTypes olt ON olst.OLType_ID = olt.OLType_id 
INNER JOIN tblGloballookup ols ON ols.LKey = ol.Status 
							  AND ols.TableName = 'tblOutlets' 
							  AND ols.FieldName = 'Status'
INNER JOIN tblSupervisors s ON m.Supervisor_ID = s.Supervisor_id AND s.Status <> 9
						  AND (@supervisorId is NULL OR @supervisorId = m.Supervisor_ID) 
INNER JOIN 
	(
		SELECT  ROW_NUMBER() OVER (PARTITION BY Supervisor_id ORDER BY (SELECT 1)) CustomerOrder
			 ,  Supervisor_id
			 ,  Cust_Id
		FROM tblCustomerSupervisors tcs
		WHERE (@supervisorId is NULL OR @supervisorId = tcs.Supervisor_ID)  
	) AS cs ON cs.Supervisor_id = s.Supervisor_id	
			AND cs.CustomerOrder = 1	-- �.�. ��� ����������� ����� ������� ��, 
										-- �������� ������ ���������� �� ������������� ��
INNER JOIN tblCustomers sp ON cs.Cust_id = sp.Cust_Id 
INNER JOIN tblAreas a      ON ol.Area_id = a.Area_Id
INNER JOIN tblCities c     ON c.City_id = a.City_Id
INNER JOIN tblDistricts d  ON d.District_id = c.District_id
INNER JOIN #products p     ON p.Product_id = os.Product_id
LEFT JOIN tblProximityFactors pf ON pf.Proximity_id = ol.Proximity 
LEFT JOIN dbo.tblSettlement st   ON c.Settlement_id = st.Settlement_id
LEFT JOIN dbo.tblPopulation ppl  ON c.Population_id = ppl.Population_id
WHERE os.orderdate IS NOT NULL

-- ��������� ������� �������� �������
DROP TABLE #merchandisersIDs
DROP TABLE #products
DROP TABLE #pm0
DROP TABLE #Orders
DROP TABLE #Sales
DROP TABLE #Sales1


";
            }
        }

        public static string RoutesByOutlets
        {
            get
            {
                return
                    @"
-- =============================================
-- Author:		Semyon Krotkih
-- Create date: 25-11-2009
-- Description:	Selects all needed data for 'Routes by outlets' report.
-- Input parameters:
--		Data range for the report:
--			@lowerDate as datetime - 
--			@upperDate as datetime

--		Supervisor filter ID:
--			@supervisorId as INTEGER
--		Merchandisers ID list:
--			@merchandisersIDs as nvarchar
--		Count of mrecandisers should be returned
--			@merchandisersCnt as INTEGER

--		It is a list of IDs. The list is a fixed width (10 characters) 
--		string like '         2,     12345,1234567898'
--		the string IDs into a table representation
-- 
-- =============================================

SET DATEFIRST 1
/*
DECLARE @lowerDate as datetime
DECLARE @upperDate as datetime

set @lowerDate = '2009-10-01'
set @upperDate = '2009-10-31'


DECLARE @supervisorId AS INTEGER
DECLARE @merchandisersIDs AS NVARCHAR(4000)
DECLARE @merchandisersCnt AS INTEGER

set @merchandisersIDs = '  14500001,  14500002,  14500003,  14500004'
set @merchandisersCnt = 0
set @supervisorId = NULL
*/
-- Define Merchandisers IDs list
CREATE TABLE #merchandisersIDs (id INT NOT NULL)
BEGIN
	DECLARE @s BIGINT, @e INT, @id INT
	DECLARE @nv NVARCHAR(10), @cnt INT
	DECLARE @string NVARCHAR(4000)
    DECLARE @merch_type INT
	
	If (@merchandisersCnt <> 0 )
		BEGIN
			SET @string = @merchandisersIDs
			SET @s = 1
			SET @cnt = @merchandisersCnt
			WHILE @cnt > 0
			BEGIN
				SET @nv = SUBSTRING(@string, @s, 10)
				SET @s = @s + 11
				SET @id  = cast(@nv as int )
				SELECT @merch_type = t.UserType_id 
                FROM dbo.tblMerchandisers  m 
                     INNER JOIN tblMerchandiserUserTypes t ON t.merch_id = m.merch_id AND m.Merch_id = @id and t.status = 2
				IF @merch_type IN (SELECT UserType FROM DW_FSM_UserTypes dfut WHERE GETDATE() BETWEEN dfut.StartDate AND dfut.EndDate)  -- Added by Huch R 
                BEGIN                
                    INSERT #merchandisersIDs VALUES (@id) 
                END
				SET @cnt = @cnt -1
			END
		END
	ELSE	
		BEGIN 
			INSERT #merchandisersIDs VALUES (-1) 
		END
END

print 'Fill #Routes'
-- ��������� �������� � ����������� ���������� ��� ������ � ���� �� �� � ��.
SELECT ROW_NUMBER() OVER (PARTITION BY OL_id, Merch_ID ORDER BY (SELECT 1)) ROUTEDAYS_CNT
	, OL_id
	, Merch_ID
	, RouteName
	, RouteDay
INTO #Routes
FROM
(
	SELECT ROW_NUMBER() OVER (PARTITION BY tblOutletRoutes.OL_id, tblRoutes.Merch_ID, (SUBSTRING(tblRoutes.RouteName, 1, 1) % 8) ORDER BY (SELECT 1)) ROUTES_CNT
		 , tblOutletRoutes.OL_id
		 , tblRoutes.Merch_ID
		 , tblRoutes.RouteName
		 , SUBSTRING(tblRoutes.RouteName, 1, 1) % 8 AS RouteDay
	FROM tblRoutes
	INNER JOIN #merchandisersIDs AS IDs ON tblRoutes.Merch_ID = IDs.id 
									OR @merchandisersCnt = 0
	INNER JOIN tblOutletRoutes ON tblRoutes.Route_id = tblOutletRoutes.Route_id 
								AND tblOutletRoutes.Status = 2
	WHERE ISNUMERIC(SUBSTRING(tblRoutes.RouteName, 1, 1)) <> 0
) AS Routes
WHERE ROUTES_CNT = 1

print 'Fill #OL_Merch_Chanal'
-- �������� ��, ������ ����������� �� � ��������� � ������
SELECT r.OL_id 
	 , r.Merch_Id
	 , matl.ActivityType
	 , ROW_NUMBER() OVER (PARTITION BY r.OL_id ORDER BY (SELECT 1))  RelatedMerch_CNT -- ��������� �� �-�� ��, ������������� ����� �� 
INTO #OL_Merch_Chanal
FROM  #Routes r 
INNER JOIN tblOutlets ol ON r.OL_id = ol.OL_id
	   AND ol.Status = 2
INNER JOIN 
( -- ���������  ���� ������� ����� ��
	SELECT  ROW_NUMBER() OVER (PARTITION BY Merch_Id ORDER BY (SELECT 1)) MerchActivity_ID
		 ,  Merch_ID
		 ,  ActivityType
	FROM tblSyncStatusActTypesLinks 
		INNER JOIN #merchandisersIDs AS IDs ON IDs.id = 
					CASE WHEN @merchandisersCnt = 0 THEN -1 ELSE Merch_ID END
) matl ON matl.MerchActivity_ID = 1  -- �������� ������ ���������� Activity (�����) � ��
	  AND r.Merch_Id = matl.Merch_Id 
WHERE r.ROUTEDAYS_CNT = 1




print 'Fill #Correct_Visits'
-- ��������� �� ��������� ������� ������ � ����������� ��
SELECT vis.Merch_id
	 , vis.OL_id
	 , vis.OLCardDate
	 , vis.OLCard_id
	 , vis.BeginTime
	 , vis.EndTime
	 , vis.Comments
         , vis.OrderNo    
	 , route_vis.RouteDay
	 , route_vis.RouteName
	 , Orders.RemaindersCount
	 , Orders.OrdersCount
INTO #Correct_Visits
FROM 
(
	SELECT olmc.Merch_id
		 , och.OL_id
		 , och.OLCardDate	
		 , och.OLCard_id
		 , och.BeginTime
		 , och.EndTime
		 , och.Comments
         , soh.OrderNo
	FROM tblOutletCardH och
	INNER JOIN tblOutletOrderH ooh ON och.OLCard_id = ooh.OLCard_id
	INNER JOIN tblSalOutH soh ON ooh.OrderNo = soh.OrderNo
	INNER JOIN #OL_Merch_Chanal olmc ON olmc.OL_id = och.OL_id AND olmc.Merch_id = 
	(CASE 
		WHEN (NOT EXISTS( -- ��������� ��-�� ��, ���������� �� ��
						SELECT 1 
						FROM #OL_Merch_Chanal olmc1 
						WHERE olmc1.OL_id = olmc.OL_id 
						  AND olmc1.RelatedMerch_CNT = 2 -- ���������� ��������� �� ��� ������ ��
						)
			  )
		THEN -- �� �� ������� ������ ���� ��.
			 -- �� ����������� ��, ���������� �����, ���������� ����� �� ���� ��, ������� ������� �� �� ����� �������
			olmc.Merch_Id 
		ELSE -- �� �� �������� ��������� ��.
			CASE
				WHEN ( -- ��������� ��, �������� �� �� � ��
					  EXISTS(
							SELECT 1 
							FROM #OL_Merch_Chanal olmc2
							WHERE olmc2.OL_id = olmc.OL_id 
								AND olmc2.Merch_Id = och.Merch_Id -- ��, ��������
							)
					 )
				THEN 
					och.Merch_Id 
				ELSE -- �������� �� �� ������� � ���� ��� ����� � �������� �� �� ���������� ����� ��������. 
					 -- �������� ������ ���������� ����� � ��, ������� �������� �����
					(SELECT TOP 1 olmc3.Merch_ID
					FROM #OL_Merch_Chanal olmc3
					INNER JOIN tblSyncStatusActTypesLinks matl ON olmc3.ActivityType = matl.ActivityType 
															  AND matl.Merch_id = och.Merch_id
															  AND olmc3.OL_id = och.OL_id
					)
			END
	END)
	WHERE och.OLCardDate BETWEEN @lowerDate AND @upperDate
) AS vis
LEFT JOIN #Routes route_vis ON route_vis.OL_id = vis.OL_id
	  AND route_vis.Merch_id = vis.Merch_id
	  AND route_vis.RouteDay = DATEPART(dw, vis.OLCardDate)
LEFT JOIN
(
	SELECT OLCard_id
	 , SUM(OrdersCount) AS OrdersCount
	 , SUM(RemaindersCount) AS RemaindersCount
	FROM
	(
			SELECT och.OLCard_id
				, SUM(tblOutletOrderD.Product_Qty * tblProducts.ProductVolume) AS OrdersCount
				, NULL AS RemaindersCount
			FROM tblOutletCardH och
			INNER JOIN tblOutletOrderH ON tblOutletOrderH.OLCard_id = och.OLCard_id
			INNER JOIN tblOutletOrderD ON tblOutletOrderD.OrderNo = tblOutletOrderH.OrderNo
			INNER JOIN tblProducts ON tblProducts.Product_id = tblOutletOrderD.Product_id
			INNER JOIN tblProductTypes ON tblProducts.ProductType_id = tblProductTypes.ProductType_id 
			WHERE och.OLCardDate BETWEEN @lowerDate AND @upperDate
			GROUP BY och.OLCard_id

			UNION ALL

			SELECT och.OLCard_id
				, NULL AS OrdersCount
				, SUM(tblOutletDistribution.IsPresent * tblProducts.ProductVolume) AS RemaindersCount
			FROM tblOutletCardH och
			INNER JOIN tblOutletDistribution ON tblOutletDistribution.OlCard_id = och.OLCard_id
			INNER JOIN tblProducts ON tblProducts.Product_id = tblOutletDistribution.Product_id
			INNER JOIN tblProductTypes ON tblProducts.ProductType_id = tblProductTypes.ProductType_id 
			WHERE och.OLCardDate BETWEEN @lowerDate AND @upperDate
			GROUP BY och.OLCard_id
	) AS Orders
	GROUP BY OLCard_id
) AS Orders ON Orders.OLCard_id = vis.OLCard_id
group by vis.Merch_id
	   , vis.OL_id
	   , vis.OLCardDate
	   , vis.OLCard_id
	   , vis.BeginTime
	   , vis.EndTime
	   , vis.Comments
	   , vis.OrderNo    
	   , route_vis.RouteDay
	   , route_vis.RouteName
	   , Orders.RemaindersCount
	   , Orders.OrdersCount
--where vis.OL_id = 1003801478

print 'Fill #Correct_Sales'
-- ��������� �� ��������� ������� ������ � ����������� ��
SELECT distinct sale.Merch_id
	 , sale.OL_id
	 , sale.[Date]
         , sale.OrderNo
	 , sale.SoldCount
	-- , route_vis.RouteDay
	-- , route_vis.RouteName
INTO #Correct_Sales
FROM 
(
	SELECT olmc.Merch_id
		 , sal.OL_id
		 , sal.[Date]
		 , SUM(tblSalOutD.Product_Qty * tblProducts.ProductVolume) AS SoldCount
                 , sal.OrderNo
	FROM tblSalOutH sal
	INNER JOIN tblSalOutD ON tblSalOutD.Invoice_id = sal.Invoice_id 
	INNER JOIN tblProducts ON tblProducts.Product_id = tblSalOutD.Product_id
	INNER JOIN #OL_Merch_Chanal olmc ON olmc.OL_id = sal.OL_id AND olmc.Merch_id = 
	(CASE 
		WHEN (NOT EXISTS( -- ��������� ��-�� ��, ���������� �� ��
						SELECT 1 
						FROM #OL_Merch_Chanal olmc1 
						WHERE olmc1.OL_id = olmc.OL_id 
						  AND olmc1.RelatedMerch_CNT = 2 -- ���������� ��������� �� ��� ������ ��
						)
			  )
		THEN -- �� �� ������� ������ ���� ��.
			 -- �� ����������� ��, ���������� �����, ���������� ����� �� ���� ��, ������� ������� �� �� ����� �������
			olmc.Merch_Id 
		ELSE -- �� �� �������� ��������� ��.
			CASE
				WHEN ( -- ��������� ��, �������� �� �� � ��
					  EXISTS(
							SELECT 1 
							FROM #OL_Merch_Chanal olmc2
							WHERE olmc2.OL_id = olmc.OL_id 
								AND olmc2.Merch_Id = sal.Merch_Id -- ��, ��������
							)
					 )
				THEN 
					sal.Merch_Id 
				ELSE -- �������� �� �� ������� � ���� ��� ����� � �������� �� �� ���������� ����� ��������. 
					 -- �������� ������ ���������� ����� � ��, ������� �������� �����
					(SELECT TOP 1 olmc3.Merch_ID
					FROM #OL_Merch_Chanal olmc3
					INNER JOIN tblSyncStatusActTypesLinks matl ON olmc3.ActivityType = matl.ActivityType 
															  AND matl.Merch_id = sal.Merch_id
															  AND olmc3.OL_id = sal.OL_id
					)
			END
	END)
	WHERE sal.[Date] >= @lowerDate--  BETWEEN @lowerDate AND @upperDate
					     AND sal.Status <> 9
	GROUP BY olmc.Merch_id
		 , sal.OL_id
		 , sal.[Date]
         , sal.OrderNo
) AS sale
LEFT JOIN #Routes route_vis ON route_vis.OL_id = sale.OL_id
	left join #Correct_Visits cv ON cv.orderno = sale.orderno
	  AND route_vis.Merch_id = sale.Merch_id
	  AND route_vis.RouteDay = DATEPART(dw, sale.[Date])
group by sale.Merch_id
	   , sale.OL_id
	   , sale.[Date]
           , sale.OrderNo
  	   , sale.SoldCount
	   , route_vis.RouteDay
	   , route_vis.RouteName
--where sale.OL_id = 1003801478


/*
SELECT ds.Date
	 , ds.RouteName
	 , tblOutlets.OLTradingName
	 , tblOutlets.OLAddress
	 , tblOutlets.OLName
	 , tblOutlets.OLDeliveryAddress
	 , tblSupervisors.Supervisor_name
	 , tblSyncStatus.MerchName
	 , tblOutletSubTypes.OLSubTypeName
	 , tblOutletTypes.OLtype_name
	 , tblOutlets.OL_Code
	 , tblOutlets.OL_id

	 , SUM(ds.OrdersCount) AS OrdersCount
	 , SUM(ds.RemaindersCount) AS RemaindersCount
	 , SUM(ds.SoldCount) AS SoldCount
	 , CONVERT(VARCHAR(8), ds.BeginTime, 114)  AS BeginTime
	 , CONVERT(VARCHAR(8), ds.EndTime, 114)  AS EndTime
	 , CONVERT(VARCHAR(8), ds.EndTime - ds.BeginTime, 114) AS Duration
	 , ds.Comments 
    -- , ds.orderno     
FROM
(
SELECT VisitDates.Date
	, Routes.RouteName
	, OutletCards.OL_id
	, OutletCards.Merch_id
	, OutletCards.OrdersCount
	, OutletCards.RemaindersCount
	, sale.SoldCount    
	, OutletCards.BeginTime
	, OutletCards.EndTime
	, OutletCards.Comments 
    , sale.orderno   
FROM 
(
	-- Generating a table with dates between @lowerDate and @upparDate
	SELECT DATEADD(day, z.num, @lowerDate) AS [Date]
	FROM (
	SELECT b10.i + b9.i + b8.i + b7.i + b6.i + b5.i + b4.i + b3.i + b2.i + b1.i + b0.i num
	FROM (SELECT 0 i UNION ALL SELECT 1) b0
	CROSS JOIN (SELECT 0 i UNION ALL SELECT 2) b1
	CROSS JOIN (SELECT 0 i UNION ALL SELECT 4) b2
	CROSS JOIN (SELECT 0 i UNION ALL SELECT 8) b3
	CROSS JOIN (SELECT 0 i UNION ALL SELECT 16) b4
	CROSS JOIN (SELECT 0 i UNION ALL SELECT 32) b5
	CROSS JOIN (SELECT 0 i UNION ALL SELECT 64) b6
	CROSS JOIN (SELECT 0 i UNION ALL SELECT 128) b7
	CROSS JOIN (SELECT 0 i UNION ALL SELECT 256) b8
	CROSS JOIN (SELECT 0 i UNION ALL SELECT 512) b9
	CROSS JOIN (SELECT 0 i UNION ALL SELECT 1024) b10
	) z
	WHERE z.num <= DATEDIFF(day, @lowerDate, @upperDate)
) AS VisitDates
INNER JOIN #Routes AS Routes ON Routes.RouteDay = DATEPART(dw, VisitDates.[Date])
LEFT JOIN
(   SELECT vis.OL_id
		 , vis.Merch_Id         
		 , vis.OLCardDate AS [Date]
		 , latest_vis.BeginTime
		 , latest_vis.EndTime
		 , latest_vis.Comments
		 , SUM(vis.OrdersCount) AS OrdersCount
		 , SUM(vis.RemaindersCount) AS RemaindersCount
         , vis.OrderNo          
	FROM #Correct_Visits vis
	INNER JOIN 
	(
		SELECT ROW_NUMBER() OVER (PARTITION BY OL_id, Merch_id, OLCardDate, RouteDay ORDER BY EndTime) LastVisit_NBR
			 , OL_id
			 , Merch_id
			 , OLCardDate
			 , BeginTime
			 , EndTime
			 , Comments
			 , RouteDay
             , OrderNo
		 FROM #Correct_Visits
	)AS latest_vis ON latest_vis.LastVisit_NBR = 1
		AND latest_vis.OLCardDate = vis.OLCardDate
		AND latest_vis.Merch_id = vis.Merch_id
		AND latest_vis.OL_id = vis.OL_id
		AND latest_vis.RouteDay = vis.RouteDay
	GROUP BY vis.OL_id
		, vis.Merch_Id        
		, vis.OLCardDate
		, latest_vis.BeginTime
		, latest_vis.EndTime
		, latest_vis.Comments
        , vis.OrderNo          
) AS OutletCards ON OutletCards.OL_id = Routes.OL_id
				AND OutletCards.Merch_Id = Routes.Merch_id
				AND CONVERT(varchar, OutletCards.[Date], 112) = CONVERT(varchar, VisitDates.[Date], 112)
LEFT JOIN #Correct_Sales sale ON --sale.[Date] = VisitDates.[Date] AND 
		  sale.OL_id = Routes.OL_id
		  AND sale.Merch_id = Routes.Merch_id
		  AND sale.orderno = OutletCards.orderno

UNION ALL

SELECT latest_vis.OLCardDate AS [Date]
	 , vis.RouteName
	 , latest_vis.OL_id
	 , latest_vis.Merch_id     
	 , min(vis.OrdersCount) AS OrdersCount
	 , SUM(vis.RemaindersCount) AS RemaindersCount
	 , SUM(sale.soldcount) AS SoldCount
	 , latest_vis.BeginTime
	 , latest_vis.EndTime
	 , latest_vis.Comments    
     , vis.OrderNo
FROM #Correct_Visits vis
	inner join #Correct_Sales sale ON sale.OrderNo = vis.OrderNo
INNER JOIN 
	(
		SELECT ROW_NUMBER() OVER (PARTITION BY OL_id, Merch_id, OLCardDate ORDER BY EndTime) LastVisit_NBR
			 , OL_id
			 , Merch_id
			 , OLCardDate
			 , BeginTime
			 , EndTime
			 , Comments
             , OrderNo 
		FROM #Correct_Visits
	)AS latest_vis ON latest_vis.LastVisit_NBR = 1
		AND latest_vis.OLCardDate = vis.OLCardDate
		AND latest_vis.Merch_id = vis.Merch_id
		AND latest_vis.OL_id = vis.OL_id
		AND vis.RouteName IS NULL

GROUP BY  latest_vis.OL_id
		, latest_vis.Merch_Id
		, latest_vis.OLCardDate
		, latest_vis.BeginTime
		, latest_vis.EndTime
		, latest_vis.Comments
		, vis.RouteName
        , vis.OrderNo       

--UNION ALL
--
--SELECT sale.[Date]
--	 , sale.RouteName
--	 , sale.OL_id
--	 , sale.Merch_id     
--	 , NULL AS OrdersCount
--	 , NULL AS RemaindersCount
--	 , sale.SoldCount
--	 , NULL AS BeginTime
--	 , NULL AS EndTime
--	 , NULL AS Comments     
--FROM #Correct_Sales sale 
	


--WHERE sale.RouteName IS NULL
) AS ds
INNER JOIN tblOutlets ON tblOutlets.OL_id = ds.OL_id
INNER JOIN tblSyncStatus ON tblSyncStatus.Merch_id = ds.Merch_ID
INNER JOIN tblSupervisors ON tblSyncStatus.Supervisor_ID = tblSupervisors.Supervisor_id AND tblSupervisors.Status <> 9
INNER JOIN tblOutletSubTypes ON tblOutletSubTypes.OLSubType_id = tblOutlets.OLSubType_id
INNER JOIN tblOutletTypes ON tblOutletTypes.OLType_ID = tblOutletSubTypes.OLType_ID
GROUP BY ds.Date
	   , ds.RouteName       
	   , tblOutlets.OLTradingName
	   , tblOutlets.OLAddress
	   , tblOutlets.OLName
	   , tblOutlets.OLDeliveryAddress
	   , tblSupervisors.Supervisor_name
	   , tblSyncStatus.MerchName
	   , tblOutletSubTypes.OLSubTypeName
	   , tblOutletTypes.OLtype_name
	   , tblOutlets.OL_id
	   , tblOutlets.OL_Code
	   , ds.Comments
	   , ds.BeginTime
	   , ds.EndTime
       , ds.orderNo
*/
select t.Date
	 , t.RouteName
	 , t.OLTradingName
	 , t.OLAddress
	 , t.OLName
	 , t.OLDeliveryAddress
	 , t.Supervisor_name
	 , t.MerchName
	 , t.OLSubTypeName
	 , t.OLtype_name
	 , t.OL_Code
	 , t.OL_id
	 , t.OrdersCount AS OrdersCount
	 , t.RemaindersCount AS RemaindersCount
	 , t.SoldCount AS SoldCount
	 , CONVERT(VARCHAR(8), t.BeginTime, 114)  AS BeginTime
	 , CONVERT(VARCHAR(8), t.EndTime, 114)  AS EndTime
	 , CONVERT(VARCHAR(8), t.EndTime - t.BeginTime, 114) AS Duration
	 , t.Comments
from
(select tblOutlets.OLTradingName
	 , tblOutlets.OLAddress
	 , tblOutlets.OLName
	 , tblOutlets.OLDeliveryAddress
	 , tblSupervisors.Supervisor_name
	 , tblSyncStatus.MerchName
	 , tblOutletSubTypes.OLSubTypeName
	 , tblOutletTypes.OLtype_name
	 , tblOutlets.OL_Code	 
     , v.merch_id
     , v.ol_id
     , v.olcarddate as date
     , v.begintime
     , v.endtime
     , min(v.Comments) as Comments
     , v.routeday
     , v.routename
     , sum(v.remainderscount) as remainderscount
     , min(v.orderscount) as orderscount
     , sum(s.soldcount) as soldcount
from #Correct_Visits v 
	LEFT JOIN #Correct_Sales s ON v.merch_id = s.merch_id and v.ol_id = s.ol_id and v.orderno = s.orderno
	INNER JOIN tblOutlets ON tblOutlets.OL_id = v.OL_id
	INNER JOIN tblSyncStatus ON tblSyncStatus.Merch_id = v.Merch_ID
	INNER JOIN tblSupervisors ON tblSyncStatus.Supervisor_ID = tblSupervisors.Supervisor_id AND tblSupervisors.Status <> 9
	INNER JOIN tblOutletSubTypes ON tblOutletSubTypes.OLSubType_id = tblOutlets.OLSubType_id
	INNER JOIN tblOutletTypes ON tblOutletTypes.OLType_ID = tblOutletSubTypes.OLType_ID
group by  tblOutlets.OLTradingName
	 , tblOutlets.OLAddress
	 , tblOutlets.OLName
	 , tblOutlets.OLDeliveryAddress
	 , tblSupervisors.Supervisor_name
	 , tblSyncStatus.MerchName
	 , tblOutletSubTypes.OLSubTypeName
	 , tblOutletTypes.OLtype_name
	 , tblOutlets.OL_Code	 
     , v.merch_id
     , v.ol_id
     , v.olcarddate    
     , v.begintime
     , v.endtime    
     , v.routeday
     , v.routename
) t

 
DROP TABLE #merchandisersIDs
DROP TABLE #OL_Merch_Chanal
DROP TABLE #Correct_Visits
DROP TABLE #Correct_Sales
DROP TABLE #Routes"
                    ;
            }
        }

        public static string StockturnAnalysis
        {
            get
            {
                return
                    @"
-- =============================================
-- Author:		Semyon Krotkih
-- Create date: 30-11-2009
-- Description:	Selects all needed data for 'Stockturn analysis' report.
-- Input parameters:
--		Data range for the report:
--			@lowerDate as datetime - 
--			@upperDate as datetime

--		Supervisor filter ID:
--			@supervisorId as INTEGER
--		Merchandisers ID list:
--			@merchandisersIDs as nvarchar
--		Count of mrecandisers should be returned
--			@merchandisersCnt as INTEGER

--		It is a list of IDs. The list is a fixed width (10 characters) 
--		string like '         2,     12345,1234567898'
--		the function dbo.fnDW_Common_SplitText must be used to transform 
--		the string IDs into a table representation
-- 
-- =============================================

SET DATEFIRST 1
/*
DECLARE @lowerDate as datetime
DECLARE @upperDate as datetime

set @lowerDate = '20091201'
set @upperDate = '20100304'


DECLARE @supervisorId AS INTEGER
DECLARE @merchandisersIDs AS NVARCHAR(4000)
DECLARE @merchandisersCnt AS INTEGER

set @merchandisersIDs = '   8300007,   8400005,   8400007'
set @merchandisersCnt = 3
set @supervisorId = NULL
*/
-- Define Merchandisers IDs list
CREATE TABLE #merchandisersIDs (id INT NOT NULL)
BEGIN
	DECLARE @s BIGINT, @e INT, @id INT
	DECLARE @nv NVARCHAR(10), @cnt INT
	DECLARE @string NVARCHAR(4000)
    DECLARE @merch_type INT
	
	If (@merchandisersCnt <> 0 )
		BEGIN
			SET @string = @merchandisersIDs
			SET @s = 1
			SET @cnt = @merchandisersCnt
			WHILE @cnt > 0
			BEGIN
				SET @nv = SUBSTRING(@string, @s, 10)
				SET @s = @s + 11
				SET @id  = cast(@nv as int )
				SELECT @merch_type = t.UserType_id 
                FROM dbo.tblMerchandisers  m 
                     INNER JOIN tblMerchandiserUserTypes t ON t.merch_id = m.merch_id AND m.Merch_id = @id and t.status = 2
				IF @merch_type IN (SELECT UserType FROM DW_FSM_UserTypes dfut WHERE GETDATE() BETWEEN dfut.StartDate AND dfut.EndDate)  -- Added by Huch R
                BEGIN                
                    INSERT #merchandisersIDs VALUES (@id) 
                END
				SET @cnt = @cnt -1
			END
		END
	ELSE	
		BEGIN 
			INSERT #merchandisersIDs VALUES (-1) 
		END
END

-- ��������� ������� ������ ���, ������� ������ � �� �����.
SELECT ExternalCode
	,Product_id
INTO #kegDebts
FROM
(
	SELECT
		 ROW_NUMBER() OVER (PARTITION BY tblDebtTypes.ExternalCode ORDER BY (SELECT 1))  ExtCode_NBR
		,tblDebtTypes.ExternalCode
		,tblProducts.Product_id

	FROM
		tblDebtTypes
		INNER JOIN tblProducts ON tblProducts.ProductType_ID = 195
			AND tblProducts.Price <> 0
			AND tblProducts.ProductName like '%' + (CASE WHEN tblDebtTypes.ExternalCode = 31 THEN '20%'
														 WHEN tblDebtTypes.ExternalCode = 32 THEN '30%'
														 WHEN tblDebtTypes.ExternalCode = 33 THEN '50%' END)

	WHERE tblDebtTypes.ExternalCode IN (31, 32, 33)
) AS kegDebts
WHERE ExtCode_NBR = 1

-- ������������ ���� ������� ��������� ID ������ ��� ������������� � ������� �� ������.
SELECT Products.Product_id
	, tblDebtTypes.DebtType_id
	, Products.Price
INTO #Product_DebtType
FROM
(
	SELECT (CASE WHEN pBeer.ProductVolume = 2 THEN 31
				 WHEN pBeer.ProductVolume = 3 THEN 32
				 WHEN pBeer.ProductVolume = 5 THEN 33 END) AS ExternalCode
		,pBeer.Product_id
		,pKeg.Price
	FROM tblProducts pBeer
	INNER JOIN tblProducts pKeg ON pKeg.ProductType_ID = 195
			AND pKeg.Price <> 0
			AND pKeg.ProductName like '%' + (CASE WHEN pBeer.ProductVolume = 2 THEN '20%'
												  WHEN pBeer.ProductVolume = 3 THEN '30%'
												  WHEN pBeer.ProductVolume = 5 THEN '50%' END)
			AND pBeer.Unit_id = 3
) AS Products
INNER JOIN tblDebtTypes ON tblDebtTypes.ExternalCode = Products.ExternalCode

-- ��������� �������� � ����������� ���������� ��� ������ � ���� �� �� � ��.
SELECT ROW_NUMBER() OVER (PARTITION BY tblOutletRoutes.OL_id, tblRoutes.Merch_ID, (SUBSTRING(tblRoutes.RouteName, 1, 1) % 8) ORDER BY (SELECT 1)) ROUTES_CNT
		, tblOutletRoutes.OL_id
		, tblRoutes.Merch_ID
		, tblRoutes.RouteName
		, SUBSTRING(tblRoutes.RouteName, 1, 1) % 8 AS RouteDay
INTO #Routes
FROM tblRoutes
INNER JOIN #merchandisersIDs AS IDs ON tblRoutes.Merch_ID = IDs.id 
								OR @merchandisersCnt = 0
INNER JOIN tblOutletRoutes ON tblRoutes.Route_id = tblOutletRoutes.Route_id 
							--AND tblOutletRoutes.Status = 2
WHERE ISNUMERIC(SUBSTRING(tblRoutes.RouteName, 1, 1)) <> 0

-- �������� ��, ������ ����������� �� � ��������� � ������
SELECT r.OL_id 
		, r.Merch_Id
		, matl.ActivityType
		, r.RouteDay
		, ROW_NUMBER() OVER (PARTITION BY r.OL_id ORDER BY (SELECT 1))  RelatedMerch_CNT -- ��������� �� �-�� ��, ������������� ����� �� 
INTO #OL_Merch_Chanal
FROM  #Routes r 
	INNER JOIN tblOutlets ol ON r.OL_id = ol.OL_id
	INNER JOIN 
	( -- ���������  ���� ������� ����� ��
		SELECT  ROW_NUMBER() OVER (PARTITION BY Merch_Id ORDER BY (SELECT 1)) MerchActivity_ID
			 ,  Merch_ID
			 ,  ActivityType
		FROM tblSyncStatusActTypesLinks 
			INNER JOIN #merchandisersIDs AS IDs ON IDs.id = 
						CASE WHEN @merchandisersCnt = 0 THEN -1 ELSE Merch_ID END
	) matl ON matl.MerchActivity_ID = 1  -- �������� ������ ���������� Activity (�����) � ��
		  AND r.Merch_Id = matl.Merch_Id 
WHERE r.ROUTES_CNT = 1

-- Generating a table with dates between @lowerDate and @upperDate
SELECT DATEADD(day, z.num, @lowerDate) AS [Date]
INTO #VisitDates
FROM (
SELECT b10.i + b9.i + b8.i + b7.i + b6.i + b5.i + b4.i + b3.i + b2.i + b1.i + b0.i num
FROM (SELECT 0 i UNION ALL SELECT 1) b0
CROSS JOIN (SELECT 0 i UNION ALL SELECT 2) b1
CROSS JOIN (SELECT 0 i UNION ALL SELECT 4) b2
CROSS JOIN (SELECT 0 i UNION ALL SELECT 8) b3
CROSS JOIN (SELECT 0 i UNION ALL SELECT 16) b4
CROSS JOIN (SELECT 0 i UNION ALL SELECT 32) b5
CROSS JOIN (SELECT 0 i UNION ALL SELECT 64) b6 
CROSS JOIN (SELECT 0 i UNION ALL SELECT 128) b7
CROSS JOIN (SELECT 0 i UNION ALL SELECT 256) b8
CROSS JOIN (SELECT 0 i UNION ALL SELECT 512) b9
CROSS JOIN (SELECT 0 i UNION ALL SELECT 1024) b10
) z
WHERE z.num <= DATEDIFF(day, @lowerDate, @upperDate)


-- ��������� ������� ������� � �������� �� �������� ������ +30 ���� �����.
SELECT SUM(tblSalOutD.Product_Qty) AS SalesInCount
	,SUM(tblSalOutD.Product_Qty * Product_DebtType.Price) AS SalesInMoney
	,tblSalOutH.OL_id
	,olmc.Merch_id
	,tblSalOutH.[Date]
	,Routes.RouteName
	,Product_DebtType.DebtType_id
	,tblProducts.ProductName
INTO #Sales
FROM tblSalOutH
INNER JOIN tblSalOutD ON tblSalOutH.Invoice_id = tblSalOutD.Invoice_id 
INNER JOIN tblProducts ON tblSalOutD.Product_id = tblProducts.Product_id 
INNER JOIN #Product_DebtType Product_DebtType ON Product_DebtType.Product_id = tblProducts.Product_id
INNER JOIN #OL_Merch_Chanal olmc ON olmc.OL_id = tblSalOutH.OL_id AND olmc.Merch_ID = 
(
CASE 
	WHEN (NOT EXISTS( -- ��������� ��-�� ��, ���������� �� ��
					SELECT 1 
					FROM #OL_Merch_Chanal olmc1 
					WHERE olmc1.OL_id = olmc.OL_id 
					  AND olmc1.RelatedMerch_CNT = 2 -- ���������� ��������� �� ��� ������ ��
					)
		  ) 
	THEN -- �� �� ������� ������ ���� ��.
		 -- �� ����������� ��, ���������� �����, ���������� ����� �� ���� ��, ������� ������� �� �� ����� �������
		olmc.Merch_ID 
	ELSE -- �� �� �������� ��������� ��.
		CASE
			WHEN ( -- ��������� ��, �������� �� �� � ��
				  EXISTS(
						SELECT 1 
						FROM #OL_Merch_Chanal olmc2
						WHERE olmc2.OL_id = olmc.OL_id 
							AND olmc2.Merch_Id = tblSalOutH.Merch_Id -- ��, ��������
						)
				 )
			THEN --  
				tblSalOutH.Merch_Id 
			ELSE -- �������� �� �� ������� � ���� ��� ����� � �������� �� �� ���������� ����� ��������. 
				 -- �������� ������ ���������� ����� � ��, ������� �������� �����
				(SELECT TOP 1 olmc3.Merch_ID
				FROM #OL_Merch_Chanal olmc3
				INNER JOIN tblSyncStatusActTypesLinks matl ON olmc3.ActivityType = matl.ActivityType 
														  AND matl.Merch_id = tblSalOutH.Merch_id
														  AND olmc3.OL_id = tblSalOutH.OL_id
				)
		END
END
)
INNER JOIN #Routes AS Routes ON Routes.RouteDay = DATEPART(dw, tblSalOutH.[Date])
			AND Routes.Merch_id = olmc.Merch_id
			AND Routes.OL_id = tblSalOutH.OL_id
			AND Routes.ROUTES_CNT = 1
INNER JOIN #merchandisersIDs AS IDs ON IDs.id = olmc.Merch_ID OR @merchandisersCnt = 0
WHERE tblSalOutH.[Date] BETWEEN DATEADD(day, -30, @lowerDate) AND @upperDate
	  AND tblSalOutH.Status <> 9
	  AND tblProducts.Unit_ID = 3
GROUP BY tblSalOutH.OL_ID
	  , tblSalOutH.[Date]
	  , olmc.Merch_ID
	  , Routes.RouteName
	  , Product_DebtType.DebtType_id
	  ,tblProducts.ProductName

-- ��������� ������� ������� � ������ �� �������� ������.
SELECT tblArchivedDebts.OL_id
	,tblArchivedDebts.DebtDate AS [Date]
	,SUM(CASE WHEN tblProducts.Price = 0 THEN 0 ELSE tblArchivedDebtsDetails.Debt / tblProducts.Price END) AS DebtInCount
	,SUM(tblArchivedDebtsDetails.Debt) AS DebtInMoney
	,tblDebtTypes.DebtType_id
INTO #Debts
FROM tblArchivedDebts
INNER JOIN tblArchivedDebtsDetails ON tblArchivedDebts.Archive_id = tblArchivedDebtsDetails.Archive_id
INNER JOIN tblDebtTypes ON tblDebtTypes.DebtType_id = tblArchivedDebtsDetails.DebtType_id
INNER JOIN #kegDebts kegs ON kegs.ExternalCode = tblDebtTypes.ExternalCode
INNER JOIN tblProducts ON kegs.Product_id = tblProducts.Product_id
WHERE tblArchivedDebts.DebtDate BETWEEN @lowerDate AND @upperDate
	  AND tblArchivedDebtsDetails.Debt IS NOT NULL
	  AND tblArchivedDebtsDetails.Debt <> 0
GROUP BY tblArchivedDebts.OL_id
	,tblArchivedDebts.DebtDate
	,tblDebtTypes.DebtType_id

-- �������������� �������
SELECT
	  	 VisitDates.[Date]
		,Sales.RouteName
		,Sales.SalesInCount
		,Sales.SalesInMoney
		,Debts.DebtInCount
		,Debts.DebtInMoney
		,StockturnSales.SaleAmount
		,tblOutlets.OL_id
		,tblOutlets.OL_Code
		,tblOutletSubTypes.OLSubTypeName
		,tblOutletTypes.OLtype_name
		,tblOutlets.OLName
		,tblOutlets.OLDeliveryAddress
		,tblOutlets.OLTradingName
		,tblOutlets.OLAddress
		,ols.LValue as OutletsSatus
		,tblSyncStatus.MerchName
		,tblSupervisors.Supervisor_name
		,tblDebtTypes.DebtType_Name
FROM #VisitDates AS VisitDates
INNER JOIN #Debts AS Debts ON Debts.[Date] = VisitDates.[Date]
		AND Debts.DebtInCount <> 0
INNER JOIN
(
	SELECT SUM(Sales.SalesInCount) AS SaleAmount
		, VisitDates.[Date]
		, Sales.OL_id
		, Sales.Merch_id
		, Sales.DebtType_id
	FROM #VisitDates VisitDates
	INNER JOIN #Sales AS Sales ON Sales.[Date] BETWEEN DATEADD(day, -30, VisitDates.[Date]) AND VisitDates.[Date]
	GROUP BY VisitDates.[Date]
		, Sales.OL_id
		, Sales.Merch_id
		, Sales.DebtType_id
) AS StockturnSales ON StockturnSales.[Date] = VisitDates.[Date]
	AND StockturnSales.DebtType_id = Debts.DebtType_id
	AND StockturnSales.OL_id = Debts.OL_id

LEFT JOIN #Sales AS Sales ON Sales.[Date] = VisitDates.[Date]
		AND Sales.OL_id = StockturnSales.OL_id
		AND Sales.Merch_id = StockturnSales.Merch_ID
		AND Sales.DebtType_id = Debts.DebtType_id


INNER JOIN tblOutlets ON tblOutlets.OL_id = StockturnSales.OL_id
INNER JOIN tblSyncStatus ON tblSyncStatus.Merch_id = StockturnSales.Merch_ID
INNER JOIN tblSupervisors ON tblSyncStatus.Supervisor_ID = tblSupervisors.Supervisor_id 
		--AND tblSupervisors.Status <> 9 -- Added by Huch R
INNER JOIN tblOutletSubTypes ON tblOutletSubTypes.OLSubType_id = tblOutlets.OLSubType_id
INNER JOIN tblOutletTypes ON tblOutletTypes.OLType_ID = tblOutletSubTypes.OLType_ID

INNER JOIN tblDebtTypes ON tblDebtTypes.DebtType_id = Debts.DebtType_id

LEFT JOIN tblProximityFactors pFactor ON (pFactor.Proximity_id = tblOutlets.Proximity)
--								CASE WHEN tblOutlets.Proximity IS NULL 
--								THEN -3 
--								ELSE tblOutlets.Proximity
--								END)
--				AND pFactor.TableName = 'tblOutlets' 
--				AND pFactor.FieldName = 'Proximity'
INNER JOIN tblGloballookup ols ON ols.LKey = tblOutlets.Status 
				AND ols.TableName = 'tblOutlets'
				AND ols.FieldName = 'Status'
WHERE StockturnSales.SaleAmount <> 0
ORDER BY VisitDates.[Date]

DROP TABLE #OL_Merch_Chanal
DROP TABLE #kegDebts
DROP TABLE #merchandisersIDs
DROP TABLE #Sales
DROP TABLE #Debts
DROP TABLE #VisitDates
DROP TABLE #Routes
DROP TABLE #Product_DebtType
";
            }
        }

        public static string TurnoverByMoney
        {
            get
            {
                return
                    @"
SET DATEFIRST 1

--DECLARE @lowerDate as datetime
--DECLARE @upperDate as datetime

--set @lowerDate = '2011-01-10 00:00:00:000'
--set @upperDate = '2011-01-11 23:59:59:000'


--DECLARE @supervisorId AS INTEGER
--DECLARE @merchandisersIDs AS NVARCHAR(4000)
--DECLARE @merchandisersCnt AS INTEGER

--set @merchandisersIDs = '   6200013'
--set @merchandisersCnt = 1
--set @supervisorId = NULL

/**All dates in SV Period*/
DECLARE @PeriodDateSV TABLE (DateSV SMALLDATETIME)
DECLARE @d1 AS SMALLDATETIME SET @d1 = @lowerDate
WHILE @d1 <> CAST(@upperDate AS SMALLDATETIME)
BEGIN
		INSERT INTO @PeriodDateSV VALUES(@d1)
		SET @d1 = @d1+1
END

declare @vat decimal(5,2)

select @vat = cc.vat
  from tblCustomers c			
       INNER JOIN dbo.tblCities ci
         ON c.City_id = ci.City_id   
	   INNER JOIN dbo.tblDistricts d
	     ON ci.District_id = d.District_id   
	   INNER JOIN dbo.tblRegions r
	     ON d.Region_id = r.Region_id
	   INNER JOIN dbo.tblCountry cc
	     ON cc.Country_id = r.Country_id

/**Debts period
*  For Debts Functionality clause #1 and #2  	
* */
DECLARE @PeriodDateDeb TABLE (DateDeb SMALLDATETIME)
INSERT INTO @PeriodDateDeb
SELECT DISTINCT adeb.DebtDate
  FROM tblArchivedDebts adeb	
	   INNER JOIN tblArchivedDebtsDetails adebd ON adeb.Archive_id = adebd.Archive_id
	   INNER JOIN tblDebtTypes dt ON dt.DebtType_id = adebd.DebtType_id					-- Table with ALL posible types of debts
	   INNER JOIN debt.DebtType_List dtl ON dtl.DebtType_ID = dt.DebtType_id			-- Table with Types of Debts only for SV Debts reports (See requirements) 
 WHERE adeb.DebtDate BETWEEN @lowerDate AND @upperDate
	   
/**Sales by Money by period between lowerDate -31 and upperDate -2*/
SELECT vwi.Date
	 , vwi.Ol_ID
--	 , vwi.Merch_id
	 , SUM(sod.Product_Qty * sod.Price) AS SumH  
  INTO #SalesHistory	 
  FROM tblSalOutH vwi --DW_Visits_With_Invoices vwi  
	   INNER JOIN tblSalOutD sod			ON sod.Invoice_id = vwi.Invoice_id
	   INNER JOIN tblProducts p				ON p.Product_Id = sod.Product_id	   
	   INNER JOIN debt.ProductCn_List dpl   ON dpl.ProductCn_id = p.ProductCn_Id 
	   INNER JOIN tblProductTypes pt		ON pt.ProductType_id = p.ProductType_Id
	  -- INNER JOIN #merchandisersIDs ml		ON ml.id = vwi.Merch_id 
 WHERE vwi.Date BETWEEN (@lowerDate -31) AND (@upperDate -2)
   AND vwi.[Status] <> 9
 GROUP BY vwi.Date
		, vwi.Ol_ID
--		, vwi.Merch_id

;WITH cte AS 
(
	SELECT t.DateSV FROM @PeriodDateSV t
)
SELECT cte.DateSV
	 , sh.OL_ID
--	 , sh.Merch_ID
	 , SUM (sh.SumH) AS SumH --Agregate for SV Date period
--	 , sh.*
  INTO #OborotH
  FROM cte 
	   CROSS JOIN #SalesHistory sh 
WHERE sh.Date BETWEEN (cte.DateSV -31) AND (cte.DateSV -2)
GROUP BY cte.DateSV
	   , sh.OL_ID
--	   , sh.Merch_ID	  


DECLARE @PerOfSalesWithOutOverHead DECIMAL(5,2)			--Fixed Percent for TaraSold
SELECT @PerOfSalesWithOutOverHead = CONVERT(DECIMAL(5,2),dp.ParamValue)
  FROM DW_Params dp WHERE dp.ParamName = 'PerOfSalesWithOutOverHead'
		
/**Debetorka for Outlets with invoices*/
SELECT DebInvoices.OL_ID
	 , DebInvoices.DebtDate
	 , SUM(DebInvoices.DebSum) AS DebSum
	 , SUM(DebInvoices.TareSold) AS TareSold
  INTO #Deb	 
  FROM (
		/**Tara sold for everyone invoice_NO*/
		SELECT soh.Invoice_No
		--	 , sod.Invoice_id
			 , adeb.DebtDate
			 , adebd.OL_ID
			 --, soh.OL_ID
			 , adebd.Debt AS DebVATSum													--With VAT%
			 , adebd.Debt / (@VAT/100+1) AS DebSum										--Without VAT%
			 , SUM(CASE WHEN (p.Tare_Id IS NOT NULL AND p.isTareSubstructable = 1)
						THEN sod.Product_Qty * ISNULL(t.Price,0) 
						ELSE 0
					END 
				  ) AS TareSold     		
		  FROM tblSalOutH soh 
			   INNER JOIN tblSalOutD sod				ON sod.Invoice_id = soh.Invoice_id
			   INNER JOIN tblProducts p					ON p.Product_Id = sod.Product_id
			   LEFT JOIN  tblProducts t					ON t.Product_Id = p.Tare_Id
			   INNER JOIN tblArchivedDebtsDetails adebd ON adebd.Invoice_No = soh.Invoice_No
			   INNER JOIN tblArchivedDebts adeb			ON adeb.Archive_id = adebd.Archive_id
			   INNER JOIN tblDebtTypes dt				ON dt.DebtType_id = adebd.DebtType_id		-- Table with ALL posible types of debts
			   INNER JOIN debt.DebtType_List  dtl		ON dtl.DebtType_id = dt.DebtType_id			-- Table with Types of Debts only for SV Debts reports (See requirements)			 
		 WHERE adeb.DebtDate BETWEEN @lowerDate AND @upperDate
		 --AND soh.OL_ID = 1006201169
		 GROUP BY soh.Invoice_No
			--	, sod.Invoice_id
				, adeb.DebtDate 
				, adebd.OL_ID
				--, soh.OL_ID
				, adebd.Debt
		 UNION ALL
		/**Debetorka for Outlets that doesn''t have Invoice_No parent records or have a mistake in Invoice_No (see requirements)*/ 
		SELECT DISTINCT 
			   adebd.Invoice_No
			 , adeb.DebtDate 
			 , adebd.OL_id
			 , adebd.Debt AS DebVATSum
			 , adebd.Debt / (@VAT/100+1) AS DebSum										--Without VAT%
			 , adebd.Debt / (@VAT/100+1) * @PerOfSalesWithOutOverHead / 100 AS TareSold
		  FROM tblArchivedDebtsDetails adebd  
			   INNER JOIN tblArchivedDebts adeb			ON adeb.Archive_id = adebd.Archive_id
			   INNER JOIN tblDebtTypes dt				ON dt.DebtType_id = adebd.DebtType_id		-- Table with ALL posible types of debts
			   INNER JOIN debt.DebtType_List  dtl		ON dtl.DebtType_id = dt.DebtType_id	
		WHERE adeb.DebtDate BETWEEN @lowerDate AND @upperDate  
		  AND NOT EXISTS (SELECT 1 
							FROM tblSalOutH soh INNER JOIN tblSalOutD sod ON sod.Invoice_id = soh.Invoice_id 
						   WHERE adebd.Invoice_No = soh.Invoice_No
						  )				
		) DebInvoices
 GROUP BY DebInvoices.OL_ID 
		, DebInvoices.DebtDate		

/**Main dataset*/	   	
SELECT DISTINCT
	   o.OLName
         , c.Cust_NAME
	 , o.OLDeliveryAddress
	 , o.OL_id 
	 , olg.OLGroup_name ChanelType	
	-- , m.MerchName
	 , psv.DateSV
	 , pdeb.DateDeb
	 , CASE WHEN pdeb.DateDeb IS NULL
			THEN NULL
			ELSE ISNULL(oh.SumH,0)
		END AS SalOutUAH
	 , CASE WHEN pdeb.DateDeb IS NULL
			THEN NULL
			ELSE ISNULL(deb.DebSum - deb.TareSold,0)
		END	AS DZ
	 , CASE WHEN pdeb.DateDeb IS NULL
			THEN NULL
			ELSE CASE WHEN ISNULL(oh.SumH,0) = 0
					  THEN 0
					  ELSE ISNULL((deb.DebSum - deb.TareSold),0) / oh.SumH * 30
				  END	 
		END AS TurnOverUAH	 
  FROM tblOutlets o	
	   INNER JOIN tblcustomers c                      ON o.cust_id = c.cust_id	  
	   INNER JOIN tblOutletSubTypes ost		  ON ost.OLSubType_id = o.OLSubType_id	
	   INNER JOIN tblOutLetTypes ot			  ON ot.OLtype_id = ost.OLType_ID
	   INNER JOIN tblOutLetGroups olg		  ON olg.OLGroup_id = ot.OLGroup_id
--	   INNER JOIN DW_Visits_With_Invoices vwi ON vwi.Ol_ID = o.OL_id
--	   INNER JOIN tblMerchandisers m		  ON m.Merch_id = vwi.Merch_Id_Assigned
	   CROSS JOIN @PeriodDateSV psv
	   LEFT JOIN @PeriodDateDeb pdeb		  ON pdeb.DateDeb = psv.DateSV	
	   LEFT JOIN #OborotH oh				  ON oh.DateSV = psv.DateSV 
											  AND oh.OL_ID = o.OL_id
											  --AND oh.Merch_Id = m.Merch_id 		
	   LEFT JOIN #Deb deb					  ON deb.OL_ID = o.OL_id 
											  AND deb.DebtDate = pdeb.DateDeb	 										  	
--WHERE o.[Status] <> 9	   

DROP TABLE #SalesHistory
DROP TABLE #OborotH
DROP TABLE #Deb

";
            }
        }

        public static string TurnoverByTare
        {
            get { return "debt.sp_turnover"; }
        }

        public static string VisitCommentsAnalysis
        {
            get
            {
                return
                    @"
-- =============================================
-- Author:		Semyon Krotkih
-- Create date: 30-11-2009
-- Description:	Selects all needed data for 'Visit comments analysis' report.
-- Input parameters:
--		Data range for the report:
--			@lowerDate as datetime - 
--			@upperDate as datetime

--		Supervisor filter ID:
--			@supervisorId as INTEGER
--		Merchandisers ID list:
--			@merchandisersIDs as nvarchar
--		Count of mrecandisers should be returned
--			@merchandisersCnt as INTEGER

--		It is a list of IDs. The list is a fixed width (10 characters) 
--		string like '         2,     12345,1234567898'
--		the string IDs into a table representation
-- 
-- =============================================

SET DATEFIRST 1
/**/
--DECLARE @lowerDate as datetime
--DECLARE @upperDate as datetime
--set @lowerDate = '2009-08-01'
--set @upperDate = '2009-10-30'
--
--
--DECLARE @supervisorId AS INTEGER
--DECLARE @merchandisersIDs AS NVARCHAR(4000)
--DECLARE @merchandisersCnt AS INTEGER
--
--set @merchandisersIDs = '  14500001,  14500002,  14500003,  14500004'
--set @merchandisersCnt = 0
--set @supervisorId = NULL


-- Define Merchandisers IDs list
CREATE TABLE #merchandisersIDs (id INT NOT NULL)
BEGIN
	DECLARE @s BIGINT, @e INT, @id INT
	DECLARE @nv NVARCHAR(10), @cnt INT
	DECLARE @string NVARCHAR(4000)
    DECLARE @merch_type INT
	
	If (@merchandisersCnt <> 0 )
		BEGIN
			SET @string = @merchandisersIDs
			SET @s = 1
			SET @cnt = @merchandisersCnt
			WHILE @cnt > 0
			BEGIN
				SET @nv = SUBSTRING(@string, @s, 10)
				SET @s = @s + 11
				SET @id  = cast(@nv as int )
				SELECT @merch_type = t.UserType_id 
                FROM dbo.tblMerchandisers  m 
                     INNER JOIN tblMerchandiserUserTypes t ON t.merch_id = m.merch_id AND m.Merch_id = @id and t.status = 2
				IF @merch_type IN (SELECT UserType FROM DW_FSM_UserTypes dfut WHERE GETDATE() BETWEEN dfut.StartDate AND dfut.EndDate)  -- Added by Huch R 
                BEGIN                
                    INSERT #merchandisersIDs VALUES (@id) 
                END
				SET @cnt = @cnt -1
			END
		END
	ELSE	
		BEGIN 
			INSERT #merchandisersIDs VALUES (-1) 
		END

END


-- �������� ��, ������ ����������� �� � ��������� � ������
SELECT ROW_NUMBER() OVER (PARTITION BY OL_id ORDER BY (SELECT 1))  RelatedMerch_CNT -- ��������� �� �-�� ��, ������������� ����� �� 
    , OL_id
    , Merch_Id
    , ActivityType  
INTO #OL_Merch_Chanal
FROM
(
	SELECT ol.OL_id 
		, MAX(r.Merch_Id) Merch_Id -- �������� �� � ������� ID
		, ActivityType
	FROM  dbo.tblRoutes r 
		INNER JOIN dbo.tblOutletRoutes olr ON r.Route_id=olr.Route_id 
		INNER JOIN dbo.tblOutlets ol ON olr.OL_id=ol.OL_id
		INNER JOIN 
		( -- ���������  ���� ������� ����� ��
			SELECT  ROW_NUMBER() OVER (PARTITION BY Merch_Id ORDER BY (SELECT 1)) MerchActivity_ID
				 ,  Merch_ID
				 , ActivityType
			FROM tblSyncStatusActTypesLinks
			INNER JOIN #merchandisersIDs AS IDs ON IDs.id = 
						CASE WHEN @merchandisersCnt = 0 THEN -1 ELSE Merch_ID END

		) matl ON matl.MerchActivity_ID = 1 -- �������� ������ ���������� Activity (�����) � ��
			  AND r.Merch_Id = matl.Merch_Id  
	WHERE
		r.Status = 2
		AND olr.Status = 2
	GROUP BY ol.OL_id, ActivityType
			 
) AS OL_Merch_Chanal 


-- �������������� �������
SELECT tblSupervisors.Supervisor_name
	 , tblSyncStatus.MerchName
	 , tblOutletTypes.OLtype_name
	 , tblOutletSubTypes.OLSubTypeName
	 , tblOutlets.OL_id
	 , tblOutlets.OLTradingName
	 , tblOutlets.OLAddress
	 , tblOutlets.OLName 
	 , tblOutlets.OLDeliveryAddress
	 , mers.LValue AS MerchStatus
	 , ols.LValue AS OutletsSatus
	 , och.OLCardDate
	 , tblInaccessibilityReasons.Reason
	 , och.Comments AS NotVisitedComments
	 , tblOutletOrderH.Comments AS CommentsByVisit
	 
FROM tblOutletCardH och
INNER JOIN #OL_Merch_Chanal olmc ON olmc.OL_id = och.OL_id
		AND olmc.Merch_ID = 
(
CASE 
	WHEN (NOT EXISTS( -- ��������� ��-�� ��, ���������� �� ��
					SELECT 1 
					FROM #OL_Merch_Chanal olmc1 
					WHERE olmc1.OL_id = olmc.OL_id 
					  AND olmc1.RelatedMerch_CNT = 2 -- ���������� ��������� �� ��� ������ ��
					)
		  ) 
	THEN -- �� �� ������� ������ ���� ��.
		 -- �� ����������� ��, ���������� �����, ���������� ����� �� ���� ��, ������� ������� �� �� ����� �������
		olmc.Merch_ID 
	ELSE -- �� �� �������� ��������� ��.
		CASE
			WHEN ( -- ��������� ��, �������� �� �� � ��
				  EXISTS(
						SELECT 1 
						FROM #OL_Merch_Chanal olmc2
						WHERE olmc2.OL_id = olmc.OL_id 
							AND olmc2.Merch_Id = och.Merch_Id -- ��, ��������
						)
				 )
			THEN --  
				och.Merch_Id 
			ELSE -- �������� �� �� ������� � ���� ��� ����� � �������� �� �� ���������� ����� ��������. 
				 -- �������� ������ ���������� ����� � ��, ������� �������� �����
				(SELECT TOP 1 olmc3.Merch_ID
				FROM #OL_Merch_Chanal olmc3
				INNER JOIN tblSyncStatusActTypesLinks matl ON olmc3.ActivityType = matl.ActivityType 
														  AND matl.Merch_id = och.Merch_id
														  AND olmc3.OL_id = och.OL_id
				)
		END
END
)
INNER JOIN tblOutlets ON tblOutlets.OL_id = och.OL_id
INNER JOIN tblSyncStatus ON tblSyncStatus.Merch_id = olmc.Merch_ID
INNER JOIN tblSupervisors ON tblSyncStatus.Supervisor_ID = tblSupervisors.Supervisor_id AND tblSupervisors.Status <> 9
INNER JOIN tblOutletSubTypes ON tblOutletSubTypes.OLSubType_id = tblOutlets.OLSubType_id
INNER JOIN tblOutletTypes ON tblOutletTypes.OLType_ID = tblOutletSubTypes.OLType_ID
LEFT JOIN tblInaccessibilityReasons ON tblInaccessibilityReasons.Reason_id = och.Reason_id
LEFT JOIN tblOutletOrderH ON tblOutletOrderH.OlCard_id = och.OLCard_id
INNER JOIN tblGloballookup ols ON ols.LKey = tblOutlets.Status 
				AND ols.TableName = 'tblOutlets'
				AND ols.FieldName = 'Status'
INNER JOIN tblGloballookup mers ON mers.LKey = tblSyncStatus.Status 
				AND mers.TableName = 'tblSyncStatus'
				AND mers.FieldName = 'Status'

GROUP BY och.Comments
		, tblOutlets.OL_id
		, tblOutlets.OLAddress
		, tblOutlets.OLTradingName
		, tblOutlets.OLDeliveryAddress
		, tblOutlets.OLName
		, tblSupervisors.Supervisor_name
		, tblSyncStatus.MerchName
		, och.OLCardDate
		, tblOutLetTypes.OLtype_name
		, tblOutletSubTypes.OLSubTypeName
		, tblOutletOrderH.Comments
		, tblInaccessibilityReasons.Reason
		, tblOutlets.Status
		, tblSyncStatus.Status
		, mers.LValue
		, ols.LValue
HAVING och.OLCardDate BETWEEN @lowerDate AND @upperDate

ORDER BY och.OLCardDate

DROP TABLE #OL_Merch_Chanal
DROP TABLE #merchandisersIDs
";
            }
        }

        public static string VisitsAnalysis
        {
            get
            {
                return
                    @"
-- =============================================
-- Author:		Developer 2
-- Create date: 18-11-2009
-- Description:	Selects all needed data for 'Failed visits analysis' report.
-- 
-- Changed
-- Author:		Developer 1
-- Create date: 24-11-2009
-- Description:	Selects all data for 'Coverage analysis' report.
-- =============================================

-- Input parameters
-- ******************* Data range for the report *******************
-- @lowerDate as datetime 
-- @upperDate as datetime
-- *****************************************************************

-- ********************* Supervisor filter ID **********************
-- @supervisorId as INTEGER
-- *****************************************************************

-- ********************* Merchandisers ID list *********************
-- @merchandisersIDs as nvarchar
-- @merchandisersCnt as INTEGER
-- It is a list of IDs. The list is a fixed width (10 characters) 
-- string like '         2,     12345,1234567898'
-- the function dbo.fnDW_Common_SplitText must be used to transform 
-- the string IDs into a table representation
-- @merchandisersCnt - count of mrecandisers should be returned
-- *****************************************************************
/*--------------------------------------------------------------*/
--DECLARE @lowerDate AS DATETIME
--DECLARE @upperDate AS DATETIME
--DECLARE @supervisorId AS INTEGER
--DECLARE @merchandisersIDs AS NVARCHAR(4000)
--DECLARE @merchandisersCnt AS INTEGER

--set @merchandisersIDs = '        33,        35,        36,        37,        40,        41,       128,    600007'
--set @merchandisersCnt = 0
--set @supervisorId = NULL
--set @lowerDate = '2009-10-01'
--set @upperDate = '2009-10-31'
/*--------------------------------------------------------------*/
DECLARE @currentDate AS DATETIME

--Define planed visits by day
SET @currentDate = @lowerDate
set datefirst 1
set dateformat ymd
CREATE TABLE #WeekDays (date DateTime NOT NULL, [weekDay] tinyInt )

WHILE @currentDate <= @upperDate
BEGIN
INSERT INTO #WeekDays (date, [weekDay])
	VALUES (@currentDate, DATEPART(dw, @currentDate))
	SET @currentDate = @currentDate + 1
END 

-- Define Merchandisers IDs list
CREATE TABLE #merchandisersIDs (id INT NOT NULL)
BEGIN
	DECLARE @s BIGINT, @e INT, @id INT
	DECLARE @nv NVARCHAR(10), @cnt INT
	DECLARE @string NVARCHAR(4000)
    DECLARE @merch_type INT
	
	If (@merchandisersCnt <> 0 )
		BEGIN
			SET @string = @merchandisersIDs
			SET @s = 1
			SET @cnt = @merchandisersCnt
			WHILE @cnt > 0
			BEGIN
				SET @nv = SUBSTRING(@string, @s, 10)
				SET @s = @s + 11
				SET @id  = CAST(@nv AS int )
				SELECT @merch_type = t.UserType_id 
                FROM dbo.tblMerchandisers  m 
                     INNER JOIN tblMerchandiserUserTypes t ON t.merch_id = m.merch_id AND m.Merch_id = @id and t.status = 2
				IF @merch_type IN (SELECT UserType FROM DW_FSM_UserTypes dfut WHERE GETDATE() BETWEEN dfut.StartDate AND dfut.EndDate)  -- Added by Huch R
                BEGIN                
                    INSERT #merchandisersIDs VALUES (@id) 
                END
				SET @cnt = @cnt -1
			END
		END
	ELSE	
		BEGIN 
			INSERT #merchandisersIDs VALUES (-1) 
		END
END

SELECT m.MerchName, m.Merch_id, s.Supervisor_name
INTO #merch
FROM 
tblMerchandisers m 
INNER JOIN tblSupervisors s ON s.Supervisor_id=m.Supervisor_ID AND s.Status <> 9 AND m.[Status]<>9
CREATE CLUSTERED INDEX Idx_tmp_merch ON #merch(Merch_ID)

-- ��������� ������� ������� � �������� ���� ������ ���� 
IF OBJECT_ID('#OutletsMerchandiserActivity_Link') IS NULL 
CREATE TABLE #OutletsMerchandiserActivity_Link
(
  RelatedMerch_CNT int,
  OL_id            bigint ,
  Merch_Id         int    ,
  ActivityType     int  
 -- CONSTRAINT PK_OutletsMerchandiserActivity_Link1 primary key (OL_id, Merch_Id)
)

INSERT INTO #OutletsMerchandiserActivity_Link
SELECT RelatedMerch_CNT
    ,  OL_id
    ,  Merch_Id
    ,  ActivityType 
FROM vwDW_OutletsMerchandiserActivity_Link

-- Define schedule for merchandisers
--SELECT olr.OL_id
--	 , r.Merch_Id 
--	 , wd.Date AS OLCardDate
--INTO #Schedule
--FROM #WeekDays wd
--    INNER JOIN dbo.tblRoutes r ON ISNUMERIC(SUBSTRING(r.RouteName, 1, 1)) <> 0 
--                                --AND wd.[weekDay] = SUBSTRING(r.RouteName, 1, 1) 
--                                AND (wd.[weekDay] = SUBSTRING(r.RouteName, 1, 1) or SUBSTRING(r.RouteName, 1, 1) in ('8','9'))
--                                AND r.Status = 2
--    INNER JOIN dbo.tblOutletRoutes olr ON r.Route_id = olr.Route_id 
--                                        AND olr.Status = 2
--    INNER JOIN vwDW_OutletsMerchandiserActivity_Link olmc ON olmc.Merch_Id = r.Merch_Id 
--                                                            AND olmc.OL_Id = olr.OL_Id
--    INNER JOIN #merchandisersIDs AS IDs ON IDs.id = CASE 
--                                                        WHEN @merchandisersCnt = 0 THEN -1 
--                                                        ELSE r.Merch_id 
--                                                    END
SELECT olr.OL_id
	 , r.Merch_Id 
	 , wd.Date AS OLCardDate
INTO #Schedule
FROM #WeekDays wd
    INNER JOIN dbo.tblRoutes r ON ISNUMERIC(SUBSTRING(r.RouteName, 1, 1)) <> 0 
                                AND (wd.[weekDay] = SUBSTRING(r.RouteName, 1, 1) or (wd.[weekDay] = case when SUBSTRING(r.RouteName, 1, 1) in ('8','9') then 7 else null end) )
                                AND r.Status = 2
    INNER JOIN dbo.tblOutletRoutes olr ON r.Route_id = olr.Route_id 
                                        AND olr.Status = 2
    INNER JOIN vwDW_OutletsMerchandiserActivity_Link olmc ON olmc.Merch_Id = r.Merch_Id 
                                                            AND olmc.OL_Id = olr.OL_Id
    INNER JOIN tbloutlets o ON olr.ol_id = o.ol_id AND o.status <> 9
    INNER JOIN #merchandisersIDs AS IDs ON IDs.id = CASE 
                                                        WHEN 1 = 0 THEN -1 
                                                        ELSE r.Merch_id 
                                                    END

CREATE INDEX IE_#Schedule ON #Schedule(OL_Id,OLCardDate, Merch_id)

--define products on scheduled OL
SELECT DISTINCT OL_Id
     , Merch_id 
     , OLCardDate
     , Product_Id
     , ProductName
     , BrandName as ProdGroupName
     , ProdTypeName
     , Unit_Name -- ��� ��������
     , Packing_Capacity -- ������� ��������
     , Packing_Type -- ��� ��������
     , ProductCnName
INTO #ScheduledProducts
FROM #Schedule
CROSS JOIN vwDW_Products



CREATE UNIQUE CLUSTERED INDEX [PK_#ScheduledProducts]  ON #ScheduledProducts([OL_Id],[Merch_id],[Product_Id],[OLCardDate])
CREATE  INDEX [IX_#ScheduledProducts_OL_Id_OLCardDate]  ON #ScheduledProducts([OL_Id],[OLCardDate])

-- Calculate
SELECT olmc.Merch_Id 
     , och.OLCardDate
     , och.OL_Id -- Visited OL
     , CASE 
            WHEN sod.Product_Id IS NULL THEN NULL
	        ELSE och.OLCard_Id--/och.OL_Id
       END AS OL_Visited_IsEffective -- The visit is effective
     , ood.Product_Id AS Product_Id
     , och.OLCard_Id
INTO #tmpOchAll
FROM (
		SELECT Merch_Id
		     , OLCardDate
		     , OL_Id 
		     , OLCard_Id AS OLCard_Id
		FROM      		
		(SELECT sh.Merch_Id
		     , CONVERT(datetime,CONVERT(VARCHAR(10),h.OLCardDate,104),104) as OLCardDate
		     , h.OL_Id
		     , h.OLCard_Id as OLCard_Id
		FROM tblOutletCardH  h
			INNER JOIN #Schedule sh ON sh.ol_id = h.ol_id --and sh.merch_id = h.merch_id
			LEFT JOIN tblOutletOrderH o ON h.OLCard_id = o.OlCard_id
			LEFT JOIN dbo.tblSalOutH s ON o.OrderNo = s.OrderNo AND h.OL_id = s.OL_ID 			 	    
		WHERE convert(datetime,CONVERT(VARCHAR(10),h.OLCardDate,104),104) BETWEEN @lowerDate AND @upperDate
		--AND s.OrderNo IS NOT NULL	 		         
		GROUP BY sh.Merch_Id, h.OLCardDate, h.OL_Id, h.OLCard_Id ) as t
		GROUP BY t.Merch_id
		       , t.OLCardDate
		       , t.OL_id  
		       , t.OLCard_Id       
	)
AS och
    INNER JOIN #OutletsMerchandiserActivity_Link olmc ON olmc.OL_ID = och.OL_ID
									    AND olmc.Merch_ID = 
		    (
			    CASE 
				    WHEN (NOT EXISTS( -- ��������� ��-�� ��, ���������� �� ��
								    SELECT 1 
								    FROM #OutletsMerchandiserActivity_Link olmc1 
								    WHERE olmc1.OL_id = olmc.OL_id 
								      AND olmc1.RelatedMerch_CNT = 2 -- ���������� ��������� �� ��� ������ ��
								    )
					      ) 
				    THEN -- �� �� ������� ������ ���� ��.
					     -- �� ����������� ��, ���������� �����, ���������� ����� �� ���� ��, ������� ������� �� �� ����� �������
					    olmc.Merch_ID 
				    ELSE -- �� �� �������� ��������� ��.
					    CASE
						    WHEN ( -- ��������� ��, �������� �� �� � ��
							      EXISTS(
									    SELECT 1 
									    FROM #OutletsMerchandiserActivity_Link olmc2
									    WHERE olmc2.OL_id = olmc.OL_id 
										    AND olmc2.Merch_Id = och.Merch_ID  -- ��, ��������
									    )
							     )
						    THEN --  
							    och.Merch_Id 
						    ELSE -- �������� �� �� '�������' � ���� ��� ����� � �������� �� �� '���������� ����� ��������'. 
							     -- �������� ������ ���������� ����� � ��, ������� �������� �����
							    (SELECT TOP 1 olmc3.Merch_ID
							    FROM #OutletsMerchandiserActivity_Link olmc3
							    INNER JOIN tblSyncStatusActTypesLinks matl ON olmc3.ActivityType = matl.ActivityType 
																	      AND matl.Merch_id = och.Merch_ID
																	      AND olmc3.OL_id = och.OL_ID
							    )
					    END
			    END
		    )
    --INNER JOIN #merchandisersIDs AS IDs ON IDs.id = 
			    --CASE WHEN @merchandisersCnt = 0 THEN -1 ELSE olmc.Merch_id END
    LEFT JOIN tblOutletOrderH ooh ON och.OLCard_id = ooh.OLCard_id -- Change inner join to left join
    LEFT JOIN tblOutletOrderD ood ON ood.OrderNo = ooh.OrderNo 
    LEFT JOIN tblSalOutH soh ON soh.OrderNo = ooh.OrderNo AND soh.Status <> 9 and och.ol_id = soh.ol_id -- Added by Huch R and Zernoklov O
    LEFT JOIN tblSalOutD sod ON sod.Invoice_Id = soh.Invoice_Id and
                             (select top 1 productcn_id from tblProducts where product_id = sod.Product_Id) =
                             (select top 1 productcn_id from tblProducts where product_id = ood.Product_Id)						                      
						    AND ood.Product_qty <> 0 
						    AND sod.Product_qty <> 0
						    AND soh.Status <> 9
     
-- ��������� ������� � ��� ������ �������� ����� �� ��������� �����
if OBJECT_ID('#tmpoch') is null 
create table #tmpoch
(
	   Merch_Id    int
     , OLCardDate  datetime
     , OL_Id       bigint
     , OL_Visited_IsEffective  bigint
     , Product_Id int
     , OLCard_Id  bigint 
)

-- �������� ���� ���� ���������� ����. ��� ���� � ��� ���� �� ���� ���������� �����, �� �������� ���� �� ���������� ����						    
insert into #tmpoch
select a.merch_id
     , a.olcarddate
     , a.ol_id
     , a.OL_Visited_IsEffective
     , a.product_id
     , a.olcard_id 
from #tmpOchAll a
inner join 
(select Merch_Id 
     , OLCardDate
     , OL_Id 
    -- , product_id --igetman from rguch
     , MAX(OL_Visited_IsEffective) as OL_Visited_IsEffective     
     , MAX(OLCard_Id) as OLCard_Id
from #tmpOchAll
group by Merch_Id 
       , OLCardDate
       , OL_Id        
      -- , product_id --igetman from rguch
) t ON  a.olcard_id = case 
						  when t.OL_Visited_IsEffective is null
						  then t.OLCard_Id
                 		  else t.OL_Visited_IsEffective
                 	   end 									   


-- �������� ������, ������� �� ���� ���������, ������ �� ������ � ���������� ������
--select COUNT(*) from vwDW_Products
SELECT * 
INTO  #tmpOch1
FROM
(
	SELECT Merch_ID
         , OLCardDate
         , OL_ID
         , OL_Visited_IsEffective
         , p.Product_Id
         , OLCard_Id AS OL_Visited	
         , ProductName -- ���
         , ProdTypeName -- ����
         , BrandName AS ProdGroupName -- �����
         , Unit_Name -- ��� ��������
         , Packing_Capacity -- ������� ��������
         , Packing_Type -- ��� ��������
         , ProductCnName  
	FROM #tmpOch och
    	LEFT JOIN vwDW_Products p ON p.Product_id = och.Product_id
	UNION
	SELECT d.Merch_ID
         , d.OLCardDate
         , d.OL_ID
         , NULL AS OL_Visited_IsEffective
         , sp.Product_Id
         , OLCard_Id AS  OL_Visited	
         , sp.ProductName -- ���
         , sp.ProdTypeName -- ����
         , sp.ProdGroupName -- �����
         , sp.Unit_Name -- ��� ��������
         , sp.Packing_Capacity -- ������� ��������
         , sp.Packing_Type -- ��� ��������
         , sp.ProductCnName  
	FROM 
	(
		SELECT DISTINCT olmc.OL_Id
		              , olmc.Merch_id
		              , OLCardDate 
		FROM tblOutletCardH och
	    	INNER JOIN vwDW_OutletsMerchandiserActivity_Link olmc ON olmc.OL_ID = och.OL_ID
									AND olmc.Merch_ID = 
		(
			CASE 
				WHEN (NOT EXISTS( -- ��������� ��-�� ��, ���������� �� ��
								SELECT 1 
								FROM vwDW_OutletsMerchandiserActivity_Link olmc1 
								WHERE olmc1.OL_id = olmc.OL_id 
								  AND olmc1.RelatedMerch_CNT = 2 -- ���������� ��������� �� ��� ������ ��
								)
					  ) 
				THEN -- �� �� ������� ������ ���� ��.
					 -- �� ����������� ��, ���������� �����, ���������� ����� �� ���� ��, ������� ������� �� �� ����� �������
					olmc.Merch_ID 
				ELSE -- �� �� �������� ��������� ��.
					CASE
						WHEN ( -- ��������� ��, �������� �� �� � ��
							  EXISTS(
									SELECT 1 
									FROM vwDW_OutletsMerchandiserActivity_Link olmc2
									WHERE olmc2.OL_id = olmc.OL_id 
										AND olmc2.Merch_Id = och.Merch_ID  -- ��, ��������
									)
							 )
						THEN --  
							och.Merch_Id 
						ELSE -- �������� �� �� '�������' � ���� ��� ����� � �������� �� �� '���������� ����� ��������'. 
							 -- �������� ������ ���������� ����� � ��, ������� �������� �����
							(SELECT TOP 1 olmc3.Merch_ID
							FROM vwDW_OutletsMerchandiserActivity_Link olmc3
							INNER JOIN tblSyncStatusActTypesLinks matl ON olmc3.ActivityType = matl.ActivityType 
																	  AND matl.Merch_id = och.Merch_ID
																	  AND olmc3.OL_id = och.OL_ID
							)
					END
			END
		)
		INNER JOIN #merchandisersIDs AS IDs ON IDs.id = CASE 
		                                                      WHEN @merchandisersCnt = 0 THEN -1 
		                                                      ELSE olmc.Merch_id 
		                                                 END
		
		--WHERE och.OLCardDate BETWEEN @lowerDate AND @upperDate
        WHERE convert(datetime,CONVERT(VARCHAR(10),och.OLCardDate,104),104) BETWEEN @lowerDate AND @upperDate  
		EXCEPT
		Select DISTINCT OL_Id
		              , Merch_id
		              , OLCardDate 
		FROM #tmpOch
	) AS D 
	    INNER JOIN tblOutletCardH och ON d.OL_Id = och.OL_Id AND d.OLCardDate = convert(datetime,CONVERT(VARCHAR(10),och.OLCardDate,104),104)
	    INNER JOIN #ScheduledProducts sp ON d.OL_Id = sp.OL_Id AND d.OLCardDate = sp.OLCardDate
)AS DD

-- ��������� ��, � ������� �� ���� �������
SELECT * 
INTO #Result
FROM
(
	SELECT Merch_ID
         , OLCardDate
         , OL_ID
         , OL_Visited	
         , OL_Visited_IsEffective
         , Product_Id
         , ProductName -- ���
         , ProdTypeName -- ����
         , ProdGroupName -- �����
         , Unit_Name -- ��� ��������
         , Packing_Capacity -- ������� ��������
         , Packing_Type -- ��� ��������
         , ProductCnName 
	FROM #tmpOch1
	UNION
	SELECT Merch_ID
         , OLCardDate 	
         , OL_ID
         , NULL AS OL_Visited
         , NULL AS OL_Visited_IsEffective
         , p.Product_Id
         , ProductName -- ���
         , ProdTypeName -- ����
         , BrandName AS ProdGroupName -- �����
         , Unit_Name -- ��� ��������
         , Packing_Capacity -- ������� ��������
         , Packing_Type -- ��� ��������
         , ProductCnName 
	FROM 
	(
		SELECT OL_Id
		     , Merch_id
		     , OLCardDate
		     , Product_Id 
		FROM #ScheduledProducts
		EXCEPT
		SELECT DISTINCT OL_Id
		              , Merch_id
		              , OLCardDate
		              , Product_Id
		FROM #tmpOch1
	) AS D 
	    INNER JOIN vwDW_Products p ON D.Product_Id = p.Product_Id
) AS DD

CREATE  INDEX [IX_#Result] ON #Result (
                                       Merch_ID
                                     , OL_ID
                                     , Product_Id
                                     , OLCardDate
                                     , OL_Visited_IsEffective)

/*-----------------------------------------------------------------------------------------------------------------*/
IF OBJECT_ID('tempdb..#tblAreas','U') !=0
    DROP TABLE #tblAreas 
SELECT * 
INTO #tblAreas 
FROM tblAreas

CREATE UNIQUE CLUSTERED INDEX [UCI_#tblAreas_Area_id] ON #tblAreas (Area_id)
CREATE  INDEX [IX_#tblAreas_City_id] ON #tblAreas (City_id)

IF OBJECT_ID('tempdb..#tblOutlets','U') !=0
    DROP TABLE #tblOutlets
SELECT * 
INTO #tblOutlets 
FROM tblOutlets
WHERE status = 2

CREATE UNIQUE CLUSTERED INDEX [UCI_tblOutlets] ON #tblOutlets (OL_id)
CREATE  INDEX [IX_tblOutlets] ON #tblOutlets (Proximity)
UPDATE #tblOutlets
SET Proximity = -3
WHERE Proximity IS NULL

IF OBJECT_ID('tempdb..#tblGlobalLookup','U') !=0
    DROP TABLE #tblGlobalLookup
SELECT * 
INTO #tblGlobalLookup 
FROM tblGlobalLookup
WHERE TableName = 'tblOutlets'  AND FieldName = 'Proximity'
CREATE UNIQUE CLUSTERED INDEX [UCI_#tblGlobalLookup_LKey_TableName_FieldName] ON #tblGlobalLookup (LKey,TableName,FieldName)

IF OBJECT_ID('tempdb..#tblGlobalLookup1','U') !=0
    DROP TABLE #tblGlobalLookup1
SELECT * 
INTO #tblGlobalLookup1 
FROM tblGlobalLookup
WHERE TableName = 'tblOutlets'  AND FieldName = 'Status'
CREATE UNIQUE CLUSTERED INDEX [UCI_#tblGlobalLookup1_LKey_TableName_FieldName] ON #tblGlobalLookup1 (LKey,TableName,FieldName)

IF OBJECT_ID('tempdb..#tblGlobalLookup2','U') !=0
    DROP TABLE #tblGlobalLookup2
SELECT * 
INTO #tblGlobalLookup2 
FROM tblGlobalLookup
WHERE TableName = 'tblMerchandisers'  AND FieldName = 'Status'
CREATE UNIQUE CLUSTERED INDEX [UCI_#tblGlobalLookup2_LKey_TableName_FieldName] ON #tblGlobalLookup2 (LKey,TableName,FieldName)

/*-----------------------------------------------------------------------------------------------------------------*/
SELECT /*sp.Cust_Name -- ����� �������������*/
 	   m.Supervisor_name -- ����������
	/* , c.City_Name	 -- �����*/
	 /*, d.District_Name -- �������*/
	/* , NULL AS Region_Name -- ����� (��) TODO: join to tblCountryArea*/
	/* , a.Area_Name -- ����� � ������ (��) */
	/* , st.Settlement AS Territory_Type -- ��� ���������� TODO: join to  tblSettlement*/
	/* , och.Merch_Id*/
     , m.MerchName -- ��
	/* , ms.LValue AS MerchandiserStatus -- ������ ��*/
	 , convert(datetime,CONVERT(VARCHAR(10),och.OLCardDate,104),104) as OLCardDate -- �����
	 , och.OL_Visited-- Outlet is visited by Merchandiser 
	 , och.OL_Visited_IsEffective AS OL_Visited_IsEffective -- Outlet is Active
	 , Schedule.OL_id  AS OLSchedule-- Outlets on schedule
	 , ol.OL_id -- ��� ��
	 , ol.OL_Code -- ������� �����
	/* , ols.LValue as OutletsSatus -- ������ ��*/
	 , olt.OLtype_name --�����
	 , olst.OLSubTypeName -- ��������
	 , ol.OLName  --���� ���
	 , ol.OLDeliveryAddress -- ����. �����
	 /*, ol.OLTradingName -- ��. ���
	 , ol.OLAddress --��. �����*/
	 /*, och.Product_Id
	 , och.ProductName -- ���*/
	 , och.ProdGroupName -- �����
	 , och.ProdTypeName -- ����
	 , och.Unit_Name -- ��� ��������
	 , och.Packing_Capacity -- ������� ��������
	 , och.Packing_Type -- ��� ��������
	 , och.ProductCnName -- ������������ ��������������� ���������
	 , pFactor.ProximityName AS ProximityFactor -- ������ ��������
	/* , ppl.Population -- ���������*/
FROM #Result AS  och 
    INNER JOIN #merch m ON m.Merch_id = och.Merch_id
--    INNER JOIN tblMerchandisers m ON m.Merch_id = och.Merch_id
   /* INNER JOIN #tblGloballookup2 ms ON ms.LKey = m.Status */
							     /* AND ms.TableName = 'tblMerchandisers' 
							      AND ms.FieldName = 'Status'*/
    INNER JOIN #tblOutlets ol ON och.OL_id = ol.OL_id
    INNER JOIN tblOutletSubTypes olst ON ol.OLSubType_id = olst.OLSubType_id 
    INNER JOIN tblOutLetTypes olt ON olst.OLType_ID = olt.OLType_id 
   /* INNER JOIN #tblGloballookup1 ols ON ols.LKey = ol.Status */
							     /* AND ols.TableName = 'tblOutlets' 
							      AND ols.FieldName = 'Status'*/
--    INNER JOIN tblSupervisors s ON m.Supervisor_ID = s.Supervisor_id AND s.Status <> 9
--						      AND (@supervisorId is NULL OR @supervisorId = m.Supervisor_ID) 
    /*INNER JOIN 
	    (
		    SELECT  ROW_NUMBER() OVER (PARTITION BY Supervisor_id ORDER BY (SELECT 1)) CustomerOrder
			     ,  Supervisor_id
			     ,  Cust_Id
		    FROM tblCustomerSupervisors tcs
		    WHERE (@supervisorId is NULL OR @supervisorId = tcs.Supervisor_ID)  
	    ) AS cs ON cs.Supervisor_id = s.Supervisor_id	
			    AND cs.CustomerOrder = 1	-- �.�. ��� ����������� ����� ������� ��, 
										    -- �������� ������ ���������� �� ������������� ��
    INNER JOIN tblCustomers sp ON cs.Cust_id = sp.Cust_Id */
   /* INNER JOIN #tblAreas a ON ol.Area_id = a.Area_Id
    INNER JOIN tblCities c ON c.City_id = a.City_Id*/
    /*INNER JOIN tblDistricts d ON d.District_id = c.District_id*/
    LEFT JOIN tblProximityFactors pFactor ON pFactor.Proximity_id = ol.Proximity
    LEFT JOIN (SELECT DISTINCT OL_ID, MERCH_ID 
              FROM #Schedule
              WHERE OLCardDate between @lowerDate and @upperDate
              ) AS Schedule ON Schedule.OL_Id = och.OL_Id 
                                     AND Schedule.Merch_Id = och.Merch_Id 
/*    LEFT JOIN #Schedule Schedule ON Schedule.OL_Id = och.OL_Id 
							    AND Schedule.OLCardDate between @lowerDate and @upperDate  
                              --AND Schedule.OLCardDate = och.OLCardDate 
							    AND Schedule.Merch_Id = och.Merch_Id*/
  /*  LEFT JOIN dbo.tblPopulation ppl ON c.Population_id = ppl.Population_id*/
  /*  LEFT JOIN dbo.tblSettlement st ON c.Settlement_id = st.Settlement_id*/
where Schedule.ol_id is not null 
                                 
   
DROP TABLE #tmpOch
DROP TABLE #tmpOch1
DROP TABLE #merchandisersIDs
DROP TABLE #Result
DROP TABLE #WeekDays
DROP TABLE #Schedule
DROP TABLE #tmpOchAll
DROP TABLE #ScheduledProducts
DROP TABLE #merch
DROP TABLE #OutletsMerchandiserActivity_Link
";
            }
        }

        public static string VisitsAnalysisNew
        {
            get
            {
                return
                    @"
-- =============================================
-- Author:		Developer 2
-- Create date: 18-11-2009
-- Description:	Selects all needed data for 'Failed visits analysis' report.
-- 
-- Changed
-- Author:		Developer 1
-- Create date: 24-11-2009
-- Description:	Selects all data for 'Coverage analysis' report.
-- =============================================

-- Input parameters
-- ******************* Data range for the report *******************
-- @lowerDate as datetime 
-- @upperDate as datetime
-- *****************************************************************

-- ********************* Supervisor filter ID **********************
-- @supervisorId as INTEGER
-- *****************************************************************

-- ********************* Merchandisers ID list *********************
-- @merchandisersIDs as nvarchar
-- @merchandisersCnt as INTEGER
-- It is a list of IDs. The list is a fixed width (10 characters) 
-- string like '         2,     12345,1234567898'
-- the function dbo.fnDW_Common_SplitText must be used to transform 
-- the string IDs into a table representation
-- @merchandisersCnt - count of mrecandisers should be returned
-- *****************************************************************


/*----------------------------------------------------------------------*/
--DECLARE @lowerDate AS DATETIME
--DECLARE @upperDate AS DATETIME
--DECLARE @supervisorId AS INTEGER
--DECLARE @merchandisersIDs AS NVARCHAR(4000)
--DECLARE @merchandisersCnt AS INTEGER

--set @merchandisersIDs = '1300001,1300002,1300004,1300005,1300006,1300007,1300008,1300011,1300014,1300016,1300021'
--set @merchandisersCnt = 11
--set @supervisorId = NULL
--set @lowerDate = '20110103'
--set @upperDate = '20110114'
/*----------------------------------------------------------------------*/
 DECLARE @isdebug BIT 
 declare @msg varchar(max)	
 SET @isdebug=0  

   if @isdebug=1
   begin
   set @msg = 'Start: ' +convert(varchar(20),getdate(), 114)
   raiserror (@msg, 0, 1, @msg)  with nowait
   End

SET NOCOUNT ON

DECLARE @currentDate AS DATETIME
set datefirst 1
set dateformat ymd

--Define planed visits by day
SET @currentDate = @lowerDate

CREATE TABLE #WeekDays (date DateTime NOT NULL primary key, [weekDay] tinyInt )

WHILE @currentDate <= @upperDate
BEGIN
INSERT INTO #WeekDays (date, [weekDay])
	VALUES (@currentDate, DATEPART(dw, @currentDate))
	SET @currentDate = @currentDate + 1
END 

-- Define Merchandisers IDs list
CREATE TABLE #merchandisersIDs (id INT NOT NULL primary key)

If (@merchandisersCnt <> 0 )
	BEGIN
		insert into #merchandisersIDs
		select ID from dbo.Parse_IntList(@merchandisersIDs)
		
		delete #merchandisersIDs where id not in 
		(select m.Merch_id
		from dbo.tblMerchandisers  m 
			INNER JOIN tblMerchandiserUserTypes t ON t.merch_id = m.merch_id and t.status = 2 
			INNER JOIN DW_FSM_UserTypes dfut ON t.UserType_id = dfut.UserType
		WHERE GETDATE() BETWEEN dfut.StartDate AND dfut.EndDate)			
	END
ELSE	
	BEGIN 
		INSERT #merchandisersIDs VALUES (-1) 
	END

 if @isdebug=1
   begin
   set @msg = '#vwDW_Products: ' +convert(varchar(20),getdate(), 114)
   raiserror (@msg, 0, 1, @msg)  with nowait
   End

--rkm, make short tbl
SELECT m.MerchName, m.Merch_id, s.Supervisor_name
INTO #merch
FROM 
tblMerchandisers m 
INNER JOIN tblSupervisors s ON s.Supervisor_id=m.Supervisor_ID AND s.Status <> 9 AND m.[Status]<>9
CREATE CLUSTERED INDEX Idx_tmp_merch ON #merch(Merch_ID)


SELECT Product_Id --, ProductCn_Id 
     , ProductName      
     , BrandName
     , ProdTypeName     
     , Unit_Name        
     , Packing_Capacity 
     , Packing_Type     
     , ProductCnName    
INTO   #vwDW_Products
FROM   vwDW_Products

CREATE CLUSTERED INDEX Idx_vwDW_Products ON #vwDW_Products(product_id)


 if @isdebug=1
   begin
   set @msg = 'vwDW_Products: ' +convert(varchar(20),getdate(), 114)
   raiserror (@msg, 0, 1, @msg)  with nowait
   END
   
DECLARE @maxOlCard_ID BIGINT
DECLARE @pDateBegin AS DATETIME  
DECLARE @vCurrentDate AS DATETIME	

DECLARE @ttWeekDays TABLE(date DateTime NOT NULL, [weekDay] tinyInt )
--���������� ����, � ������� ��� ���������� ��������� ������ ������ �� Route � OutletRoute 
SELECT @pDateBegin = MAX(mor.StartDate)+1 FROM dbo.DW_MerchOutletRoutes mor
SET @vCurrentDate = @pDateBegin
WHILE @vCurrentDate <= DATEADD(ms,-3,CONVERT(datetime,CONVERT(VARCHAR(10),GETDATE(),104),104)+1)	--����� ������������ ���
BEGIN
	INSERT INTO @ttWeekDays (date, [weekDay])
		VALUES (@vCurrentDate, datePart(dw, @vCurrentDate))
		SET @vCurrentDate = @vCurrentDate + 1
END
   

-- ��������� ������� ������� � �������� ���� ������ ���� 
IF OBJECT_ID('#OutletsMerchandiserActivity_Link') IS NULL 
CREATE TABLE #OutletsMerchandiserActivity_Link
(
  RelatedMerch_CNT int,
  OL_id            bigint ,
  Merch_Id         int    ,
  ActivityType     int  
 -- CONSTRAINT PK_OutletsMerchandiserActivity_Link1 primary key (OL_id, Merch_Id)
)


INSERT INTO #OutletsMerchandiserActivity_Link
SELECT RelatedMerch_CNT
    ,  OL_id
    ,  Merch_Id
    ,  ActivityType 
FROM vwDW_OutletsMerchandiserActivity_Link    

SELECT olr.OL_id
	 , r.Merch_Id 
	 , wd.Date AS OLCardDate
INTO #Schedule 
FROM #WeekDays wd
    INNER JOIN dbo.tblRoutes r ON ISNUMERIC(SUBSTRING(r.RouteName, 1, 1)) <> 0 
                              AND (wd.[weekDay] = SUBSTRING(r.RouteName, 1, 1) OR (wd.[weekDay] = CASE WHEN SUBSTRING(r.RouteName, 1, 1) in ('8','9') THEN 7 ELSE NULL END))
                              AND r.Status = 2
    INNER JOIN dbo.tblOutletRoutes olr ON r.Route_id = olr.Route_id 
                                        AND olr.Status = 2
    INNER JOIN #OutletsMerchandiserActivity_Link olmc ON olmc.Merch_Id = r.Merch_Id 
                                                            AND olmc.OL_Id = olr.OL_Id
    INNER JOIN tbloutlets o ON olr.ol_id = o.ol_id AND o.status <> 9
    INNER JOIN #merchandisersIDs AS IDs ON IDs.id = CASE 
                                                        WHEN @merchandisersCnt = 0 
                                                        THEN -1 
                                                        ELSE r.Merch_id 
                                                    END

 CREATE INDEX IE_#Schedule ON #Schedule(OL_Id, Merch_id)
 if @isdebug=1
   begin
   set @msg = '#Schedule: ' +convert(varchar(20),getdate(), 114)
   raiserror (@msg, 0, 1, @msg)  with nowait
   End

SELECT olr.OL_id
	 , r.Merch_Id 
	 , wd.Date AS OLCardDate
INTO #ScheduleTotal
FROM #WeekDays wd
    INNER JOIN dbo.tblRoutes r ON ISNUMERIC(SUBSTRING(r.RouteName, 1, 1)) <> 0 AND r.Status = 2
    INNER JOIN dbo.tblOutletRoutes olr ON r.Route_id = olr.Route_id AND olr.Status = 2
    INNER JOIN #OutletsMerchandiserActivity_Link olmc ON olmc.Merch_Id = r.Merch_Id AND olmc.OL_Id = olr.OL_Id
    INNER JOIN tbloutlets o ON olr.ol_id = o.ol_id AND o.status <> 9
    INNER JOIN #merchandisersIDs AS IDs ON IDs.id = CASE 
                                                        WHEN @merchandisersCnt = 0 
                                                        THEN -1 
                                                        ELSE r.Merch_id 
                                                    END


-- ��������� ������� � ��������� �������
if OBJECT_ID('#PlanedVisitOnRoute') is null 
create table #PlanedVisitOnRoute 
(
	VisitID    bigint primary key identity(1,1)
  --,	Route_id   bigint 
  , Merch_id   int
  , OL_id      bigint
  , StartDate  datetime 
)

INSERT INTO #PlanedVisitOnRoute
SELECT distinct --mr.Route_id
       mr.Merch_id --o.owner_id 
     , mr.OL_id 
     , convert(datetime,CONVERT(VARCHAR(10),mr.StartDate,20)) AS StartDate
FROM DW_MerchOutletRoutes mr 		
	INNER JOIN tbloutlets o ON o.ol_id = mr.ol_id 
	INNER JOIN #merchandisersIDs m ON m.id = mr.merch_Id
WHERE CONVERT(datetime,CONVERT(VARCHAR(10),mr.StartDate,20),20) between @lowerDate and @upperDate   
  AND o.status = 2
  AND mr.isrouteofday = 1 
UNION 
SELECT distinct --tor.Route_id     
       r.Merch_id
     , tor.OL_id
     , convert(datetime,CONVERT(VARCHAR(10),wd.date,20),20)       
FROM tblOutletRoutes tor 
	inner join tblRoutes r ON tor.Route_id = r.Route_id
	inner join #WeekDays wd ON wd.weekDay = SUBSTRING(r.RouteName, 1, 1)
	inner join #merchandisersIDs m ON m.id = r.Merch_id
WHERE wd.date BETWEEN 
					CASE WHEN (SELECT MAX(mor.StartDate) + 1 FROM dbo.DW_MerchOutletRoutes mor) < @lowerDate
						 THEN  @lowerDate 
						 ELSE  (SELECT MAX(mor.StartDate) FROM dbo.DW_MerchOutletRoutes mor)
					END  
			   AND @upperDate
  AND r.Status <> 9 	
  AND tor.Status <> 9 



CREATE INDEX IE_PlanedVisitOnRoute  ON #PlanedVisitOnRoute(OL_Id, Merch_id, StartDate)  
 
 if @isdebug=1
   begin
   set @msg = '#PlanedVisitOnRoute: ' +convert(varchar(20),getdate(), 114)
   raiserror (@msg, 0, 1, @msg)  with nowait
   End




if OBJECT_ID('#ScheduledProducts') is null 
create table #ScheduledProducts 
(
	   OL_Id            bigint
     , Merch_id         int
     , OLCardDate       datetime
     , Product_Id       int
     , ProductName      varchar(50)  collate Cyrillic_General_CI_AS
     , ProdGroupName    varchar(255) collate Cyrillic_General_CI_AS
     , ProdTypeName     varchar(255) collate Cyrillic_General_CI_AS
     , Unit_Name        varchar(255) collate Cyrillic_General_CI_AS
     , Packing_Capacity numeric(10,3)
     , Packing_Type     varchar(9)   collate Cyrillic_General_CI_AS
     , ProductCnName    varchar(255) collate Cyrillic_General_CI_AS
)


--drop Index tmp_idx_ScheduledProducts  on #ScheduledProducts



--delete from #ScheduledProducts
--drop table #ScheduledProducts 
INSERT INTO #ScheduledProducts
SELECT OL_Id            
     , Merch_id         
     , OLCardDate       
     , Product_Id       
     , ProductName      
     , BrandName
     , ProdTypeName     
     , Unit_Name        
     , Packing_Capacity 
     , Packing_Type     
    , ProductCnName    
FROM #Schedule     
CROSS JOIN vwDW_Products --#vwDW_Products

--rkm add new index
Create Index tmp_idx_ScheduledProducts ON #ScheduledProducts(OL_ID, OLCardDate)
--CREATE NONCLUSTERED INDEX idx_tmp_ScheduledProduct ON #ScheduledProducts([Merch_id],[Product_Id]) INCLUDE ([OL_Id], [OLCardDate])


if @isdebug=1
   begin
   set @msg = 'start #tmpOchAll: ' +convert(varchar(20),getdate(), 114)
   raiserror (@msg, 0, 1, @msg)  with nowait
   END
   
-- Calculate
SELECT olmc.Merch_Id 
     , och.OLCardDate
     , och.OL_Id -- Visited OL
     , CASE 
            WHEN sod.Product_Id IS NULL THEN NULL
	        ELSE och.OLCard_Id--/och.OL_Id
       END AS OL_Visited_IsEffective -- The visit is effective
     , ood.Product_Id AS Product_Id
     , och.OLCard_Id
INTO #tmpOchAll

FROM (
		SELECT Merch_Id
		     , OLCardDate
		     , OL_Id
		     , OLCard_Id AS OLCard_Id
		FROM      		
		(SELECT sh.Merch_Id
		     , CONVERT(datetime,CONVERT(VARCHAR(10),h.OLCardDate,104),104) as OLCardDate
		     , h.OL_Id
		     , h.OLCard_Id as OLCard_Id
		FROM tblOutletCardH h with (nolock)
			INNER JOIN #Schedule sh ON sh.ol_id = h.ol_id and sh.merch_id = h.merch_id
			LEFT JOIN tblOutletOrderH o with (nolock) ON h.OLCard_id = o.OlCard_id
			LEFT JOIN dbo.tblSalOutH  s with (nolock) ON o.OrderNo = s.OrderNo AND h.OL_id = s.OL_ID 			 	    
		WHERE convert(datetime,CONVERT(VARCHAR(10),h.OLCardDate,104),104) BETWEEN @lowerDate AND @upperDate
		--AND s.OrderNo IS NOT NULL	 		         
		GROUP BY sh.Merch_Id, h.OLCardDate, h.OL_Id, h.OLCard_Id ) as t
		GROUP BY t.Merch_id
		       , t.OLCardDate
		       , t.OL_id  
		       , t.OLCard_Id       
	)
AS och
    INNER JOIN #OutletsMerchandiserActivity_Link olmc ON olmc.OL_ID = och.OL_ID
									    AND olmc.Merch_ID = 
		    (
			    CASE 
				    WHEN (NOT EXISTS( -- ��������� ��-�� ��, ���������� �� ��
								    SELECT 1 
								    FROM #OutletsMerchandiserActivity_Link olmc1 
								    WHERE olmc1.OL_id = olmc.OL_id 
								      AND olmc1.RelatedMerch_CNT = 2 -- ���������� ��������� �� ��� ������ ��
								    )
					      ) 
				    THEN -- �� �� ������� ������ ���� ��.
					     -- �� ����������� ��, ���������� �����, ���������� ����� �� ���� ��, ������� ������� �� �� ����� �������
					    olmc.Merch_ID 
				    ELSE -- �� �� �������� ��������� ��.
					    CASE
						    WHEN ( -- ��������� ��, �������� �� �� � ��
							      EXISTS(
									    SELECT 1 
									    FROM #OutletsMerchandiserActivity_Link olmc2
									    WHERE olmc2.OL_id = olmc.OL_id 
										    AND olmc2.Merch_Id = och.Merch_ID  -- ��, ��������
									    )
							     )
						    THEN --  
							    och.Merch_Id 
						    ELSE -- �������� �� �� '�������' � ���� ��� ����� � �������� �� �� '���������� ����� ��������'. 
							     -- �������� ������ ���������� ����� � ��, ������� �������� �����
							    (SELECT TOP 1 olmc3.Merch_ID
							    FROM #OutletsMerchandiserActivity_Link olmc3
							    INNER JOIN tblSyncStatusActTypesLinks matl ON olmc3.ActivityType = matl.ActivityType 
																	      AND matl.Merch_id = och.Merch_ID
																	      AND olmc3.OL_id = och.OL_ID
							    )
					    END
			    END
		    )
   -- INNER JOIN #merchandisersIDs AS IDs ON IDs.id = 
 --			    CASE WHEN @merchandisersCnt = 0 THEN -1 ELSE olmc.Merch_id END
    LEFT JOIN tblOutletOrderH ooh with (nolock)  ON och.OLCard_id = ooh.OLCard_id -- Change inner join to left join
    LEFT JOIN tblOutletOrderD ood with (nolock)  ON ood.OrderNo = ooh.OrderNo 
    LEFT JOIN tblSalOutH soh with (nolock) ON soh.OrderNo = ooh.OrderNo AND soh.Status <> 9 and och.ol_id = soh.ol_id -- Added by Huch R and Zernoklov O
    LEFT JOIN tblSalOutD sod with (nolock) ON sod.Invoice_Id = soh.Invoice_Id and
                             (select top 1 productcn_id from tblProducts where product_id = sod.Product_Id) =
                             (select top 1 productcn_id from tblProducts where product_id = ood.Product_Id)						                      
						    AND ood.Product_qty <> 0 
						    AND sod.Product_qty <> 0
						    AND soh.Status <> 9


 if @isdebug=1
   begin
   set @msg = 'stop #tmpOchAll: ' +convert(varchar(20),getdate(), 114)
   raiserror (@msg, 0, 1, @msg)  with nowait
   END     
-- ��������� ������� � ��� ������ �������� ����� �� ��������� �����
if OBJECT_ID('#tmpoch') is null 
create table #tmpoch
(
	   Merch_Id    int
     , OLCardDate  datetime
     , OL_Id       bigint
     , OL_Visited_IsEffective  bigint
     , Product_Id int
     , OLCard_Id  bigint 
)

-- �������� ���� ���� ���������� ����. ��� ���� � ��� ���� �� ���� ���������� �����, �� �������� ���� �� ���������� ����						    
   if @isdebug=1
   begin
   set @msg = 'tmpoch: ' +convert(varchar(20),getdate(), 114)
   raiserror (@msg, 0, 1, @msg)  with nowait
   End

insert into #tmpoch
select a.merch_id
     , a.olcarddate
     , a.ol_id
     , a.OL_Visited_IsEffective
     , a.product_id
     , a.olcard_id 
from #tmpOchAll a
inner join 
(select Merch_Id 
     , OLCardDate
     , OL_Id 
--     , product_id 
     , MAX(OL_Visited_IsEffective) as OL_Visited_IsEffective     
     , MAX(OLCard_Id) as OLCard_Id
from #tmpOchAll
group by Merch_Id 
       , OLCardDate
       , OL_Id    
--       , product_id     
) t ON  a.olcard_id = case 
						  when t.OL_Visited_IsEffective is null
						  then t.OLCard_Id
                 		  else t.OL_Visited_IsEffective
                 	   end 									   

	
 if @isdebug=1
   begin
   set @msg = 'start #tmpOch_1: ' +convert(varchar(20),getdate(), 114)
   raiserror (@msg, 0, 1, @msg)  with nowait
   END
   	    
-- ���������� ������ �����						    
INSERT INTO #tmpoch
SELECT Merch_id 
     , StartDate
	 , OL_id
	 , null as iseffective_Id       
	 , null as Product_Id       
	 , null as OLCard_Id  
FROM #PlanedVisitOnRoute pv		


---- �������� ������, ������� �� ���� ���������, ������ �� ������ � ���������� ������
   if @isdebug=1
   begin
   set @msg = '#tmpOch1: ' +convert(varchar(20),getdate(), 114)
   raiserror (@msg, 0, 1, @msg)  with nowait
   End

--rkm union two view
/*Select * Into #tblSyncStatusActTypesLinks
From tblSyncStatusActTypesLinks
Create Clustered index idx_tmp_tblSyncStatusActTypesLinks on #tblSyncStatusActTypesLinks(Merch_ID, ActivityType)
*/

SELECT * 
INTO  #tmpOch1
FROM
(
	SELECT Merch_ID
         , OLCardDate
         , OL_ID
         , OL_Visited_IsEffective
         , p.Product_Id
         , OLCard_Id AS OL_Visited	
         , ProductName -- ���
         , ProdTypeName -- ����
         , BrandName AS ProdGroupName -- �����
         , Unit_Name -- ��� ��������
         , Packing_Capacity -- ������� ��������
         , Packing_Type -- ��� ��������
         , ProductCnName  
	FROM #tmpOch och
    	LEFT JOIN #vwDW_Products p ON p.Product_id = och.Product_id    
	UNION ALL
	SELECT d.Merch_ID
         , d.OLCardDate
         , d.OL_ID
         , NULL AS OL_Visited_IsEffective
         , sp.Product_Id
         , OLCard_Id AS  OL_Visited	
         , sp.ProductName -- ���
         , sp.ProdTypeName -- ����
         , sp.ProdGroupName -- �����
         , sp.Unit_Name -- ��� ��������
         , sp.Packing_Capacity -- ������� ��������
         , sp.Packing_Type -- ��� ��������
         , sp.ProductCnName  
	FROM 
	(
		SELECT /*DISTINCT*/ olmc.OL_Id
		              , olmc.Merch_id
		              , och.OLCardDate 
		FROM tblOutletCardH och with (nolock)
	    	INNER JOIN #OutletsMerchandiserActivity_Link olmc ON olmc.OL_ID = och.OL_ID
									AND olmc.Merch_ID = 
		(
			CASE 
				WHEN (NOT EXISTS( -- ��������� ��-�� ��, ���������� �� ��
								SELECT 1 
								FROM #OutletsMerchandiserActivity_Link olmc1 
								WHERE olmc1.OL_id = olmc.OL_id 
								  AND olmc1.RelatedMerch_CNT = 2 -- ���������� ��������� �� ��� ������ ��
								)
					  ) 
				THEN -- �� �� ������� ������ ���� ��.
					 -- �� ����������� ��, ���������� �����, ���������� ����� �� ���� ��, ������� ������� �� �� ����� �������
					olmc.Merch_ID 
				ELSE -- �� �� �������� ��������� ��.
					CASE
						WHEN ( -- ��������� ��, �������� �� �� � ��
							  EXISTS(
									SELECT 1 
									FROM #OutletsMerchandiserActivity_Link olmc2
									WHERE olmc2.OL_id = olmc.OL_id 
										AND olmc2.Merch_Id = och.Merch_ID  -- ��, ��������
									)
							 )
						THEN --  
							och.Merch_Id 
						ELSE -- �������� �� �� '�������' � ���� ��� ����� � �������� �� �� '���������� ����� ��������'. 
							 -- �������� ������ ���������� ����� � ��, ������� �������� �����
							(SELECT TOP 1 olmc3.Merch_ID
							FROM #OutletsMerchandiserActivity_Link olmc3
							INNER JOIN tblSyncStatusActTypesLinks matl ON olmc3.ActivityType = matl.ActivityType 
																	  AND matl.Merch_id = och.Merch_ID
																	  AND olmc3.OL_id = och.OL_ID
							)
					END
			END
		)
		INNER JOIN #merchandisersIDs AS IDs ON IDs.id = CASE 
		                                                      WHEN @merchandisersCnt = 0 THEN -1 
		                                                      ELSE olmc.Merch_id 

 	                                                 END
        --rkm ������ EXCEPT
    	Left Outer Join #tmpOch ON       #tmpOch.OL_ID      = olmc.OL_Id
		                             AND #tmpOch.Merch_id   = olmc.Merch_id
		                             AND #tmpOch.OLCardDate = och.OLCardDate
		WHERE och.OLCardDate BETWEEN @lowerDate AND @upperDate
		      And #tmpOch.OL_id is NULL

/*		
		WHERE och.OLCardDate BETWEEN @lowerDate AND @upperDate
		EXCEPT
		SELECT DISTINCT OL_Id
		              , Merch_id
		              , OLCardDate 
		FROM #tmpOch
*/
	) AS D 
	    INNER JOIN tblOutletCardH    och with (nolock)  ON d.OL_Id = och.OL_Id AND d.OLCardDate = och.OLCardDate
	    INNER JOIN #ScheduledProducts                sp ON d.OL_Id = sp.OL_Id  AND d.OLCardDate  = sp.OLCardDate
)AS DD
OPTION(MAXDOP 1)


/*-----------------------------------------------------------------------------------------------------------------*/
IF OBJECT_ID('tempdb..#tblOutlets','U') !=0
    DROP TABLE #tblOutlets

--rkm refactoring
SELECT OL_id, OL_Code,
       OLName,
       OLDeliveryAddress,
       CASE WHEN Proximity IS NULL Then -3 Else Proximity End as Proximity,
       OLSubType_id
INTO #tblOutlets 
FROM tblOutlets o WHERE o.[Status] = 2    
-- select count(*) from #tblOutlets 

CREATE UNIQUE CLUSTERED INDEX [UCI_tblOutlets] ON #tblOutlets (OL_id)
CREATE INDEX [IX_tblOutlets] ON #tblOutlets (Proximity)  --rkm commented

/*-----------------------------------------------------------------------------------------------------------------*/
   if @isdebug=1
   begin
   set @msg = 'With: ' +convert(varchar(20),getdate(), 114)
   raiserror (@msg, 0, 1, @msg)  with nowait
   End

-- ��������� ��, � ������� �� ���� �������
;WITH Result as
(
        SELECT Merch_ID
         , OLCardDate
         , OL_ID
         , OL_Visited   
         , OL_Visited_IsEffective
         , Product_Id
         , ProductName      -- ???
         , ProdTypeName     -- ????
         , ProdGroupName    -- ?????
         , Unit_Name        -- ??? ????????
         , Packing_Capacity -- ??????? ????????
         , Packing_Type     -- ??? ????????
         , ProductCnName 
         --select * from #tmpOch1 
        FROM #tmpOch1   
        UNION ALL
        SELECT #ScheduledProducts.Merch_ID
         , #ScheduledProducts.OLCardDate         
         , #ScheduledProducts.OL_ID
         , NULL AS OL_Visited
         , NULL AS OL_Visited_IsEffective
         , #ScheduledProducts.Product_Id
         , p.ProductName                -- ???
         , p.ProdTypeName               -- ????
         , p.BrandName AS ProdGroupName -- ?????
         , p.Unit_Name                  -- ??? ????????
         , p.Packing_Capacity           -- ??????? ????????
         , p.Packing_Type               -- ??? ????????
         , p.ProductCnName 
        FROM #ScheduledProducts Left Join  #tmpOch1 ON      #ScheduledProducts.OL_ID       = #tmpOch1.OL_ID
                                                             AND #ScheduledProducts.Merch_id    = #tmpOch1.Merch_id
                                                             AND #ScheduledProducts.Product_Id  = #tmpOch1.Product_Id 
                                                             AND #ScheduledProducts.OLCardDate  = #tmpOch1.OLCardDate 
                                 INNER JOIN #vwDW_Products p ON  #ScheduledProducts.Product_Id  = p.Product_Id
    WHERE #tmpOch1.OL_ID IS NULL 
) 
 
  SELECT 
          T.Supervisor_name
        , T.MerchName  
        , T.OL_Visited                                          -- Outlet is visited by Merchandiser 
        , T.OL_Visited_IsEffective                              -- Outlet is Active
        , Schedule.ol_id  AS OLSchedule                         -- Outlets on schedule
        , ol.OL_id                                              -- ??? ??
        , ol.OL_Code                                            -- ??????? ?????
        , olt.OLtype_name                                       --?????
        , olst.OLSubTypeName                                    -- ????????
        , ol.OLName                                             --???? ???
        , ol.OLDeliveryAddress                                  -- ????. ?????
        --, T.Product_Id
        , T.ProdGroupName                                       -- ?????
        , T.ProdTypeName                                        -- ????
        , T.Unit_Name                                           -- ??? ????????
        , T.Packing_Capacity                                    -- ??????? ????????
        , T.Packing_Type                                        -- ??? ????????
        , T.ProductCnName                                       -- ???????????? ??????????????? ?????????        
        , pf.ProximityName AS ProximityFactor                   -- ?????? ????????          
        , T.PLANNED
        , T.EffectivePlanned
--INTO #ResultAll --drop table #ResultAll      
FROM  --select top 100 * from #ResultAll
  (
     SELECT --DISTINCT
         -- pv.Merch_id AS Merch_id1
          merch.Supervisor_name 
        , merch.MerchName 
        , pv.VisitID  AS PLANNED             
        , CASE 
                        WHEN pv.OL_id IS NOT NULL AND r.OL_Visited_IsEffective IS NOT NULL 
                        THEN pv.VisitID
                        ELSE NULL
                  END AS EffectivePlanned
        , r.Merch_id
        , r.OL_ID
        , r.OL_Visited                                        -- Outlet is visited by Merchandiser 
        , r.OL_Visited_IsEffective AS OL_Visited_IsEffective  -- Outlet is Active
        --, r.Product_id
        , r.ProdGroupName                                                                         -- ?????
        , r.ProdTypeName                                      -- ????
        , r.Unit_Name                                         -- ??? ????????
        , r.Packing_Capacity                                  -- ??????? ????????
        , r.Packing_Type                                      -- ??? ????????
        , r.ProductCnName                                     -- ???????????? ??????????????? ?????????            
    FROM Result r LEFT JOIN #PlanedVisitOnRoute pv ON r.ol_id = pv.ol_id
                                                   AND r.Merch_id = pv.Merch_id 
                                                   AND convert(datetime,CONVERT(VARCHAR(10),r.OLCardDate,104),104) = pv.StartDate
                      INNER JOIN #merch merch  ON merch.Merch_id  = r.Merch_id   --or merch.Merch_id = och.Merch_id1      
   GROUP BY 
      merch.Supervisor_name 
    , merch.MerchName
    , r.OL_Visited
    , r.OL_Visited_IsEffective 
--    , r.Product_id
    , r.ProdGroupName 
    , r.ProdTypeName
    , r.Unit_Name 
    , r.Packing_Capacity 
    , r.Packing_Type 
    , r.ProductCnName
    , pv.VisitID          -- as PLANNED
    , CASE 
                        WHEN pv.OL_id IS NOT NULL AND r.OL_Visited_IsEffective IS NOT NULL 
                        THEN pv.VisitID
                        ELSE NULL
                  END             -- AS EffectivePlanned
    , r.OL_ID
    , r.Merch_ID                       
  ) AS T
          INNER JOIN #merch merch            ON merch.Merch_id   = T.Merch_id     --or merch.Merch_id = och.Merch_id1      
         --rkm if possible join product.
         --INNER JOIN #vwDW_Products p        ON p.Product_Id     = T.Product_Id
         LEFT  JOIN #tblOutlets ol          ON T.OL_id          = ol.OL_id       --or och.PLANNED = ol.OL_id        
         INNER JOIN tblOutletSubTypes olst  ON ol.OLSubType_id  = olst.OLSubType_id 
         INNER JOIN tblOutLetTypes olt      ON olst.OLType_ID   = olt.OLType_id        
         LEFT  JOIN tblProximityFactors pf  ON pf.Proximity_id  = ol.Proximity
         LEFT  JOIN
             (SELECT DISTINCT OL_ID, MERCH_ID 
              FROM #Schedule
              WHERE OLCardDate between @lowerDate and @upperDate
              ) AS Schedule ON Schedule.OL_Id = T.OL_Id 
                                     AND Schedule.Merch_Id = T.Merch_Id  
where Schedule.ol_id is not null and (OL_Visited is not null or 
                                      OL_Visited_IsEffective is not null or
                                      planned is not null or
                                      EffectivePlanned is not null) 
OPTION(MAXDOP 1)


DROP TABLE #tmpOch
drop table #tmpOchAll 
DROP TABLE #tmpOch1
DROP TABLE #merchandisersIDs
DROP TABLE #WeekDays
DROP TABLE #Schedule
DROP TABLE #ScheduledProducts
DROP TABLE #PlanedVisitOnRoute
--DROP TABLE #ResultAll
DROP TABLE #tblOutlets
DROP TABLE #OutletsMerchandiserActivity_Link
drop table #vwDW_Products
drop table #merch
drop table #ScheduleTotal

   if @isdebug=1
   begin
   set @msg = 'Finish: ' +convert(varchar(20),getdate(), 114)
   raiserror (@msg, 0, 1, @msg)  with nowait
   End

";
            }
        }

        #endregion
    }
}