﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Logica.Reports.Localization;
using System.Windows.Forms;

namespace Logica.Reports.Common
{
    public class ErrorManager
    {
        /// <summary>
        /// Shows the message box with the error description.
        /// </summary>
        /// <param name="message">The error description.</param>
        public static void ShowErrorBox(string message)
        {
            string errorToken = LocalizationProvider.GetText("ErrorBoxTitle");
            MessageBox.Show(message, errorToken, MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
    }
}
