﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Common;
using DevExpress.LookAndFeel;

namespace Logica.Reports.Common
{
    public class SettingProvider
    {
        static UserLookAndFeel ulf;
        public static UserLookAndFeel SkinStyle
        {
            get 
            {
                if (null == ulf)
                {
                    ulf = UserLookAndFeel.Default;
                    ulf.UseDefaultLookAndFeel = false;
                    ulf.UseWindowsXPTheme = false;
                    ulf.Style = LookAndFeelStyle.Skin;
                    //style views http://www.devexpress.com/Help/?document=winconcepts/customdocument2534.htm
                    ulf.SkinName = "The Asphalt World";
                }
                return ulf;
 
            }
        }
        /// <summary>
        /// Converts DBConnectionString to SQL connection string.
        /// </summary>
        /// <param name="connectionString">The connection string.</param>
        /// <returns></returns>
        public static string ConnectionString
        {
            get
            {
                return DataAccess.HostConfiguration.DBSqlConnection;
            }
        }
    }
}
