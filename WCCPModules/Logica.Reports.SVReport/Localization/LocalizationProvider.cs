﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Resources;
using System.Globalization;

namespace Logica.Reports.Localization
{
    public static class LocalizationProvider
    {
        #region Private fields

        private static ResourceManager _ResourceManager;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes the <see cref="LocalizationProvider"/> class.
        /// </summary>
        static LocalizationProvider()
        {
            _ResourceManager = new ResourceManager(typeof(Resource));
        }

        #endregion

        #region Public methods

        /// <summary>
        /// Gets text from the resource file.
        /// </summary>
        /// <param name="token">The token.</param>
        /// <returns>Text from the resource file.</returns>
        public static string GetText(string token)
        {
            return _ResourceManager.GetString(token);
        }


        /// <summary>
        /// Gets text from the resource file.
        /// </summary>
        /// <param name="token">The token.</param>
        /// <param name="culture">The culture.</param>
        /// <returns>Text from the resource file.</returns>
        public static string GetText(string token, CultureInfo culture)
        {
            return _ResourceManager.GetString(token, culture);
        }

        #endregion
    }
}
