﻿namespace Logica.Reports.SVReport {
    // Be careful to insert new enum because values used in DB to recognize report type
	public enum ReportType {
		/// <summary>
		/// Анализ движения продукции
		/// </summary>
		ProductsMotion = 1,
        /// <summary>
        /// Анализ визитов по дням
        /// </summary>
        VisitsAnalysis,
        /// <summary>
        /// Анализ покрытия по дням
        /// </summary>
        CoverageAnalysis,
		/// <summary>
        /// Анализ визитов
		/// </summary>
		VisitsAnalysisNew,
		/// <summary>
		/// Анализ покрытия
		/// </summary>
		CoverageAnalysisNew,
		/// <summary>
		/// АТТ по ТОР10
		/// </summary>
		ActiveOutletsTop10,
        //replaced by TurnoverByMoney report
        ///// <summary>
        ///// Данные по оборачиваемости
        ///// </summary>
        //StockturnAnalysis,
		/// <summary>
		/// База ТТ
		/// </summary>
		OutletBase,
		/// <summary>
		/// Маршруты ТА
		/// </summary>
		RoutesByOutlets,
		/// <summary>
		/// Анализ комментариев к визитам
		/// </summary>
		VisitCommentsAnalysis,
		/// <summary>
		/// Анализ несостоявшихся визитов
		/// </summary>
		FailedVisits,
        /// <summary>
        /// Оборачиваемость грн
        /// </summary>
        TurnoverByMoney,
        /// <summary>
        /// Оборачиваемость тары
        /// </summary>
        TurnoverByTare
	}
}
