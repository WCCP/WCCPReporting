﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.Grid;
using Logica.Reports.BaseReportControl;
using Logica.Reports.DataProvider;
using Logica.Reports.DataProvider.Providers;
using Logica.Reports.SVReport;
using System.Collections.ObjectModel;
using DevExpress.XtraTab;
using Logica.Reports.SVReport.UserControls;
using Logica.Reports.Localization;
using Logica.Reports.UserControls;
using DevExpress.XtraPivotGrid;
using DevExpress.XtraGrid;
using Logica.Reports.SVReport.UserControls.Contracts;
using DevExpress.XtraBars;
using Logica.Reports.Common.WaitWindow;
using DevExpress.Utils;
using DevExpress.XtraPrinting;
using Logica.Reports.Common;
using DevExpress.XtraPrintingLinks;

namespace WccpReporting
{
    public partial class WccpUIControl : DevExpress.XtraEditors.XtraUserControl
    {
        List<ReportType> _settings = new List<ReportType>();
        readonly SettingsForm _setForm;

        public WccpUIControl(int reportId)
        {
            _setForm = new SettingsForm(reportId);
            InitializeComponent();
            btnExportAllToExcel.Tag = btnExportToExcel.Tag = ExportToType.Xls;
            btnExportAllToPdf.Tag = btnExportToPdf.Tag = ExportToType.Pdf;
            btnExportAllToCsv.Tag = btnExportToCsv.Tag = ExportToType.Xlsx;
            LocalizeForm();
        }

        private void LoadData()
        {
            _settings = _setForm.GetCheckedReports();
            //Open or Upgate existed tabs
            for (int reportIndex = 0; reportIndex < _settings.Count; reportIndex++)
            {
                ReportType lReportType = _settings[reportIndex];
                IReportParams lTable;
                XtraTabPage lPage = tabManager.TabPages.FirstOrDefault(pg => (((IReportParams)pg.Tag).RepType == lReportType));

                if (null != lPage)
                {
                    lTable = lPage.Tag as IReportParams;
                }
                else
                {
                    lPage = new XtraTabPage();
                    switch (lReportType)
                    {
                        case ReportType.ProductsMotion:
                            lTable = new ProductsMotionReport();
                            break;
                        case ReportType.VisitsAnalysis:
                            lTable = new VisitsAnalysisReport();
                            break;
                        case ReportType.CoverageAnalysis:
                            lTable = new CoverageAnalysisReport();
                            break;
                        case ReportType.VisitsAnalysisNew:
                            lTable = new VisitsAnalysisNewReport();
                            break;
                        case ReportType.CoverageAnalysisNew:
                            lTable = new CoverageAnalysisNewReport();
                            break;
                        case ReportType.ActiveOutletsTop10:
                            lTable = new ActiveOutletsTOP10Report();
                            break;
                        //case ReportType.StockturnAnalysis:
                        //    lTable = new StockturnAnalysisReport();
                        //    break;
                        case ReportType.OutletBase:
                            lTable = new OutletBaseReport();
                            break;
                        case ReportType.RoutesByOutlets:
                            lTable = new RoutesByOutletsReport();
                            break;
                        case ReportType.VisitCommentsAnalysis:
                            lTable = new VisitCommentsAnalysisReport();
                            break;
                        case ReportType.FailedVisits:
                            lTable = new FailedVisitsReport();
                            break;
                        case ReportType.TurnoverByMoney:
                            lTable = new TurnoverByMoneyReport();
                            break;
                        case ReportType.TurnoverByTare:
                            lTable = new TurnoverByTareReport();
                            break;
                        default:
                            throw new ArgumentException("Unrecognizable report type");
                    }
                    lPage.Controls.Add(lTable as Control);
                    lTable.ParentTabPage = lPage;
                    lPage.Tag = lTable;
                    lPage.Text = LocalizationProvider.GetText(lReportType.ToString());
                    tabManager.TabPages.Add(lPage);
                }

                try
                {
                    lTable.ReportStartDate = _setForm.FromDate;
                    lTable.ReportEndDate = _setForm.ToDate;
                    lTable.MerchandiserCollection = _setForm.CheckedMerchandiser;
                    lTable.SupervisorCollection = _setForm.CheckedSupervisor;
                    lTable.PopulateReport();
                }
                catch (OutOfMemoryException)
                {
                    tabManager.TabPages.Remove(lPage);
                    lPage.Dispose();
                    DialogResult dialogResult =
                        XtraMessageBox.Show(
                            string.Format(LocalizationProvider.GetText("ErrorBoxOutOfMemoryError"),
                                          LocalizationProvider.GetText(lReportType.ToString())),
                            LocalizationProvider.GetText("ErrorBoxTitle"), MessageBoxButtons.YesNo,
                            MessageBoxIcon.Error);
                    if (dialogResult == DialogResult.Yes)
                        reportIndex--; // Пытаемся перегенерировать отчет
                }
            }
            //Close unnesesary tabs
            for (int i = tabManager.TabPages.Count - 1; i >= 0; i--)
                if (!_settings.Exists(rt => rt == ((IReportParams)tabManager.TabPages[i].Tag).RepType))
                    tabManager.TabPages.Remove(tabManager.TabPages[i]);


        }

        public int CallSettingsForm()
        {
            _setForm.SetCheckedReports(_settings);

            if (_setForm.ShowDialog() == DialogResult.OK)
            {
                LoadData();
                return 0;
            }

            return 1;
        }

        #region Helper

        private void LocalizeForm()
        {
            btnSettings.SuperTip = new SuperToolTip();
            btnSettings.SuperTip.Items.Add(Resource.ReportSettings);
            btnSettings.Caption = Resource.ReportSettings;

            btnClose.SuperTip = new SuperToolTip();
            btnClose.SuperTip.Items.Add(Resource.CloseSheet);
            btnClose.Caption = Resource.CloseSheet;

            btnPrint.SuperTip = new SuperToolTip();
            btnPrint.SuperTip.Items.Add(Resource.Print);
            btnPrint.Caption = Resource.Print;

            btnRefresh.SuperTip = new SuperToolTip();
            btnRefresh.SuperTip.Items.Add(Resource.Refresh);
            btnRefresh.Caption = Resource.Refresh;

            btnExportTo.SuperTip = new SuperToolTip();
            btnExportTo.SuperTip.Items.Add(Resource.ExportTo);
            btnExportTo.Caption = Resource.ExportTo;

            btnExportAllTo.SuperTip = new SuperToolTip();
            btnExportAllTo.SuperTip.Items.Add(Resource.ExportAllTo);
            btnExportAllTo.Caption = Resource.ExportAllTo;



            btnExportAllToCsv.Caption = btnExportToCsv.Caption = Resource.ExportToXlsx;
            btnExportAllToExcel.Caption = btnExportToExcel.Caption = Resource.ExportToExcel;
            btnExportAllToPdf.Caption = btnExportToPdf.Caption = Resource.ExportToPdf;
        }

        #endregion

        #region Menu Event

        private void btnSettings_ItemClick(object sender, ItemClickEventArgs e)
        {
            CallSettingsForm();
        }

        private void btnClose_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (null != tabManager.SelectedTabPage)
            {
                _settings.Remove((tabManager.SelectedTabPage.Tag as IReportParams).RepType);
                tabManager.TabPages.Remove(tabManager.SelectedTabPage);
            }
        }

        private void btnPrint_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (null != tabManager.SelectedTabPage)
            {
                IReportParams lReport = tabManager.SelectedTabPage.Tag as IReportParams;
                if (null != lReport)
                {
                    if (lReport.ContentControl is PivotGridControl)
                        (lReport.ContentControl as PivotGridControl).ShowPrintPreview();
                    else if (lReport.ContentControl is GridControl)
                        (lReport.ContentControl as GridControl).ShowPrintPreview();
                }
            }
        }

        private void btnRefresh_ItemClick(object sender, ItemClickEventArgs e)
        {
            XtraTabPage lSelectedTabPage = tabManager.SelectedTabPage;

            if (null != lSelectedTabPage && (lSelectedTabPage.Tag is IReportParams))
            {
                IReportParams lReport = (lSelectedTabPage.Tag as IReportParams);
                bool lIsRegenerationRequired = true;

                while (lIsRegenerationRequired)
                {
                    try
                    {
                        lReport.PopulateReport();
                        lIsRegenerationRequired = false;
                    }
                    catch (OutOfMemoryException)
                    {
                        DialogResult lDialogResult = XtraMessageBox.Show(
                            string.Format(LocalizationProvider.GetText("ErrorBoxOutOfMemoryError"),
                                          LocalizationProvider.GetText(lReport.RepType.ToString())),
                            LocalizationProvider.GetText("ErrorBoxTitle"), MessageBoxButtons.YesNo,
                            MessageBoxIcon.Error);
                        lIsRegenerationRequired = lDialogResult == DialogResult.Yes;
                        if (!lIsRegenerationRequired)
                        {
                            tabManager.TabPages.Remove(lSelectedTabPage);
                            lSelectedTabPage.Dispose();
                        }
                    }
                }
            }
        }

        #region Export engine

        private void btnExportTo_ItemClick(object sender, ItemClickEventArgs e)
        {
            Export((ExportToType)e.Item.Tag);
        }
        private void Export(ExportToType type)
        {
            if (null == tabManager.SelectedTabPage)
                return;
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Title = Resource.SpecifyFileName;
            sfd.InitialDirectory = System.Reflection.Assembly.GetExecutingAssembly().Location;
            sfd.FileName = "Report";
            sfd.Filter = String.Format("(*.{0})|*.{0}", type.ToString().ToLower());
            sfd.FilterIndex = 1;
            sfd.OverwritePrompt = true;
            sfd.RestoreDirectory = true;

            if (sfd.ShowDialog() != DialogResult.Cancel)
            {
                ExportEngine(tabManager.SelectedTabPage, sfd.FileName, type);
            }
        }

        private void btnExportAll_ItemClick(object sender, ItemClickEventArgs e)
        {
            ExportToType type = (ExportToType)e.Item.Tag;
            if (tabManager.TabPages.Count < 2)
            {
                Export(ExportToType.Xls);
                return;
            }
            if (DialogResult.Yes == MessageBox.Show(String.Format(Resource.ConfirmExportAllSheets, tabManager.TabPages.Count), Resource.Information, MessageBoxButtons.YesNo, MessageBoxIcon.Question))
            {
                FolderBrowserDialog fbd = new FolderBrowserDialog();
                fbd.Description = Resource.SelectFolder;
                string path = System.Reflection.Assembly.GetExecutingAssembly().Location;
                path = path.Substring(0, path.LastIndexOf(@"\") + 1);
                fbd.SelectedPath = path;

                if (fbd.ShowDialog() != DialogResult.Cancel)
                {
                    foreach (XtraTabPage page in tabManager.TabPages)
                    {
                        string fName = String.Format("{0}\\SVReport_{1}.{2}", fbd.SelectedPath, (page.Tag as IReportParams).RepType.ToString(), type.ToString().ToLower());
                        ExportEngine(page, fName, type);
                    }
                }
            }
        }
        private void ExportEngine(XtraTabPage page, string fName, ExportToType type)
        {
            IReportParams report = page.Tag as IReportParams;
            if (null != report)
            {
                CompositeLink pl = new CompositeLink(new PrintingSystem());
                if (report.ExportHeaderGrid
                    || (type != ExportToType.Xlsx
                    && type != ExportToType.Xls))
                {
                    pl.Links.Add(new PrintableComponentLink { Component = report.HeaderGrid });
                }
                if (report.ContentControl is GridControl)
                    pl.Links.Add(new PrintableComponentLink { Component = (GridControl)report.ContentControl });
                if (report.ContentControl is PivotGridControl)
                    pl.Links.Add(new PrintableComponentLink { Component = (PivotGridControl)report.ContentControl });
                pl.CreateDocument();
                try
                {
                    switch (type)
                    {
                        case ExportToType.Csv:
                            pl.PrintingSystem.ExportToCsv(fName);
                            break;
                        case ExportToType.Html:
                            pl.PrintingSystem.ExportToHtml(fName);
                            break;
                        case ExportToType.Bmp:
                            pl.PrintingSystem.ExportToImage(fName);
                            break;
                        case ExportToType.Mht:
                            pl.PrintingSystem.ExportToMht(fName);
                            break;
                        case ExportToType.Rtf:
                            pl.PrintingSystem.ExportToRtf(fName);
                            break;
                        case ExportToType.Txt:
                            pl.PrintingSystem.ExportToText(fName);
                            break;

                        case ExportToType.Xls:
                            pl.PrintingSystem.ExportToXls(fName);
                            break;
                        case ExportToType.Pdf:
                            pl.PrintingSystem.ExportToPdf(fName);
                            break;
                        case ExportToType.Xlsx:
                            pl.PrintingSystem.ExportToXlsx(fName);
                            break;
                    }
                }
                catch (Exception ex)
                {
                    try
                    {
                        if (File.Exists(fName))
                            File.Delete(fName);
                    }
                    catch { }
                    if (ex.Message.StartsWith("The created XLS file is too big for the XLS format, because it contains more than 256 columns."))
                    {
                        XtraMessageBox.Show(Resource.ExportError_MoreThan256Columns, Resource.Error);
                    }
                    else
                    {
                        XtraMessageBox.Show(String.Format(Resource.ExportError, fName), Resource.Error);
                    }
                }
            }
        }

        #endregion

        private void tabManager_Click(object sender, EventArgs e)
        {

        }


        #endregion
    }
}
