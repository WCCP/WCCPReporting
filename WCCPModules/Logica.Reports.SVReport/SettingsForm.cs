using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Linq;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Logica.Reports.Common.WaitWindow;
using Logica.Reports.Localization;
using Logica.Reports.DataProvider.Providers;
using DevExpress.XtraEditors.Controls;
using System.Collections.ObjectModel;
using Logica.Reports.ModelDataProvider.Entities;
using Logica.Reports.ModelDataProvider.Providers;
using Logica.Reports.Common;


namespace Logica.Reports.SVReport
{
    public enum ActivityGroupType
    {
        OnTrade = 2,
        OffTrade = 1,
        KATrade = 3,
        Undefined = -10
    }
	public partial class SettingsForm : DevExpress.XtraEditors.XtraForm
	{
		List<ReportType> exceptedReport = new List<ReportType>(new ReportType[] { ReportType.ActiveOutletsTop10 });

		#region Report option

		public DateTime FromDate {
			get { return dtFrom.DateTime.Date; }
		}

		public DateTime ToDate {
			get { return dtTo.DateTime.Date.AddDays(1).AddSeconds(-1); }
		}

		public List<Merchandiser> CheckedMerchandiser {
			get {
				List<Merchandiser> selectedMerches = new List<Merchandiser>();
				foreach (CheckedListBoxItem clbi in ccbM1.Properties.Items)
					if (clbi.CheckState == CheckState.Checked)
						selectedMerches.Add(merchList.Find(delegate(Merchandiser merch) { return Convert.ToInt32(clbi.Value) == merch.Merch_id; }));
				return selectedMerches;
			}
		}

		public List<Supervisor> CheckedSupervisor {
			get {
				List<Supervisor> selectedSvs = new List<Supervisor>();
				foreach (CheckedListBoxItem clbi in ccbM2.Properties.Items)
					if (clbi.CheckState == CheckState.Checked)
						selectedSvs.Add(svList.Find(delegate(Supervisor sv) { return Convert.ToInt32(clbi.Value) == sv.Supervisor_ID; }));
				return selectedSvs;
			}
		}

		public List<ReportType> GetCheckedReports() {
			List<ReportType> res = new List<ReportType>();
			foreach (CheckedListBoxItem clbi in clbReports.CheckedItems)
				res.Add((ReportType)clbi.Value);
			return res;
		}

		public void SetCheckedReports(List<ReportType> value) {
			foreach (CheckedListBoxItem clbi in clbReports.Items) {
				if (value.IndexOf((ReportType)clbi.Value) >= 0)
					clbi.CheckState = CheckState.Checked;
				else
					clbi.CheckState = CheckState.Unchecked;
			}
		}

		#endregion

		List<Merchandiser> merchList = null;
		List<Supervisor> svList = null;

		public SettingsForm(int reportId) {
			InitializeComponent();

			MerchandiserDataProvider merchandiserProvider = new MerchandiserDataProvider();
			SupervisorDataProvider supervisorProvider = new SupervisorDataProvider();

			merchList = merchandiserProvider.GetAll(reportId);
            svList = supervisorProvider.GetAll(reportId);

			dtFrom.DateTime = DateTime.Now.AddDays(1 - DateTime.Now.Day).Date;
			dtTo.DateTime = DateTime.Now;
			//dtFrom.DateTime = new DateTime(2009, 9, 9);
			//dtTo.DateTime = new DateTime(2009, 9, 10);
			foreach (ReportType type in Enum.GetValues(typeof(ReportType))) {
				if (exceptedReport.Contains(type))
					continue;

				clbReports.Items.Add(type, LocalizationProvider.GetText(type.ToString()));
			}

			chkOn.Tag = ActivityGroupType.OnTrade;
			chkOff.Tag = ActivityGroupType.OffTrade;

			foreach (Merchandiser m in merchList)
				ccbM1.Properties.Items.Add(m.Merch_id, m.MerchName, CheckState.Checked, true);
			foreach (Supervisor s in svList)
				ccbM2.Properties.Items.Add(s.Supervisor_ID, s.Supervisor_name, CheckState.Checked, true);
			LocalizeForm();
			ccbM2.TextChanged += new EventHandler(ccbM2_TextChanged);
            GetUpdData();
		}


		private void btnYes_Click(object sender, EventArgs e) {
			if (ValidateData())
				DialogResult = DialogResult.OK;
		}

		#region Helper

		private void LocalizeForm()
		{
			txtReportName.Text = Resource.SVReport;
			lblFrom.Text = Resource.From;
			lblTo.Text = Resource.To;
			lblM1.Text = Resource.M1;
			lblM2.Text = Resource.M2;
			lblReport.Text = Resource.Report;
			lblSpan.Text = Resource.Span;
			btnYes.Text = Resource.Yes;
			btnNo.Text = Resource.No;
			btnHelp.Text = Resource.Help;
			pgAction.Text = Resource.Action;
			Text = Resource.Report.TrimEnd(':');
			ccbM1.Properties.SelectAllItemCaption = ccbM2.Properties.SelectAllItemCaption = Resource.All;
			chkAll.Text = Resource.All;
			chkOn.Text = Resource.AllOnTrade;
			chkOff.Text = Resource.AllOffTrade;

			tcOptions.LookAndFeel.Assign(SettingProvider.SkinStyle);
			clbReports.LookAndFeel.Assign(SettingProvider.SkinStyle);
			OptionsPanel.LookAndFeel.Assign(SettingProvider.SkinStyle);
			LookAndFeel.Assign(SettingProvider.SkinStyle);
			btnHelp.LookAndFeel.Assign(SettingProvider.SkinStyle);
			btnNo.LookAndFeel.Assign(SettingProvider.SkinStyle);
			btnYes.LookAndFeel.Assign(SettingProvider.SkinStyle);
		}

		private bool ValidateData()
		{

			int spanSize = Int32.MaxValue;
			try
			{
				spanSize = Int32.Parse(System.Configuration.ConfigurationManager.AppSettings["MaxSpanSize"]);

			}
			catch
			{
				//TODO: Add info into log file
			}

			if (dtFrom.DateTime > dtTo.DateTime)
			{
				MessageBox.Show(Resource.Error_IncorrectSpan, Resource.ErrorBoxTitle, MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return false;
			}
			else if ((dtTo.DateTime - dtFrom.DateTime).Days > spanSize)
			{
				MessageBox.Show(String.Format(Resource.Error_SpanToBig, spanSize), Resource.ErrorBoxTitle, MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return false;
			}
			else if (0 == clbReports.CheckedItems.Count)
			{
				MessageBox.Show(Resource.Error_IncorrectReportSelection, Resource.ErrorBoxTitle, MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return false;
			}
			else if (String.IsNullOrEmpty(ccbM2.Properties.GetCheckedItems().ToString()))
			{
				MessageBox.Show(Resource.Error_IncorrectM2Selection, Resource.ErrorBoxTitle, MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return false;
			}
			else if (String.IsNullOrEmpty(ccbM1.Properties.GetCheckedItems().ToString()))
			{
				MessageBox.Show(Resource.Error_IncorrectM1Selection, Resource.ErrorBoxTitle, MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return false;
			}

			else
				return true;
		}
		#endregion

		private void chkAll_CheckedChanged(object sender, EventArgs e)
		{
			clbReports.ItemCheck -= clbReports_ItemCheck;
			if (chkAll.CheckState == CheckState.Checked)
				foreach (CheckedListBoxItem item in clbReports.Items)
					item.CheckState = CheckState.Checked;
			else if (chkAll.CheckState == CheckState.Unchecked)
				foreach (CheckedListBoxItem item in clbReports.Items)
					item.CheckState = CheckState.Unchecked;
			clbReports.ItemCheck += clbReports_ItemCheck;
		}

		private void clbReports_ItemCheck(object sender, DevExpress.XtraEditors.Controls.ItemCheckEventArgs e)
		{
			chkAll.CheckedChanged -= chkAll_CheckedChanged;
			if (clbReports.CheckedItems.Count == clbReports.Items.Count)
				chkAll.CheckState = CheckState.Checked;
			else if (chkAll.CheckState != CheckState.Unchecked)
				chkAll.CheckState = CheckState.Unchecked;
			chkAll.CheckedChanged += chkAll_CheckedChanged;

		}

		private void chk_CheckedChanged1(object sender, EventArgs e)
		{
			ccbM1.TextChanged -= ccbM1_TextChanged;
			foreach (CheckedListBoxItem clbi in ccbM1.Properties.Items)
			{
				if ((int)ActivityGroupType.OnTrade == merchList.Find(delegate(Merchandiser merch) { return Convert.ToInt32(clbi.Value) == merch.Merch_id; }).ActivityType)
					clbi.CheckState = chkOn.CheckState;
				else if (!chkOff.Checked)
					clbi.CheckState = CheckState.Unchecked;
			}
			ccbM1.TextChanged += ccbM1_TextChanged;
		}

		private void chk_CheckedChanged2(object sender, EventArgs e)
		{
			ccbM1.TextChanged -= ccbM1_TextChanged;
			foreach (CheckedListBoxItem clbi in ccbM1.Properties.Items)
			{
				if ((int)ActivityGroupType.OffTrade == merchList.Find(delegate(Merchandiser merch) { return Convert.ToInt32(clbi.Value) == merch.Merch_id; }).ActivityType)
					clbi.CheckState = chkOff.CheckState;
				else if (!chkOn.Checked)
					clbi.CheckState = CheckState.Unchecked;
			}
			ccbM1.TextChanged += ccbM1_TextChanged;
		}

		private void ccbM1_TextChanged(object sender, EventArgs e)
		{
			chkOn.CheckedChanged -= chk_CheckedChanged1;
			chkOff.CheckedChanged -= chk_CheckedChanged2;
			chkOn.CheckState = CheckState.Unchecked;
			chkOff.CheckState = CheckState.Unchecked;
			chkOn.CheckedChanged += chk_CheckedChanged1;
			chkOff.CheckedChanged += chk_CheckedChanged2;

		}
		private void ccbM2_TextChanged(object sender, EventArgs e)
		{
			ccbM1.Properties.Items.Clear();
			if (svList.Count == CheckedSupervisor.Count)
				foreach (Merchandiser m in merchList)
					ccbM1.Properties.Items.Add(m.Merch_id, m.MerchName, CheckState.Checked, true);
			else if (CheckedSupervisor.Count != 0)
				foreach (Merchandiser m in merchList)
					if (CheckedSupervisor.Exists(delegate(Supervisor sv) { return sv.Supervisor_ID == m.Supervisor_ID; }))
						ccbM1.Properties.Items.Add(m.Merch_id, m.MerchName, CheckState.Checked, true);

		}

        private void btn_upd_Click(object sender, EventArgs e)
        {
            Enabled = false;
            WaitManager.StartWait();
            try
            {
                using (var provider = new UpdateInfoDataProvider())
                {
                    provider.UpdateData();
                }
            }
            catch (Exception) { }

            GetUpdData();
            Enabled = true;
            WaitManager.StopWait();

        }
        
        private void GetUpdData()
        {
            lblUpd2.Text = "";

            using (var provider = new UpdateInfoDataProvider())
            {
                var updateTime = provider.GetUpdTime(DateTime.Now).FirstOrDefault();
                if (updateTime != null) lblUpd2.Text = updateTime.LastUpdateDateTime.ToString();
            }
        }

	}
}