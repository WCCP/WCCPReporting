﻿namespace Logica.Reports.DataProvider {
    /// <summary>
    /// Represents string constants to access datatable fields.
    /// </summary>
    static class FieldNames {
        public const string AreaInCity = "AreaInCityColumn";
        public const string AreaName = "AreaNameColumn";
        public const string BankAddress = "BankAddressColumn";
        public const string BankCode = "BankCodeColumn";
        public const string BankName = "BankNameColumn";
        public const string BeginTime = "BeginTimeColumn";
        public const string CityName = "CityNameColumn";
        public const string ProductCnName = "ProductCnNameColumn";
        public const string Comments = "CommentsColumn";
        public const string CoolerNumber = "CoolerNumberColumn";
        public const string ContractDate = "ContractDateColumn";
        public const string ContractNumber = "ContractNumberColumn";
        public const string CurMonthBottledSales = "CurMonthBottledSalesColumn";
        public const string CurMonthKegSales = "CurMonthKegSalesColumn";
        public const string CustomerName = "CustomerNameColumn";
        public const string Date = "DateColumn";
        public const string DistrictName = "DistrictNameColumn";
        public const string Duration = "DurationColumn";
        public const string EndTime = "EndTimeColumn";
        public const string FailedVisitsCount = "FailedVisitsCountColumn";
        public const string FridgeNumber = "FridgeNumberColumn";
        public const string InaccessibilityReason = "InaccessibilityReasonColumn";
        public const string IPN = "IPNColumn";
        public const string MerchandiserID = "MerchandiserIDColumn";
        public const string MerchandiserName = "MerchandiserNameColumn";
        public const string MerchandiserStatus = "MerchandiserStatusColumn";
        public const string OrdersCount = "OrdersCountColumn";
        public const string OutletAccount = "OutletAccountColumn";
        public const string OutletAccountant = "OutletAccountantColumn";
        public const string OutletAccountantPhone = "OutletAccountantPhoneColumn";
        public const string OutletActiveNumber = "OutletActiveNumberColumn";
        public const string OutletAddress = "OutletAddressColumn";
        public const string OutletCardDate = "OutletCardDateColumn";
        public const string OutletDeliveryAddress = "OutletDeliveryAddressColumn";
        public const string OutletDirector = "OutletDirectorColumn";
        public const string OutletEmail = "OutletEmailColumn";
        public const string OutletExternalCode = "OutletExternalCodeColumn";
        public const string OutletFactAddressName = "OutletFactAddressNameColumn";
        public const string OutletFax = "OutletFaxColumn";
        public const string OutletHasRemains = "OutletHasRemainsColumn";
        public const string OutletID = "OutletIDColumn";
        public const string OutletLawAddressName = "OutletLawAddressNameColumn";
        public const string OutletMarketingManager = "OutletMarketingManagerColumn";
        public const string OutletMarketingManagerPhone = "OutletMarketingManagerPhoneColumn";
        public const string OutletName = "OutletNameColumn";
        public const string OutletPhone = "OutletPhoneColumn";
        public const string OutletPurchaseManager = "OutletPurchaseManagerColumn";
        public const string OutletSize = "OutletSizeColumn";
        public const string OutletStatus = "OutletStatusColumn";
        public const string OutletSubtype = "OutletSubTypeColumn";
        public const string OutletTradingName = "OutletTradingNameColumn";
        public const string OutletType = "OutletTypeColumn";
        public const string Population = "PopulationColumn";
        public const string OLSchedule = "OLScheduleColumn";
        public const string OlCompletedPlan = "OlCompletedPlanColumn";
        public const string BelongingTOPSKU = "BelongingTOPSKUColumn";

        public const string IsTop = "IsTopColumn";
        public const string IsRecommendedOrder = "IsRecommendedOrderColumn";
        public const string RecomendProductVolume = "RecomendProductVolumeColumn";
        public const string RecomendProductQty = "RecomendProductQtyColumn"; 
        public const string UseRecommnededOrder = "UseRecommnededOrderColumn";
        public const string VolumeRecommnededOrder = "VolumeRecommnededOrderColumn";
        public const string RemainsProductQty = "RemainsProductQtyColumn";
        public const string RecOrderCount = "RecOrderCountColumn";

        public const string OrderedProductQty = "OrderedProductQtyColumn"; 
        
        public const string PLANNED = "PLANNEDColumn";
        public const string EFFECTIVEPLANNED = "EFFECTIVEPLANNEDColumn";

        public const string OutletVisitedIsEffective = "OutletVisitedIsEffectiveColumn";
        public const string OutletVisitedNumber = "OutletVisitedNumberColumn";
        public const string OutletWarehouseSize = "OutletWarehouseSizeColumn";
        public const string PackingCapacity = "PackingCapacityColumn";
        public const string PackingType = "PackingTypeColumn";
        public const string PlanedVisitsCount = "PlanedVisitsCountColumn";
        public const string PrevMonthBottledSales = "PrevMonthBottledSalesColumn";
        public const string PrevMonthKegSales = "PrevMonthKegSalesColumn";
        public const string ProductGroupName = "ProductGroupNameColumn";
        public const string ProductID = "ProductIDColumn";
        public const string ProductName = "ProductNameColumn";
        public const string ProductTypeName = "ProductTypeNameColumn";
        public const string ProximityFactor = "ProximityFactorColumn";
        public const string RegionName = "RegionNameColumn";
        public const string RemaindersCount = "RemaindersCountColumn";
        public const string RouteName = "RouteNameColumn";
        public const string SalesCount = "SalesCountColumn";
        public const string SupervisorName = "SupervisorNameColumn";
        public const string TerritoryType = "TerritoryTypeColumn";
        public const string TotalVisitsCount = "TotalVisitsCountColumn";
        public const string UnitName = "UnitNameColumn";
        public const string VATN = "VATNColumn";
        public const string ZKPO = "ZKPOColumn";

        public const string SaleDate = "SaleDateColumn";
        public const string OrderDate = "OrderDateColumn";
        public const string OrderedProductVolume = "OrderedProductVolumeColumn";
        public const string SaledOrderedProductVolume = "SaledOrderedProductVolumeColumn";
        public const string ToTakeOffProductVolume = "ToTakeOffProductVolumeColumn";
        public const string RemainsProductVolume = "RemainsProductVolumeColumn";
        public const string WithoutOrder = "WithoutOrderColumn";
        public const string OverOrdered = "OverOrderedColumn";
        public const string HLCode = "HLCodeColumn";
        public const string OutletHas = "OutletHasColumn";
        public const string Reason = "ReasonColumn";
        public const string NotVisitedComments = "NotVisitedCommentsColumn";
        public const string CommentsByVisit = "CommentsByVisitColumn";

        public const string SalesInCount = "SalesInCountColumn";
        public const string SalesInMoney = "SalesInMoneyColumn";
        public const string DebtInCount = "DebtInCountColumn";
        public const string DebtInMoney = "DebtInMoneyColumn";
        public const string Stockturn = "StockturnColumn";
        public const string DebtTypeName = "DebtTypeNameColumn";
        public const string DebtAmount = "DebtAmountColumn";
        public const string SaleAmount = "SaleAmountColumn";

        public const string DZ = "DZColumn";
        public const string TurnoverDZ = "TurnoverDZColumn";

        public const string Tare = "TareColumn";
        public const string TareType = "TareTypeColumn";

        
      
    }
}
