﻿using System;
using System.Drawing;

namespace SoftServe.Reports.SVReport 
{
    public class SqlConstants
    {
     
        // SP names
        public const string MotionOfProducts = "spDW_SV_MotionOfProducts";         // Анализ движения продукции
        public const string VisitsAnalysis = "spDW_SV_VisitsAnalysis";             // Анализ визитов по дням
        public const string VisitsAnalysisNew = "spDW_SV_VisitsAnalysisNew";       // Анализ визитов 
        public const string CoverageAnalysis = "spDW_SV_CoverageAnalysis";         // Анализ покрития по дням
        public const string CoverageAnalysisNew = "spDW_SV_CoverageAnalysisNew";   // Анализ покрития 
        public const string TurnoverByMoney = "spDW_SV_TurnoverByMoney";           // Оборачиваемость грн.
        public const string TurnStock = "[Debt].[sp_turnover]";                    // Оборачиваемость тара
        public const string OutletBase = "spDW_SV_OutletBase";                     // База ТТ
        public const string RoutesByOutlets = "spDW_SV_RoutesByOutlets";           // Маршрути ТА

        public const string FailedVisits = "spDW_SV_FailedVisits";                 // Анализ несостоявшихся визитов
        public const string ActiveOutletsTOP10 = "spDW_SV_ActiveOutletsTOP10";     // Анализ коментариев к визитам    

        public const string VisitCommentsAnalysisNew = "spDW_SV_VisitCommentsAnalysis";     // Анализ коментариев к визитам    


                 

        //spDW_SV_ActiveOutletsTOP10



 
       

        //conn.Open();
        //            SqlCommand cmd = new SqlCommand(SqlConstants.CoverageAnalysisNew, conn);
        //            cmd.CommandType = CommandType.StoredProcedure;
        //            cmd.CommandTimeout = 3000;


//        spDW_SV_ActiveOutletsTOP10

//spDW_SV_FailedVisits
//spDW_SV_MotionOfProducts
//spDW_SV_OutletBase
//spDW_SV_RoutesByOutlets
//spDW_SV_StockturnAnalysis
//spDW_SV_TurnoverByMoney
//spDW_SV_VisitCommentsAnalysis



        // Field Names       
        public const string LevelFieldName = "Level";

        // SP Params
        public const string StartDate = "@lowerDate";
        public const string EndDate = "@upperDate";
        public const string SV_ID = "@supervisorId";
        public const string Merch_ID = "@merchandisersIDs";
        public const string Merch_Count = "@merchandisersCnt"; 
    }
}
