﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Configuration;
using System.Data.Common;
using Logica.Reports.DataProvider.Entities;
using Logica.Reports.Localization;
using Logica.Reports.Common;
using Logica.Reports.ModelDataProvider.Entities;
using System.Data;
using Logica.Reports.DataAccess;
using SoftServe.Reports.SVReport;

namespace Logica.Reports.DataProvider.Providers
{
    public class FailedVisitsDataProvider : DataProviderBase
    {
        #region Public properties

        /// <summary>
        /// Gets the name of the report.
        /// </summary>
        /// <value>The name of the report.</value>
        public override string ReportName
        {
            get 
            { 
                return "FailedVisits";
            }
        }

        #endregion

        #region Public methods

        /// <summary>
        /// Gets the report data.
        /// </summary>
        /// <param name="startDate">The report start date.</param>
        /// <param name="endDate">The report end date.</param>
        /// <returns></returns>
        public FailedVisitsDataSet.ReportEntriesDataTable GetReportData(DateTime startDate, DateTime endDate, List<Merchandiser> merchList)
        {
            FailedVisitsDataSet dataSet = new FailedVisitsDataSet();

            try
            {

                using (SqlConnection conn = new SqlConnection(DataAccessLayer.ConnectionString.ToString()))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand(SqlConstants.FailedVisits, conn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandTimeout = 3000;

                    cmd.Parameters.Add("@lowerDate", SqlDbType.DateTime);
                    cmd.Parameters["@lowerDate"].Value = startDate;
                    cmd.Parameters.Add("@upperDate", SqlDbType.DateTime);
                    cmd.Parameters["@upperDate"].Value = endDate;
                    cmd.Parameters.Add("@merchandisersCnt", SqlDbType.Int);
                    cmd.Parameters["@merchandisersCnt"].Value = merchList.Count;
                    cmd.Parameters.Add("@merchandisersIDs", SqlDbType.VarChar);
                    cmd.Parameters["@merchandisersIDs"].Value = GetMerchandiserIDs(merchList);
                    cmd.Parameters.Add("@supervisorId", SqlDbType.Int);
                    cmd.Parameters["@supervisorId"].Value = DBNull.Value;

                    Dictionary<string, string> SupervisorNames = new Dictionary<string, string>();
                    Dictionary<string, string> MerchandiserNames = new Dictionary<string, string>();
                    Dictionary<string, string> OutletTypes = new Dictionary<string, string>();
                    Dictionary<string, string> OutletSubTypes = new Dictionary<string, string>();
                    Dictionary<string, string> OutletTradingNames = new Dictionary<string, string>();
                    Dictionary<string, string> OutletNames = new Dictionary<string, string>();
                    Dictionary<string, string> OutletDeliveryAddresss = new Dictionary<string, string>();
                    Dictionary<string, string> MerchandiserStatuss = new Dictionary<string, string>();
                    Dictionary<string, string> OutletStatuss = new Dictionary<string, string>();
                    Dictionary<string, string> OutletAddresss = new Dictionary<string, string>();
                    Dictionary<string, string> OutletExternalCodes = new Dictionary<string, string>();
                    Dictionary<string, string> DistrictNames = new Dictionary<string, string>();
                    Dictionary<string, string> CityNames = new Dictionary<string, string>();
                    Dictionary<string, string> InaccessibilityReasons = new Dictionary<string, string>();
                    Dictionary<string, string> OutletFactAddressNames = new Dictionary<string, string>();
                    Dictionary<string, string> OutletLawAddressNames = new Dictionary<string, string>();
                    Dictionary<string, string> Commentss = new Dictionary<string, string>();
                    
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        DataTable t = dataSet.Tables[ReportName];
                        t.MinimumCapacity = 100000;

                        while (reader.Read())
                        {
                            DataRow row;
                            row = t.NewRow();
                            
                            row[FieldNames.SupervisorName] = SupervisorNames.AddStringValue(reader.GetString(0));

                            row[FieldNames.MerchandiserName] = MerchandiserNames.AddStringValue(reader.GetString(1));
                            row[FieldNames.FailedVisitsCount] = reader.GetInt32(2);
                            row[FieldNames.OutletID] = reader.GetInt64(3);
                            row[FieldNames.OutletExternalCode] = OutletExternalCodes.AddStringValue(reader.GetString(4));
                            if (!reader.IsDBNull(5))
                            {
                                row[FieldNames.InaccessibilityReason] = InaccessibilityReasons.AddStringValue(reader.GetString(5));
                            }
                            row[FieldNames.OutletFactAddressName] = OutletFactAddressNames.AddStringValue(reader.GetString(6));
                            row[FieldNames.OutletLawAddressName] = OutletLawAddressNames.AddStringValue(reader.GetString(7));
                            row[FieldNames.OutletSubtype] = OutletSubTypes.AddStringValue(reader.GetString(8));
                            row[FieldNames.OutletType] = OutletTypes.AddStringValue(reader.GetString(9));
                            row[FieldNames.OutletName] = OutletNames.AddStringValue(reader.GetString(10));
                            row[FieldNames.OutletDeliveryAddress] = OutletDeliveryAddresss.AddStringValue(reader.GetString(11));
                            row[FieldNames.OutletTradingName] = OutletTradingNames.AddStringValue(reader.GetString(12));
                            row[FieldNames.OutletAddress] = OutletAddresss.AddStringValue(reader.GetString(13));
                            row[FieldNames.OutletStatus] = OutletStatuss.AddStringValue(reader.GetString(14));
                            row[FieldNames.MerchandiserStatus] = MerchandiserStatuss.AddStringValue(reader.GetString(15));
                            row[FieldNames.DistrictName] = DistrictNames.AddStringValue(reader.GetString(16));
                            row[FieldNames.CityName] = CityNames.AddStringValue(reader.GetString(17));
                            if (!reader.IsDBNull(18))
                            {
                                row[FieldNames.Comments] = Commentss.AddStringValue(reader.GetString(18));
                            }
                            row[FieldNames.OutletCardDate] = reader.GetDateTime(19);
                            //row[FieldNames.AreaInCity] = reader.GetDateTime(11);
                     
                            t.Rows.Add(row);
                        }

                        t.AcceptChanges();

                        reader.Close();
                    }
                }
            }
            catch (OutOfMemoryException)
            {
                dataSet.Dispose();
                throw;
            }
            catch (SystemException ex)
            {
                throw new SystemException(string.Format(Resource.ReportBuildingError, ReportName, ex.Message));
            }

            return dataSet.ReportEntries;
        }

        #endregion

        #region Protected methods

        /// <summary>
        /// Gets the column mapping between the source table and the dataset.
        /// </summary>
        /// <returns>Column mapping.</returns>
        protected override DataTableMapping GetMapping()
        {
            DataTableMapping map = new DataTableMapping();
            map.SourceTable = ReportName;
            map.DataSetTable = ReportName;

            map.ColumnMappings.AddRange(new DataColumnMapping[]{
                Supervisor_name,MerchName, OLtype_name,OLSubTypeName,
                OLName, OLDeliveryAddress,OLTradingName,OLAddress,
                OL_id, OL_Code, MerchStatus, OutletsSatus, 
                Cust_Name, City_Name, OLCardDate, Comments, Area_Name});
            map.ColumnMappings.Add("FailedVisitsCount", FieldNames.FailedVisitsCount);
            map.ColumnMappings.Add("Reason", FieldNames.InaccessibilityReason);
            map.ColumnMappings.Add("FactTT", FieldNames.OutletFactAddressName);
            map.ColumnMappings.Add("LawTT", FieldNames.OutletLawAddressName);
            return map;
        }

        ///// <summary>
        ///// Inserts the value into the dictionary. If it's already present in the dictionary, returns the reference on the current occurence. 
        ///// </summary>
        ///// <param name="val">The element to add</param>
        ///// <param name="d">The dictionary to be enhanced</param>
        ///// <returns>Inserted value</returns>
        //protected string AddStringValue(string val, Dictionary<string, string> d)
        //{
        //    if (!d.Keys.Contains(val))
        //    {
        //        d.Add(val, val);
        //    }

        //    return d[val];
        //}
      

        #endregion
    }
}
