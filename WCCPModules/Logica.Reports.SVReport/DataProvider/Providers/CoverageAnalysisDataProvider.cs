﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.Data.Common;
using System.Configuration;
using Logica.Reports.DataProvider.Entities;
using Logica.Reports.Localization;
using Logica.Reports.Common;
using Logica.Reports.ModelDataProvider.Entities;
using Logica.Reports.DataAccess;
using SoftServe.Reports.SVReport;

namespace Logica.Reports.DataProvider.Providers
{
	public class CoverageAnalysisDataProvider : DataProviderBase
	{
		#region Public properties

		/// <summary>
		/// Gets the current report name.
		/// </summary>
		public override string ReportName
		{
			get
			{
				return "CoverageAnalysis";
			}
		}

		#endregion

		#region Public methods

		/// <summary>
		/// Gets the report data.
		/// </summary>
		/// <param name="startDate">The report start date.</param>
		/// <param name="endDate">The report end date.</param>
		/// <returns></returns>
		public CoverageAnalysisDataSet.ReportEntriesDataTable GetReportData(DateTime startDate, DateTime endDate, List<Merchandiser> merchList)
		{
			CoverageAnalysisDataSet dataSet = new CoverageAnalysisDataSet();

			try
			{
                using (SqlConnection conn = new SqlConnection(DataAccessLayer.ConnectionString.ToString()))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand(SqlConstants.CoverageAnalysis, conn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandTimeout = 3000;

                    cmd.Parameters.Add("@lowerDate", SqlDbType.DateTime);
                    cmd.Parameters["@lowerDate"].Value = startDate;
                    cmd.Parameters.Add("@upperDate", SqlDbType.DateTime);
                    cmd.Parameters["@upperDate"].Value = endDate;
                    cmd.Parameters.Add("@merchandisersCnt", SqlDbType.Int);
                    cmd.Parameters["@merchandisersCnt"].Value = merchList.Count;
                    cmd.Parameters.Add("@merchandisersIDs", SqlDbType.VarChar);
                    cmd.Parameters["@merchandisersIDs"].Value = GetMerchandiserIDs(merchList);
                    cmd.Parameters.Add("@supervisorId", SqlDbType.Int);
                    cmd.Parameters["@supervisorId"].Value = DBNull.Value;

                    Dictionary<string, string> SupervisorNames = new Dictionary<string, string>();
                    Dictionary<string, string> MerchandiserNames = new Dictionary<string, string>();
                    Dictionary<string, string> OutletTypes = new Dictionary<string, string>();
                    Dictionary<string, string> OutletSubtypes = new Dictionary<string, string>();
                    Dictionary<string, string> OutletNames = new Dictionary<string, string>();
                    Dictionary<string, string> OutletDeliveryAddresss = new Dictionary<string, string>();
                    Dictionary<string, string> OutletExternalCodes = new Dictionary<string, string>();
                    Dictionary<string, string> ProductGroupNames = new Dictionary<string, string>();
                    Dictionary<string, string> ProductTypeNames = new Dictionary<string, string>();
                    Dictionary<string, string> UnitNames = new Dictionary<string, string>();
                    Dictionary<string, string> PackingTypes = new Dictionary<string, string>();
                    Dictionary<string, string> ProductCnNames = new Dictionary<string, string>();
                    Dictionary<string, string> ProximityFactors = new Dictionary<string, string>();

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        DataTable t = dataSet.Tables[ReportName];
                        t.MinimumCapacity = 100000;

                        while (reader.Read())
                        {
                            DataRow row;
                            row = t.NewRow();

                            row[FieldNames.SupervisorName] = SupervisorNames.AddStringValue(reader.GetString(0));
                            row[FieldNames.MerchandiserName] = MerchandiserNames.AddStringValue(reader.GetString(1));
                            if (!reader.IsDBNull(2))
                            {
                                row[FieldNames.OutletVisitedNumber] = reader.GetInt64(2);
                            }

                            if (!reader.IsDBNull(3))
                            {
                                row[FieldNames.OutletActiveNumber] = reader.GetInt64(3);
                            }

                            if (!reader.IsDBNull(4))
                            {
                                row[FieldNames.OutletHasRemains] = reader.GetInt64(4);
                            }

                            if (!reader.IsDBNull(5))
                            {
                                row[FieldNames.OLSchedule] = reader.GetInt64(5);
                            }

                            row[FieldNames.OutletID] = reader.GetInt64(6);
                            row[FieldNames.OutletExternalCode] = OutletExternalCodes.AddStringValue(reader.GetString(7));
                            row[FieldNames.OutletType] = OutletTypes.AddStringValue(reader.GetString(8));
                            row[FieldNames.OutletSubtype] = OutletSubtypes.AddStringValue(reader.GetString(9));
                            row[FieldNames.OutletName] = OutletNames.AddStringValue(reader.GetString(10));
                            row[FieldNames.OutletDeliveryAddress] = OutletDeliveryAddresss.AddStringValue(reader.GetString(11));
                            if (!reader.IsDBNull(12))
                            {
                                row[FieldNames.OutletCardDate] = reader.GetDateTime(12);
                            }

                            if (!reader.IsDBNull(13))
                            {
                                row[FieldNames.ProductGroupName] = ProductGroupNames.AddStringValue(reader.GetString(13));
                            }

                            if (!reader.IsDBNull(14))
                            {
                                row[FieldNames.ProductTypeName] = ProductTypeNames.AddStringValue(reader.GetString(14));
                            }

                            if (!reader.IsDBNull(15))
                            {
                                row[FieldNames.UnitName] = UnitNames.AddStringValue(reader.GetString(15));
                            }

                            if (!reader.IsDBNull(16))
                            {
                                row[FieldNames.PackingCapacity] = reader.GetDecimal(16);
                            }

                            if (!reader.IsDBNull(17))
                            {
                                row[FieldNames.PackingType] = PackingTypes.AddStringValue(reader.GetString(17));
                            }

                            if (!reader.IsDBNull(18))
                            {
                                row[FieldNames.ProductCnName] = ProductCnNames.AddStringValue(reader.GetString(18));
                            }

                            if (!reader.IsDBNull(19))
                            {
                                row[FieldNames.ProximityFactor] = ProximityFactors.AddStringValue(reader.GetString(19));
                            }

                            if (reader.GetInt32(20) == 1)
                            {
                                row[FieldNames.BelongingTOPSKU] = true;
                            }
                            else
                            {
                                row[FieldNames.BelongingTOPSKU] = false;
                            }
                            
                            if (!reader.IsDBNull(21))
                            {
                                row[FieldNames.OlCompletedPlan] = reader.GetInt64(21);
                            }

                            t.Rows.Add(row);
                        }

                        t.AcceptChanges();

                        reader.Close();
                    }
                }
			}
            catch (OutOfMemoryException)
            {
                dataSet.Dispose();
                throw;
            }
            catch (SystemException ex)
			{
				throw new SystemException(string.Format(Resource.ReportBuildingError, ReportName, ex.Message));
			}

			return dataSet.ReportEntries;
		}

		#endregion

		DataColumnMapping BelongingTOPSKU = new DataColumnMapping("BelongingTOPSKU", FieldNames.BelongingTOPSKU);
		DataColumnMapping OlCompletedPlan = new DataColumnMapping("OlCompletedPlan", FieldNames.OlCompletedPlan);

		#region Private methods

		/// <summary>
		/// Gets the column mapping between the source table and the dataset.
		/// </summary>
		/// <returns></returns>
		protected override DataTableMapping GetMapping()
		{
			DataTableMapping map = new DataTableMapping();
			map.SourceTable = ReportName;
			map.DataSetTable = ReportName;

			map.ColumnMappings.AddRange(new DataColumnMapping[]{
                Supervisor_name,MerchName, OLtype_name,OLSubTypeName,
                OLTradingName, OLDeliveryAddress,
                OL_id,OL_Code,
                ProdGroupName,
                ProdTypeName, Unit_Name,
                Packing_Capacity, Packing_Type, ProductCnName, OLCardDate,
                ProximityFactor, OLSchedule, OlCompletedPlan, BelongingTOPSKU });
			map.ColumnMappings.Add("OL_Visited", FieldNames.OutletVisitedNumber);
			map.ColumnMappings.Add("OL_IsActive", FieldNames.OutletActiveNumber);
			map.ColumnMappings.Add("OL_HasRemains", FieldNames.OutletHasRemains);
			return map;
		}

		#endregion
	}
}
