﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Logica.Reports.DataProvider.Entities;
using System.Data.SqlClient;
using Logica.Reports.Localization;
using System.Configuration;
using System.Data.Common;
using Logica.Reports.Common;
using Logica.Reports.ModelDataProvider.Entities;
using Logica.Reports.DataAccess;
using System.Data;
using SoftServe.Reports.SVReport;

namespace Logica.Reports.DataProvider.Providers
{
    public class OutletBaseDataProvider : DataProviderBase
    {
        #region Public properties

        /// <summary>
        /// Gets the current report name.
        /// </summary>
        public override string ReportName
        {
            get
            {
                return "OutletBase";
            }
        }

        #endregion

        #region Public methods

        /// <summary>
        /// Gets the report data.
        /// </summary>
        /// <param name="startDate">The report start date.</param>
        /// <param name="endDate">The report end date.</param>
        /// <returns></returns>
        public OutletBaseDataSet.ReportEntriesDataTable GetReportData(DateTime startDate, DateTime endDate, List<Merchandiser> merchList)
        {
            OutletBaseDataSet dataSet = new OutletBaseDataSet();

            try
            {

                using (SqlConnection conn = new SqlConnection(DataAccessLayer.ConnectionString.ToString()))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand(SqlConstants.OutletBase, conn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandTimeout = 3000;

                    //cmd.Parameters.Add("@lowerDate", SqlDbType.DateTime);
                    //cmd.Parameters["@lowerDate"].Value = startDate;
                    //cmd.Parameters.Add("@upperDate", SqlDbType.DateTime);
                    //cmd.Parameters["@upperDate"].Value = endDate;
                    cmd.Parameters.Add("@merchandisersCnt", SqlDbType.Int);
                    cmd.Parameters["@merchandisersCnt"].Value = merchList.Count;
                    cmd.Parameters.Add("@merchandisersIDs", SqlDbType.VarChar);
                    cmd.Parameters["@merchandisersIDs"].Value = GetMerchandiserIDs(merchList);
                    cmd.Parameters.Add("@supervisorId", SqlDbType.Int);
                    cmd.Parameters["@supervisorId"].Value = DBNull.Value;

                    Dictionary<string, string> OutletExternalCodes = new Dictionary<string, string>();
                    Dictionary<string, string> OutletSubtypes = new Dictionary<string, string>();
                    Dictionary<string, string> OutletTypes = new Dictionary<string, string>();
                    Dictionary<string, string> OutletNames = new Dictionary<string, string>();
                    Dictionary<string, string> OutletDeliveryAddresss = new Dictionary<string, string>();
                    Dictionary<string, string> OutletTradingNames = new Dictionary<string, string>();
                    Dictionary<string, string> OutletAddresss = new Dictionary<string, string>();
                    Dictionary<string, string> OutletStatuss = new Dictionary<string, string>();
                    Dictionary<string, string> DistrictNames = new Dictionary<string, string>();
                    Dictionary<string, string> CityNames = new Dictionary<string, string>();
                    Dictionary<string, string> OutletDirectors = new Dictionary<string, string>();
                    Dictionary<string, string> ProximityFactors = new Dictionary<string, string>();
                    Dictionary<string, string> OutletPhones = new Dictionary<string, string>();
                    Dictionary<string, string> OutletFaxs = new Dictionary<string, string>();
                    Dictionary<string, string> OutletEmails = new Dictionary<string, string>();
                    Dictionary<string, string> OutletPurchaseManagers = new Dictionary<string, string>();
                    Dictionary<string, string> OutletMarketingManagers = new Dictionary<string, string>();
                    Dictionary<string, string> OutletMarketingManagerPhones = new Dictionary<string, string>();
                    Dictionary<string, string> OutletAccountants = new Dictionary<string, string>();
                    Dictionary<string, string> OutletAccountantPhones = new Dictionary<string, string>();
                    Dictionary<string, string> ContractNumbers = new Dictionary<string, string>();
                    Dictionary<string, string> OutletAccounts = new Dictionary<string, string>();
                    Dictionary<string, string> BankCodes = new Dictionary<string, string>();
                    Dictionary<string, string> BankNames = new Dictionary<string, string>();
                    Dictionary<string, string> BankAddresss = new Dictionary<string, string>();
                    Dictionary<string, string> ZKPOs = new Dictionary<string, string>();
                    Dictionary<string, string> VATNs = new Dictionary<string, string>();
                    Dictionary<string, string> IPNs = new Dictionary<string, string>();
                    Dictionary<string, string> AreaInCitys = new Dictionary<string, string>();
                    Dictionary<string, string> SupervisorNames = new Dictionary<string, string>();
                    Dictionary<string, string> MerchandiserNames = new Dictionary<string, string>();
                    
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        DataTable t = dataSet.Tables[ReportName];
                        t.MinimumCapacity = 100000;

                        while (reader.Read())
                        {
                            DataRow row;
                            row = t.NewRow();

                            row[FieldNames.OutletID] = reader.GetInt64(0);
                            row[FieldNames.OutletExternalCode] = OutletExternalCodes.AddStringValue(reader.GetString(1));
                            row[FieldNames.OutletSubtype] = OutletSubtypes.AddStringValue(reader.GetString(2));
                            row[FieldNames.OutletType] = OutletTypes.AddStringValue(reader.GetString(3));
                            row[FieldNames.OutletName] = OutletNames.AddStringValue(reader.GetString(4));
                            row[FieldNames.OutletDeliveryAddress] = OutletDeliveryAddresss.AddStringValue(reader.GetString(5));
                            row[FieldNames.OutletTradingName] = OutletTradingNames.AddStringValue(reader.GetString(6));
                            row[FieldNames.OutletAddress] = OutletAddresss.AddStringValue(reader.GetString(7));
                            row[FieldNames.OutletStatus] = OutletStatuss.AddStringValue(reader.GetString(8));
                            row[FieldNames.DistrictName] = DistrictNames.AddStringValue(reader.GetString(9));
                            row[FieldNames.CityName] = CityNames.AddStringValue(reader.GetString(10));
                            row[FieldNames.OutletDirector] = OutletDirectors.AddStringValue(reader.GetString(11));
                            row[FieldNames.PrevMonthBottledSales] = reader.GetDecimal(12);
                            row[FieldNames.PrevMonthKegSales] = reader.GetDecimal(13);
                            row[FieldNames.CurMonthBottledSales] = reader.GetDecimal(14);
                            row[FieldNames.CurMonthKegSales] = reader.GetDecimal(15);
                            row[FieldNames.FridgeNumber] = reader.GetInt32(16);
                            row[FieldNames.CoolerNumber] = reader.GetInt32(17);
                            row[FieldNames.PlanedVisitsCount] = reader.GetInt32(18);
                            row[FieldNames.TotalVisitsCount] = reader.GetInt32(19);
                            if (!reader.IsDBNull(20))
                            {
                                row[FieldNames.ProximityFactor] = ProximityFactors.AddStringValue(reader.GetString(20));
                            }
                            row[FieldNames.OutletPhone] = OutletPhones.AddStringValue(reader.GetString(21));
                            row[FieldNames.OutletFax] = OutletFaxs.AddStringValue(reader.GetString(22));
                            row[FieldNames.OutletEmail] = OutletEmails.AddStringValue(reader.GetString(23));
                            row[FieldNames.OutletPurchaseManager] = OutletPurchaseManagers.AddStringValue(reader.GetString(24));
                            row[FieldNames.OutletMarketingManager] = OutletMarketingManagers.AddStringValue(reader.GetString(25));
                            row[FieldNames.OutletMarketingManagerPhone] = OutletMarketingManagerPhones.AddStringValue(reader.GetString(26));
                            row[FieldNames.OutletAccountant] = OutletAccountants.AddStringValue(reader.GetString(27));
                            row[FieldNames.OutletAccountantPhone] = OutletAccountantPhones.AddStringValue(reader.GetString(28));
                            if (!reader.IsDBNull(29))
                            {
                                row[FieldNames.ContractNumber] = ContractNumbers.AddStringValue(reader.GetString(29));
                            }
                            if (!reader.IsDBNull(30))
                            {
                                row[FieldNames.ContractDate] = reader.GetDateTime(30);
                            }
                            row[FieldNames.OutletAccount] = OutletAccounts.AddStringValue(reader.GetString(31));
                            row[FieldNames.BankCode] = BankCodes.AddStringValue(reader.GetString(32));
                            row[FieldNames.BankName] = BankNames.AddStringValue(reader.GetString(33));
                            row[FieldNames.BankAddress] = BankAddresss.AddStringValue(reader.GetString(34));
                            row[FieldNames.ZKPO] = ZKPOs.AddStringValue(reader.GetString(35));
                            row[FieldNames.VATN] = VATNs.AddStringValue(reader.GetString(36));
                            row[FieldNames.IPN] = IPNs.AddStringValue(reader.GetString(37));
                            if (!reader.IsDBNull(38))
                            {
                                row[FieldNames.OutletSize] = reader.GetDecimal(38);
                            }
                            if (!reader.IsDBNull(39))
                            {
                                row[FieldNames.OutletWarehouseSize] = reader.GetDecimal(39);
                            }
                            row[FieldNames.AreaInCity] = AreaInCitys.AddStringValue(reader.GetString(40));
                            row[FieldNames.SupervisorName] = SupervisorNames.AddStringValue(reader.GetString(41));
                            row[FieldNames.MerchandiserName] = MerchandiserNames.AddStringValue(reader.GetString(42));
                            
                            t.Rows.Add(row);
                        }

                        t.AcceptChanges();

                        reader.Close();
                    }
                }
            }
            catch (OutOfMemoryException)
            {
                dataSet.Dispose();
                throw;
            }
            catch (SystemException ex)
            {
                throw new SystemException(string.Format(Resource.ReportBuildingError, ReportName, ex.Message));
            }

            return dataSet.ReportEntries;
        }

        #endregion

        #region Private methods

        /// <summary>
        /// Gets the column mapping between the source table and the dataset.
        /// </summary>
        /// <returns></returns>
        protected override DataTableMapping GetMapping()
        {
            DataTableMapping map = new DataTableMapping();
            map.SourceTable = ReportName;
            map.DataSetTable = ReportName;

            map.ColumnMappings.AddRange(new DataColumnMapping[]{
                OL_id, OL_Code, OLSubTypeName,OLtype_name, 
                OLName, OLDeliveryAddress,OLTradingName,OLAddress,
                OutletsSatus, District_Name, City_Name, Supervisor_name,
                MerchName, Area_Name, ProximityFactor});
            map.ColumnMappings.Add("OLDirector", FieldNames.OutletDirector);
            map.ColumnMappings.Add("PreviousBottledSales", FieldNames.PrevMonthBottledSales);
            map.ColumnMappings.Add("PreviousKegSales", FieldNames.PrevMonthKegSales);
            map.ColumnMappings.Add("CurrentBottledSales", FieldNames.CurMonthBottledSales);
            map.ColumnMappings.Add("CurrentKegSales", FieldNames.CurMonthKegSales);
            map.ColumnMappings.Add("FridgeNumber", FieldNames.FridgeNumber);
            map.ColumnMappings.Add("PlanedVisitsCount", FieldNames.PlanedVisitsCount);
            map.ColumnMappings.Add("TotalVisitsCount", FieldNames.TotalVisitsCount);
            map.ColumnMappings.Add("OLTelephone", FieldNames.OutletPhone);
            map.ColumnMappings.Add("OLFax", FieldNames.OutletFax);
            map.ColumnMappings.Add("OLEmail", FieldNames.OutletEmail);
            map.ColumnMappings.Add("OLPurchManager", FieldNames.OutletPurchaseManager);
            map.ColumnMappings.Add("OLMarkManager", FieldNames.OutletMarketingManager);
            map.ColumnMappings.Add("OLMarkManagerPhone", FieldNames.OutletMarketingManagerPhone);
            map.ColumnMappings.Add("OLAccountant", FieldNames.OutletAccountant);
            map.ColumnMappings.Add("OLAccountantPhone", FieldNames.OutletAccountantPhone);
            map.ColumnMappings.Add("ContractNumber", FieldNames.ContractNumber);
            map.ColumnMappings.Add("ContractDate", FieldNames.ContractDate);
            map.ColumnMappings.Add("RR", FieldNames.OutletAccount);
            map.ColumnMappings.Add("BankCode", FieldNames.BankCode);
            map.ColumnMappings.Add("BankName", FieldNames.BankName);
            map.ColumnMappings.Add("BankAddress", FieldNames.BankAddress);
            map.ColumnMappings.Add("ZKPO", FieldNames.ZKPO);
            map.ColumnMappings.Add("VATN", FieldNames.VATN);
            map.ColumnMappings.Add("IPN", FieldNames.IPN);
            map.ColumnMappings.Add("OLSize", FieldNames.OutletSize);
            map.ColumnMappings.Add("OLWHSize", FieldNames.OutletWarehouseSize);
            map.ColumnMappings.Add("CoolerNumber", FieldNames.CoolerNumber);

            
            

            return map;
        }

        #endregion
    }
}
