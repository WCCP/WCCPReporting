﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using System.Collections;
using System.IO;
using System.Data.Common;
using Logica.Reports.Localization;
using Logica.Reports.Common;
using Logica.Reports.ModelDataProvider.Entities;

namespace Logica.Reports.DataProvider.Providers
{
    public abstract class DataProviderBase: IDisposable
    {
        #region Private fields

        /// <summary>
        /// Stores the current SQLConnection.
        /// </summary>
        private SqlConnection _databaseConnection;

        #endregion

        #region Protected properties

        /// <summary>
        /// Gets the database connection.
        /// </summary>
        /// <value>The database connection.</value>
        protected SqlConnection DatabaseConnection
        {
            get
            {
                if (_databaseConnection == null)
                {
                    _databaseConnection = new SqlConnection(SettingProvider.ConnectionString);
                }
                return _databaseConnection;
            }
        }

        /// <summary>
        /// Gets the name of the report.
        /// </summary>
        /// <value>The name of the report.</value>
        public abstract string ReportName
        {
            get;
        }

        #endregion

        #region Protected methods

        /// <summary>
        /// Executes plain text SQL.
        /// </summary>
        /// <param name="sqlCommand">The SQL command.</param>
        /// <param name="parameters">The parameters.</param>
        /// <returns></returns>
        protected SqlDataAdapter ExecuteAdapterCommand(string sqlCommand, Dictionary<string, object> parameters)
        {
            return ExecuteAdapterCommand(sqlCommand, parameters, CommandType.Text);
        }

        /// <summary>
        /// Executes the stored procedure.
        /// </summary>
        /// <param name="spName">Name of the stored procedure.</param>
        /// <param name="parameters">The SQL parameters.</param>
        /// <returns></returns>
        protected SqlDataAdapter ExecuteAdapterStoredProcedure(string spName, Dictionary<string, object> parameters)
        {
            return ExecuteAdapterCommand(spName, parameters, CommandType.StoredProcedure);
        }

        /// <summary>
        /// Gets the column mapping between the source table and the dataset.
        /// </summary>
        /// <returns>Column mapping.</returns>
        protected abstract DataTableMapping GetMapping();

        #endregion

        #region Helper methods

        /// <summary>
        /// Executes SQL command.
        /// </summary>
        /// <param name="sqlCommand">The SQL command.</param>
        /// <param name="parameters">The parameters.</param>
        /// <param name="commandType">Type of the command.</param>
        /// <returns></returns>
        private SqlDataAdapter ExecuteAdapterCommand(string sqlCommand, Dictionary<string, object> parameters, System.Data.CommandType commandType)
        {
            SqlDataAdapter adapter = null;
            
            SqlCommand command = new SqlCommand(sqlCommand, DatabaseConnection);
            command.CommandType = commandType;
            command.CommandTimeout = 3000;
            foreach (string key in parameters.Keys)
            {
                command.Parameters.AddWithValue(key, parameters[key]);
            }
            try
            {
                adapter = new SqlDataAdapter(command);
                adapter.MissingMappingAction = MissingMappingAction.Error;
            }
            catch (SqlException)
            {
                throw;
            }
            finally
            {
                DatabaseConnection.Close();
            }
            return adapter;
        }
        #endregion

        #region IDisposable Members

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            _databaseConnection.Close();
        }

        #endregion

        protected static string GetMerchandiserIDs(List<Merchandiser> list)
        {
            StringBuilder sb = new StringBuilder();
            foreach (Merchandiser m in list)
            {
                sb.Append(m.Merch_id.ToString().PadLeft(10, ' ') + ",");
            }
            return sb.ToString().TrimEnd(',');
        }

        protected DataColumnMapping Supervisor_name = new DataColumnMapping("Supervisor_name", FieldNames.SupervisorName);
        protected DataColumnMapping MerchName = new DataColumnMapping("MerchName", FieldNames.MerchandiserName);
        protected DataColumnMapping OLtype_name = new DataColumnMapping("OLtype_name", FieldNames.OutletType);
        protected DataColumnMapping OLSubTypeName = new DataColumnMapping("OLSubTypeName", FieldNames.OutletSubtype);
        protected DataColumnMapping OLName = new DataColumnMapping("OLTradingName", FieldNames.OutletTradingName);
        protected DataColumnMapping OLDeliveryAddress = new DataColumnMapping("OLDeliveryAddress", FieldNames.OutletDeliveryAddress);
        protected DataColumnMapping OLTradingName = new DataColumnMapping("OLName", FieldNames.OutletName);
        protected DataColumnMapping OLAddress = new DataColumnMapping("OLAddress", FieldNames.OutletAddress);
        protected DataColumnMapping City_Name = new DataColumnMapping("City_Name", FieldNames.CityName);
        protected DataColumnMapping District_Name = new DataColumnMapping("District_Name", FieldNames.DistrictName);
        protected DataColumnMapping OL_id = new DataColumnMapping("OL_id", FieldNames.OutletID);
        protected DataColumnMapping OL_Code = new DataColumnMapping("OL_Code", FieldNames.OutletExternalCode);
        protected DataColumnMapping OutletsSatus = new DataColumnMapping("OutletsSatus", FieldNames.OutletStatus);
        protected DataColumnMapping Region_Name = new DataColumnMapping("Region_Name", FieldNames.RegionName);
        protected DataColumnMapping Merch_Id = new DataColumnMapping("Merch_Id", FieldNames.MerchandiserID);
        protected DataColumnMapping MerchandiserStatus = new DataColumnMapping("MerchandiserStatus", FieldNames.MerchandiserStatus);
        protected DataColumnMapping Product_Id = new DataColumnMapping("Product_Id", FieldNames.ProductID);
        protected DataColumnMapping ProductName = new DataColumnMapping("ProductName", FieldNames.ProductName);
        protected DataColumnMapping ProdGroupName = new DataColumnMapping("ProdGroupName", FieldNames.ProductGroupName);
        protected DataColumnMapping ProdTypeName = new DataColumnMapping("ProdTypeName", FieldNames.ProductTypeName);
        protected DataColumnMapping Area_Name = new DataColumnMapping("Area_Name", FieldNames.AreaName);
        protected DataColumnMapping Territory_Type = new DataColumnMapping("Territory_Type", FieldNames.TerritoryType);
        protected DataColumnMapping Unit_Name = new DataColumnMapping("Unit_Name", FieldNames.UnitName);
        protected DataColumnMapping Packing_Capacity = new DataColumnMapping("Packing_Capacity", FieldNames.PackingCapacity);
        protected DataColumnMapping Packing_Type = new DataColumnMapping("Packing_Type", FieldNames.PackingType);
        protected DataColumnMapping ProductCnName = new DataColumnMapping("ProductCnName", FieldNames.ProductCnName);
        protected DataColumnMapping MerchStatus = new DataColumnMapping("MerchStatus", FieldNames.MerchandiserStatus);
        protected DataColumnMapping Cust_Name = new DataColumnMapping("Cust_Name", FieldNames.CustomerName);
        protected DataColumnMapping OLCardDate = new DataColumnMapping("OLCardDate", FieldNames.OutletCardDate);
        protected DataColumnMapping Comments = new DataColumnMapping("Comments", FieldNames.Comments);
        protected DataColumnMapping ProximityFactor = new DataColumnMapping("ProximityFactor", FieldNames.ProximityFactor);
		protected DataColumnMapping Population = new DataColumnMapping("Population", FieldNames.Population);
		protected DataColumnMapping OLSchedule = new DataColumnMapping("OLSchedule", FieldNames.OLSchedule);
		
	}
}
