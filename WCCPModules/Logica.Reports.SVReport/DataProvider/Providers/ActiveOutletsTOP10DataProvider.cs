﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.Data.Common;
using System.Configuration;
using Logica.Reports.DataProvider.Entities;
using Logica.Reports.Localization;
using Logica.Reports.Common;
using Logica.Reports.ModelDataProvider.Entities;
using SoftServe.Reports.SVReport;
using Logica.Reports.DataAccess;

namespace Logica.Reports.DataProvider.Providers
{
    public class ActiveOutletsTOP10DataProvider : DataProviderBase
    {
        #region Public properties

        /// <summary>
        /// Gets the current report name.
        /// </summary>
        public override string ReportName
        {
            get
            {
                return "ActiveOutletsTOP10";
            }
        }

        #endregion

        #region Public methods

        /// <summary>
        /// Gets the report data.
        /// </summary>
        /// <param name="startDate">The report start date.</param>
        /// <param name="endDate">The report end date.</param>
        /// <returns></returns>
        public ActiveOutletsTOP10DataSet.ReportEntriesDataTable GetReportData(DateTime startDate, DateTime endDate, List<Merchandiser> merchList)
        {
            ActiveOutletsTOP10DataSet dataSet = new ActiveOutletsTOP10DataSet();
           
            
            try
            {

                using (SqlConnection conn = new SqlConnection(DataAccessLayer.ConnectionString.ToString()))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand(SqlConstants.ActiveOutletsTOP10, conn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandTimeout = 3000;

                    Dictionary<string, object> sqlParameters = new Dictionary<string, object>();
                    sqlParameters.Add("@lowerDate", startDate);
                    sqlParameters.Add("@upperDate", endDate);
                    sqlParameters.Add("@merchandisersCnt", merchList.Count);
                    sqlParameters.Add("@merchandisersIDs", GetMerchandiserIDs(merchList));
                    sqlParameters.Add("@supervisorId", DBNull.Value);
                    SqlDataAdapter adapter = ExecuteAdapterCommand(SqlConstants.ActiveOutletsTOP10, sqlParameters);
                    adapter.TableMappings.Add(GetMapping());
                    adapter.Fill(dataSet, ReportName);
                }
            }
            catch (OutOfMemoryException)
            {
                dataSet.Dispose();
                throw;
            }
            catch (SystemException ex)
            {
                throw new SystemException(string.Format(Resource.ReportBuildingError, ReportName, ex.Message)); 
            }

            return dataSet.ReportEntries;
        }

        #endregion

        #region Private methods

        /// <summary>
        /// Gets the column mapping between the source table and the dataset.
        /// </summary>
        /// <returns></returns>
        protected override DataTableMapping GetMapping()
        {
            DataTableMapping map = new DataTableMapping();
            map.SourceTable = ReportName;
            map.DataSetTable = ReportName;

            map.ColumnMappings.AddRange(new DataColumnMapping[]{
                Supervisor_name,MerchName, OLtype_name,OLSubTypeName,
                OLName, OLDeliveryAddress,OLTradingName, OLAddress,
                City_Name,District_Name,OL_id,OL_Code,
                OutletsSatus, Cust_Name, Region_Name, Merch_Id,
                MerchandiserStatus,  Area_Name, Territory_Type, ProximityFactor});

            map.ColumnMappings.Add("OL_Has", FieldNames.OutletHas);
            
            return map;
        }

        #endregion
    }
}
