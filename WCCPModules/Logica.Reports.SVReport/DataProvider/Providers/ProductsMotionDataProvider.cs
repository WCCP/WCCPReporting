﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using System.Data.Common;
using Logica.Reports.DataAccess;
using Logica.Reports.DataProvider.Entities;
using Logica.Reports.Localization;
using Logica.Reports.ModelDataProvider.Entities;
using SoftServe.Reports.SVReport;

namespace Logica.Reports.DataProvider.Providers
{
    public class ProductsMotionDataProvider : DataProviderBase
    {
        #region Public properties

        /// <summary>
        /// Gets the current report name.
        /// </summary>
        public override string ReportName
        {
            get
            {
                return "ProductsMotion";
            }
        }

        #endregion

        #region Public methods

        /// <summary>
        /// Gets the report data.
        /// </summary>
        /// <param name="startDate">The report start date.</param>
        /// <param name="endDate">The report end date.</param>
        /// <returns></returns>
        public ProductsMotionDataSet.ReportEntriesDataTable GetReportData(DateTime startDate, DateTime endDate, List<Merchandiser> merchList)
        {
            ProductsMotionDataSet dataSet = new ProductsMotionDataSet();

            try
            {
                using (SqlConnection conn = new SqlConnection(DataAccessLayer.ConnectionString.ToString()))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand(SqlConstants.MotionOfProducts, conn);
                    cmd.CommandType = CommandType.StoredProcedure;

                //    DataSet ds = DataAccessLayer.ExecuteStoredProcedure(SqlConstants.MotionOfProducts,
                //new[]
                //    {
                //            new SqlParameter(SqlConstants.StartDate, startDate),
                //            new SqlParameter(SqlConstants.EndDate, endDate),
                //            new SqlParameter(SqlConstants.SV_ID,  DBNull.Value),
                //            new SqlParameter(SqlConstants.Merch_ID, GetMerchandiserIDs(merchList)),
                //            new SqlParameter(SqlConstants.Merch_Count ,  merchList.Count)
                        
                //    }
                //);


                    cmd.CommandTimeout = 3000;

                    cmd.Parameters.Add(SqlConstants.StartDate, SqlDbType.DateTime);
                    cmd.Parameters[SqlConstants.StartDate].Value = startDate;
                    cmd.Parameters.Add(SqlConstants.EndDate, SqlDbType.DateTime);
                    cmd.Parameters[SqlConstants.EndDate].Value = endDate;
                    cmd.Parameters.Add(SqlConstants.Merch_Count, SqlDbType.Int);
                    cmd.Parameters[SqlConstants.Merch_Count].Value = merchList.Count;
                    cmd.Parameters.Add(SqlConstants.Merch_ID, SqlDbType.VarChar);
                    cmd.Parameters[SqlConstants.Merch_ID].Value = GetMerchandiserIDs(merchList);
                    cmd.Parameters.Add(SqlConstants.SV_ID, SqlDbType.Int);
                    cmd.Parameters[SqlConstants.SV_ID].Value = DBNull.Value;

                    Dictionary<string, string> ProductCnNames = new Dictionary<string, string>();
                    Dictionary<string, string> ProximityFactors = new Dictionary<string, string>();
                    Dictionary<string, string> SupervisorNames = new Dictionary<string, string>();
                    Dictionary<string, string> MerchandiserNames = new Dictionary<string, string>();
                    Dictionary<string, string> OutletTypes = new Dictionary<string, string>();
                    Dictionary<string, string> OutletSubTypes = new Dictionary<string, string>();
                    Dictionary<string, string> OutletNames = new Dictionary<string, string>();
                    Dictionary<string, string> OutletDeliveryAddresss = new Dictionary<string, string>();
                    Dictionary<string, string> OutletTradingNames = new Dictionary<string, string>();
                    Dictionary<string, string> OutletAddresss = new Dictionary<string, string>();
                    Dictionary<string, string> OutletExternalCodes = new Dictionary<string, string>();
                    Dictionary<string, string> OutletStatuss = new Dictionary<string, string>();
                    Dictionary<string, string> DistrictNames = new Dictionary<string, string>();
                    Dictionary<string, string> CityNames = new Dictionary<string, string>();
                    Dictionary<string, string> CustomerNames = new Dictionary<string, string>();
                    Dictionary<string, string> RegionNames = new Dictionary<string, string>();
                    Dictionary<string, string> MerchandiserStatuss = new Dictionary<string, string>();
                    Dictionary<string, string> ProductNames = new Dictionary<string, string>();
                    Dictionary<string, string> ProductGroupNames = new Dictionary<string, string>();
                    Dictionary<string, string> ProductTypeNames = new Dictionary<string, string>();
                    Dictionary<string, string> AreaNames = new Dictionary<string, string>();
                    Dictionary<string, string> TerritoryTypes = new Dictionary<string, string>();
                    Dictionary<string, string> UnitNames = new Dictionary<string, string>();
                    Dictionary<string, string> PackingTypes = new Dictionary<string, string>();
                    Dictionary<string, string> Populations = new Dictionary<string, string>();                    
                   
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        DataTable t = dataSet.Tables[ReportName];
                        t.MinimumCapacity = 100000;

                        while (reader.Read())
                        {
                            DataRow row;
                            row = t.NewRow();

                            row[FieldNames.CustomerName] = CustomerNames.AddStringValue(reader.GetString(0));
                            row[FieldNames.SupervisorName] = SupervisorNames.AddStringValue(reader.GetString(1));
                            row[FieldNames.CityName] = CityNames.AddStringValue(reader.GetString(2));
                            row[FieldNames.DistrictName] = DistrictNames.AddStringValue(reader.GetString(3));
                            if (!reader.IsDBNull(4))
                            {
                                row[FieldNames.RegionName] = RegionNames.AddStringValue(reader.GetString(4));
                            }

                            row[FieldNames.AreaName] = AreaNames.AddStringValue(reader.GetString(5));
                            if (!reader.IsDBNull(6))
                            {
                                row[FieldNames.TerritoryType] = TerritoryTypes.AddStringValue(reader.GetString(6));
                            }
                            row[FieldNames.MerchandiserName] = MerchandiserNames.AddStringValue(reader.GetString(7));
                            row[FieldNames.MerchandiserStatus] = MerchandiserStatuss.AddStringValue(reader.GetString(8));
                            row[FieldNames.OrderDate] = reader.GetDateTime(9);
                            row[FieldNames.OrderedProductVolume] = reader.GetDecimal(10);                            
                            row[FieldNames.SaledOrderedProductVolume] = reader.GetDecimal(12);                            
                            row[FieldNames.ToTakeOffProductVolume] = reader.GetDecimal(14);
                            row[FieldNames.RemainsProductVolume] = reader.GetDecimal(15);
                            row[FieldNames.WithoutOrder] = reader.GetDecimal(16);                            
                            row[FieldNames.OverOrdered] = reader.GetDecimal(17);
                            row[FieldNames.OutletID] = reader.GetInt64(18);
                            row[FieldNames.OutletExternalCode] = OutletExternalCodes.AddStringValue(reader.GetString(19));                            
                            row[FieldNames.OutletStatus] = OutletStatuss.AddStringValue(reader.GetString(20));
                            row[FieldNames.OutletType] = OutletTypes.AddStringValue(reader.GetString(21));
                            row[FieldNames.OutletSubtype] = OutletSubTypes.AddStringValue(reader.GetString(22));
                            row[FieldNames.OutletName] = OutletNames.AddStringValue(reader.GetString(23));
                            row[FieldNames.OutletDeliveryAddress] = OutletDeliveryAddresss.AddStringValue(reader.GetString(24));
                            row[FieldNames.OutletTradingName] = OutletTradingNames.AddStringValue(reader.GetString(25));
                            row[FieldNames.OutletAddress] = OutletAddresss.AddStringValue(reader.GetString(26));
                            row[FieldNames.ProductID] = reader.GetInt32(27);
                            if (!reader.IsDBNull(28))
                                row[FieldNames.ProductName] = ProductNames.AddStringValue(reader.GetString(28));
                            if (!reader.IsDBNull(29))
                                row[FieldNames.ProductGroupName] = ProductGroupNames.AddStringValue(reader.GetString(29));
                            if (!reader.IsDBNull(30))
                                row[FieldNames.ProductTypeName] = ProductTypeNames.AddStringValue(reader.GetString(30));

                            if (!reader.IsDBNull(31))
                                row[FieldNames.UnitName] = UnitNames.AddStringValue(reader.GetString(31));

                            if (!reader.IsDBNull(32))
                                row[FieldNames.PackingCapacity] = reader.GetDecimal(32);
                            if (!reader.IsDBNull(33))
                                row[FieldNames.PackingType] = PackingTypes.AddStringValue(reader.GetString(33));

                            if (!reader.IsDBNull(34))
                                row[FieldNames.ProductCnName] = ProductCnNames.AddStringValue(reader.GetString(34));

                            if (!reader.IsDBNull(36))
                                row[FieldNames.ProximityFactor] = ProximityFactors.AddStringValue(reader.GetString(36));

                            if (!reader.IsDBNull(37))
                                row[FieldNames.Population] = Populations.AddStringValue(reader.GetString(37));

                            if (!reader.IsDBNull(39))
                                row[FieldNames.RecomendProductVolume] = reader.GetDecimal(39);
                            if (!reader.IsDBNull(40))
                                row[FieldNames.RecomendProductQty] = reader.GetDecimal(40);

                            if (!reader.IsDBNull(41))
                                row[FieldNames.UseRecommnededOrder] = reader.GetInt32(41);
                            row[FieldNames.VolumeRecommnededOrder] = reader.GetDecimal(10); //=OrderedProductVolume

                            if (!reader.IsDBNull(42))
                                row[FieldNames.IsRecommendedOrder] = reader.GetInt32(42);
                            if (!reader.IsDBNull(43))
                                row[FieldNames.IsTop] = reader.GetInt32(43);

                            if (!reader.IsDBNull(11))
                                row[FieldNames.OrderedProductQty] = reader.GetDecimal(11);

                            if (!reader.IsDBNull(38))
                                row[FieldNames.RemainsProductQty] = reader.GetDecimal(38);                          

                            if (!reader.IsDBNull(45))
                                row[FieldNames.RecOrderCount] = reader.GetInt32(45);                          

                            

                            t.Rows.Add(row);
                        }

                        t.AcceptChanges();

                        reader.Close();
                    }
                }
            }
            catch (OutOfMemoryException)
            {
                dataSet.Dispose();
                throw;
            }
            catch (SystemException ex)
            {
                throw new SystemException(string.Format(Resource.ReportBuildingError, ReportName, ex.Message));
            }

            return dataSet.ReportEntries;
        }

        #endregion

        #region Private methods
        /// <summary>
        /// Gets the column mapping between the source table and the dataset.
        /// </summary>
        /// <returns></returns>
        protected override DataTableMapping GetMapping()
        {
            DataTableMapping map = new DataTableMapping();
            map.SourceTable = ReportName;
            map.DataSetTable = ReportName;

            map.ColumnMappings.AddRange(new DataColumnMapping[]{
                Supervisor_name,MerchName, OLtype_name,OLSubTypeName,
                OLName, OLDeliveryAddress,OLTradingName, OLAddress,
                City_Name,District_Name,OL_id,OL_Code,
                OutletsSatus, Cust_Name, Region_Name, Merch_Id,
                MerchandiserStatus, Product_Id, ProductName, ProdGroupName,
                ProdTypeName, Area_Name, Territory_Type, Unit_Name,
                Packing_Capacity, Packing_Type, ProductCnName, ProximityFactor, Population
            });
            map.ColumnMappings.Add("SaleDate", FieldNames.SaleDate);
            map.ColumnMappings.Add("OrderDate", FieldNames.OrderDate);
            map.ColumnMappings.Add("OrderedProductVolume", FieldNames.OrderedProductVolume);
            map.ColumnMappings.Add("SaledOrderedProductVolume", FieldNames.SaledOrderedProductVolume);
            map.ColumnMappings.Add("ToTakeOffProductVolume", FieldNames.ToTakeOffProductVolume);
            map.ColumnMappings.Add("RemainsProductVolume", FieldNames.RemainsProductVolume);
            map.ColumnMappings.Add("WithoutOrder", FieldNames.WithoutOrder);
            map.ColumnMappings.Add("OverOrdered", FieldNames.OverOrdered);
            //map.ColumnMappings.Add("HLCode", FieldNames.HLCode);

            return map;
        }

        #endregion

    }

}
