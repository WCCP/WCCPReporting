﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.Data.Common;
using System.Configuration;
using Logica.Reports.DataProvider.Entities;
using Logica.Reports.Localization;
using Logica.Reports.Common;
using Logica.Reports.ModelDataProvider.Entities;
using Logica.Reports.DataAccess;
using SoftServe.Reports.SVReport;

namespace Logica.Reports.DataProvider.Providers
{
    public class RoutesByOutletsDataProvider : DataProviderBase
    {
        #region Public properties

        /// <summary>
        /// Gets the current report name.
        /// </summary>
        public override string ReportName
        {
            get
            {
                return "RoutesByOutlets";
            }
        }

        #endregion

        #region Public methods

        /// <summary>
        /// Gets the report data.
        /// </summary>
        /// <param name="startDate">The report start date.</param>
        /// <param name="endDate">The report end date.</param>
        /// <returns></returns>
        public RoutesByOutletsDataSet.ReportEntriesDataTable GetReportData(DateTime startDate, DateTime endDate, List<Merchandiser> merchList)
        {
            RoutesByOutletsDataSet dataSet = new RoutesByOutletsDataSet();

            try
            {

                using (SqlConnection conn = new SqlConnection(DataAccessLayer.ConnectionString.ToString()))
                {
                    conn.Open();

                    SqlCommand cmd = new SqlCommand(SqlConstants.RoutesByOutlets, conn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandTimeout = 3000;

                    //SqlCommand cmd = new SqlCommand(ScriptProvider.RoutesByOutlets, conn);

                    cmd.Parameters.Add("@lowerDate", SqlDbType.DateTime);
                    cmd.Parameters["@lowerDate"].Value = startDate;
                    cmd.Parameters.Add("@upperDate", SqlDbType.DateTime);
                    cmd.Parameters["@upperDate"].Value = endDate;
                    cmd.Parameters.Add("@merchandisersCnt", SqlDbType.Int);
                    cmd.Parameters["@merchandisersCnt"].Value = merchList.Count;
                    cmd.Parameters.Add("@merchandisersIDs", SqlDbType.VarChar);
                    cmd.Parameters["@merchandisersIDs"].Value = GetMerchandiserIDs(merchList);
                    cmd.Parameters.Add("@supervisorId", SqlDbType.Int);
                    cmd.Parameters["@supervisorId"].Value = DBNull.Value;

                    Dictionary<string, string> RouteNames = new Dictionary<string, string>();
                    Dictionary<string, string> OutletTradingNames = new Dictionary<string, string>();
                    Dictionary<string, string> OutletAddresss = new Dictionary<string, string>();
                    Dictionary<string, string> OutletNames = new Dictionary<string, string>();
                    Dictionary<string, string> OutletDeliveryAddresss = new Dictionary<string, string>();
                    Dictionary<string, string> SupervisorNames = new Dictionary<string, string>();
                    Dictionary<string, string> MerchandiserNames = new Dictionary<string, string>();
                    Dictionary<string, string> OutletSubtypes = new Dictionary<string, string>();
                    Dictionary<string, string> OutletTypes = new Dictionary<string, string>();
                    Dictionary<string, string> Commentss = new Dictionary<string, string>();
                    Dictionary<string, string> OutletExternalCodes = new Dictionary<string, string>();
                                        
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        DataTable t = dataSet.Tables[ReportName];
                        t.MinimumCapacity = 100000;

                        while (reader.Read())
                        {
                            DataRow row;
                            row = t.NewRow();

                            row[FieldNames.Date] = reader.GetDateTime(0);
                            if(!reader.IsDBNull(1))
                            {
                                row[FieldNames.RouteName] = RouteNames.AddStringValue(reader.GetString(1));
                            }

                            row[FieldNames.OutletTradingName] = OutletTradingNames.AddStringValue(reader.GetString(2));
                            row[FieldNames.OutletAddress] = OutletAddresss.AddStringValue(reader.GetString(3));
                            row[FieldNames.OutletName] = OutletNames.AddStringValue(reader.GetString(4));
                            row[FieldNames.OutletDeliveryAddress] = OutletDeliveryAddresss.AddStringValue(reader.GetString(5));
                            row[FieldNames.SupervisorName] = SupervisorNames.AddStringValue(reader.GetString(6));
                            row[FieldNames.MerchandiserName] = MerchandiserNames.AddStringValue(reader.GetString(7));
                            row[FieldNames.OutletSubtype] = OutletSubtypes.AddStringValue(reader.GetString(8));
                            row[FieldNames.OutletType] = OutletTypes.AddStringValue(reader.GetString(9));
                            row[FieldNames.OutletExternalCode] = OutletExternalCodes.AddStringValue(reader.GetString(10));
                            if (!reader.IsDBNull(11))
                            {
                                row[FieldNames.OutletID] = reader.GetInt64(11);
                            }
                            if (!reader.IsDBNull(12))
                            {
                                row[FieldNames.OrdersCount] = reader.GetDecimal(12);
                            }

                            if (!reader.IsDBNull(13))
                            {
                                row[FieldNames.RemaindersCount] = reader.GetDecimal(13);
                            }

                            if (!reader.IsDBNull(14))
                            {
                                row[FieldNames.SalesCount] = reader.GetDecimal(14);
                            }

                            row[FieldNames.BeginTime] = reader.GetString(15);
                            row[FieldNames.EndTime] = reader.GetString(16);
                            row[FieldNames.Duration] = reader.GetString(17);
                            if (!reader.IsDBNull(18))
                            {
                                row[FieldNames.Comments] = Commentss.AddStringValue(reader.GetString(18));
                            }

                            
                            
                            
                            t.Rows.Add(row);
                        }

                        t.AcceptChanges();

                        reader.Close();
                    }
                }
            }
            catch (OutOfMemoryException)
            {
                dataSet.Dispose();
                throw;
            }
            catch (SystemException ex)
            {
                throw new SystemException(string.Format(Resource.ReportBuildingError, ReportName, ex.Message)); 
            }

            return dataSet.ReportEntries;
        }

        #endregion

        #region Private methods

        /// <summary>
        /// Gets the column mapping between the source table and the dataset.
        /// </summary>
        /// <returns></returns>
        protected override DataTableMapping GetMapping()
        {
            DataTableMapping map = new DataTableMapping();
            map.SourceTable = ReportName;
            map.DataSetTable = ReportName;

            map.ColumnMappings.AddRange(new DataColumnMapping[] { 
            OLTradingName, OLAddress,OLName,OL_id, OL_Code, OLDeliveryAddress, 
            Supervisor_name, MerchName,OLSubTypeName,OLtype_name,
            Comments});
            map.ColumnMappings.Add("Date", FieldNames.Date);
            map.ColumnMappings.Add("RouteName", FieldNames.RouteName);
            map.ColumnMappings.Add("OrdersCount", FieldNames.OrdersCount);
            map.ColumnMappings.Add("RemaindersCount", FieldNames.RemaindersCount);
			map.ColumnMappings.Add("SoldCount", FieldNames.SalesCount);
            map.ColumnMappings.Add("BeginTime", FieldNames.BeginTime);
            map.ColumnMappings.Add("EndTime", FieldNames.EndTime);
            map.ColumnMappings.Add("Duration", FieldNames.Duration);
            
            return map;
        }

        #endregion
    }
}
