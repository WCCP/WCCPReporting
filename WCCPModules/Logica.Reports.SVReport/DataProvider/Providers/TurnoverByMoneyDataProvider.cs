﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Configuration;
using System.Data.Common;
using Logica.Reports.DataProvider.Entities;
using Logica.Reports.Localization;
using Logica.Reports.Common;
using Logica.Reports.ModelDataProvider.Entities;
using Logica.Reports.SVReport;
using Logica.Reports.DataAccess;
using SoftServe.Reports.SVReport;

namespace Logica.Reports.DataProvider.Providers
{
    public class TurnoverByMoneyDataProvider : DataProviderBase
    {
        #region Public properties

        /// <summary>
        /// Gets the name of the report.
        /// </summary>
        /// <value>The name of the report.</value>
        public override string ReportName
        {
            get 
            { 
                return ReportType.TurnoverByMoney.ToString();
            }
        }

        #endregion

        #region Public methods

        /// <summary>
        /// Gets the report data.
        /// </summary>
        /// <param name="startDate">The report start date.</param>
        /// <param name="endDate">The report end date.</param>
        /// <param name="merchList">The list of merchs</param>
        /// <returns></returns>
        public TurnoverByMoneyDataSet.TurnoverByMoneyDataTable GetReportData(DateTime startDate, DateTime endDate, List<Merchandiser> merchList)
        {
            TurnoverByMoneyDataSet dataSet = new TurnoverByMoneyDataSet();

            TurnoverByMoneyDataSet.TurnoverByMoneyDataTable table = dataSet.TurnoverByMoney;

            try
            {

                using (SqlConnection conn = new SqlConnection(DataAccessLayer.ConnectionString.ToString()))
                {                   
                    conn.Open();
                    SqlCommand cmd = new SqlCommand(SqlConstants.TurnoverByMoney, conn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandTimeout = 3000;

                    cmd.Parameters.Add("@lowerDate", SqlDbType.DateTime);
                    cmd.Parameters["@lowerDate"].Value = startDate;

                    cmd.Parameters.Add("@upperDate",    SqlDbType.DateTime);
                    cmd.Parameters["@upperDate"].Value = endDate;

                    //cmd.Parameters.Add("@merchandisersCnt", SqlDbType.Int);
                    //cmd.Parameters["@merchandisersCnt"].Value = merchList.Count;
                    //cmd.Parameters.Add("@merchandisersIDs", SqlDbType.VarChar);
                    //cmd.Parameters["@merchandisersIDs"].Value = GetMerchandiserIDs(merchList);
                    //cmd.Parameters.Add("@supervisorId", SqlDbType.Int);
                    //cmd.Parameters["@supervisorId"].Value = DBNull.Value;

                    Dictionary<string, string> OutletNames = new Dictionary<string, string>();
                    Dictionary<string, string> CustomerNames = new Dictionary<string, string>();
                    Dictionary<string, string> OutletFactAddressNames = new Dictionary<string, string>();
                    Dictionary<string, string> OutletTypes = new Dictionary<string, string>();
                   
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        DataTable t = dataSet.Tables[ReportName];
                        t.MinimumCapacity = 100000;

                        while (reader.Read())
                        {
                            DataRow row;
                            row = t.NewRow();

                            row[FieldNames.OutletName] = OutletNames.AddStringValue(reader.GetString(0));
                            row[FieldNames.CustomerName] = CustomerNames.AddStringValue(reader.GetString(1));
                            row[FieldNames.OutletFactAddressName] = OutletFactAddressNames.AddStringValue(reader.GetString(2));
                            row[FieldNames.OutletID] = reader.GetInt64(3);
                            row[FieldNames.OutletType] = OutletTypes.AddStringValue(reader.GetString(4));
                            row[FieldNames.Date] = reader.GetDateTime(5);
                            if (!reader.IsDBNull(7))
                            {
                                row[FieldNames.SalesInMoney] = reader.GetDecimal(7);
                            }

                            if (!reader.IsDBNull(8))
                            {
                                row[FieldNames.DZ] = reader.GetDecimal(8);
                            }

                            if (!reader.IsDBNull(9))
                            {
                                row[FieldNames.TurnoverDZ] = reader.GetDecimal(9);
                            }
                            
                            t.Rows.Add(row);
                        }

                        t.AcceptChanges();

                        reader.Close();
                    }
                }
            }
            catch (OutOfMemoryException)
            {
                dataSet.Dispose();
                throw;
            }
            catch (SystemException ex)
            {
                throw new SystemException(string.Format(Resource.ReportBuildingError, ReportName, ex.Message));
            }

            return table;
        }

        #endregion

        #region Protected methods

        /// <summary>
        /// Gets the column mapping between the source table and the dataset.
        /// </summary>
        /// <returns>Column mapping.</returns>
        protected override DataTableMapping GetMapping()
        {
            TurnoverByMoneyDataSet dataSet = new TurnoverByMoneyDataSet();
            DataTableMapping map = new DataTableMapping();
            var table = dataSet.TurnoverByMoney;
            map.SourceTable = table.TableName;
            map.DataSetTable = table.TableName;

            map.ColumnMappings.Add("OL_id", FieldNames.OutletID);
            map.ColumnMappings.Add("Cust_NAME", FieldNames.CustomerName);
            map.ColumnMappings.Add("OLName", FieldNames.OutletName);
            map.ColumnMappings.Add("OLDeliveryAddress", FieldNames.OutletFactAddressName);
            map.ColumnMappings.Add("ChanelType", FieldNames.OutletType);
            map.ColumnMappings.Add("DateSV", FieldNames.Date);
            map.ColumnMappings.Add("SalOutUAH", FieldNames.SalesInMoney);
            map.ColumnMappings.Add("DZ", FieldNames.DZ);
            map.ColumnMappings.Add("TurnOverUAH", FieldNames.TurnoverDZ);

            return map;
        }

        #endregion
    }
}
