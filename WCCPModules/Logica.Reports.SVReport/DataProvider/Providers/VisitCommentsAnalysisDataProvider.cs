﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Configuration;
using System.Data.Common;
using Logica.Reports.DataProvider.Entities;
using Logica.Reports.Localization;
using Logica.Reports.Common;
using Logica.Reports.ModelDataProvider.Entities;
using Logica.Reports.DataAccess;
using System.Data;
using SoftServe.Reports.SVReport;

namespace Logica.Reports.DataProvider.Providers
{
    public class VisitCommentsAnalysisDataProvider : DataProviderBase
    {
        #region Public properties

        /// <summary>
        /// Gets the name of the report.
        /// </summary>
        /// <value>The name of the report.</value>
        public override string ReportName
        {
            get 
            { 
                return "VisitCommentsAnalysis";
            }
        }

        #endregion

        #region Public methods

        /// <summary>
        /// Gets the report data.
        /// </summary>
        /// <param name="startDate">The report start date.</param>
        /// <param name="endDate">The report end date.</param>
        /// <returns></returns>
        public VisitCommentsAnalysisDataSet.ReportEntriesDataTable GetReportData(DateTime startDate, DateTime endDate, List<Merchandiser> merchList)
        {
            VisitCommentsAnalysisDataSet dataSet = new VisitCommentsAnalysisDataSet();

            try
            {

                using (SqlConnection conn = new SqlConnection(DataAccessLayer.ConnectionString.ToString()))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand(SqlConstants.VisitCommentsAnalysisNew, conn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    //SqlCommand cmd = new SqlCommand(ScriptProvider.VisitCommentsAnalysisNew, conn); //  VisitCommentsAnalysis, conn);
                    cmd.CommandTimeout = 3000;

                    cmd.Parameters.Add("@lowerDate", SqlDbType.DateTime);
                    cmd.Parameters["@lowerDate"].Value = startDate;
                    cmd.Parameters.Add("@upperDate", SqlDbType.DateTime);
                    cmd.Parameters["@upperDate"].Value = endDate;
                    cmd.Parameters.Add("@merchandisersCnt", SqlDbType.Int);
                    cmd.Parameters["@merchandisersCnt"].Value = merchList.Count;
                    cmd.Parameters.Add("@merchandisersIDs", SqlDbType.VarChar);
                    cmd.Parameters["@merchandisersIDs"].Value = GetMerchandiserIDs(merchList);
                    cmd.Parameters.Add("@supervisorId", SqlDbType.Int);
                    cmd.Parameters["@supervisorId"].Value = DBNull.Value;

                    Dictionary<string, string> SupervisorNames = new Dictionary<string, string>();
                    Dictionary<string, string> MerchandiserNames = new Dictionary<string, string>();
                    Dictionary<string, string> OutletTypes = new Dictionary<string, string>();
                    Dictionary<string, string> OutletSubTypes = new Dictionary<string, string>();
                    Dictionary<string, string> OutletTradingNames = new Dictionary<string, string>();
                    Dictionary<string, string> OutletNames = new Dictionary<string, string>();
                    Dictionary<string, string> OutletDeliveryAddresss = new Dictionary<string, string>();
                    Dictionary<string, string> MerchandiserStatuss = new Dictionary<string, string>();
                    Dictionary<string, string> OutletStatuss = new Dictionary<string, string>();
                    Dictionary<string, string> OutletAddresss = new Dictionary<string, string>();
                    Dictionary<string, string> Reasons = new Dictionary<string, string>();
                    Dictionary<string, string> NotVisitedCommentss = new Dictionary<string, string>();
                    Dictionary<string, string> CommentsByVisits = new Dictionary<string, string>();

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        DataTable t = dataSet.Tables[ReportName];
                        t.MinimumCapacity = 100000;

                        while (reader.Read())
                        {
                            DataRow row;
                            row = t.NewRow();

                            row[FieldNames.SupervisorName] = SupervisorNames.AddStringValue(reader.GetString(0));
                            row[FieldNames.MerchandiserName] = MerchandiserNames.AddStringValue(reader.GetString(1));
                            row[FieldNames.OutletType] = OutletTypes.AddStringValue(reader.GetString(2));
                            row[FieldNames.OutletSubtype] = OutletSubTypes.AddStringValue(reader.GetString(3));
                            row[FieldNames.OutletID] = reader.GetInt64(4);
                            row[FieldNames.OutletTradingName] = OutletTradingNames.AddStringValue(reader.GetString(5));
                            row[FieldNames.OutletAddress] = OutletAddresss.AddStringValue(reader.GetString(6));
                            row[FieldNames.OutletName] = OutletNames.AddStringValue(reader.GetString(7));
                            row[FieldNames.OutletDeliveryAddress] = OutletDeliveryAddresss.AddStringValue(reader.GetString(8));
                            row[FieldNames.MerchandiserStatus] = MerchandiserStatuss.AddStringValue(reader.GetString(9));
                            row[FieldNames.OutletStatus] = OutletStatuss.AddStringValue(reader.GetString(10));
                            row[FieldNames.OutletCardDate] = reader.GetDateTime(11);
                            if (!reader.IsDBNull(12))
                            {
                                row[FieldNames.Reason] = Reasons.AddStringValue(reader.GetString(12));
                            }

                            if (!reader.IsDBNull(13))
                            {
                                row[FieldNames.NotVisitedComments] = NotVisitedCommentss.AddStringValue(reader.GetString(13));
                            }

                            if (!reader.IsDBNull(14))
                            {
                                row[FieldNames.CommentsByVisit] = CommentsByVisits.AddStringValue(reader.GetString(14));
                            }
                            
                            t.Rows.Add(row);
                        }

                        t.AcceptChanges();

                        reader.Close();
                    }
                }
            }
            catch (OutOfMemoryException)
            {
                dataSet.Dispose();
                throw;
            }
            catch (SystemException ex)
            {
                throw new SystemException(string.Format(Resource.ReportBuildingError, ReportName, ex.Message));
            }

            return dataSet.ReportEntries;
        }

        #endregion

        #region Protected methods

        /// <summary>
        /// Gets the column mapping between the source table and the dataset.
        /// </summary>
        /// <returns>Column mapping.</returns>
        protected override DataTableMapping GetMapping()
        {
            DataTableMapping map = new DataTableMapping();
            map.SourceTable = ReportName;
            map.DataSetTable = ReportName;

            map.ColumnMappings.AddRange(new DataColumnMapping[]{
                Supervisor_name,MerchName, OLtype_name,OLSubTypeName,
                OLName, OLDeliveryAddress,OLTradingName,OLAddress,
                OL_id,  MerchStatus, OutletsSatus, OLCardDate,
                });
            
            map.ColumnMappings.Add("Reason", FieldNames.InaccessibilityReason);
            map.ColumnMappings.Add("NotVisitedComments", FieldNames.NotVisitedComments);
            map.ColumnMappings.Add("CommentsByVisit", FieldNames.CommentsByVisit);
            return map;
        }

        #endregion
    }
}
