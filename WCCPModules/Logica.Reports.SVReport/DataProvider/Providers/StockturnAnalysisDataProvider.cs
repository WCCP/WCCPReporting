﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.Data.Common;
using System.Configuration;
using Logica.Reports.DataProvider.Entities;
using Logica.Reports.Localization;
using Logica.Reports.Common;
using Logica.Reports.ModelDataProvider.Entities;

namespace Logica.Reports.DataProvider.Providers
{
    public class StockturnAnalysisDataProvider : DataProviderBase
    {
        #region Public properties

        /// <summary>
        /// Gets the current report name.
        /// </summary>
        public override string ReportName
        {
            get
            {
                return "StockturnAnalysis";
            }
        }

        #endregion

        #region Public methods

        /// <summary>
        /// Gets the report data.
        /// </summary>
        /// <param name="startDate">The report start date.</param>
        /// <param name="endDate">The report end date.</param>
        /// <returns></returns>
        public StockturnAnalysisDataSet.StockturnAnalysisDataTable GetReportData(DateTime startDate, DateTime endDate, List<Merchandiser> merchList)
        {
            StockturnAnalysisDataSet dataSet = new StockturnAnalysisDataSet();
           
            try
            {
                Dictionary<string, object> sqlParameters = new Dictionary<string,object>();
                sqlParameters.Add("@lowerDate", startDate);
                sqlParameters.Add("@upperDate", endDate);
                sqlParameters.Add("@merchandisersCnt", merchList.Count);
                sqlParameters.Add("@merchandisersIDs", GetMerchandiserIDs(merchList));
                sqlParameters.Add("@supervisorId", DBNull.Value);
                SqlDataAdapter adapter = ExecuteAdapterCommand(ScriptProvider.StockturnAnalysis, sqlParameters);  
                adapter.TableMappings.Add(GetMapping());
                adapter.Fill(dataSet, ReportName);
            }
            catch (OutOfMemoryException)
            {
                dataSet.Dispose();
                throw;
            }
            catch (SystemException ex)
            {
                throw new SystemException(string.Format(Resource.ReportBuildingError, ReportName, ex.Message)); 
            }

            return dataSet.StockturnAnalysis;
        }

        #endregion

        #region Private methods

        /// <summary>
        /// Gets the column mapping between the source table and the dataset.
        /// </summary>
        /// <returns></returns>
        protected override DataTableMapping GetMapping()
        {
            DataTableMapping map = new DataTableMapping();
            map.SourceTable = ReportName;
            map.DataSetTable = ReportName;

            map.ColumnMappings.AddRange(new DataColumnMapping[]{
                OL_id,OL_Code,OLtype_name,OLSubTypeName,
                OLName, OLDeliveryAddress,OLTradingName, OLAddress,
                OutletsSatus, Supervisor_name,MerchName });
            map.ColumnMappings.Add("Date", FieldNames.Date);
            map.ColumnMappings.Add("RouteName", FieldNames.RouteName);
            map.ColumnMappings.Add("SalesInCount", FieldNames.SalesInCount);
            map.ColumnMappings.Add("SalesInMoney", FieldNames.SalesInMoney);
            map.ColumnMappings.Add("DebtInCount", FieldNames.DebtInCount);
            map.ColumnMappings.Add("DebtInMoney", FieldNames.DebtInMoney);
            map.ColumnMappings.Add("SaleAmount", FieldNames.SaleAmount);
            map.ColumnMappings.Add("DebtAmount", FieldNames.DebtAmount);
            map.ColumnMappings.Add("DebtType_Name", FieldNames.DebtTypeName);
            return map;
        }

        #endregion
    }
}
