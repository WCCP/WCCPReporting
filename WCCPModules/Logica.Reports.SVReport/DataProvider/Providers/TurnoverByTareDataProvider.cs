﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Configuration;
using System.Data.Common;
using Logica.Reports.DataProvider.Entities;
using Logica.Reports.Localization;
using Logica.Reports.Common;
using Logica.Reports.ModelDataProvider.Entities;
using Logica.Reports.SVReport;
using Logica.Reports.DataAccess;
using SoftServe.Reports.SVReport;

namespace Logica.Reports.DataProvider.Providers
{
    public class TurnoverByTareDataProvider : DataProviderBase
    {
        #region Public properties

        /// <summary>
        /// Gets the name of the report.
        /// </summary>
        /// <value>The name of the report.</value>
        public override string ReportName
        {
            get 
            { 
                return ReportType.TurnoverByTare.ToString();
            }
        }

        #endregion

        #region Public methods

        /// <summary>
        /// Gets the report data.
        /// </summary>
        /// <param name="startDate">The report start date.</param>
        /// <param name="endDate">The report end date.</param>
        /// <param name="merchList">The list of merchs</param>
        /// <returns></returns>
        public TurnoverByTareDataSet.TurnoverByTareDataTable GetReportData(DateTime startDate, DateTime endDate, List<Merchandiser> merchList)
        {
            TurnoverByTareDataSet dataSet = new TurnoverByTareDataSet();
           
            try
            {

                using (SqlConnection conn = new SqlConnection(DataAccessLayer.ConnectionString.ToString()))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand(SqlConstants.TurnStock, conn);

                    //SqlCommand cmd = new SqlCommand(ScriptProvider.TurnoverByTare, conn);


                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandTimeout = 3000;

                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add("@lowerDate", SqlDbType.DateTime);
                    cmd.Parameters["@lowerDate"].Value = startDate;
                    cmd.Parameters.Add("@upperDate", SqlDbType.DateTime);
                    cmd.Parameters["@upperDate"].Value = endDate;
                    cmd.Parameters.Add("@merchandisersCnt", SqlDbType.Int);
                    cmd.Parameters["@merchandisersCnt"].Value = merchList.Count;
                    cmd.Parameters.Add("@merchandisersIDs", SqlDbType.VarChar);
                    cmd.Parameters["@merchandisersIDs"].Value = GetMerchandiserIDs(merchList);
                    cmd.Parameters.Add("@supervisorId", SqlDbType.Int);
                    cmd.Parameters["@supervisorId"].Value = DBNull.Value;

                    Dictionary<string, string> OutletNames = new Dictionary<string, string>();
                    Dictionary<string, string> SupervisorNames = new Dictionary<string, string>();
                    Dictionary<string, string> MerchandiserNames = new Dictionary<string, string>();
                    Dictionary<string, string> OutletFactAddressNames = new Dictionary<string, string>();
                    Dictionary<string, string> OutletTypes = new Dictionary<string, string>();
                    Dictionary<string, string> Tares = new Dictionary<string, string>();
                    Dictionary<string, string> TareTypes = new Dictionary<string, string>();

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        DataTable t = dataSet.Tables[ReportName];
                        t.MinimumCapacity = 100000;

                        while (reader.Read())
                        {
                            DataRow row;
                            row = t.NewRow();


                            row[FieldNames.OutletName] = OutletNames.AddStringValue(reader.GetString(0));
                            row[FieldNames.OutletFactAddressName] = OutletFactAddressNames.AddStringValue(reader.GetString(1));
                            row[FieldNames.OutletID] = reader.GetInt64(2);
                            row[FieldNames.SupervisorName] = SupervisorNames.AddStringValue(reader.GetString(3));
                            row[FieldNames.MerchandiserName] = MerchandiserNames.AddStringValue(reader.GetString(5));
                            
                            row[FieldNames.Date] = reader.GetDateTime(6);
                            row[FieldNames.Tare] = Tares.AddStringValue(reader.GetString(7));
                            row[FieldNames.TareType] = TareTypes.AddStringValue(reader.GetString(8));
                            if (!reader.IsDBNull(11))
                            {
                                row[FieldNames.SalesInCount] = reader.GetDecimal(11);
                            }

                            if (!reader.IsDBNull(12))
                            {
                                row[FieldNames.DZ] = reader.GetDecimal(12);
                            }

                            // row[FieldNames.TurnoverDZ] = reader.GetDecimal(11);
                            

                            t.Rows.Add(row);
                        }

                        t.AcceptChanges();

                        reader.Close();
                    }
                }
            }
            catch (OutOfMemoryException)
            {
                dataSet.Dispose();
                throw;
            }
            catch (SystemException ex)
            {
                throw new SystemException(string.Format(Resource.ReportBuildingError, ReportName, ex.Message));
            }

            return dataSet.TurnoverByTare;
        }

        #endregion

        #region Protected methods

        /// <summary>
        /// Gets the column mapping between the source table and the dataset.
        /// </summary>
        /// <returns>Column mapping.</returns>
        protected override DataTableMapping GetMapping()
        {
            var table = new TurnoverByTareDataSet().TurnoverByTare;
            var map = new DataTableMapping();
            map.SourceTable = table.TableName;
            map.DataSetTable = table.TableName;

            map.ColumnMappings.Add("OLName", FieldNames.OutletName);
            map.ColumnMappings.Add("OLAddress", FieldNames.OutletFactAddressName);
            map.ColumnMappings.Add("ol_id", FieldNames.OutletID);
            map.ColumnMappings.Add("Supervisor_name", FieldNames.SupervisorName);
            map.ColumnMappings.Add("MerchName", FieldNames.MerchandiserName);
            map.ColumnMappings.Add("date", FieldNames.Date);
            map.ColumnMappings.Add("tare", FieldNames.Tare);
            map.ColumnMappings.Add("typetare", FieldNames.TareType);

            map.ColumnMappings.Add("salesincount1", FieldNames.SalesInCount);
            map.ColumnMappings.Add("debtincount1", FieldNames.DZ);
            map.ColumnMappings.Add("turnover", FieldNames.TurnoverDZ);

            return map;
        }

        #endregion
    }
}
