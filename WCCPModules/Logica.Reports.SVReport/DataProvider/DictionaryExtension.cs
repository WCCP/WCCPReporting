﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Logica.Reports.DataProvider.Providers
{
    static class DictionaryExtension
    {
        /// <summary>
        /// Inserts the value into the dictionary. If it's already present in the dictionary, returns the reference on the current occurence. 
        /// </summary>
        /// <param name="val">The element to add</param>
        /// <returns>Inserted value</returns>
        public static string AddStringValue(this Dictionary<string, string> d, string val)
        {
            if (!d.Keys.Contains(val))
            {
                d.Add(val, val);
            }

            return d[val];
        }
    }
}
