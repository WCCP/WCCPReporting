﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Logica.Reports.ModelDataProvider.Entities;

namespace Logica.Reports.ModelDataProvider.Providers
{
    /// <summary>
    /// Provides supervisors data.
    /// </summary>
    public class SupervisorDataProvider : ModelDataProviderBase
    {
        /// <summary>
        /// Gets all supervisors.
        /// </summary>
        /// <returns>Supervisors list.</returns>
        public List<Supervisor> GetAll(int reportId)
        {
            TradingStaffDataContext db = new TradingStaffDataContext(DatabaseConnection);
            return db.spDW_SupervisorsGetList(reportId).ToList();
        }
    }
}
