﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Logica.Reports.ModelDataProvider.Entities;
using Logica.Reports.DataProvider.Providers;

namespace Logica.Reports.ModelDataProvider.Providers
{    
    /// <summary>
    /// Provides merchandisers data.
    /// </summary>
    public class MerchandiserDataProvider : ModelDataProviderBase
    {
        /// <summary>
        /// Gets all merchandisers.
        /// </summary>
        /// <returns>Merchandisers list.</returns>
        public List<Merchandiser> GetAll(int reportId)
        {
            TradingStaffDataContext db = new TradingStaffDataContext(DatabaseConnection);
            return db.spDW_MerchandisersGetList(reportId).ToList();
        }
    }
}
