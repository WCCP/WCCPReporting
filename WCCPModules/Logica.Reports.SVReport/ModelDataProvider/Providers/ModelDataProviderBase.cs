﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Configuration;
using Logica.Reports.Common;
using System.Data.Common;

namespace Logica.Reports.ModelDataProvider.Providers
{
    /// <summary>
    /// Base class for all model data providers.
    /// </summary>
    public abstract class ModelDataProviderBase : IDisposable
    {
        #region Private fields

        /// <summary>
        /// Stores the current SQLConnection.
        /// </summary>
        private SqlConnection _databaseConnection;

        #endregion

        #region Protected properties

        /// <summary>
        /// Gets the database connection.
        /// </summary>
        /// <value>The database connection.</value>
        protected SqlConnection DatabaseConnection
        {
            get
            {
                if (_databaseConnection == null)
                {
                    _databaseConnection = new SqlConnection(SettingProvider.ConnectionString);
                }
                return _databaseConnection;
            }
        }

        #endregion

        #region IDisposable Members

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public virtual void Dispose()
        {
            _databaseConnection.Close();
        }

        #endregion
    }
}
