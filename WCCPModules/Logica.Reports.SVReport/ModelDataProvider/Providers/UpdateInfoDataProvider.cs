﻿using System;
using System.Collections.Generic;
using System.Linq;
using Logica.Reports.ModelDataProvider.Entities;

namespace Logica.Reports.ModelDataProvider.Providers
{
    public class UpdateInfoDataProvider : ModelDataProviderBase
    {
        private readonly UpdateInfoDataContext context;

        public UpdateInfoDataProvider()
        {
            context = new UpdateInfoDataContext(DatabaseConnection);
        }

        public override void Dispose()
        {
            context.Dispose();
            base.Dispose();
        }

        public IEnumerable<UpdateTime> GetUpdTime(DateTime date)
        {
            return context.spDW_M_OffTrade_Get_UpdateTime(date);
        }

        public void UpdateData()
        {
            var timeout = context.CommandTimeout;
            context.CommandTimeout = 900;
            try
            {
                context.ExecuteCommand("exec sp_DW_Visits_With_Invoices_Update @isFullUpdate=1");
            }
            finally
            {
                context.CommandTimeout = timeout;
            }
        }
    }
}