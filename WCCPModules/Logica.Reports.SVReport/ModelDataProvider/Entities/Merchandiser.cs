﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Logica.Reports.ModelDataProvider.Entities
{
    
    public class Merchandiser
    {
        const string ONTRADE = "on-trade";
        const string OFFTRADE = "off-trade";
        const string KATRADE = "KA-trade";
        
        private string _name;
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        private int _id;
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        private int _supervisorId;
        public int SupervisorId
        {
            get { return _supervisorId; }
            set { _supervisorId = value; }
        }

        private ActivityGroupType _activityType = ActivityGroupType.Undefined;
        public ActivityGroupType ActivityType
        {
            get { return _activityType; }
            set { _activityType = value; }
        }

        public static ActivityGroupType ConvertType(string type)
        {
            if (type.Equals(ONTRADE, StringComparison.InvariantCultureIgnoreCase))
                return ActivityGroupType.OnTrade;
            else if (type.Equals(OFFTRADE, StringComparison.InvariantCultureIgnoreCase))
                return ActivityGroupType.OffTrade;
            else if (type.Equals(KATRADE, StringComparison.InvariantCultureIgnoreCase))
                return ActivityGroupType.KATrade;
            else
                return ActivityGroupType.Undefined;

        }
    }
}
