﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Logica.Reports.ModelDataProvider.Entities
{
    public class spDW_FSM_SupervisorsGetListResult
    {
        private string name;
        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        private int id;
        public int Id
        {
            get { return id; }
            set { id = value; }
        }
    }
}
