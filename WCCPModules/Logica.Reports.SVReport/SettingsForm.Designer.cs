namespace Logica.Reports.SVReport
{
    partial class SettingsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SettingsForm));
            this.tcOptions = new DevExpress.XtraTab.XtraTabControl();
            this.pgAction = new DevExpress.XtraTab.XtraTabPage();
            this.OptionsPanel = new DevExpress.XtraEditors.PanelControl();
            this.chkOff = new DevExpress.XtraEditors.CheckEdit();
            this.chkOn = new DevExpress.XtraEditors.CheckEdit();
            this.chkAll = new DevExpress.XtraEditors.CheckEdit();
            this.ccbM1 = new DevExpress.XtraEditors.CheckedComboBoxEdit();
            this.ccbM2 = new DevExpress.XtraEditors.CheckedComboBoxEdit();
            this.clbReports = new DevExpress.XtraEditors.CheckedListBoxControl();
            this.dtTo = new DevExpress.XtraEditors.DateEdit();
            this.dtFrom = new DevExpress.XtraEditors.DateEdit();
            this.lblTo = new System.Windows.Forms.Label();
            this.lblM1 = new System.Windows.Forms.Label();
            this.lblFrom = new System.Windows.Forms.Label();
            this.lblM2 = new System.Windows.Forms.Label();
            this.lblSpan = new System.Windows.Forms.Label();
            this.lblReport = new System.Windows.Forms.Label();
            this.txtReportName = new DevExpress.XtraEditors.TextEdit();
            this.PanelBottom = new DevExpress.XtraEditors.PanelControl();
            this.btnHelp = new DevExpress.XtraEditors.SimpleButton();
            this.imCollection = new DevExpress.Utils.ImageCollection(this.components);
            this.btnNo = new DevExpress.XtraEditors.SimpleButton();
            this.btnYes = new DevExpress.XtraEditors.SimpleButton();
            this.panelControlMain = new DevExpress.XtraEditors.PanelControl();
            this.panelControlHeader = new DevExpress.XtraEditors.PanelControl();
            this.grpUpd = new DevExpress.XtraEditors.GroupControl();
            this.lblUpd2 = new DevExpress.XtraEditors.LabelControl();
            this.lblUpd = new DevExpress.XtraEditors.LabelControl();
            this.btn_upd = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.tcOptions)).BeginInit();
            this.tcOptions.SuspendLayout();
            this.pgAction.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.OptionsPanel)).BeginInit();
            this.OptionsPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkOff.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkOn.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkAll.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ccbM1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ccbM2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.clbReports)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtTo.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtTo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtFrom.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtFrom.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReportName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PanelBottom)).BeginInit();
            this.PanelBottom.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imCollection)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControlMain)).BeginInit();
            this.panelControlMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControlHeader)).BeginInit();
            this.panelControlHeader.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grpUpd)).BeginInit();
            this.grpUpd.SuspendLayout();
            this.SuspendLayout();
            // 
            // tcOptions
            // 
            this.tcOptions.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tcOptions.Location = new System.Drawing.Point(3, 3);
            this.tcOptions.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Style3D;
            this.tcOptions.LookAndFeel.UseDefaultLookAndFeel = false;
            this.tcOptions.Name = "tcOptions";
            this.tcOptions.SelectedTabPage = this.pgAction;
            this.tcOptions.Size = new System.Drawing.Size(361, 499);
            this.tcOptions.TabIndex = 4;
            this.tcOptions.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.pgAction});
            // 
            // pgAction
            // 
            this.pgAction.Controls.Add(this.grpUpd);
            this.pgAction.Controls.Add(this.panelControlHeader);
            this.pgAction.Controls.Add(this.OptionsPanel);
            this.pgAction.Name = "pgAction";
            this.pgAction.Size = new System.Drawing.Size(357, 475);
            this.pgAction.Text = "��������";
            // 
            // OptionsPanel
            // 
            this.OptionsPanel.Controls.Add(this.chkOff);
            this.OptionsPanel.Controls.Add(this.chkOn);
            this.OptionsPanel.Controls.Add(this.chkAll);
            this.OptionsPanel.Controls.Add(this.ccbM1);
            this.OptionsPanel.Controls.Add(this.ccbM2);
            this.OptionsPanel.Controls.Add(this.clbReports);
            this.OptionsPanel.Controls.Add(this.dtTo);
            this.OptionsPanel.Controls.Add(this.dtFrom);
            this.OptionsPanel.Controls.Add(this.lblTo);
            this.OptionsPanel.Controls.Add(this.lblM1);
            this.OptionsPanel.Controls.Add(this.lblFrom);
            this.OptionsPanel.Controls.Add(this.lblM2);
            this.OptionsPanel.Controls.Add(this.lblSpan);
            this.OptionsPanel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.OptionsPanel.Location = new System.Drawing.Point(0, 127);
            this.OptionsPanel.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Style3D;
            this.OptionsPanel.LookAndFeel.UseDefaultLookAndFeel = false;
            this.OptionsPanel.Name = "OptionsPanel";
            this.OptionsPanel.Size = new System.Drawing.Size(357, 348);
            this.OptionsPanel.TabIndex = 0;
            // 
            // chkOff
            // 
            this.chkOff.Location = new System.Drawing.Point(89, 323);
            this.chkOff.Name = "chkOff";
            this.chkOff.Properties.Caption = "��� \"���\"";
            this.chkOff.Size = new System.Drawing.Size(75, 18);
            this.chkOff.TabIndex = 7;
            this.chkOff.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged2);
            // 
            // chkOn
            // 
            this.chkOn.Location = new System.Drawing.Point(8, 323);
            this.chkOn.Name = "chkOn";
            this.chkOn.Properties.Caption = "��� \"��\"";
            this.chkOn.Size = new System.Drawing.Size(75, 18);
            this.chkOn.TabIndex = 6;
            this.chkOn.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged1);
            // 
            // chkAll
            // 
            this.chkAll.Location = new System.Drawing.Point(9, 56);
            this.chkAll.Name = "chkAll";
            this.chkAll.Properties.Caption = "���";
            this.chkAll.Size = new System.Drawing.Size(75, 18);
            this.chkAll.TabIndex = 6;
            this.chkAll.CheckedChanged += new System.EventHandler(this.chkAll_CheckedChanged);
            // 
            // ccbM1
            // 
            this.ccbM1.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.ccbM1.Location = new System.Drawing.Point(7, 295);
            this.ccbM1.Name = "ccbM1";
            this.ccbM1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ccbM1.Size = new System.Drawing.Size(344, 22);
            this.ccbM1.TabIndex = 5;
            this.ccbM1.TextChanged += new System.EventHandler(this.ccbM1_TextChanged);
            // 
            // ccbM2
            // 
            this.ccbM2.Location = new System.Drawing.Point(8, 254);
            this.ccbM2.Name = "ccbM2";
            this.ccbM2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ccbM2.Size = new System.Drawing.Size(344, 22);
            this.ccbM2.TabIndex = 4;
            // 
            // clbReports
            // 
            this.clbReports.CheckOnClick = true;
            this.clbReports.Location = new System.Drawing.Point(7, 76);
            this.clbReports.Name = "clbReports";
            this.clbReports.Size = new System.Drawing.Size(344, 159);
            this.clbReports.TabIndex = 3;
            this.clbReports.ItemCheck += new DevExpress.XtraEditors.Controls.ItemCheckEventHandler(this.clbReports_ItemCheck);
            // 
            // dtTo
            // 
            this.dtTo.EditValue = null;
            this.dtTo.Location = new System.Drawing.Point(210, 24);
            this.dtTo.Name = "dtTo";
            this.dtTo.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtTo.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dtTo.Size = new System.Drawing.Size(115, 22);
            this.dtTo.TabIndex = 2;
            // 
            // dtFrom
            // 
            this.dtFrom.EditValue = null;
            this.dtFrom.Location = new System.Drawing.Point(31, 24);
            this.dtFrom.Name = "dtFrom";
            this.dtFrom.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtFrom.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dtFrom.Size = new System.Drawing.Size(115, 22);
            this.dtFrom.TabIndex = 1;
            // 
            // lblTo
            // 
            this.lblTo.AutoSize = true;
            this.lblTo.Location = new System.Drawing.Point(180, 28);
            this.lblTo.Name = "lblTo";
            this.lblTo.Size = new System.Drawing.Size(24, 13);
            this.lblTo.TabIndex = 2;
            this.lblTo.Text = "��:";
            // 
            // lblM1
            // 
            this.lblM1.AutoSize = true;
            this.lblM1.Location = new System.Drawing.Point(7, 279);
            this.lblM1.Name = "lblM1";
            this.lblM1.Size = new System.Drawing.Size(25, 13);
            this.lblM1.TabIndex = 2;
            this.lblM1.Text = "�1:";
            // 
            // lblFrom
            // 
            this.lblFrom.AutoSize = true;
            this.lblFrom.Location = new System.Drawing.Point(7, 28);
            this.lblFrom.Name = "lblFrom";
            this.lblFrom.Size = new System.Drawing.Size(18, 13);
            this.lblFrom.TabIndex = 2;
            this.lblFrom.Text = "�:";
            // 
            // lblM2
            // 
            this.lblM2.AutoSize = true;
            this.lblM2.Location = new System.Drawing.Point(8, 238);
            this.lblM2.Name = "lblM2";
            this.lblM2.Size = new System.Drawing.Size(25, 13);
            this.lblM2.TabIndex = 2;
            this.lblM2.Text = "�2:";
            // 
            // lblSpan
            // 
            this.lblSpan.AutoSize = true;
            this.lblSpan.Location = new System.Drawing.Point(7, 3);
            this.lblSpan.Name = "lblSpan";
            this.lblSpan.Size = new System.Drawing.Size(49, 13);
            this.lblSpan.TabIndex = 2;
            this.lblSpan.Text = "������:";
            // 
            // lblReport
            // 
            this.lblReport.AutoSize = true;
            this.lblReport.Location = new System.Drawing.Point(6, 3);
            this.lblReport.Name = "lblReport";
            this.lblReport.Size = new System.Drawing.Size(43, 13);
            this.lblReport.TabIndex = 2;
            this.lblReport.Text = "�����:";
            // 
            // txtReportName
            // 
            this.txtReportName.EditValue = "";
            this.txtReportName.Enabled = false;
            this.txtReportName.Location = new System.Drawing.Point(9, 21);
            this.txtReportName.Name = "txtReportName";
            this.txtReportName.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txtReportName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.txtReportName.Properties.Appearance.Options.UseBackColor = true;
            this.txtReportName.Properties.Appearance.Options.UseFont = true;
            this.txtReportName.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Flat;
            this.txtReportName.Properties.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Style3D;
            this.txtReportName.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            this.txtReportName.Properties.ReadOnly = true;
            this.txtReportName.Size = new System.Drawing.Size(341, 22);
            this.txtReportName.TabIndex = 0;
            this.txtReportName.TabStop = false;
            // 
            // PanelBottom
            // 
            this.PanelBottom.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.PanelBottom.Controls.Add(this.btnHelp);
            this.PanelBottom.Controls.Add(this.btnNo);
            this.PanelBottom.Controls.Add(this.btnYes);
            this.PanelBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.PanelBottom.Location = new System.Drawing.Point(0, 505);
            this.PanelBottom.Name = "PanelBottom";
            this.PanelBottom.Size = new System.Drawing.Size(367, 31);
            this.PanelBottom.TabIndex = 3;
            // 
            // btnHelp
            // 
            this.btnHelp.Enabled = false;
            this.btnHelp.ImageIndex = 0;
            this.btnHelp.ImageList = this.imCollection;
            this.btnHelp.Location = new System.Drawing.Point(287, 3);
            this.btnHelp.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Style3D;
            this.btnHelp.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btnHelp.Name = "btnHelp";
            this.btnHelp.Size = new System.Drawing.Size(75, 25);
            this.btnHelp.TabIndex = 2;
            this.btnHelp.Text = "&�������";
            // 
            // imCollection
            // 
            this.imCollection.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imCollection.ImageStream")));
            this.imCollection.Images.SetKeyName(0, "help_24.png");
            this.imCollection.Images.SetKeyName(1, "check_24.png");
            this.imCollection.Images.SetKeyName(2, "close_24.png");
            // 
            // btnNo
            // 
            this.btnNo.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnNo.ImageIndex = 2;
            this.btnNo.ImageList = this.imCollection;
            this.btnNo.Location = new System.Drawing.Point(206, 3);
            this.btnNo.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Style3D;
            this.btnNo.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btnNo.Name = "btnNo";
            this.btnNo.Size = new System.Drawing.Size(75, 25);
            this.btnNo.TabIndex = 9;
            this.btnNo.Text = "�&��";
            // 
            // btnYes
            // 
            this.btnYes.ImageIndex = 1;
            this.btnYes.ImageList = this.imCollection;
            this.btnYes.Location = new System.Drawing.Point(125, 3);
            this.btnYes.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Style3D;
            this.btnYes.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btnYes.Name = "btnYes";
            this.btnYes.Size = new System.Drawing.Size(75, 25);
            this.btnYes.TabIndex = 8;
            this.btnYes.Text = "&��";
            this.btnYes.Click += new System.EventHandler(this.btnYes_Click);
            // 
            // panelControlMain
            // 
            this.panelControlMain.Controls.Add(this.tcOptions);
            this.panelControlMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControlMain.Location = new System.Drawing.Point(0, 0);
            this.panelControlMain.Name = "panelControlMain";
            this.panelControlMain.Size = new System.Drawing.Size(367, 505);
            this.panelControlMain.TabIndex = 1;
            // 
            // panelControlHeader
            // 
            this.panelControlHeader.Controls.Add(this.lblReport);
            this.panelControlHeader.Controls.Add(this.txtReportName);
            this.panelControlHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControlHeader.Location = new System.Drawing.Point(0, 0);
            this.panelControlHeader.Name = "panelControlHeader";
            this.panelControlHeader.Size = new System.Drawing.Size(357, 51);
            this.panelControlHeader.TabIndex = 1;
            // 
            // grpUpd
            // 
            this.grpUpd.Controls.Add(this.lblUpd2);
            this.grpUpd.Controls.Add(this.lblUpd);
            this.grpUpd.Controls.Add(this.btn_upd);
            this.grpUpd.Location = new System.Drawing.Point(0, 57);
            this.grpUpd.Name = "grpUpd";
            this.grpUpd.Size = new System.Drawing.Size(357, 63);
            this.grpUpd.TabIndex = 22;
            this.grpUpd.Text = "������";
            // 
            // lblUpd2
            // 
            this.lblUpd2.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.lblUpd2.Appearance.Options.UseFont = true;
            this.lblUpd2.Location = new System.Drawing.Point(78, 33);
            this.lblUpd2.Name = "lblUpd2";
            this.lblUpd2.Size = new System.Drawing.Size(0, 13);
            this.lblUpd2.TabIndex = 8;
            // 
            // lblUpd
            // 
            this.lblUpd.Location = new System.Drawing.Point(13, 33);
            this.lblUpd.Name = "lblUpd";
            this.lblUpd.Size = new System.Drawing.Size(59, 13);
            this.lblUpd.TabIndex = 7;
            this.lblUpd.Text = "������ ��:";
            // 
            // btn_upd
            // 
            this.btn_upd.ImageIndex = 1;
            this.btn_upd.ImageList = this.imCollection;
            this.btn_upd.Location = new System.Drawing.Point(264, 24);
            this.btn_upd.Name = "btn_upd";
            this.btn_upd.Size = new System.Drawing.Size(86, 33);
            this.btn_upd.TabIndex = 6;
            this.btn_upd.Text = " �������� ";
            this.btn_upd.Click += new System.EventHandler(this.btn_upd_Click);
            // 
            // SettingsForm
            // 
            this.AcceptButton = this.btnYes;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnNo;
            this.ClientSize = new System.Drawing.Size(367, 536);
            this.Controls.Add(this.panelControlMain);
            this.Controls.Add(this.PanelBottom);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Style3D;
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.MaximizeBox = false;
            this.Name = "SettingsForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "�����";
            ((System.ComponentModel.ISupportInitialize)(this.tcOptions)).EndInit();
            this.tcOptions.ResumeLayout(false);
            this.pgAction.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.OptionsPanel)).EndInit();
            this.OptionsPanel.ResumeLayout(false);
            this.OptionsPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkOff.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkOn.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkAll.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ccbM1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ccbM2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.clbReports)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtTo.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtTo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtFrom.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtFrom.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReportName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PanelBottom)).EndInit();
            this.PanelBottom.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.imCollection)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControlMain)).EndInit();
            this.panelControlMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControlHeader)).EndInit();
            this.panelControlHeader.ResumeLayout(false);
            this.panelControlHeader.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grpUpd)).EndInit();
            this.grpUpd.ResumeLayout(false);
            this.grpUpd.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl PanelBottom;
        public DevExpress.XtraEditors.SimpleButton btnHelp;
        private DevExpress.XtraEditors.SimpleButton btnNo;
        public DevExpress.XtraEditors.SimpleButton btnYes;
        public DevExpress.XtraTab.XtraTabPage pgAction;
        public DevExpress.XtraEditors.PanelControl OptionsPanel;
        private System.Windows.Forms.Label lblSpan;
        private System.Windows.Forms.Label lblReport;
        protected DevExpress.XtraEditors.TextEdit txtReportName;
        private DevExpress.XtraEditors.CheckedListBoxControl clbReports;
        private DevExpress.XtraEditors.DateEdit dtTo;
        private DevExpress.XtraEditors.DateEdit dtFrom;
        private System.Windows.Forms.Label lblTo;
        private System.Windows.Forms.Label lblFrom;
        private DevExpress.XtraEditors.CheckedComboBoxEdit ccbM1;
        private DevExpress.XtraEditors.CheckedComboBoxEdit ccbM2;
        private System.Windows.Forms.Label lblM1;
        private System.Windows.Forms.Label lblM2;
        private DevExpress.XtraEditors.CheckEdit chkAll;
        private DevExpress.Utils.ImageCollection imCollection;
        private DevExpress.XtraEditors.CheckEdit chkOff;
        private DevExpress.XtraEditors.CheckEdit chkOn;
        private DevExpress.XtraTab.XtraTabControl tcOptions;
        private DevExpress.XtraEditors.PanelControl panelControlHeader;
        private DevExpress.XtraEditors.PanelControl panelControlMain;
        private DevExpress.XtraEditors.GroupControl grpUpd;
        private DevExpress.XtraEditors.LabelControl lblUpd2;
        private DevExpress.XtraEditors.LabelControl lblUpd;
        private DevExpress.XtraEditors.SimpleButton btn_upd;
    }
}