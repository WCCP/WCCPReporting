﻿namespace Logica.Reports.SVReport.UserControls
{
    sealed partial class TurnoverByMoneyReport
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.uiPivotGrid = new DevExpress.XtraPivotGrid.PivotGridControl();
            this.pivotGridFieldOLFactName = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldCustName = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldOLFactAddress = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldOLID = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldOLType = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldDate = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldSales = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldDZ = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldTurnoverDZ = new DevExpress.XtraPivotGrid.PivotGridField();
            ((System.ComponentModel.ISupportInitialize)(this.uiPivotGrid)).BeginInit();
            this.SuspendLayout();
            // 
            // uiPivotGrid
            // 
            this.uiPivotGrid.Cursor = System.Windows.Forms.Cursors.Default;
            this.uiPivotGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiPivotGrid.Fields.AddRange(new DevExpress.XtraPivotGrid.PivotGridField[] {
            this.pivotGridFieldOLFactName,
            this.pivotGridFieldCustName,
            this.pivotGridFieldOLFactAddress,
            this.pivotGridFieldOLID,
            this.pivotGridFieldOLType,
            this.pivotGridFieldDate,
            this.pivotGridFieldSales,
            this.pivotGridFieldDZ,
            this.pivotGridFieldTurnoverDZ});
            this.uiPivotGrid.Location = new System.Drawing.Point(0, 0);
            this.uiPivotGrid.Name = "uiPivotGrid";
            this.uiPivotGrid.OptionsBehavior.HorizontalScrolling = DevExpress.XtraPivotGrid.PivotGridScrolling.Control;
            this.uiPivotGrid.OptionsPrint.PrintColumnHeaders = DevExpress.Utils.DefaultBoolean.False;
            this.uiPivotGrid.OptionsPrint.PrintDataHeaders = DevExpress.Utils.DefaultBoolean.False;
            this.uiPivotGrid.OptionsPrint.PrintFilterHeaders = DevExpress.Utils.DefaultBoolean.False;
            this.uiPivotGrid.OptionsPrint.PrintRowHeaders = DevExpress.Utils.DefaultBoolean.True;
            this.uiPivotGrid.Size = new System.Drawing.Size(947, 337);
            this.uiPivotGrid.TabIndex = 0;
            this.uiPivotGrid.CustomSummary += new DevExpress.XtraPivotGrid.PivotGridCustomSummaryEventHandler(this.uiPivotGrid_CustomSummary);
            this.uiPivotGrid.CustomCellDisplayText += new DevExpress.XtraPivotGrid.PivotCellDisplayTextEventHandler(this.uiPivotGrid_CustomCellDisplayText);
            this.uiPivotGrid.CustomExportFieldValue += new System.EventHandler<DevExpress.XtraPivotGrid.CustomExportFieldValueEventArgs>(this.uiPivotGrid_CustomExportFieldValue);
            // 
            // pivotGridFieldOLFactName
            // 
            this.pivotGridFieldOLFactName.AllowedAreas = ((DevExpress.XtraPivotGrid.PivotGridAllowedAreas)(((DevExpress.XtraPivotGrid.PivotGridAllowedAreas.RowArea | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.ColumnArea)
                        | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.FilterArea)));
            this.pivotGridFieldOLFactName.AreaIndex = 5;
            this.pivotGridFieldOLFactName.FieldName = "OutletNameColumn";
            this.pivotGridFieldOLFactName.Name = "pivotGridFieldOLFactName";
            this.pivotGridFieldOLFactName.Width = 310;
            // 
            // pivotGridFieldCustName
            // 
            this.pivotGridFieldCustName.AllowedAreas = ((DevExpress.XtraPivotGrid.PivotGridAllowedAreas)(((DevExpress.XtraPivotGrid.PivotGridAllowedAreas.RowArea | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.ColumnArea)
                        | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.FilterArea)));
            this.pivotGridFieldCustName.AreaIndex = 1;
            this.pivotGridFieldCustName.FieldName = "CustomerNameColumn";
            this.pivotGridFieldCustName.Name = "pivotGridFieldCustName";
            // 
            // pivotGridFieldOLFactAddress
            // 
            this.pivotGridFieldOLFactAddress.AllowedAreas = ((DevExpress.XtraPivotGrid.PivotGridAllowedAreas)(((DevExpress.XtraPivotGrid.PivotGridAllowedAreas.RowArea | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.ColumnArea)
                        | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.FilterArea)));
            this.pivotGridFieldOLFactAddress.AreaIndex = 0;
            this.pivotGridFieldOLFactAddress.FieldName = "OutletFactAddressNameColumn";
            this.pivotGridFieldOLFactAddress.Name = "pivotGridFieldOLFactAddress";
            // 
            // pivotGridFieldOLID
            // 
            this.pivotGridFieldOLID.AllowedAreas = ((DevExpress.XtraPivotGrid.PivotGridAllowedAreas)(((DevExpress.XtraPivotGrid.PivotGridAllowedAreas.RowArea | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.ColumnArea)
                        | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.FilterArea)));
            this.pivotGridFieldOLID.AreaIndex = 4;
            this.pivotGridFieldOLID.FieldName = "OutletIDColumn";
            this.pivotGridFieldOLID.Name = "pivotGridFieldOLID";
            // 
            // pivotGridFieldOLType
            // 
            this.pivotGridFieldOLType.AllowedAreas = ((DevExpress.XtraPivotGrid.PivotGridAllowedAreas)(((DevExpress.XtraPivotGrid.PivotGridAllowedAreas.RowArea | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.ColumnArea)
                        | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.FilterArea)));
            this.pivotGridFieldOLType.AreaIndex = 2;
            this.pivotGridFieldOLType.FieldName = "OutletTypeColumn";
            this.pivotGridFieldOLType.Name = "pivotGridFieldOLType";
            // 
            // pivotGridFieldDate
            // 
            this.pivotGridFieldDate.AllowedAreas = ((DevExpress.XtraPivotGrid.PivotGridAllowedAreas)(((DevExpress.XtraPivotGrid.PivotGridAllowedAreas.RowArea | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.ColumnArea)
                        | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.FilterArea)));
            this.pivotGridFieldDate.AreaIndex = 3;
            this.pivotGridFieldDate.FieldName = "DateColumn";
            this.pivotGridFieldDate.GroupInterval = DevExpress.XtraPivotGrid.PivotGroupInterval.Date;
            this.pivotGridFieldDate.Name = "pivotGridFieldDate";
            this.pivotGridFieldDate.UnboundFieldName = "pivotGridFieldDate";
            this.pivotGridFieldDate.ValueFormat.FormatString = "d";
            this.pivotGridFieldDate.ValueFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            // 
            // pivotGridFieldSales
            // 
            this.pivotGridFieldSales.AllowedAreas = ((DevExpress.XtraPivotGrid.PivotGridAllowedAreas)((DevExpress.XtraPivotGrid.PivotGridAllowedAreas.FilterArea | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.DataArea)));
            this.pivotGridFieldSales.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
            this.pivotGridFieldSales.AreaIndex = 0;
            this.pivotGridFieldSales.CellFormat.FormatString = "F2";
            this.pivotGridFieldSales.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.pivotGridFieldSales.FieldName = "SalesInMoneyColumn";
            this.pivotGridFieldSales.Name = "pivotGridFieldSales";
            this.pivotGridFieldSales.SummaryType = DevExpress.Data.PivotGrid.PivotSummaryType.Custom;
            // 
            // pivotGridFieldDZ
            // 
            this.pivotGridFieldDZ.AllowedAreas = ((DevExpress.XtraPivotGrid.PivotGridAllowedAreas)((DevExpress.XtraPivotGrid.PivotGridAllowedAreas.FilterArea | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.DataArea)));
            this.pivotGridFieldDZ.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
            this.pivotGridFieldDZ.AreaIndex = 1;
            this.pivotGridFieldDZ.CellFormat.FormatString = "F2";
            this.pivotGridFieldDZ.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.pivotGridFieldDZ.FieldName = "DZColumn";
            this.pivotGridFieldDZ.Name = "pivotGridFieldDZ";
            this.pivotGridFieldDZ.SummaryType = DevExpress.Data.PivotGrid.PivotSummaryType.Custom;
            // 
            // pivotGridFieldTurnoverDZ
            // 
            this.pivotGridFieldTurnoverDZ.AllowedAreas = ((DevExpress.XtraPivotGrid.PivotGridAllowedAreas)((DevExpress.XtraPivotGrid.PivotGridAllowedAreas.FilterArea | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.DataArea)));
            this.pivotGridFieldTurnoverDZ.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
            this.pivotGridFieldTurnoverDZ.AreaIndex = 2;
            this.pivotGridFieldTurnoverDZ.CellFormat.FormatString = "F2";
            this.pivotGridFieldTurnoverDZ.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.pivotGridFieldTurnoverDZ.FieldName = "TurnoverDZColumn";
            this.pivotGridFieldTurnoverDZ.Name = "pivotGridFieldTurnoverDZ";
            this.pivotGridFieldTurnoverDZ.SummaryType = DevExpress.Data.PivotGrid.PivotSummaryType.Custom;
            this.pivotGridFieldTurnoverDZ.Width = 150;
            // 
            // TurnoverByMoneyReport
            // 
            this.Controls.Add(this.uiPivotGrid);
            this.Name = "TurnoverByMoneyReport";
            this.Size = new System.Drawing.Size(947, 337);
            ((System.ComponentModel.ISupportInitialize)(this.uiPivotGrid)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraPivotGrid.PivotGridControl uiPivotGrid;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldOLFactName;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldOLFactAddress;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldOLID;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldOLType;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldDate;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldSales;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldDZ;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldTurnoverDZ;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldCustName;
    }
}
