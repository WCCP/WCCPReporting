﻿using System;
using System.Collections.Generic;
using System.IO;
using DevExpress.LookAndFeel;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraPivotGrid;
using DevExpress.XtraTab;
using Logica.Reports.BaseReportControl;
using Logica.Reports.BaseReportControl.Utility;
using Logica.Reports.Common;
using Logica.Reports.Common.WaitWindow;
using Logica.Reports.DataProvider;
using Logica.Reports.Localization;
using Logica.Reports.ModelDataProvider.Entities;
using Logica.Reports.SVReport;
using Logica.Reports.SVReport.UserControls.Contracts;

namespace Logica.Reports.UserControls
{
    /// <summary>
    ///   Base class for all SVReports.
    /// </summary>
#if !DEBUG
    public abstract class ReportBase : XtraUserControl, IReportParams
#else
    public class ReportBase : XtraUserControl, IReportParams
#endif
    {
        #region Fields

        protected GridColumn MerchandiserName = new GridColumn {Name = FieldNames.MerchandiserName};
        protected GridColumn OutletAddress = new GridColumn {Name = FieldNames.OutletAddress};
        protected GridColumn OutletDeliveryAddress = new GridColumn {Name = FieldNames.OutletDeliveryAddress};
        protected GridColumn OutletExternalCode = new GridColumn {Name = FieldNames.OutletExternalCode};
        protected GridColumn OutletID = new GridColumn {Name = FieldNames.OutletID};
        protected GridColumn OutletName = new GridColumn {Name = FieldNames.OutletName};
        protected GridColumn OutletStatus = new GridColumn {Name = FieldNames.OutletStatus};
        protected GridColumn OutletSubtype = new GridColumn {Name = FieldNames.OutletSubtype};
        protected GridColumn OutletTradingName = new GridColumn {Name = FieldNames.OutletTradingName};
        protected GridColumn OutletType = new GridColumn {Name = FieldNames.OutletType};
        protected GridColumn SupervisorName = new GridColumn {Name = FieldNames.SupervisorName};
        protected PivotGridField fieldAreaName = new PivotGridField(FieldNames.AreaName, PivotArea.FilterArea);
        protected PivotGridField fieldCityName = new PivotGridField(FieldNames.CityName, PivotArea.FilterArea);
        protected PivotGridField fieldCustName = new PivotGridField(FieldNames.CustomerName, PivotArea.FilterArea);
        protected PivotGridField fieldCustomerName = new PivotGridField(FieldNames.CustomerName, PivotArea.FilterArea);
        protected PivotGridField fieldDistrictName = new PivotGridField(FieldNames.DistrictName, PivotArea.FilterArea);
        protected PivotGridField fieldMerchID = new PivotGridField(FieldNames.MerchandiserID, PivotArea.FilterArea);
        protected PivotGridField fieldMerchName = new PivotGridField(FieldNames.MerchandiserName, PivotArea.RowArea);
        protected PivotGridField fieldMerchStatus = new PivotGridField(FieldNames.MerchandiserStatus, PivotArea.FilterArea);
        protected PivotGridField fieldOLAddress = new PivotGridField(FieldNames.OutletAddress, PivotArea.FilterArea);
        protected PivotGridField fieldOLCardDate = new PivotGridField(FieldNames.OutletCardDate, PivotArea.FilterArea);
        protected PivotGridField fieldOLCity = new PivotGridField(FieldNames.CityName, PivotArea.FilterArea);
        protected PivotGridField fieldOLCode = new PivotGridField(FieldNames.OutletExternalCode, PivotArea.FilterArea);
        protected PivotGridField fieldOLComments = new PivotGridField(FieldNames.Comments, PivotArea.FilterArea);
        protected PivotGridField fieldOLDeliveryAddress = new PivotGridField(FieldNames.OutletDeliveryAddress, PivotArea.FilterArea);
        protected PivotGridField fieldOLExternalCode = new PivotGridField(FieldNames.OutletExternalCode, PivotArea.FilterArea);
        protected PivotGridField fieldOLID = new PivotGridField(FieldNames.OutletID, PivotArea.FilterArea);
        protected PivotGridField fieldOLName = new PivotGridField(FieldNames.OutletName, PivotArea.FilterArea);
        protected PivotGridField fieldOLStatus = new PivotGridField(FieldNames.OutletStatus, PivotArea.FilterArea);
        protected PivotGridField fieldOLSubTypeName = new PivotGridField(FieldNames.OutletSubtype, PivotArea.FilterArea);
        protected PivotGridField fieldOLTradingName = new PivotGridField(FieldNames.OutletTradingName, PivotArea.FilterArea);
        protected PivotGridField fieldOLTypeName = new PivotGridField(FieldNames.OutletType, PivotArea.FilterArea);
        protected PivotGridField fieldPackingCapacity = new PivotGridField(FieldNames.PackingCapacity, PivotArea.FilterArea);
        protected PivotGridField fieldPackingType = new PivotGridField(FieldNames.PackingType, PivotArea.FilterArea);
        protected PivotGridField fieldPopulation = new PivotGridField(FieldNames.Population, PivotArea.FilterArea);
        protected PivotGridField fieldProductCnName = new PivotGridField(FieldNames.ProductCnName, PivotArea.FilterArea);
        protected PivotGridField fieldProductGroupName = new PivotGridField(FieldNames.ProductGroupName, PivotArea.FilterArea);
        protected PivotGridField fieldProductID = new PivotGridField(FieldNames.ProductID, PivotArea.FilterArea);
        protected PivotGridField fieldProductName = new PivotGridField(FieldNames.ProductName, PivotArea.FilterArea);
        protected PivotGridField fieldProductTypeName = new PivotGridField(FieldNames.ProductTypeName, PivotArea.FilterArea);
        protected PivotGridField fieldProximityFactor = new PivotGridField(FieldNames.ProximityFactor, PivotArea.FilterArea);
        protected PivotGridField fieldRegionName = new PivotGridField(FieldNames.RegionName, PivotArea.FilterArea);
        protected PivotGridField fieldStatus = new PivotGridField(FieldNames.OutletStatus, PivotArea.FilterArea);
        protected PivotGridField fieldSupervisorName = new PivotGridField(FieldNames.SupervisorName, PivotArea.RowArea);
        protected PivotGridField fieldSvName = new PivotGridField(FieldNames.SupervisorName, PivotArea.FilterArea);
        protected PivotGridField fieldTerritoryType = new PivotGridField(FieldNames.TerritoryType, PivotArea.FilterArea);
        protected PivotGridField fieldUnitName = new PivotGridField(FieldNames.UnitName, PivotArea.FilterArea);
        private GridControl headerGridControl;
        private GridView headerGridView;

        #endregion

        #region Instance Methods

        /// <summary>
        ///   Populate report.
        /// </summary>
#if !DEBUG
        protected abstract void PopulateReportEngine();
#else
        protected virtual void PopulateReportEngine() { }
#endif

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            if (ContentControl is ISupportLookAndFeel)
            {
                ISupportLookAndFeel cnt = ContentControl as ISupportLookAndFeel;
                cnt.LookAndFeel.Assign(SettingProvider.SkinStyle);
            }
        }

        #endregion

        #region IReportParams Members

        /// <summary>
        ///   Gets or sets the report start date.
        /// </summary>
        /// <value>The report start date.</value>
        public DateTime ReportStartDate { get; set; }

        /// <summary>
        ///   Gets or sets the report end date.
        /// </summary>
        /// <value>The report end date.</value>
        public DateTime ReportEndDate { get; set; }

        /// <summary>
        ///   Gets or sets the merchandiser collection.
        /// </summary>
        /// <value>The merchandiser collection.</value>
        public List<Merchandiser> MerchandiserCollection { get; set; }

        /// <summary>
        ///   Gets or sets the supervisor collection.
        /// </summary>
        /// <value>The supervisor collection.</value>
        public List<Supervisor> SupervisorCollection { get; set; }

        /// <summary>
        ///   Populates the report.
        /// </summary>
        public void PopulateReport()
        {
            WaitManager.StartWait();
            //COMMENT: unfortunately PivotGridControl and GridControl don't have the same SaveLayout interface
            try
            {
                if (ContentControl is PivotGridControl)
                {
                    PivotGridControl pgc = ContentControl as PivotGridControl;
                    MemoryStream ms = new MemoryStream();

                    PivotGridOptionsLayout option = new PivotGridOptionsLayout();
                    option.Columns.StoreAllOptions = true;
                    option.StoreAllOptions = true;
                    pgc.SaveLayoutToStream(ms, option);
                    PopulateReportEngine();
                    ms.Seek(0, SeekOrigin.Begin);
                    pgc.RestoreLayoutFromStream(ms, option);
                }
                else if (ContentControl is GridControl)
                {
                    PopulateReportEngine();
                }
                TabOperationHelper.CreateSettingSection(ParentTabPage, SheetParamCollection, ref headerGridControl, ref headerGridView);
            }
            finally
            {
                WaitManager.StopWait();
            }
        }

        public SheetParamCollection SheetParamCollection
        {
            get
            {
                SheetParamCollection sheetParamCollection = new SheetParamCollection
                                                                {
                                                                    new SheetParam {DisplayParamName = " ", DisplayValue = " "},
                                                                    new SheetParam
                                                                        {
                                                                            DisplayParamName = "Период c",
                                                                            DisplayValue = ReportStartDate.ToShortDateString()
                                                                        },
                                                                    new SheetParam
                                                                        {
                                                                            DisplayParamName = "по",
                                                                            DisplayValue = ReportEndDate.ToShortDateString()
                                                                        }
                                                                };
                sheetParamCollection.MainHeader = ParentTabPage.Text;
                return sheetParamCollection;
            }
        }

        /// <summary>
        ///   Allows to customize the columns.
        /// </summary>
#if !DEBUG
        public abstract void SetupColumns();
#else
        public virtual void SetupColumns() { }
#endif

        public XtraTabPage ParentTabPage { get; set; }

        /// <summary>
        ///   Gets the type of the report.
        /// </summary>
        /// <value>The type of the report.</value>
#if !DEBUG
        public abstract ReportType RepType { get; }
#else
        public virtual ReportType RepType { get { return ReportType.ActiveOutletsTop10; } }
#endif
        /// <summary>
        ///   Gets the grid of the report.
        /// </summary>
        /// <value>The grid object.</value>
#if !DEBUG
        public abstract object ContentControl { get; }
#else
        public virtual object ContentControl
        {
            get { return null; }
        }
#endif
        public GridControl HeaderGrid
        {
            get { return headerGridControl; }
        }

        public virtual bool ExportHeaderGrid
        {
            get { return true; }
        }

        #endregion
    }
}