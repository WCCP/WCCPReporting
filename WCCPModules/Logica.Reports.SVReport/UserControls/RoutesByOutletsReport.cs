﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Logica.Reports.SVReport;
using Logica.Reports.DataProvider.Providers;
using Logica.Reports.DataProvider.Entities;
using DevExpress.XtraPivotGrid;
using Logica.Reports.DataProvider;
using DevExpress.XtraGrid.Columns;
using Logica.Reports.Localization;

namespace Logica.Reports.UserControls
{
    public partial class RoutesByOutletsReport : ReportBase
    {
        #region IReportParams Members

        /// <summary>
        /// Gets the name of the report.
        /// </summary>
        /// <value>The name of the report.</value>
        public override ReportType RepType
        {
            get
            {
                return ReportType.RoutesByOutlets;
            }
        }

        /// <summary>
        /// Populates the report.
        /// </summary>
        protected override void PopulateReportEngine()
        {
            RoutesByOutletsDataProvider dataProvider = new RoutesByOutletsDataProvider();
            RoutesByOutletsDataSet.ReportEntriesDataTable reportData = dataProvider.GetReportData(ReportStartDate, ReportEndDate, MerchandiserCollection);
            uiGrid.DataSource = reportData;
            gridView.BestFitColumns();
        }
        public override object ContentControl
        {
            get
            {
                return uiGrid;
            }
        }
        #endregion        

        public RoutesByOutletsReport()
        {
            InitializeComponent();
            Dock = DockStyle.Fill;
            SetupColumns();
            gridView.OptionsView.ColumnAutoWidth = false;
            gridView.OptionsBehavior.Editable = false;
            gridView.OptionsSelection.MultiSelect = true;
        }
        #region Helper methods
        /// <summary>
        /// Allows to customize the columns.
        /// </summary>
        public override void SetupColumns()
        {
            gridView.Columns.Clear();
            //gridView.Columns.AddRange(new GridColumn[] { 
            //OutletTradingName,OutletAddress,OutletDeliveryAddress,OutletName, OutletID, OutletExternalCode,
            //SupervisorName, MerchandiserName, OutletType, OutletSubtype});
            GridColumn beginTime = new GridColumn() { Name = FieldNames.BeginTime };
            GridColumn endTime = new GridColumn() { Name = FieldNames.EndTime };
            endTime.DisplayFormat.FormatType = beginTime.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            endTime.DisplayFormat.FormatString = beginTime.DisplayFormat.FormatString = "HH:mm:ss";

            gridView.Columns.Add(new GridColumn() { Name = FieldNames.Date });
            gridView.Columns.AddRange(new GridColumn[] { SupervisorName, MerchandiserName });
            gridView.Columns.Add(new GridColumn() { Name = FieldNames.RouteName });
            gridView.Columns.Add(beginTime);
            gridView.Columns.Add(endTime);
            gridView.Columns.Add(new GridColumn() { Name = FieldNames.Duration });
            gridView.Columns.Add(new GridColumn() { Name = FieldNames.OrdersCount });
            gridView.Columns.Add(new GridColumn() { Name = FieldNames.RemaindersCount });
            gridView.Columns.Add(new GridColumn() { Name = FieldNames.SalesCount });
            gridView.Columns.Add(new GridColumn() { Name = FieldNames.Comments });
            gridView.Columns.AddRange(new GridColumn[] { OutletID, OutletExternalCode, OutletTradingName, OutletAddress, 
                                                         OutletDeliveryAddress, OutletName, OutletType, OutletSubtype });        

            foreach (GridColumn col in gridView.Columns)
            {
                col.FieldName = col.Name;
                col.Caption = LocalizationProvider.GetText(col.Name);
                col.Visible = true;
            }
        }
        #endregion
    }
}
