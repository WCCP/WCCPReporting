﻿using System;
using System.Windows.Forms;
using DevExpress.Data.PivotGrid;
using DevExpress.XtraPivotGrid;
using Logica.Reports.DataProvider.Providers;
using Logica.Reports.Localization;
using Logica.Reports.DataProvider;
using Logica.Reports.UserControls;
using DevExpress.Utils;

namespace Logica.Reports.SVReport.UserControls
{
    /// <summary>
    /// Represents ProductsMotion report as a part of SVReport module.
    /// </summary>
    public partial class ProductsMotionReport : ReportBase {
        PivotGridField fieldOrderedProductVolume;
        PivotGridField fieldSaledOrderedProds;
        PivotGridField fieldToTakeOffProds;
        PivotGridField fieldRemainsProds;
        PivotGridField fieldWithoutOrder;
        PivotGridField fieldOverOrdered;
        PivotGridField fieldIsTop;
        PivotGridField fieldIsRecommendedOrder;
        PivotGridField fieldRecomendeProductVolume;
        PivotGridField fieldRecomendeProductQty;
        PivotGridField fieldUseRecommnededOrder;
        PivotGridField fieldVolumeRecommnededOrder;
        PivotGridField fieldOrderedProductQty;
        PivotGridField fieldRemainsProductQty;
        PivotGridField fieldRecOrderCount;
     

        
        


        public ProductsMotionReport() {
            InitializeComponent();
            Dock = DockStyle.Fill;
            SetupColumns();
            uiPivotGrid.CustomCellDisplayText += new PivotCellDisplayTextEventHandler(uiPivotGrid_CustomCellDisplayText);
            uiPivotGrid.CustomExportCell += new EventHandler<CustomExportCellEventArgs>(uiPivotGrid_CustomExportCell); 
        }

        #region IReportParams Members

        /// <summary>
        /// Gets the name of the report.
        /// </summary>
        /// <value>The name of the report.</value>
        public override ReportType RepType {
            get { return ReportType.ProductsMotion; }
        }

        /// <summary>
        /// Populates the report.
        /// </summary>
        protected override void PopulateReportEngine() {
            ProductsMotionDataProvider dataProvider = new ProductsMotionDataProvider();
            uiPivotGrid.DataSource = dataProvider.GetReportData(ReportStartDate, ReportEndDate, MerchandiserCollection);
        }

        public override object ContentControl {
            get { return uiPivotGrid; }
        }

        /// <summary>
        /// Allows to customize the columns.
        /// </summary>
        public override void SetupColumns() {
            uiPivotGrid.Fields.Clear();
            PivotGridField fieldProxFactor = new PivotGridField(FieldNames.ProximityFactor, PivotArea.FilterArea);
            PivotGridField fieldOrderDate = new PivotGridField(FieldNames.OrderDate, PivotArea.FilterArea);
            fieldOrderedProductVolume = new PivotGridField(FieldNames.OrderedProductVolume, PivotArea.DataArea);
            fieldSaledOrderedProds = new PivotGridField(FieldNames.SaledOrderedProductVolume, PivotArea.DataArea);
            fieldToTakeOffProds = new PivotGridField(FieldNames.ToTakeOffProductVolume, PivotArea.DataArea);
            fieldRemainsProds = new PivotGridField(FieldNames.RemainsProductVolume, PivotArea.DataArea);
            fieldWithoutOrder = new PivotGridField(FieldNames.WithoutOrder, PivotArea.DataArea);
            fieldOverOrdered = new PivotGridField(FieldNames.OverOrdered, PivotArea.DataArea);
            
            fieldIsTop = new PivotGridField(FieldNames.IsTop, PivotArea.FilterArea);            
            fieldIsRecommendedOrder = new PivotGridField(FieldNames.IsRecommendedOrder, PivotArea.FilterArea);
            

            fieldRecomendeProductVolume = new PivotGridField(FieldNames.RecomendProductVolume, PivotArea.DataArea);
            fieldRecomendeProductQty = new PivotGridField(FieldNames.RecomendProductQty, PivotArea.DataArea);

            fieldRemainsProductQty = new PivotGridField(FieldNames.RemainsProductQty, PivotArea.DataArea);

            fieldUseRecommnededOrder = new PivotGridField(FieldNames.UseRecommnededOrder, PivotArea.DataArea);
            fieldVolumeRecommnededOrder = new PivotGridField(FieldNames.VolumeRecommnededOrder, PivotArea.DataArea);
            fieldOrderedProductQty = new PivotGridField(FieldNames.OrderedProductQty, PivotArea.DataArea);
            fieldRecOrderCount = new PivotGridField(FieldNames.RecOrderCount, PivotArea.DataArea); 

            fieldOrderDate.ValueFormat.FormatType = fieldOrderDate.CellFormat.FormatType = FormatType.DateTime;
            fieldOrderDate.ValueFormat.FormatString = fieldOrderDate.CellFormat.FormatString = "d";

            fieldOrderedProductVolume.CellFormat.FormatType = fieldSaledOrderedProds.CellFormat.FormatType = fieldToTakeOffProds.CellFormat.FormatType =
                fieldRemainsProds.CellFormat.FormatType = fieldWithoutOrder.CellFormat.FormatType = fieldOverOrdered.CellFormat.FormatType =
                fieldRecomendeProductVolume.CellFormat.FormatType = fieldRecomendeProductVolume.CellFormat.FormatType =
                fieldOrderedProductQty.CellFormat.FormatType = fieldRecomendeProductQty.CellFormat.FormatType =
                fieldRemainsProductQty.CellFormat.FormatType = fieldRemainsProductQty.CellFormat.FormatType =
                fieldUseRecommnededOrder.CellFormat.FormatType = fieldVolumeRecommnededOrder.CellFormat.FormatType = FormatType.Numeric;

            fieldOrderedProductVolume.CellFormat.FormatString = fieldSaledOrderedProds.CellFormat.FormatString = fieldToTakeOffProds.CellFormat.FormatString =
                fieldRemainsProds.CellFormat.FormatString = fieldWithoutOrder.CellFormat.FormatString = fieldOverOrdered.CellFormat.FormatString =
                fieldRecomendeProductVolume.CellFormat.FormatString = fieldRecomendeProductQty.CellFormat.FormatString = "n3";
            fieldUseRecommnededOrder.CellFormat.FormatString = fieldVolumeRecommnededOrder.CellFormat.FormatString = "n2";

            fieldUseRecommnededOrder.SummaryType = fieldVolumeRecommnededOrder.SummaryType = PivotSummaryType.Custom;

            uiPivotGrid.Fields.AddRange(new PivotGridField[] {
                fieldSvName, fieldMerchName, fieldOLTypeName, fieldOLSubTypeName, 
                fieldOLName, fieldOLDeliveryAddress, fieldOLTradingName, fieldOLAddress,
                fieldCityName, fieldDistrictName, fieldOLID, fieldOLExternalCode, 
                fieldStatus, fieldCustName, fieldRegionName, fieldMerchID, 
                fieldMerchStatus,fieldProductID, fieldProductName, fieldProductGroupName,
                fieldProductTypeName,fieldAreaName, fieldTerritoryType, fieldUnitName,
                fieldPackingType,fieldPackingCapacity, fieldProductCnName, fieldProxFactor, 
                /*fieldHLCode, fieldSaleDate,*/ fieldOrderDate, fieldOrderedProductVolume, fieldRemainsProductQty, fieldOrderedProductQty,
                fieldRemainsProds, fieldSaledOrderedProds,fieldToTakeOffProds, fieldWithoutOrder,
                fieldOverOrdered, fieldPopulation, fieldIsTop, fieldIsRecommendedOrder,
                fieldRecomendeProductVolume, fieldRecomendeProductQty, fieldUseRecommnededOrder, fieldVolumeRecommnededOrder, fieldRecOrderCount});

            foreach (PivotGridField field in uiPivotGrid.Fields)
                field.Caption = LocalizationProvider.GetText(field.FieldName);
           
            //uiPivotGrid.Fields.FieldByName("OrderedProductQtyColumn").Visible = false;
            
        }

        

        #endregion
        
        void uiPivotGrid_CustomExportCell(object sender, CustomExportCellEventArgs e) {
            if (e.DataField == fieldOrderedProductVolume || e.DataField == fieldToTakeOffProds || e.DataField == fieldRemainsProds ||
                e.DataField == fieldSaledOrderedProds || e.DataField == fieldWithoutOrder || e.DataField == fieldOverOrdered)
            {
                ((DevExpress.XtraPrinting.ITextBrick)e.Brick).TextValue = e.Text;
            }
        }

        void uiPivotGrid_CustomCellDisplayText(object sender, PivotCellDisplayTextEventArgs e) {
            if(e.DataField == fieldOrderedProductVolume || e.DataField == fieldToTakeOffProds|| e.DataField == fieldRemainsProds|| 
                e.DataField == fieldSaledOrderedProds || e.DataField == fieldWithoutOrder || e.DataField == fieldOverOrdered)
            {
                Double tmpValue;
                if (null != e.Value && Double.TryParse(e.Value.ToString(), out tmpValue) && tmpValue == 0)
                    e.DisplayText = "";
            }
        }

        private void uiPivotGrid_CustomSummary(object sender, PivotGridCustomSummaryEventArgs e) {
            if (e.DataField == fieldUseRecommnededOrder) {
                decimal lRecOrderCount = 0;
                decimal lRecOrderCountTotal = 0;
                PivotDrillDownDataSource lDataSource = e.CreateDrillDownDataSource();
                for (int i = 0; i < lDataSource.RowCount; i++) {
                    PivotDrillDownDataRow lRow = lDataSource[i];
                    if (1 == (decimal) lRow[fieldUseRecommnededOrder] && 0 < (decimal) lRow[fieldOrderedProductQty])
                        lRecOrderCount += (int) lRow[fieldRecOrderCount];
                    lRecOrderCountTotal += (int) lRow[fieldRecOrderCount];
                }
                
                // Calculate the percentage.
                if (lRecOrderCountTotal > 0)
                    e.CustomValue = lRecOrderCount / lRecOrderCountTotal * 100;
            }
            else if (e.DataField == fieldVolumeRecommnededOrder)
            {
                decimal lOrderVolumeSum = 0;
                decimal lVolumeTotal = 0;
                PivotDrillDownDataSource lDataSource = e.CreateDrillDownDataSource();
                for (int i = 0; i < lDataSource.RowCount; i++)
                {
                    PivotDrillDownDataRow lRow = lDataSource[i];
                    if (1 == (decimal) lRow[fieldUseRecommnededOrder])
                        lOrderVolumeSum += (decimal) lRow[fieldVolumeRecommnededOrder];
                    lVolumeTotal += (decimal) lRow[fieldVolumeRecommnededOrder];
                }
                // Calculate the percentage.
                if (lVolumeTotal > 0)
                    e.CustomValue = lOrderVolumeSum / lVolumeTotal * 100;
            }
        }
    }
}
