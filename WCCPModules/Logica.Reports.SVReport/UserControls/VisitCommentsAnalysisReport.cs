﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Logica.Reports.SVReport.UserControls;
using Logica.Reports.DataProvider.Providers;
using Logica.Reports.DataProvider.Entities;
using DevExpress.XtraGrid;
using Logica.Reports.DataProvider;
using DevExpress.XtraGrid.Views.BandedGrid;
using Logica.Reports.ModelDataProvider.Entities;
using Logica.Reports.SVReport;
using Logica.Reports.Localization;
using DevExpress.XtraGrid.Columns;

namespace Logica.Reports.UserControls
{
    /// <summary>
    /// Represents VisitCommentsAnalysis report as a part of SVReport module.
    /// </summary>
    public partial class VisitCommentsAnalysisReport : ReportBase
    {
        #region IReportParams Members

        /// <summary>
        /// Gets the name of the report.
        /// </summary>
        /// <value>The name of the report.</value>
        public override ReportType RepType
        {
            get
            {
                return ReportType.VisitCommentsAnalysis;
            }
        }

        /// <summary>
        /// Populates the report.
        /// </summary>
        protected override void  PopulateReportEngine()
        {
            VisitCommentsAnalysisDataProvider dataProvider = new VisitCommentsAnalysisDataProvider();
            VisitCommentsAnalysisDataSet.ReportEntriesDataTable reportData = dataProvider.GetReportData(ReportStartDate, ReportEndDate, MerchandiserCollection);
            uiGrid.DataSource = reportData;
            gridView.BestFitColumns();
        }
        public override object ContentControl
        {
            get
            {
                return uiGrid;
            }
        }
        #endregion // IReportParams Members

        #region Constructors

        public VisitCommentsAnalysisReport()
        {
            InitializeComponent();
            Dock = DockStyle.Fill;
            SetupColumns();
            gridView.OptionsView.ColumnAutoWidth = false;
            gridView.OptionsBehavior.Editable = false;
        }

        #endregion // Constructors

        #region Helper methods
        /// <summary>
        /// Allows to customize the columns.
        /// </summary>
        public override void SetupColumns()
        {
            gridView.Columns.Clear();

            gridView.Columns.AddRange(new GridColumn[]{
                SupervisorName, MerchandiserName ,OutletID, OutletSubtype ,
                OutletType, OutletName, OutletDeliveryAddress, OutletTradingName,
                OutletAddress,OutletStatus, });
            gridView.Columns.Add(new GridColumn() { Name = FieldNames.MerchandiserStatus});
            GridColumn colOutletCardDate = new GridColumn() { Name = FieldNames.OutletCardDate };
            colOutletCardDate.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            colOutletCardDate.DisplayFormat.FormatString = "d";
            gridView.Columns.Add(colOutletCardDate); 
            gridView.Columns.Add(new GridColumn() { Name = FieldNames.Reason});
            gridView.Columns.Add(new GridColumn() { Name = FieldNames.NotVisitedComments});
            gridView.Columns.Add(new GridColumn() { Name = FieldNames.CommentsByVisit});
            
            foreach (GridColumn col in gridView.Columns)
            {
                col.FieldName = col.Name;
                col.Caption = LocalizationProvider.GetText(col.Name);
                col.Visible = true;
            }
        }

        #endregion
    }
}
