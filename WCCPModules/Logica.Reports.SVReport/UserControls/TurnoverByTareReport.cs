﻿#region

using System;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraPivotGrid;
using DevExpress.XtraPrinting;
using Logica.Reports.BaseReportControl.Utility;
using Logica.Reports.DataProvider.Providers;
using Logica.Reports.Localization;
using Logica.Reports.UserControls;

#endregion

namespace Logica.Reports.SVReport.UserControls
{
    /// <summary>
    ///   Represents FailedVisits report as a part of SVReport module.
    /// </summary>
    public sealed partial class TurnoverByTareReport : ReportBase
    {
        #region Constructors

        public TurnoverByTareReport()
        {
            InitializeComponent();
            Dock = DockStyle.Fill;
            SetupColumns();
        }

        #endregion

        #region Instance Properties

        public override object ContentControl
        {
            get { return uiPivotGrid; }
        }

        public override bool ExportHeaderGrid
        {
            get { return false; }
        }

        public override ReportType RepType
        {
            get { return ReportType.TurnoverByTare; }
        }

        #endregion

        #region Instance Methods

        /// <summary>
        ///   Allows to customize the columns.
        /// </summary>
        public override void SetupColumns()
        {
            foreach (PivotGridField field in uiPivotGrid.Fields)
            {
                field.Caption = LocalizationProvider.GetText(string.Format("{0}_{1}", RepType, field.FieldName));
            }
        }

        protected override void PopulateReportEngine()
        {
            TurnoverByTareDataProvider dataProvider = new TurnoverByTareDataProvider();
            var reportData = dataProvider.GetReportData(ReportStartDate, ReportEndDate, MerchandiserCollection);
            uiPivotGrid.DataSource = reportData;
        }

        private decimal? CalculateAvgBySum(PivotDrillDownDataSource dataSource, PivotGridFieldBase dataField)
        {
            var pivotDrillDownDataRows =
                dataSource.Cast<PivotDrillDownDataRow>().Where(r => r[dataField] != null).ToArray();
            if (pivotDrillDownDataRows.Length == 0)
                return null;
            return (from p in pivotDrillDownDataRows
                    group p by p[pivotGridFieldDate]
                    into g
                    select g.Sum(r => ConvertEx.ToDecimal(r[dataField]))).Average();
        }

        #endregion

        #region Event Handling

        private void uiPivotGrid_CustomCellDisplayText(object sender, PivotCellDisplayTextEventArgs e)
        {
            if (e.Value is decimal)
            {
                var val = (decimal) e.Value;

                if ((val%1) == 0)
                {
                    e.DisplayText = val.ToString("N0");
                }
            }
        }

        private void uiPivotGrid_CustomExportFieldValue(object sender, CustomExportFieldValueEventArgs e)
        {
            if (e.Field == e.DataField)
            {
                TextBrick brick = e.Brick as TextBrick;
                if (brick == null) return;
                brick.TextValue = e.DataField.Caption;
            }
        }

        private void uiPivotGrid_CustomSummary(object sender, PivotGridCustomSummaryEventArgs e)
        {
            PivotDrillDownDataSource pivotDrillDownDataSource = e.CreateDrillDownDataSource();
            if (e.DataField == pivotGridFieldSales || e.DataField == pivotGridFieldDZ)
            {
                e.CustomValue = CalculateAvgBySum(pivotDrillDownDataSource, e.DataField);
            }
            else if (e.DataField == pivotGridFieldTurnoverDZ)
            {
                decimal? sales = CalculateAvgBySum(pivotDrillDownDataSource, pivotGridFieldSales);
                decimal dz = CalculateAvgBySum(pivotDrillDownDataSource, pivotGridFieldDZ) ?? 0;
                if (!sales.HasValue)
                    e.CustomValue = null;
                else if (sales == 0)
                    e.CustomValue = (decimal) 0.0;
                else
                    e.CustomValue = Math.Round((decimal) (dz*(decimal) 30.0/sales), 2);
            }
        }

        #endregion
    }
}