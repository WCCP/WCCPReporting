﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Logica.Reports.SVReport.UserControls;
using Logica.Reports.DataProvider.Providers;
using Logica.Reports.DataProvider.Entities;
using DevExpress.XtraGrid;
using Logica.Reports.DataProvider;
using DevExpress.XtraGrid.Views.BandedGrid;
using Logica.Reports.ModelDataProvider.Entities;
using Logica.Reports.SVReport;
using Logica.Reports.Localization;
using DevExpress.XtraGrid.Columns;

namespace Logica.Reports.UserControls
{
    /// <summary>
    /// Represents OutletBase report as a part of SVReport module.
    /// </summary>
    public partial class OutletBaseReport : ReportBase
    {
        #region IReportParams Members

        /// <summary>
        /// Gets the name of the report.
        /// </summary>
        /// <value>The name of the report.</value>
        public override ReportType RepType
        {
            get
            {
                return ReportType.OutletBase;
            }
        }

        /// <summary>
        /// Populates the report.
        /// </summary>
        protected override void  PopulateReportEngine()
        {
            OutletBaseDataProvider dataProvider = new OutletBaseDataProvider();
            OutletBaseDataSet.ReportEntriesDataTable reportData = dataProvider.GetReportData(ReportStartDate, ReportEndDate, MerchandiserCollection);
            uiGrid.DataSource = reportData;
            gridView.BestFitColumns();
        }
        public override object ContentControl
        {
            get
            {
                return uiGrid;
            }
        }
        #endregion // IReportParams Members

        #region Constructors

        public OutletBaseReport()
        {
            InitializeComponent();
            Dock = DockStyle.Fill;
            SetupColumns();
            gridView.OptionsView.ColumnAutoWidth = false;
            gridView.OptionsBehavior.Editable = false;
            gridView.OptionsSelection.MultiSelect = true;
        }

        #endregion // Constructors

        #region Helper methods
        /// <summary>
        /// Allows to customize the columns.
        /// </summary>
        public override void SetupColumns()
        {
            gridView.Columns.Clear();
            
            gridView.Columns.AddRange(new GridColumn[]{
                OutletID, OutletExternalCode , OutletSubtype ,OutletType, 
                OutletName, OutletDeliveryAddress, OutletTradingName, OutletAddress,
                OutletStatus, SupervisorName, MerchandiserName });
            gridView.Columns.Add(new GridColumn() { Name = FieldNames.DistrictName});
            gridView.Columns.Add(new GridColumn() { Name = FieldNames.CityName}); 
            gridView.Columns.Add(new GridColumn() { Name = FieldNames.OutletDirector});
            gridView.Columns.Add(new GridColumn() { Name = FieldNames.PrevMonthBottledSales});
            gridView.Columns.Add(new GridColumn() { Name = FieldNames.PrevMonthKegSales});
            gridView.Columns.Add(new GridColumn() { Name = FieldNames.CurMonthBottledSales});
            gridView.Columns.Add(new GridColumn() { Name = FieldNames.CurMonthKegSales});
            gridView.Columns.Add(new GridColumn() { Name = FieldNames.FridgeNumber});
            gridView.Columns.Add(new GridColumn() { Name = FieldNames.CoolerNumber});
            gridView.Columns.Add(new GridColumn() { Name = FieldNames.PlanedVisitsCount});
            gridView.Columns.Add(new GridColumn() { Name = FieldNames.TotalVisitsCount});
            gridView.Columns.Add(new GridColumn() { Name = FieldNames.ProximityFactor});
            gridView.Columns.Add(new GridColumn() { Name = FieldNames.OutletPhone});
            gridView.Columns.Add(new GridColumn() { Name = FieldNames.OutletFax});
            gridView.Columns.Add(new GridColumn() { Name = FieldNames.OutletEmail});
            gridView.Columns.Add(new GridColumn() { Name = FieldNames.OutletPurchaseManager});
            gridView.Columns.Add(new GridColumn() { Name = FieldNames.OutletMarketingManager});
            gridView.Columns.Add(new GridColumn() { Name = FieldNames.OutletMarketingManagerPhone});
            gridView.Columns.Add(new GridColumn() { Name = FieldNames.OutletAccountant});
            gridView.Columns.Add(new GridColumn() { Name = FieldNames.OutletAccountantPhone});
            gridView.Columns.Add(new GridColumn() { Name = FieldNames.ContractNumber});
            gridView.Columns.Add(new GridColumn() { Name = FieldNames.ContractDate});
            gridView.Columns.Add(new GridColumn() { Name = FieldNames.OutletAccount});
            gridView.Columns.Add(new GridColumn() { Name = FieldNames.BankCode}); 
            gridView.Columns.Add(new GridColumn() { Name = FieldNames.BankName}); 
            gridView.Columns.Add(new GridColumn() { Name = FieldNames.BankAddress});
            gridView.Columns.Add(new GridColumn() { Name = FieldNames.ZKPO});
            gridView.Columns.Add(new GridColumn() { Name = FieldNames.VATN});
            gridView.Columns.Add(new GridColumn() { Name = FieldNames.IPN});
            gridView.Columns.Add(new GridColumn() { Name = FieldNames.OutletSize});
            gridView.Columns.Add(new GridColumn() { Name = FieldNames.OutletWarehouseSize});
            gridView.Columns.Add(new GridColumn() { Name = FieldNames.AreaName});

            foreach (GridColumn col in gridView.Columns)
            {
                col.FieldName = col.Name;
                col.Caption = LocalizationProvider.GetText(col.Name);
                col.Visible = true;
            }
        }

        #endregion
    }
}
