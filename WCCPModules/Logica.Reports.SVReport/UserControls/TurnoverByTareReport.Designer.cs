﻿namespace Logica.Reports.SVReport.UserControls
{
    sealed partial class TurnoverByTareReport
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.uiPivotGrid = new DevExpress.XtraPivotGrid.PivotGridControl();
            this.pivotGridFieldOLFactName = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldOLFactAddress = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldM2 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldM1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldOLID = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldDate = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldTare = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldTareType = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldSales = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldDZ = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldTurnoverDZ = new DevExpress.XtraPivotGrid.PivotGridField();
            ((System.ComponentModel.ISupportInitialize)(this.uiPivotGrid)).BeginInit();
            this.SuspendLayout();
            // 
            // uiPivotGrid
            // 
            this.uiPivotGrid.Cursor = System.Windows.Forms.Cursors.Default;
            this.uiPivotGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiPivotGrid.Fields.AddRange(new DevExpress.XtraPivotGrid.PivotGridField[] {
            this.pivotGridFieldOLFactName,
            this.pivotGridFieldOLFactAddress,
            this.pivotGridFieldM2,
            this.pivotGridFieldM1,
            this.pivotGridFieldOLID,
            this.pivotGridFieldDate,
            this.pivotGridFieldTare,
            this.pivotGridFieldTareType,
            this.pivotGridFieldSales,
            this.pivotGridFieldDZ,
            this.pivotGridFieldTurnoverDZ});
            this.uiPivotGrid.Location = new System.Drawing.Point(0, 0);
            this.uiPivotGrid.Name = "uiPivotGrid";
            this.uiPivotGrid.OptionsBehavior.HorizontalScrolling = DevExpress.XtraPivotGrid.PivotGridScrolling.Control;
            this.uiPivotGrid.OptionsDataField.RowValueLineCount = 2;
            this.uiPivotGrid.OptionsPrint.PrintColumnHeaders = DevExpress.Utils.DefaultBoolean.False;
            this.uiPivotGrid.OptionsPrint.PrintDataHeaders = DevExpress.Utils.DefaultBoolean.False;
            this.uiPivotGrid.OptionsPrint.PrintFilterHeaders = DevExpress.Utils.DefaultBoolean.False;
            this.uiPivotGrid.OptionsPrint.PrintRowHeaders = DevExpress.Utils.DefaultBoolean.True;
            this.uiPivotGrid.Size = new System.Drawing.Size(815, 385);
            this.uiPivotGrid.TabIndex = 0;
            this.uiPivotGrid.CustomSummary += new DevExpress.XtraPivotGrid.PivotGridCustomSummaryEventHandler(this.uiPivotGrid_CustomSummary);
            this.uiPivotGrid.CustomCellDisplayText += new DevExpress.XtraPivotGrid.PivotCellDisplayTextEventHandler(this.uiPivotGrid_CustomCellDisplayText);
            this.uiPivotGrid.CustomExportFieldValue += new System.EventHandler<DevExpress.XtraPivotGrid.CustomExportFieldValueEventArgs>(this.uiPivotGrid_CustomExportFieldValue);
            // 
            // pivotGridFieldOLFactName
            // 
            this.pivotGridFieldOLFactName.AllowedAreas = ((DevExpress.XtraPivotGrid.PivotGridAllowedAreas)(((DevExpress.XtraPivotGrid.PivotGridAllowedAreas.RowArea | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.ColumnArea)
                        | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.FilterArea)));
            this.pivotGridFieldOLFactName.AreaIndex = 5;
            this.pivotGridFieldOLFactName.FieldName = "OutletNameColumn";
            this.pivotGridFieldOLFactName.Name = "pivotGridFieldOLFactName";
            this.pivotGridFieldOLFactName.Width = 310;
            // 
            // pivotGridFieldOLFactAddress
            // 
            this.pivotGridFieldOLFactAddress.AllowedAreas = ((DevExpress.XtraPivotGrid.PivotGridAllowedAreas)(((DevExpress.XtraPivotGrid.PivotGridAllowedAreas.RowArea | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.ColumnArea)
                        | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.FilterArea)));
            this.pivotGridFieldOLFactAddress.AreaIndex = 0;
            this.pivotGridFieldOLFactAddress.FieldName = "OutletFactAddressNameColumn";
            this.pivotGridFieldOLFactAddress.Name = "pivotGridFieldOLFactAddress";
            // 
            // pivotGridFieldM2
            // 
            this.pivotGridFieldM2.AllowedAreas = ((DevExpress.XtraPivotGrid.PivotGridAllowedAreas)(((DevExpress.XtraPivotGrid.PivotGridAllowedAreas.RowArea | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.ColumnArea)
                        | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.FilterArea)));
            this.pivotGridFieldM2.Area = DevExpress.XtraPivotGrid.PivotArea.RowArea;
            this.pivotGridFieldM2.AreaIndex = 0;
            this.pivotGridFieldM2.FieldName = "SupervisorNameColumn";
            this.pivotGridFieldM2.Name = "pivotGridFieldM2";
            // 
            // pivotGridFieldM1
            // 
            this.pivotGridFieldM1.AllowedAreas = ((DevExpress.XtraPivotGrid.PivotGridAllowedAreas)(((DevExpress.XtraPivotGrid.PivotGridAllowedAreas.RowArea | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.ColumnArea)
                        | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.FilterArea)));
            this.pivotGridFieldM1.Area = DevExpress.XtraPivotGrid.PivotArea.RowArea;
            this.pivotGridFieldM1.AreaIndex = 1;
            this.pivotGridFieldM1.FieldName = "MerchandiserNameColumn";
            this.pivotGridFieldM1.Name = "pivotGridFieldM1";
            // 
            // pivotGridFieldOLID
            // 
            this.pivotGridFieldOLID.AllowedAreas = ((DevExpress.XtraPivotGrid.PivotGridAllowedAreas)(((DevExpress.XtraPivotGrid.PivotGridAllowedAreas.RowArea | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.ColumnArea)
                        | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.FilterArea)));
            this.pivotGridFieldOLID.AreaIndex = 4;
            this.pivotGridFieldOLID.FieldName = "OutletIDColumn";
            this.pivotGridFieldOLID.Name = "pivotGridFieldOLID";
            // 
            // pivotGridFieldDate
            // 
            this.pivotGridFieldDate.AllowedAreas = ((DevExpress.XtraPivotGrid.PivotGridAllowedAreas)(((DevExpress.XtraPivotGrid.PivotGridAllowedAreas.RowArea | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.ColumnArea)
                        | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.FilterArea)));
            this.pivotGridFieldDate.AreaIndex = 3;
            this.pivotGridFieldDate.CellFormat.FormatString = "d";
            this.pivotGridFieldDate.CellFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.pivotGridFieldDate.FieldName = "DateColumn";
            this.pivotGridFieldDate.GroupInterval = DevExpress.XtraPivotGrid.PivotGroupInterval.Date;
            this.pivotGridFieldDate.Name = "pivotGridFieldDate";
            this.pivotGridFieldDate.UnboundFieldName = "pivotGridFieldDate";
            this.pivotGridFieldDate.ValueFormat.FormatString = "d";
            this.pivotGridFieldDate.ValueFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            // 
            // pivotGridFieldTare
            // 
            this.pivotGridFieldTare.AllowedAreas = ((DevExpress.XtraPivotGrid.PivotGridAllowedAreas)(((DevExpress.XtraPivotGrid.PivotGridAllowedAreas.RowArea | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.ColumnArea)
                        | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.FilterArea)));
            this.pivotGridFieldTare.AreaIndex = 1;
            this.pivotGridFieldTare.FieldName = "TareColumn";
            this.pivotGridFieldTare.Name = "pivotGridFieldTare";
            // 
            // pivotGridFieldTareType
            // 
            this.pivotGridFieldTareType.AllowedAreas = ((DevExpress.XtraPivotGrid.PivotGridAllowedAreas)(((DevExpress.XtraPivotGrid.PivotGridAllowedAreas.RowArea | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.ColumnArea)
                        | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.FilterArea)));
            this.pivotGridFieldTareType.AreaIndex = 2;
            this.pivotGridFieldTareType.FieldName = "TareTypeColumn";
            this.pivotGridFieldTareType.Name = "pivotGridFieldTareType";
            // 
            // pivotGridFieldSales
            // 
            this.pivotGridFieldSales.AllowedAreas = ((DevExpress.XtraPivotGrid.PivotGridAllowedAreas)((DevExpress.XtraPivotGrid.PivotGridAllowedAreas.FilterArea | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.DataArea)));
            this.pivotGridFieldSales.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
            this.pivotGridFieldSales.AreaIndex = 0;
            this.pivotGridFieldSales.CellFormat.FormatString = "F0";
            this.pivotGridFieldSales.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.pivotGridFieldSales.FieldName = "SalesInCountColumn";
            this.pivotGridFieldSales.Name = "pivotGridFieldSales";
            this.pivotGridFieldSales.RowValueLineCount = 2;
            this.pivotGridFieldSales.SummaryType = DevExpress.Data.PivotGrid.PivotSummaryType.Custom;
            // 
            // pivotGridFieldDZ
            // 
            this.pivotGridFieldDZ.AllowedAreas = ((DevExpress.XtraPivotGrid.PivotGridAllowedAreas)((DevExpress.XtraPivotGrid.PivotGridAllowedAreas.FilterArea | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.DataArea)));
            this.pivotGridFieldDZ.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
            this.pivotGridFieldDZ.AreaIndex = 1;
            this.pivotGridFieldDZ.CellFormat.FormatString = "F0";
            this.pivotGridFieldDZ.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.pivotGridFieldDZ.FieldName = "DZColumn";
            this.pivotGridFieldDZ.Name = "pivotGridFieldDZ";
            this.pivotGridFieldDZ.RowValueLineCount = 2;
            this.pivotGridFieldDZ.SummaryType = DevExpress.Data.PivotGrid.PivotSummaryType.Custom;
            // 
            // pivotGridFieldTurnoverDZ
            // 
            this.pivotGridFieldTurnoverDZ.AllowedAreas = ((DevExpress.XtraPivotGrid.PivotGridAllowedAreas)((DevExpress.XtraPivotGrid.PivotGridAllowedAreas.FilterArea | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.DataArea)));
            this.pivotGridFieldTurnoverDZ.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
            this.pivotGridFieldTurnoverDZ.AreaIndex = 2;
            this.pivotGridFieldTurnoverDZ.CellFormat.FormatString = "F2";
            this.pivotGridFieldTurnoverDZ.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.pivotGridFieldTurnoverDZ.FieldName = "TurnoverDZColumn";
            this.pivotGridFieldTurnoverDZ.Name = "pivotGridFieldTurnoverDZ";
            this.pivotGridFieldTurnoverDZ.RowValueLineCount = 2;
            this.pivotGridFieldTurnoverDZ.SummaryType = DevExpress.Data.PivotGrid.PivotSummaryType.Custom;
            this.pivotGridFieldTurnoverDZ.Width = 150;
            // 
            // TurnoverByTareReport
            // 
            this.Controls.Add(this.uiPivotGrid);
            this.Name = "TurnoverByTareReport";
            this.Size = new System.Drawing.Size(815, 385);
            ((System.ComponentModel.ISupportInitialize)(this.uiPivotGrid)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraPivotGrid.PivotGridControl uiPivotGrid;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldOLFactName;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldOLFactAddress;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldOLID;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldSales;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldDZ;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldTurnoverDZ;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldM2;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldM1;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldDate;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldTare;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldTareType;
    }
}
