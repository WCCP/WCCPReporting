﻿using DevExpress.Utils;
using DevExpress.XtraPivotGrid;
using Logica.Reports.DataProvider;
using Logica.Reports.Localization;

namespace Logica.Reports.SVReport.UserControls
{
    partial class ProductsMotionReport
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.uiPivotGrid = new DevExpress.XtraPivotGrid.PivotGridControl();
            this.pivotGridField1 = new DevExpress.XtraPivotGrid.PivotGridField();
            ((System.ComponentModel.ISupportInitialize)(this.uiPivotGrid)).BeginInit();
            this.SuspendLayout();
            // 
            // uiPivotGrid
            // 
            this.uiPivotGrid.Cursor = System.Windows.Forms.Cursors.Default;
            this.uiPivotGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiPivotGrid.Fields.AddRange(new DevExpress.XtraPivotGrid.PivotGridField[] {
            this.pivotGridField1});
            this.uiPivotGrid.Location = new System.Drawing.Point(0, 0);
            this.uiPivotGrid.Name = "uiPivotGrid";
            this.uiPivotGrid.Size = new System.Drawing.Size(408, 317);
            this.uiPivotGrid.TabIndex = 0;
            this.uiPivotGrid.CustomSummary += new DevExpress.XtraPivotGrid.PivotGridCustomSummaryEventHandler(this.uiPivotGrid_CustomSummary);
            // 
            // pivotGridField1
            // 
            this.pivotGridField1.Area = DevExpress.XtraPivotGrid.PivotArea.RowArea;
            this.pivotGridField1.Name = "pivotGridField1";
            // 
            // ProductsMotionReport
            // 
            this.Controls.Add(this.uiPivotGrid);
            this.Name = "ProductsMotionReport";
            this.Size = new System.Drawing.Size(408, 317);
            ((System.ComponentModel.ISupportInitialize)(this.uiPivotGrid)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraPivotGrid.PivotGridControl uiPivotGrid;
        private PivotGridField pivotGridField1;
    }
}
