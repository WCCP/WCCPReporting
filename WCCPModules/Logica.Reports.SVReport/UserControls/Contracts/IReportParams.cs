﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.XtraTab;
using Logica.Reports.BaseReportControl;
using Logica.Reports.ModelDataProvider.Entities;
using System.ComponentModel;
using DevExpress.XtraGrid;

namespace Logica.Reports.SVReport.UserControls.Contracts
{
    interface IReportParams
    {
        /// <summary>
        /// Gets or sets the report start date.
        /// </summary>
        /// <value>The report start date.</value>
        [Browsable(true)]
        DateTime ReportStartDate { get; set;}
        
        /// <summary>
        /// Gets or sets the report end date.
        /// </summary>
        /// <value>The report end date.</value>
        [Browsable(true)]
        DateTime ReportEndDate { get; set;}

        /// <summary>
        /// Gets or sets the merchandiser collection.
        /// </summary>
        /// <value>The merchandiser collection.</value>
        List<Merchandiser> MerchandiserCollection { get; set; }

        /// <summary>
        /// Gets or sets the supervisor collection.
        /// </summary>
        /// <value>The supervisor collection.</value>
        //TODO: unnesessary, need to be removed
        List<Supervisor> SupervisorCollection { get; set; }

        /// <summary>
        /// Populate the report.
        /// </summary>
        void PopulateReport();
        
        /// <summary>
        /// Allows to customize the columns.
        /// </summary>
        void SetupColumns();

        /// <summary>
        /// Gets the type of the report.
        /// </summary>
        /// <value>The type of the report.</value>
        ReportType RepType { get; }
        /// <summary>
        /// Gets the grid of the report.
        /// </summary>
        /// <value>The grid object.</value>
        object ContentControl { get; }

        XtraTabPage ParentTabPage { get; set; }

        GridControl HeaderGrid { get; }

        bool ExportHeaderGrid { get; }

        SheetParamCollection SheetParamCollection { get; }
    }
}
