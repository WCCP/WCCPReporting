﻿using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraPivotGrid;
using Logica.Reports.DataProvider.Providers;
using Logica.Reports.DataProvider.Entities;
using Logica.Reports.Localization;
using Logica.Reports.DataProvider;
using Logica.Reports.UserControls;
using DevExpress.Data.PivotGrid;

namespace Logica.Reports.SVReport.UserControls
{
    /// <summary>
    /// Represents ActiveOutletsTOP10Analysis report as a part of SVReport module.
    /// </summary>
    public partial class ActiveOutletsTOP10Report : ReportBase
    {
        #region IReportParams Members

        /// <summary>
        /// Gets the name of the report.
        /// </summary>
        /// <value>The name of the report.</value>
        public override ReportType RepType
        {
            get
            {
                return ReportType.ActiveOutletsTop10;
            }
        }

        /// <summary>
        /// Populates the report.
        /// </summary>
        protected override void PopulateReportEngine()
        {
            ActiveOutletsTOP10DataProvider dataProvider = new ActiveOutletsTOP10DataProvider();
            ActiveOutletsTOP10DataSet.ReportEntriesDataTable reportData = dataProvider.GetReportData(ReportStartDate, ReportEndDate, MerchandiserCollection);
            uiPivotGrid.DataSource = reportData;
        }
        public override object ContentControl
        {
            get
            {
                return uiPivotGrid;
            }
        }
        #endregion


        PivotGridField fieldOLHas;

        public ActiveOutletsTOP10Report()
        {
            InitializeComponent();
            Dock = DockStyle.Fill;
            SetupColumns();
            uiPivotGrid.CustomSummary += new PivotGridCustomSummaryEventHandler(uiPivotGrid_CustomSummary);
        }

        void uiPivotGrid_CustomSummary(object sender, PivotGridCustomSummaryEventArgs e)
        {
            if (e.DataField == fieldOLHas)
            {
                PivotDrillDownDataSource ds = e.CreateDrillDownDataSource();
                List<long> list = new List<long>();
                for (int i = 0; i < ds.RowCount; i++)
                {
                    PivotDrillDownDataRow row = ds[i];
                    if (null != row[e.DataField])
                        list.Add((long)row[e.DataField]);
                }
                e.CustomValue = list.GroupBy(x => x).Count();

            }    
        }

        #region Helper methods
        /// <summary>
        /// Allows to customize the columns.
        /// </summary>
        public override void SetupColumns()
        {
            fieldOLHas = new PivotGridField(FieldNames.OutletHas, PivotArea.DataArea);

            fieldOLHas.SummaryType = PivotSummaryType.Custom;



            uiPivotGrid.Fields.AddRange(new PivotGridField[] {
                fieldSvName, fieldMerchName, fieldOLTypeName, fieldOLSubTypeName, 
                fieldOLName, fieldOLDeliveryAddress, fieldOLTradingName, fieldOLAddress,
                fieldCityName, fieldDistrictName, fieldOLID, fieldOLExternalCode, fieldStatus,
                fieldCustName, fieldRegionName, fieldMerchID, fieldMerchStatus,
                fieldAreaName, fieldTerritoryType, fieldProximityFactor,  fieldOLHas, 
            });
            fieldSvName.Area = PivotArea.RowArea;
            foreach (PivotGridField field in uiPivotGrid.Fields)
                field.Caption = LocalizationProvider.GetText(field.FieldName);
        }
        #endregion

    }
}
