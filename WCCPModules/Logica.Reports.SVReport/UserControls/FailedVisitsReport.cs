﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Logica.Reports.DataProvider.Providers;
using Logica.Reports.DataProvider.Entities;
using DevExpress.XtraPivotGrid;
using Logica.Reports.DataProvider;
using Logica.Reports.Localization;
using Logica.Reports.ModelDataProvider.Entities;
using Logica.Reports.UserControls;

namespace Logica.Reports.SVReport.UserControls
{
    /// <summary>
    /// Represents FailedVisits report as a part of SVReport module.
    /// </summary>
    public partial class FailedVisitsReport : ReportBase
    {
        #region IReportParams Members

        /// <summary>
        /// Gets the name of the report.
        /// </summary>
        /// <value>The name of the report.</value>
        public override ReportType RepType
        {
            get
            {
                return ReportType.FailedVisits;
            }
        }

        /// <summary>
        /// Populates the report.
        /// </summary>
        protected override void PopulateReportEngine()
        {
            FailedVisitsDataProvider dataProvider = new FailedVisitsDataProvider();
            FailedVisitsDataSet.ReportEntriesDataTable reportData = dataProvider.GetReportData(ReportStartDate, ReportEndDate, MerchandiserCollection);
            uiPivotGrid.DataSource = reportData;
        }
        public override object ContentControl
        {
            get
            {
                return uiPivotGrid;
            }
        }
        #endregion

        public FailedVisitsReport()
        {
            InitializeComponent();
            Dock = DockStyle.Fill;
            SetupColumns();
        }


        #region Helper methods

        /// <summary>
        /// Allows to customize the columns.
        /// </summary>
        public override void SetupColumns()
        {
            uiPivotGrid.Fields.Clear();

            PivotGridField fieldReason = new PivotGridField(FieldNames.InaccessibilityReason, PivotArea.FilterArea);
            PivotGridField fieldFactAddressName = new PivotGridField(FieldNames.OutletFactAddressName, PivotArea.FilterArea);
            PivotGridField fieldLawAddressName = new PivotGridField(FieldNames.OutletLawAddressName, PivotArea.FilterArea);
            PivotGridField fieldOLVisitDate = new PivotGridField(FieldNames.OutletCardDate, PivotArea.FilterArea);
            PivotGridField fieldFailedVisits = new PivotGridField(FieldNames.FailedVisitsCount, PivotArea.DataArea);
            
            fieldFailedVisits.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;



            fieldOLVisitDate.ValueFormat.FormatType = fieldOLVisitDate.CellFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            fieldOLVisitDate.ValueFormat.FormatString = fieldOLVisitDate.CellFormat.FormatString = "d";

            uiPivotGrid.Fields.AddRange(new PivotGridField[] {
                fieldOLID, fieldOLCode,fieldReason, fieldFactAddressName,
                fieldLawAddressName, fieldOLSubTypeName,fieldOLTypeName, fieldOLName,
                fieldOLDeliveryAddress,fieldOLTradingName, fieldOLAddress, fieldMerchStatus,
                fieldCustomerName, fieldOLCity, fieldOLComments, fieldOLVisitDate, 
                fieldSupervisorName, fieldMerchName, fieldFailedVisits,fieldOLStatus});

            foreach (PivotGridField field in uiPivotGrid.Fields)
                field.Caption = LocalizationProvider.GetText(field.FieldName);
        }
        #endregion
    }
}

