﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraPivotGrid;
using Logica.Reports.DataProvider.Providers;
using Logica.Reports.DataProvider.Entities;
using Logica.Reports.Localization;
using Logica.Reports.DataProvider;
using Logica.Reports.ModelDataProvider.Entities;
using Logica.Reports.UserControls;
using DevExpress.Utils;
using DevExpress.Data.PivotGrid;

namespace Logica.Reports.SVReport.UserControls
{
	/// <summary>
	/// Represents CoverageAnalysis report as a part of SVReport module.
	/// </summary>
	public partial class CoverageAnalysisReport : ReportBase
	{
		PivotGridField fieldTotalOutlets;
		PivotGridField fieldVisitedCount;
		PivotGridField fieldActiveCount;
		PivotGridField fieldRestCount;
		PivotGridField fieldCovering;
		PivotGridField fieldActiveOLCount;

		PivotGridField fieldBelongingTOPSKU;

		#region IReportParams Members

		/// <summary>
		/// Gets the name of the report.
		/// </summary>
		/// <value>The name of the report.</value>
		public override ReportType RepType
		{
			get
			{
				return ReportType.CoverageAnalysis;
			}
		}

		/// <summary>
		/// Populates the report.
		/// </summary>
		protected override void PopulateReportEngine()
		{
			CoverageAnalysisDataProvider dataProvider = new CoverageAnalysisDataProvider();
			CoverageAnalysisDataSet.ReportEntriesDataTable reportData = dataProvider.GetReportData(ReportStartDate, ReportEndDate, MerchandiserCollection);

            //fieldTotalOutlets.TotalCellFormat.FormatType = FormatType.Custom;
            //fieldTotalOutlets.TotalCellFormat.FormatString = reportData.Rows.Count.ToString();
            
            fieldTotalOutlets.GrandTotalCellFormat.FormatString = "0.00";
            fieldTotalOutlets.GrandTotalCellFormat.FormatType = FormatType.Custom;
            fieldTotalOutlets.GrandTotalText = reportData.Rows.Count.ToString();
            //fieldTotalOutlets.G

            uiPivotGrid.DataSource = reportData;
		}
		public override object ContentControl
		{
			get
			{
				return uiPivotGrid;
			}
		}
		#endregion


		public CoverageAnalysisReport()
		{
			InitializeComponent();
			Dock = DockStyle.Fill;
			SetupColumns();
			uiPivotGrid.CustomSummary += new PivotGridCustomSummaryEventHandler(uiPivotGrid_CustomSummary);
			uiPivotGrid.CustomCellDisplayText += new PivotCellDisplayTextEventHandler(uiPivotGrid_CustomCellDisplayText);
			uiPivotGrid.CustomExportCell += new EventHandler<CustomExportCellEventArgs>(uiPivotGrid_CustomExportCell);
			uiPivotGrid.FieldValueDisplayText += new PivotFieldDisplayTextEventHandler(uiPivotGrid_FieldValueDisplayText);

		}

		void uiPivotGrid_FieldValueDisplayText(object sender, PivotFieldDisplayTextEventArgs e)
		{
			if (e.Field == fieldBelongingTOPSKU && null != e.Value)
			{
				e.DisplayText = Convert.ToBoolean(e.Value) ? "Да" : "Нет";
			}
		}

		void uiPivotGrid_CustomExportCell(object sender, CustomExportCellEventArgs e)
		{
			if (e.DataField == fieldCovering)
			{
				((DevExpress.XtraPrinting.ITextBrick)e.Brick).TextValue = e.Text;
			}
		}
		void uiPivotGrid_CustomCellDisplayText(object sender, PivotCellDisplayTextEventArgs e)
		{
			if (e.DataField == fieldCovering)
			{
				decimal up = Convert.ToDecimal(e.GetCellValue(fieldActiveCount));
				decimal down = Convert.ToDecimal(e.GetCellValue(fieldTotalOutlets));
				e.DisplayText = (down == 0 ? 0 : up / down).ToString("p");
			}
			if (e.DataField == fieldBelongingTOPSKU && null != e.Value)
			{
				e.DisplayText = Convert.ToBoolean(e.Value) ? "Да" : "Нет";
			}
		}

		void uiPivotGrid_CustomSummary(object sender, PivotGridCustomSummaryEventArgs e)
		{
			if (e.DataField == fieldTotalOutlets ||
				e.DataField == fieldVisitedCount ||
				e.DataField == fieldActiveCount ||
				e.DataField == fieldRestCount ||
				e.DataField == fieldActiveOLCount)
			{
				PivotDrillDownDataSource ds = e.CreateDrillDownDataSource();
				List<long> list = new List<long>();
				for (int i = 0; i < ds.RowCount; i++)
				{
					PivotDrillDownDataRow row = ds[i];
					if (null != row[e.DataField])
						list.Add((long)row[e.DataField]);
				}
				e.CustomValue = list.GroupBy(x => x).Count();
			}
		}

		#region Helper methods
		/// <summary>
		/// Allows to customize the columns.
		/// </summary>
		public override void SetupColumns()
		{
            uiPivotGrid.Fields.Clear();

			fieldCovering = new PivotGridField("", PivotArea.DataArea);
			fieldTotalOutlets = new PivotGridField(FieldNames.OLSchedule, PivotArea.DataArea);
			fieldVisitedCount = new PivotGridField(FieldNames.OutletVisitedNumber, PivotArea.DataArea);
			fieldActiveCount = new PivotGridField(FieldNames.OutletActiveNumber, PivotArea.DataArea);
			fieldRestCount = new PivotGridField(FieldNames.OutletHasRemains, PivotArea.DataArea);
			fieldActiveOLCount = new PivotGridField(FieldNames.OlCompletedPlan, PivotArea.DataArea);

			fieldBelongingTOPSKU = new PivotGridField(FieldNames.BelongingTOPSKU, PivotArea.FilterArea);
	
			FormatInfo fi = new FormatInfo();
			fi.FormatType = FormatType.Custom;

			fieldTotalOutlets.CellFormat.FormatType = fieldVisitedCount.CellFormat.FormatType =
				fieldActiveCount.CellFormat.FormatType = fieldRestCount.CellFormat.FormatType = FormatType.Numeric;
            fieldTotalOutlets.CellFormat.FormatString = "{0:N0}";
			fieldTotalOutlets.SummaryType = fieldVisitedCount.SummaryType = fieldActiveOLCount.SummaryType = 
				fieldActiveCount.SummaryType = fieldRestCount.SummaryType = PivotSummaryType.Custom;


			fieldOLCardDate.ValueFormat.FormatType = fieldOLCardDate.CellFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
			fieldOLCardDate.ValueFormat.FormatString = fieldOLCardDate.CellFormat.FormatString = "d";


			uiPivotGrid.Fields.AddRange(new PivotGridField[] {
                fieldSvName, fieldMerchName, 
                fieldTotalOutlets, fieldVisitedCount, fieldActiveCount, fieldRestCount, 
                fieldOLID, fieldOLExternalCode, 
                fieldOLTypeName, fieldOLSubTypeName, fieldOLName, fieldOLDeliveryAddress, 
                fieldProductGroupName, fieldProductTypeName,fieldOLCardDate,
                fieldUnitName, fieldPackingType,
                fieldPackingCapacity, fieldProductCnName, fieldProximityFactor,  
                fieldActiveOLCount, fieldCovering, fieldBelongingTOPSKU
            });

			foreach (PivotGridField field in uiPivotGrid.Fields)
				field.Caption = LocalizationProvider.GetText(field.FieldName);

			fieldTotalOutlets.Caption = Resource.OutletCountColumn;
			fieldCovering.Caption = Resource.CoveringColumn;
            /*
            object[] exludedValue = new object[1];
            exludedValue[0] = 0;
            object[] empty = new object[0];
            uiPivotGrid.Fields[FieldNames.OutletVisitedNumber].FilterValues.ValuesExcluded = exludedValue;
            uiPivotGrid.Fields[FieldNames.OutletVisitedNumber].FilterValues.Values = empty;

            uiPivotGrid.Fields[FieldNames.OutletActiveNumber].FilterValues.ValuesExcluded = exludedValue;
            uiPivotGrid.Fields[FieldNames.OutletActiveNumber].FilterValues.Values = empty;

            uiPivotGrid.Fields[FieldNames.OutletHasRemains].FilterValues.ValuesExcluded = exludedValue;
            uiPivotGrid.Fields[FieldNames.OutletHasRemains].FilterValues.Values = empty;
             */
		}
		#endregion

	}
}
