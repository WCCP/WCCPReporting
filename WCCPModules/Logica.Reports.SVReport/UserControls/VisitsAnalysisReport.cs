﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraPivotGrid;
using Logica.Reports.DataProvider.Providers;
using Logica.Reports.DataProvider.Entities;
using Logica.Reports.Localization;
using Logica.Reports.DataProvider;
using Logica.Reports.ModelDataProvider.Entities;
using Logica.Reports.UserControls;
using DevExpress.Utils;
using DevExpress.Data.PivotGrid;

namespace Logica.Reports.SVReport.UserControls
{
    /// <summary>
    /// Represents VisitsAnalysis report as a part of SVReport module.
    /// </summary>
    public partial class VisitsAnalysisReport : ReportBase
    {
        PivotGridField fieldStrike;
        PivotGridField fieldVisitedCount;
        PivotGridField fieldActiveCount;
        PivotGridField fieldTotalOutlets;

        #region IReportParams Members

        /// <summary>
        /// Gets the name of the report.
        /// </summary>
        /// <value>The name of the report.</value>
        public override ReportType RepType
        {
            get
            {
                return ReportType.VisitsAnalysis;
            }
        }

        /// <summary>
        /// Populates the report.
        /// </summary>
        protected override void PopulateReportEngine()
        {
            VisitsAnalysisDataProvider dataProvider = new VisitsAnalysisDataProvider();
            VisitsAnalysisDataSet.ReportEntriesDataTable reportData = dataProvider.GetReportData(ReportStartDate, ReportEndDate, MerchandiserCollection);
            uiPivotGrid.DataSource = reportData;
        }
        public override object ContentControl
        {
            get 
            {
                return uiPivotGrid;    
            }
        }
        #endregion        

        
        public VisitsAnalysisReport()
        {
            InitializeComponent();
            Dock = DockStyle.Fill;
            SetupColumns();
            uiPivotGrid.CustomSummary += new PivotGridCustomSummaryEventHandler(uiPivotGrid_CustomSummary);
            uiPivotGrid.CustomCellDisplayText += new PivotCellDisplayTextEventHandler(uiPivotGrid_CustomCellDisplayText);
            uiPivotGrid.CustomExportCell += new EventHandler<CustomExportCellEventArgs>(uiPivotGrid_CustomExportCell);
        }

        void uiPivotGrid_CustomExportCell(object sender, CustomExportCellEventArgs e)
        {
            if (e.DataField == fieldStrike)
            {
                ((DevExpress.XtraPrinting.ITextBrick)e.Brick).TextValue = e.Text;
            }
        }

        void uiPivotGrid_CustomCellDisplayText(object sender, PivotCellDisplayTextEventArgs e)
        {
            if (e.DataField == fieldStrike)
            {
                decimal up = Convert.ToDecimal(e.GetCellValue(fieldActiveCount));
                decimal down = Convert.ToDecimal(e.GetCellValue(fieldVisitedCount));
                e.DisplayText = (down == 0 ? 0 : up / down).ToString("p");
            }
        }

        void uiPivotGrid_CustomSummary(object sender, PivotGridCustomSummaryEventArgs e)
        {
            if (e.DataField == fieldTotalOutlets || 
                e.DataField == fieldVisitedCount ||
                e.DataField == fieldActiveCount)
            {
                PivotDrillDownDataSource ds = e.CreateDrillDownDataSource();
                List<long> list = new List<long>();
                for (int i = 0; i < ds.RowCount; i++)
                {
                    PivotDrillDownDataRow row = ds[i];
                    if(null != row[e.DataField])
                        list.Add((long)row[e.DataField]);
                }
                e.CustomValue = list.GroupBy(x => x).Count();

            }         
        }

        #region Helper methods
        /// <summary>
        /// Allows to customize the columns.
        /// </summary>
        public override void SetupColumns()
        {
            uiPivotGrid.Fields.Clear();
            
            fieldStrike = new PivotGridField("",PivotArea.DataArea);
            fieldVisitedCount = new PivotGridField(FieldNames.OutletVisitedNumber, PivotArea.DataArea);
            fieldActiveCount = new PivotGridField(FieldNames.OutletVisitedIsEffective, PivotArea.DataArea);
			fieldTotalOutlets = new PivotGridField(FieldNames.OLSchedule, PivotArea.DataArea);
            fieldSvName.Area = PivotArea.RowArea;

            fieldStrike.CellFormat.FormatType = fieldVisitedCount.CellFormat.FormatType =
                fieldActiveCount.CellFormat.FormatType = fieldTotalOutlets.CellFormat.FormatType = FormatType.Numeric;
            fieldVisitedCount.SummaryType = fieldActiveCount.SummaryType = fieldTotalOutlets.SummaryType = PivotSummaryType.Custom;

            fieldOLCardDate.ValueFormat.FormatType = fieldOLCardDate.CellFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            fieldOLCardDate.ValueFormat.FormatString = fieldOLCardDate.CellFormat.FormatString = "d";

            uiPivotGrid.Fields.AddRange(new PivotGridField[] {
                fieldSvName, fieldCityName,  
                fieldMerchName, fieldOLCardDate,fieldOLID,
                fieldOLExternalCode, fieldOLTypeName, fieldOLSubTypeName, 
                fieldOLName, fieldOLDeliveryAddress, 
                fieldProductGroupName, fieldProductTypeName,
                fieldUnitName, fieldPackingCapacity, fieldPackingType, fieldProductCnName,
                fieldTotalOutlets, fieldVisitedCount, fieldActiveCount,fieldStrike });

            foreach (PivotGridField field in uiPivotGrid.Fields)
                field.Caption = LocalizationProvider.GetText(field.FieldName);
            fieldStrike.Caption = Resource.StrikeRateColumn; 
            fieldVisitedCount.Caption = Resource.VisitedCountColumn;
            fieldActiveCount.Caption = Resource.ActiveVisitedCountColumn;
            fieldTotalOutlets.Caption = Resource.OutletCountColumn;
        }
        #endregion

    }
}
