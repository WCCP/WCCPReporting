﻿using System;

namespace Logica.Reports
{
    public struct DateUsage
    {
        public bool Use { get; set; }
        public DateTime From { get; set; }
        public DateTime To { get; set; }
    }
}
