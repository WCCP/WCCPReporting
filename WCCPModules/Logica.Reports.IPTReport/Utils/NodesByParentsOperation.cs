﻿using System;
using System.Collections.Generic;
using DevExpress.XtraTreeList.Nodes;
using DevExpress.XtraTreeList.Nodes.Operations;

namespace SoftServe.Reports.TasksReport.Utils
{
    class NodesByParentsOperation : TreeListOperation
    {
        private string _colLevelName;
        private string _colMaskName;
        private string _colParentIdName;
        private string _colIdName;

        private int _level;
        private byte _mask;


        private List<int> _parentIds; 
        public List<int> Ids { get; set; }

        public NodesByParentsOperation(string colLevelName, int level, string colMaskName, byte mask, string colParentIdName, List<int> parentIds, string colIdName)
        {
            _level = level;
            _mask = mask;

            _colLevelName = colLevelName;
            _colMaskName = colMaskName;
            _colParentIdName = colParentIdName;
            _colIdName = colIdName;

            _parentIds = parentIds;

            Ids = new List<int>();
        }

        public override void Execute(TreeListNode node)
        {
            if (NodeSatisfiesCondition(node))
            {
                Ids.Add(Convert.ToInt32(node[_colIdName]));
            }
        }

        bool NodeSatisfiesCondition(TreeListNode node)
        {
            return _level == Convert.ToInt32(node[_colLevelName]) &&  _parentIds.Contains(Convert.ToInt32(node[_colParentIdName])) && (Convert.ToUInt16(node[_colMaskName]) & _mask) > 0;
        }
    }
}
