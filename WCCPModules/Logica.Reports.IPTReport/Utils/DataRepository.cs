﻿using System;
using System.Collections.Generic;


namespace Logica.Reports
{
    public static class DataRepository
    {
        private static List<Channel> _channels;
        private static List<StaffItem> _staff;

        public static List<Channel> Channels
        {
            get
            {
                if (_channels == null)
                {
                    _channels = DataProvider.GetChannels();
                    _channels.Add(new Channel { Id = -1, Mask = 0, Name = "Все" });
                }
                return _channels;
            }
        }

        public static int UserLevel
        {
            get { return DataProvider.GetUserLevel(); }
        }

        public static List<StaffItem> GetNewStaff(DateTime p)
        {
            _staff = DataProvider.GetStaffTree(p.Date);
            return _staff;
        }
    }
}
