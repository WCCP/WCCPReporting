﻿using System;

namespace Logica.Reports
{
    public class FilteringArgs: EventArgs
    {
        public bool IsNotFiltered { get; set; }

        public FilteringArgs(bool value)
        {
            IsNotFiltered = value;
        }
    }
}
