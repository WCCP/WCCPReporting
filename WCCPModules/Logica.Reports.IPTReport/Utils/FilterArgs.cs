﻿using System;
using System.Data;

namespace Logica.Reports
{
    public class FilterArgs : EventArgs
    {
        public DataTable Source { get; set; }

        public FilterArgs(DataTable source)
        {
            Source = source;
        }
    }
}
