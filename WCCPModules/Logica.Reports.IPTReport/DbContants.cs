﻿namespace Logica.Reports
{
    public static class DbContants
    {
        public const string spChanelList = "spDW_ChanelTypeList";
        public const string spChanelList_ChanelID = "ChanelType_id";
        public const string spChanelList_ChanelName = "ChanelType";

        public const string spDistrictList = "spDW_DistrictList";
        public const string spDistrictList_DistrictID = "District_id";
        public const string spDistrictList_DistrictName = "District_name";
        public const string spDistrictList_Param_ID = "@ID";
        public const string spDistrictList_Param_StaffLevel_ID = "@StaffLevel_ID";

        public const string spEquipmentClassList = "spDW_IPTR_EquipmentClassList";
        public const string spEquipmentClassList_EquipmentClassID = "EquipmentClass_ID";
        public const string spEquipmentClassList_EquipmentClassName = "ClassName";

        public const string spStaffTree = "spDW_StaffTree";
        public const string spStaffTree_ID = "ID";
        public const string spStaffTree_ItemID = "Item_ID";
        public const string spStaffTree_Name = "Name";
        public const string spStaffTree_Param_ChanelTypeID = "@ChanelType_ID";
        public const string spStaffTree_ParentID = "Parent_ID";
        public const string spStaffTree_StaffLevelID = "StaffLevel_ID";

        public const string SQL_ChanelTypeID = "@ChanelType_id";
        public const string SQL_Date = "@Date";
        public const string SQL_Debug = "@debug";
        public const string SQL_DistrictID = "@District_ID";
        public const string SQL_EquipmentClassID = "@EquipmentClass_ID";
        public const string SQL_ID = "@ID";
        public const string SQL_StaffLevelID = "@StaffLevel_ID";
    }
}
