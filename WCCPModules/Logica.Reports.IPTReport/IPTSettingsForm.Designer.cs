﻿namespace Logica.Reports
{
    partial class IPTSettingsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(IPTSettingsForm));
            this.panel1 = new DevExpress.XtraEditors.PanelControl();
            this.btnHelp = new DevExpress.XtraEditors.SimpleButton();
            this.imCollection = new DevExpress.Utils.ImageCollection(this.components);
            this.btnNo = new DevExpress.XtraEditors.SimpleButton();
            this.btnYes = new DevExpress.XtraEditors.SimpleButton();
            this.chkSelectAllReports = new DevExpress.XtraEditors.CheckEdit();
            this.cbDistrict = new DevExpress.XtraEditors.ComboBoxEdit();
            this.lbDistrict = new DevExpress.XtraEditors.LabelControl();
            this.cbEquipmentClass = new DevExpress.XtraEditors.ComboBoxEdit();
            this.lbEquipmentClass = new DevExpress.XtraEditors.LabelControl();
            this.lbChannelType = new DevExpress.XtraEditors.LabelControl();
            this.lbDateTime = new DevExpress.XtraEditors.LabelControl();
            this.dtDate = new DevExpress.XtraEditors.DateEdit();
            this.clbReports = new DevExpress.XtraEditors.CheckedListBoxControl();
            this.treeListColumn1 = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.staffLevelTree = new DevExpress.XtraTreeList.TreeList();
            this.groupControlCommon = new DevExpress.XtraEditors.GroupControl();
            this.cbChannelType = new DevExpress.XtraEditors.LookUpEdit();
            this.groupControlReports = new DevExpress.XtraEditors.GroupControl();
            this.groupControlStaff = new DevExpress.XtraEditors.GroupControl();
            this.tStaff = new DevExpress.XtraTreeList.TreeList();
            this.colName = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.colStaffId = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.colLevel = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.colChannelMask = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.cStaffAll = new DevExpress.XtraEditors.CheckEdit();
            this.groupControlLevel = new DevExpress.XtraEditors.GroupControl();
            this.rUserLevel = new DevExpress.XtraEditors.RadioGroup();
            ((System.ComponentModel.ISupportInitialize)(this.panel1)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imCollection)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSelectAllReports.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbDistrict.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbEquipmentClass.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.clbReports)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.staffLevelTree)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControlCommon)).BeginInit();
            this.groupControlCommon.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbChannelType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControlReports)).BeginInit();
            this.groupControlReports.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControlStaff)).BeginInit();
            this.groupControlStaff.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tStaff)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cStaffAll.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControlLevel)).BeginInit();
            this.groupControlLevel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rUserLevel.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.panel1.Appearance.Options.UseBackColor = true;
            this.panel1.Controls.Add(this.btnHelp);
            this.panel1.Controls.Add(this.btnNo);
            this.panel1.Controls.Add(this.btnYes);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 418);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(574, 36);
            this.panel1.TabIndex = 0;
            // 
            // btnHelp
            // 
            this.btnHelp.Enabled = false;
            this.btnHelp.ImageIndex = 0;
            this.btnHelp.ImageList = this.imCollection;
            this.btnHelp.Location = new System.Drawing.Point(491, 6);
            this.btnHelp.Name = "btnHelp";
            this.btnHelp.Size = new System.Drawing.Size(75, 25);
            this.btnHelp.TabIndex = 2;
            this.btnHelp.Text = "&Справка";
            // 
            // imCollection
            // 
            this.imCollection.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imCollection.ImageStream")));
            this.imCollection.Images.SetKeyName(0, "help_24.png");
            this.imCollection.Images.SetKeyName(1, "check_24.png");
            this.imCollection.Images.SetKeyName(2, "close_24.png");
            // 
            // btnNo
            // 
            this.btnNo.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnNo.ImageIndex = 2;
            this.btnNo.ImageList = this.imCollection;
            this.btnNo.Location = new System.Drawing.Point(410, 6);
            this.btnNo.Name = "btnNo";
            this.btnNo.Size = new System.Drawing.Size(75, 25);
            this.btnNo.TabIndex = 1;
            this.btnNo.Text = "Н&ет";
            // 
            // btnYes
            // 
            this.btnYes.ImageIndex = 1;
            this.btnYes.ImageList = this.imCollection;
            this.btnYes.Location = new System.Drawing.Point(329, 6);
            this.btnYes.Name = "btnYes";
            this.btnYes.Size = new System.Drawing.Size(75, 25);
            this.btnYes.TabIndex = 0;
            this.btnYes.Text = "&Да";
            this.btnYes.Click += new System.EventHandler(this.btnYes_Click);
            // 
            // chkSelectAllReports
            // 
            this.chkSelectAllReports.Location = new System.Drawing.Point(203, 1);
            this.chkSelectAllReports.Name = "chkSelectAllReports";
            this.chkSelectAllReports.Properties.Caption = "Все";
            this.chkSelectAllReports.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.chkSelectAllReports.Size = new System.Drawing.Size(41, 19);
            this.chkSelectAllReports.TabIndex = 10;
            this.chkSelectAllReports.CheckedChanged += new System.EventHandler(this.chkSelectAllReports_CheckedChanged);
            // 
            // cbDistrict
            // 
            this.cbDistrict.Location = new System.Drawing.Point(122, 119);
            this.cbDistrict.Name = "cbDistrict";
            this.cbDistrict.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbDistrict.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbDistrict.Size = new System.Drawing.Size(109, 20);
            this.cbDistrict.TabIndex = 8;
            // 
            // lbDistrict
            // 
            this.lbDistrict.Location = new System.Drawing.Point(69, 122);
            this.lbDistrict.Name = "lbDistrict";
            this.lbDistrict.Size = new System.Drawing.Size(47, 13);
            this.lbDistrict.TabIndex = 7;
            this.lbDistrict.Text = "Область:";
            // 
            // cbEquipmentClass
            // 
            this.cbEquipmentClass.Location = new System.Drawing.Point(122, 91);
            this.cbEquipmentClass.Name = "cbEquipmentClass";
            this.cbEquipmentClass.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbEquipmentClass.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbEquipmentClass.Size = new System.Drawing.Size(109, 20);
            this.cbEquipmentClass.TabIndex = 6;
            this.cbEquipmentClass.SelectedValueChanged += new System.EventHandler(this.cbEquipmentClass_SelectedValueChanged);
            // 
            // lbEquipmentClass
            // 
            this.lbEquipmentClass.Location = new System.Drawing.Point(7, 94);
            this.lbEquipmentClass.Name = "lbEquipmentClass";
            this.lbEquipmentClass.Size = new System.Drawing.Size(109, 13);
            this.lbEquipmentClass.TabIndex = 5;
            this.lbEquipmentClass.Text = "Класс оборудования:";
            // 
            // lbChannelType
            // 
            this.lbChannelType.Location = new System.Drawing.Point(55, 66);
            this.lbChannelType.Name = "lbChannelType";
            this.lbChannelType.Size = new System.Drawing.Size(61, 13);
            this.lbChannelType.TabIndex = 3;
            this.lbChannelType.Text = "Тип канала:";
            // 
            // lbDateTime
            // 
            this.lbDateTime.Location = new System.Drawing.Point(86, 38);
            this.lbDateTime.Name = "lbDateTime";
            this.lbDateTime.Size = new System.Drawing.Size(30, 13);
            this.lbDateTime.TabIndex = 2;
            this.lbDateTime.Text = "Дата:";
            // 
            // dtDate
            // 
            this.dtDate.EditValue = null;
            this.dtDate.Location = new System.Drawing.Point(122, 35);
            this.dtDate.Name = "dtDate";
            this.dtDate.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.dtDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dtDate.Size = new System.Drawing.Size(109, 20);
            this.dtDate.TabIndex = 1;
            this.dtDate.EditValueChanged += new System.EventHandler(this.dtDate_EditValueChanged);
            // 
            // clbReports
            // 
            this.clbReports.CheckOnClick = true;
            this.clbReports.Dock = System.Windows.Forms.DockStyle.Fill;
            this.clbReports.Location = new System.Drawing.Point(2, 21);
            this.clbReports.Name = "clbReports";
            this.clbReports.Size = new System.Drawing.Size(243, 228);
            this.clbReports.TabIndex = 0;
            this.clbReports.ItemCheck += new DevExpress.XtraEditors.Controls.ItemCheckEventHandler(this.clbReports_ItemCheck);
            // 
            // treeListColumn1
            // 
            this.treeListColumn1.Caption = "treeListColumn1";
            this.treeListColumn1.FieldName = "treeListColumn1";
            this.treeListColumn1.Name = "treeListColumn1";
            this.treeListColumn1.OptionsColumn.AllowEdit = false;
            this.treeListColumn1.OptionsColumn.AllowMove = false;
            this.treeListColumn1.OptionsColumn.AllowMoveToCustomizationForm = false;
            this.treeListColumn1.OptionsColumn.ReadOnly = true;
            this.treeListColumn1.OptionsColumn.ShowInCustomizationForm = false;
            this.treeListColumn1.Visible = true;
            this.treeListColumn1.VisibleIndex = 0;
            // 
            // staffLevelTree
            // 
            this.staffLevelTree.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.treeListColumn1});
            this.staffLevelTree.Location = new System.Drawing.Point(2, 24);
            this.staffLevelTree.Name = "staffLevelTree";
            this.staffLevelTree.OptionsSelection.InvertSelection = true;
            this.staffLevelTree.OptionsSelection.MultiSelect = true;
            this.staffLevelTree.OptionsSelection.UseIndicatorForSelection = true;
            this.staffLevelTree.OptionsView.ShowColumns = false;
            this.staffLevelTree.OptionsView.ShowIndicator = false;
            this.staffLevelTree.Size = new System.Drawing.Size(270, 143);
            this.staffLevelTree.TabIndex = 0;
            this.staffLevelTree.SelectionChanged += new System.EventHandler(this.staffLevelTree_SelectionChanged);
            // 
            // groupControlCommon
            // 
            this.groupControlCommon.Controls.Add(this.cbChannelType);
            this.groupControlCommon.Controls.Add(this.dtDate);
            this.groupControlCommon.Controls.Add(this.cbDistrict);
            this.groupControlCommon.Controls.Add(this.lbDateTime);
            this.groupControlCommon.Controls.Add(this.lbChannelType);
            this.groupControlCommon.Controls.Add(this.lbDistrict);
            this.groupControlCommon.Controls.Add(this.lbEquipmentClass);
            this.groupControlCommon.Controls.Add(this.cbEquipmentClass);
            this.groupControlCommon.Location = new System.Drawing.Point(5, 5);
            this.groupControlCommon.Name = "groupControlCommon";
            this.groupControlCommon.Size = new System.Drawing.Size(247, 150);
            this.groupControlCommon.TabIndex = 11;
            this.groupControlCommon.Text = "Общие:";
            // 
            // cbChannelType
            // 
            this.cbChannelType.EditValue = "-1";
            this.cbChannelType.Location = new System.Drawing.Point(122, 62);
            this.cbChannelType.Name = "cbChannelType";
            this.cbChannelType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbChannelType.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "Name")});
            this.cbChannelType.Properties.DisplayMember = "Name";
            this.cbChannelType.Properties.NullText = "";
            this.cbChannelType.Properties.PopupFormMinSize = new System.Drawing.Size(100, 0);
            this.cbChannelType.Properties.PopupSizeable = false;
            this.cbChannelType.Properties.ShowFooter = false;
            this.cbChannelType.Properties.ShowHeader = false;
            this.cbChannelType.Properties.ShowPopupShadow = false;
            this.cbChannelType.Properties.ValueMember = "Id";
            this.cbChannelType.Size = new System.Drawing.Size(109, 20);
            this.cbChannelType.TabIndex = 9;
            this.cbChannelType.EditValueChanged += new System.EventHandler(this.cbChannelType_EditValueChanged);
            // 
            // groupControlReports
            // 
            this.groupControlReports.Controls.Add(this.clbReports);
            this.groupControlReports.Controls.Add(this.chkSelectAllReports);
            this.groupControlReports.Location = new System.Drawing.Point(5, 163);
            this.groupControlReports.Name = "groupControlReports";
            this.groupControlReports.Size = new System.Drawing.Size(247, 251);
            this.groupControlReports.TabIndex = 12;
            this.groupControlReports.Text = "Отчеты:";
            // 
            // groupControlStaff
            // 
            this.groupControlStaff.Controls.Add(this.tStaff);
            this.groupControlStaff.Controls.Add(this.cStaffAll);
            this.groupControlStaff.Controls.Add(this.staffLevelTree);
            this.groupControlStaff.Location = new System.Drawing.Point(258, 61);
            this.groupControlStaff.Name = "groupControlStaff";
            this.groupControlStaff.Size = new System.Drawing.Size(313, 353);
            this.groupControlStaff.TabIndex = 13;
            this.groupControlStaff.Text = "Персонал:";
            // 
            // tStaff
            // 
            this.tStaff.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.colName,
            this.colStaffId,
            this.colLevel,
            this.colChannelMask});
            this.tStaff.KeyFieldName = "Id";
            this.tStaff.Location = new System.Drawing.Point(3, 174);
            this.tStaff.Name = "tStaff";
            this.tStaff.OptionsBehavior.AllowQuickHideColumns = false;
            this.tStaff.OptionsBehavior.AllowRecursiveNodeChecking = true;
            this.tStaff.OptionsBehavior.Editable = false;
            this.tStaff.OptionsBehavior.EnableFiltering = true;
            this.tStaff.OptionsBehavior.ReadOnly = true;
            this.tStaff.OptionsBehavior.ShowToolTips = false;
            this.tStaff.OptionsFilter.AllowColumnMRUFilterList = false;
            this.tStaff.OptionsFilter.AllowMRUFilterList = false;
            this.tStaff.OptionsFilter.ShowAllValuesInCheckedFilterPopup = false;
            this.tStaff.OptionsFilter.ShowAllValuesInFilterPopup = true;
            this.tStaff.OptionsMenu.EnableColumnMenu = false;
            this.tStaff.OptionsMenu.EnableFooterMenu = false;
            this.tStaff.OptionsMenu.ShowAutoFilterRowItem = false;
            this.tStaff.OptionsView.AutoWidth = false;
            this.tStaff.OptionsView.ShowCheckBoxes = true;
            this.tStaff.OptionsView.ShowColumns = false;
            this.tStaff.OptionsView.ShowHorzLines = false;
            this.tStaff.OptionsView.ShowIndicator = false;
            this.tStaff.OptionsView.ShowVertLines = false;
            this.tStaff.ParentFieldName = "ParentId";
            this.tStaff.Size = new System.Drawing.Size(274, 167);
            this.tStaff.TabIndex = 16;
            this.tStaff.AfterExpand += new DevExpress.XtraTreeList.NodeEventHandler(this.tStaff_AfterExpand);
            this.tStaff.AfterCheckNode += new DevExpress.XtraTreeList.NodeEventHandler(this.tStaff_AfterCheckNode);
            // 
            // colName
            // 
            this.colName.Caption = "Name";
            this.colName.FieldName = "Name";
            this.colName.MinWidth = 32;
            this.colName.Name = "colName";
            this.colName.Visible = true;
            this.colName.VisibleIndex = 0;
            // 
            // colStaffId
            // 
            this.colStaffId.Caption = "StaffId";
            this.colStaffId.FieldName = "StaffId";
            this.colStaffId.Name = "colStaffId";
            // 
            // colLevel
            // 
            this.colLevel.Caption = "Level";
            this.colLevel.FieldName = "Level";
            this.colLevel.Name = "colLevel";
            // 
            // colChannelMask
            // 
            this.colChannelMask.Caption = "ChannelMask";
            this.colChannelMask.FieldName = "ChannelMask";
            this.colChannelMask.Name = "colChannelMask";
            // 
            // cStaffAll
            // 
            this.cStaffAll.Location = new System.Drawing.Point(268, -1);
            this.cStaffAll.Name = "cStaffAll";
            this.cStaffAll.Properties.Caption = "Все";
            this.cStaffAll.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.cStaffAll.Size = new System.Drawing.Size(40, 19);
            this.cStaffAll.TabIndex = 11;
            this.cStaffAll.CheckStateChanged += new System.EventHandler(this.cStaffAll_CheckStateChanged);
            this.cStaffAll.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(this.cStaffAll_EditValueChanging);
            // 
            // groupControlLevel
            // 
            this.groupControlLevel.Controls.Add(this.rUserLevel);
            this.groupControlLevel.Location = new System.Drawing.Point(258, 5);
            this.groupControlLevel.Name = "groupControlLevel";
            this.groupControlLevel.Size = new System.Drawing.Size(313, 50);
            this.groupControlLevel.TabIndex = 14;
            this.groupControlLevel.Text = "Нижний уровень персонала в отчете:";
            // 
            // rUserLevel
            // 
            this.rUserLevel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rUserLevel.EditValue = 1D;
            this.rUserLevel.Location = new System.Drawing.Point(2, 21);
            this.rUserLevel.Name = "rUserLevel";
            this.rUserLevel.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.rUserLevel.Properties.Appearance.Options.UseBackColor = true;
            this.rUserLevel.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Default;
            this.rUserLevel.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(6, "M6"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(5, "M5"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(4, "M4"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(3, "M3"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(2, "M2"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(1, "M1")});
            this.rUserLevel.Size = new System.Drawing.Size(309, 27);
            this.rUserLevel.TabIndex = 0;
            this.rUserLevel.SelectedIndexChanged += new System.EventHandler(this.rUserLevel_SelectedIndexChanged);
            // 
            // IPTSettingsForm
            // 
            this.AcceptButton = this.btnYes;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnNo;
            this.ClientSize = new System.Drawing.Size(574, 454);
            this.Controls.Add(this.groupControlLevel);
            this.Controls.Add(this.groupControlStaff);
            this.Controls.Add(this.groupControlReports);
            this.Controls.Add(this.groupControlCommon);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MaximizeBox = false;
            this.Name = "IPTSettingsForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Параметры";
            ((System.ComponentModel.ISupportInitialize)(this.panel1)).EndInit();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.imCollection)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSelectAllReports.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbDistrict.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbEquipmentClass.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.clbReports)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.staffLevelTree)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControlCommon)).EndInit();
            this.groupControlCommon.ResumeLayout(false);
            this.groupControlCommon.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbChannelType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControlReports)).EndInit();
            this.groupControlReports.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControlStaff)).EndInit();
            this.groupControlStaff.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tStaff)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cStaffAll.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControlLevel)).EndInit();
            this.groupControlLevel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.rUserLevel.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        
        #endregion

        private DevExpress.Utils.ImageCollection imCollection;
        private DevExpress.XtraEditors.SimpleButton btnHelp;
        private DevExpress.XtraEditors.SimpleButton btnNo;
        private DevExpress.XtraEditors.SimpleButton btnYes;
        private DevExpress.XtraEditors.PanelControl panel1;
        private DevExpress.XtraEditors.CheckEdit chkSelectAllReports;
        private DevExpress.XtraEditors.ComboBoxEdit cbDistrict;
        private DevExpress.XtraEditors.LabelControl lbDistrict;
        private DevExpress.XtraEditors.ComboBoxEdit cbEquipmentClass;
        private DevExpress.XtraEditors.LabelControl lbEquipmentClass;
        private DevExpress.XtraEditors.LabelControl lbChannelType;
        private DevExpress.XtraEditors.LabelControl lbDateTime;
        private DevExpress.XtraEditors.DateEdit dtDate;
        private DevExpress.XtraEditors.CheckedListBoxControl clbReports;
        private DevExpress.XtraTreeList.Columns.TreeListColumn treeListColumn1;
        private DevExpress.XtraTreeList.TreeList staffLevelTree;
        private DevExpress.XtraEditors.GroupControl groupControlCommon;
        private DevExpress.XtraEditors.GroupControl groupControlReports;
        private DevExpress.XtraEditors.GroupControl groupControlStaff;
        private DevExpress.XtraEditors.GroupControl groupControlLevel;
        private DevExpress.XtraEditors.RadioGroup rUserLevel;
        private DevExpress.XtraEditors.CheckEdit cStaffAll;
        private DevExpress.XtraTreeList.TreeList tStaff;
        private DevExpress.XtraTreeList.Columns.TreeListColumn colName;
        private DevExpress.XtraTreeList.Columns.TreeListColumn colStaffId;
        private DevExpress.XtraTreeList.Columns.TreeListColumn colLevel;
        private DevExpress.XtraTreeList.Columns.TreeListColumn colChannelMask;
        private DevExpress.XtraEditors.LookUpEdit cbChannelType;
    }
}