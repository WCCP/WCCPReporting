﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using BLToolkit.Data;
using Logica.Reports.DataAccess;

namespace Logica.Reports
{
    class DataProvider
    {
        private static DbManager _db;

        #region Properties

        private static DbManager Db
        {
            get
            {
                if (_db == null)
                {
                    DbManager.DefaultConfiguration = ""; //to reset possible previous changes
                    DbManager.AddConnectionString(HostConfiguration.DBSqlConnection);
                    _db = new DbManager();
                    _db.Command.CommandTimeout = 15 * 60; // 15 minutes by default
                }
                return _db;
            }
        }

        #endregion

        #region Settings Form

        public static DataTable GetEquipmClass
        {
            get { return Db.SetSpCommand(DbContants.spEquipmentClassList).ExecuteDataTable(); }
        }

        public static DataTable GetDistricts(object staffId, object id)
        {
            if (id == null)
            {
                return Db.SetSpCommand(DbContants.spDistrictList).ExecuteDataTable();
            }                                                        
            else
            {
                return Db.SetSpCommand(DbContants.spDistrictList,
                    Db.Parameter(DbContants.spDistrictList_Param_StaffLevel_ID, staffId),
                    Db.Parameter(DbContants.spDistrictList_Param_ID, id)).ExecuteDataTable();
            }
        }

        public static List<StaffItem> GetStaffTree(DateTime date)
        {
            return Db.SetSpCommand(DbContants.spGetStaffTree,
                Db.Parameter(DbContants.SQL_Date, date),
                Db.Parameter(DbContants.spGetStaffTree_Login, null)
                ).ExecuteList<StaffItem>();
        }

        public static DataTable GetStaffTreeOld(Object id)
        {
            if (id != null)
            {
                return Db.SetSpCommand(DbContants.spStaffTree, Db.Parameter(DbContants.spStaffTree_Param_ChanelTypeID, id))
                        .ExecuteDataTable();
            }
            else
            {
                return Db.SetSpCommand(DbContants.spStaffTree)
                        .ExecuteDataTable(); 
            }
        }

        public static List<Channel> GetChannels()
        {
            return Db.SetSpCommand(DbContants.spGetChannelList).ExecuteList<Channel>();
        }

        public static DataTable GetChannelsOld()
        {
            return Db.SetSpCommand(DbContants.spChanelList).ExecuteDataTable();
        }

        public static int GetUserLevel()
        {
           return (int)Db.SetSpCommand(DbContants.SP_GET_USER_LEVEL).ExecuteScalar();
        }

        #endregion 
    }
}
