﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using DevExpress.Data.Filtering;
using DevExpress.Data.Linq;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraGrid.Views.BandedGrid;
using DevExpress.XtraTab;
using Logica.Reports.BaseReportControl;
using Logica.Reports;
using Logica.Reports.ConfigXmlParser.GridExtension;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Grid;
using Logica.Reports.Models;

namespace WccpReporting
{
    public partial class WccpUIControl : BaseReportUserControl
    {
        #region Fields

        readonly StaffExecution2 _customTab;
        readonly IPTSettingsForm _setForm;

        private readonly Dictionary<string, List<string>> _tabColumnsToHide = new Dictionary<string, List<string>>
        {
            {"Эффективность ТТ", new List<string> {"VolumeCategoryName", "isMemberTM", "RM_Name", "NM_Name", "OLSubTypeName"}},
            {
                "По месяцам",
                new List<string>
                {
                    "Персонал",
                    "Уровень",
                    "Факт: % эффективного оборудования без учета склада",
                    "Факт: Кол-во сезонных ТТ",
                    "Факт: Кол-во оборудования в ТТ не на маршруте",
                    "Факт: Кол-во оборудования в сезонных ТТ",
                    "Факт: Кол-во оборудования в полях",
                    "Факт: Кол-во оборудования на складе",
                    "Факт: Кол-во оборудования: Итого"
                }
            },
            {"По формату", new List<string> {"POC: Характер продаж"}},
            {
                "По персонал/кластер",
                new List<string>
                {
                    "Персонал",
                    "Уровень",
                    "неэффективные без учета склада",
                    "неэффективные без учета склада %",
                    "Оборудование на складе",
                    "Оборудование на складе %",
                    "Оборудование в сезонных ТТ",
                    "Оборудование в сезонных ТТ %",
                    "Оборудование в ТТ не на маршруте",
                    "Оборудование в ТТ не на маршруте %"
                }
            },
            {
                "По персонал/тип",
                new List<string>
                {
                    "Персонал",
                    "Уровень",
                    "Кол-во оборудования на складе:",
                    "Кол-во оборудования не на маршруте:",
                    "Кол-во оборудования в сезонных ТТ:",
                    "Факт: % эффективного оборудования без учета склада",
                    "% оборудования на складе",
                    "% оборудования в сезонных ТТ",
                    "% оборудования в ТТ не на маршруте",
                    "Факт: % эффективного оборудования на дату без учета склада "
                }
            },
            {
                "Оборудование по кластерам",
                new List<string>
                {
                    "Персонал",
                    "Уровень",
                    "Warehouse # coolers doors",
                    "Warehouse % of Group",
                    "Seasonality # coolers doors",
                    "Seasonality % of Group",
                    "Not on the route # coolers doors",
                    "Not on the route % of Group",
                    "Факт: % эффективного оборудования на дату без учета склада"
                }
            },
            {
                "ТТ по кластерам",
                new List<string>
                {
                    "Персонал",
                    "Уровень",
                    "Warehouse # POC",
                    "Warehouse % of Group",
                    "Seasonality # POC",
                    "Seasonality % of Group",
                    "Not on the route # POC",
                    "Not on the route % of Group",
                }
            },
            {
                "Выполнение персоналом",
                new List<string>
                {
                    "M5",
                    "Уровень"
                }
            },
            {
                "Фокусные ТТ (тек. месяц)",
                new List<string>
                {
                    "VolumeCategoryName",
                    "isMemberTM",
                    "NM_Name",
                    "RM_Name",
                    "OLSubTypeName"
                }
            }
        };

        private readonly Dictionary<string, List<string>> _tabColumnsToHideInv = new Dictionary<string, List<string>>
        {
            {"По персонал/кластер", new List<string> {"Регион"}},
            {"По персонал/тип", new List<string> {"Регион"}},
            {"Оборудование по кластерам", new List<string> {"Регион"}},
            {"ТТ по кластерам", new List<string> {"Регион"}}
        };

        private readonly Dictionary<string, List <GroupColumnInfo>> _tabGroupColumnsToHide =
            new Dictionary<string, List<GroupColumnInfo>>
            {
                {"Оборудование по кластерам", new List<GroupColumnInfo> {new GroupColumnInfo(0, "Регион", "Персонал")}},
                {"ТТ по кластерам", new List<GroupColumnInfo> {new GroupColumnInfo(0, "Регион", "Персонал")}}
            };

        private readonly Dictionary<string, List<GroupColumnInfo>> _tabGroupColumnsToHideInv =
            new Dictionary<string, List<GroupColumnInfo>>
            {
                {"Оборудование по кластерам", new List<GroupColumnInfo> {new GroupColumnInfo(0, "Персонал", "Регион")}},
                {"ТТ по кластерам", new List<GroupColumnInfo> {new GroupColumnInfo(0, "Персонал", "Регион")}}
            };

        private readonly Dictionary<string, List<string>> _tabColumnsToReaname = new Dictionary<string, List<string>>
        {
            {"Выполнение персоналом", new List<string> {"Execution %"}},
            {"Оборудование по кластерам",new List<string> 
                {
                    "TOTAL # coolers doors", 
                    "ZERO sales # coolers doors",
                    "Below 1/2 minimum volume # coolers doors",
                    "Below minimum volume # coolers doors",
                    "Above minimum volume # coolers doors",
                    "10 times above minimum volume # coolers doors"}},
            {"Эффективность ТТ", new List<string>(){"OLSubTypeName"}},
            {"Фокусные ТТ (тек. месяц)", new List<string>(){"OLSubTypeName"}}
        };

        private readonly Dictionary<string, ColumnMapInfo> _columnCaptionsToRenameInv = new Dictionary<string, ColumnMapInfo>
        {
            {"Execution %", new ColumnMapInfo{Caption = "% POCs with effective coolers", Width = 120}},
            {"TOTAL # coolers doors", new ColumnMapInfo{Caption = "# coolers"}},
            {"ZERO sales # coolers doors", new ColumnMapInfo{Caption = "# coolers"}},
            {"Below 1/2 minimum volume # coolers doors", new ColumnMapInfo{Caption = "# coolers"}},
            {"Below minimum volume # coolers doors", new ColumnMapInfo{Caption = "# coolers"}},
            {"Above minimum volume # coolers doors", new ColumnMapInfo{Caption = "# coolers"}},
            {"10 times above minimum volume # coolers doors", new ColumnMapInfo{Caption = "# coolers"}},
            {"OLSubTypeName", new ColumnMapInfo{Caption = "Подтип ТТ"}}
        };

        private readonly Dictionary<string, ColumnMapInfo> _columnCaptionsToRenameNotInv = new Dictionary<string, ColumnMapInfo>
        {
            {"Execution %", new ColumnMapInfo{Caption = String.Empty, Width = 75}},
            {"TOTAL # coolers doors", new ColumnMapInfo{Caption = "# coolers doors"}},
            {"ZERO sales # coolers doors", new ColumnMapInfo{Caption = "# coolers doors"}},
            {"Below 1/2 minimum volume # coolers doors", new ColumnMapInfo{Caption = "# coolers doors"}},
            {"Below minimum volume # coolers doors", new ColumnMapInfo{Caption = "# coolers doors"}},
            {"Above minimum volume # coolers doors", new ColumnMapInfo{Caption = "# coolers doors"}},
            {"10 times above minimum volume # coolers doors", new ColumnMapInfo{Caption = "# coolers doors"}},
            {"OLSubTypeName", new ColumnMapInfo{Caption = "OLSubTypeName"}}
        };

        private readonly List<string> _tabsToShowHideTotals = new List<string>
        {
            "По персонал/кластер",
            "Оборудование по кластерам",
            "По персонал/тип",
            "ТТ по кластерам"
        };

        #endregion

        #region Constructor & overrides
        public WccpUIControl(int reportId)
            : base(reportId)
        {
            InitializeComponent();
            Load += WccpUIControl_Load;
            RefreshClick += WccpUIControl_RefreshClick;
            _customTab = new StaffExecution2(this);
            _setForm = new IPTSettingsForm(this);
            
            SettingsFormClick += WccpUIControl_SettingsFormClick;
        }
        #endregion

        #region Event handlers
        void WccpUIControl_SettingsFormClick(object sender, XtraTabPage selectedPage)
        {
            if (_setForm.ShowDialog() == DialogResult.OK)
            {
                UpdateSheets(_setForm.SheetParamsList);
                _customTab.UpdateSheet(_setForm.SheetParamsList);
                SetExportOptionsForPercentData();
                UpdateColumnsVisibility();
                EnableMultiSelect();
            }
        }

        private void WccpUIControl_Load(object sender, EventArgs e)
        {
            EnableMultiSelect();
        }

        private void WccpUIControl_RefreshClick(object sender, XtraTabPage selectedpage)
        {
            UpdateColumnsVisibility();
            EnableMultiSelect();
        }

        private void view_CustomColumnDisplayText(object sender, DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventArgs e)
        {
            GridView view = (GridView)sender;
            BaseTab tab = view.GridControl.Parent.Parent.Parent.Parent as BaseTab;
            Guid idGridTable = tab.IdGridColection[view];

            List<string> listColNames = Constants._tablesColumnsToHideForM1M2[idGridTable];

            if (listColNames != null && listColNames.Contains(e.Column.FieldName) && IsM1M2LevelRow(view, e))
            {
                e.DisplayText = String.Empty;
            }

        }

        private void view_CustomUnboundColumnData(object sender, DevExpress.XtraGrid.Views.Base.CustomColumnDataEventArgs e)
        {
            var view = (GridView)sender;

            if (e.Column.FieldName == "colEffectiveEquipment")
            {
                if (Convert.ToString(view.GetListSourceRowCellValue(e.ListSourceRowIndex, "Уровень")) == "M1" ||
                    Convert.ToString(view.GetListSourceRowCellValue(e.ListSourceRowIndex, "Уровень")) == "M2")
                {
                    decimal lChys = Convert.ToDecimal(view.GetListSourceRowCellValue(e.ListSourceRowIndex,
                        "Факт: % эффективного оборудования_chisl"));
                    decimal lZnam = Convert.ToDecimal(view.GetListSourceRowCellValue(e.ListSourceRowIndex, "_znam2"));
                    e.Value = lChys != 0 && lZnam != 0 ? lChys/lZnam*100 : 0;
                }
                else
                {
                    decimal lChys = Convert.ToDecimal(view.GetListSourceRowCellValue(e.ListSourceRowIndex,
                        "Факт: % эффективного оборудования_chisl"));
                    decimal lZnam = Convert.ToDecimal(view.GetListSourceRowCellValue(e.ListSourceRowIndex, "_znam1"));
                    e.Value = lChys != 0 && lZnam != 0 ? lChys / lZnam * 100 : 0;
                }
            }
        }
        #endregion

        #region Methods
        public int ReportInit()
        {
            if (_setForm.ShowDialog() == DialogResult.OK)
            {
                UpdateSheets(_setForm.SheetParamsList);
                _customTab.UpdateSheet(_setForm.SheetParamsList);
                SetExportOptionsForPercentData();
                UpdateColumnsVisibility();
                return 0;
            }

            return 1;
        }

        /// <summary>
        /// Turns multiselect on for all grids: in order to get around the problem with export to xls/xlsx files  
        /// </summary>
        private void EnableMultiSelect()
        {
            for (int tabindex = 0; tabindex < tabManager.TabPages.Count; tabindex++)
            {
                BaseTab baseTabPage = (tabManager.TabPages[tabindex] as BaseTab);
                if (baseTabPage == null) continue;

                baseTabPage.AllowMultiSelect = true;
            }
        }

        private void SetExportOptionsForPercentData()
        {
            for (int tabindex = 0; tabindex < tabManager.TabPages.Count; tabindex++)
            {
                BaseTab baseTabPage = (tabManager.TabPages[tabindex] as BaseTab);
                if (baseTabPage == null) continue;

                for (int gridIndex = 0; gridIndex < baseTabPage.Length; gridIndex++)
                {
                    BaseGridControl lGrid = baseTabPage.GetGrid(gridIndex);

                    var lTextEdit = new RepositoryItemTextEdit();
                    lTextEdit.BeginInit();
                    lTextEdit.ExportMode = ExportMode.DisplayText;
                    lGrid.GridControl.RepositoryItems.Add(lTextEdit);
                    lTextEdit.EndInit();

                    GroupBandedView lView = (GroupBandedView)lGrid.GridControl.DefaultView;

                    for (int colIndex = 0; colIndex < lView.Columns.Count; colIndex++)
                    {
                        var lColumn = lView.Columns[colIndex];
                        if (lColumn.Caption.Contains("%") || lColumn.FieldName.Contains("%"))
                        {
                            lColumn.ColumnEdit = lTextEdit;
                        }
                    }
                }
            }
        }

        private void UpdateColumnsVisibility()
        {
            if (_setForm.IsEquipmentClassInv)
            {
                RenameReportColumns(_tabColumnsToReaname, true);
                HideReportColumns(_tabColumnsToHideInv);
                HideReportGroupColumns(_tabGroupColumnsToHideInv);
            }
            else
            {
                RenameReportColumns(_tabColumnsToReaname, false);
                HideReportColumns(_tabColumnsToHide);
                HideReportGroupColumns(_tabGroupColumnsToHide);
            }
        }

        private void HideReportColumns(Dictionary<string, List<string>> tabAndColumns)
        {
            for (int tabindex = 0; tabindex < tabManager.TabPages.Count; tabindex++)
            {
                var baseTabPage = tabManager.TabPages[tabindex] as BaseTab;
                if (baseTabPage == null || !tabAndColumns.ContainsKey(baseTabPage.Text))
                {
                    continue;
                }
                var columnsToHide = tabAndColumns[baseTabPage.Text];
                HideTabColumns(columnsToHide, baseTabPage);
            }
        }

        private static void HideTabColumns(List<string> columnsToHide, BaseTab baseTabPage)
        {
            for (int gridIndex = 0; gridIndex < baseTabPage.Length; gridIndex++)
            {
                BaseGridControl grid = baseTabPage.GetGrid(gridIndex);
                var view = (GroupBandedView)grid.GridControl.DefaultView;
                view.BeginUpdate();

                for (int colIndex = 0; colIndex < view.Columns.Count; colIndex++)
                {
                    var column = view.Columns[colIndex];
                    if (columnsToHide.Contains(column.FieldName))
                    {
                        column.Visible = false;
                        UpdateBandVisibility(column);
                    }
                }
                view.EndUpdate();
            }
        }

        private void RenameReportColumns(Dictionary<string, List<string>> tabAndColumns, bool isInv)
        {
            for (int tabindex = 0; tabindex < tabManager.TabPages.Count; tabindex++)
            {
                var baseTabPage = tabManager.TabPages[tabindex] as BaseTab;
                if (baseTabPage == null || (!tabAndColumns.ContainsKey(baseTabPage.Text)
                    && !_tabsToShowHideTotals.Contains(baseTabPage.Text)
                    && !Constants._tabsTablesToHideForM1M2.ContainsKey(baseTabPage.Text)))
                {
                    continue;
                }
                if (tabAndColumns.ContainsKey(baseTabPage.Text))
                {
                    var columnsToRename = tabAndColumns[baseTabPage.Text];
                    RenameColumn(columnsToRename, baseTabPage, isInv);
                }

                for (int gridIndex = 0; gridIndex < baseTabPage.Length; gridIndex++)
                {
                    BaseGridControl grid = baseTabPage.GetGrid(gridIndex);
                    var view = (GridView) grid.GridControl.DefaultView;
                    
                    if (_tabsToShowHideTotals.Contains(baseTabPage.Text))
                    {
                        view.OptionsView.ShowFooter = !isInv;
                    }
                    if (isInv)
                    {
                        view.GroupSummary.Clear();
                        if (gridIndex == 1 && baseTabPage.Text == "По персонал/тип")
                        {
                            view.CustomUnboundColumnData += view_CustomUnboundColumnData;
                        }
                        Guid gridTableId = baseTabPage.IdGridColection[view];
                        if (Constants._tablesColumnsToHideForM1M2.ContainsKey(gridTableId))
                        {
                            view.CustomColumnDisplayText += view_CustomColumnDisplayText;
                        }
                    }
                }
            }
        }

        private void RenameColumn(List<string> colToRename, BaseTab baseTabPage, bool isInv)
        {
            for (int gridIndex = 0; gridIndex < baseTabPage.Length; gridIndex++)
            {
                BaseGridControl grid = baseTabPage.GetGrid(gridIndex);
                var view = (GroupBandedView) grid.GridControl.DefaultView;
                view.BeginUpdate();

                for (int colIndex = 0; colIndex < view.Columns.Count; colIndex++)
                {
                    var column = view.Columns[colIndex];
                    if (colToRename.Contains(column.FieldName))
                    {
                        if (isInv)
                        {
                            column.Caption = _columnCaptionsToRenameInv[column.FieldName].Caption;
                            column.Width = _columnCaptionsToRenameInv[column.FieldName].Width == 0 ?
                                column.Width : _columnCaptionsToRenameInv[column.FieldName].Width;
                        }
                        else
                        {
                            column.Caption = _columnCaptionsToRenameNotInv[column.FieldName].Caption;
                            column.Width = _columnCaptionsToRenameNotInv[column.FieldName].Width == 0 ?
                                column.Width : _columnCaptionsToRenameNotInv[column.FieldName].Width;
                        }
                    }
                }
                view.EndUpdate();
            }
        }

        private static void UpdateBandVisibility(BandedGridColumn column)
        {
            if (column.OwnerBand != null && column.OwnerBand.Columns.VisibleColumnCount == 0)
            {
                column.OwnerBand.Visible = false;
            }
        }

        private void HideReportGroupColumns(Dictionary<string, List<GroupColumnInfo>> tabAndGroupColumns)
        {
            for (int tabindex = 0; tabindex < tabManager.TabPages.Count; tabindex++)
            {
                var baseTabPage = tabManager.TabPages[tabindex] as BaseTab;
                if (baseTabPage == null || !tabAndGroupColumns.ContainsKey(baseTabPage.Text))
                {
                    continue;
                }
                var columnsToHide = tabAndGroupColumns[baseTabPage.Text];
                HideTabGroupColumns(columnsToHide, baseTabPage);
            }
        }

        private static void HideTabGroupColumns(List<GroupColumnInfo> groupColumnsToHide, BaseTab baseTabPage)
        {
            for (int gridIndex = 0; gridIndex < baseTabPage.Length; gridIndex++)
            {
                BaseGridControl grid = baseTabPage.GetGrid(gridIndex);
                var view = (GroupBandedView)grid.GridControl.DefaultView;
                view.BeginUpdate();
                var groupColumnInfo = groupColumnsToHide.FirstOrDefault(gci => gci.GridIndex == gridIndex);
                if (groupColumnInfo == null)
                {
                    continue;
                }

                for (int colIndex = 0; colIndex < view.Columns.Count; colIndex++)
                {
                    var column = view.Columns[colIndex];
                    if (groupColumnInfo.FieldNameToHide == column.FieldName)
                    {
                        column.GroupIndex = -1;
                    }
                    if (groupColumnInfo.FieldNameToShow == column.FieldName)
                    {
                        column.GroupIndex = 0;
                    }
                }
                view.EndUpdate();
            }
        }

        private bool IsM1M2LevelRow(GridView view, DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventArgs e)
        {
            if ((Convert.ToString(view.GetListSourceRowCellValue(e.ListSourceRowIndex, "Уровень")) == "M1") ||
                (Convert.ToString(view.GetListSourceRowCellValue(e.ListSourceRowIndex, "Уровень")) == "M2"))
                return true;
            return false;
        }

        #endregion
    }
}
