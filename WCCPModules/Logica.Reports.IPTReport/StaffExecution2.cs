﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;
using DevExpress.Utils;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraPivotGrid;
using DevExpress.XtraPrinting;
using DevExpress.XtraPrintingLinks;
using DevExpress.XtraTab;
using Logica.Reports.BaseReportControl;
using Logica.Reports.BaseReportControl.Utility;
using Logica.Reports.Common.WaitWindow;
using Logica.Reports.DataAccess;

namespace Logica.Reports
{
    public class StaffExecution2 : XtraTabPage, IPrint
    {
        #region Readonly & Static Fields

        private static Guid tabId = Guid.NewGuid();

        #endregion

        #region Fields

        private PivotGridField execution;
        private PivotGridField factVolume;

        private PanelControl headerPanel = new PanelControl();
        private LabelControl lbDate = new LabelControl();
        private LabelControl lbDateHeader = new LabelControl();
        private LabelControl lbEquipment = new LabelControl();
        private LabelControl lbEquipmentHeader = new LabelControl();
        private PivotGridField noSales, noSales3m;
        private BaseReportUserControl parent;
        private PivotGridControl pivotGrid = new PivotGridControl();
        private PivotGridField planeVolume;
        private PivotGridField pocDone, pocNotDone;
        private SheetParamCollection sheetSettings;

        #endregion

        #region Constructors

        public StaffExecution2(BaseReportUserControl parent)
        {
            this.parent = parent;
            InitializeComponent();
            parent.RefreshClick += parent_RefreshClick;
            parent.ExportClick += parent_ExportClick;
            parent.CloseClick += parent_CloseClick;
            pivotGrid.CustomCellDisplayText += pivotGrid_CustomCellDisplayText;
            SetupColumns();
        }

        #endregion

        #region Instance Methods

        public void UpdateData(SheetParamCollection parameters, bool isForce)
        {
            if (!isForce && CompareSettings(parameters))
            {
                return;
            }

            WaitManager.StartWait();
            CreateSettingSection(parameters);
            parent.ConstructTemporaryData(parameters);
            List<SqlParameter> list = new List<SqlParameter>();
            if (parameters.Exists(p => p.SqlParamName == "@Date"))
                list.Add(new SqlParameter("@Date", parameters.Find(p => p.SqlParamName == "@Date").Value) {SqlDbType = SqlDbType.DateTime});
            if (parameters.Exists(p => p.SqlParamName == "@EquipmentClass_ID"))
                list.Add(new SqlParameter("@EquipmentClass_ID", parameters.Find(p => p.SqlParamName == "@EquipmentClass_ID").Value) {SqlDbType = SqlDbType.Int});
            if (parameters.Exists(p => p.SqlParamName == "@debug"))
                list.Add(new SqlParameter("@debug", parameters.Find(p => p.SqlParamName == "@debug").Value) {SqlDbType = SqlDbType.Bit});

            pivotGrid.DataSource = DataAccessLayer.ExecuteStoredProcedure("spDW_IPTR_ReportStaffExecution", list.ToArray());
            pivotGrid.DataMember = "Table";
            pivotGrid.BestFit();
            parent.DestructTemporaryData();
            sheetSettings = parameters;
            WaitManager.StopWait();
        }

        public void UpdateSheet(List<SheetParamCollection> listSettings)
        {
            SheetParamCollection settings = listSettings.Find(p => p.TabId == TabId);
            if (null != settings)
            {
                if (!parent.TabControl.TabPages.Contains(this))
                    parent.TabControl.TabPages.Add(this);
                UpdateData(settings);
            }
            else if (parent.TabControl.TabPages.Contains(this))
            {
                parent.TabControl.TabPages.Remove(this);
            }
        }

        private bool CompareSettings(SheetParamCollection settings)
        {
            if (null == sheetSettings || sheetSettings.Count != settings.Count)
                return false;
            foreach (SheetParam param in settings)
                if (!sheetSettings.Exists(p => p.SqlParamName.Equals(param.SqlParamName) && p.Value.Equals(param.Value)))
                    return false;
            return true;
        }

        private void CreateSettingSection(SheetParamCollection dispPar)
        {
            lbDateHeader.Text = dispPar[0].DisplayParamName;
            lbDate.Text = dispPar[0].DisplayValue.ToString();
            lbEquipmentHeader.Text = dispPar[1].DisplayParamName;
            lbEquipment.Text = dispPar[1].DisplayValue.ToString();
        }

        private void InitializeComponent()
        {
            Text = TabName;
            Tag = 0;
            AutoScroll = true;
            //Create main grid
            pivotGrid.Dock = DockStyle.Fill;
            pivotGrid.OptionsPrint.PrintFilterHeaders = DefaultBoolean.False;
            pivotGrid.OptionsPrint.PrintColumnHeaders = DefaultBoolean.False;
            pivotGrid.OptionsPrint.PrintDataHeaders = DefaultBoolean.False;
            pivotGrid.OptionsPrint.PrintRowHeaders = DefaultBoolean.False;
            pivotGrid.OptionsPrint.PrintHeadersOnEveryPage = true;
            pivotGrid.CustomCellValue += pivotGrid_CustomCellValue;
            Controls.Add(pivotGrid);
            //Create header
            int shift = 75;
            headerPanel.Dock = DockStyle.Top;
            headerPanel.Height = 20;
            headerPanel.Controls.AddRange(new Control[]
                                              {
                                                  lbDateHeader, lbDate, lbEquipmentHeader, lbEquipment
                                              });
            lbDateHeader.Font = new System.Drawing.Font(lbDateHeader.Font, System.Drawing.FontStyle.Bold);
            lbDateHeader.Width = 40;
            lbDateHeader.Location = new System.Drawing.Point(10 + shift, 2);
            lbDate.Width = 100;
            lbDate.Location = new System.Drawing.Point(50 + shift, 2);

            lbEquipmentHeader.Font = new System.Drawing.Font(lbEquipmentHeader.Font, System.Drawing.FontStyle.Bold);
            lbEquipmentHeader.Width = 100;
            lbEquipmentHeader.Location = new System.Drawing.Point(175 + shift, 2);
            lbEquipment.Width = 100;
            lbEquipment.Location = new System.Drawing.Point(275 + shift, 2);
            Controls.Add(headerPanel);
        }

        private void SetupColumns()
        {
            PivotGridField rmName = new PivotGridField("RM_Name", PivotArea.FilterArea) {Caption = Resource.RMNameColumn};
            PivotGridField dsmName = new PivotGridField("DSM_Name", PivotArea.FilterArea) {Caption = Resource.DSMNameColumn};
            PivotGridField svName = new PivotGridField("Supervisor_name", PivotArea.FilterArea) {Caption = Resource.SupervisorNameColumn};
            PivotGridField merchName = new PivotGridField("MerchName", PivotArea.RowArea) {Caption = Resource.MerchNameColumn};
            PivotGridField channel = new PivotGridField("Channel", PivotArea.FilterArea) {Caption = Resource.ChannelColumn};
            PivotGridField custName = new PivotGridField("Cust_name", PivotArea.FilterArea) {Caption = Resource.CustNameColumn};
            PivotGridField month = new PivotGridField("Month", PivotArea.FilterArea);
            planeVolume = new PivotGridField("Plan Volume", PivotArea.DataArea);
            factVolume = new PivotGridField("Fact Volume", PivotArea.DataArea);
            noSales = new PivotGridField("POC with 0 sales", PivotArea.DataArea);
            noSales3m = new PivotGridField("POC with 0 sales for last 3 m", PivotArea.DataArea);
            execution = new PivotGridField("Execution %", PivotArea.DataArea);
            execution.CellFormat.FormatType = FormatType.Numeric;
            execution.CellFormat.FormatString = "P2";
            pocDone = new PivotGridField("POC done plan 3 m", PivotArea.DataArea);
            pocNotDone = new PivotGridField("POC not done plan 3 m", PivotArea.DataArea);


            pivotGrid.Fields.AddRange(new[]
                                          {
                                              rmName, dsmName, svName, merchName,
                                              channel, custName, month, planeVolume,
                                              factVolume, noSales, noSales3m, execution,
                                              pocDone, pocNotDone
                                          });
        }

        private void UpdateData(SheetParamCollection settings)
        {
            UpdateData(settings, false);
        }

        #endregion

        #region Event Handling

        private void parent_CloseClick(object sender, XtraTabPage selectedPage)
        {
            if (selectedPage == this)
            {
                if (parent.TabControl.TabPages.Contains(this))
                {
                    parent.TabControl.TabPages.Remove(this);
                }
            }
        }

        private void parent_ExportClick(object sender, XtraTabPage selectedPage, ExportToType type, string fName)
        {
            if (this != selectedPage)
            {
                return;
            }

            switch (type)
            {
                case ExportToType.Html:
                    PrepareCompositeLink().PrintingSystem.ExportToHtml(fName);
                    break;
                case ExportToType.Mht:
                    PrepareCompositeLink().PrintingSystem.ExportToMht(fName);
                    break;
                case ExportToType.Pdf:
                    PrepareCompositeLink().PrintingSystem.ExportToPdf(fName, new PdfExportOptions {Compressed = true});
                    break;
                case ExportToType.Rtf:
                    PrepareCompositeLink().PrintingSystem.ExportToRtf(fName);
                    break;
                case ExportToType.Txt:
                    PrepareCompositeLink().PrintingSystem.ExportToText(fName);
                    break;
                case ExportToType.Xls:
                    PrepareCompositeLink().PrintingSystem.ExportToXls(fName, new XlsExportOptions {SheetName = TabName});
                    break;
                case ExportToType.Xlsx:
                    PrepareCompositeLink().PrintingSystem.ExportToXlsx(fName);
                    break;
                case ExportToType.Bmp:
                    PrepareCompositeLink().PrintingSystem.ExportToImage(fName);
                    break;
                case ExportToType.Csv:
                    PrepareCompositeLink().PrintingSystem.ExportToCsv(fName);
                    break;
            }
        }

        private void parent_RefreshClick(object sender, XtraTabPage selectedPage)
        {
            if (this != selectedPage)
            {
                return;
            }
            UpdateData(sheetSettings, true);
        }

        private void pivotGrid_CustomCellDisplayText(object sender, PivotCellDisplayTextEventArgs e)
        {
            if (e.DataField == planeVolume || e.DataField == factVolume || e.DataField == noSales ||
                e.DataField == noSales3m || e.DataField == execution || e.DataField == pocDone || e.DataField == pocNotDone)
            {
                if (e.Value == null || String.IsNullOrEmpty(e.Value.ToString()) || e.Value.Equals(0m))
                {
                    e.DisplayText = string.Empty;
                }
            }
        }

        private void pivotGrid_CustomCellValue(object sender, PivotCellValueEventArgs e)
        {
            if (e.DataField == execution)
            {
                PivotDrillDownDataSource pds = e.CreateDrillDownDataSource();
                decimal ve = 0;
                decimal vne = 0;
                for (int i = 0; i < pds.RowCount; i++)
                {
                    ve += ConvertEx.ToDecimal(pds.GetValue(i, pocDone));
                    vne += ConvertEx.ToDecimal(pds.GetValue(i, pocNotDone));
                }
                e.Value = (ve + vne) == 0 ? (object) null : ve/(ve + vne);
            }
        }

        #endregion

        #region IPrint Members

        public CompositeLink PrepareCompositeLink()
        {
            CompositeLink pl = new CompositeLink(new PrintingSystem());
            GridControl tmpGrid = new GridControl();
            GridView tmprGridView = new GridView();
            tmprGridView.OptionsPrint.PrintHeader = false;
            tmpGrid.MainView = tmprGridView;

            DataTable source = new DataTable();
            List<object> lst = new List<object>();
            while (source.Columns.Count < 4)
            {
                source.Columns.Add();
                lst.Add("");
            }
            source.Rows.Add(lst.ToArray());
            source.Rows[0][0] = lbDateHeader.Text;
            source.Rows[0][1] = lbDate.Text;
            source.Rows[0][2] = lbEquipmentHeader.Text;
            source.Rows[0][3] = lbEquipment.Text;
            tmpGrid.DataSource = source;
            Controls.Add(tmpGrid);
            pl.Links.Add(new PrintableComponentLink {Component = tmpGrid});
            pl.Links.Add(new PrintableComponentLink {Component = pivotGrid});
            pl.CreateDocument();
            Controls.Remove(tmpGrid);
            return pl;
        }

        #endregion

        #region Class Properties

        public static Guid TabId
        {
            get { return tabId; }
        }

        public static string TabName
        {
            get { return Resource.StaffExecution2; }
        }

        #endregion
    }
}