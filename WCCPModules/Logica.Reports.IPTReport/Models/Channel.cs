﻿using BLToolkit.Mapping;

namespace Logica.Reports
{
    public class Channel
    {
        [MapField("ID")]
        public int Id { get; set; }

        [MapField("ChannelName")]
        public string Name { get; set; }

        [MapField("ChannelMask")]
        public int Mask { get; set; }

        public override string ToString()
        {
            return Name ?? string.Empty;
        }

        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            if (obj is Channel)
            {
                return Id.Equals((obj as Channel).Id);
            }

            return ReferenceEquals(obj, this);
        }
    }
}
