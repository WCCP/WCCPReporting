﻿namespace Logica.Reports.Models
{
    public class GroupColumnInfo
    {
        public int GridIndex { get; private set; }
        public string FieldNameToShow { get; private set; }
        public string FieldNameToHide { get; private set; }

        public GroupColumnInfo(int gridIndex, string fieldNameToShow, string fieldNameToHide)
        {
            GridIndex = gridIndex;
            FieldNameToShow = fieldNameToShow;
            FieldNameToHide = fieldNameToHide;
        }
    }
}
