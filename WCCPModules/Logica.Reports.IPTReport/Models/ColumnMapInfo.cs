﻿
namespace Logica.Reports.Models
{
    class ColumnMapInfo
    {
        public int Width { get; set; }
        public int Height { get; set; }
        public string Caption { get; set; }
        public bool? Visible { get; set; }
        public string Field { get; set; }
    }
}
