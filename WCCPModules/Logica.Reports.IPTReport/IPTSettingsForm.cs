﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraTab;
using DevExpress.XtraTreeList;
using DevExpress.XtraTreeList.Nodes;
using Logica.Reports.BaseReportControl;
using Logica.Reports.ConfigXmlParser.Model;
using Logica.Reports.DataAccess;
using ComboBoxItem = Logica.Reports.BaseReportControl.CommonControls.Addresses.ComboBoxItem;

namespace Logica.Reports
{
    public partial class IPTSettingsForm : XtraForm
    {
        #region Fields

        private BaseReportUserControl parent;

        private readonly List<string> _tabsOnlyForEquipmentClassInv = new List<string> { "Оборудование на складе", "Оборудование в сезонных ТТ", "По категории" };

        #endregion

        #region Constructors

        public IPTSettingsForm(BaseReportUserControl parent)
        {
            InitializeComponent();

            //BuildDateControl
            dtDate.DateTime = DateTime.Now;
            //BuildChannelType
            BuildChannelType(DataRepository.Channels);
            // BuildTree
            cbChannelType_EditValueChanged(null, null);
            // BuildEquipment
            BuildEquipment();
            //BuildDistrict
            BuildDistrict(null, null);
            // BuildReportList
            clbReports.Items.Clear();
            clbReports.Items.Add(StaffExecution2.TabId, StaffExecution2.TabName);
            foreach (Tab tab in parent.Report.Tabs)
                clbReports.Items.Add(tab.Id, tab.Header);
            UpdateReportsListBox();

            this.parent = parent;
            parent.CloseClick += parent_CloseClick;
        }

        #endregion

        #region Instance Properties

        public List<SheetParamCollection> SheetParamsList
        {
            get
            {
                List<SheetParamCollection> lst = new List<SheetParamCollection>();
                foreach (CheckedListBoxItem item in clbReports.CheckedItems)
                {
                    SheetParamCollection pars = new SheetParamCollection
                                                    {
                                                        MainHeader = Resource.DSP_MainHeader,
                                                        TabId = (Guid)item.Value,
                                                        TableDataType = TableType.Fact
                                                    };
                    pars.Add(new SheetParam
                                 {
                                     DisplayParamName = Resource.DSP_Date,
                                     SqlParamName = DbContants.SQL_Date,
                                     Value = dtDate.DateTime.Date,
                                     DisplayValue = dtDate.DateTime.ToShortDateString()
                                 });
                    pars.Add(new SheetParam
                                 {
                                     DisplayParamName = Resource.DSP_Equipment,
                                     SqlParamName = DbContants.SQL_EquipmentClassID,
                                     Value = ((ComboBoxItem)cbEquipmentClass.SelectedItem).Id,
                                     DisplayValue = ((ComboBoxItem)cbEquipmentClass.SelectedItem).Text
                                 });
                    pars.Add(new SheetParam
                                  {
                                      DisplayParamName = Resource.DSP_ChannelType,
                                      SqlParamName = DbContants.SQL_ChanelTypeID,
                                      Value = cbChannelType.EditValue,//------Gap1003
                                      DisplayValue = cbChannelType.Text
                                  });
                    pars.Add(new SheetParam
                                 {
                                     DisplayParamName = Resource.DSP_StaffLevel,
                                     SqlParamName = DbContants.SQL_StaffLevelID,
                                     Value = IsEquipmentClassInv ? StaffLevel : ((IList<object>)staffLevelTree.Selection[0].Tag)[1],
                                     DisplayValue = IsEquipmentClassInv ? null : staffLevelTree.Selection[0].GetDisplayText(0)
                                 });
                    pars.Add(new SheetParam
                    {
                        SqlParamName = DbContants.SQL_ID,
                        Value = IsEquipmentClassInv ? DBNull.Value :
                            ((IList<object>)staffLevelTree.Selection[0].Tag)[2]
                    });
                    pars.Add(new SheetParam
                                 {
                                     DisplayParamName = Resource.DSP_District,
                                     SqlParamName = DbContants.SQL_DistrictID,
                                     Value = ((ComboBoxItem)cbDistrict.SelectedItem).Id,
                                     DisplayValue = ((ComboBoxItem)cbDistrict.SelectedItem).Text
                                 });
                    pars.Add(new SheetParam { SqlParamName = DbContants.SQL_Debug, Value = 0 });

                    //------Gap1003
                    pars.Add(new SheetParam
                    {
                        SqlParamName = DbContants.spStaffIdList,
                        Value = IsEquipmentClassInv ? GetM1Checked() : null
                    });

                    pars.Add(new SheetParam
                    {
                        SqlParamName = DbContants.spLowestLevel,
                        Value = IsEquipmentClassInv ? rUserLevel.EditValue : 1
                    });

                    lst.Add(pars);
                }
                return lst;
            }
        }

        public bool IsEquipmentClassInv
        {
            get
            {
                bool res = false;
                if (cbEquipmentClass.SelectedItem != null)
                {
                    int equpmentId = Convert.ToInt32(((ComboBoxItem)cbEquipmentClass.SelectedItem).Id);
                    res = DbContants.EquipmentClassesInv.Contains(equpmentId);
                }
                return res;
            }
        }

        private int StaffLevel { get; set; }

        #endregion

        #region Instance Methods

        public bool ValidateData()
        {
            bool isValid = false;
            if (dtDate.DateTime > DateTime.Now)
                MessageBox.Show(Resource.InvalidDate, Resource.Error, MessageBoxButtons.OK, MessageBoxIcon.Warning);
            else if (null == cbChannelType.EditValue || String.IsNullOrEmpty(cbChannelType.EditValue.ToString()))
                MessageBox.Show(Resource.IncorrectChannel, Resource.Error, MessageBoxButtons.OK, MessageBoxIcon.Warning);
            else if (null == staffLevelTree.Selection || 0 == staffLevelTree.Selection.Count || String.IsNullOrEmpty(staffLevelTree.Selection[0].ToString()))
                MessageBox.Show(Resource.IncorrectStaffLevel, Resource.Error, MessageBoxButtons.OK, MessageBoxIcon.Warning);
            else if (null == cbDistrict.SelectedItem || String.IsNullOrEmpty(cbDistrict.SelectedItem.ToString()))
                MessageBox.Show(Resource.IncorrectDistrict, Resource.Error, MessageBoxButtons.OK, MessageBoxIcon.Warning);
            else if (null == cbEquipmentClass.SelectedItem || String.IsNullOrEmpty(cbDistrict.SelectedItem.ToString()))
                MessageBox.Show(Resource.IncorrectEquipmentClass, Resource.Error, MessageBoxButtons.OK, MessageBoxIcon.Warning);
            else
                isValid = true;
            return isValid;
        }

        private void AddChildNodes(TreeListNode parentNode, DataTable table)
        {
            foreach (DataRow dr in table.Rows)
            {
                if ((null == parentNode && dr[DbContants.spStaffTree_ParentID].Equals(0)) ||
                    (null != parentNode && dr[DbContants.spStaffTree_ParentID].Equals(((IList<object>)parentNode.Tag)[0])))
                {
                    TreeListNode node = staffLevelTree.AppendNode(new[] { dr[DbContants.spStaffTree_Name] }, parentNode);
                    node.Tag = new List<object>
                                   {
                                       dr[DbContants.spStaffTree_ItemID],
                                       dr[DbContants.spStaffTree_StaffLevelID],
                                       dr[DbContants.spStaffTree_ID]
                                   };
                    AddChildNodes(node, table);
                }
            }
        }

        private void BuildChannelType(List<Channel> list)
        {
            cbChannelType.Properties.DataSource = null;
            cbChannelType.Properties.DataSource = list;
            cbChannelType.Properties.DropDownRows = list.Count;
        }

        private void BuildDistrict(object staffId, object id)
        {
            cbDistrict.Properties.Items.Clear();
            DataTable dt = DataProvider.GetDistricts(staffId, id);
            foreach (DataRow dr in dt.Rows)
            {
                cbDistrict.Properties.Items.Add(new ComboBoxItem(dr[DbContants.spDistrictList_DistrictID], dr[DbContants.spDistrictList_DistrictName].ToString()));
            }
            cbDistrict.SelectedIndex = 0;
        }

        private void BuildEquipment()
        {
            DataTable dt = DataProvider.GetEquipmClass;
            foreach (DataRow dr in dt.Rows)
            {
                cbEquipmentClass.Properties.Items.Add(new ComboBoxItem(dr[DbContants.spEquipmentClassList_EquipmentClassID],
                                                                       dr[DbContants.spEquipmentClassList_EquipmentClassName].ToString()));
            }
            cbEquipmentClass.SelectedIndex = 0;
        }

        private void BuildTree(object id)
        {
            staffLevelTree.ClearNodes();
            DataTable dt = DataProvider.GetStaffTreeOld(id);
            AddChildNodes(null, dt);
        }

        private void BuildNewTree(List<StaffItem> list)
        {
            if (list != null)
            {
                tStaff.BeginUpdate();
                tStaff.DataSource = list;
                //tStaff.ExpandAll();
                tStaff.CollapseAll();
                tStaff.EndUpdate();
            }
        }

        private void GetStaffAllCheckState()
        {
            int lChecked = tStaff.Nodes.Count(n => n.CheckState == CheckState.Checked && n.Visible);
            int lUnhecked = tStaff.Nodes.Count(n => n.CheckState == CheckState.Unchecked && n.Visible);
            int lVisible = tStaff.Nodes.Count(n => n.Visible);

            cStaffAll.EditValueChanging -= cStaffAll_EditValueChanging;
            cStaffAll.CheckStateChanged -= cStaffAll_CheckStateChanged;

            if (lChecked == lVisible)
            {
                cStaffAll.CheckState = CheckState.Checked;
            }
            else if (lUnhecked == lVisible)
            {
                cStaffAll.CheckState = CheckState.Unchecked;
            }
            else
            {
                cStaffAll.CheckState = CheckState.Indeterminate;
            }

            cStaffAll.EditValueChanging += cStaffAll_EditValueChanging;
            cStaffAll.CheckStateChanged += cStaffAll_CheckStateChanged;
        }

        private void FilterStaffTree()
        {
            byte lChannelMask = GetChannelMask();

            tStaff.BeginUpdate();

            FilterNodeOperation lOperation = new FilterNodeOperation(colLevel.FieldName, (int)rUserLevel.EditValue, colChannelMask.FieldName, lChannelMask);
            tStaff.NodesIterator.DoOperation(lOperation);

            tStaff.EndUpdate();

            GetStaffAllCheckState();
        }

        private byte GetChannelMask()
        {
            Channel c = (Channel)cbChannelType.Properties.GetDataSourceRowByKeyValue(cbChannelType.EditValue);
            if (c.Mask != 0)
            {
                return (byte)c.Mask;
            }
            else
            {
                return (byte)((List<Channel>)cbChannelType.Properties.DataSource).Sum(ch => ch.Mask);
            }
        }

        private void RedrawSettingForm(bool newTree)
        {
            if (newTree)
            {
                StaffLevel = DataRepository.UserLevel;

                BuildNewTree(DataRepository.GetNewStaff(dtDate.DateTime.Date));
                tStaff.CheckAll();

                //hide old staff treee
                staffLevelTree.Visible = false;
                staffLevelTree.Enabled = false;

                //Show new tree
                groupControlLevel.Visible = true;
                groupControlLevel.Enabled = true;
                cStaffAll.Visible = true;
                cStaffAll.Enabled = true;
                groupControlStaff.Location = new Point(groupControlLevel.Location.X,
                    groupControlLevel.Bottom + 3);
                groupControlStaff.Size = new Size(groupControlLevel.Size.Width,
                    groupControlReports.Bottom - groupControlLevel.Bottom - 3);
                tStaff.Visible = true;
                tStaff.Enabled = true;
                tStaff.Dock = DockStyle.Fill;
                UpdateRLowestLevel();
            }
            else
            {
                //hide new treee
                tStaff.Visible = false;
                tStaff.Enabled = false;

                //Show old tree
                groupControlLevel.Visible = false;
                groupControlLevel.Enabled = false;
                cStaffAll.Visible = false;
                cStaffAll.Enabled = false;
                groupControlStaff.Location = new Point(groupControlLevel.Location.X,
                    groupControlLevel.Location.Y);
                groupControlStaff.Size = new Size(groupControlLevel.Size.Width,
                    groupControlReports.Bottom - groupControlLevel.Top);
                staffLevelTree.Visible = true;
                staffLevelTree.Enabled = true;
                staffLevelTree.Dock = DockStyle.Fill;
            }
        }

        private string GetM1Checked()
        {
            var checkedNodes = tStaff.GetAllCheckedNodes()
                .Where(node => node.Visible && ((int)node.GetValue(colLevel)) <= StaffLevel).ToList();
            List<Int32> listId = new List<Int32>();
            if (checkedNodes != null && checkedNodes.Count > 0)
            {
                listId =
                    checkedNodes.Select(node => node.GetValue(colStaffId).ToString())
                        .ToList()
                        .Select(t => Convert.ToInt32(t))
                        .ToList();
                return string.Join(",", listId.ConvertAll(i => i.ToString()).ToArray());
            }
            return string.Empty;
        }

        private void UpdateStaffTree()
        {
            if (IsEquipmentClassInv)
            {
                FilterStaffTree();
                cStaffAll.CheckState = CheckState.Checked;
                tStaff.CollapseAll();
            }
            else
            {
                if (!String.IsNullOrEmpty(cbChannelType.EditValue.ToString()))
                {
                    BuildTree(cbChannelType.EditValue);
                }
            }
        }

        private void UpdateReportsListBox()
        {
            bool enableTabs = IsEquipmentClassInv;
            foreach (CheckedListBoxItem item in clbReports.Items)
            {
                if (_tabsOnlyForEquipmentClassInv.Contains(item.Description))
                {
                    item.CheckState = !enableTabs ? CheckState.Unchecked : item.CheckState;
                    item.Enabled = enableTabs;
                }
            }
        }

        private void UpdateRLowestLevel()
        {
            int lStaffLevel = StaffLevel;
            foreach (RadioGroupItem itm in rUserLevel.Properties.Items)
            {
                if ((int)itm.Value > lStaffLevel)
                {
                    itm.Enabled = false;
                }
            }
            rUserLevel.EditValue = lStaffLevel - 1;
        }

        #endregion

        #region Event Handling

        private void btnYes_Click(object sender, EventArgs e)
        {
            if (ValidateData())
                DialogResult = DialogResult.OK;
        }

        private void chkSelectAllReports_CheckedChanged(object sender, EventArgs e)
        {
            if (IsEquipmentClassInv)
            {
                foreach (CheckedListBoxItem item in clbReports.Items)
                {
                    item.CheckState = chkSelectAllReports.Checked ? CheckState.Checked : CheckState.Unchecked;
                }
            }
            else
            {
                foreach (CheckedListBoxItem item in clbReports.Items)
                {
                    item.CheckState = chkSelectAllReports.Checked && !_tabsOnlyForEquipmentClassInv.Contains(item.Description) ?
                        CheckState.Checked : CheckState.Unchecked;
                }
            }
        }

        private void clbReports_ItemCheck(object sender, DevExpress.XtraEditors.Controls.ItemCheckEventArgs e)
        {
            if (clbReports.Items.Cast<CheckedListBoxItem>().All(item => item.CheckState == CheckState.Checked))
            {
                chkSelectAllReports.Checked = true;
            }
            if (clbReports.Items.Cast<CheckedListBoxItem>().All(item => item.CheckState == CheckState.Unchecked))
            {
                chkSelectAllReports.Checked = false;
            }
        }

        private void parent_CloseClick(object sender, XtraTabPage selectedPage)
        {
            Guid guid;

            if (selectedPage is BaseTab)
                guid = (selectedPage as BaseTab).SheetSettings.TabId;
            else if (selectedPage is StaffExecution2)
                guid = StaffExecution2.TabId;
            else
                throw new ArgumentNullException("Undefined selectedPage type");
            foreach (CheckedListBoxItem item in clbReports.Items)
            {
                if (item.Value.ToString() == guid.ToString())
                {
                    item.CheckState = CheckState.Unchecked;
                    break;
                }
            }
        }

        private void staffLevelTree_SelectionChanged(object sender, EventArgs e)
        {
            if (null != staffLevelTree.Selection[0] && null != staffLevelTree.Selection[0].Tag)
            {
                Cursor = Cursors.WaitCursor;
                BuildDistrict(((IList<object>)staffLevelTree.Selection[0].Tag)[1], ((IList<object>)staffLevelTree.Selection[0].Tag)[2]);
                Cursor = Cursors.Default;
            }
        }

        private void cStaffAll_CheckStateChanged(object sender, EventArgs e)
        {
            if (cStaffAll.CheckState == CheckState.Checked)
            {
                tStaff.BeginUpdate();
                tStaff.CheckAll();
                tStaff.EndUpdate();
            }

            if (cStaffAll.CheckState == CheckState.Unchecked)
            {
                tStaff.BeginUpdate();
                tStaff.UncheckAll();
                tStaff.EndUpdate();
            }
        }

        private void cStaffAll_EditValueChanging(object sender, ChangingEventArgs e)
        {
            if (e.NewValue == null)
            {
                e.NewValue = !Convert.ToBoolean(e.OldValue);
            }
        }

        private void tStaff_AfterCheckNode(object sender, NodeEventArgs e)
        {
            GetStaffAllCheckState();
        }

        private void rUserLevel_SelectedIndexChanged(object sender, EventArgs e)
        {
            FilterStaffTree();
            cStaffAll.CheckState = CheckState.Checked;
            tStaff.CollapseAll();
        }

        private void cbChannelType_EditValueChanged(object sender, EventArgs e)
        {
            UpdateStaffTree();
        }

        private void cbEquipmentClass_SelectedValueChanged(object sender, EventArgs e)
        {
            UpdateReportsListBox();
            RedrawSettingForm(IsEquipmentClassInv);
            UpdateStaffTree();
            chkSelectAllReports_CheckedChanged(null, null);
        }

        private void tStaff_AfterExpand(object sender, NodeEventArgs e)
        {
            tStaff.BestFitColumns();
        }

        private void dtDate_EditValueChanged(object sender, EventArgs e)
        {
            if (IsEquipmentClassInv)
            {
                BuildNewTree(DataRepository.GetNewStaff(dtDate.DateTime.Date));
                UpdateStaffTree();
            }
        }
        #endregion
    }
}