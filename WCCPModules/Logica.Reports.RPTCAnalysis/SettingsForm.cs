﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using Logica.Reports.BaseReportControl;
using Logica.Reports.DataAccess;
using DevExpress.XtraTreeList.Nodes;
using DevExpress.XtraEditors.Controls;
using System.Data.SqlClient;
using Logica.Reports.Common;
using DevExpress.XtraEditors;
using Logica.Reports.ConfigXmlParser.Model;
using WccpReporting.RPTCAnalysis.Localization;
using WccpReporting.RPTCAnalysis.DataProvider;
using WccpReporting.RPTCAnalysis.Constants;
using WccpReporting.RPTCAnalysis.UserControls.Contracts;
using Logica.Reports.Common.WaitWindow;
using DevExpress.XtraTreeList;

namespace WccpReporting.RPTCAnalysis
{
    public partial class SettingsForm : XtraForm
    {
        private class NodeInfo
        {
            public int ID { get; set; }
            public String NAME { get; set; }
        }

        private enum PlaceType
        {
            None = 0,
            Region,
            AllRegion,
            District,
            AllDistricts,
            City,
            AllCities
        }

        private enum StaffLevel
        {
            Unknown,
            M4,
            M3,
            M2
        }

        private enum SKUType
        {
            Brand = 0,
            Sort,
            CombiProduct,
            SimpleProduct
        }


        private class SKUInfo
        {
            public String Brand { get; set; }
            public String Sort { get; set; }
            public String CombiProduct { get; set; }
            public String SimpleProduct { get; set; }
            public String Stat { get; set; }
            public SKUInfo()
            {
                Brand = String.Empty;
                Sort = String.Empty;
                CombiProduct = String.Empty;
                SimpleProduct = String.Empty;
                Stat = String.Empty;
            }
        }

        private DataTable skuList = null;
        private Dictionary<object, object> channelTypeCollection = new Dictionary<object, object>();
        private ReportSettings initialReportSettings = new ReportSettings();

        public SettingsForm()
        {
            InitializeComponent();
            //dateStart1.Edi
            DataAccessProvider.OpenConnection();
            //reportSettings.Copy(ref initialReportSettings);
            //lbReportsHeader.Text = WccpReporting.RPTCAnalysis.Localization.Resource.Reports;
            BuildDates();
            BuildSKU();
            BuildTTInfo();
            cmbCategoryTT.SelectedIndex = 0;
            cmbCategoryTT.Refresh();
            BuildRegionTree();
            BuildChanelType();
            txtStep.Properties.ReadOnly = false;
        }

        void parent_CloseClick(object sender, DevExpress.XtraTab.XtraTabPage selectedPage)
        { 
        }

        #region Build Controls
        private void BuildDates()
        {
            lbDateFrom.Text = Resource.DateFrom;
            lbDateTo.Text = Resource.DateTo;
            // period 1
            dateStart1.DateTime = initialReportSettings.DateStartPeriod1;
            dateEnd1.DateTime = initialReportSettings.DateEndPeriod1;
            
            // period 2
            dateStart2.DateTime = initialReportSettings.DateStartPeriod2;
            dateEnd2.DateTime = initialReportSettings.DateEndPeriod2;
        }

        private void BuildCustomers()
        {
            initialReportSettings.M2 = initialReportSettings.M3 = initialReportSettings.M4 = String.Empty;
            GatherPersonalInfo();
            DataTable customersInfo = DataAccessProvider.GetDistributor(initialReportSettings.M2, initialReportSettings.M3, initialReportSettings.M4);
            cmbCustomers.Properties.Items.Clear();

            Dictionary<int, int> idInfo = new Dictionary<int, int>();
            AddDefaultValue(cmbCustomers, idInfo);

            if (null != customersInfo)
            {
                foreach (DataRow row in customersInfo.Rows)
                {
                    int curItem = cmbCustomers.Properties.Items.Add(String.Format("{0}: {1}", row[FieldNames.NAME], row[FieldNames.ADRESS]));
                    idInfo.Add(curItem, int.Parse(row[FieldNames.ID].ToString()));
                }
            }

            cmbCustomers.Properties.Tag = new object[] { idInfo, customersInfo };
            cmbCustomers.SelectedIndex = 0;
            cmbCustomers.Refresh();
        }


        private void BuildTTInfo()
        {
            DataTable TTInfo = DataAccessProvider.GetTypesAndSubtypesTT();
            cmbTypeTT.Properties.Items.Clear();
            cmbSubTypeTT.Properties.Items.Clear();

            Dictionary<int, int> idInfo = new Dictionary<int, int>();
            AddDefaultValue(cmbTypeTT, idInfo);

            if (null != TTInfo)
            {
                string filter = string.Format("{0}={1}", FieldNames.PARENT_ID, 0);
                DataRow[] selected = TTInfo.Select(filter);

                foreach (DataRow row in selected)
                {
                    int curItem = cmbTypeTT.Properties.Items.Add(row[FieldNames.NAME]);
                    idInfo.Add(curItem, int.Parse(row[FieldNames.ID].ToString()));
                }
            }

            cmbTypeTT.Properties.Tag = new object[] { idInfo, TTInfo };
            cmbTypeTT.SelectedIndex = 0;
            cmbTypeTT.Refresh();
        }

        private void AddDefaultValue(ComboBoxEdit cmb, Dictionary<int, int> idInfo)
        {
            int curItem = cmb.Properties.Items.Add(Resource.All);
            idInfo.Add(curItem, 0);
        }

        private void BuildSKUComboBox(ComboBoxEdit cmb)
        {
            cmb.Properties.Items.Clear();
            if (null != skuList)
            {
                Dictionary<int, int> idInfo = new Dictionary<int, int>();
                foreach (DataRow row in skuList.Rows)
                {
                    int curItem = cmb.Properties.Items.Add(row[FieldNames.NAME]);
                    idInfo.Add(curItem, int.Parse(row[FieldNames.ID].ToString()));
                }
                cmb.Properties.Tag = idInfo;
                if (0 != idInfo.Count)
                    cmb.SelectedIndex = 0;
                cmb.Refresh();
            }
        }

        private void BuildSKU()
        {
            skuList = DataAccessProvider.GetSKUsTreeInfo();
            BuildSKUTreeList(skuList, treeListSKU1, null, SKUType.Brand, 0);
            treeListSKU1.BestFitColumns();
            BuildSKUTreeList(skuList, treeListSKU2, null, SKUType.Brand, 0);
            treeListSKU2.BestFitColumns();
        }

        private void BuildChanelType()
        {
            lbChannelType.Text = Resource.ChannelType;

            DataTable chanelTypes = DataAccessProvider.GetChanelTypeList();
            foreach (DataRow dr in chanelTypes.Rows)
            {
                if (null == dr["ChanelType_id"] || String.IsNullOrEmpty(dr["ChanelType_id"].ToString()))
                    continue;
                channelTypeCollection.Add(dr["ChanelType"], dr["ChanelType_id"]);
                cbChannelType.Properties.Items.Add(dr["ChanelType"]);
            }
            cbChannelType.SelectedIndex = 0;
            cbChannelType.Refresh();
            initialReportSettings.ChanelType = cbChannelType.Text;
        }

        #region BuildTree
        private void BuildRegionTree()
        {
            treeRegion.ClearNodes();
            TreeListNode root = treeRegion.AppendNode(new object[] {"Вся страна"}, null);
            root.Tag = new List<object>() { -1, PlaceType.None};
            AddRegions(root);
            root.Expanded = true;
            treeRegion.BestFitColumns();
        }

        private void AddRegions(TreeListNode root)
        {
            DataTable regions = DataAccessProvider.GetRegions();
            if (null != regions)
            {
                foreach(DataRow row in regions.Rows)
                {
                    TreeListNode node = AddGEOChildNode(row, PlaceType.Region, root);
                    AddDistricts(int.Parse(row[FieldNames.ID].ToString()), node);
                }
            }
        }

        private void AddDistricts(int regionID, TreeListNode root)
        {
            DataTable regions = DataAccessProvider.GetDistricts(regionID);
            if (null != regions)
            {
                foreach (DataRow row in regions.Rows)
                {
                    TreeListNode node = AddGEOChildNode(row, PlaceType.District, root);
                    AddCities(int.Parse(row[FieldNames.ID].ToString()), node);
                }
            }
        }

        private void AddCities(int districtID, TreeListNode root)
        {
            TreeListNode nodeCities = treeRegion.AppendNode(new object[] { "Города" }, root);
            nodeCities.Tag = new List<object>() { 0, PlaceType.None };

            DataTable cities = DataAccessProvider.GetCities(districtID, "2");
            if (null != cities)
            {
                foreach (DataRow row in cities.Rows)
                {
                    TreeListNode node = AddGEOChildNode(row, PlaceType.City, nodeCities);
                }
            }

            TreeListNode nodeVilages = treeRegion.AppendNode(new object[] { "Села" }, root);
            nodeVilages.Tag = new List<object>() { 0, PlaceType.None };

            DataTable vilages = DataAccessProvider.GetCities(districtID, "1");
            if (null != vilages)
            {
                foreach (DataRow row in vilages.Rows)
                {
                    TreeListNode node = AddGEOChildNode(row, PlaceType.City, nodeVilages);
                }
            }
        }

        private TreeListNode AddGEOChildNode(DataRow row, PlaceType placeType, TreeListNode root)
        {
            TreeListNode node = treeRegion.AppendNode(new object[] { row[FieldNames.NAME] }, root);
            node.Tag = new List<object>() { row[FieldNames.ID], placeType };
            return node;
        }

        private void BuildTree1(object id)
        {
            treeRegion.ClearNodes();
            DataSet ds1;
            DataAccessLayer.OpenConnection();
            if(null != id)
                ds1 = DataAccessLayer.ExecuteStoredProcedure("spDW_StaffTree", new SqlParameter[] { new SqlParameter("@ChanelType_ID", id) });
            else
                ds1 = DataAccessLayer.ExecuteStoredProcedure("spDW_StaffTree", new SqlParameter[] { });
            DataAccessLayer.CloseConnection();
            AddChildNodes(null, ds1.Tables[0]);
        }

        private void AddChildNodes(TreeListNode parent, DataTable table)
        {
            foreach (DataRow dr in table.Rows)
            {
                if ((null == parent && dr["Parent_ID"].Equals(0)) || (null != parent && dr["Parent_ID"].Equals(((IList<object>)parent.Tag)[0])))
                {
                    TreeListNode node = treeRegion.AppendNode(new object[] { dr["Name"] }, parent);
                    node.Tag = new List<object>() { dr["Item_ID"], dr["StaffLevel_ID"], dr["ID"] };
                    AddChildNodes(node, table);
                }
            }
        }

        private void cbChannelType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(-1 != cbChannelType.SelectedIndex)
              BuildStaff();
        }

        private void BuildStaff()
        {
            WaitManager.StartWait();
            GatherSettings();
            treeStaffList.Nodes.Clear();
            treeStaffList.Tag = null;

            int chanelType = int.Parse(channelTypeCollection[cbChannelType.Properties.Items[cbChannelType.SelectedIndex]].ToString());

            DataTable stafInfo = DataAccessProvider.GetPersonal(initialReportSettings.Regions, initialReportSettings.Districs, initialReportSettings.Cities, chanelType);
            BuildStaffTree(stafInfo, StaffLevel.M4, null, 0);
            treeStaffList.BestFitColumns();

            BuildCustomers();
            WaitManager.StopWait();
        }

        private SKUType GetNextSKUType(SKUType skuType)
        {
            SKUType result = SKUType.Brand;
            switch(skuType)
            {
                case SKUType.Brand:
                    result = SKUType.Sort;
                    break;
                case SKUType.Sort:
                    result = SKUType.CombiProduct;
                    break;
                case SKUType.CombiProduct:
                    result = SKUType.SimpleProduct;
                    break;
            }
            return result;
        }

        private StaffLevel GetNextStaffLevel(StaffLevel level)
        {
            StaffLevel res = StaffLevel.Unknown;
            switch (level)
            {
                case StaffLevel.M4:
                    res = StaffLevel.M3;
                    break;
                case StaffLevel.M3:
                    res = StaffLevel.M2;
                    break;
            }
            return res;
        }

        private List<NodeInfo> GetStaffByLevel(DataTable staffInfo, StaffLevel level, int parentID)
        {
            List<NodeInfo> res = null;
            switch (level)
            {
                case StaffLevel.M4:
                    {
                        res = (from r in staffInfo.AsEnumerable()
                               where r.Field<int?>(FieldNames.M4_ID).HasValue && r.Field<string>(FieldNames.M4_NAME) != null
                               select new NodeInfo
                               {
                                   ID = r.Field<int>(FieldNames.M4_ID),
                                   NAME = r.Field<string>(FieldNames.M4_NAME)
                               }).ToList();
                        break;
                    }
                case StaffLevel.M3:
                    {
                        res = (from r in staffInfo.AsEnumerable()
                               where r.Field<int?>(FieldNames.M3_ID).HasValue && r.Field<string>(FieldNames.M3_NAME) != null && r.Field<int>(FieldNames.M4_ID) == parentID
                               select new NodeInfo
                               {
                                   ID = r.Field<int>(FieldNames.M3_ID),
                                   NAME = r.Field<string>(FieldNames.M3_NAME)
                               }).ToList();
                        break;
                    }
                case StaffLevel.M2:
                    {
                        res = (from r in staffInfo.AsEnumerable()
                               where r.Field<int?>(FieldNames.M2_ID).HasValue && r.Field<string>(FieldNames.M2_NAME) != null && r.Field<int>(FieldNames.M3_ID) == parentID
                               select new NodeInfo
                               {
                                   ID = r.Field<int>(FieldNames.M2_ID),
                                   NAME = r.Field<string>(FieldNames.M2_NAME)
                               }).ToList();
                        break;
                    }
            }
            return res;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="source"></param>
        /// <param name="level">int = (int)SKUType</param>
        /// <returns></returns>
        private List<NodeInfo> GetSKUByLevel(DataTable source, int level, int parentID)
        {
            List<NodeInfo> res = null;

            res = (from r in source.AsEnumerable()
                   where r.Field<int>(FieldNames.PARENT_ID) == parentID && r.Field<int>(FieldNames.LEVEL) == level
                   select new NodeInfo
                   {
                       ID = r.Field<int>(FieldNames.ID),
                       NAME = r.Field<string>(FieldNames.NAME)
                   }).ToList();

            return res;
        }
        private void BuildStaffTree(DataTable staffInfo, StaffLevel level, TreeListNode parentNode, int parentID)
        {
            List<NodeInfo> staff = GetStaffByLevel(staffInfo, level, parentID);
            if (null != staff)
            {
                Dictionary<int, int> tmp = new Dictionary<int, int>();
                for (int i = 0; i < staff.Count; i++)
                {
                    if (!tmp.Keys.Contains(staff[i].ID))
                    {
                        TreeListNode node = treeStaffList.AppendNode(new object[] { staff[i].NAME }, parentNode);
                        node.Tag = new List<object>() { staff[i].ID, level };
                        BuildStaffTree(staffInfo, GetNextStaffLevel(level), node, staff[i].ID);
                        tmp.Add(staff[i].ID, staff[i].ID);
                    }
                }
            }
        }

        private void BuildSKUTreeList(DataTable source, TreeList tree, TreeListNode parentNode, SKUType skuType, int parentID)
        {
            //PopupContainerEdit
            int level = (int)skuType;
            List<NodeInfo> skuData = GetSKUByLevel(source, level, parentID);
            if (null != skuData)
            {
                for (int i = 0; i < skuData.Count; i++)
                {
                    TreeListNode node = tree.AppendNode(new object[] { skuData[i].NAME }, parentNode);
                    node.Tag = new List<object>() { skuData[i].ID, level };
                    BuildSKUTreeList(source, tree, node, GetNextSKUType(skuType), skuData[i].ID);
                }
            }
        }

        #endregion
        
        private void Tree_AfterCheckNode(object sender, DevExpress.XtraTreeList.NodeEventArgs e)
        {
            WaitManager.StartWait();
            if (e.Node.CheckState == CheckState.Checked)
            {
                //e.Node.Selected
                CheckChildNodes(e.Node, true);
            }
            else if (e.Node.CheckState == CheckState.Unchecked)
            {
                CheckChildNodes(e.Node, false);
                UncheckParentNodes(e.Node);
            }

            if (sender == treeRegion)
                BuildStaff();
            else if (sender == treeStaffList)
                BuildCustomers();

            WaitManager.StopWait();
        }

        private void CheckChildNodes(TreeListNode treeListNode, bool checkedState)
        {
            if (null != treeListNode && treeListNode.HasChildren)
            {
                foreach (TreeListNode childNode in treeListNode.Nodes)
                {
                    childNode.Checked = checkedState;
                    CheckChildNodes(childNode, checkedState);
                }
            }
        }
        private void UncheckParentNodes(TreeListNode treeListNode)
        {
            if (null != treeListNode && null != treeListNode.ParentNode)
            {
                treeListNode.ParentNode.Checked = false;
                UncheckParentNodes(treeListNode.ParentNode);
            }
        }
        
        #endregion

        public bool ValidateData()
        {
            GatherSKUsData();
            bool isValid = false;
            DateTime tmpDateTime = DateTime.Now;
            if ((!DateTime.TryParse(dateStart1.Text, out tmpDateTime)) || (!DateTime.TryParse(dateEnd1.Text, out tmpDateTime)) ||
                (!DateTime.TryParse(dateStart2.Text, out tmpDateTime)) || (!DateTime.TryParse(dateEnd2.Text, out tmpDateTime)))
            {
                MessageBox.Show(Resource.InvalidDate, Resource.Error, MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else if ((dateStart1.DateTime > dateEnd1.DateTime) || (dateStart2.DateTime > dateEnd2.DateTime))
            {
                MessageBox.Show(Resource.InvalidDate, Resource.Error, MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else if (initialReportSettings.SKU1String.Length == 0)
            {
                MessageBox.Show(Resource.InvalidProductsNeedSelectSKU1, Resource.Error, MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else if (initialReportSettings.SKU2String.Length == 0)
            {
                MessageBox.Show(Resource.InvalidProductsNeedSelectSKU2, Resource.Error, MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else if (initialReportSettings.SKU1String.Equals(initialReportSettings.SKU2String))
            {
                MessageBox.Show(Resource.InvalidProducts, Resource.Error, MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
                isValid = true;
            return isValid;
        }

        private void btnYes_Click(object sender, EventArgs e)
        {
            if (ValidateData())
            {
                GatherSettings();
                DialogResult = DialogResult.OK;
            }
        }

        private void GatherSettings()
        {
            initialReportSettings.Reset();

            // Step
            initialReportSettings.Step = txtStep.Value;
            
            // period 1
            initialReportSettings.DateStartPeriod1 = dateStart1.DateTime;
            initialReportSettings.DateEndPeriod1 = dateEnd1.DateTime;

            // period 2
            initialReportSettings.DateStartPeriod2 = dateStart2.DateTime;
            initialReportSettings.DateEndPeriod2 = dateEnd2.DateTime;

            // SKU1 & SKU2
            GatherSKUsData();

            // TT Info
            if (-1 != cmbTypeTT.SelectedIndex)
            {
                if (0 == cmbTypeTT.SelectedIndex)
                    initialReportSettings.TypeTT = String.Empty;
                else
                {
                    Dictionary<int, int> idInfo = ((Dictionary<int, int>)((object[])(cmbTypeTT.Properties.Tag))[0]);
                    initialReportSettings.TypeTT = idInfo[cmbTypeTT.SelectedIndex].ToString();
                }
                initialReportSettings.TypeTTString = cmbTypeTT.Text;
            }

            if (-1 != cmbSubTypeTT.SelectedIndex)
            {
                if (0 == cmbSubTypeTT.SelectedIndex)
                    initialReportSettings.SubTypeTT = String.Empty;
                else
                {
                    Dictionary<int, int> idInfo = ((Dictionary<int, int>)cmbSubTypeTT.Properties.Tag);
                    initialReportSettings.SubTypeTT = idInfo[cmbSubTypeTT.SelectedIndex].ToString();
                }
                initialReportSettings.SubTypeTTString = cmbSubTypeTT.Text;
            }

            if (-1 != cmbCategoryTT.SelectedIndex)
            {
                if (0 == cmbCategoryTT.SelectedIndex)
                    initialReportSettings.CategoryTT = String.Empty;
                else
                    initialReportSettings.CategoryTT = cmbCategoryTT.Properties.Items[cmbCategoryTT.SelectedIndex].ToString();
                initialReportSettings.CategoryTTString = cmbCategoryTT.Text;
            }

            if (-1 != cmbCustomers.SelectedIndex)
            {
                if (0 == cmbCustomers.SelectedIndex)
                    initialReportSettings.Client = String.Empty;
                else
                {
                    Dictionary<int, int> idInfo = (Dictionary<int, int>)((object[])cmbCustomers.Properties.Tag)[0];
                    initialReportSettings.Client = idInfo[cmbCustomers.SelectedIndex].ToString();
                }
                initialReportSettings.ClientString = cmbCustomers.Text;
            }

            initialReportSettings.ChanelType = cbChannelType.Text;

            GatherRegionsInfo(treeRegion.Nodes);
            GatherGeographicalTypeInfo(treeRegion.Nodes);
            GatherPersonalInfo();
        }

        private void GatherSKUsData()
        {
            SKUInfo skuInfo = new SKUInfo();
            GatherSKUData(treeListSKU1.Nodes, ref skuInfo);
            initialReportSettings.SKU1_Brand = skuInfo.Brand;
            initialReportSettings.SKU1_Sort = skuInfo.Sort;
            initialReportSettings.SKU1_CombiProduct = skuInfo.CombiProduct;
            initialReportSettings.SKU1_SimpleProduct = skuInfo.SimpleProduct;
            initialReportSettings.SKU1String = skuInfo.Stat;

            skuInfo = new SKUInfo();
            GatherSKUData(treeListSKU2.Nodes, ref skuInfo);
            initialReportSettings.SKU2_Brand = skuInfo.Brand;
            initialReportSettings.SKU2_Sort = skuInfo.Sort;
            initialReportSettings.SKU2_CombiProduct = skuInfo.CombiProduct;
            initialReportSettings.SKU2_SimpleProduct = skuInfo.SimpleProduct;
            initialReportSettings.SKU2String = skuInfo.Stat;
        }

        private void GatherSKUData(TreeListNodes treeListNodes, ref SKUInfo skuInfo)
        {
            if (null != treeListNodes)
            {
                foreach (TreeListNode node in treeListNodes)
                {
                    if (node.Checked)
                    {
                        List<object> nodeInfo = ((List<object>)node.Tag);
                        switch ((SKUType)nodeInfo[1])
                        {
                            case SKUType.Brand:
                                if (!String.IsNullOrEmpty(skuInfo.Brand))
                                    skuInfo.Brand += ",";
                                skuInfo.Brand += nodeInfo[0].ToString();
                                break;
                            case SKUType.Sort:
                                if (!String.IsNullOrEmpty(skuInfo.Sort))
                                    skuInfo.Sort += ",";
                                skuInfo.Sort += nodeInfo[0].ToString();
                                break;
                            case SKUType.CombiProduct:
                                if (!String.IsNullOrEmpty(skuInfo.CombiProduct))
                                    skuInfo.CombiProduct += ",";
                                skuInfo.CombiProduct += nodeInfo[0].ToString();
                                break;
                            case SKUType.SimpleProduct:
                                if (!String.IsNullOrEmpty(skuInfo.SimpleProduct))
                                    skuInfo.SimpleProduct += ",";
                                skuInfo.SimpleProduct += nodeInfo[0].ToString();
                                break;
                        }
                        if (!String.IsNullOrEmpty(skuInfo.Stat))
                            skuInfo.Stat += ", ";
                        skuInfo.Stat += node.GetDisplayText(0);
                    }
                    else
                        GatherSKUData(node.Nodes, ref skuInfo);
                }
            }
        }

        private void GatherPersonalInfo()
        {
            foreach (TreeListNode node in treeStaffList.Nodes)
            {
                if (node.Checked)
                {
                    List<object> nodeInfo = ((List<object>)node.Tag);
                    if (!String.IsNullOrEmpty(initialReportSettings.M4))
                        initialReportSettings.M4 += ",";
                    if (!String.IsNullOrEmpty(initialReportSettings.M4String))
                        initialReportSettings.M4String += ", ";
                    initialReportSettings.M4 += nodeInfo[0].ToString();
                    initialReportSettings.M4String += node.GetDisplayText(0);
                }
                else
                    GatherPersonalInfo(node.Nodes);
            }
        }

        private void GatherPersonalInfo(TreeListNodes nodes)
        {
            if (null == nodes)
                return;

            foreach (TreeListNode node in nodes)
            {
                if (node.Checked)
                {
                    List<object> nodeInfo = ((List<object>)node.Tag);
                    switch (((StaffLevel)nodeInfo[1]))
                    {
                        case StaffLevel.M3:
                            if (!String.IsNullOrEmpty(initialReportSettings.M3))
                            {
                                initialReportSettings.M3 += ",";
                                initialReportSettings.M3String += ", ";
                            }
                            initialReportSettings.M3 += nodeInfo[0].ToString();
                            initialReportSettings.M3String += node.GetDisplayText(0);
                            break;
                        case StaffLevel.M2:
                            if (!String.IsNullOrEmpty(initialReportSettings.M2))
                            {
                                initialReportSettings.M2 += ",";
                                initialReportSettings.M2String += ", ";
                            }
                            initialReportSettings.M2 += nodeInfo[0].ToString();
                            initialReportSettings.M2String += node.GetDisplayText(0);
                            break;
                    }
                }
                else
                    GatherPersonalInfo(node.Nodes);
            }
        }

        private void GatherRegionsInfo(TreeListNodes nodes)
        {
            if (null == nodes)
                return;

            foreach (TreeListNode node in nodes)
            {
                bool needProcess = true;
                if (node.Checked)
                {
                    List<object> nodeInfo = ((List<object>)node.Tag);
                    switch (((PlaceType)nodeInfo[1]))
                    {
                        case PlaceType.Region:
                            if (!String.IsNullOrEmpty(initialReportSettings.Regions))
                            {
                                initialReportSettings.Regions += ",";
                                initialReportSettings.RegionsString += ", ";
                            }
                            initialReportSettings.Regions += nodeInfo[0].ToString();
                            initialReportSettings.RegionsString += node.GetDisplayText(0);
                            needProcess = false;
                            break;
                        case PlaceType.District:
                            if (!String.IsNullOrEmpty(initialReportSettings.Districs))
                            {
                                initialReportSettings.Districs += ",";
                                initialReportSettings.DistricsString += ", ";
                            }
                            initialReportSettings.Districs += nodeInfo[0].ToString();
                            initialReportSettings.DistricsString += node.GetDisplayText(0);
                            needProcess = false;
                            break;
                        case PlaceType.City:
                            if (!String.IsNullOrEmpty(initialReportSettings.Cities))
                            {
                                initialReportSettings.Cities += ",";
                                initialReportSettings.CitiesString += ", ";
                            }
                            initialReportSettings.Cities += nodeInfo[0].ToString();
                            initialReportSettings.CitiesString += node.GetDisplayText(0);
                            needProcess = false;
                            break;
                    }
                }
                if (needProcess)
                    GatherRegionsInfo(node.Nodes);
            }
        }

        private void GatherGeographicalTypeInfo(TreeListNodes nodes)
        {
            this.initialReportSettings.GeographicalType = ReportSettings.GeographicalUnitType.Region;

            if (null == nodes)
                return;

            foreach (TreeListNode country in nodes)
            {
                foreach (TreeListNode region in country.Nodes)
                {
                    if (SetGeographicalType(region))
                    {
                        return;
                    }
                }
            }
        }

        private bool SetGeographicalType(TreeListNode parentNode)
        {
            bool checkedExists = false, uncheckedExists = false;

            foreach (TreeListNode node in parentNode.Nodes)
            {
                if (node.Checked)
                {
                    checkedExists = true;
                }
                else
                {
                    uncheckedExists = true;
                }

                PlaceType placeType =(PlaceType)((List<object>)node.Tag)[1];
                if (placeType == PlaceType.District || placeType == PlaceType.None)
                {
                    if (SetGeographicalType(node))
                    {
                        return true;
                    }
                }
                if (placeType == PlaceType.City || placeType == PlaceType.None)
                {
                    if (checkedExists && uncheckedExists)
                    {
                        this.initialReportSettings.GeographicalType = ReportSettings.GeographicalUnitType.City;
                        return true;
                    }
                }
            }

            if (checkedExists && uncheckedExists)
            {
                this.initialReportSettings.GeographicalType = ReportSettings.GeographicalUnitType.District;
                return false;
            }

            return false;
        }

        internal void GetReportSettings(ref ReportSettings reportSettings)
        {
            initialReportSettings.Copy(ref reportSettings);
        }

        private void SettingsForm_Load(object sender, EventArgs e)
        {

        }

        private void comboBoxEdit1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void comboBoxEdit2_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void cmbTypeTT_SelectedIndexChanged(object sender, EventArgs e)
        {
            cmbSubTypeTT.Properties.Items.Clear();
            Dictionary<int, int> idInfo = new Dictionary<int, int>();
            AddDefaultValue(cmbSubTypeTT, idInfo);
            if (-1 != cmbTypeTT.SelectedIndex)
            {
                object[] info = ((object[])(cmbTypeTT.Properties.Tag));
                Dictionary<int, int> typeIDInfo = (Dictionary<int, int>)info[0];
                int typeID = typeIDInfo[cmbTypeTT.SelectedIndex];

                DataTable TTInfo = ((DataTable)info[1]);
                string filter = string.Format("{0}={1}", FieldNames.PARENT_ID, typeID);
                DataRow[] selected = TTInfo.Select(filter);
                
                foreach (DataRow row in selected)
                {
                    int curItem = cmbSubTypeTT.Properties.Items.Add(row[FieldNames.NAME]);
                    idInfo.Add(curItem, int.Parse(row[FieldNames.ID].ToString()));
                }

            }
            cmbSubTypeTT.Properties.Tag = idInfo;
            cmbSubTypeTT.SelectedIndex = 0;
            cmbSubTypeTT.Refresh();
        }

        private void Tree_NormalizeWidth(object sender, NodeEventArgs e)
        {
            TreeList tree = sender as TreeList;
            if (null != tree)
            {
                tree.BestFitColumns();
            }
        }
    }

    internal class ComboBoxItem
    {
        int id;
        public int Id
        {
            get { return this.id; }
            set { this.id = value; }
        }
        string text;
        public string Text
        {
            get { return text; }
            set { text = value; }
        }
        public override string ToString()
        {
            return Text;
        }
    }
}
