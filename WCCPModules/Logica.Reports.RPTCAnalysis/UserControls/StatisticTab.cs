﻿//#define DESIGN_MODE
using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using WccpReporting.RPTCAnalysis.Localization;
using WccpReporting.RPTCAnalysis.Constants;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraPrintingLinks;
using WccpReporting.RPTCAnalysis.UserControls.Contracts;
using Logica.Reports.BaseReportControl;
using DevExpress.XtraPrinting;

namespace WccpReporting.RPTCAnalysis.UserControls
{
    public partial class StatisticTab 
#if DESIGN_MODE
        : UserControl
#else
        : BaseTabReport, IReportTab
#endif
    {
        private static string DATE_FORMAT = @"dd-MM-yyyy";
        private static string[] lstPercentInfo = new string[] { @"%# POCs", @"%Vol SKU1 @ ST1", @"%Vol SKU1 @ ST2", @"%Change" };
        public StatisticTab()
            : base()
        {
            InitializeComponent();
            TabName = Resource.Summary;
            InitBottomStaticData();
            base.SourceInitializedEvent += new Delegates.DataSourceChanged(StatisticTab_SourceInitializedEvent);
        }

#if DESIGN_MODE
        internal override CompositeLink ExportTo(ExportToType exportType, CompositeLink compositeLink)
        { return compositeLink; }
#else
        internal override CompositeLink ExportTo(ExportToType exportType, CompositeLink compositeLink)
        {
            try
            {
                switch (exportType)
                {
                    case ExportToType.Csv:
                    case ExportToType.Bmp:
                        //MessageBox.Show(Resource.ExportFormatNotSupported, Resource.Information);
                        break;
                        /*
                    case ExportToType.Html:
                        PrepareCompositeLink().PrintingSystem.ExportToHtml(fileName);
                        break;
                    case ExportToType.Mht:
                        PrepareCompositeLink().PrintingSystem.ExportToMht(fileName);
                        break;
                    case ExportToType.Pdf:
                        PrepareCompositeLink().PrintingSystem.ExportToPdf(fileName);
                        break;
                    case ExportToType.Rtf:
                        PrepareCompositeLink().PrintingSystem.ExportToRtf(fileName);
                        break;
                    case ExportToType.Txt:
                        PrepareCompositeLink().PrintingSystem.ExportToText(fileName);
                        break;
                    case ExportToType.Xls:
                        PrepareCompositeLink().PrintingSystem.ExportToXls(fileName);
                        break;
                         */
                    default:
                        compositeLink = PrepareCompositeLink(compositeLink);
                        break;
                }
            }
            catch (Exception ex)
            {
                //BaseTabReport.HandleExportException(ex, fileName);
            }
            return compositeLink;
        }
#endif     
        private CompositeLink PrepareCompositeLink(CompositeLink compositeLink)
        {
            compositeLink.Links.Add(new RichTextBoxLink() { RichTextBox = new RichTextBox() { Text = GetTabText() } });
            compositeLink.Links.Add(new PrintableComponentLink() { Component = gridParamsInfo });
            compositeLink.Links.Add(new RichTextBoxLink() { RichTextBox = new RichTextBox() { Text = " " } });
            compositeLink.Links.Add(new PrintableComponentLink() { Component = gridControlTopLeft });
            compositeLink.Links.Add(new RichTextBoxLink() { RichTextBox = new RichTextBox() { Text = " " } });
            compositeLink.Links.Add(new PrintableComponentLink() { Component = gridControlData });
            compositeLink.Links.Add(new RichTextBoxLink() { RichTextBox = new RichTextBox() { Text = " " } });
            compositeLink.Links.Add(new PrintableComponentLink() { Component = gridBotton1 });
            compositeLink.Links.Add(new RichTextBoxLink() { RichTextBox = new RichTextBox() { Text = " " } });
            compositeLink.Links.Add(new PrintableComponentLink() { Component = gridBotton2 });
            compositeLink.Links.Add(new RichTextBoxLink() { RichTextBox = new RichTextBox() { Text = " " } });
            compositeLink.Links.Add(new PrintableComponentLink() { Component = gridBotton3 });
            compositeLink.Links.Add(new RichTextBoxLink() { RichTextBox = new RichTextBox() { Text = " " } });
            compositeLink.Links.Add(new PrintableComponentLink() { Component = gridControlTop });
            compositeLink.Links.Add(new RichTextBoxLink() { RichTextBox = new RichTextBox() { Text = " " } });
            compositeLink.CreateDocument();
            return compositeLink;
        }

        void StatisticTab_SourceInitializedEvent(DataTable newSource)
        {
            DataTable statData = null;
            EnsureViewDataStructure(ref statData, (DevExpress.XtraGrid.Views.Grid.GridView)bandedGridTopView);

            // initalize main statistic
            if (null != newSource)
            {
                string guField = this.reportSettings.GeographicalType == ReportSettings.GeographicalUnitType.Region ? FieldNames.REGION_NAME : 
                    this.reportSettings.GeographicalType == ReportSettings.GeographicalUnitType.District? FieldNames.DISTRICT_NAME :
                    FieldNames.CITY_NAME;

                string[] geographicalUnits = (from row in newSource.AsEnumerable()
                                              select row.Field<string>(guField)
                                            ).Distinct().ToArray();

                foreach (string geographicalUnit in geographicalUnits)
                {
                    var geographicalUnitInfo = newSource.AsEnumerable().Where(r => r.Field<string>(guField).Equals(geographicalUnit));
                    // SKU1 section
                    decimal sku1_ptc1 = 0M;
                    var tmp = geographicalUnitInfo.AsEnumerable().Where(r => r.Field<decimal?>(FieldNames.PTC1SKU1).HasValue);
                    if (tmp.Count() > 0)
                        sku1_ptc1 = tmp.Average(r => r.Field<decimal>(FieldNames.PTC1SKU1));
                    decimal sku1_ptc2 = 0M;
                    tmp = geographicalUnitInfo.AsEnumerable().Where(r => r.Field<decimal?>(FieldNames.PTC2SKU1).HasValue);
                    if (tmp.Count() > 0)
                        sku1_ptc2 = tmp.Average(r => r.Field<decimal>(FieldNames.PTC2SKU1));
                    decimal changeSKU1 = sku1_ptc2 - sku1_ptc1;
                    decimal changeSKU1Percent = (sku1_ptc1 == 0) ? 0M : ((sku1_ptc2 / sku1_ptc1 - 1));

                    // SKU2 section
                    decimal sku2_ptc1 = 0M;
                    tmp = geographicalUnitInfo.AsEnumerable().Where(r => r.Field<decimal?>(FieldNames.PTC1SKU2).HasValue);
                    if (tmp.Count() > 0)
                        sku2_ptc1 = tmp.Average(r => r.Field<decimal>(FieldNames.PTC1SKU2));
                    decimal sku2_ptc2 = 0M;
                    tmp = geographicalUnitInfo.AsEnumerable().Where(r => r.Field<decimal?>(FieldNames.PTC2SKU2).HasValue);
                    if (tmp.Count() > 0)
                        sku2_ptc2 = tmp.Average(r => r.Field<decimal>(FieldNames.PTC2SKU2));
                    decimal changeSKU2 = sku2_ptc2 - sku2_ptc1;
                    decimal changeSKU2Percent = (sku2_ptc1 == 0) ? 0M : (sku2_ptc2 / sku2_ptc1 - 1);

                    // RPTC section
                    decimal st1 = (sku2_ptc1 == 0) ? 0 : sku1_ptc1 / sku2_ptc1 - 1;
                    decimal st2 = (sku2_ptc2 == 0) ? 0 : sku1_ptc2 / sku2_ptc2 - 1;
                    decimal changeST = st2 - st1;
                    decimal changeSTPercent = (st1 == 0) ? 0 : st2 / st1 - 1;//(sku2_ptc1 == 0 ? 0 : (sku1_ptc1 / sku2_ptc1)) - (sku2_ptc2 == 0 ? 0 : (sku1_ptc2 / sku2_ptc2));

                    DataRow row = statData.NewRow();
                    row[FieldNames.REGION_NAME] = geographicalUnit;

                    // SKU1 section
                    row[FieldNames.SKU1PTC1] = sku1_ptc1;
                    row[FieldNames.SKU1PTC2] = sku1_ptc2;
                    row[FieldNames.SKU1CHANGE] = changeSKU1;
                    row[FieldNames.SKU1CHANGEPERCENT] = changeSKU1Percent;

                    // SKU2 section
                    row[FieldNames.SKU2PTC1] = sku2_ptc1;
                    row[FieldNames.SKU2PTC2] = sku2_ptc2;
                    row[FieldNames.SKU2CHANGE] = changeSKU2;
                    row[FieldNames.SKU2CHANGEPERCENT] = changeSKU2Percent;

                    // RPTC section
                    row[FieldNames.RPTCST1] = st1;
                    row[FieldNames.RPTCST2] = st2;
                    row[FieldNames.RPTCCHANGE] = changeSKU2;
                    row[FieldNames.RPTCCHANGEPERCENT] = changeSTPercent;

                    statData.Rows.Add(row);
                }
            }
            gridControlTop.DataSource = statData;

            SetParametersInfo();
        }

        private void SetParametersInfo()
        {
            DataTable viewData = new DataTable();
            // Create dataset structure
            DevExpress.XtraGrid.Views.Grid.GridView gridView = (DevExpress.XtraGrid.Views.Grid.GridView)bandedGridViewParamsInfo;
            foreach (GridColumn column in gridView.Columns)
            {
                viewData.Columns.Add(column.FieldName, typeof(string));
            }

            int index = 0;
            DataRow row = viewData.NewRow();
            row[index++] = reportSettings.DateStartPeriod1.ToString(DATE_FORMAT);
            row[index++] = reportSettings.DateEndPeriod1.ToString(DATE_FORMAT);
            row[index++] = reportSettings.DateStartPeriod2.ToString(DATE_FORMAT);
            row[index++] = reportSettings.DateEndPeriod2.ToString(DATE_FORMAT);

            row[index++] = Math.Round(reportSettings.Step, 2).ToString("0.00");

            row[index++] = reportSettings.ChanelType;
            row[index++] = (String.IsNullOrEmpty(reportSettings.M4String) ? "" : ("M4: " + reportSettings.M4String + "\n")) + (String.IsNullOrEmpty(reportSettings.M3String) ? "" : ("M3: " + reportSettings.M3String + "\n")) + (String.IsNullOrEmpty(reportSettings.M2String) ? "" : ("M2: " + reportSettings.M2String));

            row[index++] = reportSettings.ClientString;

            row[index++] = reportSettings.SKU1String;
            row[index++] = reportSettings.SKU2String;

            row[index++] = reportSettings.TypeTTString;
            row[index++] = reportSettings.SubTypeTTString;
            row[index++] = reportSettings.CategoryTTString;

            viewData.Rows.Add(row);
            gridParamsInfo.DataSource = viewData;
            //bandedGridViewParamsInfo.BestFitColumns();
        }

        public void SetShortSummary(DataTable shortSummary)
        {
            gridControlTopLeft.DataSource = shortSummary;
        }

        public DataTable V_Table2
        {
            set
            {
                CalcStatV_Table2(value);
            }
        }

        public DataTable V_Table3
        {
            set
            {
                CalcStatV_Table3(value);
            }
        }

        private void CalcStatV_Table2(DataTable v_table2)
        {
            BuildLeftStat(v_table2);
            BuildMidStat(v_table2);
        }

        private void CalcStatV_Table3(DataTable v_table3)
        {
            BuildRightStat(v_table3);
        }

        private void BuildRightStat(DataTable v_table3)
        {
            DataTable dtData = null;
            EnsureViewDataStructure(ref dtData, (DevExpress.XtraGrid.Views.Grid.GridView)bandedGridBottonView3);

            if (null != v_table3)
            {
                decimal lessZero = v_table3.AsEnumerable().Where(r => r.Field<decimal?>(FieldNames.D21).HasValue && r.Field<decimal?>(FieldNames.D21Percent).HasValue && r.Field<decimal?>(FieldNames.D21) < 0M).Sum(r => r.Field<decimal>(FieldNames.D21Percent));
                decimal equalZero = v_table3.AsEnumerable().Where(r => r.Field<decimal?>(FieldNames.D21).HasValue && r.Field<decimal?>(FieldNames.D21Percent).HasValue && r.Field<decimal?>(FieldNames.D21) == 0M).Sum(r => r.Field<decimal>(FieldNames.D21Percent));
                decimal greaterZero = v_table3.AsEnumerable().Where(r => r.Field<decimal?>(FieldNames.D21).HasValue && r.Field<decimal?>(FieldNames.D21Percent).HasValue && r.Field<decimal?>(FieldNames.D21) > 0M).Sum(r => r.Field<decimal>(FieldNames.D21Percent));

                DataRow row = dtData.NewRow();
                row[FieldNames.LESS_ZERO] = lessZero;
                row[FieldNames.EQUAL_ZERO] = equalZero;
                row[FieldNames.GREATER_ZERO] = greaterZero;
                dtData.Rows.Add(row);

                decimal lessZero1 = v_table3.AsEnumerable().Where(r => r.Field<decimal?>(FieldNames.D21).HasValue && r.Field<decimal?>(FieldNames.V1SKU1Percent).HasValue && r.Field<decimal?>(FieldNames.D21) < 0M).Sum(r => r.Field<decimal>(FieldNames.V1SKU1Percent));
                decimal equalZero1 = v_table3.AsEnumerable().Where(r => r.Field<decimal?>(FieldNames.D21).HasValue && r.Field<decimal?>(FieldNames.V1SKU1Percent).HasValue && r.Field<decimal?>(FieldNames.D21) == 0M).Sum(r => r.Field<decimal>(FieldNames.V1SKU1Percent));
                decimal greaterZero1 = v_table3.AsEnumerable().Where(r => r.Field<decimal?>(FieldNames.D21).HasValue && r.Field<decimal?>(FieldNames.V1SKU1Percent).HasValue && r.Field<decimal?>(FieldNames.D21) > 0M).Sum(r => r.Field<decimal>(FieldNames.V1SKU1Percent));

                row = dtData.NewRow();
                row[FieldNames.LESS_ZERO] = lessZero1;
                row[FieldNames.EQUAL_ZERO] = equalZero1;
                row[FieldNames.GREATER_ZERO] = greaterZero1;
                dtData.Rows.Add(row);


                decimal lessZero2 = v_table3.AsEnumerable().Where(r => r.Field<decimal?>(FieldNames.D21).HasValue && r.Field<decimal?>(FieldNames.V2SKU1Percent).HasValue && r.Field<decimal?>(FieldNames.D21) < 0M).Sum(r => r.Field<decimal>(FieldNames.V2SKU1Percent));
                decimal equalZero2 = v_table3.AsEnumerable().Where(r => r.Field<decimal?>(FieldNames.D21).HasValue && r.Field<decimal?>(FieldNames.V2SKU1Percent).HasValue && r.Field<decimal?>(FieldNames.D21) == 0M).Sum(r => r.Field<decimal>(FieldNames.V2SKU1Percent));
                decimal greaterZero2 = v_table3.AsEnumerable().Where(r => r.Field<decimal?>(FieldNames.D21).HasValue && r.Field<decimal?>(FieldNames.V2SKU1Percent).HasValue && r.Field<decimal?>(FieldNames.D21) > 0M).Sum(r => r.Field<decimal>(FieldNames.V2SKU1Percent));

                row = dtData.NewRow();
                row[FieldNames.LESS_ZERO] = lessZero2;
                row[FieldNames.EQUAL_ZERO] = equalZero2;
                row[FieldNames.GREATER_ZERO] = greaterZero2;
                dtData.Rows.Add(row);

                row = dtData.NewRow();
                row[FieldNames.LESS_ZERO] = lessZero2 - lessZero1;
                row[FieldNames.EQUAL_ZERO] = equalZero2 - equalZero1;
                row[FieldNames.GREATER_ZERO] = greaterZero2 - greaterZero1;
                dtData.Rows.Add(row);

                gridBotton3.DataSource = dtData;
            }
        }

        private void BuildMidStat(DataTable v_table2)
        {
            DataTable dtData = null;
            EnsureViewDataStructure(ref dtData, (DevExpress.XtraGrid.Views.Grid.GridView)bandedGridBottonView2);

            if (null != v_table2)
            {
                decimal lessZero = v_table2.AsEnumerable().Where(r => r.Field<decimal?>(FieldNames.Di).HasValue && r.Field<decimal?>(FieldNames.D2Percent).HasValue && r.Field<decimal?>(FieldNames.Di) < 0M).Sum(r => r.Field<decimal>(FieldNames.D2Percent));
                decimal equalZero = v_table2.AsEnumerable().Where(r => r.Field<decimal?>(FieldNames.Di).HasValue && r.Field<decimal?>(FieldNames.D2Percent).HasValue && r.Field<decimal?>(FieldNames.Di) == 0M).Sum(r => r.Field<decimal>(FieldNames.D2Percent));
                decimal greaterZero = v_table2.AsEnumerable().Where(r => r.Field<decimal?>(FieldNames.Di).HasValue && r.Field<decimal?>(FieldNames.D2Percent).HasValue && r.Field<decimal?>(FieldNames.Di) > 0M).Sum(r => r.Field<decimal>(FieldNames.D2Percent));

                DataRow row = dtData.NewRow();
                row[FieldNames.LESS_ZERO] = lessZero;
                row[FieldNames.EQUAL_ZERO] = equalZero;
                row[FieldNames.GREATER_ZERO] = greaterZero;
                dtData.Rows.Add(row);

                // add empty row
                dtData.Rows.Add(dtData.NewRow());

                lessZero = v_table2.AsEnumerable().Where(r => r.Field<decimal?>(FieldNames.Di).HasValue && r.Field<decimal?>(FieldNames.V2SKU1Percent).HasValue && r.Field<decimal?>(FieldNames.Di) < 0M).Sum(r => r.Field<decimal>(FieldNames.V2SKU1Percent));
                equalZero = v_table2.AsEnumerable().Where(r => r.Field<decimal?>(FieldNames.Di).HasValue && r.Field<decimal?>(FieldNames.V2SKU1Percent).HasValue && r.Field<decimal?>(FieldNames.Di) == 0M).Sum(r => r.Field<decimal>(FieldNames.V2SKU1Percent));
                greaterZero = v_table2.AsEnumerable().Where(r => r.Field<decimal?>(FieldNames.Di).HasValue && r.Field<decimal?>(FieldNames.V2SKU1Percent).HasValue && r.Field<decimal?>(FieldNames.Di) > 0M).Sum(r => r.Field<decimal>(FieldNames.V2SKU1Percent));

                row = dtData.NewRow();
                row[FieldNames.LESS_ZERO] = lessZero;
                row[FieldNames.EQUAL_ZERO] = equalZero;
                row[FieldNames.GREATER_ZERO] = greaterZero;
                dtData.Rows.Add(row);

                // add empty row
                dtData.Rows.Add(dtData.NewRow());

                gridBotton2.DataSource = dtData;
            }
        }

        private void BuildLeftStat(DataTable v_table2)
        {
            DataTable dtData = null;
            EnsureViewDataStructure(ref dtData, (DevExpress.XtraGrid.Views.Grid.GridView)bandedGridBottonView1);

            if (null != v_table2)
            {
                decimal lessZero = v_table2.AsEnumerable().Where(r => r.Field<decimal?>(FieldNames.Di).HasValue && r.Field<decimal?>(FieldNames.D1Percent).HasValue && r.Field<decimal?>(FieldNames.Di) < 0M).Sum(r => r.Field<decimal>(FieldNames.D1Percent));
                decimal equalZero = v_table2.AsEnumerable().Where(r => r.Field<decimal?>(FieldNames.Di).HasValue && r.Field<decimal?>(FieldNames.D1Percent).HasValue && r.Field<decimal?>(FieldNames.Di) == 0M).Sum(r => r.Field<decimal>(FieldNames.D1Percent));
                decimal greaterZero = v_table2.AsEnumerable().Where(r => r.Field<decimal?>(FieldNames.Di).HasValue && r.Field<decimal?>(FieldNames.D1Percent).HasValue && r.Field<decimal?>(FieldNames.Di) > 0M).Sum(r => r.Field<decimal>(FieldNames.D1Percent));

                DataRow row = dtData.NewRow();
                row[FieldNames.LESS_ZERO] = lessZero;
                row[FieldNames.EQUAL_ZERO] = equalZero;
                row[FieldNames.GREATER_ZERO] = greaterZero;
                dtData.Rows.Add(row);

                lessZero = v_table2.AsEnumerable().Where(r => r.Field<decimal?>(FieldNames.Di).HasValue && r.Field<decimal?>(FieldNames.V1SKU1Percent).HasValue && r.Field<decimal?>(FieldNames.Di) < 0M).Sum(r => r.Field<decimal>(FieldNames.V1SKU1Percent));
                equalZero = v_table2.AsEnumerable().Where(r => r.Field<decimal?>(FieldNames.Di).HasValue && r.Field<decimal?>(FieldNames.V1SKU1Percent).HasValue && r.Field<decimal?>(FieldNames.Di) == 0M).Sum(r => r.Field<decimal>(FieldNames.V1SKU1Percent));
                greaterZero = v_table2.AsEnumerable().Where(r => r.Field<decimal?>(FieldNames.Di).HasValue && r.Field<decimal?>(FieldNames.V1SKU1Percent).HasValue && r.Field<decimal?>(FieldNames.Di) > 0M).Sum(r => r.Field<decimal>(FieldNames.V1SKU1Percent));

                row = dtData.NewRow();
                row[FieldNames.LESS_ZERO] = lessZero;
                row[FieldNames.EQUAL_ZERO] = equalZero;
                row[FieldNames.GREATER_ZERO] = greaterZero;
                dtData.Rows.Add(row);

                // add empty rows
                dtData.Rows.Add(dtData.NewRow());
                dtData.Rows.Add(dtData.NewRow());

                gridBotton1.DataSource = dtData;
            }
        }
        private void InitBottomStaticData()
        {
            // init labels
            DataTable dtStatic = new DataTable();
            dtStatic.Columns.Add(FieldNames.DATATEXT);
            for (int i = 0; i < lstPercentInfo.Count(); i++)
            {
                DataRow row = dtStatic.NewRow();
                row[FieldNames.DATATEXT] = lstPercentInfo[i];
                dtStatic.Rows.Add(row);
            }
            gridControlData.DataSource = dtStatic;
        }

        private void EnsureViewDataStructure(ref DataTable viewData, DevExpress.XtraGrid.Views.Grid.GridView gridView)
        {
            if (null != viewData)
            {
                viewData.Rows.Clear();
            }
            else
            {
                viewData = new DataTable();
                foreach (GridColumn column in gridView.Columns)
                {
                    if (!column.FieldName.Equals(FieldNames.REGION_NAME))
                        viewData.Columns.Add(column.FieldName, typeof(decimal));
                    else
                        viewData.Columns.Add(column.FieldName, typeof(string));
                }
            }
        }

    }
}
