﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using WccpReporting.RPTCAnalysis.Localization;
using WccpReporting.RPTCAnalysis.Utility;
using WccpReporting.RPTCAnalysis.UserControls.Contracts;
using Logica.Reports.BaseReportControl;
using DevExpress.XtraPrintingLinks;

namespace WccpReporting.RPTCAnalysis.UserControls
{
    public partial class InDataDynamicTab : BaseTabReport, IReportTab
    {
        internal event Delegates.DataSourceChanged FilterChanged;
        public InDataDynamicTab()
            : base()
        {
            InitializeComponent();
            TabName = Resource.InData; 
            base.SourceInitializedEvent += new Delegates.DataSourceChanged(InDataDynamicTab_SourceInitializedEvent);
            gridViewInDataDynamic.BestFitColumns();
        }

        internal override CompositeLink ExportTo(ExportToType exportType, CompositeLink compositeLink)
        {
            return ExportGridControlTo(gridInDataDynamic, compositeLink, exportType);
        }

        void InDataDynamicTab_SourceInitializedEvent(DataTable source)
        {
            gridInDataDynamic.DataSource = source;
            gridViewInDataDynamic.BestFitColumns();
        }

        private void gridView_ColumnFilterChanged(object sender, EventArgs e)
        {
            FireEvent();
        }

        private void gridInDataDynamic_DataSourceChanged(object sender, EventArgs e)
        {
            FireEvent();
        }

        private void FireEvent()
        {
            if (null != FilterChanged && null != gridInDataDynamic.DataSource)
            {
                DataTable dt = Functions.GetFilteredDataTable((DataTable)gridInDataDynamic.DataSource, gridViewInDataDynamic);
                FilterChanged(dt);
            }
        }
    }
}
