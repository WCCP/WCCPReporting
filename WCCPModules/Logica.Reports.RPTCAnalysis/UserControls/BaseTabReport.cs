﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using WccpReporting.RPTCAnalysis.UserControls.Contracts;
using Logica.Reports.Common.WaitWindow;
using DevExpress.XtraTab;
using Logica.Reports.BaseReportControl;
using DevExpress.XtraGrid;
using WccpReporting.RPTCAnalysis.Localization;
using DevExpress.XtraPrintingLinks;
using DevExpress.XtraPrinting;

namespace WccpReporting.RPTCAnalysis.UserControls
{
    public partial class BaseTabReport : UserControl
    {
        internal event Delegates.DataSourceChanged SourceInitializedEvent;
        internal DataTable sourceDataTable = null;
        protected ReportSettings reportSettings = null;

        private string tabName = "Tab";
        public string TabName
        {
            get
            {
                return tabName;
            }
            protected set
            {
                tabName = value;
            }
        }

        internal virtual CompositeLink ExportTo(ExportToType exportType, CompositeLink compositeLink)
        {
            return null;
        }

        public BaseTabReport()
        {
            InitializeComponent();
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            Dock = DockStyle.Fill;
        }

        internal void SetReportSetting(ReportSettings initialSettings)
        {
            reportSettings = initialSettings;
        }

        internal void SetSource(DataTable source)
        {
            //WaitManager.StartWait();

            sourceDataTable = source;
            if (null != source && null != SourceInitializedEvent)
                SourceInitializedEvent(source);

            //WaitManager.StopWait();
        }

        internal virtual CompositeLink ExportGridControlTo(GridControl gridControl, CompositeLink compositeLink, ExportToType type)
        {
            if (null != gridControl)
            {
                try
                {
                    switch (type)
                    {
                        case ExportToType.Csv:
                        case ExportToType.Bmp:
                            //MessageBox.Show(Resource.ExportFormatNotSupported, Resource.Information);
                            break;
                        default:
                            compositeLink.Links.Add(new RichTextBoxLink() { RichTextBox = new RichTextBox() { Text = GetTabText() } });
                            compositeLink.Links.Add(new PrintableComponentLink() { Component = gridControl });
                            compositeLink.Links.Add(new RichTextBoxLink() { RichTextBox = new RichTextBox() { Text = " " } });
                            compositeLink.CreateDocument();
                            break;
                            /*
                        case ExportToType.Html:
                            gridControl.ExportToHtml(fileName);
                            break;
                        case ExportToType.Mht:
                            gridControl.ExportToMht(fileName);
                            break;
                        case ExportToType.Pdf:
                            gridControl.ExportToPdf(fileName);
                            break;
                        case ExportToType.Rtf:
                            gridControl.ExportToRtf(fileName);
                            break;
                        case ExportToType.Txt:
                            gridControl.ExportToText(fileName);
                            break;
                        case ExportToType.Xls:
                            gridControl.ExportToXls(fileName);
                            break;
                             * */
                    }
                }
                catch (Exception ex)
                {
                    //BaseTabReport.HandleExportException(ex, fileName);
                }
            }
            return compositeLink;
        }

        protected string GetTabText()
        {
            return "\n" + TabName + "\n";
        }

        /// <summary>
        /// Handles the export exception.
        /// </summary>
        /// <param name="ex">The ex.</param>
        internal static void HandleExportException(Exception ex, string fileName)
        {
            string errorMsg = String.Format(Resource.ExportError, fileName);
            if (ex != null)
            {
                errorMsg += string.Format(Resource.ExportErrorAddInfo, ex.Message);
                if (ex.InnerException != null && !string.IsNullOrEmpty(ex.InnerException.Message))
                {
                    errorMsg += string.Format("\n{0}", ex.InnerException.Message);
                }
            }
            MessageBox.Show(errorMsg, Resource.Error);
        }
    }
}
