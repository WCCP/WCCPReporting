﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using WccpReporting.RPTCAnalysis.UserControls.Contracts;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Grid;

namespace WccpReporting.RPTCAnalysis.UserControls
{
    public abstract partial class V_TableBaseTab : BaseTabReport, IReportTab
    {
        internal event Delegates.DataSourceChanged DataSourceChanged;

        internal DataTable viewData = null;

        public V_TableBaseTab()
            : base()
        {
            InitializeComponent();
        }

        protected void EnsureViewDataStructure(GridView gridView)
        {
            if (null != viewData)
            {
                viewData.Rows.Clear();
            }
            else
            {
                viewData = new DataTable();
                foreach (GridColumn column in gridView.Columns)
                {
                    viewData.Columns.Add(column.FieldName, typeof(decimal));
                }
            }
        }
        
        protected void FireEventDataSourceChanged()
        {
            if (null != viewData && null != DataSourceChanged)
                DataSourceChanged(viewData.Copy());
        }
    }
}
