﻿//#define DESIGN_MODE
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using WccpReporting.RPTCAnalysis.UserControls;
using WccpReporting.RPTCAnalysis.Localization;
using DevExpress.XtraCharts;
using WccpReporting.RPTCAnalysis.Utility;
using WccpReporting.RPTCAnalysis.Constants;
using Logica.Reports.BaseReportControl;
using DevExpress.XtraPrintingLinks;
using DevExpress.XtraPrinting;

namespace WccpReporting.RPTCAnalysis.UserControls
{
    public partial class ChartsTab : BaseTabReport, IReportTab
    {
        public ChartsTab()
            : base()
        {
            InitializeComponent();
            TabName = Resource.Charts;
        }

        DataTable v_table1 = null;
        DataTable v_table2 = null;
        DataTable v_table3 = null;
        DataTable inDataTable = null;

        private double barWidthKoef = 100;

        private double step = 0.1f;

        private double Step
        {
            get
            {
                if (null != reportSettings)
                    return (double)reportSettings.Step;
                else
                    return step;
            }
        }

        // properties for Charts12
        private double MinPTC
        {
            get
            {
                decimal? minVal = null;
                decimal? maxVal = null;
                Functions.FindMinMaxDecimal(v_table1, ref maxVal, ref minVal, FieldNames.PTC);
                double chunkLength = (double)(maxVal ?? 0 - minVal ?? 0) / 100;
                return fromPTC + (Convert.ToDouble(hScrollBar1.Value) - 1) * chunkLength;// Step;
            }
        }
        private double MaxPTC
        {
            get { return MinPTC + (Convert.ToDouble(vScrollBar1.Value)) * Step; }
        }
        private double fromPTC = new double();

        //properties for Charts34
        private double MinDi
        {
            get
            {
                decimal? minVal = null;
                decimal? maxVal = null;
                Functions.FindMinMaxDecimal(v_table2, ref maxVal, ref minVal, FieldNames.Di);
                double chunkLength = (double)(maxVal ?? 0 - minVal ?? 0) / 100;
                return fromDi + (Convert.ToDouble(hScrollBar2.Value) - 1) * chunkLength;// Step;
            }
        }
        private double MaxDi
        {
            get { return MinDi + (Convert.ToDouble(vScrollBar2.Value)) * Step; }
        }

        private double fromDi = new double();

        //properties for Charts56
        private double MinD21
        {
            get
            {
                decimal? minVal = null;
                decimal? maxVal = null;
                Functions.FindMinMaxDecimal(v_table3, ref maxVal, ref minVal, FieldNames.D21);
                double chunkLength = (double)(maxVal ?? 0 - minVal ?? 0) / 100;
                return fromD21 + (Convert.ToDouble(hScrollBar3.Value) - 1) * chunkLength;// Step;
            }
        }
        private double MaxD21
        {
            get { return MinD21 + (Convert.ToDouble(vScrollBar3.Value)) * Step; }
        }

        private double fromD21 = new double();

        public DataTable InData
        {
            set
            {
                inDataTable = value;
            }
        }
        public DataTable V_Table1
        {
            set
            {
                v_table1 = value;

                SetupChart12();
                SetupInfoChart12();
                SetRangeCharts(chartControl1, chartControl2, MinPTC, MaxPTC);
                SetupInfoSample12();

                hScrollBar1Label.Text = hScrollBar1.Value.ToString();
                vScrollBar1Label.Text = vScrollBar1.Value.ToString();
            }

        }

        #region Methods For Chart12
        private void SetupInfoSample12()
        {
            decimal? maxValSKU1 = null;
            decimal? minValSKU1 = null;
            Functions.FindMinMaxDecimal(inDataTable, ref maxValSKU1, ref minValSKU1, FieldNames.PTC1SKU1);
            Functions.FindMinMaxDecimal(inDataTable, ref maxValSKU1, ref minValSKU1, FieldNames.PTC2SKU1);
            decimal? maxValSKU2 = null;
            decimal? minValSKU2 = null;
            Functions.FindMinMaxDecimal(inDataTable, ref maxValSKU2, ref minValSKU2, FieldNames.PTC1SKU2);
            Functions.FindMinMaxDecimal(inDataTable, ref maxValSKU2, ref minValSKU2, FieldNames.PTC2SKU2);

            txtMinSKU1.Text = NuberToString(minValSKU1);
            txtMaxSKU1.Text = NuberToString(maxValSKU1);
            txtMinSKU2.Text = NuberToString(minValSKU2);
            txtMaxSKU2.Text = NuberToString(maxValSKU2);
        }

        private void SetupChart12()
        {
            chartControl1.DataSource = v_table1;
            chartControl1.Series[0].ArgumentDataMember = FieldNames.PTC;
            chartControl1.Series[0].ValueDataMembers.AddRange(new string[] { FieldNames.ST1SKU1POCPercent });
            chartControl1.Series[1].ArgumentDataMember = FieldNames.PTC;
            chartControl1.Series[1].ValueDataMembers.AddRange(new string[] { FieldNames.ST2SKU1POCPercent });

            chartControl2.DataSource = v_table1;
            chartControl2.Series[0].ArgumentDataMember = FieldNames.PTC;
            chartControl2.Series[0].ValueDataMembers.AddRange(new string[] { FieldNames.ST2SKU1POCPercent });
            chartControl2.Series[1].ArgumentDataMember = FieldNames.PTC;
            chartControl2.Series[1].ValueDataMembers.AddRange(new string[] { FieldNames.ST2SKU2POCPercent });

            fromPTC = GetFirstValue(v_table1, FieldNames.PTC);
        }

        private void SetupInfoChart12()
        {
            txtStep12.Text = NuberToString(Step);
            txtMinPTC.Text = NuberToString(MinPTC);
            txtMaxPTC.Text = NuberToString(MaxPTC);
            double minValue = MinPTC - Step / 2;
            double maxValue = MaxPTC + Step / 2;

            double OUSSKU1 = GetOUS(v_table1, FieldNames.PTC, FieldNames.ST1SKU1POCPercent, (decimal)minValue, (decimal)maxValue);
            double OUSSKU2 = GetOUS(v_table1, FieldNames.PTC, FieldNames.ST1SKU2POCPercent, (decimal)minValue, (decimal)maxValue);

            txtOUSSKU1.Text = NuberToString(OUSSKU1) + " %";
            txtOUSSKU2.Text = NuberToString(OUSSKU2) + " %";
        }

        private void hScrollBar1_ValueChanged(object sender, EventArgs e)
        {
            SetRangeCharts(chartControl1, chartControl2, MinPTC, MaxPTC);
            hScrollBar1.Focus();
            hScrollBar1Label.Text = hScrollBar1.Value.ToString();
            SetupInfoChart12();
        }

        private void vScrollBar1_ValueChanged(object sender, EventArgs e)
        {
            SetRangeCharts(chartControl1, chartControl2, MinPTC, MaxPTC);
            vScrollBar1Label.Text = vScrollBar1.Value.ToString();
            SetupInfoChart12();
        }
        #endregion

        public DataTable V_Table2
        {
            set
            {
                v_table2 = value;

                SetupChart34();
                SetupInfoChart34();
                SetupInfoSample34();
                SetRangeCharts(chartControl3, chartControl4, MinDi, MaxDi);

                hScrollBar2Label.Text = hScrollBar2.Value.ToString();
                vScrollBar2Label.Text = vScrollBar2.Value.ToString();
            }
        }
        #region Methods for Chart34

        private void SetupChart34()
        {
            chartControl3.DataSource = v_table2;
            chartControl3.Series[0].ArgumentDataMember = FieldNames.Di;
            chartControl3.Series[0].ValueDataMembers.AddRange(new string[] { FieldNames.V2SKU1perPOC });
            chartControl3.Series[1].ArgumentDataMember = FieldNames.Di;
            chartControl3.Series[1].ValueDataMembers.AddRange(new string[] { FieldNames.TREND });

            chartControl4.DataSource = v_table2;
            chartControl4.Series[0].ArgumentDataMember = FieldNames.Di;
            chartControl4.Series[0].ValueDataMembers.AddRange(new string[] { FieldNames.D1Percent });
            chartControl4.Series[1].ArgumentDataMember = FieldNames.Di;
            chartControl4.Series[1].ValueDataMembers.AddRange(new string[] { FieldNames.D2Percent });

            fromDi = GetFirstValue(v_table2, FieldNames.Di);
        }
        private void hScrollBar2_ValueChanged(object sender, EventArgs e)
        {
            SetRangeCharts(chartControl3, chartControl4, MinDi, MaxDi);
            hScrollBar2.Focus();
            hScrollBar2Label.Text = hScrollBar2.Value.ToString();
            SetupInfoChart34();
        }

        private void SetupInfoSample34()
        {
            decimal? minValueD1 = null;
            decimal? maxValueD1 = null;
            Functions.FindMinMaxDecimal(inDataTable, ref maxValueD1, ref minValueD1, FieldNames.D1);
            decimal? minValueD2 = null;
            decimal? maxValueD2 = null;
            Functions.FindMinMaxDecimal(inDataTable, ref maxValueD2, ref minValueD2, FieldNames.D2);

            txtMin_D1.Text = NuberToString(minValueD1);
            txtMin_D2.Text = NuberToString(minValueD2);
            txtMax_D1.Text = NuberToString(maxValueD1);
            txtMax_D2.Text = NuberToString(maxValueD2);
        }

        private void SetupInfoChart34()
        {
            txtStep34.Text = NuberToString(Step);
            txtMinDi.Text = NuberToString(MinDi);
            txtMaxDi.Text = NuberToString(MaxDi);

            txtOUS_D1.Text = NuberToString(GetOUS(v_table2, FieldNames.Di, FieldNames.D1Percent, (decimal)MinDi, (decimal)MaxDi)) + " %";
            txtOUS_D2.Text = NuberToString(GetOUS(v_table2, FieldNames.Di, FieldNames.D2Percent, (decimal)MinDi, (decimal)MaxDi)) + " %";
        }

        private void vScrollBar2_ValueChanged(object sender, EventArgs e)
        {
            SetRangeCharts(chartControl3, chartControl4, MinDi, MaxDi);
            vScrollBar2Label.Text = vScrollBar2.Value.ToString();
            SetupInfoChart34();
        }
        #endregion

        private double ApplySpaceToCharts(ChartControl chart1, ChartControl chart2, double minX, double maxX)
        {
            double space;
            double dX = maxX - minX;
            double bestStep = (dX / 5);
            space = Step * (int)Math.Round(bestStep / Step);

            if (space < 0.01)
            {
                space = Step;
            }

            (chart1.Diagram as XYDiagram).AxisX.GridSpacingAuto = false;
            (chart1.Diagram as XYDiagram).AxisX.GridSpacing = space;
            (chart2.Diagram as XYDiagram).AxisX.GridSpacingAuto = false;
            (chart2.Diagram as XYDiagram).AxisX.GridSpacing = space;

            return space;
        }

        public DataTable V_Table3
        {
            set
            {
                v_table3 = value;
                SetupChart56();
                SetupInfoChart56();
                SetupInfoSample56();
                SetRangeCharts(chartControl5, chartControl6, MinDi, MaxDi);

                hScrollBar3Label.Text = hScrollBar3.Value.ToString();
                vScrollBar3Label.Text = vScrollBar3.Value.ToString();
            }
        }

        private void SetupInfoChart56()
        {
            txtStep56.Text = NuberToString(Step);
            txtMin_D21.Text = NuberToString(MinD21);
            txtMax_D21.Text = NuberToString(MaxD21);
            double minValue = MinD21 - Step / 2;
            double maxValue = MaxD21 + Step / 2;
            txtOUS_D21.Text = NuberToString(GetOUS(v_table3, FieldNames.D21, FieldNames.D21Percent, (decimal)minValue, (decimal)maxValue)) + " %";
        }

        private void SetupInfoSample56()
        {
            decimal? minValueD21 = null;
            decimal? maxValueD21 = null;
            Functions.FindMinMaxDecimal(inDataTable, ref maxValueD21, ref minValueD21, FieldNames.D21);

            txtMin_D21Sample.Text = NuberToString(minValueD21);
            txtMax_D21Sample.Text = NuberToString(maxValueD21);
        }

        private void SetupChart56()
        {
            chartControl6.DataSource = v_table3;
            chartControl6.Series[0].ArgumentDataMember = FieldNames.D21;
            chartControl6.Series[0].ValueDataMembers.AddRange(new string[] { FieldNames.D21Percent });

            chartControl5.DataSource = v_table3;
            chartControl5.Series[0].ArgumentDataMember = FieldNames.D21;
            chartControl5.Series[0].ValueDataMembers.AddRange(new string[] { FieldNames.POCShareST2SKU1 });
            chartControl5.Series[1].ArgumentDataMember = FieldNames.D21;
            chartControl5.Series[1].ValueDataMembers.AddRange(new string[] { FieldNames.TREND });

            fromD21 = GetFirstValue(v_table3, FieldNames.D21);

        }

        private void SetRangeCharts(ChartControl chartControl_1, ChartControl chartControl_2, double minValue, double maxValue)
        {
            if (maxValue > minValue)
            {
                bool columnsView1 = chartControl_1.Series.Count > 0 && chartControl_1.Series[0].View is SideBySideBarSeriesView;
                bool columnsView2 = chartControl_2.Series.Count > 0 && chartControl_2.Series[0].View is SideBySideBarSeriesView;

                try
                {
                    double space = ApplySpaceToCharts(chartControl_1, chartControl_2, minValue, maxValue);

                    AxisRange range1 = ((XYDiagram)chartControl_1.Diagram).AxisX.Range;
                    AxisRange range2 = ((XYDiagram)chartControl_2.Diagram).AxisX.Range;

                    //range1.SetMinMaxValues(minValue, maxValue);
                    //range2.SetMinMaxValues(minValue, maxValue);

                    double width = (maxValue - minValue) / this.barWidthKoef;
                    foreach (Series s in chartControl_1.Series)
                    {
                        if (s.View is SideBySideBarSeriesView)
                        {
                            (s.View as SideBySideBarSeriesView).BarWidth = width * chartControl_1.Series.Count;
                        }
                    }

                    foreach (Series s in chartControl_2.Series)
                    {
                        if (s.View is SideBySideBarSeriesView)
                        {
                            (s.View as SideBySideBarSeriesView).BarWidth = width * chartControl_2.Series.Count;
                        }
                    }

                    range1.SetMinMaxValues(minValue - width, maxValue + width);
                    range2.SetMinMaxValues(minValue - width, maxValue + width);
                }
                catch (Exception e)
                {
                    string mess = e.Message;
                }
            }
        }

        private static double GetFirstValue(DataTable dataTable, string fieldName)
        {
            double res = 0.0;
            var selectedData = (from row in dataTable.AsEnumerable()
                                select row).FirstOrDefault();

            if (null != selectedData)
                res = (double)selectedData.Field<decimal>(fieldName);
            return res;
        }

        private static double GetOUS(DataTable dataTable, string indicatorFieldName, string sumFieldName, decimal minValue, decimal maxValue)
        {
            var tmp = from row in dataTable.AsEnumerable()
                      where row.Field<decimal?>(sumFieldName).HasValue && row.Field<decimal?>(indicatorFieldName).HasValue && (row.Field<decimal>(indicatorFieldName) < minValue || row.Field<decimal>(indicatorFieldName) > maxValue)
                      select row;

            var result = tmp.AsEnumerable().Sum(r => r.Field<decimal>(sumFieldName));

            return (double)result;
        }

        private void hScrollBar3_ValueChanged(object sender, EventArgs e)
        {
            SetRangeCharts(chartControl5, chartControl6, MinD21, MaxD21);
            hScrollBar3.Focus();
            hScrollBar3Label.Text = hScrollBar3.Value.ToString();
            SetupInfoChart56();
        }

        private void vScrollBar3_ValueChanged(object sender, EventArgs e)
        {
            SetRangeCharts(chartControl5, chartControl6, MinD21, MaxD21);
            vScrollBar3Label.Text = vScrollBar3.Value.ToString();
            SetupInfoChart56();
        }

        private void chartControl_ObjectHotTracked(object sender, HotTrackEventArgs e)
        {
            SeriesPoint point = (e.AdditionalObject as SeriesPoint);
            if (point != null && point.Values.Count() > 0)
            {
                toolTipController1.ShowHint(point.Values[0].ToString("0.00") + " %");
            }
            else
            {
                toolTipController1.HideHint();
            }
        }

        private string NuberToString(double d)
        {
            return Math.Round(d, 2).ToString("0.00");
        }

        private string NuberToString(decimal d)
        {
            return Math.Round(d, 2).ToString("0.00");
        }

        private string NuberToString(decimal? d)
        {
            return NuberToString(d.HasValue ? d.Value : 0);
        }

        internal override CompositeLink ExportTo(ExportToType exportType, CompositeLink compositeLink)
        {
            try
            {
                switch (exportType)
                {
                    case ExportToType.Csv:
                    case ExportToType.Bmp:
                    case ExportToType.Rtf:
                    case ExportToType.Txt:

                        //MessageBox.Show(Resource.ExportFormatNotSupported, Resource.Information);
                        break;
                    /*
                case ExportToType.Html:
                    PrepareCompositeLink().PrintingSystem.ExportToHtml(fileName);
                    break;
                case ExportToType.Mht:
                    PrepareCompositeLink().PrintingSystem.ExportToMht(fileName);
                    break;
                case ExportToType.Pdf:
                    PrepareCompositeLink().PrintingSystem.ExportToPdf(fileName);
                    break;
                case ExportToType.Xls:
                    PrepareCompositeLink().PrintingSystem.ExportToXls(fileName);
                    break;
                     */
                    default:
                        compositeLink = PrepareCompositeLink(compositeLink);
                        break;
                }
            }
            catch (Exception ex)
            {
                //BaseTabReport.HandleExportException(ex, fileName);
            }
            return compositeLink;
        }

        public CompositeLink PrepareCompositeLink(CompositeLink compositeLink)
        {
            compositeLink.Links.Add(new RichTextBoxLink() { RichTextBox = new RichTextBox() { Text = GetTabText() } });
            compositeLink.Links.Add(new PrintableComponentLink() { Component = chartControl1 });
            compositeLink.Links.Add(new PrintableComponentLink() { Component = chartControl2 });
            compositeLink.Links.Add(new RichTextBoxLink() { RichTextBox = new RichTextBox() { Text = " " } });
            compositeLink.Links.Add(new PrintableComponentLink() { Component = chartControl3 });
            compositeLink.Links.Add(new PrintableComponentLink() { Component = chartControl4 });
            compositeLink.Links.Add(new RichTextBoxLink() { RichTextBox = new RichTextBox() { Text = " " } });
            compositeLink.Links.Add(new PrintableComponentLink() { Component = chartControl5 });
            compositeLink.Links.Add(new PrintableComponentLink() { Component = chartControl6 });
            compositeLink.Links.Add(new RichTextBoxLink() { RichTextBox = new RichTextBox() { Text = " " } });
            compositeLink.CreateDocument();
            return compositeLink;
        }
    }
}
