﻿namespace WccpReporting.RPTCAnalysis.UserControls
{
    partial class StatisticTab
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gridControlTopLeft = new DevExpress.XtraGrid.GridControl();
            this.gridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridControlTop = new DevExpress.XtraGrid.GridControl();
            this.bandedGridTopView = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridView();
            this.gridBand1 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridColumn3 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand2 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridColumn4 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridColumn7 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand3 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridColumn8 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridColumn9 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridColumn10 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridColumn11 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand4 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridColumn12 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridColumn13 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridColumn14 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridColumn15 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridControlData = new DevExpress.XtraGrid.GridControl();
            this.gridViewData = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridBottonLeft = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridBotton1 = new DevExpress.XtraGrid.GridControl();
            this.bandedGridBottonView1 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridView();
            this.gridBand5 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridColumn16 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridColumn17 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridColumn18 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBotton2 = new DevExpress.XtraGrid.GridControl();
            this.bandedGridBottonView2 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridView();
            this.gridBand6 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn1 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn2 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn3 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBotton3 = new DevExpress.XtraGrid.GridControl();
            this.bandedGridBottonView3 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridView();
            this.gridBand7 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn4 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn5 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn6 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridParamsInfo = new DevExpress.XtraGrid.GridControl();
            this.bandedGridViewParamsInfo = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridView();
            this.gridBand15 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand9 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn201 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn202 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand10 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn203 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn204 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand11 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn205 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand8 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn206 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn207 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand14 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn208 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand12 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn209 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn210 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand13 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn211 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn212 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn213 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlTopLeft)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlTop)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bandedGridTopView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridBotton1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bandedGridBottonView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridBotton2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bandedGridBottonView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridBotton3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bandedGridBottonView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridParamsInfo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bandedGridViewParamsInfo)).BeginInit();
            this.SuspendLayout();
            // 
            // gridControlTopLeft
            // 
            this.gridControlTopLeft.Location = new System.Drawing.Point(0, 104);
            this.gridControlTopLeft.MainView = this.gridView;
            this.gridControlTopLeft.Name = "gridControlTopLeft";
            this.gridControlTopLeft.Size = new System.Drawing.Size(197, 124);
            this.gridControlTopLeft.TabIndex = 24;
            this.gridControlTopLeft.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView});
            // 
            // gridView
            // 
            this.gridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2});
            this.gridView.GridControl = this.gridControlTopLeft;
            this.gridView.Name = "gridView";
            this.gridView.OptionsBehavior.Editable = false;
            this.gridView.OptionsCustomization.AllowFilter = false;
            this.gridView.OptionsMenu.EnableFooterMenu = false;
            this.gridView.OptionsView.ShowGroupPanel = false;
            this.gridView.OptionsView.ShowIndicator = false;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "gridColumn1";
            this.gridColumn1.FieldName = "NAME";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowEdit = false;
            this.gridColumn1.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumn1.OptionsColumn.AllowMove = false;
            this.gridColumn1.OptionsColumn.AllowShowHide = false;
            this.gridColumn1.OptionsColumn.ReadOnly = true;
            this.gridColumn1.OptionsColumn.ShowCaption = false;
            this.gridColumn1.OptionsFilter.AllowAutoFilter = false;
            this.gridColumn1.OptionsFilter.AllowFilter = false;
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 0;
            this.gridColumn1.Width = 88;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "gridColumn2";
            this.gridColumn2.FieldName = "PARVALUE";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.AllowEdit = false;
            this.gridColumn2.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumn2.OptionsColumn.AllowMove = false;
            this.gridColumn2.OptionsColumn.AllowShowHide = false;
            this.gridColumn2.OptionsColumn.ReadOnly = true;
            this.gridColumn2.OptionsColumn.ShowCaption = false;
            this.gridColumn2.OptionsFilter.AllowAutoFilter = false;
            this.gridColumn2.OptionsFilter.AllowFilter = false;
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 1;
            this.gridColumn2.Width = 88;
            // 
            // gridControlTop
            // 
            this.gridControlTop.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControlTop.Location = new System.Drawing.Point(0, 241);
            this.gridControlTop.MainView = this.bandedGridTopView;
            this.gridControlTop.Name = "gridControlTop";
            this.gridControlTop.Size = new System.Drawing.Size(1094, 505);
            this.gridControlTop.TabIndex = 25;
            this.gridControlTop.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.bandedGridTopView});
            // 
            // bandedGridTopView
            // 
            this.bandedGridTopView.Bands.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand1,
            this.gridBand2,
            this.gridBand3,
            this.gridBand4});
            this.bandedGridTopView.Columns.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn[] {
            this.gridColumn3,
            this.gridColumn4,
            this.gridColumn5,
            this.gridColumn6,
            this.gridColumn7,
            this.gridColumn8,
            this.gridColumn9,
            this.gridColumn10,
            this.gridColumn11,
            this.gridColumn12,
            this.gridColumn13,
            this.gridColumn14,
            this.gridColumn15});
            this.bandedGridTopView.GridControl = this.gridControlTop;
            this.bandedGridTopView.Name = "bandedGridTopView";
            this.bandedGridTopView.OptionsBehavior.Editable = false;
            this.bandedGridTopView.OptionsCustomization.AllowFilter = false;
            this.bandedGridTopView.OptionsMenu.EnableFooterMenu = false;
            this.bandedGridTopView.OptionsView.ShowGroupPanel = false;
            this.bandedGridTopView.OptionsView.ShowIndicator = false;
            // 
            // gridBand1
            // 
            this.gridBand1.Columns.Add(this.gridColumn3);
            this.gridBand1.Name = "gridBand1";
            this.gridBand1.Width = 191;
            // 
            // gridColumn3
            // 
            this.gridColumn3.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn3.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn3.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn3.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn3.Caption = "Geographical Unit";
            this.gridColumn3.FieldName = "Region_name";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.Visible = true;
            this.gridColumn3.Width = 191;
            // 
            // gridBand2
            // 
            this.gridBand2.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand2.Caption = "SKU1";
            this.gridBand2.Columns.Add(this.gridColumn4);
            this.gridBand2.Columns.Add(this.gridColumn5);
            this.gridBand2.Columns.Add(this.gridColumn6);
            this.gridBand2.Columns.Add(this.gridColumn7);
            this.gridBand2.Name = "gridBand2";
            this.gridBand2.Width = 240;
            // 
            // gridColumn4
            // 
            this.gridColumn4.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn4.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn4.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn4.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn4.Caption = "PTC 1";
            this.gridColumn4.DisplayFormat.FormatString = "{0:N2}";
            this.gridColumn4.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn4.FieldName = "SKU1PTC1";
            this.gridColumn4.MinWidth = 60;
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.Visible = true;
            this.gridColumn4.Width = 60;
            // 
            // gridColumn5
            // 
            this.gridColumn5.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn5.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn5.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn5.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn5.Caption = "PTC 2";
            this.gridColumn5.DisplayFormat.FormatString = "{0:N2}";
            this.gridColumn5.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn5.FieldName = "SKU1PTC2";
            this.gridColumn5.MinWidth = 60;
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.Visible = true;
            this.gridColumn5.Width = 60;
            // 
            // gridColumn6
            // 
            this.gridColumn6.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn6.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn6.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn6.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn6.Caption = "Change";
            this.gridColumn6.DisplayFormat.FormatString = "({0:N2})";
            this.gridColumn6.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn6.FieldName = "SKU1Change";
            this.gridColumn6.MinWidth = 60;
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.Visible = true;
            this.gridColumn6.Width = 60;
            // 
            // gridColumn7
            // 
            this.gridColumn7.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn7.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn7.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn7.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn7.Caption = "Change%";
            this.gridColumn7.DisplayFormat.FormatString = "{0:N2}%";
            this.gridColumn7.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn7.FieldName = "SKU1ChangePercent";
            this.gridColumn7.MinWidth = 60;
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.Visible = true;
            this.gridColumn7.Width = 60;
            // 
            // gridBand3
            // 
            this.gridBand3.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand3.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand3.Caption = "SKU2";
            this.gridBand3.Columns.Add(this.gridColumn8);
            this.gridBand3.Columns.Add(this.gridColumn9);
            this.gridBand3.Columns.Add(this.gridColumn10);
            this.gridBand3.Columns.Add(this.gridColumn11);
            this.gridBand3.Name = "gridBand3";
            this.gridBand3.Width = 240;
            // 
            // gridColumn8
            // 
            this.gridColumn8.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn8.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn8.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn8.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn8.Caption = "PTC 1";
            this.gridColumn8.DisplayFormat.FormatString = "{0:N2}";
            this.gridColumn8.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn8.FieldName = "SKU2PTC1";
            this.gridColumn8.MinWidth = 60;
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.Visible = true;
            this.gridColumn8.Width = 60;
            // 
            // gridColumn9
            // 
            this.gridColumn9.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn9.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn9.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn9.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn9.Caption = "PTC 2";
            this.gridColumn9.DisplayFormat.FormatString = "{0:N2}";
            this.gridColumn9.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn9.FieldName = "SKU2PTC2";
            this.gridColumn9.MinWidth = 60;
            this.gridColumn9.Name = "gridColumn9";
            this.gridColumn9.Visible = true;
            this.gridColumn9.Width = 60;
            // 
            // gridColumn10
            // 
            this.gridColumn10.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn10.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn10.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn10.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn10.Caption = "Change";
            this.gridColumn10.DisplayFormat.FormatString = "({0:N2})";
            this.gridColumn10.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn10.FieldName = "SKU2Change";
            this.gridColumn10.MinWidth = 60;
            this.gridColumn10.Name = "gridColumn10";
            this.gridColumn10.Visible = true;
            this.gridColumn10.Width = 60;
            // 
            // gridColumn11
            // 
            this.gridColumn11.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn11.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn11.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn11.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn11.Caption = "Change%";
            this.gridColumn11.DisplayFormat.FormatString = "{0:N2}%";
            this.gridColumn11.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn11.FieldName = "SKU2ChangePercent";
            this.gridColumn11.MinWidth = 60;
            this.gridColumn11.Name = "gridColumn11";
            this.gridColumn11.Visible = true;
            this.gridColumn11.Width = 60;
            // 
            // gridBand4
            // 
            this.gridBand4.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand4.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand4.Caption = "RPTC";
            this.gridBand4.Columns.Add(this.gridColumn12);
            this.gridBand4.Columns.Add(this.gridColumn13);
            this.gridBand4.Columns.Add(this.gridColumn14);
            this.gridBand4.Columns.Add(this.gridColumn15);
            this.gridBand4.Name = "gridBand4";
            this.gridBand4.Width = 240;
            // 
            // gridColumn12
            // 
            this.gridColumn12.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn12.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn12.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn12.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn12.Caption = "ST1";
            this.gridColumn12.DisplayFormat.FormatString = "{0:N2}";
            this.gridColumn12.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn12.FieldName = "RPTCST1";
            this.gridColumn12.MinWidth = 60;
            this.gridColumn12.Name = "gridColumn12";
            this.gridColumn12.Visible = true;
            this.gridColumn12.Width = 60;
            // 
            // gridColumn13
            // 
            this.gridColumn13.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn13.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn13.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn13.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn13.Caption = "ST2";
            this.gridColumn13.DisplayFormat.FormatString = "{0:N2}";
            this.gridColumn13.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn13.FieldName = "RPTCST2";
            this.gridColumn13.MinWidth = 60;
            this.gridColumn13.Name = "gridColumn13";
            this.gridColumn13.Visible = true;
            this.gridColumn13.Width = 60;
            // 
            // gridColumn14
            // 
            this.gridColumn14.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn14.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn14.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn14.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn14.Caption = "Change";
            this.gridColumn14.DisplayFormat.FormatString = "({0:N2})";
            this.gridColumn14.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn14.FieldName = "RPTCChange";
            this.gridColumn14.MinWidth = 60;
            this.gridColumn14.Name = "gridColumn14";
            this.gridColumn14.Visible = true;
            this.gridColumn14.Width = 60;
            // 
            // gridColumn15
            // 
            this.gridColumn15.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn15.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn15.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn15.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn15.Caption = "Change%";
            this.gridColumn15.DisplayFormat.FormatString = "{0:N2}%";
            this.gridColumn15.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn15.FieldName = "RPTCChangePercent";
            this.gridColumn15.MinWidth = 60;
            this.gridColumn15.Name = "gridColumn15";
            this.gridColumn15.Visible = true;
            this.gridColumn15.Width = 60;
            // 
            // gridControlData
            // 
            this.gridControlData.Location = new System.Drawing.Point(208, 104);
            this.gridControlData.MainView = this.gridViewData;
            this.gridControlData.Name = "gridControlData";
            this.gridControlData.Size = new System.Drawing.Size(197, 126);
            this.gridControlData.TabIndex = 26;
            this.gridControlData.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewData});
            // 
            // gridViewData
            // 
            this.gridViewData.BestFitMaxRowCount = 4;
            this.gridViewData.ColumnPanelRowHeight = 40;
            this.gridViewData.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridBottonLeft});
            this.gridViewData.GridControl = this.gridControlData;
            this.gridViewData.HorzScrollStep = 1;
            this.gridViewData.HorzScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Never;
            this.gridViewData.Name = "gridViewData";
            this.gridViewData.OptionsBehavior.Editable = false;
            this.gridViewData.OptionsCustomization.AllowFilter = false;
            this.gridViewData.OptionsMenu.EnableFooterMenu = false;
            this.gridViewData.OptionsView.ShowGroupPanel = false;
            this.gridViewData.OptionsView.ShowIndicator = false;
            this.gridViewData.ScrollStyle = DevExpress.XtraGrid.Views.Grid.ScrollStyleFlags.None;
            this.gridViewData.VertScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Never;
            // 
            // gridBottonLeft
            // 
            this.gridBottonLeft.AppearanceCell.Options.UseTextOptions = true;
            this.gridBottonLeft.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBottonLeft.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBottonLeft.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBottonLeft.Caption = "Data";
            this.gridBottonLeft.FieldName = "DataText";
            this.gridBottonLeft.Name = "gridBottonLeft";
            this.gridBottonLeft.Visible = true;
            this.gridBottonLeft.VisibleIndex = 0;
            // 
            // gridBotton1
            // 
            this.gridBotton1.Location = new System.Drawing.Point(416, 104);
            this.gridBotton1.MainView = this.bandedGridBottonView1;
            this.gridBotton1.Name = "gridBotton1";
            this.gridBotton1.Size = new System.Drawing.Size(171, 126);
            this.gridBotton1.TabIndex = 27;
            this.gridBotton1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.bandedGridBottonView1});
            // 
            // bandedGridBottonView1
            // 
            this.bandedGridBottonView1.Bands.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand5});
            this.bandedGridBottonView1.Columns.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn[] {
            this.gridColumn16,
            this.gridColumn17,
            this.gridColumn18});
            this.bandedGridBottonView1.GridControl = this.gridBotton1;
            this.bandedGridBottonView1.HorzScrollStep = 1;
            this.bandedGridBottonView1.HorzScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Never;
            this.bandedGridBottonView1.Name = "bandedGridBottonView1";
            this.bandedGridBottonView1.OptionsBehavior.Editable = false;
            this.bandedGridBottonView1.OptionsCustomization.AllowFilter = false;
            this.bandedGridBottonView1.OptionsMenu.EnableFooterMenu = false;
            this.bandedGridBottonView1.OptionsView.ShowGroupPanel = false;
            this.bandedGridBottonView1.OptionsView.ShowIndicator = false;
            this.bandedGridBottonView1.ScrollStyle = DevExpress.XtraGrid.Views.Grid.ScrollStyleFlags.None;
            this.bandedGridBottonView1.VertScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Never;
            // 
            // gridBand5
            // 
            this.gridBand5.AppearanceHeader.BackColor = System.Drawing.Color.Red;
            this.gridBand5.AppearanceHeader.Options.UseBackColor = true;
            this.gridBand5.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand5.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand5.Caption = "PTC1-PTC2@ST1 (D1)";
            this.gridBand5.Columns.Add(this.gridColumn16);
            this.gridBand5.Columns.Add(this.gridColumn17);
            this.gridBand5.Columns.Add(this.gridColumn18);
            this.gridBand5.Name = "gridBand5";
            this.gridBand5.Width = 225;
            // 
            // gridColumn16
            // 
            this.gridColumn16.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn16.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn16.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn16.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn16.Caption = "<0";
            this.gridColumn16.DisplayFormat.FormatString = "{0:N2}%";
            this.gridColumn16.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn16.FieldName = "lessZero";
            this.gridColumn16.Name = "gridColumn16";
            this.gridColumn16.Visible = true;
            // 
            // gridColumn17
            // 
            this.gridColumn17.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn17.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn17.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn17.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn17.Caption = "0";
            this.gridColumn17.DisplayFormat.FormatString = "{0:N2}%";
            this.gridColumn17.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn17.FieldName = "equalZero";
            this.gridColumn17.Name = "gridColumn17";
            this.gridColumn17.Visible = true;
            // 
            // gridColumn18
            // 
            this.gridColumn18.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn18.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn18.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn18.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn18.Caption = ">0";
            this.gridColumn18.DisplayFormat.FormatString = "{0:N2}%";
            this.gridColumn18.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn18.FieldName = "greaterZero";
            this.gridColumn18.Name = "gridColumn18";
            this.gridColumn18.Visible = true;
            // 
            // gridBotton2
            // 
            this.gridBotton2.Location = new System.Drawing.Point(598, 104);
            this.gridBotton2.MainView = this.bandedGridBottonView2;
            this.gridBotton2.Name = "gridBotton2";
            this.gridBotton2.Size = new System.Drawing.Size(171, 126);
            this.gridBotton2.TabIndex = 28;
            this.gridBotton2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.bandedGridBottonView2});
            // 
            // bandedGridBottonView2
            // 
            this.bandedGridBottonView2.Bands.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand6});
            this.bandedGridBottonView2.Columns.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn[] {
            this.bandedGridColumn1,
            this.bandedGridColumn2,
            this.bandedGridColumn3});
            this.bandedGridBottonView2.GridControl = this.gridBotton2;
            this.bandedGridBottonView2.HorzScrollStep = 1;
            this.bandedGridBottonView2.HorzScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Never;
            this.bandedGridBottonView2.Name = "bandedGridBottonView2";
            this.bandedGridBottonView2.OptionsBehavior.Editable = false;
            this.bandedGridBottonView2.OptionsCustomization.AllowFilter = false;
            this.bandedGridBottonView2.OptionsMenu.EnableFooterMenu = false;
            this.bandedGridBottonView2.OptionsView.ShowGroupPanel = false;
            this.bandedGridBottonView2.OptionsView.ShowIndicator = false;
            this.bandedGridBottonView2.ScrollStyle = DevExpress.XtraGrid.Views.Grid.ScrollStyleFlags.None;
            this.bandedGridBottonView2.VertScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Never;
            // 
            // gridBand6
            // 
            this.gridBand6.AppearanceHeader.BackColor = System.Drawing.Color.Red;
            this.gridBand6.AppearanceHeader.Options.UseBackColor = true;
            this.gridBand6.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand6.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand6.Caption = "PTC1-PTC2@ST2 (D2)";
            this.gridBand6.Columns.Add(this.bandedGridColumn1);
            this.gridBand6.Columns.Add(this.bandedGridColumn2);
            this.gridBand6.Columns.Add(this.bandedGridColumn3);
            this.gridBand6.Name = "gridBand6";
            this.gridBand6.Width = 225;
            // 
            // bandedGridColumn1
            // 
            this.bandedGridColumn1.AppearanceCell.Options.UseTextOptions = true;
            this.bandedGridColumn1.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn1.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn1.Caption = "<0";
            this.bandedGridColumn1.DisplayFormat.FormatString = "{0:N2}%";
            this.bandedGridColumn1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.bandedGridColumn1.FieldName = "lessZero";
            this.bandedGridColumn1.Name = "bandedGridColumn1";
            this.bandedGridColumn1.Visible = true;
            // 
            // bandedGridColumn2
            // 
            this.bandedGridColumn2.AppearanceCell.Options.UseTextOptions = true;
            this.bandedGridColumn2.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn2.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn2.Caption = "0";
            this.bandedGridColumn2.DisplayFormat.FormatString = "{0:N2}%";
            this.bandedGridColumn2.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.bandedGridColumn2.FieldName = "equalZero";
            this.bandedGridColumn2.Name = "bandedGridColumn2";
            this.bandedGridColumn2.Visible = true;
            // 
            // bandedGridColumn3
            // 
            this.bandedGridColumn3.AppearanceCell.Options.UseTextOptions = true;
            this.bandedGridColumn3.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn3.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn3.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn3.Caption = ">0";
            this.bandedGridColumn3.DisplayFormat.FormatString = "{0:N2}%";
            this.bandedGridColumn3.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.bandedGridColumn3.FieldName = "greaterZero";
            this.bandedGridColumn3.Name = "bandedGridColumn3";
            this.bandedGridColumn3.Visible = true;
            // 
            // gridBotton3
            // 
            this.gridBotton3.Location = new System.Drawing.Point(780, 104);
            this.gridBotton3.MainView = this.bandedGridBottonView3;
            this.gridBotton3.Name = "gridBotton3";
            this.gridBotton3.Size = new System.Drawing.Size(171, 126);
            this.gridBotton3.TabIndex = 29;
            this.gridBotton3.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.bandedGridBottonView3});
            // 
            // bandedGridBottonView3
            // 
            this.bandedGridBottonView3.Bands.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand7});
            this.bandedGridBottonView3.Columns.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn[] {
            this.bandedGridColumn4,
            this.bandedGridColumn5,
            this.bandedGridColumn6});
            this.bandedGridBottonView3.GridControl = this.gridBotton3;
            this.bandedGridBottonView3.HorzScrollStep = 1;
            this.bandedGridBottonView3.HorzScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Never;
            this.bandedGridBottonView3.Name = "bandedGridBottonView3";
            this.bandedGridBottonView3.OptionsBehavior.Editable = false;
            this.bandedGridBottonView3.OptionsCustomization.AllowFilter = false;
            this.bandedGridBottonView3.OptionsMenu.EnableFooterMenu = false;
            this.bandedGridBottonView3.OptionsView.ShowGroupPanel = false;
            this.bandedGridBottonView3.OptionsView.ShowIndicator = false;
            this.bandedGridBottonView3.ScrollStyle = DevExpress.XtraGrid.Views.Grid.ScrollStyleFlags.None;
            this.bandedGridBottonView3.VertScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Never;
            // 
            // gridBand7
            // 
            this.gridBand7.AppearanceHeader.BackColor = System.Drawing.Color.Red;
            this.gridBand7.AppearanceHeader.Options.UseBackColor = true;
            this.gridBand7.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand7.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand7.Caption = "DiffPTC1-DiffPTC2 (D21)";
            this.gridBand7.Columns.Add(this.bandedGridColumn4);
            this.gridBand7.Columns.Add(this.bandedGridColumn5);
            this.gridBand7.Columns.Add(this.bandedGridColumn6);
            this.gridBand7.Name = "gridBand7";
            this.gridBand7.Width = 225;
            // 
            // bandedGridColumn4
            // 
            this.bandedGridColumn4.AppearanceCell.Options.UseTextOptions = true;
            this.bandedGridColumn4.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn4.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn4.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn4.Caption = "<0";
            this.bandedGridColumn4.DisplayFormat.FormatString = "{0:N2}%";
            this.bandedGridColumn4.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.bandedGridColumn4.FieldName = "lessZero";
            this.bandedGridColumn4.Name = "bandedGridColumn4";
            this.bandedGridColumn4.Visible = true;
            // 
            // bandedGridColumn5
            // 
            this.bandedGridColumn5.AppearanceCell.Options.UseTextOptions = true;
            this.bandedGridColumn5.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn5.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn5.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn5.Caption = "0";
            this.bandedGridColumn5.DisplayFormat.FormatString = "{0:N2}%";
            this.bandedGridColumn5.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.bandedGridColumn5.FieldName = "equalZero";
            this.bandedGridColumn5.Name = "bandedGridColumn5";
            this.bandedGridColumn5.Visible = true;
            // 
            // bandedGridColumn6
            // 
            this.bandedGridColumn6.AppearanceCell.Options.UseTextOptions = true;
            this.bandedGridColumn6.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn6.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn6.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn6.Caption = ">0";
            this.bandedGridColumn6.DisplayFormat.FormatString = "{0:N2}%";
            this.bandedGridColumn6.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.bandedGridColumn6.FieldName = "greaterZero";
            this.bandedGridColumn6.Name = "bandedGridColumn6";
            this.bandedGridColumn6.Visible = true;
            // 
            // gridParamsInfo
            // 
            this.gridParamsInfo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.gridParamsInfo.Location = new System.Drawing.Point(0, 0);
            this.gridParamsInfo.MainView = this.bandedGridViewParamsInfo;
            this.gridParamsInfo.Name = "gridParamsInfo";
            this.gridParamsInfo.Size = new System.Drawing.Size(1097, 98);
            this.gridParamsInfo.TabIndex = 30;
            this.gridParamsInfo.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.bandedGridViewParamsInfo});
            // 
            // bandedGridViewParamsInfo
            // 
            this.bandedGridViewParamsInfo.Bands.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand15});
            this.bandedGridViewParamsInfo.Columns.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn[] {
            this.bandedGridColumn201,
            this.bandedGridColumn202,
            this.bandedGridColumn203,
            this.bandedGridColumn204,
            this.bandedGridColumn205,
            this.bandedGridColumn206,
            this.bandedGridColumn207,
            this.bandedGridColumn208,
            this.bandedGridColumn209,
            this.bandedGridColumn210,
            this.bandedGridColumn211,
            this.bandedGridColumn212,
            this.bandedGridColumn213});
            this.bandedGridViewParamsInfo.GridControl = this.gridParamsInfo;
            this.bandedGridViewParamsInfo.Name = "bandedGridViewParamsInfo";
            this.bandedGridViewParamsInfo.OptionsBehavior.Editable = false;
            this.bandedGridViewParamsInfo.OptionsCustomization.AllowFilter = false;
            this.bandedGridViewParamsInfo.OptionsMenu.EnableFooterMenu = false;
            this.bandedGridViewParamsInfo.OptionsView.ShowGroupPanel = false;
            this.bandedGridViewParamsInfo.OptionsView.ShowIndicator = false;
            // 
            // gridBand15
            // 
            this.gridBand15.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand15.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand15.Caption = "Параметры";
            this.gridBand15.Children.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand9,
            this.gridBand10,
            this.gridBand11,
            this.gridBand8,
            this.gridBand14,
            this.gridBand12,
            this.gridBand13});
            this.gridBand15.Name = "gridBand15";
            this.gridBand15.Width = 1081;
            // 
            // gridBand9
            // 
            this.gridBand9.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand9.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand9.Caption = "Период 1";
            this.gridBand9.Columns.Add(this.bandedGridColumn201);
            this.gridBand9.Columns.Add(this.bandedGridColumn202);
            this.gridBand9.Name = "gridBand9";
            this.gridBand9.Width = 170;
            // 
            // bandedGridColumn201
            // 
            this.bandedGridColumn201.AppearanceCell.Options.UseTextOptions = true;
            this.bandedGridColumn201.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn201.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn201.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn201.Caption = "Начало";
            this.bandedGridColumn201.FieldName = "DateStart1";
            this.bandedGridColumn201.Name = "bandedGridColumn201";
            this.bandedGridColumn201.Visible = true;
            this.bandedGridColumn201.Width = 85;
            // 
            // bandedGridColumn202
            // 
            this.bandedGridColumn202.AppearanceCell.Options.UseTextOptions = true;
            this.bandedGridColumn202.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn202.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn202.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn202.Caption = "Окончание";
            this.bandedGridColumn202.FieldName = "DateEnd1";
            this.bandedGridColumn202.MinWidth = 60;
            this.bandedGridColumn202.Name = "bandedGridColumn202";
            this.bandedGridColumn202.Visible = true;
            this.bandedGridColumn202.Width = 85;
            // 
            // gridBand10
            // 
            this.gridBand10.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand10.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand10.Caption = "Период 2";
            this.gridBand10.Columns.Add(this.bandedGridColumn203);
            this.gridBand10.Columns.Add(this.bandedGridColumn204);
            this.gridBand10.Name = "gridBand10";
            this.gridBand10.Width = 170;
            // 
            // bandedGridColumn203
            // 
            this.bandedGridColumn203.AppearanceCell.Options.UseTextOptions = true;
            this.bandedGridColumn203.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn203.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn203.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn203.Caption = "Начала";
            this.bandedGridColumn203.FieldName = "DateStart2";
            this.bandedGridColumn203.MinWidth = 60;
            this.bandedGridColumn203.Name = "bandedGridColumn203";
            this.bandedGridColumn203.Visible = true;
            this.bandedGridColumn203.Width = 85;
            // 
            // bandedGridColumn204
            // 
            this.bandedGridColumn204.AppearanceCell.Options.UseTextOptions = true;
            this.bandedGridColumn204.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn204.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn204.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn204.Caption = "Окончание";
            this.bandedGridColumn204.FieldName = "DateEnd2";
            this.bandedGridColumn204.MinWidth = 60;
            this.bandedGridColumn204.Name = "bandedGridColumn204";
            this.bandedGridColumn204.Visible = true;
            this.bandedGridColumn204.Width = 85;
            // 
            // gridBand11
            // 
            this.gridBand11.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand11.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand11.Columns.Add(this.bandedGridColumn205);
            this.gridBand11.Name = "gridBand11";
            this.gridBand11.Width = 59;
            // 
            // bandedGridColumn205
            // 
            this.bandedGridColumn205.AppearanceCell.Options.UseTextOptions = true;
            this.bandedGridColumn205.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn205.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn205.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn205.Caption = "Step";
            this.bandedGridColumn205.FieldName = "Step";
            this.bandedGridColumn205.MinWidth = 55;
            this.bandedGridColumn205.Name = "bandedGridColumn205";
            this.bandedGridColumn205.Visible = true;
            this.bandedGridColumn205.Width = 59;
            // 
            // gridBand8
            // 
            this.gridBand8.Columns.Add(this.bandedGridColumn206);
            this.gridBand8.Columns.Add(this.bandedGridColumn207);
            this.gridBand8.Name = "gridBand8";
            this.gridBand8.Width = 138;
            // 
            // bandedGridColumn206
            // 
            this.bandedGridColumn206.AppearanceCell.Options.UseTextOptions = true;
            this.bandedGridColumn206.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn206.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn206.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn206.Caption = "Тип канала";
            this.bandedGridColumn206.FieldName = "ChanelType";
            this.bandedGridColumn206.MinWidth = 60;
            this.bandedGridColumn206.Name = "bandedGridColumn206";
            this.bandedGridColumn206.Visible = true;
            this.bandedGridColumn206.Width = 78;
            // 
            // bandedGridColumn207
            // 
            this.bandedGridColumn207.AppearanceCell.Options.UseTextOptions = true;
            this.bandedGridColumn207.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn207.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn207.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn207.Caption = "Персонал";
            this.bandedGridColumn207.FieldName = "Staff";
            this.bandedGridColumn207.MinWidth = 60;
            this.bandedGridColumn207.Name = "bandedGridColumn207";
            this.bandedGridColumn207.Visible = true;
            this.bandedGridColumn207.Width = 60;
            // 
            // gridBand14
            // 
            this.gridBand14.Columns.Add(this.bandedGridColumn208);
            this.gridBand14.Name = "gridBand14";
            this.gridBand14.Width = 85;
            // 
            // bandedGridColumn208
            // 
            this.bandedGridColumn208.AppearanceCell.Options.UseTextOptions = true;
            this.bandedGridColumn208.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn208.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn208.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn208.Caption = "Клиент";
            this.bandedGridColumn208.FieldName = "Client";
            this.bandedGridColumn208.MinWidth = 60;
            this.bandedGridColumn208.Name = "bandedGridColumn208";
            this.bandedGridColumn208.Visible = true;
            this.bandedGridColumn208.Width = 85;
            // 
            // gridBand12
            // 
            this.gridBand12.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand12.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand12.Caption = "Продукты";
            this.gridBand12.Columns.Add(this.bandedGridColumn209);
            this.gridBand12.Columns.Add(this.bandedGridColumn210);
            this.gridBand12.Name = "gridBand12";
            this.gridBand12.Width = 245;
            // 
            // bandedGridColumn209
            // 
            this.bandedGridColumn209.AppearanceCell.Options.UseTextOptions = true;
            this.bandedGridColumn209.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn209.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn209.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn209.Caption = "SKU 1";
            this.bandedGridColumn209.FieldName = "SKU1";
            this.bandedGridColumn209.MinWidth = 60;
            this.bandedGridColumn209.Name = "bandedGridColumn209";
            this.bandedGridColumn209.Visible = true;
            this.bandedGridColumn209.Width = 120;
            // 
            // bandedGridColumn210
            // 
            this.bandedGridColumn210.AppearanceCell.Options.UseTextOptions = true;
            this.bandedGridColumn210.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn210.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn210.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn210.Caption = "SKU 2";
            this.bandedGridColumn210.FieldName = "SKU2";
            this.bandedGridColumn210.MinWidth = 60;
            this.bandedGridColumn210.Name = "bandedGridColumn210";
            this.bandedGridColumn210.Visible = true;
            this.bandedGridColumn210.Width = 125;
            // 
            // gridBand13
            // 
            this.gridBand13.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand13.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand13.Caption = "Форма ТТ";
            this.gridBand13.Columns.Add(this.bandedGridColumn211);
            this.gridBand13.Columns.Add(this.bandedGridColumn212);
            this.gridBand13.Columns.Add(this.bandedGridColumn213);
            this.gridBand13.Name = "gridBand13";
            this.gridBand13.Width = 214;
            // 
            // bandedGridColumn211
            // 
            this.bandedGridColumn211.AppearanceCell.Options.UseTextOptions = true;
            this.bandedGridColumn211.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn211.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn211.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn211.Caption = "Тип";
            this.bandedGridColumn211.FieldName = "Type";
            this.bandedGridColumn211.MinWidth = 60;
            this.bandedGridColumn211.Name = "bandedGridColumn211";
            this.bandedGridColumn211.Visible = true;
            this.bandedGridColumn211.Width = 67;
            // 
            // bandedGridColumn212
            // 
            this.bandedGridColumn212.AppearanceCell.Options.UseTextOptions = true;
            this.bandedGridColumn212.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn212.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn212.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn212.Caption = "Подтип";
            this.bandedGridColumn212.FieldName = "SubType";
            this.bandedGridColumn212.MinWidth = 60;
            this.bandedGridColumn212.Name = "bandedGridColumn212";
            this.bandedGridColumn212.Visible = true;
            this.bandedGridColumn212.Width = 67;
            // 
            // bandedGridColumn213
            // 
            this.bandedGridColumn213.AppearanceCell.Options.UseTextOptions = true;
            this.bandedGridColumn213.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn213.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn213.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn213.Caption = "Категория";
            this.bandedGridColumn213.FieldName = "Category";
            this.bandedGridColumn213.MinWidth = 60;
            this.bandedGridColumn213.Name = "bandedGridColumn213";
            this.bandedGridColumn213.Visible = true;
            this.bandedGridColumn213.Width = 80;
            // 
            // StatisticTab
            // 
            this.AutoScroll = true;
            this.Controls.Add(this.gridParamsInfo);
            this.Controls.Add(this.gridBotton3);
            this.Controls.Add(this.gridBotton2);
            this.Controls.Add(this.gridBotton1);
            this.Controls.Add(this.gridControlData);
            this.Controls.Add(this.gridControlTop);
            this.Controls.Add(this.gridControlTopLeft);
            this.Name = "StatisticTab";
            this.Size = new System.Drawing.Size(1097, 749);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlTopLeft)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlTop)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bandedGridTopView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridBotton1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bandedGridBottonView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridBotton2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bandedGridBottonView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridBotton3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bandedGridBottonView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridParamsInfo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bandedGridViewParamsInfo)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gridControlTopLeft;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.GridControl gridControlTop;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridView bandedGridTopView;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn gridColumn3;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn gridColumn4;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn gridColumn5;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn gridColumn6;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn gridColumn7;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn gridColumn8;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn gridColumn9;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn gridColumn10;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn gridColumn11;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn gridColumn12;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn gridColumn13;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn gridColumn14;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn gridColumn15;
        private DevExpress.XtraGrid.GridControl gridControlData;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewData;
        private DevExpress.XtraGrid.Columns.GridColumn gridBottonLeft;
        private DevExpress.XtraGrid.GridControl gridBotton1;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridView bandedGridBottonView1;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand5;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn gridColumn16;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn gridColumn17;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn gridColumn18;
        private DevExpress.XtraGrid.GridControl gridBotton2;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridView bandedGridBottonView2;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand6;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn1;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn2;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn3;
        private DevExpress.XtraGrid.GridControl gridBotton3;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridView bandedGridBottonView3;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn4;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn5;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn6;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand7;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand1;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand2;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand3;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand4;
        private DevExpress.XtraGrid.GridControl gridParamsInfo;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridView bandedGridViewParamsInfo;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn201;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn202;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn203;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn204;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn205;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn206;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn208;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn207;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn209;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn210;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn211;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn212;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn213;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand15;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand9;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand10;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand11;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand8;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand14;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand12;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand13;
    }
}
