﻿namespace WccpReporting.RPTCAnalysis.UserControls
{
    partial class ChartsTab
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraCharts.XYDiagram xyDiagram1 = new DevExpress.XtraCharts.XYDiagram();
            DevExpress.XtraCharts.Series series1 = new DevExpress.XtraCharts.Series();
            DevExpress.XtraCharts.SideBySideBarSeriesLabel sideBySideBarSeriesLabel1 = new DevExpress.XtraCharts.SideBySideBarSeriesLabel();
            DevExpress.XtraCharts.SideBySideBarSeriesView sideBySideBarSeriesView1 = new DevExpress.XtraCharts.SideBySideBarSeriesView();
            DevExpress.XtraCharts.Series series2 = new DevExpress.XtraCharts.Series();
            DevExpress.XtraCharts.SideBySideBarSeriesLabel sideBySideBarSeriesLabel2 = new DevExpress.XtraCharts.SideBySideBarSeriesLabel();
            DevExpress.XtraCharts.SideBySideBarSeriesView sideBySideBarSeriesView2 = new DevExpress.XtraCharts.SideBySideBarSeriesView();
            DevExpress.XtraCharts.SideBySideBarSeriesLabel sideBySideBarSeriesLabel3 = new DevExpress.XtraCharts.SideBySideBarSeriesLabel();
            DevExpress.XtraCharts.XYDiagram xyDiagram2 = new DevExpress.XtraCharts.XYDiagram();
            DevExpress.XtraCharts.Series series3 = new DevExpress.XtraCharts.Series();
            DevExpress.XtraCharts.SideBySideBarSeriesLabel sideBySideBarSeriesLabel4 = new DevExpress.XtraCharts.SideBySideBarSeriesLabel();
            DevExpress.XtraCharts.SideBySideBarSeriesView sideBySideBarSeriesView3 = new DevExpress.XtraCharts.SideBySideBarSeriesView();
            DevExpress.XtraCharts.Series series4 = new DevExpress.XtraCharts.Series();
            DevExpress.XtraCharts.SideBySideBarSeriesLabel sideBySideBarSeriesLabel5 = new DevExpress.XtraCharts.SideBySideBarSeriesLabel();
            DevExpress.XtraCharts.SideBySideBarSeriesView sideBySideBarSeriesView4 = new DevExpress.XtraCharts.SideBySideBarSeriesView();
            DevExpress.XtraCharts.SideBySideBarSeriesLabel sideBySideBarSeriesLabel6 = new DevExpress.XtraCharts.SideBySideBarSeriesLabel();
            DevExpress.XtraCharts.XYDiagram xyDiagram3 = new DevExpress.XtraCharts.XYDiagram();
            DevExpress.XtraCharts.Series series5 = new DevExpress.XtraCharts.Series();
            DevExpress.XtraCharts.PointSeriesLabel pointSeriesLabel1 = new DevExpress.XtraCharts.PointSeriesLabel();
            DevExpress.XtraCharts.LineSeriesView lineSeriesView1 = new DevExpress.XtraCharts.LineSeriesView();
            DevExpress.XtraCharts.Series series6 = new DevExpress.XtraCharts.Series();
            DevExpress.XtraCharts.PointSeriesLabel pointSeriesLabel2 = new DevExpress.XtraCharts.PointSeriesLabel();
            DevExpress.XtraCharts.LineSeriesView lineSeriesView2 = new DevExpress.XtraCharts.LineSeriesView();
            DevExpress.XtraCharts.PointSeriesLabel pointSeriesLabel3 = new DevExpress.XtraCharts.PointSeriesLabel();
            DevExpress.XtraCharts.LineSeriesView lineSeriesView3 = new DevExpress.XtraCharts.LineSeriesView();
            DevExpress.XtraCharts.XYDiagram xyDiagram4 = new DevExpress.XtraCharts.XYDiagram();
            DevExpress.XtraCharts.Series series7 = new DevExpress.XtraCharts.Series();
            DevExpress.XtraCharts.SideBySideBarSeriesLabel sideBySideBarSeriesLabel7 = new DevExpress.XtraCharts.SideBySideBarSeriesLabel();
            DevExpress.XtraCharts.SideBySideBarSeriesView sideBySideBarSeriesView5 = new DevExpress.XtraCharts.SideBySideBarSeriesView();
            DevExpress.XtraCharts.Series series8 = new DevExpress.XtraCharts.Series();
            DevExpress.XtraCharts.SideBySideBarSeriesLabel sideBySideBarSeriesLabel8 = new DevExpress.XtraCharts.SideBySideBarSeriesLabel();
            DevExpress.XtraCharts.SideBySideBarSeriesView sideBySideBarSeriesView6 = new DevExpress.XtraCharts.SideBySideBarSeriesView();
            DevExpress.XtraCharts.SideBySideBarSeriesLabel sideBySideBarSeriesLabel9 = new DevExpress.XtraCharts.SideBySideBarSeriesLabel();
            DevExpress.XtraCharts.XYDiagram xyDiagram5 = new DevExpress.XtraCharts.XYDiagram();
            DevExpress.XtraCharts.Series series9 = new DevExpress.XtraCharts.Series();
            DevExpress.XtraCharts.PointSeriesLabel pointSeriesLabel4 = new DevExpress.XtraCharts.PointSeriesLabel();
            DevExpress.XtraCharts.LineSeriesView lineSeriesView4 = new DevExpress.XtraCharts.LineSeriesView();
            DevExpress.XtraCharts.Series series10 = new DevExpress.XtraCharts.Series();
            DevExpress.XtraCharts.PointSeriesLabel pointSeriesLabel5 = new DevExpress.XtraCharts.PointSeriesLabel();
            DevExpress.XtraCharts.LineSeriesView lineSeriesView5 = new DevExpress.XtraCharts.LineSeriesView();
            DevExpress.XtraCharts.PointSeriesLabel pointSeriesLabel6 = new DevExpress.XtraCharts.PointSeriesLabel();
            DevExpress.XtraCharts.LineSeriesView lineSeriesView6 = new DevExpress.XtraCharts.LineSeriesView();
            DevExpress.XtraCharts.XYDiagram xyDiagram6 = new DevExpress.XtraCharts.XYDiagram();
            DevExpress.XtraCharts.Series series11 = new DevExpress.XtraCharts.Series();
            DevExpress.XtraCharts.SideBySideBarSeriesLabel sideBySideBarSeriesLabel10 = new DevExpress.XtraCharts.SideBySideBarSeriesLabel();
            DevExpress.XtraCharts.SideBySideBarSeriesLabel sideBySideBarSeriesLabel11 = new DevExpress.XtraCharts.SideBySideBarSeriesLabel();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.textBox7 = new System.Windows.Forms.TextBox();
            this.textBox8 = new System.Windows.Forms.TextBox();
            this.textBox9 = new System.Windows.Forms.TextBox();
            this.textBox10 = new System.Windows.Forms.TextBox();
            this.textBox11 = new System.Windows.Forms.TextBox();
            this.txtStep12 = new System.Windows.Forms.TextBox();
            this.txtMinPTC = new System.Windows.Forms.TextBox();
            this.txtMaxPTC = new System.Windows.Forms.TextBox();
            this.txtOUSSKU1 = new System.Windows.Forms.TextBox();
            this.txtOUSSKU2 = new System.Windows.Forms.TextBox();
            this.txtMinSKU1 = new System.Windows.Forms.TextBox();
            this.txtMinSKU2 = new System.Windows.Forms.TextBox();
            this.txtMaxSKU1 = new System.Windows.Forms.TextBox();
            this.txtMaxSKU2 = new System.Windows.Forms.TextBox();
            this.hScrollBar1 = new DevExpress.XtraEditors.HScrollBar();
            this.vScrollBar1 = new DevExpress.XtraEditors.VScrollBar();
            this.chartControl1 = new DevExpress.XtraCharts.ChartControl();
            this.chartControl2 = new DevExpress.XtraCharts.ChartControl();
            this.label1 = new System.Windows.Forms.Label();
            this.hScrollBar1Label = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.hScrollBar2Label = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.chartControl3 = new DevExpress.XtraCharts.ChartControl();
            this.chartControl4 = new DevExpress.XtraCharts.ChartControl();
            this.vScrollBar2 = new DevExpress.XtraEditors.VScrollBar();
            this.hScrollBar2 = new DevExpress.XtraEditors.HScrollBar();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.hScrollBar3Label = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.chartControl5 = new DevExpress.XtraCharts.ChartControl();
            this.vScrollBar3 = new DevExpress.XtraEditors.VScrollBar();
            this.hScrollBar3 = new DevExpress.XtraEditors.HScrollBar();
            this.chartControl6 = new DevExpress.XtraCharts.ChartControl();
            this.vScrollBar1Label = new System.Windows.Forms.Label();
            this.txtMax_D2 = new System.Windows.Forms.TextBox();
            this.txtMax_D1 = new System.Windows.Forms.TextBox();
            this.txtMin_D2 = new System.Windows.Forms.TextBox();
            this.txtMin_D1 = new System.Windows.Forms.TextBox();
            this.txtOUS_D2 = new System.Windows.Forms.TextBox();
            this.txtOUS_D1 = new System.Windows.Forms.TextBox();
            this.txtMaxDi = new System.Windows.Forms.TextBox();
            this.txtMinDi = new System.Windows.Forms.TextBox();
            this.txtStep34 = new System.Windows.Forms.TextBox();
            this.textBox23 = new System.Windows.Forms.TextBox();
            this.textBox24 = new System.Windows.Forms.TextBox();
            this.textBox25 = new System.Windows.Forms.TextBox();
            this.textBox26 = new System.Windows.Forms.TextBox();
            this.textBox27 = new System.Windows.Forms.TextBox();
            this.textBox28 = new System.Windows.Forms.TextBox();
            this.textBox29 = new System.Windows.Forms.TextBox();
            this.textBox30 = new System.Windows.Forms.TextBox();
            this.textBox31 = new System.Windows.Forms.TextBox();
            this.textBox32 = new System.Windows.Forms.TextBox();
            this.textBox33 = new System.Windows.Forms.TextBox();
            this.vScrollBar2Label = new System.Windows.Forms.Label();
            this.txtMax_D21Sample = new System.Windows.Forms.TextBox();
            this.txtMin_D21Sample = new System.Windows.Forms.TextBox();
            this.txtOUS_D21 = new System.Windows.Forms.TextBox();
            this.txtMax_D21 = new System.Windows.Forms.TextBox();
            this.txtMin_D21 = new System.Windows.Forms.TextBox();
            this.txtStep56 = new System.Windows.Forms.TextBox();
            this.textBox34 = new System.Windows.Forms.TextBox();
            this.textBox35 = new System.Windows.Forms.TextBox();
            this.textBox36 = new System.Windows.Forms.TextBox();
            this.textBox38 = new System.Windows.Forms.TextBox();
            this.textBox39 = new System.Windows.Forms.TextBox();
            this.textBox42 = new System.Windows.Forms.TextBox();
            this.textBox43 = new System.Windows.Forms.TextBox();
            this.textBox44 = new System.Windows.Forms.TextBox();
            this.vScrollBar3Label = new System.Windows.Forms.Label();
            this.toolTipController1 = new DevExpress.Utils.ToolTipController();
            ((System.ComponentModel.ISupportInitialize)(this.chartControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(xyDiagram1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(series1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(sideBySideBarSeriesLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(sideBySideBarSeriesView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(series2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(sideBySideBarSeriesLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(sideBySideBarSeriesView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(sideBySideBarSeriesLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(xyDiagram2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(series3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(sideBySideBarSeriesLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(sideBySideBarSeriesView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(series4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(sideBySideBarSeriesLabel5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(sideBySideBarSeriesView4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(sideBySideBarSeriesLabel6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartControl3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(xyDiagram3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(series5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(pointSeriesLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(lineSeriesView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(series6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(pointSeriesLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(lineSeriesView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(pointSeriesLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(lineSeriesView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartControl4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(xyDiagram4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(series7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(sideBySideBarSeriesLabel7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(sideBySideBarSeriesView5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(series8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(sideBySideBarSeriesLabel8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(sideBySideBarSeriesView6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(sideBySideBarSeriesLabel9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartControl5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(xyDiagram5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(series9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(pointSeriesLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(lineSeriesView4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(series10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(pointSeriesLabel5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(lineSeriesView5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(pointSeriesLabel6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(lineSeriesView6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartControl6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(xyDiagram6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(series11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(sideBySideBarSeriesLabel10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(sideBySideBarSeriesLabel11)).BeginInit();
            this.SuspendLayout();
            // 
            // textBox2
            // 
            this.textBox2.BackColor = System.Drawing.Color.Red;
            this.textBox2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox2.ForeColor = System.Drawing.SystemColors.Window;
            this.textBox2.Location = new System.Drawing.Point(16, 69);
            this.textBox2.MinimumSize = new System.Drawing.Size(180, 18);
            this.textBox2.Name = "textBox2";
            this.textBox2.ReadOnly = true;
            this.textBox2.Size = new System.Drawing.Size(180, 23);
            this.textBox2.TabIndex = 2;
            this.textBox2.Text = "Chart info";
            this.textBox2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBox2.WordWrap = false;
            // 
            // textBox1
            // 
            this.textBox1.BackColor = System.Drawing.Color.Red;
            this.textBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox1.ForeColor = System.Drawing.SystemColors.Window;
            this.textBox1.Location = new System.Drawing.Point(16, 201);
            this.textBox1.MaximumSize = new System.Drawing.Size(2, 15);
            this.textBox1.MinimumSize = new System.Drawing.Size(180, 20);
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.Size = new System.Drawing.Size(180, 20);
            this.textBox1.TabIndex = 3;
            this.textBox1.Text = "Sample info";
            this.textBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBox1.WordWrap = false;
            // 
            // textBox3
            // 
            this.textBox3.BackColor = System.Drawing.Color.Chocolate;
            this.textBox3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox3.ForeColor = System.Drawing.SystemColors.WindowText;
            this.textBox3.Location = new System.Drawing.Point(16, 91);
            this.textBox3.MinimumSize = new System.Drawing.Size(100, 18);
            this.textBox3.Name = "textBox3";
            this.textBox3.ReadOnly = true;
            this.textBox3.Size = new System.Drawing.Size(100, 23);
            this.textBox3.TabIndex = 4;
            this.textBox3.Text = "Step";
            this.textBox3.WordWrap = false;
            // 
            // textBox4
            // 
            this.textBox4.BackColor = System.Drawing.Color.Chocolate;
            this.textBox4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox4.ForeColor = System.Drawing.SystemColors.WindowText;
            this.textBox4.Location = new System.Drawing.Point(16, 286);
            this.textBox4.MinimumSize = new System.Drawing.Size(100, 18);
            this.textBox4.Name = "textBox4";
            this.textBox4.ReadOnly = true;
            this.textBox4.Size = new System.Drawing.Size(100, 23);
            this.textBox4.TabIndex = 5;
            this.textBox4.Text = "Max SKU2";
            this.textBox4.WordWrap = false;
            // 
            // textBox5
            // 
            this.textBox5.BackColor = System.Drawing.Color.Chocolate;
            this.textBox5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox5.ForeColor = System.Drawing.SystemColors.WindowText;
            this.textBox5.Location = new System.Drawing.Point(16, 264);
            this.textBox5.MinimumSize = new System.Drawing.Size(100, 18);
            this.textBox5.Name = "textBox5";
            this.textBox5.ReadOnly = true;
            this.textBox5.Size = new System.Drawing.Size(100, 23);
            this.textBox5.TabIndex = 6;
            this.textBox5.Text = "Max SKU1";
            this.textBox5.WordWrap = false;
            // 
            // textBox6
            // 
            this.textBox6.BackColor = System.Drawing.Color.Chocolate;
            this.textBox6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox6.ForeColor = System.Drawing.SystemColors.WindowText;
            this.textBox6.Location = new System.Drawing.Point(16, 242);
            this.textBox6.MinimumSize = new System.Drawing.Size(100, 18);
            this.textBox6.Name = "textBox6";
            this.textBox6.ReadOnly = true;
            this.textBox6.Size = new System.Drawing.Size(100, 23);
            this.textBox6.TabIndex = 7;
            this.textBox6.Text = "Min SKU2";
            this.textBox6.WordWrap = false;
            // 
            // textBox7
            // 
            this.textBox7.BackColor = System.Drawing.Color.Chocolate;
            this.textBox7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox7.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox7.ForeColor = System.Drawing.SystemColors.WindowText;
            this.textBox7.Location = new System.Drawing.Point(16, 220);
            this.textBox7.MinimumSize = new System.Drawing.Size(100, 18);
            this.textBox7.Name = "textBox7";
            this.textBox7.ReadOnly = true;
            this.textBox7.Size = new System.Drawing.Size(100, 23);
            this.textBox7.TabIndex = 8;
            this.textBox7.Text = "Min SKU1";
            this.textBox7.WordWrap = false;
            // 
            // textBox8
            // 
            this.textBox8.BackColor = System.Drawing.Color.Chocolate;
            this.textBox8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox8.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox8.ForeColor = System.Drawing.SystemColors.WindowText;
            this.textBox8.Location = new System.Drawing.Point(16, 179);
            this.textBox8.MinimumSize = new System.Drawing.Size(100, 18);
            this.textBox8.Name = "textBox8";
            this.textBox8.ReadOnly = true;
            this.textBox8.Size = new System.Drawing.Size(100, 23);
            this.textBox8.TabIndex = 9;
            this.textBox8.Text = "OUS SKU2";
            this.textBox8.WordWrap = false;
            // 
            // textBox9
            // 
            this.textBox9.BackColor = System.Drawing.Color.Chocolate;
            this.textBox9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox9.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox9.ForeColor = System.Drawing.SystemColors.WindowText;
            this.textBox9.Location = new System.Drawing.Point(16, 157);
            this.textBox9.MinimumSize = new System.Drawing.Size(100, 18);
            this.textBox9.Name = "textBox9";
            this.textBox9.ReadOnly = true;
            this.textBox9.Size = new System.Drawing.Size(100, 23);
            this.textBox9.TabIndex = 10;
            this.textBox9.Text = "OUS SKU1";
            this.textBox9.WordWrap = false;
            // 
            // textBox10
            // 
            this.textBox10.BackColor = System.Drawing.Color.Chocolate;
            this.textBox10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox10.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox10.ForeColor = System.Drawing.SystemColors.WindowText;
            this.textBox10.Location = new System.Drawing.Point(16, 135);
            this.textBox10.MinimumSize = new System.Drawing.Size(100, 18);
            this.textBox10.Name = "textBox10";
            this.textBox10.ReadOnly = true;
            this.textBox10.Size = new System.Drawing.Size(100, 23);
            this.textBox10.TabIndex = 11;
            this.textBox10.Text = "Max PTC";
            this.textBox10.WordWrap = false;
            // 
            // textBox11
            // 
            this.textBox11.BackColor = System.Drawing.Color.Chocolate;
            this.textBox11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox11.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox11.ForeColor = System.Drawing.SystemColors.WindowText;
            this.textBox11.Location = new System.Drawing.Point(16, 113);
            this.textBox11.MinimumSize = new System.Drawing.Size(100, 18);
            this.textBox11.Name = "textBox11";
            this.textBox11.ReadOnly = true;
            this.textBox11.Size = new System.Drawing.Size(100, 23);
            this.textBox11.TabIndex = 12;
            this.textBox11.Text = "Min PTC";
            this.textBox11.WordWrap = false;
            // 
            // txtStep12
            // 
            this.txtStep12.BackColor = System.Drawing.Color.DarkGray;
            this.txtStep12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtStep12.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.txtStep12.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txtStep12.Location = new System.Drawing.Point(115, 91);
            this.txtStep12.MinimumSize = new System.Drawing.Size(81, 18);
            this.txtStep12.Name = "txtStep12";
            this.txtStep12.ReadOnly = true;
            this.txtStep12.Size = new System.Drawing.Size(81, 23);
            this.txtStep12.TabIndex = 13;
            this.txtStep12.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtStep12.WordWrap = false;
            // 
            // txtMinPTC
            // 
            this.txtMinPTC.BackColor = System.Drawing.Color.DarkGray;
            this.txtMinPTC.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtMinPTC.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.txtMinPTC.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txtMinPTC.Location = new System.Drawing.Point(115, 113);
            this.txtMinPTC.MinimumSize = new System.Drawing.Size(81, 18);
            this.txtMinPTC.Name = "txtMinPTC";
            this.txtMinPTC.ReadOnly = true;
            this.txtMinPTC.Size = new System.Drawing.Size(81, 23);
            this.txtMinPTC.TabIndex = 14;
            this.txtMinPTC.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMinPTC.WordWrap = false;
            // 
            // txtMaxPTC
            // 
            this.txtMaxPTC.BackColor = System.Drawing.Color.DarkGray;
            this.txtMaxPTC.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtMaxPTC.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.txtMaxPTC.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txtMaxPTC.Location = new System.Drawing.Point(115, 135);
            this.txtMaxPTC.MinimumSize = new System.Drawing.Size(81, 18);
            this.txtMaxPTC.Name = "txtMaxPTC";
            this.txtMaxPTC.ReadOnly = true;
            this.txtMaxPTC.Size = new System.Drawing.Size(81, 23);
            this.txtMaxPTC.TabIndex = 15;
            this.txtMaxPTC.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMaxPTC.WordWrap = false;
            // 
            // txtOUSSKU1
            // 
            this.txtOUSSKU1.BackColor = System.Drawing.Color.DarkGray;
            this.txtOUSSKU1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtOUSSKU1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.txtOUSSKU1.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txtOUSSKU1.Location = new System.Drawing.Point(115, 157);
            this.txtOUSSKU1.MinimumSize = new System.Drawing.Size(81, 18);
            this.txtOUSSKU1.Name = "txtOUSSKU1";
            this.txtOUSSKU1.ReadOnly = true;
            this.txtOUSSKU1.Size = new System.Drawing.Size(81, 23);
            this.txtOUSSKU1.TabIndex = 16;
            this.txtOUSSKU1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtOUSSKU1.WordWrap = false;
            // 
            // txtOUSSKU2
            // 
            this.txtOUSSKU2.BackColor = System.Drawing.Color.DarkGray;
            this.txtOUSSKU2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtOUSSKU2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.txtOUSSKU2.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txtOUSSKU2.Location = new System.Drawing.Point(115, 179);
            this.txtOUSSKU2.MinimumSize = new System.Drawing.Size(81, 18);
            this.txtOUSSKU2.Name = "txtOUSSKU2";
            this.txtOUSSKU2.ReadOnly = true;
            this.txtOUSSKU2.Size = new System.Drawing.Size(81, 23);
            this.txtOUSSKU2.TabIndex = 17;
            this.txtOUSSKU2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtOUSSKU2.WordWrap = false;
            // 
            // txtMinSKU1
            // 
            this.txtMinSKU1.BackColor = System.Drawing.Color.DarkGray;
            this.txtMinSKU1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtMinSKU1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.txtMinSKU1.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txtMinSKU1.Location = new System.Drawing.Point(115, 220);
            this.txtMinSKU1.MinimumSize = new System.Drawing.Size(81, 18);
            this.txtMinSKU1.Name = "txtMinSKU1";
            this.txtMinSKU1.ReadOnly = true;
            this.txtMinSKU1.Size = new System.Drawing.Size(81, 23);
            this.txtMinSKU1.TabIndex = 18;
            this.txtMinSKU1.Text = "Step";
            this.txtMinSKU1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMinSKU1.WordWrap = false;
            // 
            // txtMinSKU2
            // 
            this.txtMinSKU2.BackColor = System.Drawing.Color.DarkGray;
            this.txtMinSKU2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtMinSKU2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.txtMinSKU2.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txtMinSKU2.Location = new System.Drawing.Point(115, 242);
            this.txtMinSKU2.MinimumSize = new System.Drawing.Size(81, 18);
            this.txtMinSKU2.Name = "txtMinSKU2";
            this.txtMinSKU2.ReadOnly = true;
            this.txtMinSKU2.Size = new System.Drawing.Size(81, 23);
            this.txtMinSKU2.TabIndex = 19;
            this.txtMinSKU2.Text = "Step";
            this.txtMinSKU2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMinSKU2.WordWrap = false;
            // 
            // txtMaxSKU1
            // 
            this.txtMaxSKU1.BackColor = System.Drawing.Color.DarkGray;
            this.txtMaxSKU1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtMaxSKU1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.txtMaxSKU1.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txtMaxSKU1.Location = new System.Drawing.Point(115, 264);
            this.txtMaxSKU1.MinimumSize = new System.Drawing.Size(81, 18);
            this.txtMaxSKU1.Name = "txtMaxSKU1";
            this.txtMaxSKU1.ReadOnly = true;
            this.txtMaxSKU1.Size = new System.Drawing.Size(81, 23);
            this.txtMaxSKU1.TabIndex = 20;
            this.txtMaxSKU1.Text = "Step";
            this.txtMaxSKU1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMaxSKU1.WordWrap = false;
            // 
            // txtMaxSKU2
            // 
            this.txtMaxSKU2.BackColor = System.Drawing.Color.DarkGray;
            this.txtMaxSKU2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtMaxSKU2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.txtMaxSKU2.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txtMaxSKU2.Location = new System.Drawing.Point(115, 286);
            this.txtMaxSKU2.MinimumSize = new System.Drawing.Size(81, 18);
            this.txtMaxSKU2.Name = "txtMaxSKU2";
            this.txtMaxSKU2.ReadOnly = true;
            this.txtMaxSKU2.Size = new System.Drawing.Size(81, 23);
            this.txtMaxSKU2.TabIndex = 21;
            this.txtMaxSKU2.Text = "Step";
            this.txtMaxSKU2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMaxSKU2.WordWrap = false;
            // 
            // hScrollBar1
            // 
            this.hScrollBar1.LargeChange = 1;
            this.hScrollBar1.Location = new System.Drawing.Point(295, 35);
            this.hScrollBar1.Maximum = 101;
            this.hScrollBar1.Minimum = 1;
            this.hScrollBar1.Name = "hScrollBar1";
            this.hScrollBar1.Size = new System.Drawing.Size(732, 30);
            this.hScrollBar1.TabIndex = 25;
            this.hScrollBar1.Value = 1;
            this.hScrollBar1.ValueChanged += new System.EventHandler(this.hScrollBar1_ValueChanged);
            // 
            // vScrollBar1
            // 
            this.vScrollBar1.LargeChange = 1;
            this.vScrollBar1.Location = new System.Drawing.Point(638, 94);
            this.vScrollBar1.Maximum = 51;
            this.vScrollBar1.Minimum = 1;
            this.vScrollBar1.Name = "vScrollBar1";
            this.vScrollBar1.Size = new System.Drawing.Size(44, 223);
            this.vScrollBar1.TabIndex = 26;
            this.vScrollBar1.Value = 51;
            this.vScrollBar1.ValueChanged += new System.EventHandler(this.vScrollBar1_ValueChanged);
            // 
            // chartControl1
            // 
            xyDiagram1.AxisX.Label.Angle = 270;
            xyDiagram1.AxisX.Tickmarks.MinorVisible = false;
            xyDiagram1.AxisX.Title.Text = "Axis of arguments";
            xyDiagram1.AxisX.VisibleInPanesSerializable = "-1";
            xyDiagram1.AxisX.WholeRange.AutoSideMargins = true;
            xyDiagram1.AxisY.Label.TextPattern = "{V:N1}%";
            xyDiagram1.AxisY.MinorCount = 1;
            xyDiagram1.AxisY.Tickmarks.MinorLength = 1;
            xyDiagram1.AxisY.Tickmarks.MinorVisible = false;
            xyDiagram1.AxisY.Title.Text = "Axis of values";
            xyDiagram1.AxisY.VisibleInPanesSerializable = "-1";
            xyDiagram1.AxisY.WholeRange.AutoSideMargins = true;
            this.chartControl1.Diagram = xyDiagram1;
            this.chartControl1.EmptyChartText.Text = "";
            this.chartControl1.Legend.AlignmentHorizontal = DevExpress.XtraCharts.LegendAlignmentHorizontal.Center;
            this.chartControl1.Legend.AlignmentVertical = DevExpress.XtraCharts.LegendAlignmentVertical.BottomOutside;
            this.chartControl1.Legend.Direction = DevExpress.XtraCharts.LegendDirection.LeftToRight;
            this.chartControl1.Legend.EquallySpacedItems = false;
            this.chartControl1.Location = new System.Drawing.Point(240, 64);
            this.chartControl1.Name = "chartControl1";
            this.chartControl1.PaletteName = "Concourse";
            series1.ArgumentScaleType = DevExpress.XtraCharts.ScaleType.Numerical;
            sideBySideBarSeriesLabel1.Border.Visibility = DevExpress.Utils.DefaultBoolean.False;
            sideBySideBarSeriesLabel1.LineVisibility = DevExpress.Utils.DefaultBoolean.False;
            sideBySideBarSeriesLabel1.ResolveOverlappingMode = DevExpress.XtraCharts.ResolveOverlappingMode.HideOverlapped;
            sideBySideBarSeriesLabel1.TextPattern = "{V:N2}%";
            series1.Label = sideBySideBarSeriesLabel1;
            series1.Name = "ST1SKU1POC%";
            sideBySideBarSeriesView1.BarWidth = 0.1D;
            series1.View = sideBySideBarSeriesView1;
            series2.ArgumentScaleType = DevExpress.XtraCharts.ScaleType.Numerical;
            sideBySideBarSeriesLabel2.Border.Visibility = DevExpress.Utils.DefaultBoolean.False;
            sideBySideBarSeriesLabel2.LineVisibility = DevExpress.Utils.DefaultBoolean.False;
            sideBySideBarSeriesLabel2.ResolveOverlappingMode = DevExpress.XtraCharts.ResolveOverlappingMode.HideOverlapped;
            sideBySideBarSeriesLabel2.TextPattern = "{V:N2}%";
            series2.Label = sideBySideBarSeriesLabel2;
            series2.Name = "ST2SKU1POC%";
            sideBySideBarSeriesView2.BarWidth = 0.1D;
            series2.View = sideBySideBarSeriesView2;
            this.chartControl1.SeriesSerializable = new DevExpress.XtraCharts.Series[] {
        series1,
        series2};
            sideBySideBarSeriesLabel3.LineVisibility = DevExpress.Utils.DefaultBoolean.True;
            this.chartControl1.SeriesTemplate.Label = sideBySideBarSeriesLabel3;
            this.chartControl1.Size = new System.Drawing.Size(400, 270);
            this.chartControl1.SmallChartText.Text = "Increase the chart\'s size,\r\nto view its layout.\r\n    ";
            this.chartControl1.TabIndex = 27;
            this.chartControl1.ObjectHotTracked += new DevExpress.XtraCharts.HotTrackEventHandler(this.chartControl_ObjectHotTracked);
            // 
            // chartControl2
            // 
            xyDiagram2.AxisX.Label.Angle = 270;
            xyDiagram2.AxisX.Tickmarks.MinorVisible = false;
            xyDiagram2.AxisX.Title.Text = "Axis of arguments";
            xyDiagram2.AxisX.VisibleInPanesSerializable = "-1";
            xyDiagram2.AxisX.WholeRange.AutoSideMargins = true;
            xyDiagram2.AxisY.Label.TextPattern = "{V:N1}%";
            xyDiagram2.AxisY.Tickmarks.MinorVisible = false;
            xyDiagram2.AxisY.Title.Text = "Axis of values";
            xyDiagram2.AxisY.VisibleInPanesSerializable = "-1";
            xyDiagram2.AxisY.WholeRange.AutoSideMargins = true;
            this.chartControl2.Diagram = xyDiagram2;
            this.chartControl2.EmptyChartText.Text = "";
            this.chartControl2.Legend.AlignmentHorizontal = DevExpress.XtraCharts.LegendAlignmentHorizontal.Center;
            this.chartControl2.Legend.AlignmentVertical = DevExpress.XtraCharts.LegendAlignmentVertical.BottomOutside;
            this.chartControl2.Legend.Direction = DevExpress.XtraCharts.LegendDirection.LeftToRight;
            this.chartControl2.Legend.EquallySpacedItems = false;
            this.chartControl2.Location = new System.Drawing.Point(682, 64);
            this.chartControl2.Name = "chartControl2";
            this.chartControl2.PaletteName = "Concourse";
            series3.ArgumentScaleType = DevExpress.XtraCharts.ScaleType.Numerical;
            sideBySideBarSeriesLabel4.Border.Visibility = DevExpress.Utils.DefaultBoolean.False;
            sideBySideBarSeriesLabel4.LineVisibility = DevExpress.Utils.DefaultBoolean.False;
            sideBySideBarSeriesLabel4.ResolveOverlappingMode = DevExpress.XtraCharts.ResolveOverlappingMode.HideOverlapped;
            sideBySideBarSeriesLabel4.TextPattern = "{V:N2}%";
            series3.Label = sideBySideBarSeriesLabel4;
            series3.Name = "ST2SKU1POC%";
            sideBySideBarSeriesView3.BarWidth = 0.1D;
            series3.View = sideBySideBarSeriesView3;
            series4.ArgumentScaleType = DevExpress.XtraCharts.ScaleType.Numerical;
            sideBySideBarSeriesLabel5.Border.Visibility = DevExpress.Utils.DefaultBoolean.False;
            sideBySideBarSeriesLabel5.LineVisibility = DevExpress.Utils.DefaultBoolean.False;
            sideBySideBarSeriesLabel5.ResolveOverlappingMode = DevExpress.XtraCharts.ResolveOverlappingMode.HideOverlapped;
            sideBySideBarSeriesLabel5.TextPattern = "{V:N2}%";
            series4.Label = sideBySideBarSeriesLabel5;
            series4.Name = "ST2SKU2POC%";
            sideBySideBarSeriesView4.BarWidth = 0.1D;
            series4.View = sideBySideBarSeriesView4;
            this.chartControl2.SeriesSerializable = new DevExpress.XtraCharts.Series[] {
        series3,
        series4};
            sideBySideBarSeriesLabel6.LineVisibility = DevExpress.Utils.DefaultBoolean.True;
            this.chartControl2.SeriesTemplate.Label = sideBySideBarSeriesLabel6;
            this.chartControl2.Size = new System.Drawing.Size(400, 270);
            this.chartControl2.SmallChartText.Text = "Increase the chart\'s size,\r\nto view its layout.\r\n    ";
            this.chartControl2.TabIndex = 28;
            this.chartControl2.ObjectHotTracked += new DevExpress.XtraCharts.HotTrackEventHandler(this.chartControl_ObjectHotTracked);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Gold;
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(240, 35);
            this.label1.MinimumSize = new System.Drawing.Size(56, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(56, 30);
            this.label1.TabIndex = 32;
            this.label1.Text = "Scroll";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // hScrollBar1Label
            // 
            this.hScrollBar1Label.AutoSize = true;
            this.hScrollBar1Label.BackColor = System.Drawing.Color.Gold;
            this.hScrollBar1Label.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.hScrollBar1Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.hScrollBar1Label.Location = new System.Drawing.Point(1026, 35);
            this.hScrollBar1Label.MinimumSize = new System.Drawing.Size(56, 30);
            this.hScrollBar1Label.Name = "hScrollBar1Label";
            this.hScrollBar1Label.Size = new System.Drawing.Size(56, 30);
            this.hScrollBar1Label.TabIndex = 33;
            this.hScrollBar1Label.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Gold;
            this.label3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(639, 64);
            this.label3.MinimumSize = new System.Drawing.Size(40, 31);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(46, 31);
            this.label3.TabIndex = 34;
            this.label3.Text = "Zoom";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Red;
            this.label4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label4.Location = new System.Drawing.Point(240, 5);
            this.label4.MinimumSize = new System.Drawing.Size(400, 30);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(400, 30);
            this.label4.TabIndex = 35;
            this.label4.Text = "1) PTC SKU1 distibution by # of POCs : Stage1 vs Stage 2";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Red;
            this.label5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label5.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label5.Location = new System.Drawing.Point(682, 5);
            this.label5.MinimumSize = new System.Drawing.Size(400, 30);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(400, 30);
            this.label5.TabIndex = 36;
            this.label5.Text = "2) PTC distibution by # of POCs  @ Stage2: SKU1 vs SKU2";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Red;
            this.label6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label6.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label6.Location = new System.Drawing.Point(682, 333);
            this.label6.MinimumSize = new System.Drawing.Size(400, 30);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(400, 30);
            this.label6.TabIndex = 45;
            this.label6.Text = "4) VOL SKU1 by POCs @ Stage2 vs RPTC";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Red;
            this.label7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label7.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label7.Location = new System.Drawing.Point(240, 333);
            this.label7.MinimumSize = new System.Drawing.Size(400, 30);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(400, 30);
            this.label7.TabIndex = 44;
            this.label7.Text = "3) RPTC distribution by # of POCs: Stage1 vs Stage2";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Gold;
            this.label8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label8.Location = new System.Drawing.Point(639, 392);
            this.label8.MinimumSize = new System.Drawing.Size(40, 31);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(46, 31);
            this.label8.TabIndex = 43;
            this.label8.Text = "Zoom";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // hScrollBar2Label
            // 
            this.hScrollBar2Label.AutoSize = true;
            this.hScrollBar2Label.BackColor = System.Drawing.Color.Gold;
            this.hScrollBar2Label.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.hScrollBar2Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.hScrollBar2Label.Location = new System.Drawing.Point(1026, 363);
            this.hScrollBar2Label.MinimumSize = new System.Drawing.Size(56, 30);
            this.hScrollBar2Label.Name = "hScrollBar2Label";
            this.hScrollBar2Label.Size = new System.Drawing.Size(56, 30);
            this.hScrollBar2Label.TabIndex = 42;
            this.hScrollBar2Label.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Gold;
            this.label10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label10.Location = new System.Drawing.Point(240, 363);
            this.label10.MinimumSize = new System.Drawing.Size(56, 30);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(56, 30);
            this.label10.TabIndex = 41;
            this.label10.Text = "Scroll";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // chartControl3
            // 
            xyDiagram3.AxisX.Label.Angle = 270;
            xyDiagram3.AxisX.Title.Text = "Axis of arguments";
            xyDiagram3.AxisX.VisibleInPanesSerializable = "-1";
            xyDiagram3.AxisX.WholeRange.AutoSideMargins = true;
            xyDiagram3.AxisY.Label.TextPattern = "{V:N1}";
            xyDiagram3.AxisY.Title.Text = "Axis of values";
            xyDiagram3.AxisY.VisibleInPanesSerializable = "-1";
            xyDiagram3.AxisY.WholeRange.AutoSideMargins = true;
            this.chartControl3.Diagram = xyDiagram3;
            this.chartControl3.EmptyChartText.Text = "";
            this.chartControl3.Legend.AlignmentHorizontal = DevExpress.XtraCharts.LegendAlignmentHorizontal.Right;
            this.chartControl3.Legend.Direction = DevExpress.XtraCharts.LegendDirection.LeftToRight;
            this.chartControl3.Legend.EquallySpacedItems = false;
            this.chartControl3.Location = new System.Drawing.Point(682, 392);
            this.chartControl3.Name = "chartControl3";
            this.chartControl3.PaletteName = "Oriel";
            series5.ArgumentScaleType = DevExpress.XtraCharts.ScaleType.Numerical;
            pointSeriesLabel1.LineVisibility = DevExpress.Utils.DefaultBoolean.True;
            series5.Label = pointSeriesLabel1;
            series5.LabelsVisibility = DevExpress.Utils.DefaultBoolean.False;
            series5.Name = "Series1";
            series5.ShowInLegend = false;
            lineSeriesView1.LineMarkerOptions.Size = 3;
            series5.View = lineSeriesView1;
            series6.ArgumentScaleType = DevExpress.XtraCharts.ScaleType.Numerical;
            pointSeriesLabel2.LineVisibility = DevExpress.Utils.DefaultBoolean.True;
            series6.Label = pointSeriesLabel2;
            series6.LabelsVisibility = DevExpress.Utils.DefaultBoolean.False;
            series6.LegendText = "R2 = 37%";
            series6.Name = "Series2";
            series6.ShowInLegend = false;
            lineSeriesView2.LineStyle.DashStyle = DevExpress.XtraCharts.DashStyle.Dash;
            lineSeriesView2.LineStyle.Thickness = 1;
            lineSeriesView2.MarkerVisibility = DevExpress.Utils.DefaultBoolean.False;
            series6.View = lineSeriesView2;
            this.chartControl3.SeriesSerializable = new DevExpress.XtraCharts.Series[] {
        series5,
        series6};
            pointSeriesLabel3.LineVisibility = DevExpress.Utils.DefaultBoolean.True;
            this.chartControl3.SeriesTemplate.Label = pointSeriesLabel3;
            this.chartControl3.SeriesTemplate.View = lineSeriesView3;
            this.chartControl3.Size = new System.Drawing.Size(400, 270);
            this.chartControl3.SmallChartText.Text = "Increase the chart\'s size,\r\nto view its layout.\r\n    ";
            this.chartControl3.TabIndex = 40;
            // 
            // chartControl4
            // 
            xyDiagram4.AxisX.Label.Angle = 270;
            xyDiagram4.AxisX.Title.Text = "Axis of arguments";
            xyDiagram4.AxisX.VisibleInPanesSerializable = "-1";
            xyDiagram4.AxisX.WholeRange.AutoSideMargins = true;
            xyDiagram4.AxisY.Label.TextPattern = "{V:N1}%";
            xyDiagram4.AxisY.Title.Text = "Axis of values";
            xyDiagram4.AxisY.VisibleInPanesSerializable = "-1";
            xyDiagram4.AxisY.WholeRange.AutoSideMargins = true;
            this.chartControl4.Diagram = xyDiagram4;
            this.chartControl4.EmptyChartText.Text = "";
            this.chartControl4.Legend.AlignmentHorizontal = DevExpress.XtraCharts.LegendAlignmentHorizontal.Center;
            this.chartControl4.Legend.AlignmentVertical = DevExpress.XtraCharts.LegendAlignmentVertical.BottomOutside;
            this.chartControl4.Legend.Direction = DevExpress.XtraCharts.LegendDirection.LeftToRight;
            this.chartControl4.Legend.EquallySpacedItems = false;
            this.chartControl4.Location = new System.Drawing.Point(240, 392);
            this.chartControl4.Name = "chartControl4";
            this.chartControl4.PaletteName = "Concourse";
            series7.ArgumentScaleType = DevExpress.XtraCharts.ScaleType.Numerical;
            sideBySideBarSeriesLabel7.Border.Visibility = DevExpress.Utils.DefaultBoolean.False;
            sideBySideBarSeriesLabel7.LineVisibility = DevExpress.Utils.DefaultBoolean.False;
            sideBySideBarSeriesLabel7.ResolveOverlappingMode = DevExpress.XtraCharts.ResolveOverlappingMode.HideOverlapped;
            sideBySideBarSeriesLabel7.TextPattern = "{V:N2}%";
            series7.Label = sideBySideBarSeriesLabel7;
            series7.Name = "D1%";
            sideBySideBarSeriesView5.BarWidth = 0.1D;
            series7.View = sideBySideBarSeriesView5;
            series8.ArgumentScaleType = DevExpress.XtraCharts.ScaleType.Numerical;
            sideBySideBarSeriesLabel8.LineVisibility = DevExpress.Utils.DefaultBoolean.True;
            sideBySideBarSeriesLabel8.ResolveOverlappingMode = DevExpress.XtraCharts.ResolveOverlappingMode.HideOverlapped;
            sideBySideBarSeriesLabel8.TextPattern = "{V:N2}%";
            series8.Label = sideBySideBarSeriesLabel8;
            series8.Name = "D2%";
            sideBySideBarSeriesView6.BarWidth = 0.1D;
            series8.View = sideBySideBarSeriesView6;
            this.chartControl4.SeriesSerializable = new DevExpress.XtraCharts.Series[] {
        series7,
        series8};
            sideBySideBarSeriesLabel9.LineVisibility = DevExpress.Utils.DefaultBoolean.True;
            this.chartControl4.SeriesTemplate.Label = sideBySideBarSeriesLabel9;
            this.chartControl4.Size = new System.Drawing.Size(400, 270);
            this.chartControl4.SmallChartText.Text = "Increase the chart\'s size,\r\nto view its layout.\r\n    ";
            this.chartControl4.TabIndex = 39;
            this.chartControl4.ObjectHotTracked += new DevExpress.XtraCharts.HotTrackEventHandler(this.chartControl_ObjectHotTracked);
            // 
            // vScrollBar2
            // 
            this.vScrollBar2.LargeChange = 1;
            this.vScrollBar2.Location = new System.Drawing.Point(638, 422);
            this.vScrollBar2.Maximum = 51;
            this.vScrollBar2.Minimum = 1;
            this.vScrollBar2.Name = "vScrollBar2";
            this.vScrollBar2.Size = new System.Drawing.Size(44, 227);
            this.vScrollBar2.TabIndex = 38;
            this.vScrollBar2.Value = 51;
            this.vScrollBar2.ValueChanged += new System.EventHandler(this.vScrollBar2_ValueChanged);
            // 
            // hScrollBar2
            // 
            this.hScrollBar2.LargeChange = 1;
            this.hScrollBar2.Location = new System.Drawing.Point(295, 363);
            this.hScrollBar2.Maximum = 101;
            this.hScrollBar2.Minimum = 1;
            this.hScrollBar2.Name = "hScrollBar2";
            this.hScrollBar2.Size = new System.Drawing.Size(732, 30);
            this.hScrollBar2.TabIndex = 37;
            this.hScrollBar2.Value = 1;
            this.hScrollBar2.ValueChanged += new System.EventHandler(this.hScrollBar2_ValueChanged);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Red;
            this.label11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label11.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label11.Location = new System.Drawing.Point(682, 661);
            this.label11.MinimumSize = new System.Drawing.Size(400, 30);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(400, 30);
            this.label11.TabIndex = 54;
            this.label11.Text = "6) SKU1 POCs share by Vol @ Stage2 vs RPTC CHANGE ";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Red;
            this.label12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label12.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label12.Location = new System.Drawing.Point(240, 661);
            this.label12.MinimumSize = new System.Drawing.Size(400, 30);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(400, 30);
            this.label12.TabIndex = 53;
            this.label12.Text = "5) RPTC CHANGE distribution by POCs #";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.Gold;
            this.label13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label13.Location = new System.Drawing.Point(638, 720);
            this.label13.MinimumSize = new System.Drawing.Size(40, 31);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(46, 31);
            this.label13.TabIndex = 52;
            this.label13.Text = "Zoom";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // hScrollBar3Label
            // 
            this.hScrollBar3Label.AutoSize = true;
            this.hScrollBar3Label.BackColor = System.Drawing.Color.Gold;
            this.hScrollBar3Label.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.hScrollBar3Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.hScrollBar3Label.Location = new System.Drawing.Point(1026, 691);
            this.hScrollBar3Label.MinimumSize = new System.Drawing.Size(56, 30);
            this.hScrollBar3Label.Name = "hScrollBar3Label";
            this.hScrollBar3Label.Size = new System.Drawing.Size(56, 30);
            this.hScrollBar3Label.TabIndex = 51;
            this.hScrollBar3Label.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.Color.Gold;
            this.label15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label15.Location = new System.Drawing.Point(240, 691);
            this.label15.MinimumSize = new System.Drawing.Size(56, 30);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(56, 30);
            this.label15.TabIndex = 50;
            this.label15.Text = "Scroll";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // chartControl5
            // 
            xyDiagram5.AxisX.Label.Angle = 270;
            xyDiagram5.AxisX.Title.Text = "Axis of arguments";
            xyDiagram5.AxisX.VisibleInPanesSerializable = "-1";
            xyDiagram5.AxisX.WholeRange.AutoSideMargins = true;
            xyDiagram5.AxisY.Label.TextPattern = "{V:G}";
            xyDiagram5.AxisY.Title.Text = "Axis of values";
            xyDiagram5.AxisY.VisibleInPanesSerializable = "-1";
            xyDiagram5.AxisY.WholeRange.AutoSideMargins = true;
            xyDiagram5.EnableAxisXZooming = true;
            xyDiagram5.EnableAxisYZooming = true;
            this.chartControl5.Diagram = xyDiagram5;
            this.chartControl5.EmptyChartText.Text = "";
            this.chartControl5.Legend.AlignmentHorizontal = DevExpress.XtraCharts.LegendAlignmentHorizontal.Center;
            this.chartControl5.Legend.AlignmentVertical = DevExpress.XtraCharts.LegendAlignmentVertical.BottomOutside;
            this.chartControl5.Legend.Direction = DevExpress.XtraCharts.LegendDirection.LeftToRight;
            this.chartControl5.Legend.EquallySpacedItems = false;
            this.chartControl5.Legend.Visible = false;
            this.chartControl5.Location = new System.Drawing.Point(682, 720);
            this.chartControl5.Name = "chartControl5";
            series9.ArgumentScaleType = DevExpress.XtraCharts.ScaleType.Numerical;
            pointSeriesLabel4.LineVisibility = DevExpress.Utils.DefaultBoolean.True;
            pointSeriesLabel4.TextPattern = "{V:N0}%";
            series9.Label = pointSeriesLabel4;
            series9.LabelsVisibility = DevExpress.Utils.DefaultBoolean.False;
            series9.Name = "ST2SKU1POC%";
            lineSeriesView4.LineMarkerOptions.Size = 3;
            series9.View = lineSeriesView4;
            series10.ArgumentScaleType = DevExpress.XtraCharts.ScaleType.Numerical;
            pointSeriesLabel5.LineVisibility = DevExpress.Utils.DefaultBoolean.True;
            series10.Label = pointSeriesLabel5;
            series10.LabelsVisibility = DevExpress.Utils.DefaultBoolean.False;
            series10.Name = "Series62";
            lineSeriesView5.LineStyle.DashStyle = DevExpress.XtraCharts.DashStyle.Dot;
            lineSeriesView5.MarkerVisibility = DevExpress.Utils.DefaultBoolean.False;
            series10.View = lineSeriesView5;
            this.chartControl5.SeriesSerializable = new DevExpress.XtraCharts.Series[] {
        series9,
        series10};
            pointSeriesLabel6.LineVisibility = DevExpress.Utils.DefaultBoolean.True;
            this.chartControl5.SeriesTemplate.Label = pointSeriesLabel6;
            this.chartControl5.SeriesTemplate.View = lineSeriesView6;
            this.chartControl5.Size = new System.Drawing.Size(400, 270);
            this.chartControl5.SmallChartText.Text = "Increase the chart\'s size,\r\nto view its layout.\r\n    ";
            this.chartControl5.TabIndex = 49;
            // 
            // vScrollBar3
            // 
            this.vScrollBar3.LargeChange = 1;
            this.vScrollBar3.Location = new System.Drawing.Point(638, 750);
            this.vScrollBar3.Maximum = 51;
            this.vScrollBar3.Minimum = 1;
            this.vScrollBar3.Name = "vScrollBar3";
            this.vScrollBar3.Size = new System.Drawing.Size(44, 223);
            this.vScrollBar3.TabIndex = 47;
            this.vScrollBar3.Value = 51;
            this.vScrollBar3.ValueChanged += new System.EventHandler(this.vScrollBar3_ValueChanged);
            // 
            // hScrollBar3
            // 
            this.hScrollBar3.LargeChange = 1;
            this.hScrollBar3.Location = new System.Drawing.Point(295, 691);
            this.hScrollBar3.Maximum = 101;
            this.hScrollBar3.Minimum = 1;
            this.hScrollBar3.Name = "hScrollBar3";
            this.hScrollBar3.Size = new System.Drawing.Size(732, 30);
            this.hScrollBar3.TabIndex = 46;
            this.hScrollBar3.Value = 1;
            this.hScrollBar3.ValueChanged += new System.EventHandler(this.hScrollBar3_ValueChanged);
            // 
            // chartControl6
            // 
            xyDiagram6.AxisX.Label.Angle = 270;
            xyDiagram6.AxisX.Title.Text = "Axis of arguments";
            xyDiagram6.AxisX.VisibleInPanesSerializable = "-1";
            xyDiagram6.AxisX.WholeRange.AutoSideMargins = true;
            xyDiagram6.AxisY.Label.TextPattern = "{V:G}";
            xyDiagram6.AxisY.Title.Text = "Axis of values";
            xyDiagram6.AxisY.VisibleInPanesSerializable = "-1";
            xyDiagram6.AxisY.WholeRange.AutoSideMargins = true;
            xyDiagram6.Margins.Left = 39;
            this.chartControl6.Diagram = xyDiagram6;
            this.chartControl6.EmptyChartText.Text = "";
            this.chartControl6.Legend.AlignmentHorizontal = DevExpress.XtraCharts.LegendAlignmentHorizontal.Center;
            this.chartControl6.Legend.AlignmentVertical = DevExpress.XtraCharts.LegendAlignmentVertical.BottomOutside;
            this.chartControl6.Legend.Direction = DevExpress.XtraCharts.LegendDirection.LeftToRight;
            this.chartControl6.Legend.EquallySpacedItems = false;
            this.chartControl6.Location = new System.Drawing.Point(240, 720);
            this.chartControl6.Name = "chartControl6";
            this.chartControl6.PaletteName = "Concourse";
            series11.ArgumentScaleType = DevExpress.XtraCharts.ScaleType.Numerical;
            sideBySideBarSeriesLabel10.Border.Visibility = DevExpress.Utils.DefaultBoolean.False;
            sideBySideBarSeriesLabel10.LineVisibility = DevExpress.Utils.DefaultBoolean.False;
            sideBySideBarSeriesLabel10.ResolveOverlappingMode = DevExpress.XtraCharts.ResolveOverlappingMode.HideOverlapped;
            sideBySideBarSeriesLabel10.TextPattern = "{V:N2}%";
            series11.Label = sideBySideBarSeriesLabel10;
            series11.Name = "D21%";
            this.chartControl6.SeriesSerializable = new DevExpress.XtraCharts.Series[] {
        series11};
            sideBySideBarSeriesLabel11.LineVisibility = DevExpress.Utils.DefaultBoolean.True;
            this.chartControl6.SeriesTemplate.Label = sideBySideBarSeriesLabel11;
            this.chartControl6.Size = new System.Drawing.Size(400, 270);
            this.chartControl6.SmallChartText.Text = "Increase the chart\'s size,\r\nto view its layout.\r\n    ";
            this.chartControl6.TabIndex = 48;
            this.chartControl6.ObjectHotTracked += new DevExpress.XtraCharts.HotTrackEventHandler(this.chartControl_ObjectHotTracked);
            // 
            // vScrollBar1Label
            // 
            this.vScrollBar1Label.BackColor = System.Drawing.Color.Gold;
            this.vScrollBar1Label.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.vScrollBar1Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.vScrollBar1Label.Location = new System.Drawing.Point(640, 317);
            this.vScrollBar1Label.Name = "vScrollBar1Label";
            this.vScrollBar1Label.Size = new System.Drawing.Size(42, 17);
            this.vScrollBar1Label.TabIndex = 56;
            // 
            // txtMax_D2
            // 
            this.txtMax_D2.BackColor = System.Drawing.Color.DarkGray;
            this.txtMax_D2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtMax_D2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.txtMax_D2.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txtMax_D2.Location = new System.Drawing.Point(116, 613);
            this.txtMax_D2.MinimumSize = new System.Drawing.Size(81, 18);
            this.txtMax_D2.Name = "txtMax_D2";
            this.txtMax_D2.ReadOnly = true;
            this.txtMax_D2.Size = new System.Drawing.Size(81, 23);
            this.txtMax_D2.TabIndex = 76;
            this.txtMax_D2.Text = "Step";
            this.txtMax_D2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMax_D2.WordWrap = false;
            // 
            // txtMax_D1
            // 
            this.txtMax_D1.BackColor = System.Drawing.Color.DarkGray;
            this.txtMax_D1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtMax_D1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.txtMax_D1.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txtMax_D1.Location = new System.Drawing.Point(116, 591);
            this.txtMax_D1.MinimumSize = new System.Drawing.Size(81, 18);
            this.txtMax_D1.Name = "txtMax_D1";
            this.txtMax_D1.ReadOnly = true;
            this.txtMax_D1.Size = new System.Drawing.Size(81, 23);
            this.txtMax_D1.TabIndex = 75;
            this.txtMax_D1.Text = "Step";
            this.txtMax_D1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMax_D1.WordWrap = false;
            // 
            // txtMin_D2
            // 
            this.txtMin_D2.BackColor = System.Drawing.Color.DarkGray;
            this.txtMin_D2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtMin_D2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.txtMin_D2.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txtMin_D2.Location = new System.Drawing.Point(116, 569);
            this.txtMin_D2.MinimumSize = new System.Drawing.Size(81, 18);
            this.txtMin_D2.Name = "txtMin_D2";
            this.txtMin_D2.ReadOnly = true;
            this.txtMin_D2.Size = new System.Drawing.Size(81, 23);
            this.txtMin_D2.TabIndex = 74;
            this.txtMin_D2.Text = "Step";
            this.txtMin_D2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMin_D2.WordWrap = false;
            // 
            // txtMin_D1
            // 
            this.txtMin_D1.BackColor = System.Drawing.Color.DarkGray;
            this.txtMin_D1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtMin_D1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.txtMin_D1.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txtMin_D1.Location = new System.Drawing.Point(116, 547);
            this.txtMin_D1.MinimumSize = new System.Drawing.Size(81, 18);
            this.txtMin_D1.Name = "txtMin_D1";
            this.txtMin_D1.ReadOnly = true;
            this.txtMin_D1.Size = new System.Drawing.Size(81, 23);
            this.txtMin_D1.TabIndex = 73;
            this.txtMin_D1.Text = "Step";
            this.txtMin_D1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMin_D1.WordWrap = false;
            // 
            // txtOUS_D2
            // 
            this.txtOUS_D2.BackColor = System.Drawing.Color.DarkGray;
            this.txtOUS_D2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtOUS_D2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.txtOUS_D2.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txtOUS_D2.Location = new System.Drawing.Point(116, 506);
            this.txtOUS_D2.MinimumSize = new System.Drawing.Size(81, 18);
            this.txtOUS_D2.Name = "txtOUS_D2";
            this.txtOUS_D2.ReadOnly = true;
            this.txtOUS_D2.Size = new System.Drawing.Size(81, 23);
            this.txtOUS_D2.TabIndex = 72;
            this.txtOUS_D2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtOUS_D2.WordWrap = false;
            // 
            // txtOUS_D1
            // 
            this.txtOUS_D1.BackColor = System.Drawing.Color.DarkGray;
            this.txtOUS_D1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtOUS_D1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.txtOUS_D1.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txtOUS_D1.Location = new System.Drawing.Point(116, 484);
            this.txtOUS_D1.MinimumSize = new System.Drawing.Size(81, 18);
            this.txtOUS_D1.Name = "txtOUS_D1";
            this.txtOUS_D1.ReadOnly = true;
            this.txtOUS_D1.Size = new System.Drawing.Size(81, 23);
            this.txtOUS_D1.TabIndex = 71;
            this.txtOUS_D1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtOUS_D1.WordWrap = false;
            // 
            // txtMaxDi
            // 
            this.txtMaxDi.BackColor = System.Drawing.Color.DarkGray;
            this.txtMaxDi.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtMaxDi.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.txtMaxDi.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txtMaxDi.Location = new System.Drawing.Point(116, 462);
            this.txtMaxDi.MinimumSize = new System.Drawing.Size(81, 18);
            this.txtMaxDi.Name = "txtMaxDi";
            this.txtMaxDi.ReadOnly = true;
            this.txtMaxDi.Size = new System.Drawing.Size(81, 23);
            this.txtMaxDi.TabIndex = 70;
            this.txtMaxDi.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMaxDi.WordWrap = false;
            // 
            // txtMinDi
            // 
            this.txtMinDi.BackColor = System.Drawing.Color.DarkGray;
            this.txtMinDi.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtMinDi.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.txtMinDi.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txtMinDi.Location = new System.Drawing.Point(116, 440);
            this.txtMinDi.MinimumSize = new System.Drawing.Size(81, 18);
            this.txtMinDi.Name = "txtMinDi";
            this.txtMinDi.ReadOnly = true;
            this.txtMinDi.Size = new System.Drawing.Size(81, 23);
            this.txtMinDi.TabIndex = 69;
            this.txtMinDi.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMinDi.WordWrap = false;
            // 
            // txtStep34
            // 
            this.txtStep34.BackColor = System.Drawing.Color.DarkGray;
            this.txtStep34.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtStep34.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.txtStep34.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txtStep34.Location = new System.Drawing.Point(116, 418);
            this.txtStep34.MinimumSize = new System.Drawing.Size(81, 18);
            this.txtStep34.Name = "txtStep34";
            this.txtStep34.ReadOnly = true;
            this.txtStep34.Size = new System.Drawing.Size(81, 23);
            this.txtStep34.TabIndex = 68;
            this.txtStep34.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtStep34.WordWrap = false;
            // 
            // textBox23
            // 
            this.textBox23.BackColor = System.Drawing.Color.Chocolate;
            this.textBox23.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox23.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox23.ForeColor = System.Drawing.SystemColors.WindowText;
            this.textBox23.Location = new System.Drawing.Point(17, 440);
            this.textBox23.MinimumSize = new System.Drawing.Size(100, 18);
            this.textBox23.Name = "textBox23";
            this.textBox23.ReadOnly = true;
            this.textBox23.Size = new System.Drawing.Size(100, 23);
            this.textBox23.TabIndex = 67;
            this.textBox23.Text = "Min Di";
            this.textBox23.WordWrap = false;
            // 
            // textBox24
            // 
            this.textBox24.BackColor = System.Drawing.Color.Chocolate;
            this.textBox24.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox24.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox24.ForeColor = System.Drawing.SystemColors.WindowText;
            this.textBox24.Location = new System.Drawing.Point(17, 462);
            this.textBox24.MinimumSize = new System.Drawing.Size(100, 18);
            this.textBox24.Name = "textBox24";
            this.textBox24.ReadOnly = true;
            this.textBox24.Size = new System.Drawing.Size(100, 23);
            this.textBox24.TabIndex = 66;
            this.textBox24.Text = "Max Di";
            this.textBox24.WordWrap = false;
            // 
            // textBox25
            // 
            this.textBox25.BackColor = System.Drawing.Color.Chocolate;
            this.textBox25.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox25.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox25.ForeColor = System.Drawing.SystemColors.WindowText;
            this.textBox25.Location = new System.Drawing.Point(17, 484);
            this.textBox25.MinimumSize = new System.Drawing.Size(100, 18);
            this.textBox25.Name = "textBox25";
            this.textBox25.ReadOnly = true;
            this.textBox25.Size = new System.Drawing.Size(100, 23);
            this.textBox25.TabIndex = 65;
            this.textBox25.Text = "OUS D1";
            this.textBox25.WordWrap = false;
            // 
            // textBox26
            // 
            this.textBox26.BackColor = System.Drawing.Color.Chocolate;
            this.textBox26.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox26.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox26.ForeColor = System.Drawing.SystemColors.WindowText;
            this.textBox26.Location = new System.Drawing.Point(17, 506);
            this.textBox26.MinimumSize = new System.Drawing.Size(100, 18);
            this.textBox26.Name = "textBox26";
            this.textBox26.ReadOnly = true;
            this.textBox26.Size = new System.Drawing.Size(100, 23);
            this.textBox26.TabIndex = 64;
            this.textBox26.Text = "OUS D2";
            this.textBox26.WordWrap = false;
            // 
            // textBox27
            // 
            this.textBox27.BackColor = System.Drawing.Color.Chocolate;
            this.textBox27.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox27.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox27.ForeColor = System.Drawing.SystemColors.WindowText;
            this.textBox27.Location = new System.Drawing.Point(17, 547);
            this.textBox27.MinimumSize = new System.Drawing.Size(100, 18);
            this.textBox27.Name = "textBox27";
            this.textBox27.ReadOnly = true;
            this.textBox27.Size = new System.Drawing.Size(100, 23);
            this.textBox27.TabIndex = 63;
            this.textBox27.Text = "Min D1";
            this.textBox27.WordWrap = false;
            // 
            // textBox28
            // 
            this.textBox28.BackColor = System.Drawing.Color.Chocolate;
            this.textBox28.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox28.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox28.ForeColor = System.Drawing.SystemColors.WindowText;
            this.textBox28.Location = new System.Drawing.Point(17, 569);
            this.textBox28.MinimumSize = new System.Drawing.Size(100, 18);
            this.textBox28.Name = "textBox28";
            this.textBox28.ReadOnly = true;
            this.textBox28.Size = new System.Drawing.Size(100, 23);
            this.textBox28.TabIndex = 62;
            this.textBox28.Text = "Min D2";
            this.textBox28.WordWrap = false;
            // 
            // textBox29
            // 
            this.textBox29.BackColor = System.Drawing.Color.Chocolate;
            this.textBox29.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox29.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox29.ForeColor = System.Drawing.SystemColors.WindowText;
            this.textBox29.Location = new System.Drawing.Point(17, 591);
            this.textBox29.MinimumSize = new System.Drawing.Size(100, 18);
            this.textBox29.Name = "textBox29";
            this.textBox29.ReadOnly = true;
            this.textBox29.Size = new System.Drawing.Size(100, 23);
            this.textBox29.TabIndex = 61;
            this.textBox29.Text = "Max D1";
            this.textBox29.WordWrap = false;
            // 
            // textBox30
            // 
            this.textBox30.BackColor = System.Drawing.Color.Chocolate;
            this.textBox30.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox30.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox30.ForeColor = System.Drawing.SystemColors.WindowText;
            this.textBox30.Location = new System.Drawing.Point(17, 613);
            this.textBox30.MinimumSize = new System.Drawing.Size(100, 18);
            this.textBox30.Name = "textBox30";
            this.textBox30.ReadOnly = true;
            this.textBox30.Size = new System.Drawing.Size(100, 23);
            this.textBox30.TabIndex = 60;
            this.textBox30.Text = "Max D2";
            this.textBox30.WordWrap = false;
            // 
            // textBox31
            // 
            this.textBox31.BackColor = System.Drawing.Color.Chocolate;
            this.textBox31.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox31.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox31.ForeColor = System.Drawing.SystemColors.WindowText;
            this.textBox31.Location = new System.Drawing.Point(17, 418);
            this.textBox31.MinimumSize = new System.Drawing.Size(100, 18);
            this.textBox31.Name = "textBox31";
            this.textBox31.ReadOnly = true;
            this.textBox31.Size = new System.Drawing.Size(100, 23);
            this.textBox31.TabIndex = 59;
            this.textBox31.Text = "Step";
            this.textBox31.WordWrap = false;
            // 
            // textBox32
            // 
            this.textBox32.BackColor = System.Drawing.Color.Red;
            this.textBox32.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox32.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox32.ForeColor = System.Drawing.SystemColors.Window;
            this.textBox32.Location = new System.Drawing.Point(17, 528);
            this.textBox32.MaximumSize = new System.Drawing.Size(2, 15);
            this.textBox32.MinimumSize = new System.Drawing.Size(180, 20);
            this.textBox32.Name = "textBox32";
            this.textBox32.ReadOnly = true;
            this.textBox32.Size = new System.Drawing.Size(180, 20);
            this.textBox32.TabIndex = 58;
            this.textBox32.Text = "Sample info";
            this.textBox32.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBox32.WordWrap = false;
            // 
            // textBox33
            // 
            this.textBox33.BackColor = System.Drawing.Color.Red;
            this.textBox33.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox33.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox33.ForeColor = System.Drawing.SystemColors.Window;
            this.textBox33.Location = new System.Drawing.Point(17, 396);
            this.textBox33.MinimumSize = new System.Drawing.Size(180, 18);
            this.textBox33.Name = "textBox33";
            this.textBox33.ReadOnly = true;
            this.textBox33.Size = new System.Drawing.Size(180, 23);
            this.textBox33.TabIndex = 57;
            this.textBox33.Text = "Chart info";
            this.textBox33.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBox33.WordWrap = false;
            // 
            // vScrollBar2Label
            // 
            this.vScrollBar2Label.BackColor = System.Drawing.Color.Gold;
            this.vScrollBar2Label.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.vScrollBar2Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.vScrollBar2Label.Location = new System.Drawing.Point(640, 645);
            this.vScrollBar2Label.Name = "vScrollBar2Label";
            this.vScrollBar2Label.Size = new System.Drawing.Size(42, 17);
            this.vScrollBar2Label.TabIndex = 77;
            // 
            // txtMax_D21Sample
            // 
            this.txtMax_D21Sample.BackColor = System.Drawing.Color.DarkGray;
            this.txtMax_D21Sample.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtMax_D21Sample.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.txtMax_D21Sample.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txtMax_D21Sample.Location = new System.Drawing.Point(119, 875);
            this.txtMax_D21Sample.MinimumSize = new System.Drawing.Size(81, 18);
            this.txtMax_D21Sample.Name = "txtMax_D21Sample";
            this.txtMax_D21Sample.ReadOnly = true;
            this.txtMax_D21Sample.Size = new System.Drawing.Size(81, 23);
            this.txtMax_D21Sample.TabIndex = 95;
            this.txtMax_D21Sample.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMax_D21Sample.WordWrap = false;
            // 
            // txtMin_D21Sample
            // 
            this.txtMin_D21Sample.BackColor = System.Drawing.Color.DarkGray;
            this.txtMin_D21Sample.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtMin_D21Sample.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.txtMin_D21Sample.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txtMin_D21Sample.Location = new System.Drawing.Point(119, 853);
            this.txtMin_D21Sample.MinimumSize = new System.Drawing.Size(81, 18);
            this.txtMin_D21Sample.Name = "txtMin_D21Sample";
            this.txtMin_D21Sample.ReadOnly = true;
            this.txtMin_D21Sample.Size = new System.Drawing.Size(81, 23);
            this.txtMin_D21Sample.TabIndex = 94;
            this.txtMin_D21Sample.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMin_D21Sample.WordWrap = false;
            // 
            // txtOUS_D21
            // 
            this.txtOUS_D21.BackColor = System.Drawing.Color.DarkGray;
            this.txtOUS_D21.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtOUS_D21.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.txtOUS_D21.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txtOUS_D21.Location = new System.Drawing.Point(119, 810);
            this.txtOUS_D21.MinimumSize = new System.Drawing.Size(81, 18);
            this.txtOUS_D21.Name = "txtOUS_D21";
            this.txtOUS_D21.ReadOnly = true;
            this.txtOUS_D21.Size = new System.Drawing.Size(81, 23);
            this.txtOUS_D21.TabIndex = 92;
            this.txtOUS_D21.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtOUS_D21.WordWrap = false;
            // 
            // txtMax_D21
            // 
            this.txtMax_D21.BackColor = System.Drawing.Color.DarkGray;
            this.txtMax_D21.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtMax_D21.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.txtMax_D21.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txtMax_D21.Location = new System.Drawing.Point(119, 788);
            this.txtMax_D21.MinimumSize = new System.Drawing.Size(81, 18);
            this.txtMax_D21.Name = "txtMax_D21";
            this.txtMax_D21.ReadOnly = true;
            this.txtMax_D21.Size = new System.Drawing.Size(81, 23);
            this.txtMax_D21.TabIndex = 91;
            this.txtMax_D21.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMax_D21.WordWrap = false;
            // 
            // txtMin_D21
            // 
            this.txtMin_D21.BackColor = System.Drawing.Color.DarkGray;
            this.txtMin_D21.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtMin_D21.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.txtMin_D21.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txtMin_D21.Location = new System.Drawing.Point(119, 766);
            this.txtMin_D21.MinimumSize = new System.Drawing.Size(81, 18);
            this.txtMin_D21.Name = "txtMin_D21";
            this.txtMin_D21.ReadOnly = true;
            this.txtMin_D21.Size = new System.Drawing.Size(81, 23);
            this.txtMin_D21.TabIndex = 90;
            this.txtMin_D21.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMin_D21.WordWrap = false;
            // 
            // txtStep56
            // 
            this.txtStep56.BackColor = System.Drawing.Color.DarkGray;
            this.txtStep56.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtStep56.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.txtStep56.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txtStep56.Location = new System.Drawing.Point(119, 744);
            this.txtStep56.MinimumSize = new System.Drawing.Size(81, 18);
            this.txtStep56.Name = "txtStep56";
            this.txtStep56.ReadOnly = true;
            this.txtStep56.Size = new System.Drawing.Size(81, 23);
            this.txtStep56.TabIndex = 89;
            this.txtStep56.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtStep56.WordWrap = false;
            // 
            // textBox34
            // 
            this.textBox34.BackColor = System.Drawing.Color.Chocolate;
            this.textBox34.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox34.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox34.ForeColor = System.Drawing.SystemColors.WindowText;
            this.textBox34.Location = new System.Drawing.Point(20, 766);
            this.textBox34.MinimumSize = new System.Drawing.Size(100, 18);
            this.textBox34.Name = "textBox34";
            this.textBox34.ReadOnly = true;
            this.textBox34.Size = new System.Drawing.Size(100, 23);
            this.textBox34.TabIndex = 88;
            this.textBox34.Text = "Min D21";
            this.textBox34.WordWrap = false;
            // 
            // textBox35
            // 
            this.textBox35.BackColor = System.Drawing.Color.Chocolate;
            this.textBox35.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox35.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox35.ForeColor = System.Drawing.SystemColors.WindowText;
            this.textBox35.Location = new System.Drawing.Point(20, 788);
            this.textBox35.MinimumSize = new System.Drawing.Size(100, 18);
            this.textBox35.Name = "textBox35";
            this.textBox35.ReadOnly = true;
            this.textBox35.Size = new System.Drawing.Size(100, 23);
            this.textBox35.TabIndex = 87;
            this.textBox35.Text = "Max D21";
            this.textBox35.WordWrap = false;
            // 
            // textBox36
            // 
            this.textBox36.BackColor = System.Drawing.Color.Chocolate;
            this.textBox36.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox36.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox36.ForeColor = System.Drawing.SystemColors.WindowText;
            this.textBox36.Location = new System.Drawing.Point(20, 810);
            this.textBox36.MinimumSize = new System.Drawing.Size(100, 18);
            this.textBox36.Name = "textBox36";
            this.textBox36.ReadOnly = true;
            this.textBox36.Size = new System.Drawing.Size(100, 23);
            this.textBox36.TabIndex = 86;
            this.textBox36.Text = "OUS D21";
            this.textBox36.WordWrap = false;
            // 
            // textBox38
            // 
            this.textBox38.BackColor = System.Drawing.Color.Chocolate;
            this.textBox38.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox38.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox38.ForeColor = System.Drawing.SystemColors.WindowText;
            this.textBox38.Location = new System.Drawing.Point(20, 853);
            this.textBox38.MinimumSize = new System.Drawing.Size(100, 18);
            this.textBox38.Name = "textBox38";
            this.textBox38.ReadOnly = true;
            this.textBox38.Size = new System.Drawing.Size(100, 23);
            this.textBox38.TabIndex = 84;
            this.textBox38.Text = "Min D21";
            this.textBox38.WordWrap = false;
            // 
            // textBox39
            // 
            this.textBox39.BackColor = System.Drawing.Color.Chocolate;
            this.textBox39.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox39.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox39.ForeColor = System.Drawing.SystemColors.WindowText;
            this.textBox39.Location = new System.Drawing.Point(20, 875);
            this.textBox39.MinimumSize = new System.Drawing.Size(100, 18);
            this.textBox39.Name = "textBox39";
            this.textBox39.ReadOnly = true;
            this.textBox39.Size = new System.Drawing.Size(100, 23);
            this.textBox39.TabIndex = 83;
            this.textBox39.Text = "MaxD21";
            this.textBox39.WordWrap = false;
            // 
            // textBox42
            // 
            this.textBox42.BackColor = System.Drawing.Color.Chocolate;
            this.textBox42.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox42.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox42.ForeColor = System.Drawing.SystemColors.WindowText;
            this.textBox42.Location = new System.Drawing.Point(20, 744);
            this.textBox42.MinimumSize = new System.Drawing.Size(100, 18);
            this.textBox42.Name = "textBox42";
            this.textBox42.ReadOnly = true;
            this.textBox42.Size = new System.Drawing.Size(100, 23);
            this.textBox42.TabIndex = 80;
            this.textBox42.Text = "Step";
            this.textBox42.WordWrap = false;
            // 
            // textBox43
            // 
            this.textBox43.BackColor = System.Drawing.Color.Red;
            this.textBox43.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox43.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox43.ForeColor = System.Drawing.SystemColors.Window;
            this.textBox43.Location = new System.Drawing.Point(20, 833);
            this.textBox43.MaximumSize = new System.Drawing.Size(2, 15);
            this.textBox43.MinimumSize = new System.Drawing.Size(180, 20);
            this.textBox43.Name = "textBox43";
            this.textBox43.ReadOnly = true;
            this.textBox43.Size = new System.Drawing.Size(180, 20);
            this.textBox43.TabIndex = 79;
            this.textBox43.Text = "Sample info";
            this.textBox43.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBox43.WordWrap = false;
            // 
            // textBox44
            // 
            this.textBox44.BackColor = System.Drawing.Color.Red;
            this.textBox44.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox44.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox44.ForeColor = System.Drawing.SystemColors.Window;
            this.textBox44.Location = new System.Drawing.Point(20, 722);
            this.textBox44.MinimumSize = new System.Drawing.Size(180, 18);
            this.textBox44.Name = "textBox44";
            this.textBox44.ReadOnly = true;
            this.textBox44.Size = new System.Drawing.Size(180, 23);
            this.textBox44.TabIndex = 78;
            this.textBox44.Text = "Chart info";
            this.textBox44.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBox44.WordWrap = false;
            // 
            // vScrollBar3Label
            // 
            this.vScrollBar3Label.BackColor = System.Drawing.Color.Gold;
            this.vScrollBar3Label.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.vScrollBar3Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.vScrollBar3Label.Location = new System.Drawing.Point(640, 973);
            this.vScrollBar3Label.Name = "vScrollBar3Label";
            this.vScrollBar3Label.Size = new System.Drawing.Size(42, 17);
            this.vScrollBar3Label.TabIndex = 96;
            // 
            // ChartsTab
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScroll = true;
            this.Controls.Add(this.vScrollBar3Label);
            this.Controls.Add(this.txtMax_D21Sample);
            this.Controls.Add(this.txtMin_D21Sample);
            this.Controls.Add(this.txtOUS_D21);
            this.Controls.Add(this.txtMax_D21);
            this.Controls.Add(this.txtMin_D21);
            this.Controls.Add(this.txtStep56);
            this.Controls.Add(this.textBox34);
            this.Controls.Add(this.textBox35);
            this.Controls.Add(this.textBox36);
            this.Controls.Add(this.textBox38);
            this.Controls.Add(this.textBox39);
            this.Controls.Add(this.textBox42);
            this.Controls.Add(this.textBox43);
            this.Controls.Add(this.textBox44);
            this.Controls.Add(this.vScrollBar2Label);
            this.Controls.Add(this.txtMax_D2);
            this.Controls.Add(this.txtMax_D1);
            this.Controls.Add(this.txtMin_D2);
            this.Controls.Add(this.txtMin_D1);
            this.Controls.Add(this.txtOUS_D2);
            this.Controls.Add(this.txtOUS_D1);
            this.Controls.Add(this.txtMaxDi);
            this.Controls.Add(this.txtMinDi);
            this.Controls.Add(this.txtStep34);
            this.Controls.Add(this.textBox23);
            this.Controls.Add(this.textBox24);
            this.Controls.Add(this.textBox25);
            this.Controls.Add(this.textBox26);
            this.Controls.Add(this.textBox27);
            this.Controls.Add(this.textBox28);
            this.Controls.Add(this.textBox29);
            this.Controls.Add(this.textBox30);
            this.Controls.Add(this.textBox31);
            this.Controls.Add(this.textBox32);
            this.Controls.Add(this.textBox33);
            this.Controls.Add(this.vScrollBar1Label);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.hScrollBar3Label);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.chartControl5);
            this.Controls.Add(this.chartControl6);
            this.Controls.Add(this.vScrollBar3);
            this.Controls.Add(this.hScrollBar3);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.hScrollBar2Label);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.chartControl3);
            this.Controls.Add(this.chartControl4);
            this.Controls.Add(this.vScrollBar2);
            this.Controls.Add(this.hScrollBar2);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.hScrollBar1Label);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.chartControl2);
            this.Controls.Add(this.chartControl1);
            this.Controls.Add(this.vScrollBar1);
            this.Controls.Add(this.hScrollBar1);
            this.Controls.Add(this.txtMaxSKU2);
            this.Controls.Add(this.txtMaxSKU1);
            this.Controls.Add(this.txtMinSKU2);
            this.Controls.Add(this.txtMinSKU1);
            this.Controls.Add(this.txtOUSSKU2);
            this.Controls.Add(this.txtOUSSKU1);
            this.Controls.Add(this.txtMaxPTC);
            this.Controls.Add(this.txtMinPTC);
            this.Controls.Add(this.txtStep12);
            this.Controls.Add(this.textBox11);
            this.Controls.Add(this.textBox10);
            this.Controls.Add(this.textBox9);
            this.Controls.Add(this.textBox8);
            this.Controls.Add(this.textBox7);
            this.Controls.Add(this.textBox6);
            this.Controls.Add(this.textBox5);
            this.Controls.Add(this.textBox4);
            this.Controls.Add(this.textBox3);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.textBox2);
            this.Name = "ChartsTab";
            this.Size = new System.Drawing.Size(716, 498);
            ((System.ComponentModel.ISupportInitialize)(xyDiagram1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(sideBySideBarSeriesLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(sideBySideBarSeriesView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(series1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(sideBySideBarSeriesLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(sideBySideBarSeriesView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(series2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(sideBySideBarSeriesLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(xyDiagram2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(sideBySideBarSeriesLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(sideBySideBarSeriesView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(series3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(sideBySideBarSeriesLabel5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(sideBySideBarSeriesView4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(series4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(sideBySideBarSeriesLabel6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(xyDiagram3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(pointSeriesLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(lineSeriesView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(series5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(pointSeriesLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(lineSeriesView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(series6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(pointSeriesLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(lineSeriesView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartControl3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(xyDiagram4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(sideBySideBarSeriesLabel7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(sideBySideBarSeriesView5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(series7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(sideBySideBarSeriesLabel8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(sideBySideBarSeriesView6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(series8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(sideBySideBarSeriesLabel9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartControl4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(xyDiagram5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(pointSeriesLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(lineSeriesView4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(series9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(pointSeriesLabel5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(lineSeriesView5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(series10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(pointSeriesLabel6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(lineSeriesView6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartControl5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(xyDiagram6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(sideBySideBarSeriesLabel10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(series11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(sideBySideBarSeriesLabel11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartControl6)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.TextBox textBox7;
        private System.Windows.Forms.TextBox textBox8;
        private System.Windows.Forms.TextBox textBox9;
        private System.Windows.Forms.TextBox textBox10;
        private System.Windows.Forms.TextBox textBox11;
        private System.Windows.Forms.TextBox txtMinSKU1;
        private System.Windows.Forms.TextBox txtMinSKU2;
        private System.Windows.Forms.TextBox txtMaxSKU1;
        private System.Windows.Forms.TextBox txtMaxSKU2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label hScrollBar1Label;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label hScrollBar2Label;
        private System.Windows.Forms.Label label10;
        private DevExpress.XtraCharts.ChartControl chartControl3;
        private DevExpress.XtraCharts.ChartControl chartControl4;
        private DevExpress.XtraEditors.VScrollBar vScrollBar2;
        private DevExpress.XtraEditors.HScrollBar hScrollBar2;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label hScrollBar3Label;
        private System.Windows.Forms.Label label15;
        private DevExpress.XtraCharts.ChartControl chartControl5;
        private DevExpress.XtraEditors.VScrollBar vScrollBar3;
        private DevExpress.XtraEditors.HScrollBar hScrollBar3;
        private DevExpress.XtraCharts.ChartControl chartControl6;
        private System.Windows.Forms.Label vScrollBar1Label;
        private System.Windows.Forms.TextBox txtMax_D2;
        private System.Windows.Forms.TextBox txtMax_D1;
        private System.Windows.Forms.TextBox txtMin_D2;
        private System.Windows.Forms.TextBox txtMin_D1;
        private System.Windows.Forms.TextBox txtOUS_D2;
        private System.Windows.Forms.TextBox txtOUS_D1;
        private System.Windows.Forms.TextBox txtMaxDi;
        private System.Windows.Forms.TextBox txtMinDi;
        private System.Windows.Forms.TextBox txtStep34;
        private System.Windows.Forms.TextBox textBox23;
        private System.Windows.Forms.TextBox textBox24;
        private System.Windows.Forms.TextBox textBox25;
        private System.Windows.Forms.TextBox textBox26;
        private System.Windows.Forms.TextBox textBox27;
        private System.Windows.Forms.TextBox textBox28;
        private System.Windows.Forms.TextBox textBox29;
        private System.Windows.Forms.TextBox textBox30;
        private System.Windows.Forms.TextBox textBox31;
        private System.Windows.Forms.TextBox textBox32;
        private System.Windows.Forms.TextBox textBox33;
        private System.Windows.Forms.Label vScrollBar2Label;
        private System.Windows.Forms.TextBox txtMax_D21Sample;
        private System.Windows.Forms.TextBox txtMin_D21Sample;
        private System.Windows.Forms.TextBox txtOUS_D21;
        private System.Windows.Forms.TextBox txtMax_D21;
        private System.Windows.Forms.TextBox txtMin_D21;
        private System.Windows.Forms.TextBox txtStep56;
        private System.Windows.Forms.TextBox textBox34;
        private System.Windows.Forms.TextBox textBox35;
        private System.Windows.Forms.TextBox textBox36;
        private System.Windows.Forms.TextBox textBox38;
        private System.Windows.Forms.TextBox textBox39;
        private System.Windows.Forms.TextBox textBox42;
        private System.Windows.Forms.TextBox textBox43;
        private System.Windows.Forms.TextBox textBox44;
        private System.Windows.Forms.TextBox txtStep12;
        private System.Windows.Forms.TextBox txtMinPTC;
        private System.Windows.Forms.TextBox txtMaxPTC;
        private System.Windows.Forms.TextBox txtOUSSKU1;
        private System.Windows.Forms.TextBox txtOUSSKU2;
        private DevExpress.XtraEditors.HScrollBar hScrollBar1;
        private DevExpress.XtraEditors.VScrollBar vScrollBar1;
        private DevExpress.XtraCharts.ChartControl chartControl1;
        private DevExpress.XtraCharts.ChartControl chartControl2;
        private System.Windows.Forms.Label vScrollBar3Label;
        private DevExpress.Utils.ToolTipController toolTipController1;

    }
}
