﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using WccpReporting.RPTCAnalysis.Localization;
using WccpReporting.RPTCAnalysis.UserControls;
using WccpReporting.RPTCAnalysis.Constants;
using DevExpress.XtraGrid.Columns;
using WccpReporting.RPTCAnalysis.Utility;
using Logica.Reports.BaseReportControl;
using DevExpress.XtraPrintingLinks;

namespace WccpReporting.RPTCAnalysis.UserControls
{
    public partial class V_Table2Tab : V_TableBaseTab
    {
        public V_Table2Tab()
            : base()
        {
            InitializeComponent();
            
            TabName = Resource.V_Table2;

            base.SourceInitializedEvent += new WccpReporting.RPTCAnalysis.UserControls.Contracts.Delegates.DataSourceChanged(V_Table2Tab_SourceInitializedEvent);
            gridView.BestFitColumns();
        }

        internal override CompositeLink ExportTo(ExportToType exportType, CompositeLink compositeLink)
        {
            return ExportGridControlTo(gridControl1, compositeLink, exportType);
        }

        void V_Table2Tab_SourceInitializedEvent(DataTable newSource)
        {
            CalcNewData(newSource);
        }

        private void CalcNewData(DataTable newSource)
        {
            if (null != newSource)
            {
                EnsureViewDataStructure(gridView);
                if (!viewData.Columns.Contains(FieldNames.TREND))
                    viewData.Columns.Add(FieldNames.TREND, typeof(decimal));
                //if (CalcDI(newSource))
                CalcDI(newSource);
                {
                    CalcTable(newSource);
                    gridControl1.DataSource = viewData.Copy();
                    gridView.BestFitColumns();

                    FireEventDataSourceChanged();
                }
            }
        }

        private void CalcTable(DataTable newSource)
        {
            int totalCount = newSource.Rows.Count;
            if (0 < totalCount)
            {
                for (int i = 0; i < viewData.Rows.Count; i++)
                {
                    DataRow dr = viewData.Rows[i];

                    // calc zone
                    decimal curDi = dr.Field<decimal>(FieldNames.Di);
                    decimal fromDi = curDi - reportSettings.Step / 2;
                    decimal toDi = fromDi + reportSettings.Step;

                    int d1Total = newSource.AsEnumerable().Where(w => w.Field<decimal?>(FieldNames.D1).HasValue).Count();
                    int d2Total = newSource.AsEnumerable().Where(w => w.Field<decimal?>(FieldNames.D2).HasValue).Count();
                    // calc D1POC
                    var selectedData = from row in newSource.AsEnumerable()
                                       where row.Field<decimal?>(FieldNames.D1).HasValue && row.Field<decimal>(FieldNames.D1) >= fromDi && row.Field<decimal>(FieldNames.D1) < toDi
                                       select row;
                    var countD1 = selectedData.Count();
                    dr[FieldNames.D1] = countD1;
                    dr[FieldNames.D1Percent] = (d1Total == 0) ? 0 : 1M * countD1 / d1Total * 100;

                    // calc D2POC
                    selectedData = from row in newSource.AsEnumerable()
                                   where row.Field<decimal?>(FieldNames.D2).HasValue && row.Field<decimal>(FieldNames.D2) >= fromDi && row.Field<decimal>(FieldNames.D2) < toDi
                                   select row;
                    var countD2 = selectedData.Count();
                    dr[FieldNames.D2] = countD2;
                    dr[FieldNames.D2Percent] = (d2Total == 0) ? 0 : 1M * countD2 / d2Total * 100;

                    // calc PTC2SKU1, PTC2SKU2, V2SKU1
                    selectedData = from row in newSource.AsEnumerable()
                                   where row.Field<decimal?>(FieldNames.D2).HasValue && row.Field<decimal>(FieldNames.D2) == curDi
                                   select row;

                    if (0 < selectedData.Count())
                    {
                        dr[FieldNames.PTC2SKU1] = selectedData.Where(w => w.Field<decimal?>(FieldNames.AVGPTC2SKU1_by_D2).HasValue).Sum(s => s.Field<decimal>(FieldNames.AVGPTC2SKU1_by_D2)) / selectedData.Count();
                        dr[FieldNames.PTC2SKU2] = selectedData.Where(w => w.Field<decimal?>(FieldNames.AVGPTC2SKU2_by_D2).HasValue).Sum(s => s.Field<decimal>(FieldNames.AVGPTC2SKU2_by_D2)) / selectedData.Count();
                    }
                    decimal v2sku1 = selectedData.Where(w => w.Field<decimal?>(FieldNames.VOL2SKU1).HasValue).Sum(s => s.Field<decimal>(FieldNames.VOL2SKU1));
                    dr[FieldNames.V2SKU1] = v2sku1;

                    // calc V1SKU1
                    decimal v1sku1 = selectedData.Where(w => w.Field<decimal?>(FieldNames.VOL1SKU1).HasValue).Sum(s => s.Field<decimal>(FieldNames.VOL1SKU1));
                    dr[FieldNames.V1SKU1] = v1sku1;

                    // calc V1SKU1perPOC, V2SKU1perPOC
                    dr[FieldNames.V1SKU1perPOC] = (countD1 != 0) ? v1sku1 / countD1 : 0;
                    dr[FieldNames.V2SKU1perPOC] = (countD2 != 0) ? v2sku1 / countD2 : 0;
                }

                // calc V1SKU1%, V2SKU1%
                var source = viewData.AsEnumerable();
                decimal sumV1SKU1 = source.Where(w => w.Field<decimal?>(FieldNames.V1SKU1).HasValue).Sum(s => s.Field<decimal>(FieldNames.V1SKU1));
                decimal sumV2SKU1 = source.Where(w => w.Field<decimal?>(FieldNames.V2SKU1).HasValue).Sum(s => s.Field<decimal>(FieldNames.V2SKU1));
                
                for (int i = 0; i < viewData.Rows.Count; i++)
                {
                    DataRow dr = viewData.Rows[i];
                    if (0 != sumV1SKU1)
                        dr[FieldNames.V1SKU1Percent] = dr.Field<decimal>(FieldNames.V1SKU1) / sumV1SKU1 * 100;
                    if (0 != sumV2SKU1)
                        dr[FieldNames.V2SKU1Percent] = dr.Field<decimal>(FieldNames.V2SKU1) / sumV2SKU1 * 100;
                }
                PolynomHelper ph = new PolynomHelper();
                ph.CalcCoeficients(viewData, FieldNames.Di, FieldNames.V2SKU1perPOC);
                for (int i = 0; i < viewData.Rows.Count; i++)
                {
                    // TREND
                    viewData.Rows[i][FieldNames.TREND] = ph.GetTrendParamValue(viewData.Rows[i].Field<decimal>(FieldNames.Di));
                }
            }
        }
        private bool CalcDI(DataTable newSource)
        {
            viewData.Rows.Clear();
            bool res = false;
            decimal? maxVal = null;
            decimal? minVal = null;
            Functions.FindMinMaxDecimal(newSource, ref maxVal, ref minVal, FieldNames.D1);
            Functions.FindMinMaxDecimal(newSource, ref maxVal, ref minVal, FieldNames.D2);

            if (null != maxVal && null != minVal)
            {
                int step1 = (int)(minVal / base.reportSettings.Step);

                minVal = base.reportSettings.Step * step1;

                int step2 = (int)(maxVal / base.reportSettings.Step) + ((maxVal % base.reportSettings.Step) > 0 ? 1 : 0);
                maxVal = base.reportSettings.Step * step2;

                int steps = (int)((maxVal - minVal) / base.reportSettings.Step);

                for (int i = 0; i <= steps; i++)
                {
                    DataRow row = viewData.NewRow();
                    row[FieldNames.Di] = minVal + (reportSettings.Step * i);
                    viewData.Rows.Add(row);
                }
                res = true;
            }
            return res;
        }
    }
}
