﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using WccpReporting.RPTCAnalysis.Localization;
using WccpReporting.RPTCAnalysis.UserControls;
using WccpReporting.RPTCAnalysis.UserControls.Contracts;
using Logica.Reports.BaseReportControl;
using DevExpress.XtraPrintingLinks;

namespace WccpReporting.RPTCAnalysis.UserControls
{
    public partial class InDataTab : BaseTabReport, IReportTab
    {
        public InDataTab()
            : base()
        {
            InitializeComponent();

            TabName = Resource.InData_Static; 
            
            base.SourceInitializedEvent += new Delegates.DataSourceChanged(InDataTab_SourceInitializedEvent);
            gridView.BestFitColumns();
        }

        internal override CompositeLink ExportTo(ExportToType exportType, CompositeLink compositeLink)
        {
            return ExportGridControlTo(gridControl1, compositeLink, exportType);
        }

        void InDataTab_SourceInitializedEvent(DataTable source)
        {
            gridControl1.DataSource = source;
            gridView.BestFitColumns();
        }
    }
}
