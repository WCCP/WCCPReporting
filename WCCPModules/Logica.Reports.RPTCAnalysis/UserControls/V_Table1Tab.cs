﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using WccpReporting.RPTCAnalysis.UserControls.Contracts;
using System.Windows.Forms;
using WccpReporting.RPTCAnalysis.Localization;
using WccpReporting.RPTCAnalysis.UserControls;
using WccpReporting.RPTCAnalysis.Constants;
using DevExpress.XtraGrid.Columns;
using System.Collections;
using WccpReporting.RPTCAnalysis.Utility;
using Logica.Reports.BaseReportControl;
using DevExpress.XtraPrintingLinks;

namespace WccpReporting.RPTCAnalysis.UserControls
{
    public partial class V_Table1Tab : V_TableBaseTab
    {
        public V_Table1Tab() : base()
        {
            InitializeComponent();

            TabName = Resource.V_Table1;

            base.SourceInitializedEvent += new Delegates.DataSourceChanged(V_Table1Tab_SourceInitializedEvent);
            gridView.BestFitColumns();
        }

        internal override CompositeLink ExportTo(ExportToType exportType, CompositeLink compositeLink)
        {
            return ExportGridControlTo(gridControl1, compositeLink, exportType);
        }

        void V_Table1Tab_SourceInitializedEvent(DataTable newSource)
        {
            CalcNewData(newSource);
        }

        private void CalcNewData(DataTable newSource)
        {
            if (null != newSource)
            {
                //gridControl1.Data
                EnsureViewDataStructure(gridView);
                //if (CalcPTC(newSource))
                CalcPTC(newSource);
                {
                    CalcTable(newSource);
                    gridControl1.DataSource = viewData.Copy();
                    gridView.BestFitColumns();

                    FireEventDataSourceChanged();
                }
            }
        }

        private void CalcTable(DataTable newSource)
        {
            int totalCount = newSource.Rows.Count;
            if (0 < totalCount)
            {
                int PTC1SKU1Total = newSource.AsEnumerable().Where(c => c.Field<decimal?>(FieldNames.PTC1SKU1).HasValue).Count();
                int PTC2SKU1Total = newSource.AsEnumerable().Where(c => c.Field<decimal?>(FieldNames.PTC2SKU1).HasValue).Count();
                int PTC1SKU2Total = newSource.AsEnumerable().Where(c => c.Field<decimal?>(FieldNames.PTC1SKU2).HasValue).Count();
                int PTC2SKU2Total = newSource.AsEnumerable().Where(c => c.Field<decimal?>(FieldNames.PTC2SKU2).HasValue).Count();
                for (int i = 0; i < viewData.Rows.Count; i++)
                {
                    DataRow dr = viewData.Rows[i];

                    // calc zone
                    decimal curPTC = dr.Field<decimal>(FieldNames.PTC);
                    decimal fromPTC = curPTC - reportSettings.Step / 2;
                    decimal toPTC = fromPTC + reportSettings.Step;

                    // calc ST1SKU1POC#, ST1SKU1POC%
                    var selectedData = from row in newSource.AsEnumerable()
                                       where row.Field<decimal?>(FieldNames.PTC1SKU1).HasValue && row.Field<decimal>(FieldNames.PTC1SKU1) >= fromPTC && row.Field<decimal>(FieldNames.PTC1SKU1) < toPTC
                                       select row;
                    var count = selectedData.Count();
                    dr[FieldNames.ST1SKU1POCSharp] = count;
                    dr[FieldNames.ST1SKU1POCPercent] = (PTC1SKU1Total == 0) ? 0 : 1M * count / PTC1SKU1Total * 100;

                    // calc ST2SKU1POC#, ST2SKU1POC%
                    selectedData = from row in newSource.AsEnumerable()
                                   where row.Field<decimal?>(FieldNames.PTC2SKU1).HasValue && row.Field<decimal>(FieldNames.PTC2SKU1) >= fromPTC && row.Field<decimal>(FieldNames.PTC2SKU1) < toPTC
                                   select row;
                    count = selectedData.Count();
                    dr[FieldNames.ST2SKU1POCSharp] = count;
                    dr[FieldNames.ST2SKU1POCPercent] = (PTC2SKU1Total == 0) ? 0 : 1M * count / PTC2SKU1Total * 100;

                    // calc ST1SKU2POC#, ST1SKU2POC%
                    selectedData = from row in newSource.AsEnumerable()
                                   where row.Field<decimal?>(FieldNames.PTC1SKU2).HasValue && row.Field<decimal>(FieldNames.PTC1SKU2) >= fromPTC && row.Field<decimal>(FieldNames.PTC1SKU2) < toPTC
                                   select row;
                    count = selectedData.Count();
                    dr[FieldNames.ST1SKU2POCSharp] = count;
                    dr[FieldNames.ST1SKU2POCPercent] = (PTC1SKU2Total == 0) ? 0 : 1M * count / PTC1SKU2Total * 100;

                    // calc ST2SKU1POC#, ST2SKU1POC%
                    selectedData = from row in newSource.AsEnumerable()
                                   where row.Field<decimal?>(FieldNames.PTC2SKU2).HasValue && row.Field<decimal>(FieldNames.PTC2SKU2) >= fromPTC && row.Field<decimal>(FieldNames.PTC2SKU2) < toPTC
                                   select row;
                    count = selectedData.Count();
                    dr[FieldNames.ST2SKU2POCSharp] = count;
                    dr[FieldNames.ST2SKU2POCPercent] = (PTC2SKU2Total == 0) ? 0 : 1M * count / PTC2SKU2Total * 100;
                }
            }
        }

        private bool CalcPTC(DataTable newSource)
        {
            viewData.Rows.Clear();
            bool res = false;
            decimal? maxVal = null;
            decimal? minVal = null;
            Functions.FindMinMaxDecimal(newSource, ref maxVal, ref minVal, FieldNames.PTC1SKU1);
            Functions.FindMinMaxDecimal(newSource, ref maxVal, ref minVal, FieldNames.PTC1SKU2);
            Functions.FindMinMaxDecimal(newSource, ref maxVal, ref minVal, FieldNames.PTC2SKU1);
            Functions.FindMinMaxDecimal(newSource, ref maxVal, ref minVal, FieldNames.PTC2SKU2);

            if (null != maxVal && null != minVal)
            {
                int step1 = (int)(minVal / base.reportSettings.Step);

                minVal = base.reportSettings.Step * step1;

                int step2 = (int)(maxVal / base.reportSettings.Step) + ((maxVal % base.reportSettings.Step) > 0 ? 1 : 0);
                maxVal = base.reportSettings.Step * step2;

                int steps = (int)((maxVal - minVal) / base.reportSettings.Step);

                for (int i = 0; i <= steps; i++)
                {
                    DataRow row = viewData.NewRow();
                    row[FieldNames.PTC] = minVal + (reportSettings.Step * i);
                    viewData.Rows.Add(row);
                }
                res = true;
            }
            return res;
        }
    }
}
