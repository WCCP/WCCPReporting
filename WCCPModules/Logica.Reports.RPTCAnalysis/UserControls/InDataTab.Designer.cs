﻿namespace WccpReporting.RPTCAnalysis.UserControls
{
    partial class InDataTab
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn101 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn102 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn103 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn104 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn105 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn106 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn107 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn108 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn109 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn110 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn111 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn112 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn113 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn114 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn115 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn116 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn117 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn118 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn119 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn120 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn121 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn122 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn123 = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView)).BeginInit();
            this.SuspendLayout();
            // 
            // gridControl1
            // 
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.Location = new System.Drawing.Point(0, 0);
            this.gridControl1.MainView = this.gridView;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(1173, 749);
            this.gridControl1.TabIndex = 26;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView});
            // 
            // gridView
            // 
            this.gridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn101,
            this.gridColumn102,
            this.gridColumn103,
            this.gridColumn104,
            this.gridColumn105,
            this.gridColumn106,
            this.gridColumn107,
            this.gridColumn108,
            this.gridColumn109,
            this.gridColumn110,
            this.gridColumn111,
            this.gridColumn112,
            this.gridColumn113,
            this.gridColumn114,
            this.gridColumn115,
            this.gridColumn116,
            this.gridColumn117,
            this.gridColumn118,
            this.gridColumn119,
            this.gridColumn120,
            this.gridColumn121,
            this.gridColumn122,
            this.gridColumn123});
            this.gridView.GridControl = this.gridControl1;
            this.gridView.Name = "gridView";
            this.gridView.OptionsBehavior.Editable = false;
            this.gridView.OptionsCustomization.AllowFilter = false;
            this.gridView.OptionsMenu.EnableFooterMenu = false;
            this.gridView.OptionsView.ColumnAutoWidth = false;
            this.gridView.OptionsView.ShowDetailButtons = false;
            this.gridView.OptionsView.ShowGroupExpandCollapseButtons = false;
            this.gridView.OptionsView.ShowGroupPanel = false;
            this.gridView.OptionsView.ShowIndicator = false;
            // 
            // gridColumn101
            // 
            this.gridColumn101.Caption = "Region_name";
            this.gridColumn101.FieldName = "Region_name";
            this.gridColumn101.Name = "gridColumn101";
            this.gridColumn101.OptionsColumn.AllowEdit = false;
            this.gridColumn101.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumn101.OptionsColumn.ReadOnly = true;
            this.gridColumn101.OptionsFilter.AllowAutoFilter = false;
            this.gridColumn101.OptionsFilter.AllowFilter = false;
            this.gridColumn101.Visible = true;
            this.gridColumn101.VisibleIndex = 0;
            // 
            // gridColumn102
            // 
            this.gridColumn102.Caption = "District_name";
            this.gridColumn102.FieldName = "District_name";
            this.gridColumn102.Name = "gridColumn102";
            this.gridColumn102.OptionsColumn.AllowEdit = false;
            this.gridColumn102.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumn102.OptionsColumn.ReadOnly = true;
            this.gridColumn102.OptionsFilter.AllowAutoFilter = false;
            this.gridColumn102.OptionsFilter.AllowFilter = false;
            this.gridColumn102.Visible = true;
            this.gridColumn102.VisibleIndex = 1;
            // 
            // gridColumn103
            // 
            this.gridColumn103.Caption = "DSM_Name";
            this.gridColumn103.FieldName = "DSM_Name";
            this.gridColumn103.Name = "gridColumn103";
            this.gridColumn103.OptionsColumn.AllowEdit = false;
            this.gridColumn103.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumn103.OptionsColumn.ReadOnly = true;
            this.gridColumn103.OptionsFilter.AllowAutoFilter = false;
            this.gridColumn103.OptionsFilter.AllowFilter = false;
            this.gridColumn103.Visible = true;
            this.gridColumn103.VisibleIndex = 2;
            // 
            // gridColumn104
            // 
            this.gridColumn104.Caption = "Cust_NAME";
            this.gridColumn104.FieldName = "Cust_NAME";
            this.gridColumn104.Name = "gridColumn104";
            this.gridColumn104.OptionsColumn.AllowEdit = false;
            this.gridColumn104.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumn104.OptionsColumn.ReadOnly = true;
            this.gridColumn104.OptionsFilter.AllowAutoFilter = false;
            this.gridColumn104.OptionsFilter.AllowFilter = false;
            this.gridColumn104.Visible = true;
            this.gridColumn104.VisibleIndex = 3;
            // 
            // gridColumn105
            // 
            this.gridColumn105.Caption = "City_name";
            this.gridColumn105.FieldName = "City_name";
            this.gridColumn105.Name = "gridColumn105";
            this.gridColumn105.OptionsColumn.AllowEdit = false;
            this.gridColumn105.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumn105.OptionsColumn.ReadOnly = true;
            this.gridColumn105.OptionsFilter.AllowAutoFilter = false;
            this.gridColumn105.OptionsFilter.AllowFilter = false;
            this.gridColumn105.Visible = true;
            this.gridColumn105.VisibleIndex = 4;
            // 
            // gridColumn106
            // 
            this.gridColumn106.Caption = "PTC1SKU1";
            this.gridColumn106.DisplayFormat.FormatString = "{0:N2}";
            this.gridColumn106.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn106.FieldName = "PTC1SKU1";
            this.gridColumn106.Name = "gridColumn106";
            this.gridColumn106.OptionsColumn.AllowEdit = false;
            this.gridColumn106.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumn106.OptionsColumn.ReadOnly = true;
            this.gridColumn106.OptionsFilter.AllowAutoFilter = false;
            this.gridColumn106.OptionsFilter.AllowFilter = false;
            this.gridColumn106.Visible = true;
            this.gridColumn106.VisibleIndex = 5;
            // 
            // gridColumn107
            // 
            this.gridColumn107.Caption = "VOL1SKU1";
            this.gridColumn107.DisplayFormat.FormatString = "{0:N2}";
            this.gridColumn107.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn107.FieldName = "VOL1SKU1";
            this.gridColumn107.Name = "gridColumn107";
            this.gridColumn107.OptionsColumn.AllowEdit = false;
            this.gridColumn107.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumn107.OptionsColumn.ReadOnly = true;
            this.gridColumn107.OptionsFilter.AllowAutoFilter = false;
            this.gridColumn107.OptionsFilter.AllowFilter = false;
            this.gridColumn107.Visible = true;
            this.gridColumn107.VisibleIndex = 6;
            // 
            // gridColumn108
            // 
            this.gridColumn108.Caption = "PTC1SKU2";
            this.gridColumn108.DisplayFormat.FormatString = "{0:N2}";
            this.gridColumn108.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn108.FieldName = "PTC1SKU2";
            this.gridColumn108.Name = "gridColumn108";
            this.gridColumn108.OptionsColumn.AllowEdit = false;
            this.gridColumn108.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumn108.OptionsColumn.ReadOnly = true;
            this.gridColumn108.OptionsFilter.AllowAutoFilter = false;
            this.gridColumn108.OptionsFilter.AllowFilter = false;
            this.gridColumn108.Visible = true;
            this.gridColumn108.VisibleIndex = 7;
            // 
            // gridColumn109
            // 
            this.gridColumn109.Caption = "PTC2SKU1";
            this.gridColumn109.DisplayFormat.FormatString = "{0:N2}";
            this.gridColumn109.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn109.FieldName = "PTC2SKU1";
            this.gridColumn109.Name = "gridColumn109";
            this.gridColumn109.OptionsColumn.AllowEdit = false;
            this.gridColumn109.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumn109.OptionsColumn.ReadOnly = true;
            this.gridColumn109.OptionsFilter.AllowAutoFilter = false;
            this.gridColumn109.OptionsFilter.AllowFilter = false;
            this.gridColumn109.Visible = true;
            this.gridColumn109.VisibleIndex = 8;
            // 
            // gridColumn110
            // 
            this.gridColumn110.Caption = "PTC2SKU2";
            this.gridColumn110.DisplayFormat.FormatString = "{0:N2}";
            this.gridColumn110.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn110.FieldName = "PTC2SKU2";
            this.gridColumn110.Name = "gridColumn110";
            this.gridColumn110.OptionsColumn.AllowEdit = false;
            this.gridColumn110.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumn110.OptionsColumn.ReadOnly = true;
            this.gridColumn110.OptionsFilter.AllowAutoFilter = false;
            this.gridColumn110.OptionsFilter.AllowFilter = false;
            this.gridColumn110.Visible = true;
            this.gridColumn110.VisibleIndex = 9;
            // 
            // gridColumn111
            // 
            this.gridColumn111.Caption = "VOL2SKU1";
            this.gridColumn111.DisplayFormat.FormatString = "{0:N2}";
            this.gridColumn111.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn111.FieldName = "VOL2SKU1";
            this.gridColumn111.Name = "gridColumn111";
            this.gridColumn111.OptionsColumn.AllowEdit = false;
            this.gridColumn111.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumn111.OptionsColumn.ReadOnly = true;
            this.gridColumn111.OptionsFilter.AllowAutoFilter = false;
            this.gridColumn111.OptionsFilter.AllowFilter = false;
            this.gridColumn111.Visible = true;
            this.gridColumn111.VisibleIndex = 10;
            // 
            // gridColumn112
            // 
            this.gridColumn112.Caption = "ТТ";
            this.gridColumn112.FieldName = "ТТ";
            this.gridColumn112.Name = "gridColumn112";
            this.gridColumn112.OptionsColumn.AllowEdit = false;
            this.gridColumn112.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumn112.OptionsColumn.ReadOnly = true;
            this.gridColumn112.OptionsFilter.AllowAutoFilter = false;
            this.gridColumn112.OptionsFilter.AllowFilter = false;
            this.gridColumn112.Visible = true;
            this.gridColumn112.VisibleIndex = 11;
            // 
            // gridColumn113
            // 
            this.gridColumn113.Caption = "OLtype_name";
            this.gridColumn113.FieldName = "OLtype_name";
            this.gridColumn113.Name = "gridColumn113";
            this.gridColumn113.OptionsColumn.AllowEdit = false;
            this.gridColumn113.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumn113.OptionsColumn.ReadOnly = true;
            this.gridColumn113.OptionsFilter.AllowAutoFilter = false;
            this.gridColumn113.OptionsFilter.AllowFilter = false;
            this.gridColumn113.Visible = true;
            this.gridColumn113.VisibleIndex = 12;
            // 
            // gridColumn114
            // 
            this.gridColumn114.Caption = "OLSubTypeName";
            this.gridColumn114.FieldName = "OLSubTypeName";
            this.gridColumn114.Name = "gridColumn114";
            this.gridColumn114.OptionsColumn.AllowEdit = false;
            this.gridColumn114.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumn114.OptionsColumn.ReadOnly = true;
            this.gridColumn114.OptionsFilter.AllowAutoFilter = false;
            this.gridColumn114.OptionsFilter.AllowFilter = false;
            this.gridColumn114.Visible = true;
            this.gridColumn114.VisibleIndex = 13;
            // 
            // gridColumn115
            // 
            this.gridColumn115.Caption = "OLName";
            this.gridColumn115.FieldName = "OLName";
            this.gridColumn115.Name = "gridColumn115";
            this.gridColumn115.OptionsColumn.AllowEdit = false;
            this.gridColumn115.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumn115.OptionsColumn.ReadOnly = true;
            this.gridColumn115.OptionsFilter.AllowAutoFilter = false;
            this.gridColumn115.OptionsFilter.AllowFilter = false;
            this.gridColumn115.Visible = true;
            this.gridColumn115.VisibleIndex = 14;
            // 
            // gridColumn116
            // 
            this.gridColumn116.Caption = "OLAddress";
            this.gridColumn116.FieldName = "OLAddress";
            this.gridColumn116.Name = "gridColumn116";
            this.gridColumn116.OptionsColumn.AllowEdit = false;
            this.gridColumn116.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumn116.OptionsColumn.ReadOnly = true;
            this.gridColumn116.OptionsFilter.AllowAutoFilter = false;
            this.gridColumn116.OptionsFilter.AllowFilter = false;
            this.gridColumn116.Visible = true;
            this.gridColumn116.VisibleIndex = 15;
            // 
            // gridColumn117
            // 
            this.gridColumn117.Caption = "D1";
            this.gridColumn117.DisplayFormat.FormatString = "{0:N2}";
            this.gridColumn117.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn117.FieldName = "D1";
            this.gridColumn117.Name = "gridColumn117";
            this.gridColumn117.OptionsColumn.AllowEdit = false;
            this.gridColumn117.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumn117.OptionsColumn.ReadOnly = true;
            this.gridColumn117.OptionsFilter.AllowAutoFilter = false;
            this.gridColumn117.OptionsFilter.AllowFilter = false;
            this.gridColumn117.Visible = true;
            this.gridColumn117.VisibleIndex = 16;
            // 
            // gridColumn118
            // 
            this.gridColumn118.Caption = "D2";
            this.gridColumn118.DisplayFormat.FormatString = "{0:N2}";
            this.gridColumn118.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn118.FieldName = "D2";
            this.gridColumn118.Name = "gridColumn118";
            this.gridColumn118.OptionsColumn.AllowEdit = false;
            this.gridColumn118.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumn118.OptionsColumn.ReadOnly = true;
            this.gridColumn118.OptionsFilter.AllowAutoFilter = false;
            this.gridColumn118.OptionsFilter.AllowFilter = false;
            this.gridColumn118.Visible = true;
            this.gridColumn118.VisibleIndex = 17;
            // 
            // gridColumn119
            // 
            this.gridColumn119.Caption = "D21";
            this.gridColumn119.DisplayFormat.FormatString = "{0:N2}";
            this.gridColumn119.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn119.FieldName = "D21";
            this.gridColumn119.Name = "gridColumn119";
            this.gridColumn119.OptionsColumn.AllowEdit = false;
            this.gridColumn119.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumn119.OptionsColumn.ReadOnly = true;
            this.gridColumn119.OptionsFilter.AllowAutoFilter = false;
            this.gridColumn119.OptionsFilter.AllowFilter = false;
            this.gridColumn119.Visible = true;
            this.gridColumn119.VisibleIndex = 18;
            // 
            // gridColumn120
            // 
            this.gridColumn120.Caption = "AVGPTC2SKU1_by_D2";
            this.gridColumn120.DisplayFormat.FormatString = "{0:N2}";
            this.gridColumn120.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn120.FieldName = "AVGPTC2SKU1_by_D2";
            this.gridColumn120.Name = "gridColumn120";
            this.gridColumn120.OptionsColumn.AllowEdit = false;
            this.gridColumn120.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumn120.OptionsColumn.ReadOnly = true;
            this.gridColumn120.OptionsFilter.AllowAutoFilter = false;
            this.gridColumn120.OptionsFilter.AllowFilter = false;
            this.gridColumn120.Visible = true;
            this.gridColumn120.VisibleIndex = 19;
            // 
            // gridColumn121
            // 
            this.gridColumn121.Caption = "AVGPTC2SKU2_by_D2";
            this.gridColumn121.DisplayFormat.FormatString = "{0:N2}";
            this.gridColumn121.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn121.FieldName = "AVGPTC2SKU2_by_D2";
            this.gridColumn121.Name = "gridColumn121";
            this.gridColumn121.OptionsColumn.AllowEdit = false;
            this.gridColumn121.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumn121.OptionsColumn.ReadOnly = true;
            this.gridColumn121.OptionsFilter.AllowAutoFilter = false;
            this.gridColumn121.OptionsFilter.AllowFilter = false;
            this.gridColumn121.Visible = true;
            this.gridColumn121.VisibleIndex = 20;
            // 
            // gridColumn122
            // 
            this.gridColumn122.Caption = "AVGPTC2SKU1_by_D21";
            this.gridColumn122.DisplayFormat.FormatString = "{0:N2}";
            this.gridColumn122.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn122.FieldName = "AVGPTC2SKU1_by_D21";
            this.gridColumn122.Name = "gridColumn122";
            this.gridColumn122.OptionsColumn.AllowEdit = false;
            this.gridColumn122.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumn122.OptionsColumn.ReadOnly = true;
            this.gridColumn122.OptionsFilter.AllowAutoFilter = false;
            this.gridColumn122.OptionsFilter.AllowFilter = false;
            this.gridColumn122.Visible = true;
            this.gridColumn122.VisibleIndex = 21;
            // 
            // gridColumn123
            // 
            this.gridColumn123.Caption = "AVGPTC2SKU2_by_D21";
            this.gridColumn123.DisplayFormat.FormatString = "{0:N2}";
            this.gridColumn123.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn123.FieldName = "AVGPTC2SKU2_by_D21";
            this.gridColumn123.Name = "gridColumn123";
            this.gridColumn123.OptionsColumn.AllowEdit = false;
            this.gridColumn123.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumn123.OptionsColumn.ReadOnly = true;
            this.gridColumn123.OptionsFilter.AllowAutoFilter = false;
            this.gridColumn123.OptionsFilter.AllowFilter = false;
            this.gridColumn123.Visible = true;
            this.gridColumn123.VisibleIndex = 22;
            // 
            // InDataTab
            // 
            //this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            //this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gridControl1);
            this.Name = "InDataTab";
            this.Size = new System.Drawing.Size(1173, 749);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn101;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn102;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn103;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn104;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn105;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn106;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn107;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn108;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn109;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn110;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn111;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn112;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn113;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn114;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn115;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn116;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn117;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn118;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn119;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn120;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn121;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn122;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn123;
    }
}
