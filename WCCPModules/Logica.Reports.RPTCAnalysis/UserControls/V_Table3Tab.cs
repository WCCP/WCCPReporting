﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using WccpReporting.RPTCAnalysis.Localization;
using WccpReporting.RPTCAnalysis.UserControls;
using DevExpress.XtraGrid.Columns;
using WccpReporting.RPTCAnalysis.Constants;
using WccpReporting.RPTCAnalysis.Utility;
using WccpReporting.RPTCAnalysis.UserControls.Contracts;
using Logica.Reports.BaseReportControl;
using DevExpress.XtraPrintingLinks;

namespace WccpReporting.RPTCAnalysis.UserControls
{
    public partial class V_Table3Tab : V_TableBaseTab
    {
        public V_Table3Tab()
            : base()
        {
            InitializeComponent();
            TabName = Resource.V_Table3;

            base.SourceInitializedEvent += new Delegates.DataSourceChanged(V_Table3Tab_SourceInitializedEvent);
            gridView.BestFitColumns();
        }

        internal override CompositeLink ExportTo(ExportToType exportType, CompositeLink compositeLink)
        {
            return ExportGridControlTo(gridControl1, compositeLink, exportType);
        }

        void V_Table3Tab_SourceInitializedEvent(DataTable newSource)
        {
            CalcNewData(newSource);
        }

        private void CalcNewData(DataTable newSource)
        {
            if (null != newSource)
            {
                EnsureViewDataStructure(gridView);
                if (!viewData.Columns.Contains(FieldNames.TREND))
                    viewData.Columns.Add(FieldNames.TREND, typeof(decimal));
                //if (CalcD21(newSource))
                CalcD21(newSource);
                {
                    CalcTable(newSource);
                    gridControl1.DataSource = viewData.Copy();
                    gridView.BestFitColumns();

                    FireEventDataSourceChanged();
                }
            }
        }

        private void CalcTable(DataTable newSource)
        {
            int totalCount = newSource.Rows.Count;
            decimal vol1sku1Total = 0M;
            decimal vol2sku1Total = 0M;
            if (0 < newSource.Rows.Count)
            {
                vol1sku1Total = newSource.AsEnumerable().Where(w => w.Field<decimal?>(FieldNames.VOL1SKU1).HasValue).Sum(s => s.Field<decimal>(FieldNames.VOL1SKU1));
                vol2sku1Total = newSource.AsEnumerable().Where(w => w.Field<decimal?>(FieldNames.VOL2SKU1).HasValue).Sum(s => s.Field<decimal>(FieldNames.VOL2SKU1));

                for (int i = 0; i < viewData.Rows.Count; i++)
                {
                    DataRow dr = viewData.Rows[i];

                    // calc zone
                    decimal curD21 = dr.Field<decimal>(FieldNames.D21);
                    decimal fromD21 = curD21 - reportSettings.Step / 2;
                    decimal toD21 = fromD21 + reportSettings.Step;

                    // calc D21POC
                    var selectedData = from row in newSource.AsEnumerable()
                                       where row.Field<decimal>(FieldNames.D21) >= fromD21 && row.Field<decimal>(FieldNames.D21) < toD21
                                       select row;
                    
                    var count = selectedData.Count();
                    dr[FieldNames.D21Sharp] = count;
                    dr[FieldNames.D21Percent] = 1.0 * count / totalCount * 100;

                    // calc PTC2SKU1, PTC2SKU2
                    selectedData = from row in newSource.AsEnumerable()
                                   where row.Field<decimal>(FieldNames.D21) == curD21
                                   select row;

                    if (0 != selectedData.Count())
                    {
                        dr[FieldNames.PTC2SKU1] = selectedData.Sum(s => (s.Field<decimal?>(FieldNames.AVGPTC2SKU1_by_D2).HasValue ?
                            s.Field<decimal>(FieldNames.AVGPTC2SKU1_by_D2) : 0)) / selectedData.Count();
                        dr[FieldNames.PTC2SKU2] = selectedData.Sum(s => s.Field<decimal?>(FieldNames.AVGPTC2SKU2_by_D2).HasValue?
                            s.Field<decimal>(FieldNames.AVGPTC2SKU2_by_D2): 0) / selectedData.Count();
                    }

                    // calc V1SKU1
                    decimal v1sku1 = selectedData.Where(s => s.Field<decimal?>(FieldNames.VOL1SKU1).HasValue).Sum(s => s.Field<decimal>(FieldNames.VOL1SKU1));
                    dr[FieldNames.V1SKU1] = v1sku1;
                    
                    // calc V2SKU1
                    decimal v2sku1 = selectedData.Where(s => s.Field<decimal?>(FieldNames.VOL2SKU1).HasValue).Sum(s => s.Field<decimal>(FieldNames.VOL2SKU1));
                    dr[FieldNames.V2SKU1] = v2sku1;

                    // calc V1SKU1perPOC
                    if (0 != vol1sku1Total)
                        dr[FieldNames.V1SKU1perPOC] = v1sku1 / vol1sku1Total;
                    // calc V2SKU1perPOC
                    if (0 != vol2sku1Total)
                        dr[FieldNames.V2SKU1perPOC] = v2sku1 / vol2sku1Total;
                }

                decimal v1sku1Total = viewData.AsEnumerable().Where(s => s.Field<decimal?>(FieldNames.V1SKU1).HasValue).Sum(s => s.Field<decimal>(FieldNames.V1SKU1));
                decimal v2sku1Total = viewData.AsEnumerable().Where(s => s.Field<decimal?>(FieldNames.V2SKU1).HasValue).Sum(s => s.Field<decimal>(FieldNames.V2SKU1));
                decimal v1sku1perpocTotal = viewData.AsEnumerable().Where(s => s.Field<decimal?>(FieldNames.V1SKU1perPOC).HasValue).Sum(s => s.Field<decimal>(FieldNames.V1SKU1perPOC));
                decimal v2sku1perpocTotal = viewData.AsEnumerable().Where(s => s.Field<decimal?>(FieldNames.V2SKU1perPOC).HasValue).Sum(s => s.Field<decimal>(FieldNames.V2SKU1perPOC));
                
                for (int i = 0; i < viewData.Rows.Count; i++)
                {
                    DataRow dr = viewData.Rows[i];
                    decimal v1sku1Tmp = 0M;
                    decimal v2sku1Tmp = 0M;
                    decimal v1sku1perpocTmp = 0M;
                    decimal v2sku1perpocTmp = 0M;
                    if (0 != v1sku1Total)
                        dr[FieldNames.V1SKU1Percent] = v1sku1Tmp = dr.Field<decimal>(FieldNames.V1SKU1) / v1sku1Total * 100;
                    if ( 0 != v2sku1Total)
                        dr[FieldNames.V2SKU1Percent] = v2sku1Tmp = dr.Field<decimal>(FieldNames.V2SKU1) / v2sku1Total * 100;
                    dr[FieldNames.Vchange] = v2sku1Tmp - v1sku1Tmp;

                    if (0 != v1sku1perpocTotal)
                        dr[FieldNames.POCShareST1SKU1] = v1sku1perpocTmp = dr.Field<decimal>(FieldNames.V1SKU1perPOC) / v1sku1perpocTotal * 100;
                    
                    if (0 != v2sku1perpocTotal)
                        dr[FieldNames.POCShareST2SKU1] = v2sku1perpocTmp = dr.Field<decimal>(FieldNames.V2SKU1perPOC) / v2sku1perpocTotal * 100;

                    dr[FieldNames.POCsShareChange] = v2sku1perpocTmp - v1sku1perpocTmp;
                }
                PolynomHelper ph = new PolynomHelper();
                ph.CalcCoeficients(viewData, FieldNames.D21, FieldNames.D21Percent);
                for (int i = 0; i < viewData.Rows.Count; i++)
                {
                    // TREND
                    viewData.Rows[i][FieldNames.TREND] = ph.GetTrendParamValue(viewData.Rows[i].Field<decimal>(FieldNames.D21));
                }
            }
        }

        private bool CalcD21(DataTable newSource)
        {
            viewData.Rows.Clear();
            bool res = false;
            decimal? maxVal = null;
            decimal? minVal = null;
            
            Functions.FindMinMaxDecimal(newSource, ref maxVal, ref minVal, FieldNames.D21);

            if (null != maxVal && null != minVal)
            {
                int step1 = (int)(minVal / base.reportSettings.Step);// -((minVal % base.reportSettings.Step) > 0 ? 1 : 0);
                minVal = base.reportSettings.Step * step1;

                int step2 = (int)(maxVal / base.reportSettings.Step) + ((maxVal % base.reportSettings.Step) > 0 ? 1 : 0);
                maxVal = base.reportSettings.Step * step2;

                int steps = (int)((maxVal - minVal) / base.reportSettings.Step);

                for (int i = 0; i <= steps; i++)
                {
                    DataRow row = viewData.NewRow();
                    row[FieldNames.D21] = minVal + (reportSettings.Step * i);
                    viewData.Rows.Add(row);
                }
                res = true;
            }
            return res;
        }
    }
}
