﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WccpReporting.RPTCAnalysis.UserControls.Contracts
{
    public class ReportSettings
    {
        private const string ALL = @"Все";
        // first period
        public DateTime DateStartPeriod1 { get; set; }
        public DateTime DateEndPeriod1 { get; set; }

        // second period
        public DateTime DateStartPeriod2 { get; set; }
        public DateTime DateEndPeriod2 { get; set; }

        // SKU section
//        public int SKU1 { get; set; }
//        public int SKU2 { get; set; }

        public string SKU1_Brand { get; set; }
        public string SKU1_Sort { get; set; }
        public string SKU1_CombiProduct { get; set; }
        public string SKU1_SimpleProduct { get; set; }

        public string SKU2_Brand { get; set; }
        public string SKU2_Sort { get; set; }
        public string SKU2_CombiProduct { get; set; }
        public string SKU2_SimpleProduct { get; set; }

        public string SKU1String { get; set; }
        public string SKU2String { get; set; }

        // region info
        public string Regions { get; set; }
        public string Districs { get; set; }
        public string Cities { get; set; }

        public string RegionsString { get; set; }
        public string DistricsString { get; set; }
        public string CitiesString { get; set; }

        public GeographicalUnitType GeographicalType { get; set; }

        public string ChanelType { get; set; }

        public string M2 { get; set; }
        public string M3 { get; set; }
        public string M4 { get; set; }

        public string M2String { get; set; }
        public string M3String { get; set; }
        public string M4String { get; set; }

        public string TypeTT { get; set; }
        public string SubTypeTT { get; set; }
        public string CategoryTT { get; set; }

        public string TypeTTString { get; set; }
        public string SubTypeTTString { get; set; }
        public string CategoryTTString { get; set; }

        public string Client { get; set; }

        public string ClientString { get; set; }

        public decimal Step { get; set; }

        public ReportSettings()
        {
            Reset();
        }

        public void Reset()
        {
            DateStartPeriod1 = DateTime.Now.AddMonths(-2).AddDays(-DateTime.Now.Day + 1); // setup date 1st of (current month - 2)
            DateEndPeriod1 = DateStartPeriod1.AddMonths(1).AddDays(-1);

            DateStartPeriod2 = DateEndPeriod1.AddDays(1);
            DateEndPeriod2 = DateStartPeriod2.AddMonths(1).AddDays(-1);

            SKU1_Brand = String.Empty;
            SKU1_Sort = String.Empty;
            SKU1_CombiProduct = String.Empty;
            SKU1_SimpleProduct = String.Empty;

            SKU2_Brand = String.Empty;
            SKU2_Sort = String.Empty;
            SKU2_CombiProduct = String.Empty;
            SKU2_SimpleProduct = String.Empty;

            SKU1String = String.Empty;
            SKU2String = String.Empty;

            Regions = String.Empty;
            Districs = String.Empty;
            Cities = String.Empty;

            RegionsString = String.Empty;
            DistricsString = String.Empty;
            CitiesString = String.Empty;

            GeographicalType = GeographicalUnitType.Region;

            ChanelType = String.Empty;

            M2 = String.Empty;
            M3 = String.Empty;
            M4 = String.Empty;

            M2String = String.Empty;
            M3String = String.Empty;
            M4String = String.Empty;

            Client = String.Empty;

            ClientString = ALL;

            TypeTT = null;
            SubTypeTT = null;
            CategoryTT = null;

            TypeTTString = ALL;
            SubTypeTTString = ALL;
            CategoryTTString = ALL;

            Step = (decimal)0.1;
        }

        public void Copy(ref ReportSettings destination)
        {
            if (null == destination)
            {
                destination = new ReportSettings();
            }

            // first period
            destination.DateStartPeriod1 = DateStartPeriod1;
            destination.DateEndPeriod1 = DateEndPeriod1;

            // second period
            destination.DateStartPeriod2 = DateStartPeriod2;
            destination.DateEndPeriod2 = DateEndPeriod2;

            destination.SKU1_Brand = SKU1_Brand;
            destination.SKU1_Sort = SKU1_Sort;
            destination.SKU1_CombiProduct = SKU1_CombiProduct;
            destination.SKU1_SimpleProduct = SKU1_SimpleProduct;

            destination.SKU2_Brand = SKU2_Brand;
            destination.SKU2_Sort = SKU2_Sort;
            destination.SKU2_CombiProduct = SKU2_CombiProduct;
            destination.SKU2_SimpleProduct = SKU2_SimpleProduct;

            destination.SKU1String = SKU1String;
            destination.SKU2String = SKU2String;

            destination.Regions = Regions;
            destination.Districs = Districs;
            destination.Cities = Cities;

            destination.RegionsString = RegionsString;
            destination.DistricsString = DistricsString;
            destination.CitiesString = CitiesString;

            destination.GeographicalType = GeographicalType;

            destination.ChanelType = ChanelType;

            destination.M2 = M2;
            destination.M3 = M3;
            destination.M4 = M4;

            destination.M2String = M2String;
            destination.M3String = M3String;
            destination.M4String = M4String;

            destination.Client = Client;
            destination.ClientString = ClientString;
            // TT Info
            destination.TypeTT = TypeTT;
            destination.SubTypeTT = SubTypeTT;
            destination.CategoryTT = CategoryTT;

            destination.TypeTTString = TypeTTString;
            destination.SubTypeTTString = SubTypeTTString;
            destination.CategoryTTString = CategoryTTString;

            destination.Step = Step;
        }

        public enum GeographicalUnitType
        {
            Region,
            District,
            City
        }
    }
}
