﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DevExpress.XtraGrid.Views.Grid;
using WccpReporting.RPTCAnalysis.Constants;

namespace WccpReporting.RPTCAnalysis.Utility
{
    internal class Functions
    {
        internal static DataTable GetFilteredDataTable(DataTable sourceTable, GridView view)
        {
            DataTable dt = null;

            if (null != sourceTable && null != view)
            {
                if (!String.IsNullOrEmpty(view.ActiveFilterString))
                {
                    string filter = NormalizeFilter(view.ActiveFilterString);
                    DataRow[] selectedRows = sourceTable.Select(filter);
                    if (sourceTable.Rows.Count == selectedRows.Count())
                    {
                        dt = sourceTable.Copy();
                    }
                    else
                    {
                        dt = sourceTable.Clone();
                        foreach (DataRow row in selectedRows)
                        {
                            dt.Rows.Add(row.ItemArray);
                        }
                    }
                }
                else
                {
                    dt = sourceTable.Copy();
                }
            }
            return dt;
        }

        private static string NormalizeFilter(string sourceFilter)
        {
            string filter = string.Empty;
            string[] filterLogicalParts = sourceFilter.Split(new string[] { SQLConstants.AND }, StringSplitOptions.RemoveEmptyEntries);
            bool firstLogicalPart = true;
            foreach (string filterLogicalPart in filterLogicalParts)
            {
                string filterPart = String.Empty;
                if (filterLogicalPart.Contains(SQLConstants.IN))
                    filterPart = NormalizeFilterPart_IN(filterLogicalPart);
                else
                    filterPart = NormalizeFilterPart_OR(filterLogicalPart);

                if (firstLogicalPart)
                    firstLogicalPart = false;
                else
                    filter += SQLConstants.AND;

                filter += filterPart;

            }
            return filter;   
        }

        private static string NormalizeFilterPart_IN(string filterLogicalPart)
        {
            string filterPart = String.Empty;
            string[] filterParts = filterLogicalPart.Split(new string[] { SQLConstants.IN }, StringSplitOptions.RemoveEmptyEntries);

            if (filterParts.Length != 2) // incorect filter ???
                filterPart = filterLogicalPart;
            else
            {
                filterPart += filterParts[0];

                string filterData = filterParts[1];

                string[] filterDataArray = filterData.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
                bool bFirst = true;
                string tmpfilterData = string.Empty;
                foreach (string filterDataPart in filterDataArray)
                {
                    string tmp = filterDataPart;
                    tmp = tmp.TrimEnd(" ".ToCharArray());
                    tmp = tmp.TrimStart("(".ToCharArray());
                    tmp = tmp.TrimEnd(")".ToCharArray());
                    tmp = tmp.TrimEnd("m".ToCharArray());
                    tmp = tmp.TrimEnd("L".ToCharArray());
                    if (bFirst)
                        bFirst = false;
                    else
                        tmpfilterData += ",";
                    tmpfilterData += tmp;
                }

                filterPart = String.Format("{0}{1}({2})", filterPart, SQLConstants.IN, tmpfilterData);
            }
            return filterPart;
        }

        private static string NormalizeFilterPart_OR(string filterLogicalPart)
        {
            string res = String.Empty;
            string[] filterParts = filterLogicalPart.Split(new string[] { SQLConstants.OR }, StringSplitOptions.RemoveEmptyEntries);

            bool bFirst = true;
            foreach (string filterPart in filterParts)
            {
                string tmp = filterPart;
                tmp = tmp.TrimEnd(" ".ToCharArray());
                tmp = tmp.TrimStart("(".ToCharArray());
                tmp = tmp.TrimEnd(")".ToCharArray());
                tmp = tmp.TrimEnd("m".ToCharArray());
                tmp = tmp.TrimEnd("L".ToCharArray());
                if (bFirst)
                    bFirst = false;
                else
                    res += SQLConstants.OR;
                res += tmp;
            }
            return String.Format("({0})", res);
        }
        
        internal static void FindMinMaxDecimal(DataTable newSource, ref decimal? maxVal, ref decimal? minVal, string fieldName)
        {
            if (newSource == null)
            {
                maxVal = minVal = null;

                return;
            }

            var column = from row in newSource.AsEnumerable()
                         select row.Field<decimal?>(fieldName);

            decimal? max = column.Where(c => c.HasValue).Max();
            if (max != null && (null == maxVal || max > maxVal))
                maxVal = max;

            decimal? min = column.Where(c => c.HasValue).Min();
            if (min != null && (null == minVal || min < minVal))
                minVal = min;
        }
    }
}
