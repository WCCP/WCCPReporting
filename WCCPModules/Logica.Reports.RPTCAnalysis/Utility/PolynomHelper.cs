﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace WccpReporting.RPTCAnalysis.Utility
{
    public class PolynomHelper
    {
        private double A = 0;
        private double B = 0;
        private double C = 0;
        private double D = 0;

        public PolynomHelper()
        {
        }

        public decimal GetTrendParamValue(decimal X)
        {
            return ((decimal)A * X * X * X + (decimal)B * X * X + (decimal)C * X + (decimal)D);
        }
        public void CalcCoeficients(DataTable table, string fieldNameX, string fieldNameY)
        {
            double [] freeParts = new double[4];
            double[,] mainMatrix = new double[4,4];
            mainMatrix[3, 3] = (double)table.Rows.Count;
            #region cicle
            foreach (DataRow row in table.Rows)
            {
                double x = double.Parse(row[fieldNameX].ToString());
                double y = double.Parse(row[fieldNameY].ToString());
                double tmp = x;

                freeParts[3] += y;
                
                // x
                mainMatrix[3, 2] += tmp;
                mainMatrix[2, 3] += tmp;

                freeParts[2] += y * tmp;

                //x^2
                tmp *= x;
                mainMatrix[1, 3] += tmp;
                mainMatrix[2, 2] += tmp;
                mainMatrix[3, 1] += tmp;

                freeParts[1] += y * tmp;

                //x^3
                tmp *= x;
                mainMatrix[0, 3] += tmp;
                mainMatrix[1, 2] += tmp;
                mainMatrix[2, 1] += tmp;
                mainMatrix[3, 0] += tmp;

                freeParts[0] += y * tmp;

                //x^4
                tmp *= x;
                mainMatrix[0, 2] += tmp;
                mainMatrix[1, 1] += tmp;
                mainMatrix[2, 0] += tmp;

                //x^5
                tmp *= x;
                mainMatrix[0, 1] += tmp;
                mainMatrix[1, 0] += tmp;

                //x^6
                tmp *= x;
                mainMatrix[0, 0] += tmp;
            }
#endregion
            double mainDeterminant = CalcDeterminant4(
                mainMatrix[0, 0],mainMatrix[1, 0],mainMatrix[2, 0],mainMatrix[3, 0],
                mainMatrix[0, 1],mainMatrix[1, 1],mainMatrix[2, 1],mainMatrix[3, 1],
                mainMatrix[0, 2],mainMatrix[1, 2],mainMatrix[2, 2],mainMatrix[3, 2],
                mainMatrix[0, 3],mainMatrix[1, 3],mainMatrix[2, 3],mainMatrix[3, 3]
                );
            if (mainDeterminant != 0)
            {
                double determinant1 = CalcDeterminant4(
                    freeParts[0], mainMatrix[1, 0], mainMatrix[2, 0], mainMatrix[3, 0],
                    freeParts[1], mainMatrix[1, 1], mainMatrix[2, 1], mainMatrix[3, 1],
                    freeParts[2], mainMatrix[1, 2], mainMatrix[2, 2], mainMatrix[3, 2],
                    freeParts[3], mainMatrix[1, 3], mainMatrix[2, 3], mainMatrix[3, 3]
                    );

                double determinant2 = CalcDeterminant4(
                    mainMatrix[0, 0], freeParts[0], mainMatrix[2, 0], mainMatrix[3, 0],
                    mainMatrix[0, 1], freeParts[1], mainMatrix[2, 1], mainMatrix[3, 1],
                    mainMatrix[0, 2], freeParts[2], mainMatrix[2, 2], mainMatrix[3, 2],
                    mainMatrix[0, 3], freeParts[3], mainMatrix[2, 3], mainMatrix[3, 3]
                    );

                double determinant3 = CalcDeterminant4(
                    mainMatrix[0, 0], mainMatrix[1, 0], freeParts[0], mainMatrix[3, 0],
                    mainMatrix[0, 1], mainMatrix[1, 1], freeParts[1], mainMatrix[3, 1],
                    mainMatrix[0, 2], mainMatrix[1, 2], freeParts[2], mainMatrix[3, 2],
                    mainMatrix[0, 3], mainMatrix[1, 3], freeParts[3], mainMatrix[3, 3]
                    );

                double determinant4 = CalcDeterminant4(
                    mainMatrix[0, 0], mainMatrix[1, 0], mainMatrix[2, 0], freeParts[0],
                    mainMatrix[0, 1], mainMatrix[1, 1], mainMatrix[2, 1], freeParts[1],
                    mainMatrix[0, 2], mainMatrix[1, 2], mainMatrix[2, 2], freeParts[2],
                    mainMatrix[0, 3], mainMatrix[1, 3], mainMatrix[2, 3], freeParts[3]
                    );

                A = determinant1 / mainDeterminant;
                B = determinant2 / mainDeterminant;
                C = determinant3 / mainDeterminant;
                D = determinant4 / mainDeterminant;
            }
        }

        private double CalcDeterminant4(
            double a11, double a12, double a13, double a14, 
            double a21, double a22, double a23, double a24, 
            double a31, double a32, double a33, double a34, 
            double a41, double a42, double a43, double a44
            )
        {
            double res = 0;
            res = a11*CalcDeterminant3(a22, a23, a24, 
                                       a32, a33, a34, 
                                       a42, a43, a44) -
                    a12*CalcDeterminant3(a21, a23, a24, 
                                         a31, a33, a34, 
                                         a41, a43, a44) +
                    a13*CalcDeterminant3(a21, a22, a24, 
                                         a31, a32, a34, 
                                         a41, a42, a44) -
                    a14*CalcDeterminant3(a21, a22, a23,
                                         a31, a32, a33,
                                         a41, a42, a43);


            return res;
        }

        private double CalcDeterminant3(
            double a11, double a12, double a13,
            double a21, double a22, double a23,
            double a31, double a32, double a33
            )
        {
            double res = 0;
            res = (a11 * a22 * a33 + a21 * a32 * a13 + a12 * a23 * a31) - (a31 * a22 * a13 + a32 * a23 * a11 + a21 * a12 * a33);
            return res;
        }
    }
}
