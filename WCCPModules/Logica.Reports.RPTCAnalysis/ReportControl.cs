﻿using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraTab;
using DevExpress.XtraBars;
using Logica.Reports.Common.WaitWindow;
using DevExpress.Utils;
using DevExpress.XtraPrinting;
using Logica.Reports.Common;
using WccpReporting.RPTCAnalysis.Localization;
using WccpReporting.RPTCAnalysis.UserControls;
using WccpReporting.RPTCAnalysis.UserControls.Contracts;
using Logica.Reports.BaseReportControl;
using WccpReporting.RPTCAnalysis.DataProvider;
using WccpReporting.RPTCAnalysis;
using DevExpress.XtraPrintingLinks;

namespace WccpReporting
{
    public partial class WccpUIControl : /*BaseReportUserControl// */DevExpress.XtraEditors.XtraUserControl
    {
        // tabs
        StatisticTab statisticTab = null;
        ChartsTab chartsTab = null;
        V_Table1Tab v_Table1Tab = null;
        V_Table2Tab v_Table2Tab = null;
        V_Table3Tab v_Table3Tab = null;
        InDataDynamicTab inDataDynamicTab = null;
        InDataTab inDataTab = null;
        //

        private DataTable BaseDataTable {get; set;}
        public ReportSettings ReportSettings;
        
        public static SettingsForm SettingsForm{ get; set; }
        
        public WccpUIControl() //: base(WCCPAPI.GetReportId())
        {
            InitializeComponent();            
            btnExportAllToExcel.Tag = btnExportToExcel.Tag = ExportToType.Xls;
            btnExportAllToHtml.Tag = btnExportToHtml.Tag = ExportToType.Html;
            btnExportAllToMht.Tag = btnExportToMht.Tag = ExportToType.Mht;
            btnExportAllToPdf.Tag = btnExportToPdf.Tag = ExportToType.Pdf;
            btnExportAllToRtf.Tag = btnExportToRtf.Tag = ExportToType.Rtf;
            btnExportAllToText.Tag = btnExportToText.Tag = ExportToType.Txt;
            btnExportAllToImage.Tag = btnExportToImage.Tag = ExportToType.Bmp;
            btnExportAllToCsv.Tag = btnExportToCsv.Tag = ExportToType.Csv;
            btnClose.Visibility = BarItemVisibility.Never;

            SettingsForm.GetReportSettings(ref ReportSettings); // At this point ReportSettings will be initialized
            ShowReport();
            
            LocalizeForm();
        }

        private void CallSettingsForm()
        {
            if (SettingsForm.ShowDialog() == DialogResult.OK)
            {
                SettingsForm.GetReportSettings(ref ReportSettings);
                ShowReport();
            }
        }

        private void SetupTab(BaseTabReport tab)
        {
            tab.SetReportSetting(ReportSettings);
            XtraTabPage xtraTabPage = new XtraTabPage();
            xtraTabPage.Controls.Add(tab);
            xtraTabPage.Tag = tab;
            xtraTabPage.Text = tab.TabName;
            tabManager.TabPages.Add(xtraTabPage);
        }

        private void ShowReport()
        {
            WaitManager.StartWait();

            try
            {
                GetBaseData();

                CreateTabs();

                inDataDynamicTab.SetSource(BaseDataTable.Copy());
                WaitManager.StartWait();
                inDataTab.SetSource(BaseDataTable);

                statisticTab.SetShortSummary(DataAccessProvider.GetShortSummury(ReportSettings));
            }
            catch (Exception e)
            {
                throw new Exception(e.Message, e.InnerException);
            }
            finally
            {
                WaitManager.StopWait();
            }
        }

        private void CreateTabs()
        {
            if (null == statisticTab)
            {
                statisticTab = new StatisticTab();
                SetupTab(statisticTab);
            }
            if (null == chartsTab)
            {
                chartsTab = new ChartsTab();
                SetupTab(chartsTab);
            }
            if (null == v_Table1Tab)
            {
                v_Table1Tab = new V_Table1Tab();
                SetupTab(v_Table1Tab);
                v_Table1Tab.DataSourceChanged += new Delegates.DataSourceChanged(vt1_DataSourceChanged);
            }
            if (null == v_Table2Tab)
            {
                v_Table2Tab = new V_Table2Tab();
                SetupTab(v_Table2Tab);
                v_Table2Tab.DataSourceChanged += new Delegates.DataSourceChanged(vt2_DataSourceChanged);
            }
            if (null == v_Table3Tab)
            {
                v_Table3Tab = new V_Table3Tab();
                SetupTab(v_Table3Tab);
                v_Table3Tab.DataSourceChanged += new Delegates.DataSourceChanged(vt3_DataSourceChanged);
            }
            if (null == inDataDynamicTab)
            {
                inDataDynamicTab = new InDataDynamicTab();
                SetupTab(inDataDynamicTab);
                inDataDynamicTab.FilterChanged += new Delegates.DataSourceChanged(InDataDynamicTab_FilterChanged);
            }
            if (null == inDataTab)
            {
                inDataTab = new InDataTab();
                SetupTab(inDataTab);
            }
        }

        void vt1_DataSourceChanged(DataTable newSource)
        {
            chartsTab.V_Table1 = newSource;
        }

        void vt2_DataSourceChanged(DataTable newSource)
        {
            chartsTab.V_Table2 = newSource;
            statisticTab.V_Table2 = newSource;
        }

        void vt3_DataSourceChanged(DataTable newSource)
        {
            chartsTab.V_Table3 = newSource;
            statisticTab.V_Table3 = newSource;
        }

        void InDataDynamicTab_FilterChanged(DataTable newSource)
        {
            chartsTab.InData = newSource.Copy();
            v_Table1Tab.SetSource(newSource);
            v_Table2Tab.SetSource(newSource);
            v_Table3Tab.SetSource(newSource);
            statisticTab.SetSource(newSource);
        }

        private void GetBaseData()
        {
            BaseDataTable = DataAccessProvider.GetInDataTable(ReportSettings);
        }

        #region Helper
        private void LocalizeForm()
        {
            btnSettings.SuperTip = new SuperToolTip();
            btnSettings.SuperTip.Items.Add(Resource.ReportSettings);
            btnSettings.Caption = Resource.ReportSettings;

            btnClose.SuperTip = new SuperToolTip();
            btnClose.SuperTip.Items.Add(Resource.CloseSheet);
            btnClose.Caption = Resource.CloseSheet;

            btnPrint.SuperTip = new SuperToolTip();
            btnPrint.SuperTip.Items.Add(Resource.Print);
            btnPrint.Caption = Resource.Print;

            btnRefresh.SuperTip = new SuperToolTip();
            btnRefresh.SuperTip.Items.Add(Resource.Refresh);
            btnRefresh.Caption = Resource.Refresh;

            btnExportTo.SuperTip = new SuperToolTip();
            btnExportTo.SuperTip.Items.Add(Resource.ExportTo);
            btnExportTo.Caption = Resource.ExportTo;

            btnExportAllTo.SuperTip = new SuperToolTip();
            btnExportAllTo.SuperTip.Items.Add(Resource.ExportAllTo);
            btnExportAllTo.Caption = Resource.ExportAllTo;
            
            

            btnExportAllToCsv.Caption = btnExportToCsv.Caption = Resource.ExportToCsv;
            btnExportAllToExcel.Caption = btnExportToExcel.Caption = Resource.ExportToExcel;
            btnExportAllToHtml.Caption = btnExportToHtml.Caption = Resource.ExportToHtml;
            btnExportAllToImage.Caption = btnExportToImage.Caption = Resource.ExportToBmp;
            btnExportAllToMht.Caption = btnExportToMht.Caption = Resource.ExportToMht;
            btnExportAllToPdf.Caption = btnExportToPdf.Caption = Resource.ExportToPdf;
            btnExportAllToRtf.Caption = btnExportToRtf.Caption = Resource.ExportToRtf;
            btnExportAllToText.Caption = btnExportToText.Caption = Resource.ExportToText;
        }
        #endregion

        #region Menu Event

        private void btnSettings_ItemClick(object sender, ItemClickEventArgs e)
        {
            CallSettingsForm();
        }

        private void btnClose_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (null != tabManager.SelectedTabPage)
            {
                /*
                tabManager.TabPages.Remove(tabManager.SelectedTabPage);
                 */
            }
        }
       

        private void btnPrint_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (DialogResult.Yes == MessageBox.Show(String.Format(Resource.ConfirmPrintAllSheets, tabManager.TabPages.Count), Resource.Information, MessageBoxButtons.YesNo, MessageBoxIcon.Question))
            {
                CompositeLink compositeLink = BuildExportInfo(ExportToType.Pdf, false);
                ExportToPrint(compositeLink);
            }
        }

        private void btnRefresh_ItemClick(object sender, ItemClickEventArgs e)
        {
            ShowReport();
        }

        #region Export engine
        private void btnExportTo_ItemClick(object sender, ItemClickEventArgs e)
        {
            Export((ExportToType)e.Item.Tag);
        }

        private String GetFilePath(ExportToType type)
        {
            String res = String.Empty;
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Title = Resource.SpecifyFileName;
            sfd.InitialDirectory = System.Reflection.Assembly.GetExecutingAssembly().Location;
            sfd.FileName = "Report";
            sfd.Filter = String.Format("(*.{0})|*.{0}", type.ToString().ToLower());
            sfd.FilterIndex = 1;
            sfd.OverwritePrompt = true;
            sfd.RestoreDirectory = true;

            if (sfd.ShowDialog() != DialogResult.Cancel)
            {
                res = sfd.FileName;
            }
            return res;
        }

        private void Export(ExportToType type)
        {
            if (null == tabManager.SelectedTabPage)
                return;

            String filePath = GetFilePath(type);
            if (!String.IsNullOrEmpty(filePath))
            {
                CompositeLink compositeLink = new CompositeLink(new PrintingSystem());
                compositeLink = ExportEngine(tabManager.SelectedTabPage.Tag as BaseTabReport, type, compositeLink);
                ExportToFile(compositeLink, filePath, type);
            }
        }
        
        private void btnExportAll_ItemClick(object sender, ItemClickEventArgs e)
        {
            ExportToType type = (ExportToType)e.Item.Tag;
            if (tabManager.TabPages.Count < 2)
            {
                Export(ExportToType.Xls);
                return;
            }
            if (DialogResult.Yes == MessageBox.Show(String.Format(Resource.ConfirmExportAllSheets,tabManager.TabPages.Count), Resource.Information, MessageBoxButtons.YesNo, MessageBoxIcon.Question))
            {
                String fileName = GetFilePath(type);

                if(!String.IsNullOrEmpty(fileName)) 
                {
                    CompositeLink compositeLink = BuildExportInfo(type, false);
                    ExportToFile(compositeLink, fileName, type);
                }
            }
        }

        private CompositeLink BuildExportInfo(ExportToType type, bool forPrint)
        {
            CompositeLink compositeLink = new CompositeLink(new PrintingSystem());
            foreach (XtraTabPage page in tabManager.TabPages)
            {
                BaseTabReport tab = (BaseTabReport)page.Tag;
                compositeLink = ExportEngine(tab, type, compositeLink);

                if (forPrint)
                {
                    if (tabManager.TabPages.Last() != page)
                        compositeLink.BreakSpace = (int)(compositeLink.PrintingSystem.PageSettings.UsablePageSizeInPixels.Height - 1.0);
                }
            }
            return compositeLink;
        }

        private CompositeLink ExportEngine(BaseTabReport report, ExportToType type, CompositeLink compositeLink)
        {
            if (null != report)
                compositeLink = report.ExportTo(type, compositeLink);

            return compositeLink;
        }

        private void ExportToPrint(CompositeLink compositeLink)
        {
            compositeLink.PrintingSystem.PreviewFormEx.Show();
        }
        private void ExportToFile(CompositeLink compositeLink, string fileName, ExportToType type)
        {
            try
            {
                switch (type)
                {
                    case ExportToType.Bmp:
                        MessageBox.Show(Resource.ExportFormatNotSupported, Resource.Information);
                        break;
                    case ExportToType.Csv:
                        compositeLink.PrintingSystem.ExportToCsv(fileName);
                        break;
                    case ExportToType.Html:
                        compositeLink.PrintingSystem.ExportToHtml(fileName);
                        break;
                    case ExportToType.Mht:
                        compositeLink.PrintingSystem.ExportToMht(fileName);
                        break;
                    case ExportToType.Pdf:
                        compositeLink.PrintingSystem.ExportToPdf(fileName);
                        break;
                    case ExportToType.Rtf:
                        compositeLink.PrintingSystem.ExportToRtf(fileName);
                        break;
                    case ExportToType.Txt:
                        compositeLink.PrintingSystem.ExportToText(fileName);
                        break;
                    case ExportToType.Xls:
                        compositeLink.PrintingSystem.ExportToXls(fileName);
                        break;
                }
            }
            catch (Exception ex)
            {
                BaseTabReport.HandleExportException(ex, fileName);
            }
        }
        #endregion

       
        #endregion
    }
}
