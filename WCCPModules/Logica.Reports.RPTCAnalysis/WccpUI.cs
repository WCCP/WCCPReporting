﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using Logica.Reports.Common;
using ModularWinApp.Core.Interfaces;
using WccpReporting.RPTCAnalysis.Localization;
using WccpReporting.RPTCAnalysis;
using System.Windows.Forms;

namespace WccpReporting
{
    [Export(typeof(IStartupClass))]
    [ExportMetadata("ModuleName", "RPTCAnalysis.dll")]
    public class WccpUI : IStartupClass
    {
        #region IStartupClass Members

        private WccpUIControl _reportControl;
        private string _reportCaption;

        public Control ReportControl
        {
            get { return _reportControl; }
        }

        public string ReportCaption
        {
            get { return _reportCaption; }
        }

        public string GetVersion()
        {
            return System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();
        }

        public int ShowUI(int isSkined, string reportCaption)
        {
            try
            {
                WccpUIControl.SettingsForm = new SettingsForm();
                if (WccpUIControl.SettingsForm.ShowDialog() == DialogResult.OK)
                {
                    _reportControl = new WccpUIControl();
                    _reportCaption = reportCaption;
                    
                    return 0;
                }
            }
            catch (Exception ex)
            {
                ErrorManager.ShowErrorBox(ex.Message);
            }

            return 1;
        }

        public void CloseUI()
        {
            //throw new NotImplementedException();
        }

        public bool AllowClose()
        {
            return true;
        }

        #endregion
    }
}
