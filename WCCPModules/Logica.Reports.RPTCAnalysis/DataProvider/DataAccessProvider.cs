﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Logica.Reports.DataAccess;
using WccpReporting.RPTCAnalysis.Constants;
using System.Data.SqlClient;
using WccpReporting.RPTCAnalysis.UserControls.Contracts;

namespace WccpReporting.RPTCAnalysis.DataProvider
{
    internal class DataAccessProvider
    {
        private static String BuildInStatement(int[] values)
        {
            String res = String.Empty;

            for (int i = 0; i < values.Length; i++)
            {
                res += values[i].ToString();
                res += ",";
            }
            res = res.TrimEnd(",".ToCharArray());

            if (!String.IsNullOrEmpty(res))
                res = String.Format("({0})", res);

            return res;
        }

        private static String BuildInStatement(String value)
        {
            String res = String.Empty;

            if (!String.IsNullOrEmpty(value))
                res = String.Format("({0})", value);

            return res;
        }

        private static SqlParameter[] GetInStatementAsParameters(String paramName, SqlDbType paramType, int[] values)
        {
            SqlParameter[] spParams = new SqlParameter[1];
            SqlParameter param = new SqlParameter()
            {
                ParameterName = paramName,
                SqlValue = BuildInStatement(values),
                SqlDbType = paramType
            };
            spParams[0] = param;
            return spParams;
        }

        private static SqlParameter[] BuildFullParameters(ReportSettings reportSettings, bool needStep)
        {
            int count = 0;
            SqlParameter sqlParam = null;
            SqlParameter[] sqlParams = null;
            if (needStep)
                sqlParams = new SqlParameter[23];
            else
                sqlParams = new SqlParameter[22];

            sqlParam = new SqlParameter()
            {
                SqlDbType = SqlDbType.DateTime,
                Value = reportSettings.DateStartPeriod1.ToString("yyyy-MM-dd"),
                ParameterName = SQLConstants.PERIOD1_BEGIN
            };
            sqlParams[count++] = sqlParam;

            sqlParam = new SqlParameter()
            {
                SqlDbType = SqlDbType.DateTime,
                Value = reportSettings.DateEndPeriod1.ToString("yyyy-MM-dd"),
                ParameterName = SQLConstants.PERIOD1_END
            };
            sqlParams[count++] = sqlParam;

            sqlParam = new SqlParameter()
            {
                SqlDbType = SqlDbType.DateTime,
                Value = reportSettings.DateStartPeriod2.ToString("yyyy-MM-dd"),
                ParameterName = SQLConstants.PERIOD2_BEGIN
            };
            sqlParams[count++] = sqlParam;

            sqlParam = new SqlParameter()
            {
                SqlDbType = SqlDbType.DateTime,
                Value = reportSettings.DateEndPeriod2.ToString("yyyy-MM-dd"),
                ParameterName = SQLConstants.PERIOD2_END
            };
            sqlParams[count++] = sqlParam;

            sqlParam = new SqlParameter()
            {
                SqlDbType = SqlDbType.VarChar,
                Value = BuildInStatement(reportSettings.Regions),
                ParameterName = SQLConstants.REGION_ID
            };
            sqlParams[count++] = sqlParam;

            sqlParam = new SqlParameter()
            {
                SqlDbType = SqlDbType.VarChar,
                Value = BuildInStatement(reportSettings.Districs),
                ParameterName = SQLConstants.DISTRICT_ID
            };
            sqlParams[count++] = sqlParam;

            sqlParam = new SqlParameter()
            {
                SqlDbType = SqlDbType.VarChar,
                Value = BuildInStatement(reportSettings.Cities),
                ParameterName = SQLConstants.CITY_ID
            };
            sqlParams[count++] = sqlParam;

            sqlParam = new SqlParameter()
            {
                SqlDbType = SqlDbType.VarChar,
                Value = BuildInStatement(reportSettings.M4),
                ParameterName = SQLConstants.M4_ID
            };
            sqlParams[count++] = sqlParam;

            sqlParam = new SqlParameter()
            {
                SqlDbType = SqlDbType.VarChar,
                Value = BuildInStatement(reportSettings.M3),
                ParameterName = SQLConstants.M3_ID
            };
            sqlParams[count++] = sqlParam;

            sqlParam = new SqlParameter()
            {
                SqlDbType = SqlDbType.VarChar,
                Value = BuildInStatement(reportSettings.M2),
                ParameterName = SQLConstants.M2_ID
            };
            sqlParams[count++] = sqlParam;

            sqlParam = new SqlParameter()
            {
                SqlDbType = SqlDbType.VarChar,
                Value = BuildInStatement(reportSettings.Client),
                ParameterName = SQLConstants.CUST_ID
            };
            sqlParams[count++] = sqlParam;

            sqlParam = new SqlParameter()
            {
                SqlDbType = SqlDbType.VarChar,
                Value = BuildInStatement(reportSettings.TypeTT),
                ParameterName = SQLConstants.TYPETT_ID
            };
            sqlParams[count++] = sqlParam;

            sqlParam = new SqlParameter()
            {
                SqlDbType = SqlDbType.VarChar,
                Value = BuildInStatement(reportSettings.SubTypeTT),
                ParameterName = SQLConstants.SUBTYPETT_ID
            };
            sqlParams[count++] = sqlParam;

            sqlParam = new SqlParameter()
            {
                SqlDbType = SqlDbType.VarChar,
                Value = BuildInStatement(reportSettings.CategoryTT),
                ParameterName = SQLConstants.CATEGORY
            };
            sqlParams[count++] = sqlParam;

            sqlParam = new SqlParameter()
            {
                SqlDbType = SqlDbType.VarChar,
                Value = BuildInStatement(reportSettings.SKU1_Brand),
                ParameterName = SQLConstants.SKU1_P1
            };
            sqlParams[count++] = sqlParam;

            sqlParam = new SqlParameter()
            {
                SqlDbType = SqlDbType.VarChar,
                Value = BuildInStatement(reportSettings.SKU1_Sort),
                ParameterName = SQLConstants.SKU1_P2
            };
            sqlParams[count++] = sqlParam;

            sqlParam = new SqlParameter()
            {
                SqlDbType = SqlDbType.VarChar,
                Value = BuildInStatement(reportSettings.SKU1_CombiProduct),
                ParameterName = SQLConstants.SKU1_P3
            };
            sqlParams[count++] = sqlParam;

            sqlParam = new SqlParameter()
            {
                SqlDbType = SqlDbType.VarChar,
                Value = BuildInStatement(reportSettings.SKU1_SimpleProduct),
                ParameterName = SQLConstants.SKU1_P4
            };
            sqlParams[count++] = sqlParam;

            sqlParam = new SqlParameter()
            {
                SqlDbType = SqlDbType.VarChar,
                Value = BuildInStatement(reportSettings.SKU2_Brand),
                ParameterName = SQLConstants.SKU2_P1
            };
            sqlParams[count++] = sqlParam;

            sqlParam = new SqlParameter()
            {
                SqlDbType = SqlDbType.VarChar,
                Value = BuildInStatement(reportSettings.SKU2_Sort),
                ParameterName = SQLConstants.SKU2_P2
            };
            sqlParams[count++] = sqlParam;

            sqlParam = new SqlParameter()
            {
                SqlDbType = SqlDbType.VarChar,
                Value = BuildInStatement(reportSettings.SKU2_CombiProduct),
                ParameterName = SQLConstants.SKU2_P3
            };
            sqlParams[count++] = sqlParam;

            sqlParam = new SqlParameter()
            {
                SqlDbType = SqlDbType.VarChar,
                Value = BuildInStatement(reportSettings.SKU2_SimpleProduct),
                ParameterName = SQLConstants.SKU2_P4
            };
            sqlParams[count++] = sqlParam;

            if (needStep)
            {
                sqlParam = new SqlParameter()
                {
                    SqlDbType = SqlDbType.Decimal,
                    Value = reportSettings.Step,
                    ParameterName = SQLConstants.STEP
                };
                sqlParams[count++] = sqlParam;
            }

            return sqlParams;
        }

        internal static void OpenConnection()
        {
            DataAccessLayer.OpenConnection();
        }
        
        internal static void CloseConnection()
        {
            DataAccessLayer.CloseConnection();
        }

        internal static DataTable GetInDataTable(ReportSettings reportSettings)
        {
            DataTable res = null;

            DataSet ds = DataAccessLayer.ExecuteStoredProcedure(SQLConstants.SP_DW_RPTC_PAGEINDATA, BuildFullParameters(reportSettings, true));
            if (null != ds && ds.Tables.Count > 0)
                res = ds.Tables[0];

            return res;
        }

        internal static DataTable GetShortSummury(ReportSettings reportSettings)
        {
            DataTable res = null;

            DataSet ds = DataAccessLayer.ExecuteStoredProcedure(SQLConstants.SP_DW_RPTC_SHORTSUMMARY, BuildFullParameters(reportSettings, false));
            if (null != ds && ds.Tables.Count > 0)
                res = ds.Tables[0];

            return res;
        }
        /* for testing
        internal static DataTable GetInDataTable(DateTime dtStartPeriod1, DateTime dtEndPeriod1, DateTime dtStartPeriod2, DateTime dtEndPeriod2, string regionIDs, int sku1, int sku2)
        {
            DataTable res = null;
            //exec spDW_RPTC_PageInData '20100101','20100131','20100201','20100228','(7)', null,null, null,null,253,258
            string SQL = string.Format(@"exec spDW_RPTC_PageInData '{0}', '{1}', '{2}', '{3}', '({4})', null,null, null,null, {5}, {6}",
                dtStartPeriod1.ToString("yyyy-MM-dd"), dtEndPeriod1.ToString("yyyy-MM-dd"), dtStartPeriod2.ToString("yyyy-MM-dd"), dtEndPeriod2.ToString("yyyy-MM-dd"), regionIDs, sku1, sku2);

            DataAccessLayer.OpenConnection();
            DataSet ds = DataAccessLayer.ExecuteQuery(SQL);
            if (null != ds && ds.Tables.Count > 0)
                res = ds.Tables[0];
            DataAccessLayer.CloseConnection();
            return res;
        }
         * */
        /// <summary>
        /// exec spDW_RPTC_GetRegions
        /// </summary>
        /// <returns></returns>
        internal static DataTable GetRegions()
        {
            DataTable res = null;
            DataSet ds = DataAccessLayer.ExecuteStoredProcedure(SQLConstants.SP_DW_RPTC_GET_REGIONS, new SqlParameter[] {});

            if(null != ds && ds.Tables.Count >0)
                res = ds.Tables[0];

            return res;
        }
        internal static DataTable GetDistricts(int region)
        {
            return GetDistricts(new int[] { region });
        }

        internal static DataTable GetDistributor(string m2, string m3, string m4)
        {
            DataTable res = null;
            SqlParameter[] sqlParams = new SqlParameter[3];
            if (!String.IsNullOrEmpty(m2))
                m2 = String.Format("({0})", m2);

            if (!String.IsNullOrEmpty(m3))
                m3 = String.Format("({0})", m3);

            if (!String.IsNullOrEmpty(m4))
                m4 = String.Format("({0})", m4);

            SqlParameter param = new SqlParameter()
            {
                SqlDbType = SqlDbType.VarChar,
                Value = m2,
                ParameterName = SQLConstants.PAR_M2
            };
            sqlParams[0] = param;

            param = new SqlParameter()
            {
                SqlDbType = SqlDbType.VarChar,
                Value = m3,
                ParameterName = SQLConstants.PAR_M3
            };
            sqlParams[1] = param;
            
            param = new SqlParameter()
            {
                SqlDbType = SqlDbType.VarChar,
                Value = m4,
                ParameterName = SQLConstants.PAR_M4
            };
            sqlParams[2] = param;
            DataSet ds = DataAccessLayer.ExecuteStoredProcedure(SQLConstants.SP_DW_RPTC_GET_DISTRIBUTOR, sqlParams);

            if (null != ds && ds.Tables.Count > 0)
                res = ds.Tables[0];

            return res;
        }

        internal static DataTable GetPersonal(string regionIDs, string districIDs, string citieIDs, int chanelType)
        {
            DataTable res = null;
            SqlParameter[] sqlParams = new SqlParameter[4];
            if (!String.IsNullOrEmpty(regionIDs))
                regionIDs = String.Format("({0})", regionIDs);
            
            if (!String.IsNullOrEmpty(districIDs))
                districIDs = String.Format("({0})", districIDs);
            if (!String.IsNullOrEmpty(citieIDs))
                citieIDs = String.Format("({0})", citieIDs);
            
            SqlParameter param = new SqlParameter()
            {
                SqlDbType = SqlDbType.VarChar,
                Value = regionIDs,
                ParameterName = SQLConstants.PAR_REGIONS
            };
            sqlParams[0] = param;
            param = new SqlParameter()
            {
                SqlDbType = SqlDbType.VarChar,
                Value = districIDs,
                ParameterName = SQLConstants.PAR_DISTRICTS
            };
            sqlParams[1] = param;
            param = new SqlParameter()
            {
                SqlDbType = SqlDbType.VarChar,
                Value = citieIDs,
                ParameterName = SQLConstants.PAR_CITY
            };
            sqlParams[2] = param;
            param = new SqlParameter()
            {
                SqlDbType = SqlDbType.Int,
                Value = chanelType,
                ParameterName = SQLConstants.PAR_CHANELTYPE
            };
            sqlParams[3] = param;
            DataSet ds = DataAccessLayer.ExecuteStoredProcedure(SQLConstants.SP_DW_RPTC_GET_PERSONAL, sqlParams);

            if(null != ds && ds.Tables.Count >0)
                res = ds.Tables[0];

            return res;
        }
        /// <summary>
        /// exec spDW_RPTC_GetDistricts
        ///  повертає ID та назви областей які належать регіонам що передались в якості вхідного параметра.
        /// </summary>
        /// <param name="regions">Вхідні дані – (Приклад:  ‘(1,7)’ - Передаються ID регіонів Києва і Західного регіону  ) На виході отримаємо всі області що належать Києвсьому і Західному регіону</param>
        /// <returns></returns>
        internal static DataTable GetDistricts(int [] regions)
        {
            DataTable res = null;

            DataSet ds = DataAccessLayer.ExecuteStoredProcedure(SQLConstants.SP_DW_RPTC_GET_DISTRICTS, GetInStatementAsParameters(SQLConstants.PAR_REGIONSID, SqlDbType.VarChar, regions));

            if(null != ds && ds.Tables.Count >0)
                res = ds.Tables[0];

            return res;
        }

        internal static DataTable GetCities(int district, string isCity)
        {
            return GetCities(new int[] { district }, isCity);
        }
        /// <summary>
        /// exec spDW_RPTC_GetCity
        /// </summary>
        /// <param name="regions">повертає всі міста що належать областям ID яких передаються вякості вхідних параметрів. 
        /// ID передавати через кому і в дужках. Прикдад ‘(ID1, ID2, ID3)’
        /// 1 – Село
        /// 2 – Місто
        /// </param>
        /// <returns></returns>
        internal static DataTable GetCities(int[] districts,  string isCity)
        {
            DataTable res = null;

            SqlParameter[] spParams = new SqlParameter[2];
            SqlParameter param = new SqlParameter()
            {
                ParameterName = SQLConstants.PAR_DISTRICTSID,
                SqlValue = BuildInStatement(districts),
                SqlDbType =  SqlDbType.VarChar
            };
            spParams[0] = param;
            param = new SqlParameter()
            {
                ParameterName = SQLConstants.PAR_ISCITY,
                SqlValue = isCity,
                SqlDbType = SqlDbType.VarChar
            };
            spParams[1] = param;
            DataSet ds = DataAccessLayer.ExecuteStoredProcedure(SQLConstants.SP_DW_RPTC_GET_CITY, spParams);

            if(null != ds && ds.Tables.Count >0)
                res = ds.Tables[0];

            return res;
        }
        /// <summary>
        /// exec spDW_RPTC_GetSKU
        /// Викликаєтсья без параметрів
        /// </summary>
        /// <returns>Повертає ID та назви всіх SKU</returns>
        internal static DataTable GetSKUs()
        {
            DataTable res = null;
            DataSet ds = DataAccessLayer.ExecuteStoredProcedure(SQLConstants.SP_DW_RPTC_GET_SKU, new SqlParameter[]{});

            if(null != ds && ds.Tables.Count >0)
                res = ds.Tables[0];

            return res;
        }
        /// <summary>
        /// exec spDW_RPTC_GetSKU
        /// Викликаєтсья без параметрів
        /// </summary>
        /// <returns>Повертає ID та назви всіх SKU</returns>
        internal static DataTable GetSKUsTreeInfo()
        {
            DataTable res = null;
            DataSet ds = DataAccessLayer.ExecuteStoredProcedure(SQLConstants.SP_DW_RPTC_GET_SKU_TREE, new SqlParameter[]{});

            if(null != ds && ds.Tables.Count >0)
                res = ds.Tables[0];

            return res;
        }
        /// <summary>
        /// exec spDW_RPTC_TypeTT
        /// Викликаєтсья без параметрів
        /// </summary>
        /// <returns>Повертає ID та назви типів торгових точок.</returns>
        internal static DataTable GetTTTypes()
        {
            DataTable res = null;
            DataSet ds = DataAccessLayer.ExecuteStoredProcedure(SQLConstants.SP_DW_RPTC_TYPETT, new SqlParameter[]{});

            if(null != ds && ds.Tables.Count >0)
                res = ds.Tables[0];

            return res;
        }

        /// <summary>
        /// exec spDW_RPTC_GetTypeAndSubTypeTT
        /// Викликаєтсья без параметрів
        /// </summary>
        /// <returns></returns>
        internal static DataTable GetTypesAndSubtypesTT()
        {
            DataTable res = null;
            DataSet ds = DataAccessLayer.ExecuteStoredProcedure(SQLConstants.SP_DW_RPTC_GET_TYPEANDSUBTYPE_TT, new SqlParameter[]{});

            if(null != ds && ds.Tables.Count >0)
                res = ds.Tables[0];

            return res;
        }

        internal static DataTable GetChanelTypeList()
        {
            DataTable res = null;
            DataSet ds = DataAccessLayer.ExecuteStoredProcedure(SQLConstants.SP_DW_CHANEL_TYPE_LIST, new SqlParameter[] { });

            if (null != ds && ds.Tables.Count > 0)
                res = ds.Tables[0];

            return res;
        }
    }
}
