﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WccpReporting.RPTCAnalysis.Constants
{
    public class SQLConstants
    {
        // SP names
        public const string SP_DW_RPTC_TYPETT = @"spDW_RPTC_TypeTT";
        public const string SP_DW_RPTC_GET_SKU = @"spDW_RPTC_GetSKU";
        public const string SP_DW_RPTC_GET_SKU_TREE = @"spDW_RPTC_GetSKUTree";
        public const string SP_DW_RPTC_GET_CITY = @"spDW_RPTC_GetCity";
        public const string SP_DW_RPTC_GET_DISTRICTS = @"spDW_RPTC_GetDistricts";
        public const string SP_DW_RPTC_GET_REGIONS = @"spDW_RPTC_GetRegions";
        public const string SP_DW_RPTC_GET_TYPEANDSUBTYPE_TT = @"spDW_RPTC_GetTypeAndSubTypeTT";
        public const string SP_DW_RPTC_GET_PERSONAL = @"spDW_RPTC_GetPersonal";
        public const string SP_DW_CHANEL_TYPE_LIST = @"spDW_ChanelTypeList";
        public const string SP_DW_RPTC_GET_DISTRIBUTOR = @"spDW_RPTC_GetDistributor";
        public const string SP_DW_RPTC_PAGEINDATA = @"spDW_RPTC_PageInData";
        public const string SP_DW_RPTC_SHORTSUMMARY = @"spDW_RPTC_ShortSummary";
        // Sp parameter names
        public const string PAR_DISTRICTSID = @"@DistrictsId";
        public const string PAR_REGIONSID = @"@RegionsId";
        public const string PAR_ISCITY = @"@IsCity";
        public const string PAR_REGIONS = @"@regions";
        public const string PAR_DISTRICTS = @"@districts";
        public const string PAR_CITY = @"@city";
        public const string PAR_CHANELTYPE = @"@ChanelType";
        public const string PAR_M2 = @"@m2";
        public const string PAR_M3 = @"@m3";
        public const string PAR_M4 = @"@m4";

        public const string AND = @" And ";
        public const string OR = @" Or ";
        public const string IN = @" In ";


        public const string PERIOD1_BEGIN = @"@Period1Begin";
        public const string PERIOD1_END = @"@Period1End";
        public const string PERIOD2_BEGIN = @"@Period2Begin";
        public const string PERIOD2_END = @"@Period2End";
        public const string REGION_ID = @"@RegionID";
        public const string DISTRICT_ID = @"@DistrictID";
        public const string CITY_ID = @"@CityId";
        public const string M4_ID = @"@M4Id";
        public const string M3_ID = @"@M3Id";
        public const string M2_ID = @"@M2Id";
        public const string CUST_ID = @"@CustID";
        public const string TYPETT_ID = @"@TypeTTId";
        public const string SUBTYPETT_ID = @"@SubTypeTTId";
        public const string CATEGORY = @"@Category";
        public const string SKU1 = @"@SKU1";
        public const string SKU2 = @"@SKU2";
        public const string @SKU1_P1 = @"@SKU1_P1";     // Бренд  
        public const string @SKU1_P2 = @"@SKU1_P2";        // Сорт  
        public const string @SKU1_P3 = @"@SKU1_P3";        // Комбінований продукт    
        public const string @SKU1_P4 = @"@SKU1_P4";        // Продукт   
        public const string @SKU2_P1 = @"@SKU2_P1";        // Бренд  
        public const string @SKU2_P2 = @"@SKU2_P2";        // Сорт   
        public const string @SKU2_P3 = @"@SKU2_P3";        // Комбінований продукт  
        public const string @SKU2_P4 = @"@SKU2_P4";        // Продукт  
        public const string STEP = @"@STEP";
    }
}
