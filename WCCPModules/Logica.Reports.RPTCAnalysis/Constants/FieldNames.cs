﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WccpReporting.RPTCAnalysis.Constants
{
    public class FieldNames
    {
        // V_Table1 constants
        public const string PTC = @"PTC";
        public const string ST1SKU1POCSharp = @"ST1SKU1POC#";
        public const string ST1SKU1POCPercent = @"ST1SKU1POC%";
        public const string ST2SKU1POCSharp = @"ST2SKU1POC#";
        public const string ST2SKU1POCPercent = @"ST2SKU1POC%";
        public const string ST1SKU2POCSharp = @"ST1SKU2POC#";
        public const string ST1SKU2POCPercent = @"ST1SKU2POC%";
        public const string ST2SKU2POCSharp = @"ST2SKU2POC#";
        public const string ST2SKU2POCPercent = @"ST2SKU2POC%";

        // V_Table2 constants
        public const string Di = @"Di";
        public const string D1 = @"D1";
        public const string D1Percent = @"D1%";
        public const string D2 = @"D2";
        public const string D2Percent = @"D2%";
        public const string PTC2SKU1 = @"PTC2SKU1";
        public const string PTC2SKU2 = @"PTC2SKU2";
        public const string V1SKU1 = @"V1SKU1";
        public const string V2SKU1 = @"V2SKU1";
        public const string V1SKU1perPOC = @"V1SKU1perPOC";
        public const string V2SKU1perPOC = @"V2SKU1perPOC";
        public const string V1SKU1Percent = @"V1SKU1%";
        public const string V2SKU1Percent = @"V2SKU1%";
        public const string TREND = @"TREND";

        // V_Table3 constants
        public const string D21 = @"D21";
        public const string D21Sharp = @"D21#";
        public const string D21Percent = @"D21%";
        //public const string PTC2SKU1 = @"PTC2SKU1";
        //public const string PTC2SKU2 = @"PTC2SKU2";
        //public const string V1SKU1 = @"V1SKU1";
        //public const string V2SKU1 = @"V2SKU1";
        //public const string V1SKU1perPOC = @"V1SKU1perPOC";
        //public const string V2SKU1perPOC = @"V2SKU1perPOC";
        //public const string V1SKU1Percent = @"V1SKU1%";
        //public const string V2SKU1Percent = @"V2SKU1%";
        public const string Vchange = @"Vchange";
        public const string POCShareST1SKU1 = @"POCShareST1SKU1";
        public const string POCShareST2SKU1 = @"POCShareST2SKU1";
        public const string POCsShareChange = @"POCsShareChange";

        // InData constants
        public const string Region_name = @"Region_name";
        public const string District_name = @"District_name";
        public const string DSM_Name = @"DSM_Name";
        public const string Cust_NAME = @"Cust_NAME";
        public const string City_name = @"City_name";
        public const string PTC1SKU1 = @"PTC1SKU1";
        public const string VOL1SKU1 = @"VOL1SKU1";
        public const string PTC1SKU2 = @"PTC1SKU2";
        //public const string PTC2SKU1 = @"PTC2SKU1";
        //public const string PTC2SKU2 = @"PTC2SKU2";
        public const string VOL2SKU1 = @"VOL2SKU1";
        public const string ТТ = @"ТТ";
        public const string OLtype_name = @"OLtype_name";
        public const string OLSubTypeName = @"OLSubTypeName";
        public const string OLName = @"OLName";
        public const string OLAddress = @"OLAddress";
        //public const string D1 = @"D1";
        //public const string D2 = @"D2";
        //public const string D21 = @"D21";
        public const string AVGPTC2SKU1_by_D2 = @"AVGPTC2SKU1_by_D2";
        public const string AVGPTC2SKU2_by_D2 = @"AVGPTC2SKU2_by_D2";
        public const string AVGPTC2SKU1_by_D21 = @"AVGPTC2SKU1_by_D21";
        public const string AVGPTC2SKU2_by_D21 = @"AVGPTC2SKU2_by_D21";

        // 
        public const string ID = @"ID";
        public const string NAME = @"NAME";
        public const string ADRESS = @"ADRESS";
        public const string PARENT_ID = @"PARENT_ID";
        public const string REGION_ID = @"Region_Id";
        public const string REGION_NAME = @"Region_name";
        public const string DISTRICT_ID = @"District_Id";
        public const string DISTRICT_NAME = @"District_name";
        public const string CITY_NAME = @"City_name";

        public const string DATATEXT = @"DataText";

        // statistic fields
        public const string LESS_ZERO = @"lessZero";
        public const string EQUAL_ZERO = @"equalZero";
        public const string GREATER_ZERO = @"greaterZero";

        public const string SKU1PTC1 = @"SKU1PTC1";
        public const string SKU1PTC2 = @"SKU1PTC2";
        public const string SKU1CHANGE = @"SKU1Change";
        public const string SKU1CHANGEPERCENT = @"SKU1ChangePercent";

        public const string SKU2PTC1 = @"SKU2PTC1";
        public const string SKU2PTC2 = @"SKU2PTC2";
        public const string SKU2CHANGE = @"SKU2Change";
        public const string SKU2CHANGEPERCENT = @"SKU2ChangePercent";

        public const string RPTCST1 = @"RPTCST1";
        public const string RPTCST2 = @"RPTCST2";
        public const string RPTCCHANGE = @"RPTCChange";
        public const string RPTCCHANGEPERCENT = @"RPTCChangePercent";

        public const string M2_ID = @"M2_ID";
        public const string M2_NAME = @"M2_Name";
        public const string M3_ID = @"M3_ID";
        public const string M3_NAME = @"M3_NAME";
        public const string M4_ID = @"M4_ID";
        public const string M4_NAME = @"M4_NAME";

        public const string LEVEL = @"LEVEL";
    }
}
