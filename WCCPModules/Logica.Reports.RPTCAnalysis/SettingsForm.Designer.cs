﻿namespace WccpReporting.RPTCAnalysis
{
    partial class SettingsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SettingsForm));
            this.panel1 = new DevExpress.XtraEditors.PanelControl();
            this.btnHelp = new DevExpress.XtraEditors.SimpleButton();
            this.imCollection = new DevExpress.Utils.ImageCollection(this.components);
            this.btnNo = new DevExpress.XtraEditors.SimpleButton();
            this.btnYes = new DevExpress.XtraEditors.SimpleButton();
            this.treeRegion = new DevExpress.XtraTreeList.TreeList();
            this.treeListColumn3 = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.lbDateTo = new DevExpress.XtraEditors.LabelControl();
            this.cbChannelType = new DevExpress.XtraEditors.ComboBoxEdit();
            this.lbChannelType = new DevExpress.XtraEditors.LabelControl();
            this.lbDateFrom = new DevExpress.XtraEditors.LabelControl();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.dateEnd1 = new DevExpress.XtraEditors.DateEdit();
            this.dateStart1 = new DevExpress.XtraEditors.DateEdit();
            this.treeStaffList = new DevExpress.XtraTreeList.TreeList();
            this.treeListColumn1 = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.cmbSKU1 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.cmbSKU2 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.dateEnd2 = new DevExpress.XtraEditors.DateEdit();
            this.dateStart2 = new DevExpress.XtraEditors.DateEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.cmbCustomers = new DevExpress.XtraEditors.ComboBoxEdit();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.cmbCategoryTT = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.cmbSubTypeTT = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.cmbTypeTT = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.txtStep = new DevExpress.XtraEditors.SpinEdit();
            this.treeListSKU1 = new DevExpress.XtraTreeList.TreeList();
            this.treeListColumn2 = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.treeListSKU2 = new DevExpress.XtraTreeList.TreeList();
            this.treeListColumn4 = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.panel1)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imCollection)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeRegion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbChannelType.Properties)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateEnd1.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEnd1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateStart1.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateStart1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeStaffList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbSKU1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbSKU2.Properties)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateEnd2.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEnd2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateStart2.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateStart2.Properties)).BeginInit();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmbCustomers.Properties)).BeginInit();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmbCategoryTT.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbSubTypeTT.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbTypeTT.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStep.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeListSKU1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeListSKU2)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.panel1.Appearance.Options.UseBackColor = true;
            this.panel1.Controls.Add(this.btnHelp);
            this.panel1.Controls.Add(this.btnNo);
            this.panel1.Controls.Add(this.btnYes);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 455);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(893, 36);
            this.panel1.TabIndex = 0;
            // 
            // btnHelp
            // 
            this.btnHelp.Enabled = false;
            this.btnHelp.ImageIndex = 0;
            this.btnHelp.ImageList = this.imCollection;
            this.btnHelp.Location = new System.Drawing.Point(804, 5);
            this.btnHelp.Name = "btnHelp";
            this.btnHelp.Size = new System.Drawing.Size(75, 25);
            this.btnHelp.TabIndex = 2;
            this.btnHelp.Text = "&Справка";
            // 
            // imCollection
            // 
            this.imCollection.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imCollection.ImageStream")));
            this.imCollection.Images.SetKeyName(0, "help_24.png");
            this.imCollection.Images.SetKeyName(1, "check_24.png");
            this.imCollection.Images.SetKeyName(2, "close_24.png");
            // 
            // btnNo
            // 
            this.btnNo.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnNo.ImageIndex = 2;
            this.btnNo.ImageList = this.imCollection;
            this.btnNo.Location = new System.Drawing.Point(723, 5);
            this.btnNo.Name = "btnNo";
            this.btnNo.Size = new System.Drawing.Size(75, 25);
            this.btnNo.TabIndex = 1;
            this.btnNo.Text = "Н&ет";
            // 
            // btnYes
            // 
            this.btnYes.ImageIndex = 1;
            this.btnYes.ImageList = this.imCollection;
            this.btnYes.Location = new System.Drawing.Point(642, 5);
            this.btnYes.Name = "btnYes";
            this.btnYes.Size = new System.Drawing.Size(75, 25);
            this.btnYes.TabIndex = 0;
            this.btnYes.Text = "&Да";
            this.btnYes.Click += new System.EventHandler(this.btnYes_Click);
            // 
            // treeRegion
            // 
            this.treeRegion.BestFitVisibleOnly = true;
            this.treeRegion.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.treeListColumn3});
            this.treeRegion.Location = new System.Drawing.Point(13, 135);
            this.treeRegion.Name = "treeRegion";
            this.treeRegion.OptionsSelection.InvertSelection = true;
            this.treeRegion.OptionsSelection.UseIndicatorForSelection = true;
            this.treeRegion.OptionsView.AutoWidth = false;
            this.treeRegion.OptionsView.ShowCheckBoxes = true;
            this.treeRegion.OptionsView.ShowColumns = false;
            this.treeRegion.OptionsView.ShowIndicator = false;
            this.treeRegion.Size = new System.Drawing.Size(212, 312);
            this.treeRegion.TabIndex = 2;
            this.treeRegion.AfterExpand += new DevExpress.XtraTreeList.NodeEventHandler(this.Tree_NormalizeWidth);
            this.treeRegion.AfterCheckNode += new DevExpress.XtraTreeList.NodeEventHandler(this.Tree_AfterCheckNode);
            // 
            // treeListColumn3
            // 
            this.treeListColumn3.Caption = "treeListColumn1";
            this.treeListColumn3.FieldName = "treeListColumn1";
            this.treeListColumn3.MinWidth = 200;
            this.treeListColumn3.Name = "treeListColumn3";
            this.treeListColumn3.OptionsColumn.AllowEdit = false;
            this.treeListColumn3.OptionsColumn.AllowMove = false;
            this.treeListColumn3.OptionsColumn.AllowMoveToCustomizationForm = false;
            this.treeListColumn3.OptionsColumn.ReadOnly = true;
            this.treeListColumn3.OptionsColumn.ShowInCustomizationForm = false;
            this.treeListColumn3.Visible = true;
            this.treeListColumn3.VisibleIndex = 0;
            this.treeListColumn3.Width = 200;
            // 
            // lbDateTo
            // 
            this.lbDateTo.Location = new System.Drawing.Point(6, 53);
            this.lbDateTo.Name = "lbDateTo";
            this.lbDateTo.Size = new System.Drawing.Size(87, 13);
            this.lbDateTo.TabIndex = 19;
            this.lbDateTo.Text = "Дата окончания:";
            // 
            // cbChannelType
            // 
            this.cbChannelType.Location = new System.Drawing.Point(331, 109);
            this.cbChannelType.Name = "cbChannelType";
            this.cbChannelType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbChannelType.Properties.PopupSizeable = true;
            this.cbChannelType.Size = new System.Drawing.Size(100, 20);
            this.cbChannelType.TabIndex = 18;
            this.cbChannelType.SelectedIndexChanged += new System.EventHandler(this.cbChannelType_SelectedIndexChanged);
            // 
            // lbChannelType
            // 
            this.lbChannelType.Location = new System.Drawing.Point(237, 112);
            this.lbChannelType.Name = "lbChannelType";
            this.lbChannelType.Size = new System.Drawing.Size(61, 13);
            this.lbChannelType.TabIndex = 17;
            this.lbChannelType.Text = "Тип канала:";
            // 
            // lbDateFrom
            // 
            this.lbDateFrom.Location = new System.Drawing.Point(6, 25);
            this.lbDateFrom.Name = "lbDateFrom";
            this.lbDateFrom.Size = new System.Drawing.Size(69, 13);
            this.lbDateFrom.TabIndex = 16;
            this.lbDateFrom.Text = "Дата начала:";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.dateEnd1);
            this.groupBox1.Controls.Add(this.dateStart1);
            this.groupBox1.Controls.Add(this.lbDateFrom);
            this.groupBox1.Controls.Add(this.lbDateTo);
            this.groupBox1.Location = new System.Drawing.Point(13, 13);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(212, 86);
            this.groupBox1.TabIndex = 24;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = " Период 1";
            // 
            // dateEnd1
            // 
            this.dateEnd1.EditValue = null;
            this.dateEnd1.Location = new System.Drawing.Point(100, 50);
            this.dateEnd1.Name = "dateEnd1";
            this.dateEnd1.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.dateEnd1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEnd1.Properties.ValidateOnEnterKey = true;
            this.dateEnd1.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEnd1.Size = new System.Drawing.Size(100, 20);
            this.dateEnd1.TabIndex = 21;
            // 
            // dateStart1
            // 
            this.dateStart1.EditValue = null;
            this.dateStart1.Location = new System.Drawing.Point(100, 22);
            this.dateStart1.Name = "dateStart1";
            this.dateStart1.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.dateStart1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateStart1.Properties.ValidateOnEnterKey = true;
            this.dateStart1.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateStart1.Size = new System.Drawing.Size(100, 20);
            this.dateStart1.TabIndex = 20;
            // 
            // treeStaffList
            // 
            this.treeStaffList.BestFitVisibleOnly = true;
            this.treeStaffList.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.treeListColumn1});
            this.treeStaffList.Location = new System.Drawing.Point(231, 135);
            this.treeStaffList.Name = "treeStaffList";
            this.treeStaffList.OptionsSelection.InvertSelection = true;
            this.treeStaffList.OptionsSelection.UseIndicatorForSelection = true;
            this.treeStaffList.OptionsView.AutoWidth = false;
            this.treeStaffList.OptionsView.ShowCheckBoxes = true;
            this.treeStaffList.OptionsView.ShowColumns = false;
            this.treeStaffList.OptionsView.ShowIndicator = false;
            this.treeStaffList.Size = new System.Drawing.Size(212, 312);
            this.treeStaffList.TabIndex = 25;
            this.treeStaffList.AfterExpand += new DevExpress.XtraTreeList.NodeEventHandler(this.Tree_NormalizeWidth);
            this.treeStaffList.AfterCheckNode += new DevExpress.XtraTreeList.NodeEventHandler(this.Tree_AfterCheckNode);
            // 
            // treeListColumn1
            // 
            this.treeListColumn1.Caption = "treeListColumn1";
            this.treeListColumn1.FieldName = "treeListColumn1";
            this.treeListColumn1.MinWidth = 200;
            this.treeListColumn1.Name = "treeListColumn1";
            this.treeListColumn1.OptionsColumn.AllowEdit = false;
            this.treeListColumn1.OptionsColumn.AllowMove = false;
            this.treeListColumn1.OptionsColumn.AllowMoveToCustomizationForm = false;
            this.treeListColumn1.OptionsColumn.ReadOnly = true;
            this.treeListColumn1.OptionsColumn.ShowInCustomizationForm = false;
            this.treeListColumn1.Visible = true;
            this.treeListColumn1.VisibleIndex = 0;
            this.treeListColumn1.Width = 200;
            // 
            // cmbSKU1
            // 
            this.cmbSKU1.Location = new System.Drawing.Point(47, 21);
            this.cmbSKU1.Name = "cmbSKU1";
            this.cmbSKU1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbSKU1.Properties.PopupSizeable = true;
            this.cmbSKU1.Size = new System.Drawing.Size(153, 20);
            this.cmbSKU1.TabIndex = 27;
            this.cmbSKU1.TabStop = false;
            this.cmbSKU1.Visible = false;
            this.cmbSKU1.SelectedIndexChanged += new System.EventHandler(this.comboBoxEdit1_SelectedIndexChanged);
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(449, 116);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(32, 13);
            this.labelControl1.TabIndex = 26;
            this.labelControl1.Text = "SKU 1:";
            // 
            // cmbSKU2
            // 
            this.cmbSKU2.Location = new System.Drawing.Point(47, 47);
            this.cmbSKU2.Name = "cmbSKU2";
            this.cmbSKU2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbSKU2.Properties.PopupSizeable = true;
            this.cmbSKU2.Size = new System.Drawing.Size(153, 20);
            this.cmbSKU2.TabIndex = 29;
            this.cmbSKU2.TabStop = false;
            this.cmbSKU2.Visible = false;
            this.cmbSKU2.SelectedIndexChanged += new System.EventHandler(this.comboBoxEdit2_SelectedIndexChanged);
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(667, 116);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(32, 13);
            this.labelControl2.TabIndex = 28;
            this.labelControl2.Text = "SKU 2:";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.dateEnd2);
            this.groupBox2.Controls.Add(this.dateStart2);
            this.groupBox2.Controls.Add(this.labelControl3);
            this.groupBox2.Controls.Add(this.labelControl4);
            this.groupBox2.Location = new System.Drawing.Point(231, 13);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(212, 86);
            this.groupBox2.TabIndex = 25;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = " Период 2";
            // 
            // dateEnd2
            // 
            this.dateEnd2.EditValue = null;
            this.dateEnd2.Location = new System.Drawing.Point(100, 48);
            this.dateEnd2.Name = "dateEnd2";
            this.dateEnd2.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.dateEnd2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEnd2.Properties.ValidateOnEnterKey = true;
            this.dateEnd2.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEnd2.Size = new System.Drawing.Size(100, 20);
            this.dateEnd2.TabIndex = 23;
            // 
            // dateStart2
            // 
            this.dateStart2.EditValue = null;
            this.dateStart2.Location = new System.Drawing.Point(100, 22);
            this.dateStart2.Name = "dateStart2";
            this.dateStart2.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.dateStart2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateStart2.Properties.ValidateOnEnterKey = true;
            this.dateStart2.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateStart2.Size = new System.Drawing.Size(100, 20);
            this.dateStart2.TabIndex = 22;
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(6, 25);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(69, 13);
            this.labelControl3.TabIndex = 16;
            this.labelControl3.Text = "Дата начала:";
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(6, 51);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(87, 13);
            this.labelControl4.TabIndex = 19;
            this.labelControl4.Text = "Дата окончания:";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.cmbSKU2);
            this.groupBox3.Controls.Add(this.cmbSKU1);
            this.groupBox3.Location = new System.Drawing.Point(19, 509);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(212, 81);
            this.groupBox3.TabIndex = 30;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = " Продукти ";
            this.groupBox3.Visible = false;
            // 
            // labelControl5
            // 
            this.labelControl5.Location = new System.Drawing.Point(452, 66);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(41, 13);
            this.labelControl5.TabIndex = 31;
            this.labelControl5.Text = "Клиент:";
            // 
            // cmbCustomers
            // 
            this.cmbCustomers.Location = new System.Drawing.Point(499, 63);
            this.cmbCustomers.Name = "cmbCustomers";
            this.cmbCustomers.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbCustomers.Properties.PopupSizeable = true;
            this.cmbCustomers.Size = new System.Drawing.Size(153, 20);
            this.cmbCustomers.TabIndex = 32;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.cmbCategoryTT);
            this.groupBox5.Controls.Add(this.labelControl9);
            this.groupBox5.Controls.Add(this.cmbSubTypeTT);
            this.groupBox5.Controls.Add(this.labelControl10);
            this.groupBox5.Controls.Add(this.cmbTypeTT);
            this.groupBox5.Controls.Add(this.labelControl11);
            this.groupBox5.Location = new System.Drawing.Point(667, 13);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(212, 104);
            this.groupBox5.TabIndex = 35;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = " Форма ТТ ";
            // 
            // cmbCategoryTT
            // 
            this.cmbCategoryTT.Location = new System.Drawing.Point(74, 73);
            this.cmbCategoryTT.Name = "cmbCategoryTT";
            this.cmbCategoryTT.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbCategoryTT.Properties.Items.AddRange(new object[] {
            "Все",
            "A",
            "B",
            "C",
            "D",
            "E"});
            this.cmbCategoryTT.Properties.PopupSizeable = true;
            this.cmbCategoryTT.Size = new System.Drawing.Size(126, 20);
            this.cmbCategoryTT.TabIndex = 38;
            // 
            // labelControl9
            // 
            this.labelControl9.Location = new System.Drawing.Point(7, 76);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(58, 13);
            this.labelControl9.TabIndex = 37;
            this.labelControl9.Text = "Категория:";
            // 
            // cmbSubTypeTT
            // 
            this.cmbSubTypeTT.Location = new System.Drawing.Point(74, 47);
            this.cmbSubTypeTT.Name = "cmbSubTypeTT";
            this.cmbSubTypeTT.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbSubTypeTT.Properties.PopupSizeable = true;
            this.cmbSubTypeTT.Size = new System.Drawing.Size(126, 20);
            this.cmbSubTypeTT.TabIndex = 36;
            // 
            // labelControl10
            // 
            this.labelControl10.Location = new System.Drawing.Point(7, 50);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(42, 13);
            this.labelControl10.TabIndex = 35;
            this.labelControl10.Text = "Подтип:";
            // 
            // cmbTypeTT
            // 
            this.cmbTypeTT.Location = new System.Drawing.Point(74, 20);
            this.cmbTypeTT.Name = "cmbTypeTT";
            this.cmbTypeTT.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbTypeTT.Properties.PopupSizeable = true;
            this.cmbTypeTT.Size = new System.Drawing.Size(126, 20);
            this.cmbTypeTT.TabIndex = 34;
            this.cmbTypeTT.SelectedIndexChanged += new System.EventHandler(this.cmbTypeTT_SelectedIndexChanged);
            // 
            // labelControl11
            // 
            this.labelControl11.Location = new System.Drawing.Point(7, 23);
            this.labelControl11.Name = "labelControl11";
            this.labelControl11.Size = new System.Drawing.Size(22, 13);
            this.labelControl11.TabIndex = 33;
            this.labelControl11.Text = "Тип:";
            // 
            // labelControl6
            // 
            this.labelControl6.Location = new System.Drawing.Point(452, 38);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(26, 13);
            this.labelControl6.TabIndex = 36;
            this.labelControl6.Text = "Step:";
            // 
            // txtStep
            // 
            this.txtStep.EditValue = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.txtStep.Location = new System.Drawing.Point(552, 35);
            this.txtStep.Name = "txtStep";
            this.txtStep.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.txtStep.Properties.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.txtStep.Properties.Mask.EditMask = "n2";
            this.txtStep.Properties.MaxValue = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.txtStep.Properties.MinValue = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.txtStep.Size = new System.Drawing.Size(100, 20);
            this.txtStep.TabIndex = 38;
            // 
            // treeListSKU1
            // 
            this.treeListSKU1.BestFitVisibleOnly = true;
            this.treeListSKU1.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.treeListColumn2});
            this.treeListSKU1.Location = new System.Drawing.Point(449, 135);
            this.treeListSKU1.Name = "treeListSKU1";
            this.treeListSKU1.OptionsSelection.InvertSelection = true;
            this.treeListSKU1.OptionsSelection.UseIndicatorForSelection = true;
            this.treeListSKU1.OptionsView.AutoWidth = false;
            this.treeListSKU1.OptionsView.ShowCheckBoxes = true;
            this.treeListSKU1.OptionsView.ShowColumns = false;
            this.treeListSKU1.OptionsView.ShowIndicator = false;
            this.treeListSKU1.Size = new System.Drawing.Size(212, 312);
            this.treeListSKU1.TabIndex = 39;
            this.treeListSKU1.AfterExpand += new DevExpress.XtraTreeList.NodeEventHandler(this.Tree_NormalizeWidth);
            this.treeListSKU1.AfterCheckNode += new DevExpress.XtraTreeList.NodeEventHandler(this.Tree_AfterCheckNode);
            // 
            // treeListColumn2
            // 
            this.treeListColumn2.Caption = "treeListColumn1";
            this.treeListColumn2.FieldName = "treeListColumn1";
            this.treeListColumn2.MinWidth = 200;
            this.treeListColumn2.Name = "treeListColumn2";
            this.treeListColumn2.OptionsColumn.AllowEdit = false;
            this.treeListColumn2.OptionsColumn.AllowMove = false;
            this.treeListColumn2.OptionsColumn.AllowMoveToCustomizationForm = false;
            this.treeListColumn2.OptionsColumn.ReadOnly = true;
            this.treeListColumn2.OptionsColumn.ShowInCustomizationForm = false;
            this.treeListColumn2.Visible = true;
            this.treeListColumn2.VisibleIndex = 0;
            this.treeListColumn2.Width = 200;
            // 
            // treeListSKU2
            // 
            this.treeListSKU2.BestFitVisibleOnly = true;
            this.treeListSKU2.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.treeListColumn4});
            this.treeListSKU2.Location = new System.Drawing.Point(667, 135);
            this.treeListSKU2.Name = "treeListSKU2";
            this.treeListSKU2.OptionsSelection.InvertSelection = true;
            this.treeListSKU2.OptionsSelection.UseIndicatorForSelection = true;
            this.treeListSKU2.OptionsView.AutoWidth = false;
            this.treeListSKU2.OptionsView.ShowCheckBoxes = true;
            this.treeListSKU2.OptionsView.ShowColumns = false;
            this.treeListSKU2.OptionsView.ShowIndicator = false;
            this.treeListSKU2.Size = new System.Drawing.Size(212, 312);
            this.treeListSKU2.TabIndex = 40;
            this.treeListSKU2.AfterExpand += new DevExpress.XtraTreeList.NodeEventHandler(this.Tree_NormalizeWidth);
            this.treeListSKU2.AfterCheckNode += new DevExpress.XtraTreeList.NodeEventHandler(this.Tree_AfterCheckNode);
            // 
            // treeListColumn4
            // 
            this.treeListColumn4.Caption = "treeListColumn1";
            this.treeListColumn4.FieldName = "treeListColumn1";
            this.treeListColumn4.MinWidth = 200;
            this.treeListColumn4.Name = "treeListColumn4";
            this.treeListColumn4.OptionsColumn.AllowEdit = false;
            this.treeListColumn4.OptionsColumn.AllowMove = false;
            this.treeListColumn4.OptionsColumn.AllowMoveToCustomizationForm = false;
            this.treeListColumn4.OptionsColumn.ReadOnly = true;
            this.treeListColumn4.OptionsColumn.ShowInCustomizationForm = false;
            this.treeListColumn4.Visible = true;
            this.treeListColumn4.VisibleIndex = 0;
            this.treeListColumn4.Width = 200;
            // 
            // labelControl7
            // 
            this.labelControl7.Location = new System.Drawing.Point(13, 116);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(39, 13);
            this.labelControl7.TabIndex = 41;
            this.labelControl7.Text = "Регион:";
            // 
            // SettingsForm
            // 
            this.AcceptButton = this.btnYes;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnNo;
            this.ClientSize = new System.Drawing.Size(893, 491);
            this.ControlBox = false;
            this.Controls.Add(this.labelControl7);
            this.Controls.Add(this.treeListSKU2);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.treeListSKU1);
            this.Controls.Add(this.txtStep);
            this.Controls.Add(this.labelControl6);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.cmbCustomers);
            this.Controls.Add(this.labelControl5);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.treeStaffList);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.cbChannelType);
            this.Controls.Add(this.lbChannelType);
            this.Controls.Add(this.treeRegion);
            this.Controls.Add(this.panel1);
            this.MaximizeBox = false;
            this.Name = "SettingsForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Параметры";
            this.Load += new System.EventHandler(this.SettingsForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.panel1)).EndInit();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.imCollection)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeRegion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbChannelType.Properties)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateEnd1.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEnd1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateStart1.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateStart1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeStaffList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbSKU1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbSKU2.Properties)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateEnd2.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEnd2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateStart2.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateStart2.Properties)).EndInit();
            this.groupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmbCustomers.Properties)).EndInit();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmbCategoryTT.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbSubTypeTT.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbTypeTT.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStep.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeListSKU1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeListSKU2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        
        #endregion

        private DevExpress.Utils.ImageCollection imCollection;
        private DevExpress.XtraEditors.SimpleButton btnHelp;
        private DevExpress.XtraEditors.SimpleButton btnNo;
        private DevExpress.XtraEditors.SimpleButton btnYes;
        private DevExpress.XtraEditors.PanelControl panel1;
        private DevExpress.XtraTreeList.TreeList treeRegion;
        private DevExpress.XtraTreeList.Columns.TreeListColumn treeListColumn3;
        private DevExpress.XtraEditors.LabelControl lbDateTo;
        private DevExpress.XtraEditors.ComboBoxEdit cbChannelType;
        private DevExpress.XtraEditors.LabelControl lbChannelType;
        private DevExpress.XtraEditors.LabelControl lbDateFrom;
        private System.Windows.Forms.GroupBox groupBox1;
        private DevExpress.XtraTreeList.TreeList treeStaffList;
        private DevExpress.XtraTreeList.Columns.TreeListColumn treeListColumn1;
        private DevExpress.XtraEditors.ComboBoxEdit cmbSKU1;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.ComboBoxEdit cmbSKU2;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private System.Windows.Forms.GroupBox groupBox2;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private System.Windows.Forms.GroupBox groupBox3;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.DateEdit dateStart1;
        private DevExpress.XtraEditors.DateEdit dateEnd1;
        private DevExpress.XtraEditors.DateEdit dateEnd2;
        private DevExpress.XtraEditors.DateEdit dateStart2;
        private DevExpress.XtraEditors.ComboBoxEdit cmbCustomers;
        private System.Windows.Forms.GroupBox groupBox5;
        private DevExpress.XtraEditors.ComboBoxEdit cmbCategoryTT;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.ComboBoxEdit cmbSubTypeTT;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraEditors.ComboBoxEdit cmbTypeTT;
        private DevExpress.XtraEditors.LabelControl labelControl11;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.SpinEdit txtStep;
        private DevExpress.XtraTreeList.TreeList treeListSKU1;
        private DevExpress.XtraTreeList.Columns.TreeListColumn treeListColumn2;
        private DevExpress.XtraTreeList.TreeList treeListSKU2;
        private DevExpress.XtraTreeList.Columns.TreeListColumn treeListColumn4;
        private DevExpress.XtraEditors.LabelControl labelControl7;
    }
}