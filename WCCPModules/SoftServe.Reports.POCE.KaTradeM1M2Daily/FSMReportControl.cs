﻿using DevExpress.XtraEditors.Repository;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.BandedGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraTab;
using Logica.Reports.BaseReportControl;
using SoftServe.POCE.KaTradeM1M2Daily;
using SoftServe.POCE.KaTradeM1M2Daily.Properties;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Windows.Forms;

namespace WccpReporting
{
    public partial class WccpUIControl : BaseReportUserControl
    {
        #region Readonly & Static Fields

        public static WccpUIControl ReportControl;

        private static List<SettingsForm> forms;

        #endregion

        #region Fields
        private Dictionary<Guid, BandedGridView> m1DaViews = new Dictionary<Guid, BandedGridView>();
        private SettingsForm setForm;
        #endregion

        #region Constructors

        public WccpUIControl(int reportId)
            : base(reportId)
        {
            InitializeComponent();
            PrintingMarginOffest = 60; //set margins to 5 mm.
            Load += WccpUIControl_Load;

            RefreshClick += WccpUIControl_RefreshClick;
            SettingsFormClick += WccpUIControl_SettingsFormClick;
            ReportControl = this;
        }

        #endregion

        #region Instance Methods
        
        public bool IsNeedFitTabToOnePage()
        {
            return true;
        }

        protected override void OnControlRemoved(ControlEventArgs e)
        {
            forms.Remove(setForm);
            base.OnControlRemoved(e);
        }

        private void AdjustM1DailikTab(BaseTab baseTabPage)
        {
            if (baseTabPage.Length > 0)
            {
                var grid = baseTabPage.GetGrid(0);

                int gridHeight = ((DataTable) grid.GridControl.DataSource).Rows.Count*20 + 400;
                    //Sometimes control don't want to obtain new height and keep old one.
                grid.Height = gridHeight;
                grid.MinimumSize = new Size(0, gridHeight);
                grid.MaximumSize = new Size(0, gridHeight);
                var bgv = (BandedGridView) grid.GridControl.DefaultView;

                var repositoryItemMemoEdit = new RepositoryItemMemoEdit();
                repositoryItemMemoEdit.BeginInit();
                grid.GridControl.RepositoryItems.Add(repositoryItemMemoEdit);
                repositoryItemMemoEdit.EndInit();
                for (int i = 0; i < bgv.Columns.Count; i++)
                {
                    var column = bgv.Columns[i];
                    if (column.Visible && (column.FieldName.Equals("OLName") || column.FieldName.Equals("OLDeliveryAddress")))
                    {
                        column.ColumnEdit = repositoryItemMemoEdit;
                    }
                }

                bgv.AppearancePrint.Lines.BackColor = Color.Black;
                bgv.AppearancePrint.Lines.BackColor2 = Color.Black;
                bgv.Appearance.HorzLine.BackColor = Color.Black;
                bgv.Appearance.HorzLine.BackColor2 = Color.Black;
                bgv.Appearance.VertLine.BackColor = Color.Black;
                bgv.Appearance.VertLine.BackColor2 = Color.Black;

                //Add additional events
                bgv.CustomColumnDisplayText += DailikPage_CustomColumnDisplayText;
                bgv.RowCellStyle += DailikPage_RowCellStyle;

                var dataTable = (DataTable)grid.GridControl.DataSource;
                
                m1DaViews.Add(baseTabPage.SheetStructure.Id, bgv);
                baseTabPage.Refresh();
            }
        }
        
        private void AdjustTabForScrolls(BaseTab baseTabPage)
        {
            int minWidth = 0;
            for (int i = 0; i < baseTabPage.Length; i++)
            {
                GridView view = (GridView) baseTabPage.GetGrid(i).GridControl.DefaultView;
                if (!view.OptionsView.ColumnAutoWidth)
                {
                    int width = 0;
                    foreach (GridColumn column in view.VisibleColumns)
                    {
                        width += column.Width;
                    }
                    if (width > minWidth)
                        minWidth = width;
                }
            }
            if (minWidth > 0)
            {
                baseTabPage.AutoScroll = true;
                baseTabPage.AutoScrollMinSize = new Size(minWidth + 4, 0);
            }
        }
        
        private void CustomSettings()
        {
            cmbTab.Visible = true;
            lblTab.Visible = true;
            //btnPrintAll.ItemClick -= btnPrintAll_ItemClick;
            //btnPrintAll.ItemClick += btnPrintAll_ItemClick_M1M2;
            cmbTab.SelectedIndexChanged += SelectedIndexChanged;

            FillComboBox();
        }

        private void FillComboBox()
        {
            cmbTab.Properties.Items.Clear();
            List<XtraTabPage> tmpLst = new List<XtraTabPage>(tabManager.TabPages);
            foreach (XtraTabPage page in tmpLst)
            {
                cmbTab.Properties.Items.Add(new ComboBoxItem {Id = page, Text = page.Text});
            }
            //set first Tab name as combobox Text.
            try
            {
                cmbTab.Text = ((ComboBoxItem) cmbTab.Properties.Items[0]).Text;
            }
            catch (IndexOutOfRangeException)
            {
            }
        }

        private SettingsForm GetForm()
        {
            if (forms != null && forms.Count > 0)
                return forms[forms.Count - 1];
            return null;
        }

        private void PopulateAdditionalPages()
        {
            for (int tabindex = 0; tabindex < tabManager.TabPages.Count; tabindex++)
            {
                BaseTab baseTabPage = (tabManager.TabPages[tabindex] as BaseTab);
                if (baseTabPage == null) continue;
                if (setForm.M1DaTabs.IndexOf(baseTabPage.SheetStructure.Id) > -1)
                    AdjustM1DailikTab(baseTabPage);
                
                AdjustTabForScrolls(baseTabPage);
                baseTabPage.IsNeedFitToPage = IsNeedFitTabToOnePage;
                baseTabPage.AlwaysScrollActiveControlIntoView = false;
            }
        }

        #endregion

        #region Event Handling
        
        private void DailikPage_CustomColumnDisplayText(object sender, CustomColumnDisplayTextEventArgs e)
        {
            if (!IsColorColumn(e.Column)) 
                return;

            e.DisplayText = "";
        }

        private void DailikPage_RowCellStyle(object sender, RowCellStyleEventArgs e)
        {
            if (!IsColorColumn(e.Column)) 
                return;

            BandedGridView bandedGridView = (BandedGridView) sender;
            GridControl gridControl = bandedGridView.GridControl;

            DataTable dataSource = (DataTable)gridControl.DataSource;
            DataRow row = dataSource.Rows[bandedGridView.GetDataSourceRowIndex(e.RowHandle)];

            if (dataSource.Columns.Contains(e.Column.FieldName) && row.IsNull(e.Column.FieldName)) 
                return;

            if (dataSource.Columns.Contains(e.Column.FieldName) && row.Field<int>(e.Column.FieldName) == 0)
            {
                e.Appearance.BackColor = Color.Gainsboro;
                e.Appearance.BackColor2 = Color.Gainsboro;
            }
        }
        
        private void SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!cmbTab.Text.Equals(Resources.All))
                tabManager.SelectedTabPage = ((XtraTabPage) ((ComboBoxItem) cmbTab.SelectedItem).Id);
        }

        private void WccpUIControl_Load(object sender, EventArgs e)
        {
            if (GetForm() != null)
            {
                setForm = GetForm();
                setForm.SetParent(this);
                off_reports = true;
                UpdateSheets(setForm.SheetParamsList);
                PopulateAdditionalPages();
                CustomSettings();
            }
        }

        private void WccpUIControl_RefreshClick(object sender, XtraTabPage selectedpage)
        {
            BaseTab tab = selectedpage as BaseTab;
            if (tab == null) return;
            
            if (m1DaViews.ContainsKey(tab.SheetStructure.Id))
            {
                m1DaViews[tab.SheetStructure.Id].CustomColumnDisplayText -= DailikPage_CustomColumnDisplayText;
                m1DaViews[tab.SheetStructure.Id].RowCellStyle -= DailikPage_RowCellStyle;
                m1DaViews.Remove(tab.SheetStructure.Id);
                AdjustM1DailikTab(tab);
            }
            AdjustTabForScrolls(tab);
            tab.IsNeedFitToPage = IsNeedFitTabToOnePage;
        }

        private void WccpUIControl_SettingsFormClick(object sender, XtraTabPage selectedPage)
        {
            bool isShowSettings = true;
            
            if (isShowSettings && setForm.ShowDialog() == DialogResult.OK)
            {
                m1DaViews.Clear();

                UpdateSheets(setForm.SheetParamsList);
                PopulateAdditionalPages();
                FillComboBox();
            }
        }

        #endregion

        #region Class Properties

        //private static int ReportID
        //{
        //    get
        //    {
        //        //return WccpUI.myHost.GetReportId(); 
        //        return WCCPAPI.GetReportId(); 
        //    }
        //}

        #endregion

        #region Class Methods

        public static void AddForm(SettingsForm form)
        {
            if (forms == null) forms = new List<SettingsForm>();
            forms.Add(form);
        }

        private static bool IsColorColumn(GridColumn column)
        {
            int tryParseResult;
            return int.TryParse(column.Caption, out tryParseResult);
        }

        #endregion
    }
}