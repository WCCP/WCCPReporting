﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using Logica.Reports.BaseReportControl;
using Logica.Reports.Common.WaitWindow;
using Logica.Reports.ConfigXmlParser.Model;
using SoftServe.POCE.KaTradeM1M2Daily.DataAccess;
using SoftServe.POCE.KaTradeM1M2Daily.Properties;
using Logica.Reports.DataAccess;

namespace SoftServe.POCE.KaTradeM1M2Daily
{
    public partial class SettingsForm : XtraForm
    {
        #region Readonly & Static Fields

        private readonly DataTable dtSuper;

        #endregion

        #region Fields

        private BaseReportUserControl bruc;
        private DataTable dataTableDate;
        private Tab[] tabs;

        #endregion

        #region Constructors

        public SettingsForm(BaseReportUserControl parent)
        {
            InitializeComponent();
            SetParent(parent);

            M1DaTabs = new List<Guid>();
            dtSuper = DataAccessProvider.AllM2;
            FillM2();
            dateEdit.DateTime = DateTime.Now;
            GetUpdData();
            if (!DataAccessLayer.IsLDB)
            {
                grpUpd.Visible = false;
                ResizeForm();
            }
        }

        public SettingsForm()
        {
            InitializeComponent();

            M1DaTabs = new List<Guid>();
            dtSuper = DataAccessProvider.AllM2;
            FillM2();
            dateEdit.DateTime = DateTime.Now;
            GetUpdData();
            if (!DataAccessLayer.IsLDB)
            {
                grpUpd.Visible = false; 
                ResizeForm();
            }
        }

        #endregion

        #region Instance Properties

        public List<SheetParamCollection> SheetParamsList
        {
            get
            {
                bruc.tabManager.TabPages.Clear();
                //additionalPages = new List<DailikTabPage>();
                dataTableDate = DataAccessProvider.GetWorkingDays(dateEdit.DateTime);
                List<SheetParamCollection> list = new List<SheetParamCollection>();
                M1DaTabs.Clear();

                //add and configure M1 tabs 
                bruc.Report.Tabs = tabs;
                foreach (CheckedListBoxItem item in cmb1.Items)
                {
                    if (item.CheckState != CheckState.Checked) continue;

                    Dictionary<Guid, Guid> clonedTabs = bruc.CloneTabs(item.Description);
                    list.Add(FillParamCol(clonedTabs[Constants.M1DaTabId], item.Description, item.Value));
                    M1DaTabs.Add(clonedTabs[Constants.M1DaTabId]);
                }

                return list;
            }
        }

        public List<Guid> M1DaTabs { get; private set; }

        #endregion

        #region Instance Methods

        public void SetParent(BaseReportUserControl parent)
        {
            bruc = parent;
            tabs = parent.Report.Tabs;
        }

        private void AddM1(DataTable dtMerch)
        {
            cmb1.Items.Clear();

            foreach (DataRow dr in dtMerch.Rows)
            {
                cmb1.Items.Add(dr[SqlConstants.col_M1_ID], dr[SqlConstants.col_M1_NAME].ToString(), CheckState.Unchecked, true);
            }
        }

        private void CheckAllM1()
        {
            foreach (CheckedListBoxItem item in cmb1.Items)
            {
                item.CheckState = CheckState.Checked;
            }
        }

        private int CheckedM1Count()
        {
            int count = 0;

            foreach (CheckedListBoxItem item in cmb1.Items)
            {
                if (item.CheckState == CheckState.Checked)
                {
                    count++;
                }
            }

            return count;
        }

        private void FillM2()
        {
            lookUpEditM2.Properties.DataSource = dtSuper;

            if (dtSuper.Rows.Count > 0)
            {
                lookUpEditM2.EditValue = dtSuper.Rows[0][SqlConstants.col_M2_ID];
            }
        }

        public SheetParamCollection FillParamCol(Guid id, string merchName, object merchId)
        {
            SheetParamCollection paramCollection = new SheetParamCollection
                                                     { 
                                                         TabId = id,
                                                         TableDataType = TableType.Fact
                                                     };

            paramCollection.Add(new SheetParam { SqlParamName = SqlConstants.spDateParam, Value = dateEdit.DateTime.Date, DisplayParamName = Resources.Date, DisplayValue = dateEdit.DateTime.Date.ToShortDateString() });


            paramCollection.Add(new SheetParam
                                      {
                                          DisplayParamName = Resources.WorkDay,
                                          DisplayValue = dataTableDate.Rows[0][SqlConstants.col_WorkDayOfTotalTextFieldName].ToString()
                                      });

            if (!String.IsNullOrEmpty(merchName))
            {
                paramCollection.MainHeader = String.Format(Resources.MainHeader_M1, merchName, lookUpEditM2.Properties.GetDisplayText(lookUpEditM2.EditValue), merchName); //TODO: Последний заменить на ТП
                paramCollection.Add(new SheetParam { SqlParamName = SqlConstants.spMerchIdParam, Value = merchId });

            }
            else
            {
                paramCollection.MainHeader =
                    String.Format(Resources.MainHeader_M2,
                        lookUpEditM2.Properties.GetDisplayText(lookUpEditM2.EditValue));
                paramCollection.Add(new SheetParam { SqlParamName = SqlConstants.spSVIdParam, Value = lookUpEditM2.EditValue });
            }
            if (DataAccessLayer.IsLDB)
            {
                paramCollection.Add(new SheetParam
                {
                    DisplayParamName = Resources.Distributor,
                    DisplayValue = DataAccessProvider.GetDistributorName((int) lookUpEditM2.EditValue)
                });
            }
            return paramCollection;
        }

        #endregion

        #region Event Handling

        private void btnNo_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }

        private void btnYes_Click(object sender, EventArgs e)
        {
            var executionAllowed = DataAccessProvider.IsExecutionAllowed();

            if (!string.IsNullOrEmpty(executionAllowed))
            {
                XtraMessageBox.Show(executionAllowed, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (CheckedM1Count() > 0)
            {
                DialogResult = DialogResult.OK;
            }
            else
            {
                XtraMessageBox.Show("Нужно выбрать хотя бы одного М1", "Параметры", MessageBoxButtons.OK,
                                    MessageBoxIcon.Warning);
            }
        }

        private void lookUpEditM2_EditValueChanged(object sender, EventArgs e)
        {
            if (lookUpEditM2.EditValue != null)
            {
                AddM1(DataAccessProvider.GetM1ByM2(((int)lookUpEditM2.EditValue)));
            }

            CheckAllM1();
        }

        #endregion

        private void btn_upd_Click(object sender, EventArgs e)
        {
            WaitManager.StartWait();
            try
            {
                DataAccessProvider.RecalculateData(dateEdit.DateTime.Date);
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                WaitManager.StopWait();
                GetUpdData();
            }
        }

        private void GetUpdData()
        {
            lblUpd2.Text = (DataAccessProvider.LastRecalcDate ?? "нет данных");
        }

        private void ResizeForm()
        {
            lbDate.Location = new Point(lbDate.Location.X, lbDate.Location.Y - grpUpd.Height);
            dateEdit.Location = new Point(dateEdit.Location.X, dateEdit.Location.Y - grpUpd.Height);
            lblM2.Location = new Point(lblM2.Location.X, lblM2.Location.Y - grpUpd.Height);
            lookUpEditM2.Location = new Point(lookUpEditM2.Location.X, lookUpEditM2.Location.Y - grpUpd.Height);
            groupControl2.Location = new Point(groupControl2.Location.X, groupControl2.Location.Y - grpUpd.Height);
            btnYes.Location = new Point(btnYes.Location.X, btnYes.Location.Y - grpUpd.Height);
            btnNo.Location = new Point(btnNo.Location.X, btnNo.Location.Y - grpUpd.Height);
            this.Size = new Size(this.Size.Width, this.Size.Height - grpUpd.Height);
        }
    }

    internal class ComboBoxItem
    {
        #region Instance Properties

        public object Id { get; set; }

        public string Text { get; set; }

        #endregion

        #region Instance Methods

        public override string ToString()
        {
            return Text;
        }

        #endregion
    }
}