﻿using System;
using System.ComponentModel.Composition;
using System.Windows.Forms;
using Logica.Reports.Common;
using ModularWinApp.Core.Interfaces;
using SoftServe.POCE.KaTradeM1M2Daily;

namespace WccpReporting
{
    public delegate void CloseUIdelegate();

    [Export(typeof(IStartupClass))]
    [ExportMetadata("ModuleName", "wccpPoceKaTradeM1M2Daily.dll")]
    public class WccpUI : IStartupClass
    {
        private WccpUIControl _reportControl;
        private string _reportCaption;

        public Control ReportControl
        {
            get { return _reportControl; }
        }

        public string ReportCaption
        {
            get { return _reportCaption; }
        }

        #region IStartupClass Members

        public string GetVersion()
        {
            return System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();
        }

        public int ShowUI(int reportId, string reportCaption)
        {
            try
            {
                SettingsForm setForm = new SettingsForm();

                if (setForm.ShowDialog() == DialogResult.OK)
                {
                    _reportControl = new WccpUIControl(reportId);
                    _reportCaption = reportCaption;
                    WccpUIControl.AddForm(setForm);

                    return 0;
                }
            }
            catch (Exception ex)
            {
                ErrorManager.ShowErrorBox(ex.Message);
            }

            return 1;
        }

        public void CloseUI() { }

        #endregion

        public bool AllowClose()
        {
            return true;
        }
    }
}
