﻿using System;
using System.Data;
using System.Data.SqlClient;
using BLToolkit.Data;
using Logica.Reports.DataAccess;

namespace SoftServe.POCE.KaTradeM1M2Daily.DataAccess
{
    static class DataAccessProvider
    {
        private static DbManager _db;

        #region Properties

        private static DbManager Db
        {
            get
            {
                if (_db == null)
                {
                    DbManager.DefaultConfiguration = ""; //to reset possible previous changes
                    DbManager.AddConnectionString(HostConfiguration.DBSqlConnection);
                    _db = new DbManager();
                    _db.Command.CommandTimeout = 15 * 60; // 15 minutes by default
                }
                else if (!HostConfiguration.DBSqlConnection.Contains(_db.Connection.ConnectionString))
                {
                    _db.Close();
                    DbManager.DefaultConfiguration = ""; //to reset possible previous changes
                    DbManager.AddConnectionString(HostConfiguration.DBSqlConnection);
                    _db = new DbManager();
                    _db.Command.CommandTimeout = 15 * 60; // 15 minutes by default
                }
                 return _db;
            }
        }

        #endregion

        public static DataTable AllM2
        {
            get
            {
                DataTable res = Db.SetSpCommand(SqlConstants.SpGetListOfM1OrM2,
                Db.Parameter(SqlConstants.SpGetListOfM1OrM2_M_TYPE, SqlConstants.SpGetListOfM1OrM2_param_M2),
                Db.Parameter(SqlConstants.SpGetListOfM1OrM2_ID, DBNull.Value)
                ).ExecuteDataTable();

                return res;
            }
        }

        public static string LastRecalcDate
        {
            get
            {
                DateTime lastDataUpdate = Db.SetSpCommand(SqlConstants.SpGetLastRecalcDate).ExecuteScalar<DateTime>();
                return lastDataUpdate.ToString();
            }

        }

        public static void RecalculateData(DateTime date)
        {
            try
            {
                DateTime lDateMonthBeginning = new DateTime(date.Year, date.Month, 1);
                DateTime lDateMonthEnd = lDateMonthBeginning.AddMonths(1).AddDays(-1);

                Db.SetSpCommand(SqlConstants.SpRecalculateData,
                    Db.Parameter(SqlConstants.SpRecalculateDataParam1, lDateMonthBeginning),
                    Db.Parameter(SqlConstants.SpRecalculateDataParam2, lDateMonthEnd)
                    ).ExecuteNonQuery();
            }
            catch (Exception lException)
            {
                if (lException.InnerException is SqlException)
                {
                    var lSqlException = (SqlException)lException.InnerException;
                    if (lSqlException.State == 100 && lSqlException.Class == 15)
                    {
                        throw new ApplicationException(lSqlException.Message, lSqlException);
                    }
                }
                throw;
            }
        }

        internal static string IsExecutionAllowed()
        {
            string result = null;
            try
            {
                Db.SetSpCommand(SqlConstants.SpIsReportExtcutionAllowed).ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                if (ex.InnerException is SqlException
                    && (((SqlException)ex.InnerException).State == 100
                    && ((SqlException)ex.InnerException).Class == 15))
                {
                    result = ex.InnerException.Message;
                }
                else
                {
                    throw;
                }
            }
            return result;
        }

        public static DataTable GetM1ByM2(object supervisorId)
        {
            DataTable res = Db.SetSpCommand(SqlConstants.SpGetListOfM1OrM2,
                Db.Parameter(SqlConstants.SpGetListOfM1OrM2_M_TYPE, SqlConstants.SpGetListOfM1OrM2_param_M1),
                Db.Parameter(SqlConstants.SpGetListOfM1OrM2_ID, supervisorId)
                ).ExecuteDataTable();

            return res;
        }

        public static string GetDistributorName(int supervisorId)
        {
            string distributorName = Db.SetSpCommand(SqlConstants.SpGetDistributorName,
                Db.Parameter(SqlConstants.SpGetDistributorName_SPV_ID, supervisorId)
                ).ExecuteScalar<string>();

            return distributorName;
        }

        public static DataTable GetWorkingDays(DateTime date)
        {
            DataTable res = Db.SetSpCommand(SqlConstants.SpGetWorkingDays,
                Db.Parameter(SqlConstants.SpGetWorkingDays_Date, date)
                ).ExecuteDataTable();

            return res;
        }
    }
}
