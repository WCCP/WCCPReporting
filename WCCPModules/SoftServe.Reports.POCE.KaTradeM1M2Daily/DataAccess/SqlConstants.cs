﻿namespace SoftServe.POCE.KaTradeM1M2Daily.DataAccess
{
    static class SqlConstants
    {
        public const string SpGetLastRecalcDate = "spDW_M_KaTrade_Get_UpdateTime";
        
        public const string SpRecalculateData = "POCE.PIVOT_Recalc_KA";
        public const string SpRecalculateDataParam1 = "Begin_Date";
        public const string SpRecalculateDataParam2 = "End_Date";

        public const string SpIsReportExtcutionAllowed = "POCE.Check_Run_POCE_KA_Reports";

        public const string SpGetListOfM1OrM2 = "spDW_M_KaTrade_OR_GetListM";
        public const string SpGetListOfM1OrM2_M_TYPE = "MType";
        public const string SpGetListOfM1OrM2_ID = "SPV_id";
        public const string SpGetListOfM1OrM2_param_M1 = "M1";
        public const string SpGetListOfM1OrM2_param_M2 = "M2";

        public const string col_M1_ID = "Merch_id";
        public const string col_M1_NAME = "MerchName";

        public const string col_M2_ID = "Supervisor_id";
        public const string col_M2_NAME = "Supervisor_name";

        public const string SpGetWorkingDays = "spDW_WorkingDays";
        public const string SpGetWorkingDays_Date = "Date";
        public const string col_CURRENT_DATE = "CurrentDate";
        public const string col_WORK_DAYS_IN_MONTH = "WorkDaysInMonth";
        public const string col_WORK_DAY = "WorkDay";
        public const string col_WorkDayOfTotalTextFieldName = "WorkDayOfTotalText";


        public const string SpGetDistributorName = "spDW_M_OffTrade_OR_GetCustName";
        public const string SpGetDistributorName_SPV_ID = "SPV_ID";
        public const string col_NAME = "Cust_NAME";

        public const string spDateParam = "@date";
        public const string spMerchIdParam = "@Merch_ID";
        public const string spSVIdParam = "@SPV_ID";
    }
}
