﻿using System;
using System.ComponentModel.Composition;
using System.Windows.Forms;
using Logica.Reports.Common;
using Logica.Reports.DataAccess;
using SoftServe.Reports.SmashTargetReport;
using SoftServe.Reports.SmashTargetReport.Properties;
using ModularWinApp.Core.Interfaces;

namespace WccpReporting
{
    [Export(typeof(IStartupClass))]
    [ExportMetadata("ModuleName", "wccpsmashtargetreport.dll")]
        public class WccpUI : IStartupClass
    {
        #region IStartupClass Members

        private WccpUIControl _reportControl;
        private string _reportCaption;

        public Control ReportControl
        {
            get { return _reportControl; }
        }

        public string ReportCaption
        {
            get { return _reportCaption; }
        }

        public string GetVersion()
        {
            return System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();
        }

        public int ShowUI(int reportId, string reportCaption)
        {

            try
            {
                DataAccessLayer.OpenConnection();
                SettingsForm setForm = new SettingsForm();

                if (setForm.ShowDialog() == DialogResult.OK)
                {
                    WccpUIControl.SettingsForm = setForm;

                    _reportControl = new WccpUIControl();
                    _reportCaption = Resources.Execution;

                    return 0;
                }
            }
            catch (Exception ex)
            {
                ErrorManager.ShowErrorBox(ex.Message);
            }
            return 1;
        }

        public void CloseUI()
        {
            //throw new NotImplementedException();
        }

        public bool AllowClose()
        {
            return true;
        }
        #endregion
    }
}