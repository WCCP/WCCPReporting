﻿using DevExpress.XtraPivotGrid;

namespace SoftServe.Reports.SmashTargetReport.UserControl
{
  partial class SmashReport
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
            DevExpress.XtraCharts.XYDiagram xyDiagram1 = new DevExpress.XtraCharts.XYDiagram();
            DevExpress.XtraCharts.SideBySideBarSeriesLabel sideBySideBarSeriesLabel1 = new DevExpress.XtraCharts.SideBySideBarSeriesLabel();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.chartControl = new DevExpress.XtraCharts.ChartControl();
            this.pivotGridControl = new DevExpress.XtraPivotGrid.PivotGridControl();
            this.pgfStaffMonthID = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pgfStaffID = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pgfM2 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pgfM3 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pgfM1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pgfRegion = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pgfPersonalLevel = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pgfFIO = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pgfYear = new DevExpress.XtraPivotGrid.PivotGridField();
            this.PgfMonth = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pgfPersonalCount = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pgfPersonalPercent = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pgfMaxZP = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pgfFactZP = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pgfZPAchivement = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pgfZPAchivementsRange = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pgfM3Channel = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pgfPersonalChannel = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pgfPersonalType = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pgfBlockbaster = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pgfSecondV = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pgfSecondVRange = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pgfGoal = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pgfGoalRange = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pgfValue = new DevExpress.XtraPivotGrid.PivotGridField();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.splitterItem1 = new DevExpress.XtraLayout.SplitterItem();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chartControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(xyDiagram1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(sideBySideBarSeriesLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pivotGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem1)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.chartControl);
            this.layoutControl1.Controls.Add(this.pivotGridControl);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(1058, 520);
            this.layoutControl1.TabIndex = 6;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // chartControl
            // 
            this.chartControl.DataSource = this.pivotGridControl;
            xyDiagram1.AxisX.Title.Text = "Диапазон % достиж. ЗП";
            xyDiagram1.AxisX.VisibleInPanesSerializable = "-1";
            xyDiagram1.AxisX.WholeRange.AutoSideMargins = true;
            xyDiagram1.AxisY.Title.Text = "Кол-во персонала % персонала";
            xyDiagram1.AxisY.VisibleInPanesSerializable = "-1";
            xyDiagram1.AxisY.WholeRange.AutoSideMargins = true;
            this.chartControl.Diagram = xyDiagram1;
            this.chartControl.EmptyChartText.Text = "";
            this.chartControl.Legend.MaxHorizontalPercentage = 30D;
            this.chartControl.Location = new System.Drawing.Point(7, 7);
            this.chartControl.Name = "chartControl";
            this.chartControl.OptionsPrint.SizeMode = DevExpress.XtraCharts.Printing.PrintSizeMode.Stretch;
            this.chartControl.SeriesDataMember = "Series";
            this.chartControl.SeriesSerializable = new DevExpress.XtraCharts.Series[0];
            this.chartControl.SeriesTemplate.ArgumentDataMember = "Arguments";
            this.chartControl.SeriesTemplate.ArgumentScaleType = DevExpress.XtraCharts.ScaleType.Qualitative;
            sideBySideBarSeriesLabel1.LineVisibility = DevExpress.Utils.DefaultBoolean.True;
            this.chartControl.SeriesTemplate.Label = sideBySideBarSeriesLabel1;
            this.chartControl.SeriesTemplate.ValueDataMembersSerializable = "Values";
            this.chartControl.Size = new System.Drawing.Size(1044, 210);
            this.chartControl.SmallChartText.Text = "Increase the chart\'s size,\r\nto view its layout.\r\n    ";
            this.chartControl.TabIndex = 0;
            this.chartControl.BoundDataChanged += new DevExpress.XtraCharts.BoundDataChangedEventHandler(this.chartControl_BoundDataChanged);
            // 
            // pivotGridControl
            // 
            this.pivotGridControl.Cursor = System.Windows.Forms.Cursors.Default;
            this.pivotGridControl.Fields.AddRange(new DevExpress.XtraPivotGrid.PivotGridField[] {
            this.pgfStaffMonthID,
            this.pgfStaffID,
            this.pgfM2,
            this.pgfM3,
            this.pgfM1,
            this.pgfRegion,
            this.pgfPersonalLevel,
            this.pgfFIO,
            this.pgfYear,
            this.PgfMonth,
            this.pgfPersonalCount,
            this.pgfPersonalPercent,
            this.pgfMaxZP,
            this.pgfFactZP,
            this.pgfZPAchivement,
            this.pgfZPAchivementsRange,
            this.pgfM3Channel,
            this.pgfPersonalChannel,
            this.pgfPersonalType,
            this.pgfBlockbaster,
            this.pgfSecondV,
            this.pgfSecondVRange,
            this.pgfGoal,
            this.pgfGoalRange,
            this.pgfValue});
            this.pivotGridControl.Location = new System.Drawing.Point(7, 233);
            this.pivotGridControl.Name = "pivotGridControl";
            this.pivotGridControl.OptionsChartDataSource.DataProvideMode = DevExpress.XtraPivotGrid.PivotChartDataProvideMode.UseCustomSettings;
            this.pivotGridControl.OptionsChartDataSource.ProvideRowCustomTotals = true;
            this.pivotGridControl.OptionsChartDataSource.ProvideRowGrandTotals = true;
            this.pivotGridControl.OptionsChartDataSource.ProvideRowTotals = true;
            this.pivotGridControl.OptionsLayout.Columns.AddNewColumns = false;
            this.pivotGridControl.OptionsLayout.Columns.RemoveOldColumns = false;
            this.pivotGridControl.OptionsLayout.StoreDataSettings = false;
            this.pivotGridControl.OptionsLayout.StoreVisualOptions = false;
            this.pivotGridControl.OptionsPrint.PrintFilterHeaders = DevExpress.Utils.DefaultBoolean.False;
            this.pivotGridControl.Size = new System.Drawing.Size(1044, 280);
            this.pivotGridControl.TabIndex = 0;
            this.pivotGridControl.CustomSummary += new DevExpress.XtraPivotGrid.PivotGridCustomSummaryEventHandler(this.pivotGridControl_CustomSummary);
            this.pivotGridControl.CustomFieldSort += new DevExpress.XtraPivotGrid.PivotGridCustomFieldSortEventHandler(this.pivotGridControl_CustomFieldSort);
            // 
            // pgfStaffMonthID
            // 
            this.pgfStaffMonthID.Area = DevExpress.XtraPivotGrid.PivotArea.ColumnArea;
            this.pgfStaffMonthID.AreaIndex = 0;
            this.pgfStaffMonthID.FieldName = "STAFF_MONTH_ID";
            this.pgfStaffMonthID.Name = "pgfStaffMonthID";
            this.pgfStaffMonthID.Visible = false;
            // 
            // pgfStaffID
            // 
            this.pgfStaffID.Area = DevExpress.XtraPivotGrid.PivotArea.ColumnArea;
            this.pgfStaffID.AreaIndex = 0;
            this.pgfStaffID.FieldName = "STAFF_ID";
            this.pgfStaffID.Name = "pgfStaffID";
            this.pgfStaffID.Visible = false;
            // 
            // pgfM2
            // 
            this.pgfM2.AreaIndex = 2;
            this.pgfM2.FieldName = "M2";
            this.pgfM2.Name = "pgfM2";
            // 
            // pgfM3
            // 
            this.pgfM3.AreaIndex = 1;
            this.pgfM3.FieldName = "M3";
            this.pgfM3.Name = "pgfM3";
            // 
            // pgfM1
            // 
            this.pgfM1.AreaIndex = 3;
            this.pgfM1.FieldName = "M1";
            this.pgfM1.Name = "pgfM1";
            // 
            // pgfRegion
            // 
            this.pgfRegion.AreaIndex = 0;
            this.pgfRegion.FieldName = "Регион";
            this.pgfRegion.Name = "pgfRegion";
            this.pgfRegion.Width = 170;
            // 
            // pgfPersonalLevel
            // 
            this.pgfPersonalLevel.AreaIndex = 4;
            this.pgfPersonalLevel.FieldName = "Уровень персонала";
            this.pgfPersonalLevel.Name = "pgfPersonalLevel";
            // 
            // pgfFIO
            // 
            this.pgfFIO.AreaIndex = 6;
            this.pgfFIO.FieldName = "ФИО";
            this.pgfFIO.Name = "pgfFIO";
            // 
            // pgfYear
            // 
            this.pgfYear.AreaIndex = 8;
            this.pgfYear.CellFormat.FormatString = "N0";
            this.pgfYear.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.pgfYear.FieldName = "год";
            this.pgfYear.Name = "pgfYear";
            // 
            // PgfMonth
            // 
            this.PgfMonth.AreaIndex = 9;
            this.PgfMonth.CellFormat.FormatString = "N0";
            this.PgfMonth.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.PgfMonth.FieldName = "месяц";
            this.PgfMonth.Name = "PgfMonth";
            // 
            // pgfPersonalCount
            // 
            this.pgfPersonalCount.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
            this.pgfPersonalCount.AreaIndex = 0;
            this.pgfPersonalCount.Caption = "Кол-во персонала";
            this.pgfPersonalCount.CellFormat.FormatString = "N0";
            this.pgfPersonalCount.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.pgfPersonalCount.Name = "pgfPersonalCount";
            this.pgfPersonalCount.SummaryType = DevExpress.Data.PivotGrid.PivotSummaryType.Custom;
            this.pgfPersonalCount.UnboundExpression = "1";
            this.pgfPersonalCount.UnboundFieldName = "pgfPersonalCount";
            this.pgfPersonalCount.UnboundType = DevExpress.Data.UnboundColumnType.Integer;
            // 
            // pgfPersonalPercent
            // 
            this.pgfPersonalPercent.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
            this.pgfPersonalPercent.AreaIndex = 1;
            this.pgfPersonalPercent.Caption = "% персонала";
            this.pgfPersonalPercent.Name = "pgfPersonalPercent";
            this.pgfPersonalPercent.SummaryDisplayType = DevExpress.Data.PivotGrid.PivotSummaryDisplayType.PercentOfColumn;
            this.pgfPersonalPercent.UnboundExpression = "1";
            this.pgfPersonalPercent.UnboundFieldName = "pgfPersonalPercent";
            this.pgfPersonalPercent.UnboundType = DevExpress.Data.UnboundColumnType.Integer;
            this.pgfPersonalPercent.ValueFormat.FormatString = "P2";
            this.pgfPersonalPercent.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            // 
            // pgfMaxZP
            // 
            this.pgfMaxZP.AreaIndex = 10;
            this.pgfMaxZP.CellFormat.FormatString = "N2";
            this.pgfMaxZP.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.pgfMaxZP.FieldName = "Max ЗП";
            this.pgfMaxZP.Name = "pgfMaxZP";
            this.pgfMaxZP.SummaryType = DevExpress.Data.PivotGrid.PivotSummaryType.Custom;
            this.pgfMaxZP.ValueFormat.FormatString = "N2";
            this.pgfMaxZP.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            // 
            // pgfFactZP
            // 
            this.pgfFactZP.AreaIndex = 11;
            this.pgfFactZP.CellFormat.FormatString = "N2";
            this.pgfFactZP.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.pgfFactZP.FieldName = "Факт ЗП";
            this.pgfFactZP.Name = "pgfFactZP";
            this.pgfFactZP.SummaryType = DevExpress.Data.PivotGrid.PivotSummaryType.Custom;
            this.pgfFactZP.ValueFormat.FormatString = "N2";
            this.pgfFactZP.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            // 
            // pgfZPAchivement
            // 
            this.pgfZPAchivement.AreaIndex = 16;
            this.pgfZPAchivement.Caption = "% полученной ЗП";
            this.pgfZPAchivement.CellFormat.FormatString = "P2";
            this.pgfZPAchivement.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.pgfZPAchivement.Name = "pgfZPAchivement";
            this.pgfZPAchivement.SummaryType = DevExpress.Data.PivotGrid.PivotSummaryType.Custom;
            this.pgfZPAchivement.UnboundExpression = "Iif([Max ЗП]==0, 0, [Факт ЗП] / [Max ЗП])";
            this.pgfZPAchivement.UnboundFieldName = "pgfZPAchivement";
            this.pgfZPAchivement.UnboundType = DevExpress.Data.UnboundColumnType.Decimal;
            this.pgfZPAchivement.ValueFormat.FormatString = "P2";
            this.pgfZPAchivement.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            // 
            // pgfZPAchivementsRange
            // 
            this.pgfZPAchivementsRange.Area = DevExpress.XtraPivotGrid.PivotArea.RowArea;
            this.pgfZPAchivementsRange.AreaIndex = 0;
            this.pgfZPAchivementsRange.FieldName = "Диапазон % достиж. ЗП";
            this.pgfZPAchivementsRange.Name = "pgfZPAchivementsRange";
            this.pgfZPAchivementsRange.SortMode = DevExpress.XtraPivotGrid.PivotSortMode.Custom;
            this.pgfZPAchivementsRange.Width = 170;
            // 
            // pgfM3Channel
            // 
            this.pgfM3Channel.Area = DevExpress.XtraPivotGrid.PivotArea.ColumnArea;
            this.pgfM3Channel.AreaIndex = 0;
            this.pgfM3Channel.FieldName = "Канал М3";
            this.pgfM3Channel.Name = "pgfM3Channel";
            // 
            // pgfPersonalChannel
            // 
            this.pgfPersonalChannel.AreaIndex = 5;
            this.pgfPersonalChannel.FieldName = "Канал персонала";
            this.pgfPersonalChannel.Name = "pgfPersonalChannel";
            // 
            // pgfPersonalType
            // 
            this.pgfPersonalType.AreaIndex = 7;
            this.pgfPersonalType.FieldName = "Тип персонала";
            this.pgfPersonalType.Name = "pgfPersonalType";
            // 
            // pgfBlockbaster
            // 
            this.pgfBlockbaster.AreaIndex = 15;
            this.pgfBlockbaster.FieldName = "Блокбастер";
            this.pgfBlockbaster.Name = "pgfBlockbaster";
            // 
            // pgfSecondV
            // 
            this.pgfSecondV.AreaIndex = 17;
            this.pgfSecondV.CellFormat.FormatString = "P2";
            this.pgfSecondV.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.pgfSecondV.FieldName = "Вторичный V";
            this.pgfSecondV.Name = "pgfSecondV";
            this.pgfSecondV.SummaryType = DevExpress.Data.PivotGrid.PivotSummaryType.Custom;
            this.pgfSecondV.Visible = false;
            // 
            // pgfSecondVRange
            // 
            this.pgfSecondVRange.AreaIndex = 14;
            this.pgfSecondVRange.FieldName = "Диапазон Вторичный V";
            this.pgfSecondVRange.Name = "pgfSecondVRange";
            this.pgfSecondVRange.SortMode = DevExpress.XtraPivotGrid.PivotSortMode.Custom;
            this.pgfSecondVRange.Width = 170;
            // 
            // pgfGoal
            // 
            this.pgfGoal.AreaIndex = 13;
            this.pgfGoal.Caption = "Цель";
            this.pgfGoal.FieldName = "KPI_NAME";
            this.pgfGoal.Name = "pgfGoal";
            // 
            // pgfGoalRange
            // 
            this.pgfGoalRange.AreaIndex = 12;
            this.pgfGoalRange.Caption = "Диапазон цели";
            this.pgfGoalRange.FieldName = "RANGE_DESCRIPTION";
            this.pgfGoalRange.Name = "pgfGoalRange";
            this.pgfGoalRange.SortMode = DevExpress.XtraPivotGrid.PivotSortMode.Custom;
            // 
            // pgfValue
            // 
            this.pgfValue.Area = DevExpress.XtraPivotGrid.PivotArea.ColumnArea;
            this.pgfValue.AreaIndex = 1;
            this.pgfValue.Caption = "Значение цели";
            this.pgfValue.CellFormat.FormatString = "P2";
            this.pgfValue.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.pgfValue.FieldName = "Ratio";
            this.pgfValue.Name = "pgfValue";
            this.pgfValue.SummaryType = DevExpress.Data.PivotGrid.PivotSummaryType.Custom;
            this.pgfValue.Visible = false;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.splitterItem1});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroup1.Size = new System.Drawing.Size(1058, 520);
            this.layoutControlGroup1.Text = "Root";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.pivotGridControl;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 226);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(1054, 290);
            this.layoutControlItem1.Text = "layoutControlItem1";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.chartControl;
            this.layoutControlItem2.CustomizationFormText = "layoutControlItem2";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(1054, 220);
            this.layoutControlItem2.Text = "layoutControlItem2";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextToControlDistance = 0;
            this.layoutControlItem2.TextVisible = false;
            // 
            // splitterItem1
            // 
            this.splitterItem1.AllowHotTrack = true;
            this.splitterItem1.CustomizationFormText = "splitterItem1";
            this.splitterItem1.Location = new System.Drawing.Point(0, 220);
            this.splitterItem1.Name = "splitterItem1";
            this.splitterItem1.Size = new System.Drawing.Size(1054, 6);
            // 
            // SmashReport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.layoutControl1);
            this.Name = "SmashReport";
            this.Size = new System.Drawing.Size(1058, 520);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(xyDiagram1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(sideBySideBarSeriesLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pivotGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem1)).EndInit();
            this.ResumeLayout(false);

    }

    #endregion

    private DevExpress.XtraLayout.LayoutControl layoutControl1;
    private DevExpress.XtraCharts.ChartControl chartControl;
    private DevExpress.XtraPivotGrid.PivotGridControl pivotGridControl;
    private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
    private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
    private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
    private DevExpress.XtraLayout.SplitterItem splitterItem1;
    private DevExpress.XtraPivotGrid.PivotGridField pgfM2;
    private DevExpress.XtraPivotGrid.PivotGridField pgfM3;
    private DevExpress.XtraPivotGrid.PivotGridField pgfM1;
    private DevExpress.XtraPivotGrid.PivotGridField pgfRegion;
    private DevExpress.XtraPivotGrid.PivotGridField pgfPersonalLevel;
    private DevExpress.XtraPivotGrid.PivotGridField pgfFIO;
    private DevExpress.XtraPivotGrid.PivotGridField pgfYear;
    private DevExpress.XtraPivotGrid.PivotGridField PgfMonth;
    private DevExpress.XtraPivotGrid.PivotGridField pgfMaxZP;
    private DevExpress.XtraPivotGrid.PivotGridField pgfFactZP;
    private DevExpress.XtraPivotGrid.PivotGridField pgfZPAchivementsRange;
    private DevExpress.XtraPivotGrid.PivotGridField pgfM3Channel;
    private DevExpress.XtraPivotGrid.PivotGridField pgfPersonalChannel;
    private DevExpress.XtraPivotGrid.PivotGridField pgfPersonalType;
    private DevExpress.XtraPivotGrid.PivotGridField pgfBlockbaster;
    private DevExpress.XtraPivotGrid.PivotGridField pgfSecondV;
    private DevExpress.XtraPivotGrid.PivotGridField pgfSecondVRange;
    private DevExpress.XtraPivotGrid.PivotGridField pgfGoal;
    private DevExpress.XtraPivotGrid.PivotGridField pgfGoalRange;
    private DevExpress.XtraPivotGrid.PivotGridField pgfValue;
    private DevExpress.XtraPivotGrid.PivotGridField pgfZPAchivement;
    private DevExpress.XtraPivotGrid.PivotGridField pgfPersonalCount;
    private DevExpress.XtraPivotGrid.PivotGridField pgfPersonalPercent;
    private DevExpress.XtraPivotGrid.PivotGridField pgfStaffID;
    private PivotGridField pgfStaffMonthID;

  }
}
