﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using DevExpress.Charts.Native;
using DevExpress.Data.PivotGrid;
using DevExpress.XtraCharts;
using DevExpress.XtraEditors;
using DevExpress.XtraPivotGrid;
using DevExpress.XtraPrinting;
using Logica.Reports.BaseReportControl.Utility;
using SoftServe.Reports.SmashTargetReport.Properties;

namespace SoftServe.Reports.SmashTargetReport.UserControl
{
  public partial class SmashReport : System.Windows.Forms.UserControl
  {
    #region Constructors

    public SmashReport()
    {
      InitializeComponent();
    }

    #endregion

    #region Instance Properties

    public IEnumerable<PrintableComponentLink> PrintableComponents
    {
      get
      {
        return new[]
                 {  
                   new PrintableComponentLink { Component = chartControl }, 
                   new PrintableComponentLink {Component = pivotGridControl } 
                 }; }
    }

    #endregion

    #region Instance Methods

    public void ClearKPIFilter()
    {
      pgfGoal.FilterValues.Clear();
    }

    public void LoadData(DataTable dataTable)
    {
      pivotGridControl.DataSource = dataTable;
    }

    public void LoadLayout(int layoutId)
    {
      string layout = string.Empty;
      switch (layoutId)
      {
        case 1:
          layout = Resources.layout1;
          break;
        case 2:
          layout = Resources.layout2;
          break;
        case 3:
          layout = Resources.layout3;
          break;
        case 4:
          layout = Resources.layout4;
          break;
      }
      if (!string.IsNullOrEmpty(layout))
      {
        MemoryStream memoryStream = new MemoryStream();
        StreamWriter sw = new StreamWriter(memoryStream);
        sw.Write(layout);
        sw.Flush();
        memoryStream.Position = 0;
        pivotGridControl.RestoreLayoutFromStream(memoryStream);
      }
      //force summary type to avoid changes in designer.
      //pgfPersonalCount.SummaryType = PivotSummaryType.Custom;
      //pgfFactZP.SummaryType = PivotSummaryType.Custom;
      //pgfMaxZP.SummaryType = PivotSummaryType.Custom;
      //pgfZPAchivement.SummaryType = PivotSummaryType.Custom;
    }

    public void SetKPIFilter()
    {
      DataTable dataTable = (DataTable) pivotGridControl.DataSource;
      List<string> vizibleKPI = dataTable.Select(pgfValue.FieldName + " IS NOT NULL").Select(row => row.Field<string>(pgfGoal.FieldName)).Distinct().ToList();
      pgfGoal.FilterValues.FilterType = PivotFilterType.Included;
      pgfGoal.FilterValues.Values = vizibleKPI.ToArray();
    }

    private List<PivotDrillDownDataRow> UnifyRowsByField(PivotDrillDownDataSource dataSource, PivotGridField idField)
    {
      List<PivotDrillDownDataRow> result = new List<PivotDrillDownDataRow>();
      List<long> addedIds = new List<long>();

      foreach (PivotDrillDownDataRow row in dataSource)
      {
        if (addedIds.IndexOf(Convert.ToInt64(row[idField])) > -1) continue;
        result.Add(row);
        addedIds.Add(Convert.ToInt64(row[idField]));
      }
      return result;
    }

    #endregion

    #region Event Handling

    private void chartControl_BoundDataChanged(object sender, EventArgs e)
    {
      List<PivotGridField> fields = pivotGridControl.GetFieldsByArea(PivotArea.DataArea);

      if (fields.Count == 0)
        return;

      for (int i = 0; i < chartControl.Series.Count; i++)
      {
        PivotGridField field = fields[i % fields.Count];
        if (field == pgfValue || field == pgfPersonalPercent || field == pgfZPAchivement || field == pgfSecondV)
        {
          chartControl.Series[i].PointOptions.ValueNumericOptions.Format = NumericFormat.Number;
          chartControl.Series[i].PointOptions.ValueNumericOptions.Precision = 2;
          chartControl.Series[i].PointOptions.Pattern = "{V}%";

          //foreach (ISeriesPoint point in chartControl.Series[i].Points)
          //{
          //    for (int j = 0; j < point.UserValues.Length; j++)
          //  {
          //      point.UserValues[j] = point.UserValues[j] * 100;
          //  }
          //}

          for(int k = 0; k < chartControl.Series[i].Points.Count; k++)
            {
                SeriesPoint point = chartControl.Series[i].Points[k];
          
              for (int j = 0; j < point.Values.Length; j++)
              {
                  point.Values[j] = point.Values[j] * 100;
              }
          }
        }
        else if (field == pgfMaxZP || field == pgfFactZP)
        {
          chartControl.Series[i].PointOptions.ValueNumericOptions.Format = NumericFormat.Number;
          chartControl.Series[i].PointOptions.ValueNumericOptions.Precision = 2;
        }
      }
    }

    private void pivotGridControl_CustomFieldSort(object sender, PivotGridCustomFieldSortEventArgs e)
    {

    }

    private void pivotGridControl_CustomSummary(object sender, PivotGridCustomSummaryEventArgs e)
    {

    }

    #endregion
  }
}