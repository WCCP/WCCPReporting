using Logica.Reports.Common;

namespace SoftServe.Reports.SmashTargetReport.Utility
{
  public static class Constants
  {
    public const string LIST_SEPARATOR = ",";

    #region Settings Form

    public const string SP_GET_PERIOD = "spDW_CAPS_SMASH_GET_PERIOD";
    public const string SP_GET_PERIOD_IS_CURRENT = "isCurrent";
    public const string SP_GET_PERIOD_TARGET_ID = "Target_ID";
    public const string SP_GET_PERIOD_TARGET_START = "Target_start";

    public const string SP_GET_TERRITORY = "spDW_CAPS_SMASH_GET_TERRITORY";
    public const string SP_GET_TERRITORY_PARAM_1 = "UserLogin";

    #endregion

    #region Report

    public const string GET_REPORT = "spDW_CAPS_SMASH_REPORT";
    public const string GET_REPORT_PARAM_1 = "Start_Target_id";
    public const string GET_REPORT_PARAM_2 = "End_Target_id";
    public const string GET_REPORT_PARAM_3 = "M3_List";

    #endregion
  }
}