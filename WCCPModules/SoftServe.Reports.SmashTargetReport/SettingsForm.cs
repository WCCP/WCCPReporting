using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraTreeList;
using DevExpress.XtraTreeList.Nodes;
using Logica.Reports.BaseReportControl.Utility;
using SoftServe.Reports.SmashTargetReport.DataProvider;
using SoftServe.Reports.SmashTargetReport.Properties;
using SoftServe.Reports.SmashTargetReport.Utility;

namespace SoftServe.Reports.SmashTargetReport
{
    public partial class SettingsForm : XtraForm
    {
        #region Constructors

        public SettingsForm()
        {
            InitializeComponent();
            LoadData();
        }

        #endregion

        #region Instance Properties

        public int PeriodEnd { get; private set; }

        public int PeriodStart { get; private set; }

        public int[] SelectedIds { get; private set; }

        #endregion

        #region Instance Methods

        private void LoadData()
        {
            DataTable periods = DataAccessProvider.Periods;
            lookUpEditPeriodFrom.Properties.DataSource = periods;
            lookUpEditPeriodTo.Properties.DataSource = periods;
            var currentItems = (from d in periods.AsEnumerable()
                                where d.Field<int>(Constants.SP_GET_PERIOD_IS_CURRENT) == 1
                                select d.Field<int>(Constants.SP_GET_PERIOD_TARGET_ID)).ToArray();
            if (currentItems.Length > 0)
            {
                lookUpEditPeriodFrom.EditValue = lookUpEditPeriodTo.EditValue = currentItems[0];
            }
            else
            {
                lookUpEditPeriodFrom.EditValue =
                  lookUpEditPeriodTo.EditValue = periods.Rows[0].Field<int>(Constants.SP_GET_PERIOD_TARGET_ID);
            }

            treeList.DataSource = DataAccessProvider.Territory;
            treeList.ExpandAll();
            treeList.NodesIterator.DoOperation(node =>
                                                 {
                                                     if (!node.HasChildren && ConvertEx.ToBool(node[treeListModifyable]))
                                                     {
                                                         node.Checked = true;
                                                         treeList_AfterCheckNode(treeList, new NodeEventArgs(node));
                                                     }
                                                 }
              );
            treeList.CollapseAll();
            foreach (TreeListNode node in treeList.Nodes)
            {
                node.Expanded = node.Level == 0;
            }
            btnYes.Enabled = GetSelectedIds().Count > 0;
        }

        private bool ValidateData()
        {
            bool isValid = true;
            string errorText = string.Empty;

            if (Convert.ToDateTime(lookUpEditPeriodFrom.GetColumnValue(Constants.SP_GET_PERIOD_TARGET_START)) > Convert.ToDateTime(lookUpEditPeriodTo.GetColumnValue(Constants.SP_GET_PERIOD_TARGET_START)))
            {
                errorText = string.Format(Resources.Settings_PeriodEnd_LessThan_PeriodStart, errorText, Environment.NewLine);
            }

            if (GetSelectedIds().Count == 0)
            {
                errorText = string.Format(Resources.Settings_NothingSelectedInTree, errorText, Environment.NewLine);
            }

            if (!string.IsNullOrEmpty(errorText))
            {
                MessageBox.Show(errorText, Resources.Settings_ErrorBoxHeader, MessageBoxButtons.OK, MessageBoxIcon.Error);
                isValid = false;
            }
            return isValid;
        }

        #endregion

        #region Event Handling

        private void btnYes_Click(object sender, EventArgs e)
        {
            if (ValidateData())
            {
                PeriodEnd = (int)lookUpEditPeriodTo.EditValue;
                PeriodStart = (int)lookUpEditPeriodFrom.EditValue;
                SelectedIds = GetSelectedIds().ToArray();

                DialogResult = DialogResult.OK;
            }
        }

        private List<int> GetSelectedIds()
        {
            GetFinalCheckedLeafNodesOperation op = new GetFinalCheckedLeafNodesOperation(treeListID);
            treeList.NodesIterator.DoOperation(op);

            return op.SelectedIds;
        }

        private void treeList_AfterCheckNode(object sender, NodeEventArgs e)
        {
            if (!e.Node.HasChildren && e.Node.Checked && !ConvertEx.ToBool(e.Node[treeListModifyable]))
                e.Node.Checked = false;
            SetChildNodesStatus(e.Node);
            TreeListNode rootNode = e.Node.RootNode;
            rootNode.Checked = SetNodesState(rootNode);
            btnYes.Enabled = GetSelectedIds().Count > 0;
        }

        #endregion

        #region Class Methods

        private void SetChildNodesStatus(TreeListNode node)
        {
            foreach (TreeListNode childNode in node.Nodes)
            {
                if (!childNode.HasChildren && !ConvertEx.ToBool(childNode.GetValue(treeListModifyable))) continue;
                childNode.Checked = node.Checked;
                SetChildNodesStatus(childNode);
            }
        }

        private static bool SetNodesState(TreeListNode topNode)
        {
            if (!topNode.HasChildren)
            {
                return topNode.Checked;
            }
            bool isChecked = true;
            foreach (TreeListNode treeListNode in topNode.Nodes)
            {
                treeListNode.Checked = SetNodesState(treeListNode);
                isChecked = isChecked && treeListNode.Checked;
            }
            return isChecked;
        }

        #endregion

        private void SettingsForm_VisibleChanged(object sender, EventArgs e)
        {
            if (Visible || SelectedIds == null || DialogResult == DialogResult.OK) return;
            lookUpEditPeriodTo.EditValue = PeriodEnd;
            lookUpEditPeriodFrom.EditValue = PeriodStart;
            //uncheck all nodes
            treeList.NodesIterator.DoOperation(node => node.Checked = false);
            //check nodes which were selected
            treeList.NodesIterator.DoOperation(node =>
            {
                if (!node.HasChildren && SelectedIds.Contains((int)node[treeListID]))
                {
                    node.Checked = true;
                    treeList_AfterCheckNode(treeList, new NodeEventArgs(node));
                }
            });
        }
    }
}