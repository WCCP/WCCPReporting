﻿using Logica.Reports.BaseReportControl;
namespace WccpReporting
{
    partial class WccpUIControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
          this.components = new System.ComponentModel.Container();
          System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(WccpUIControl));
          this.barMenuManager = new DevExpress.XtraBars.BarManager(this.components);
          this.bar1 = new DevExpress.XtraBars.Bar();
          this.btnRefresh = new DevExpress.XtraBars.BarButtonItem();
          this.btnSettings = new DevExpress.XtraBars.BarButtonItem();
          this.btnExportTo = new DevExpress.XtraBars.BarSubItem();
          this.btnExportToExcel = new DevExpress.XtraBars.BarButtonItem();
          this.btnExportToPdf = new DevExpress.XtraBars.BarButtonItem();
          this.btnExportToHtml = new DevExpress.XtraBars.BarButtonItem();
          this.btnExportToMht = new DevExpress.XtraBars.BarButtonItem();
          this.btnExportToRtf = new DevExpress.XtraBars.BarButtonItem();
          this.btnExportToText = new DevExpress.XtraBars.BarButtonItem();
          this.btnExportToImage = new DevExpress.XtraBars.BarButtonItem();
          this.btnExportToCsv = new DevExpress.XtraBars.BarButtonItem();
          this.btnExportToXlsx = new DevExpress.XtraBars.BarButtonItem();
          this.btnPrint = new DevExpress.XtraBars.BarButtonItem();
          this.barStaticItem1 = new DevExpress.XtraBars.BarStaticItem();
          this.barEditItem1 = new DevExpress.XtraBars.BarEditItem();
          this.repositoryItemComboBox1 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
          this.standaloneBarDockControl1 = new DevExpress.XtraBars.StandaloneBarDockControl();
          this.barAndDockingController1 = new DevExpress.XtraBars.BarAndDockingController(this.components);
          this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
          this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
          this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
          this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
          this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
          this.imCollection = new DevExpress.Utils.ImageCollection(this.components);
          this.smashReport1 = new SoftServe.Reports.SmashTargetReport.UserControl.SmashReport();
          ((System.ComponentModel.ISupportInitialize)(this.barMenuManager)).BeginInit();
          ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox1)).BeginInit();
          ((System.ComponentModel.ISupportInitialize)(this.barAndDockingController1)).BeginInit();
          ((System.ComponentModel.ISupportInitialize)(this.imCollection)).BeginInit();
          this.SuspendLayout();
          // 
          // barMenuManager
          // 
          this.barMenuManager.AllowCustomization = false;
          this.barMenuManager.AllowMoveBarOnToolbar = false;
          this.barMenuManager.AllowQuickCustomization = false;
          this.barMenuManager.AllowShowToolbarsPopup = false;
          this.barMenuManager.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1});
          this.barMenuManager.Controller = this.barAndDockingController1;
          this.barMenuManager.DockControls.Add(this.barDockControlTop);
          this.barMenuManager.DockControls.Add(this.barDockControlBottom);
          this.barMenuManager.DockControls.Add(this.barDockControlLeft);
          this.barMenuManager.DockControls.Add(this.barDockControlRight);
          this.barMenuManager.DockControls.Add(this.standaloneBarDockControl1);
          this.barMenuManager.Form = this;
          this.barMenuManager.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.btnSettings,
            this.btnPrint,
            this.btnRefresh,
            this.btnExportTo,
            this.btnExportToExcel,
            this.btnExportToPdf,
            this.btnExportToHtml,
            this.btnExportToMht,
            this.btnExportToRtf,
            this.btnExportToText,
            this.btnExportToImage,
            this.btnExportToCsv,
            this.btnExportToXlsx,
            this.barButtonItem1,
            this.barStaticItem1,
            this.barEditItem1});
          this.barMenuManager.LargeImages = this.imCollection;
          this.barMenuManager.MaxItemId = 44;
          this.barMenuManager.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemComboBox1});
          // 
          // bar1
          // 
          this.bar1.BarName = "menuBar";
          this.bar1.DockCol = 0;
          this.bar1.DockRow = 0;
          this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Standalone;
          this.bar1.FloatLocation = new System.Drawing.Point(74, 165);
          this.bar1.FloatSize = new System.Drawing.Size(48, 32);
          this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btnRefresh, "", true, true, true, 0, null, DevExpress.XtraBars.BarItemPaintStyle.Standard),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnSettings),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btnExportTo, "", true, true, true, 0, null, DevExpress.XtraBars.BarItemPaintStyle.CaptionInMenu),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnPrint),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.barEditItem1)});
          this.bar1.OptionsBar.AllowQuickCustomization = false;
          this.bar1.OptionsBar.DisableClose = true;
          this.bar1.OptionsBar.MultiLine = true;
          this.bar1.StandaloneBarDockControl = this.standaloneBarDockControl1;
          this.bar1.Text = "menuBar";
          // 
          // btnRefresh
          // 
          this.btnRefresh.Caption = "Обновить";
          this.btnRefresh.Id = 6;
          this.btnRefresh.LargeImageIndex = 0;
          this.btnRefresh.Name = "btnRefresh";
          this.btnRefresh.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnRefresh_ItemClick);
          // 
          // btnSettings
          // 
          this.btnSettings.Caption = "Параметры отчета";
          this.btnSettings.Id = 3;
          this.btnSettings.LargeImageIndex = 6;
          this.btnSettings.Name = "btnSettings";
          this.btnSettings.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnSettings_ItemClick);
          // 
          // btnExportTo
          // 
          this.btnExportTo.Caption = "Экспортировать в ...";
          this.btnExportTo.Id = 10;
          this.btnExportTo.LargeImageIndex = 10;
          this.btnExportTo.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btnExportToExcel, DevExpress.XtraBars.BarItemPaintStyle.Standard),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnExportToPdf),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnExportToHtml),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnExportToMht),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnExportToRtf),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnExportToText),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnExportToImage),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnExportToCsv),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnExportToXlsx)});
          this.btnExportTo.Name = "btnExportTo";
          // 
          // btnExportToExcel
          // 
          this.btnExportToExcel.Caption = "Xml";
          this.btnExportToExcel.Id = 11;
          this.btnExportToExcel.LargeImageIndex = 3;
          this.btnExportToExcel.Name = "btnExportToExcel";
          this.btnExportToExcel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnExportTo_ItemClick);
          // 
          // btnExportToPdf
          // 
          this.btnExportToPdf.Caption = "Pdf";
          this.btnExportToPdf.Id = 12;
          this.btnExportToPdf.LargeImageIndex = 13;
          this.btnExportToPdf.Name = "btnExportToPdf";
          this.btnExportToPdf.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
          this.btnExportToPdf.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnExportTo_ItemClick);
          // 
          // btnExportToHtml
          // 
          this.btnExportToHtml.Caption = "Html";
          this.btnExportToHtml.Id = 13;
          this.btnExportToHtml.LargeImageIndex = 9;
          this.btnExportToHtml.Name = "btnExportToHtml";
          this.btnExportToHtml.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
          this.btnExportToHtml.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnExportTo_ItemClick);
          // 
          // btnExportToMht
          // 
          this.btnExportToMht.Caption = "Mht";
          this.btnExportToMht.Id = 14;
          this.btnExportToMht.LargeImageIndex = 11;
          this.btnExportToMht.Name = "btnExportToMht";
          this.btnExportToMht.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
          this.btnExportToMht.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnExportTo_ItemClick);
          // 
          // btnExportToRtf
          // 
          this.btnExportToRtf.Caption = "Rtf";
          this.btnExportToRtf.Id = 15;
          this.btnExportToRtf.LargeImageIndex = 8;
          this.btnExportToRtf.Name = "btnExportToRtf";
          this.btnExportToRtf.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnExportTo_ItemClick);
          // 
          // btnExportToText
          // 
          this.btnExportToText.Caption = "Text";
          this.btnExportToText.Id = 16;
          this.btnExportToText.LargeImageIndex = 12;
          this.btnExportToText.Name = "btnExportToText";
          this.btnExportToText.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
          this.btnExportToText.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnExportTo_ItemClick);
          // 
          // btnExportToImage
          // 
          this.btnExportToImage.Caption = "Image";
          this.btnExportToImage.Id = 16;
          this.btnExportToImage.LargeImageIndex = 16;
          this.btnExportToImage.Name = "btnExportToImage";
          this.btnExportToImage.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
          this.btnExportToImage.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnExportTo_ItemClick);
          // 
          // btnExportToCsv
          // 
          this.btnExportToCsv.Caption = "Csv";
          this.btnExportToCsv.Id = 16;
          this.btnExportToCsv.LargeImageIndex = 17;
          this.btnExportToCsv.Name = "btnExportToCsv";
          this.btnExportToCsv.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
          this.btnExportToCsv.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnExportTo_ItemClick);
          // 
          // btnExportToXlsx
          // 
          this.btnExportToXlsx.Caption = "Xlsx";
          this.btnExportToXlsx.Id = 16;
          this.btnExportToXlsx.LargeImageIndex = 15;
          this.btnExportToXlsx.Name = "btnExportToXlsx";
          this.btnExportToXlsx.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnExportTo_ItemClick);
          // 
          // btnPrint
          // 
          this.btnPrint.Caption = "Печать";
          this.btnPrint.Id = 5;
          this.btnPrint.LargeImageIndex = 19;
          this.btnPrint.Name = "btnPrint";
          this.btnPrint.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnPrint_ItemClick);
          // 
          // barStaticItem1
          // 
          this.barStaticItem1.Border = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
          this.barStaticItem1.Caption = "Формат отображения:";
          this.barStaticItem1.Id = 42;
          this.barStaticItem1.LeftIndent = 75;
          this.barStaticItem1.Name = "barStaticItem1";
          this.barStaticItem1.TextAlignment = System.Drawing.StringAlignment.Near;
          // 
          // barEditItem1
          // 
          this.barEditItem1.Caption = "barEditItemTab";
          this.barEditItem1.Edit = this.repositoryItemComboBox1;
          this.barEditItem1.Id = 43;
          this.barEditItem1.Name = "barEditItem1";
          this.barEditItem1.Width = 200;
          this.barEditItem1.EditValueChanged += new System.EventHandler(this.barEditItem1_EditValueChanged);
          // 
          // repositoryItemComboBox1
          // 
          this.repositoryItemComboBox1.AutoHeight = false;
          this.repositoryItemComboBox1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
          this.repositoryItemComboBox1.Name = "repositoryItemComboBox1";
          this.repositoryItemComboBox1.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
          // 
          // standaloneBarDockControl1
          // 
          this.standaloneBarDockControl1.AutoSize = true;
          this.standaloneBarDockControl1.Dock = System.Windows.Forms.DockStyle.Top;
          this.standaloneBarDockControl1.Location = new System.Drawing.Point(0, 0);
          this.standaloneBarDockControl1.Name = "standaloneBarDockControl1";
          this.standaloneBarDockControl1.Size = new System.Drawing.Size(956, 32);
          this.standaloneBarDockControl1.Text = "standaloneBarDockControl1";
          // 
          // barAndDockingController1
          // 
          this.barAndDockingController1.AppearancesBar.ItemsFont = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
          this.barAndDockingController1.PaintStyleName = "WindowsXP";
          this.barAndDockingController1.PropertiesBar.AllowLinkLighting = false;
          this.barAndDockingController1.PropertiesBar.LargeIcons = true;
          // 
          // barButtonItem1
          // 
          this.barButtonItem1.Caption = "d";
          this.barButtonItem1.Id = 41;
          this.barButtonItem1.Name = "barButtonItem1";
          // 
          // imCollection
          // 
          this.imCollection.ImageSize = new System.Drawing.Size(24, 24);
          this.imCollection.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imCollection.ImageStream")));
          this.imCollection.Images.SetKeyName(0, "refresh_24.png");
          this.imCollection.Images.SetKeyName(1, "briefcase_prev_24.png");
          this.imCollection.Images.SetKeyName(2, "del_24.png");
          this.imCollection.Images.SetKeyName(3, "Excel_42.bmp");
          this.imCollection.Images.SetKeyName(4, "ExcelAll_42.bmp");
          this.imCollection.Images.SetKeyName(5, "print_24.png");
          this.imCollection.Images.SetKeyName(6, "briefcase_ok_24.png");
          this.imCollection.Images.SetKeyName(7, "briefcase_save_24.png");
          this.imCollection.Images.SetKeyName(15, "xlsx.png");
          this.imCollection.Images.SetKeyName(16, "bmp.PNG");
          this.imCollection.Images.SetKeyName(17, "csv.PNG");
          this.imCollection.Images.SetKeyName(18, "copy_prev_24.png");
          this.imCollection.Images.SetKeyName(19, "doc_prev_24.png");
          // 
          // smashReport1
          // 
          this.smashReport1.Dock = System.Windows.Forms.DockStyle.Fill;
          this.smashReport1.Location = new System.Drawing.Point(0, 32);
          this.smashReport1.Name = "smashReport1";
          this.smashReport1.Size = new System.Drawing.Size(956, 444);
          this.smashReport1.TabIndex = 11;
          // 
          // WccpUIControl
          // 
          this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
          this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
          this.Controls.Add(this.smashReport1);
          this.Controls.Add(this.standaloneBarDockControl1);
          this.Controls.Add(this.barDockControlLeft);
          this.Controls.Add(this.barDockControlRight);
          this.Controls.Add(this.barDockControlBottom);
          this.Controls.Add(this.barDockControlTop);
          this.Name = "WccpUIControl";
          this.Size = new System.Drawing.Size(956, 476);
          ((System.ComponentModel.ISupportInitialize)(this.barMenuManager)).EndInit();
          ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox1)).EndInit();
          ((System.ComponentModel.ISupportInitialize)(this.barAndDockingController1)).EndInit();
          ((System.ComponentModel.ISupportInitialize)(this.imCollection)).EndInit();
          this.ResumeLayout(false);
          this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarManager barMenuManager;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.StandaloneBarDockControl standaloneBarDockControl1;
        private DevExpress.XtraBars.BarAndDockingController barAndDockingController1;
        private DevExpress.XtraBars.BarButtonItem btnSettings;
        private DevExpress.XtraBars.BarButtonItem btnRefresh;
        private DevExpress.XtraBars.BarSubItem btnExportTo;
        private DevExpress.XtraBars.BarButtonItem btnExportToExcel;
        private DevExpress.XtraBars.BarButtonItem btnExportToPdf;
        private DevExpress.XtraBars.BarButtonItem btnExportToHtml;
        private DevExpress.XtraBars.BarButtonItem btnExportToMht;
        private DevExpress.XtraBars.BarButtonItem btnExportToRtf;
        private DevExpress.XtraBars.BarButtonItem btnExportToText;
        private DevExpress.XtraBars.BarButtonItem btnExportToImage;
        private DevExpress.XtraBars.BarButtonItem btnExportToCsv;
        private DevExpress.XtraBars.BarButtonItem btnExportToXlsx;
        private DevExpress.XtraBars.BarButtonItem barButtonItem1;
        public DevExpress.XtraBars.BarButtonItem btnPrint;
        private DevExpress.XtraBars.BarStaticItem barStaticItem1;
        private DevExpress.XtraBars.BarEditItem barEditItem1;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox1;
        private DevExpress.Utils.ImageCollection imCollection;
        private SoftServe.Reports.SmashTargetReport.UserControl.SmashReport smashReport1;

    }
}
