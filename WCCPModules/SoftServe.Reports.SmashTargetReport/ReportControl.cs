﻿using System;
using System.Drawing;
using System.Drawing.Printing;
using System.Windows.Forms;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using DevExpress.XtraPrinting;
using DevExpress.XtraPrintingLinks;
using Logica.Reports.BaseReportControl;
using Logica.Reports.BaseReportControl.CommonControls.Addresses;
using Logica.Reports.Common.WaitWindow;
using SoftServe.Reports.SmashTargetReport;
using SoftServe.Reports.SmashTargetReport.DataProvider;
using SoftServe.Reports.SmashTargetReport.Properties;

namespace WccpReporting
{
  public partial class WccpUIControl : XtraUserControl
  {
    #region Constructors

    public WccpUIControl()
    {
      InitializeComponent();

      btnExportToExcel.Tag = btnExportToExcel.Tag = ExportToType.Xls;
      btnExportToHtml.Tag = btnExportToHtml.Tag = ExportToType.Html;
      btnExportToMht.Tag = btnExportToMht.Tag = ExportToType.Mht;
      btnExportToPdf.Tag = btnExportToPdf.Tag = ExportToType.Pdf;
      btnExportToRtf.Tag = btnExportToRtf.Tag = ExportToType.Rtf;
      btnExportToText.Tag = btnExportToText.Tag = ExportToType.Txt;
      btnExportToImage.Tag = btnExportToImage.Tag = ExportToType.Bmp;
      btnExportToCsv.Tag = btnExportToCsv.Tag = ExportToType.Csv;
      btnExportToXlsx.Tag = btnExportToXlsx.Tag = ExportToType.Xlsx;

      repositoryItemComboBox1.Items.AddRange(new[]
                                               {
                                                 new ComboBoxItem(2, Resources.TabCaption_Tab2),
                                                 new ComboBoxItem(1, Resources.TabCaption_Tab1),
                                                 new ComboBoxItem(3, Resources.TabCaption_Tab3),
                                                 new ComboBoxItem(4, Resources.TabCaption_Tab4)
                                               });
      barEditItem1.EditValue = repositoryItemComboBox1.Items[0];

      ShowReport();
    }

    #endregion

    #region Instance Methods

    private void CallSettingsForm()
    {
      if (SettingsForm.ShowDialog() != DialogResult.OK) return;
      ShowReport();
    }

    private CompositeLink PrepareCompositeLink()
    {
      CompositeLink compositeLink = new CompositeLink(new PrintingSystem());
      foreach (PrintableComponentLink printableComponentLink in smashReport1.PrintableComponents)
      {
        compositeLink.Links.Add(printableComponentLink);
      }
      compositeLink.PaperKind = PaperKind.A4Rotated;
      PageHeaderFooter phf = compositeLink.PageHeaderFooter as PageHeaderFooter;
      phf.Header.Content.Clear();
      phf.Header.Content.AddRange(new[] { String.Empty, ((ComboBoxItem)barEditItem1.EditValue).Text, String.Empty });
      phf.Header.Font = new Font(phf.Header.Font.FontFamily, phf.Header.Font.Size + 2, FontStyle.Bold);
      phf.Header.LineAlignment = BrickAlignment.Center;

      compositeLink.CreateDocument();
      return compositeLink;
    }

    private string SelectFilePath(ExportToType type)
    {
      String res = String.Empty;
      SaveFileDialog sfd = new SaveFileDialog();
      sfd.Title = Resources.SpecifyFileName;
      sfd.InitialDirectory = System.Reflection.Assembly.GetExecutingAssembly().Location;
      sfd.FileName = Resources.DefaultFileName;
      sfd.Filter = String.Format("(*.{0})|*.{0}", type.ToString().ToLower());
      sfd.FilterIndex = 1;
      sfd.OverwritePrompt = true;
      sfd.RestoreDirectory = true;

      if (sfd.ShowDialog() != DialogResult.Cancel)
      {
        res = sfd.FileName;
      }
      return res;
    }

    private void ShowReport()
    {
      WaitManager.StartWait();

      smashReport1.LoadData(DataAccessProvider.GetReport(SettingsForm.PeriodStart, SettingsForm.PeriodEnd, SettingsForm.SelectedIds));
      SetOrClearKPIFilter();

      WaitManager.StopWait();
    }

    private void SetOrClearKPIFilter()
    {
      if ((int) ((ComboBoxItem) barEditItem1.EditValue).Id == 3)
        smashReport1.SetKPIFilter();
      else
        smashReport1.ClearKPIFilter();
    }

    #endregion

    #region Event Handling

    private void barEditItem1_EditValueChanged(object sender, EventArgs e)
    {
      ComboBoxItem item = (ComboBoxItem)barEditItem1.EditValue;
      smashReport1.LoadLayout((int)item.Id);
      SetOrClearKPIFilter();
    }

    private void btnExportTo_ItemClick(object sender, ItemClickEventArgs e)
    {
      ExportToType type = (ExportToType) e.Item.Tag;
      string fName = SelectFilePath(type);
      if (string.IsNullOrEmpty(fName)) return;
      CompositeLink cl = PrepareCompositeLink();
      switch (type)
      {
        case ExportToType.Rtf:
          cl.PrintingSystem.ExportToRtf(fName);
          break;
        case ExportToType.Xls:
          cl.PrintingSystem.ExportToXls(fName, new XlsExportOptions {SheetName = ((ComboBoxItem) barEditItem1.EditValue).Text, TextExportMode = TextExportMode.Text});
          break;
        case ExportToType.Xlsx:
          cl.PrintingSystem.ExportToXlsx(fName);
          break;
      }
    }

    private void btnPrint_ItemClick(object sender, ItemClickEventArgs e)
    {
      PrepareCompositeLink().PrintingSystem.PreviewFormEx.Show();
    }

    private void btnRefresh_ItemClick(object sender, ItemClickEventArgs e)
    {
      ShowReport();
    }

    private void btnSettings_ItemClick(object sender, ItemClickEventArgs e)
    {
      CallSettingsForm();
    }

    #endregion

    #region Class Properties

    public static SettingsForm SettingsForm { get; set; }

    #endregion
  }
}