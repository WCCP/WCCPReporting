﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using Logica.Reports.DataAccess;
using SoftServe.Reports.SmashTargetReport.Utility;

namespace SoftServe.Reports.SmashTargetReport.DataProvider
{
  internal static class DataAccessProvider
  {
    private static readonly object userLogin = DBNull.Value; // used to obtain territory //@"EEINTERBREW\Oleksandr.Protsenko"

    private static DataTable periods;

    #region Settings Form

    internal static DataTable Periods
    {
      get
      {
        if (periods == null)
        {
          DataSet ds = DataAccessLayer.ExecuteStoredProcedure(Constants.SP_GET_PERIOD);
          if (ds != null && ds.Tables.Count > 0)
          {
            periods = ds.Tables[0];
          }
        }
        return periods.Copy();
      }
    }

    internal static DataTable Territory
    {
      get
      {
        DataSet ds = DataAccessLayer.ExecuteStoredProcedure(Constants.SP_GET_TERRITORY,
                                                            new[]
                                                              {
                                                                new SqlParameter(Constants.SP_GET_TERRITORY_PARAM_1,userLogin)
                                                              });
        if (ds != null && ds.Tables.Count > 0)
        {
          return ds.Tables[0];
        }
        return null;
      }
    }

    #endregion

    #region Main Report

    internal static DataTable GetReport(int startDate, int endDate, IEnumerable<int> m3List)
    {
      StringBuilder sb = new StringBuilder();
      foreach (int m3 in m3List)
        sb.AppendFormat("{0}{1}", m3, Constants.LIST_SEPARATOR);
      sb.Remove(sb.Length - Constants.LIST_SEPARATOR.Length, Constants.LIST_SEPARATOR.Length);

      DataSet ds = DataAccessLayer.ExecuteStoredProcedure(Constants.GET_REPORT,
                                                          new[]
                                                            {
                                                              new SqlParameter(Constants.GET_REPORT_PARAM_1, startDate),
                                                              new SqlParameter(Constants.GET_REPORT_PARAM_2, endDate),
                                                              new SqlParameter(Constants.GET_REPORT_PARAM_3, sb.ToString())
                                                            });
      if (ds != null && ds.Tables.Count > 0)
      {
        return ds.Tables[0];
      }
      return null;
    }

    #endregion
  }
}