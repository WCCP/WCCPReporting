namespace SoftServe.Reports.SmashTargetReport
{
    partial class SettingsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
          this.components = new System.ComponentModel.Container();
          System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SettingsForm));
          this.btnYes = new DevExpress.XtraEditors.SimpleButton();
          this.imCollection = new DevExpress.Utils.ImageCollection(this.components);
          this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
          this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
          this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
          this.lookUpEditPeriodFrom = new DevExpress.XtraEditors.LookUpEdit();
          this.lookUpEditPeriodTo = new DevExpress.XtraEditors.LookUpEdit();
          this.treeList = new DevExpress.XtraTreeList.TreeList();
          this.treeListData = new DevExpress.XtraTreeList.Columns.TreeListColumn();
          this.treeListID = new DevExpress.XtraTreeList.Columns.TreeListColumn();
          this.treeListModifyable = new DevExpress.XtraTreeList.Columns.TreeListColumn();
          ((System.ComponentModel.ISupportInitialize)(this.imCollection)).BeginInit();
          ((System.ComponentModel.ISupportInitialize)(this.lookUpEditPeriodFrom.Properties)).BeginInit();
          ((System.ComponentModel.ISupportInitialize)(this.lookUpEditPeriodTo.Properties)).BeginInit();
          ((System.ComponentModel.ISupportInitialize)(this.treeList)).BeginInit();
          this.SuspendLayout();
          // 
          // btnYes
          // 
          this.btnYes.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
          this.btnYes.ImageIndex = 0;
          this.btnYes.ImageList = this.imCollection;
          this.btnYes.Location = new System.Drawing.Point(71, 305);
          this.btnYes.Name = "btnYes";
          this.btnYes.Size = new System.Drawing.Size(148, 23);
          this.btnYes.TabIndex = 1;
          this.btnYes.Text = "������������� �����";
          this.btnYes.Click += new System.EventHandler(this.btnYes_Click);
          // 
          // imCollection
          // 
          this.imCollection.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imCollection.ImageStream")));
          this.imCollection.Images.SetKeyName(0, "check_24.png");
          this.imCollection.Images.SetKeyName(1, "close_24.png");
          // 
          // btnCancel
          // 
          this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
          this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
          this.btnCancel.ImageIndex = 1;
          this.btnCancel.ImageList = this.imCollection;
          this.btnCancel.Location = new System.Drawing.Point(225, 305);
          this.btnCancel.Name = "btnCancel";
          this.btnCancel.Size = new System.Drawing.Size(72, 23);
          this.btnCancel.TabIndex = 2;
          this.btnCancel.Text = "������";
          // 
          // labelControl1
          // 
          this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
          this.labelControl1.Appearance.Options.UseFont = true;
          this.labelControl1.Location = new System.Drawing.Point(12, 15);
          this.labelControl1.Name = "labelControl1";
          this.labelControl1.Size = new System.Drawing.Size(53, 13);
          this.labelControl1.TabIndex = 3;
          this.labelControl1.Text = "������ �";
          // 
          // labelControl2
          // 
          this.labelControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
          this.labelControl2.Appearance.Options.UseFont = true;
          this.labelControl2.Location = new System.Drawing.Point(177, 15);
          this.labelControl2.Name = "labelControl2";
          this.labelControl2.Size = new System.Drawing.Size(14, 13);
          this.labelControl2.TabIndex = 4;
          this.labelControl2.Text = "��";
          // 
          // lookUpEditPeriodFrom
          // 
          this.lookUpEditPeriodFrom.Location = new System.Drawing.Point(71, 12);
          this.lookUpEditPeriodFrom.Name = "lookUpEditPeriodFrom";
          this.lookUpEditPeriodFrom.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
          this.lookUpEditPeriodFrom.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Target_ID", "Target_ID", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("DateName", "DateName"),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("isCurrent", "isCurrent", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default)});
          this.lookUpEditPeriodFrom.Properties.DisplayMember = "DateName";
          this.lookUpEditPeriodFrom.Properties.DropDownRows = 12;
          this.lookUpEditPeriodFrom.Properties.NullText = "<�� ������>";
          this.lookUpEditPeriodFrom.Properties.PopupFormMinSize = new System.Drawing.Size(100, 0);
          this.lookUpEditPeriodFrom.Properties.ShowFooter = false;
          this.lookUpEditPeriodFrom.Properties.ShowHeader = false;
          this.lookUpEditPeriodFrom.Properties.ValueMember = "Target_ID";
          this.lookUpEditPeriodFrom.Size = new System.Drawing.Size(100, 20);
          this.lookUpEditPeriodFrom.TabIndex = 5;
          // 
          // lookUpEditPeriodTo
          // 
          this.lookUpEditPeriodTo.Location = new System.Drawing.Point(197, 12);
          this.lookUpEditPeriodTo.Name = "lookUpEditPeriodTo";
          this.lookUpEditPeriodTo.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
          this.lookUpEditPeriodTo.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Target_ID", "Target_ID", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("DateName", "DateName"),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("isCurrent", "isCurrent", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default)});
          this.lookUpEditPeriodTo.Properties.DisplayMember = "DateName";
          this.lookUpEditPeriodTo.Properties.DropDownRows = 12;
          this.lookUpEditPeriodTo.Properties.NullText = "<�� ������>";
          this.lookUpEditPeriodTo.Properties.PopupFormMinSize = new System.Drawing.Size(100, 0);
          this.lookUpEditPeriodTo.Properties.ShowFooter = false;
          this.lookUpEditPeriodTo.Properties.ShowHeader = false;
          this.lookUpEditPeriodTo.Properties.ValueMember = "Target_ID";
          this.lookUpEditPeriodTo.Size = new System.Drawing.Size(100, 20);
          this.lookUpEditPeriodTo.TabIndex = 6;
          // 
          // treeList
          // 
          this.treeList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                      | System.Windows.Forms.AnchorStyles.Left)
                      | System.Windows.Forms.AnchorStyles.Right)));
          this.treeList.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.treeListData,
            this.treeListID,
            this.treeListModifyable});
          this.treeList.Location = new System.Drawing.Point(12, 38);
          this.treeList.Name = "treeList";
          this.treeList.OptionsMenu.EnableColumnMenu = false;
          this.treeList.OptionsMenu.EnableFooterMenu = false;
          this.treeList.OptionsView.ShowCheckBoxes = true;
          this.treeList.OptionsView.ShowColumns = false;
          this.treeList.OptionsView.ShowIndicator = false;
          this.treeList.ParentFieldName = "PARENT_ID";
          this.treeList.Size = new System.Drawing.Size(285, 261);
          this.treeList.TabIndex = 7;
          this.treeList.AfterCheckNode += new DevExpress.XtraTreeList.NodeEventHandler(this.treeList_AfterCheckNode);
          // 
          // treeListData
          // 
          this.treeListData.Caption = "DATA";
          this.treeListData.FieldName = "DATA";
          this.treeListData.Name = "treeListData";
          this.treeListData.OptionsColumn.AllowEdit = false;
          this.treeListData.Visible = true;
          this.treeListData.VisibleIndex = 0;
          this.treeListData.Width = 91;
          // 
          // treeListID
          // 
          this.treeListID.Caption = "DATA_ID";
          this.treeListID.FieldName = "DATA_ID";
          this.treeListID.Name = "treeListID";
          this.treeListID.OptionsColumn.AllowEdit = false;
          // 
          // treeListModifyable
          // 
          this.treeListModifyable.Caption = "treeListColumn1";
          this.treeListModifyable.FieldName = "CAN_MODIFY";
          this.treeListModifyable.Name = "treeListModifyable";
          // 
          // SettingsForm
          // 
          this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
          this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
          this.ClientSize = new System.Drawing.Size(309, 340);
          this.Controls.Add(this.treeList);
          this.Controls.Add(this.lookUpEditPeriodTo);
          this.Controls.Add(this.lookUpEditPeriodFrom);
          this.Controls.Add(this.labelControl2);
          this.Controls.Add(this.labelControl1);
          this.Controls.Add(this.btnCancel);
          this.Controls.Add(this.btnYes);
          this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
          this.Name = "SettingsForm";
          this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
          this.Text = "Smash Target Report";
          this.VisibleChanged += new System.EventHandler(this.SettingsForm_VisibleChanged);
          ((System.ComponentModel.ISupportInitialize)(this.imCollection)).EndInit();
          ((System.ComponentModel.ISupportInitialize)(this.lookUpEditPeriodFrom.Properties)).EndInit();
          ((System.ComponentModel.ISupportInitialize)(this.lookUpEditPeriodTo.Properties)).EndInit();
          ((System.ComponentModel.ISupportInitialize)(this.treeList)).EndInit();
          this.ResumeLayout(false);
          this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton btnYes;
        private DevExpress.XtraEditors.SimpleButton btnCancel;
        private DevExpress.Utils.ImageCollection imCollection;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LookUpEdit lookUpEditPeriodFrom;
        private DevExpress.XtraEditors.LookUpEdit lookUpEditPeriodTo;
        private DevExpress.XtraTreeList.TreeList treeList;
        private DevExpress.XtraTreeList.Columns.TreeListColumn treeListData;
        private DevExpress.XtraTreeList.Columns.TreeListColumn treeListID;
        private DevExpress.XtraTreeList.Columns.TreeListColumn treeListModifyable;
    }
}