namespace SoftServe.Reports.CAPEXInFieldSummary
{
    partial class SettingsForm : DevExpress.XtraEditors.XtraForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SettingsForm));
            this.labelRegion = new DevExpress.XtraEditors.LabelControl();
            this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.imCollection = new DevExpress.Utils.ImageCollection();
            this.btnYes = new DevExpress.XtraEditors.SimpleButton();
            this.chkBoxRegion = new DevExpress.XtraEditors.CheckedListBoxControl();
            this.chkSelectAll = new DevExpress.XtraEditors.CheckEdit();
            this.chkShowTS = new DevExpress.XtraEditors.CheckEdit();
            this.ccbEquipment = new DevExpress.XtraEditors.CheckedComboBoxEdit();
            ((System.ComponentModel.ISupportInitialize)(this.imCollection)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkBoxRegion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSelectAll.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkShowTS.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ccbEquipment.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // labelRegion
            // 
            this.labelRegion.Location = new System.Drawing.Point(12, 12);
            this.labelRegion.Name = "labelRegion";
            this.labelRegion.Size = new System.Drawing.Size(106, 13);
            this.labelRegion.TabIndex = 3;
            this.labelRegion.Text = "�������� �������*:";
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Image = global::SoftServe.Reports.CAPEXInFieldSummary.Properties.Resources.Close_16;
            this.btnCancel.Location = new System.Drawing.Point(182, 404);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(90, 25);
            this.btnCancel.TabIndex = 18;
            this.btnCancel.Text = "��������";
            // 
            // imCollection
            // 
            this.imCollection.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imCollection.ImageStream")));
            this.imCollection.Images.SetKeyName(0, "check_24.png");
            this.imCollection.Images.SetKeyName(1, "close_24.png");
            // 
            // btnYes
            // 
            this.btnYes.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnYes.Image = global::SoftServe.Reports.CAPEXInFieldSummary.Properties.Resources.Check_16;
            this.btnYes.Location = new System.Drawing.Point(19, 404);
            this.btnYes.Name = "btnYes";
            this.btnYes.Size = new System.Drawing.Size(157, 25);
            this.btnYes.TabIndex = 17;
            this.btnYes.Text = "������������� �����";
            this.btnYes.Click += new System.EventHandler(this.btnYes_Click);
            // 
            // chkBoxRegion
            // 
            this.chkBoxRegion.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.chkBoxRegion.CheckOnClick = true;
            this.chkBoxRegion.Location = new System.Drawing.Point(12, 31);
            this.chkBoxRegion.Name = "chkBoxRegion";
            this.chkBoxRegion.Size = new System.Drawing.Size(260, 291);
            this.chkBoxRegion.TabIndex = 19;
            this.chkBoxRegion.ItemCheck += new DevExpress.XtraEditors.Controls.ItemCheckEventHandler(this.chkBoxRegion_ItemCheck);
            // 
            // chkSelectAll
            // 
            this.chkSelectAll.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.chkSelectAll.Location = new System.Drawing.Point(12, 354);
            this.chkSelectAll.Name = "chkSelectAll";
            this.chkSelectAll.Properties.Caption = "������� ��� �������";
            this.chkSelectAll.Size = new System.Drawing.Size(260, 19);
            this.chkSelectAll.TabIndex = 20;
            this.chkSelectAll.CheckedChanged += new System.EventHandler(this.chkSelectAll_CheckedChanged);
            // 
            // chkShowTS
            // 
            this.chkShowTS.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.chkShowTS.Location = new System.Drawing.Point(12, 379);
            this.chkShowTS.Name = "chkShowTS";
            this.chkShowTS.Properties.Caption = "���������� ��";
            this.chkShowTS.Size = new System.Drawing.Size(260, 19);
            this.chkShowTS.TabIndex = 21;
            // 
            // ccbEquipment
            // 
            this.ccbEquipment.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.ccbEquipment.Location = new System.Drawing.Point(12, 328);
            this.ccbEquipment.Name = "ccbEquipment";
            this.ccbEquipment.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ccbEquipment.Properties.SelectAllItemCaption = "���";
            this.ccbEquipment.Size = new System.Drawing.Size(260, 20);
            this.ccbEquipment.TabIndex = 22;
            this.ccbEquipment.EditValueChanged += new System.EventHandler(this.ccbEquipment_EditValueChanged);
            // 
            // SettingsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 441);
            this.Controls.Add(this.ccbEquipment);
            this.Controls.Add(this.chkShowTS);
            this.Controls.Add(this.chkSelectAll);
            this.Controls.Add(this.chkBoxRegion);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnYes);
            this.Controls.Add(this.labelRegion);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SettingsForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "��������� ������ Summary �� ���";
            ((System.ComponentModel.ISupportInitialize)(this.imCollection)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkBoxRegion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSelectAll.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkShowTS.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ccbEquipment.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.LabelControl labelRegion;
        private DevExpress.XtraEditors.SimpleButton btnCancel;
        private DevExpress.XtraEditors.SimpleButton btnYes;
        private DevExpress.Utils.ImageCollection imCollection;
        private DevExpress.XtraEditors.CheckedListBoxControl chkBoxRegion;
        private DevExpress.XtraEditors.CheckEdit chkSelectAll;
        private DevExpress.XtraEditors.CheckEdit chkShowTS;
        private DevExpress.XtraEditors.CheckedComboBoxEdit ccbEquipment;
    }
}