﻿using System;
using System.ComponentModel.Composition;
using System.Reflection;
using System.Windows.Forms;
using Logica.Reports.Common;
using Logica.Reports.DataAccess;
using ModularWinApp.Core.Interfaces;
using SoftServe.Reports.CAPEXInFieldSummary;

namespace WccpReporting
{
    [PartCreationPolicy(CreationPolicy.NonShared)]
    [Export(typeof(IStartupClass))]
    [ExportMetadata("ModuleName", "wccpCAPEXInFieldSummary.dll")]
    public class WccpUI : IStartupClass
    {
        #region IStartupClass Members

        private WccpUIControl _reportControl;
        private string _reportCaption;

        public Control ReportControl
        {
            get { return _reportControl; }
        }

        public string ReportCaption
        {
            get { return _reportCaption; }
        }

        public string GetVersion()
        {
            return Assembly.GetExecutingAssembly().GetName().Version.ToString();
        }

        public int ShowUI(int reportId, string reportCaption)
        {
            try
            {
                DataAccessLayer.OpenConnection();

                _reportControl = new WccpUIControl(reportId, reportCaption);
                _reportCaption = reportCaption;

                return _reportControl.ReportInit();
            }
            catch (Exception ex)
            {
                ErrorManager.ShowErrorBox(ex.Message);
            }

            return 1;
        }

        public void CloseUI()
        {
            //throw new NotImplementedException();
        }

        public bool AllowClose()
        {
            return true;
        }

        #endregion
    }
}
