﻿using System.Data;
using System.Data.SqlClient;
using Logica.Reports.DataAccess;

namespace SoftServe.Reports.CAPEXInFieldSummary.DataProvider
{
    class DataAccessProvider
    {
        /// <summary>
        /// Gets regions
        /// </summary>
        /// <returns>Table with regions</returns>
        internal static DataTable GetRegions()
        {
            DataTable res = null;
            DataAccessLayer.OpenConnection();
            DataSet ds = DataAccessLayer.ExecuteStoredProcedure(SqlConstants.PROC_GET_REGION, new SqlParameter[] { });

            if (null != ds && ds.Tables.Count > 0)
            {
                res = ds.Tables[0];
            }
            DataAccessLayer.CloseConnection();

            return res;
        }
        internal static DataTable GetEquipment()
        {
            DataTable res = null;
            DataAccessLayer.OpenConnection();
            DataSet ds = DataAccessLayer.ExecuteStoredProcedure(SqlConstants.PROC_GET_EQUIPMENT, new SqlParameter[] { });

            if (null != ds && ds.Tables.Count > 0)
            {
                res = ds.Tables[0];
            }
            DataAccessLayer.CloseConnection();

            return res;
        }
    }
}
