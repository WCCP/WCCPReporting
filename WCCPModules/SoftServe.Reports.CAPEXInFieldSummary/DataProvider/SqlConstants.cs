﻿namespace SoftServe.Reports.CAPEXInFieldSummary.DataProvider
{
    public static class SqlConstants
    {
        // SP names
        public const string PROC_GET_REGION = "spDW_GetUserAccesibleRegions";
        public const string PROC_GET_REGION_FLD_NAME = "NAME";
        public const string PROC_GET_REGION_FLD_ID = "ID";

        public const string PROC_GET_EQUIPMENT = "spDW_IPTR_GetPosGroups";
        public const string PROC_GET_EQUIPMENT_FLD_ID = "POSGroup_ID";
        public const string PROC_GET_EQUIPMENT_FLD_NAME = "POSGroup_Name";

        // spDW_CAPEXInFieldsReport Params
        public const string PARAM_REGIONS = "@sRegions";
        public const string PARAM_IS_SUMMARY = "@isSummary";
        public const string PARAM_USE_TS = "@useSynchPoint";
        public const string PARAM_POS_GROUPS = "@sPosGroups";

    }
}
