﻿using DevExpress.Data;
using DevExpress.Utils;
using DevExpress.XtraBars;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraPrinting;
using DevExpress.XtraTab;
using Logica.Reports.BaseReportControl;
using Logica.Reports.BaseReportControl.CommonFunctionality.EventArgs;
using Logica.Reports.BaseReportControl.Utility;
using Logica.Reports.ConfigXmlParser.GridExtension;
using SoftServe.Reports.CAPEXInFieldSummary;
using System;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Windows.Forms;
using DataTable = System.Data.DataTable;

namespace WccpReporting
{
    /// <summary>
    /// </summary>
    public partial class WccpUIControl : BaseReportUserControl
    {
        #region Fields

        private SettingsForm settingsForm;
        private string _reportCaption;

        #endregion

        #region Constructors

        /// <summary>
        ///   Initializes a new instance of the <see cref = "WccpUIControl" /> class.
        /// </summary>
        public WccpUIControl(int reportId, string reportCaption)
            : base(reportId)
        {
            InitializeComponent();
            _reportCaption = reportCaption;
            btnExportAllTo.Visibility = BarItemVisibility.Never;
            btnPrintAll.Visibility = BarItemVisibility.Never;
            btnExportToXlsx.Caption = "Excel";

            SettingsFormClick += WccpUIControl_SettingsFormClick;
            RefreshClick += WccpUIControl_RefreshClick;
            CustomExport += WccpUIControl_CustomExport;
        }

        #endregion

        #region Instance Methods

        public int ReportInit()
        {
            settingsForm = new SettingsForm(Report);

            if ((settingsForm.FormDisabled && settingsForm.ValidateData()) ||
                settingsForm.ShowDialog() == DialogResult.OK)
            {
                UpdateAllSheets();
                return 0;
            }

            return 1;
        }

        private void AdjustTabs()
        {
            foreach (XtraTabPage tabPage in tabManager.TabPages)
            {
                tabPage.ShowCloseButton = DefaultBoolean.False;
            }
            if (!settingsForm.ShowTS)
            {
                var baseTab = TabControl.TabPages[0] as BaseTab;
                if (baseTab != null && baseTab.Length > 0)
                {
                    baseTab.GetGrid(0).DefaultView.Columns["CUST_NAME"].Visible = false;
                    //baseTab.GetGrid(0).DefaultView.CustomColumnDisplayText += new DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventHandler(DefaultView_CustomColumnDisplayText);
                }
            }

            RefreshControls();
        }

        /// <summary>
        ///   Refreshes the controls.
        /// </summary>
        private void RefreshControls()
        {
            foreach (XtraTabPage tab in tabManager.TabPages)
            {
                if (tab is BaseTab)
                {
                    (tab as BaseTab).AllowMultiSelect = true;
                    (tab as BaseTab).EnableAppearenceFocusedCell = false;
                }

                if (tab.Controls.Count == 2)
                {
                    Control grid = tab.Controls[0];
                    Control label = tab.Controls[1];

                    grid.Dock = DockStyle.Fill;
                    label.Dock = DockStyle.Fill;

                    tab.Controls.Remove(label);
                    tab.Controls.Remove(grid);

                    SplitContainer sc = new SplitContainer();
                    sc.Dock = DockStyle.Fill;
                    sc.Orientation = Orientation.Horizontal;
                    sc.Panel1MinSize = 19;
                    sc.SplitterDistance = 19;
                    sc.SizeChanged += (x, y) => { sc.SplitterDistance = 19; };
                    sc.IsSplitterFixed = true;
                    sc.SplitterWidth = 1;

                    sc.Panel1.Controls.Add(label);
                    sc.Panel2.Controls.Add(grid);

                    tab.Controls.Add(sc);
                }
            }
        }

        /// <summary>
        ///   Updates all sheets.
        /// </summary>
        private void UpdateAllSheets()
        {
            UpdateSheets(settingsForm.ListSheetParams);
            AdjustTabs();
        }

        #endregion

        #region Event Handling

        private void WccpUIControl_CustomExport(object sender, CustomExportEventArgs customExportEventArgs)
        {
            string lFilePath = SelectFilePath(customExportEventArgs.ExportType);

            if (!string.IsNullOrEmpty(lFilePath) && customExportEventArgs.ExportPage != null)
            {
                ExportToXls(lFilePath, customExportEventArgs.ExportType);
            }

            customExportEventArgs.Handled = true;
        }

        private void ExportToXls(string filePath, ExportToType type)
        {
            var baseTab = TabControl.TabPages[0] as BaseTab;
            PrintingSystem lPrintingSystem = new PrintingSystem();
            PrintableComponentLink lPrintableComponentLink = new PrintableComponentLink();

            lPrintableComponentLink.CreateReportHeaderArea += printableComponentLink_CreateReportHeaderArea;
            //lPrintableComponentLink.CreateReportFooterArea += printableComponentLinkOnCreateDetailFooterArea;

            lPrintingSystem.Links.AddRange(new object[] { lPrintableComponentLink });

            CustomGridControl grid = baseTab.GetGrid(0).GridControl;
            GridView view = grid.MainView as GridView;
            view.LevelIndent = 0;

            lPrintableComponentLink.Component = grid;
            lPrintableComponentLink.CreateDocument();
            lPrintableComponentLink.PrintingSystem.ExportToXlsx(filePath, new XlsxExportOptions { SheetName = "Summary" });

            view.LevelIndent = -1;
        }

        private void WccpUIControl_RefreshClick(object sender, XtraTabPage selectedPage)
        {
            AdjustTabs();
        }

        private void printableComponentLink_CreateReportHeaderArea(object sender, CreateAreaEventArgs e)
        {
            var baseTab = TabControl.TabPages[0] as BaseTab;
            //Set report settings
            var sheetParams = baseTab.SheetSettings.Where(s => !string.IsNullOrEmpty(s.DisplayParamName)).ToArray();
            var view = baseTab.GetGrid(0).DefaultView;

            TextBrick lDateFromHeader;
            lDateFromHeader = e.Graph.DrawString("Регион:", Color.Black, new RectangleF(0, 0, view.VisibleColumns[0].Width, 20), BorderSide.None);
            lDateFromHeader.Font = new Font("Calibri", 11, FontStyle.Bold);
            lDateFromHeader.StringFormat = new BrickStringFormat(StringAlignment.Far);

            int viewWidth = view.VisibleColumns.Sum(c => c.Width);

            TextBrick lDateToHeader;
            lDateToHeader = e.Graph.DrawString(sheetParams[0].DisplayValue.ToString(), Color.Black, new RectangleF(view.VisibleColumns[0].Width, 0, viewWidth - view.VisibleColumns[0].Width, 20), BorderSide.None);
            lDateToHeader.Font = new Font("Calibri", 11);
            lDateToHeader.StringFormat = new BrickStringFormat(StringAlignment.Near);
        }

        private void printableComponentLinkOnCreateDetailFooterArea(object sender, CreateAreaEventArgs e)
        {
            var baseTab = TabControl.TabPages[0] as BaseTab;
            //Set report settings
            var sheetParams = baseTab.SheetSettings.Where(s => !string.IsNullOrEmpty(s.DisplayParamName)).ToArray();
            var view = baseTab.GetGrid(0).DefaultView;

            for (int i = 2; i < view.VisibleColumns.Count; i++)
            {
                TextBrick lDateFromHeader;
                lDateFromHeader = e.Graph.DrawString(view.VisibleColumns[i].SummaryText, Color.Black, new RectangleF(800, 20, view.VisibleColumns[i].Width, 20), BorderSide.Bottom);
                lDateFromHeader.Font = new Font("Tahoma", 9, FontStyle.Bold);
                lDateFromHeader.StringFormat = new BrickStringFormat(StringAlignment.Far);
            }
        }

        /// <summary>
        ///   WCCPs the UI control_ settings form click.
        /// </summary>
        /// <param name = "sender">The sender.</param>
        /// <param name = "selectedPage">The selected page.</param>
        private void WccpUIControl_SettingsFormClick(object sender, XtraTabPage selectedPage)
        {
            if (settingsForm.ShowDialog() == DialogResult.OK)
            {
                UpdateAllSheets();
            }
        }

        #endregion

        #region Class Methods

        //private static void ApplyBorders(Range range)
        //{
        //    range.Borders[XlBordersIndex.xlEdgeTop].LineStyle = XlLineStyle.xlContinuous;
        //    range.Borders[XlBordersIndex.xlEdgeTop].Weight = XlBorderWeight.xlMedium;
        //    range.Borders[XlBordersIndex.xlEdgeBottom].LineStyle = XlLineStyle.xlContinuous;
        //    range.Borders[XlBordersIndex.xlEdgeBottom].Weight = XlBorderWeight.xlMedium;
        //    range.Borders[XlBordersIndex.xlEdgeLeft].LineStyle = XlLineStyle.xlContinuous;
        //    range.Borders[XlBordersIndex.xlEdgeLeft].Weight = XlBorderWeight.xlMedium;
        //    range.Borders[XlBordersIndex.xlEdgeRight].LineStyle = XlLineStyle.xlContinuous;
        //    range.Borders[XlBordersIndex.xlEdgeRight].Weight = XlBorderWeight.xlMedium;
        //    if (range.Rows.Count > 1)
        //    {
        //        range.Borders[XlBordersIndex.xlInsideHorizontal].LineStyle = XlLineStyle.xlContinuous;
        //        range.Borders[XlBordersIndex.xlInsideHorizontal].Weight = XlBorderWeight.xlThin;
        //    }
        //    if (range.Columns.Count > 1)
        //    {
        //        range.Borders[XlBordersIndex.xlInsideVertical].LineStyle = XlLineStyle.xlContinuous;
        //        range.Borders[XlBordersIndex.xlInsideVertical].Weight = XlBorderWeight.xlThin;
        //    }
        //}

        //private static void ExcelApplyBackColor(Range rangeGroupRow)
        //{
        //    rangeGroupRow.Interior.Pattern = XlPattern.xlPatternSolid;
        //    rangeGroupRow.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.LightGray);

        //    ApplyBorders(rangeGroupRow);
        //}

        private static int GetGroupRowCount(GridView view)
        {
            for (int i = -1; i >= int.MinValue; i--)
            {
                if (!view.IsValidRowHandle(i))
                    return -(i + 1);
            }
            return 0;
        }

        #endregion
    }
}