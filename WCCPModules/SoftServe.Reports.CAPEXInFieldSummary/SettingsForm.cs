using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using Logica.Reports.BaseReportControl;
using Logica.Reports.BaseReportControl.Helpers;
using Logica.Reports.BaseReportControl.Utility;
using Logica.Reports.ConfigXmlParser.Model;
using SoftServe.Reports.CAPEXInFieldSummary.DataProvider;
using SoftServe.Reports.CAPEXInFieldSummary.Properties;
using System.Linq;

namespace SoftServe.Reports.CAPEXInFieldSummary
{
    public partial class SettingsForm : XtraForm
    {
        public bool FormDisabled { get; set; }
        private List<SheetParamCollection> listSheetParams = new List<SheetParamCollection>();
        private Report report;
        private string selectedRegion;
        private string selectedRegionName;
        private string selectedEquipment;
        public bool ShowTS { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="SettingsForm"/> class.
        /// </summary>
        /// <param name="report">The report.</param>
        public SettingsForm(Report report)
        {
            this.report = report;

            InitializeComponent();
            BuildSettings();
        }

        /// <summary>
        /// Gets the list sheet params.
        /// </summary>
        /// <value>The list sheet params.</value>
        public List<SheetParamCollection> ListSheetParams
        {
            get { BuildSheetSettings(); return listSheetParams; }
        }

        /// <summary>
        /// Validates the data.
        /// </summary>
        /// <returns>True if data is valid otherwise false.</returns>
        public bool ValidateData()
        {
            btnYes.Enabled = chkBoxRegion.CheckedIndices.Count > 0 && !string.IsNullOrEmpty(ccbEquipment.EditValue as string);
            return chkBoxRegion.CheckedIndices.Count > 0;
        }

        private void BuildSettings()
        {
            //Text = string.Format(Resources.ParamsFormCaption, Constants.ReportCaption);

            // Select data for Region
            chkBoxRegion.DataSource = DataAccessProvider.GetRegions();
            chkBoxRegion.DisplayMember = SqlConstants.PROC_GET_REGION_FLD_NAME;
            chkBoxRegion.ValueMember = SqlConstants.PROC_GET_REGION_FLD_ID;
            chkBoxRegion.CheckAll();

            ccbEquipment.Properties.Items.Clear();
            foreach (var row in DataAccessProvider.GetEquipment().AsEnumerable())
            {
                ccbEquipment.Properties.Items.Add(row[SqlConstants.PROC_GET_EQUIPMENT_FLD_ID], row[SqlConstants.PROC_GET_EQUIPMENT_FLD_NAME].ToString(), CheckState.Checked, true);
            }
            //ccbEquipment.Properties.DisplayMember = SqlConstants.PROC_GET_EQUIPMENT_FLD_NAME;
            //ccbEquipment.Properties.ValueMember = SqlConstants.PROC_GET_EQUIPMENT_FLD_ID;
            //ccbEquipment.Properties.DataSource = DataAccessProvider.GetEquipment();
            //foreach (CheckedListBoxItem item in ccbEquipment.Properties.Items)
            //{
            //    item.CheckState = CheckState.Checked;
            //}


            if (chkBoxRegion.Items.Count == 1)
            {
                chkBoxRegion.Items[0].Enabled = false;
                FormDisabled = true;
            }

            ValidateData();
        }

        private void BuildSheetSettings()
        {
            if (report != null)
            {
                listSheetParams.Clear();

                foreach (Tab tab in report.Tabs)
                {
                    listSheetParams.Add(GetParamCollection(tab.Id));
                }
            }
        }

        private SheetParamCollection GetParamCollection(Guid tabId)
        {
            SheetParamCollection parameterCollection = new SheetParamCollection
                                                           {TabId = tabId, TableDataType = TableType.Fact};

            parameterCollection.Add(new SheetParam
                                        {
                                            SqlParamName = SqlConstants.PARAM_REGIONS,
                                            DisplayParamName = Resources.Region,
                                            DisplayValue = selectedRegionName,
                                            Value = selectedRegion
                                        });
            parameterCollection.Add(new SheetParam {SqlParamName = SqlConstants.PARAM_USE_TS, Value = ShowTS});
            parameterCollection.Add(new SheetParam {SqlParamName = SqlConstants.PARAM_IS_SUMMARY, Value = true});
            parameterCollection.Add(new SheetParam { SqlParamName = SqlConstants.PARAM_POS_GROUPS, Value = selectedEquipment});
            return parameterCollection;
        }

        private void btnYes_Click(Object sender, EventArgs e)
        {
            if (ValidateData())
            {
                StringBuilder selectedRegionNameSb = new StringBuilder();
                StringBuilder selectedRegionSb = new StringBuilder();

                foreach (int checkedIndex in chkBoxRegion.CheckedIndices)
                {
                    if (selectedRegionNameSb.Length > 0)
                    {
                        selectedRegionNameSb.Append(", ");
                    }
                    selectedRegionNameSb.Append(chkBoxRegion.GetItemText(checkedIndex));

                    if (selectedRegionSb.Length > 0)
                    {
                        selectedRegionSb.Append(",");
                    }
                    selectedRegionSb.Append(chkBoxRegion.GetItemValue(checkedIndex));
                }

                selectedRegionName = selectedRegionNameSb.ToString();
                selectedRegion = selectedRegionSb.ToString();
                if (ccbEquipment.EditValue is string && ((string)ccbEquipment.EditValue).Split(ccbEquipment.Properties.SeparatorChar).Length == ccbEquipment.Properties.Items.Count)
                {
                    selectedEquipment = null;
                }
                else
                {
                    selectedEquipment = ccbEquipment.EditValue as string;
                }
            
                ShowTS = chkShowTS.Checked;
                DialogResult = DialogResult.OK;
            }
        }

        private void chkSelectAll_CheckedChanged(object sender, EventArgs e)
        {
            chkBoxRegion.ItemCheck -= chkBoxRegion_ItemCheck;
            if (chkSelectAll.Checked)
            {
                chkBoxRegion.CheckAll();
            }
            else
            {
                chkBoxRegion.UnCheckAll();
            }
            chkBoxRegion.ItemCheck += chkBoxRegion_ItemCheck;
            
            ValidateData();
        }

        private void chkBoxRegion_ItemCheck(object sender, DevExpress.XtraEditors.Controls.ItemCheckEventArgs e)
        {
            chkSelectAll.CheckedChanged -= chkSelectAll_CheckedChanged;
            chkSelectAll.Checked = chkBoxRegion.CheckedIndices.Count == chkBoxRegion.ItemCount;
            chkSelectAll.CheckedChanged += chkSelectAll_CheckedChanged;
            
            ValidateData();
        }

        private void ccbEquipment_EditValueChanged(object sender, EventArgs e)
        {
            ValidateData();
        }
    }
}