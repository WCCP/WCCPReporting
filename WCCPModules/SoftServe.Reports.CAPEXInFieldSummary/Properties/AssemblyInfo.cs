﻿using System.Reflection;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("SoftServe.Reports.CAPEXInFieldSummary")]
[assembly: AssemblyDescription("Capex in Field Summary Report")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Logica, SoftServe Inc.")]
[assembly: AssemblyProduct("WCCP Reporting")]
[assembly: AssemblyCopyright("Copyright (c) 2010 Logica, 2011 - 2014 SoftServe Inc.")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("ad544496-e850-42b0-aacf-036db950fa3d")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.0.0.64999")]
[assembly: AssemblyFileVersion("1.0.0.64999")]
