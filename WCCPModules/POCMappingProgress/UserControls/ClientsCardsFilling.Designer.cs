namespace WccpReporting.POCMappingProgress.UserControls
{
    partial class ClientsCardsFilling
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pivotGridFieldM1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldM2 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFiedlM3 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridControl = new DevExpress.XtraPivotGrid.PivotGridControl();
            this.pivotGridFieldM4 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldStatusName = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldOLTradingName = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldOLName = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldOLDeliveryAddress = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldOLAddress = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldOLDirector = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldOLTelephone = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldLocation = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldPremium = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldNet = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldZkpo = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldEMail = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridField1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridField2 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridField3 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridField4 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridField6 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridField5 = new DevExpress.XtraPivotGrid.PivotGridField();
            ((System.ComponentModel.ISupportInitialize)(this.pivotGridControl)).BeginInit();
            this.SuspendLayout();
            // 
            // pivotGridFieldM1
            // 
            this.pivotGridFieldM1.Area = DevExpress.XtraPivotGrid.PivotArea.RowArea;
            this.pivotGridFieldM1.AreaIndex = 3;
            this.pivotGridFieldM1.Caption = "M1";
            this.pivotGridFieldM1.FieldName = "M1";
            this.pivotGridFieldM1.Name = "pivotGridFieldM1";
            // 
            // pivotGridFieldM2
            // 
            this.pivotGridFieldM2.Area = DevExpress.XtraPivotGrid.PivotArea.RowArea;
            this.pivotGridFieldM2.AreaIndex = 2;
            this.pivotGridFieldM2.Caption = "M2";
            this.pivotGridFieldM2.FieldName = "M2";
            this.pivotGridFieldM2.Name = "pivotGridFieldM2";
            // 
            // pivotGridFiedlM3
            // 
            this.pivotGridFiedlM3.Area = DevExpress.XtraPivotGrid.PivotArea.RowArea;
            this.pivotGridFiedlM3.AreaIndex = 1;
            this.pivotGridFiedlM3.Caption = "M3";
            this.pivotGridFiedlM3.FieldName = "M3";
            this.pivotGridFiedlM3.Name = "pivotGridFiedlM3";
            // 
            // pivotGridControl
            // 
            this.pivotGridControl.Cursor = System.Windows.Forms.Cursors.Default;
            this.pivotGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pivotGridControl.Fields.AddRange(new DevExpress.XtraPivotGrid.PivotGridField[] {
            this.pivotGridFieldM1,
            this.pivotGridFieldM2,
            this.pivotGridFiedlM3,
            this.pivotGridFieldM4,
            this.pivotGridFieldStatusName,
            this.pivotGridFieldOLTradingName,
            this.pivotGridFieldOLName,
            this.pivotGridFieldOLDeliveryAddress,
            this.pivotGridFieldOLAddress,
            this.pivotGridFieldOLDirector,
            this.pivotGridFieldOLTelephone,
            this.pivotGridFieldLocation,
            this.pivotGridFieldPremium,
            this.pivotGridFieldNet,
            this.pivotGridFieldZkpo,
            this.pivotGridFieldEMail,
            this.pivotGridField1,
            this.pivotGridField2,
            this.pivotGridField3,
            this.pivotGridField4,
            this.pivotGridField6,
            this.pivotGridField5});
            this.pivotGridControl.Location = new System.Drawing.Point(0, 0);
            this.pivotGridControl.Name = "pivotGridControl";
            this.pivotGridControl.OptionsDataField.Area = DevExpress.XtraPivotGrid.PivotDataArea.RowArea;
            this.pivotGridControl.OptionsDataField.AreaIndex = 4;
            this.pivotGridControl.OptionsDataField.RowHeaderWidth = 184;
            this.pivotGridControl.Size = new System.Drawing.Size(807, 315);
            this.pivotGridControl.TabIndex = 0;
            this.pivotGridControl.CustomSummary += new DevExpress.XtraPivotGrid.PivotGridCustomSummaryEventHandler(this.pivotGridControl_CustomSummary);
            this.pivotGridControl.FieldValueDisplayText += new DevExpress.XtraPivotGrid.PivotFieldDisplayTextEventHandler(this.pivotGridControl_FieldValueDisplayText);
            this.pivotGridControl.CustomCellDisplayText += new DevExpress.XtraPivotGrid.PivotCellDisplayTextEventHandler(this.pivotGridControl_CustomCellDisplayText);
            // 
            // pivotGridFieldM4
            // 
            this.pivotGridFieldM4.Area = DevExpress.XtraPivotGrid.PivotArea.RowArea;
            this.pivotGridFieldM4.AreaIndex = 0;
            this.pivotGridFieldM4.Caption = "M4";
            this.pivotGridFieldM4.FieldName = "M4";
            this.pivotGridFieldM4.Name = "pivotGridFieldM4";
            // 
            // pivotGridFieldStatusName
            // 
            this.pivotGridFieldStatusName.Area = DevExpress.XtraPivotGrid.PivotArea.ColumnArea;
            this.pivotGridFieldStatusName.AreaIndex = 0;
            this.pivotGridFieldStatusName.Caption = "����/�����";
            this.pivotGridFieldStatusName.FieldName = "StatusName";
            this.pivotGridFieldStatusName.Name = "pivotGridFieldStatusName";
            // 
            // pivotGridFieldOLTradingName
            // 
            this.pivotGridFieldOLTradingName.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
            this.pivotGridFieldOLTradingName.AreaIndex = 0;
            this.pivotGridFieldOLTradingName.Caption = "������������� ����. ��� ��";
            this.pivotGridFieldOLTradingName.EmptyCellText = "0";
            this.pivotGridFieldOLTradingName.EmptyValueText = "0";
            this.pivotGridFieldOLTradingName.FieldName = "OL_TradingName";
            this.pivotGridFieldOLTradingName.Name = "pivotGridFieldOLTradingName";
            this.pivotGridFieldOLTradingName.SummaryType = DevExpress.Data.PivotGrid.PivotSummaryType.Custom;
            this.pivotGridFieldOLTradingName.Width = 500;
            // 
            // pivotGridFieldOLName
            // 
            this.pivotGridFieldOLName.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
            this.pivotGridFieldOLName.AreaIndex = 1;
            this.pivotGridFieldOLName.Caption = "������������� ��. ��� ��";
            this.pivotGridFieldOLName.EmptyCellText = "0";
            this.pivotGridFieldOLName.EmptyValueText = "0";
            this.pivotGridFieldOLName.FieldName = "OL_Name";
            this.pivotGridFieldOLName.Name = "pivotGridFieldOLName";
            this.pivotGridFieldOLName.SummaryType = DevExpress.Data.PivotGrid.PivotSummaryType.Custom;
            this.pivotGridFieldOLName.Width = 500;
            // 
            // pivotGridFieldOLDeliveryAddress
            // 
            this.pivotGridFieldOLDeliveryAddress.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
            this.pivotGridFieldOLDeliveryAddress.AreaIndex = 2;
            this.pivotGridFieldOLDeliveryAddress.Caption = "������������� ����. ����� ��";
            this.pivotGridFieldOLDeliveryAddress.EmptyCellText = "0";
            this.pivotGridFieldOLDeliveryAddress.EmptyValueText = "0";
            this.pivotGridFieldOLDeliveryAddress.FieldName = "OL_DeliveryAddress";
            this.pivotGridFieldOLDeliveryAddress.Name = "pivotGridFieldOLDeliveryAddress";
            this.pivotGridFieldOLDeliveryAddress.SummaryType = DevExpress.Data.PivotGrid.PivotSummaryType.Custom;
            this.pivotGridFieldOLDeliveryAddress.Width = 500;
            // 
            // pivotGridFieldOLAddress
            // 
            this.pivotGridFieldOLAddress.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
            this.pivotGridFieldOLAddress.AreaIndex = 3;
            this.pivotGridFieldOLAddress.Caption = "������������� ��. ����� ��";
            this.pivotGridFieldOLAddress.EmptyCellText = "0";
            this.pivotGridFieldOLAddress.EmptyValueText = "0";
            this.pivotGridFieldOLAddress.FieldName = "OL_Address";
            this.pivotGridFieldOLAddress.Name = "pivotGridFieldOLAddress";
            this.pivotGridFieldOLAddress.SummaryType = DevExpress.Data.PivotGrid.PivotSummaryType.Custom;
            this.pivotGridFieldOLAddress.Width = 500;
            // 
            // pivotGridFieldOLDirector
            // 
            this.pivotGridFieldOLDirector.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
            this.pivotGridFieldOLDirector.AreaIndex = 4;
            this.pivotGridFieldOLDirector.Caption = "������������� �����. ���� ��";
            this.pivotGridFieldOLDirector.EmptyCellText = "0";
            this.pivotGridFieldOLDirector.EmptyValueText = "0";
            this.pivotGridFieldOLDirector.FieldName = "OL_Director";
            this.pivotGridFieldOLDirector.Name = "pivotGridFieldOLDirector";
            this.pivotGridFieldOLDirector.SummaryType = DevExpress.Data.PivotGrid.PivotSummaryType.Custom;
            this.pivotGridFieldOLDirector.Width = 500;
            // 
            // pivotGridFieldOLTelephone
            // 
            this.pivotGridFieldOLTelephone.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
            this.pivotGridFieldOLTelephone.AreaIndex = 5;
            this.pivotGridFieldOLTelephone.Caption = "������������� ���. ��";
            this.pivotGridFieldOLTelephone.EmptyCellText = "0";
            this.pivotGridFieldOLTelephone.EmptyValueText = "0";
            this.pivotGridFieldOLTelephone.FieldName = "OL_Telephone";
            this.pivotGridFieldOLTelephone.Name = "pivotGridFieldOLTelephone";
            this.pivotGridFieldOLTelephone.SummaryType = DevExpress.Data.PivotGrid.PivotSummaryType.Custom;
            this.pivotGridFieldOLTelephone.Width = 500;
            // 
            // pivotGridFieldLocation
            // 
            this.pivotGridFieldLocation.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
            this.pivotGridFieldLocation.AreaIndex = 6;
            this.pivotGridFieldLocation.Caption = "������������� ���. ����� ��";
            this.pivotGridFieldLocation.EmptyCellText = "0";
            this.pivotGridFieldLocation.EmptyValueText = "0";
            this.pivotGridFieldLocation.FieldName = "City_Id";
            this.pivotGridFieldLocation.Name = "pivotGridFieldLocation";
            this.pivotGridFieldLocation.SummaryType = DevExpress.Data.PivotGrid.PivotSummaryType.Custom;
            this.pivotGridFieldLocation.Width = 500;
            // 
            // pivotGridFieldPremium
            // 
            this.pivotGridFieldPremium.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
            this.pivotGridFieldPremium.AreaIndex = 7;
            this.pivotGridFieldPremium.Caption = "����������� ������������� ��";
            this.pivotGridFieldPremium.EmptyCellText = "0";
            this.pivotGridFieldPremium.EmptyValueText = "0";
            this.pivotGridFieldPremium.FieldName = "CustomField_Id";
            this.pivotGridFieldPremium.Name = "pivotGridFieldPremium";
            // 
            // pivotGridFieldNet
            // 
            this.pivotGridFieldNet.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
            this.pivotGridFieldNet.AreaIndex = 8;
            this.pivotGridFieldNet.Caption = "����������� �������� ����";
            this.pivotGridFieldNet.FieldName = "OL_Network_ID";
            this.pivotGridFieldNet.Name = "pivotGridFieldNet";
            this.pivotGridFieldNet.SummaryType = DevExpress.Data.PivotGrid.PivotSummaryType.Custom;
            this.pivotGridFieldNet.Width = 500;
            // 
            // pivotGridFieldZkpo
            // 
            this.pivotGridFieldZkpo.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
            this.pivotGridFieldZkpo.AreaIndex = 9;
            this.pivotGridFieldZkpo.Caption = "���������� ��� ������";
            this.pivotGridFieldZkpo.EmptyCellText = "0";
            this.pivotGridFieldZkpo.EmptyValueText = "0";
            this.pivotGridFieldZkpo.FieldName = "ZKPO";
            this.pivotGridFieldZkpo.Name = "pivotGridFieldZkpo";
            this.pivotGridFieldZkpo.SummaryType = DevExpress.Data.PivotGrid.PivotSummaryType.Custom;
            this.pivotGridFieldZkpo.Width = 500;
            // 
            // pivotGridFieldEMail
            // 
            this.pivotGridFieldEMail.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
            this.pivotGridFieldEMail.AreaIndex = 10;
            this.pivotGridFieldEMail.Caption = "���������� ��� E-MAIL";
            this.pivotGridFieldEMail.EmptyCellText = "0";
            this.pivotGridFieldEMail.EmptyValueText = "0";
            this.pivotGridFieldEMail.FieldName = "OL_EMail";
            this.pivotGridFieldEMail.Name = "pivotGridFieldEMail";
            this.pivotGridFieldEMail.SummaryType = DevExpress.Data.PivotGrid.PivotSummaryType.Custom;
            this.pivotGridFieldEMail.Width = 500;
            // 
            // pivotGridField1
            // 
            this.pivotGridField1.AreaIndex = 0;
            this.pivotGridField1.Caption = "����. ��� ��";
            this.pivotGridField1.FieldName = "OLTradingName";
            this.pivotGridField1.Name = "pivotGridField1";
            // 
            // pivotGridField2
            // 
            this.pivotGridField2.AreaIndex = 1;
            this.pivotGridField2.Caption = "����. ����� ��";
            this.pivotGridField2.FieldName = "OLDeliveryAddress";
            this.pivotGridField2.Name = "pivotGridField2";
            // 
            // pivotGridField3
            // 
            this.pivotGridField3.AreaIndex = 2;
            this.pivotGridField3.Caption = "��� ��";
            this.pivotGridField3.FieldName = "OL_id";
            this.pivotGridField3.Name = "pivotGridField3";
            // 
            // pivotGridField4
            // 
            this.pivotGridField4.AreaIndex = 3;
            this.pivotGridField4.Caption = "����� ��";
            this.pivotGridField4.FieldName = "OlChannel";
            this.pivotGridField4.Name = "pivotGridField4";
            // 
            // pivotGridField6
            // 
            this.pivotGridField6.AreaIndex = 4;
            this.pivotGridField6.Caption = "����� ��������� �3";
            this.pivotGridField6.FieldName = "Channel_Type";
            this.pivotGridField6.Name = "pivotGridField6";
            // 
            // pivotGridField5
            // 
            this.pivotGridField5.Area = DevExpress.XtraPivotGrid.PivotArea.ColumnArea;
            this.pivotGridField5.AreaIndex = 1;
            this.pivotGridField5.Caption = "�������������� ��";
            this.pivotGridField5.FieldName = "OlOwner";
            this.pivotGridField5.Name = "pivotGridField5";
            // 
            // ClientsCardsFilling
            // 
            this.AutoSize = true;
            this.Controls.Add(this.pivotGridControl);
            this.Name = "ClientsCardsFilling";
            this.Size = new System.Drawing.Size(807, 315);
            ((System.ComponentModel.ISupportInitialize)(this.pivotGridControl)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldM1;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldM2;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFiedlM3;
        private DevExpress.XtraPivotGrid.PivotGridControl pivotGridControl;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldM4;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldOLTradingName;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldOLName;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldOLDeliveryAddress;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldOLAddress;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldOLDirector;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldOLTelephone;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldLocation;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldZkpo;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldStatusName;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldNet;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridField1;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridField2;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridField3;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldPremium;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridField4;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridField5;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridField6;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldEMail;
    }
}
