using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Logica.Reports.Common;
using WccpReporting.POCMappingProgress;
using DevExpress.XtraPivotGrid;
using System.Linq;
using DevExpress.XtraTab;
using DevExpress.XtraGrid.Columns;
using DevExpress.Data;
using WccpReporting.POCMappingProgress.DataProvider;
using WccpReporting.POCMappingProgress.Core;
using WccpReporting.POCMappingProgress.Properties;

namespace WccpReporting.POCMappingProgress.UserControls
{
    public partial class ClientsCardsFilling : UserControl, IReport
    {
        public ClientsCardsFilling()
        {
            InitializeComponent();
        }

        public void ShowReport(SettingsForm setForm)
        {
            try
            {
                DataTable dataTable = DataAccessProvider.GetClientsCardsFillingData(
                    setForm.SelectedWave, setForm.SelectedRegion, setForm.SelectedChanel , setForm.AccntNotCovTerr);

                pivotGridControl.DataSource = dataTable;
            }
            catch (Exception ex)
            {
                ErrorManager.ShowErrorBox(ex.Message);
            }
        }

        private void pivotGridControl_FieldValueDisplayText(object sender, PivotFieldDisplayTextEventArgs e)
        {
            if (e.DisplayText == CommonConstants.QuestionnairiesFilling.GrandTotalCaption)
            {
                if (e.IsColumn)
                {
                    e.DisplayText = Resources.AllTt;
                }
                else
                {
                    e.DisplayText = Resources.AllTtSummary;
                }
            }
        }

        private void pivotGridControl_CustomCellDisplayText(object sender, PivotCellDisplayTextEventArgs e)
        {
            if (e.DataField == pivotGridFieldNet || e.DataField == pivotGridFieldOLDirector) 
            {
                if (e.Value == null)
                {
                    e.DisplayText = "0";
                }
                else
                {
                    e.DisplayText = e.Value.ToString();
                }
            }
        }

        private void pivotGridControl_CustomSummary(object sender, PivotGridCustomSummaryEventArgs e)
        {
            if (e.DataField != pivotGridFieldOLTradingName
                && e.DataField != pivotGridFieldOLName
                && e.DataField != pivotGridFieldOLDeliveryAddress
                && e.DataField != pivotGridFieldOLAddress
                && e.DataField != pivotGridFieldOLDirector
                && e.DataField != pivotGridFieldOLTelephone
                && e.DataField != pivotGridFieldLocation
                && e.DataField != pivotGridFieldZkpo
                && e.DataField != pivotGridFieldNet
                && e.DataField != pivotGridFieldPremium
                && e.DataField != pivotGridFieldEMail) return;

            PivotDrillDownDataSource ds = e.CreateDrillDownDataSource();

            int value = 0;

            foreach (PivotDrillDownDataRow row in ds)
            {
                int count;
                if (row[e.DataField.FieldName] != null && int.TryParse(row[e.DataField.FieldName].ToString(), out count))
                {
                    value += count;
                }
            }

            e.CustomValue = value;
        }

        #region IReport Members

        public void ShowPrintPreview()
        {
            this.pivotGridControl.ShowPrintPreview();
        }

        public void ExportToXls(string fileName)
        {
            // export
            this.pivotGridControl.ExportToXls(fileName);
        }
        public void ExportToXlsx(string fileName)
        {
            // export
            this.pivotGridControl.ExportToXlsx(fileName);
        }

        #endregion
    }
}