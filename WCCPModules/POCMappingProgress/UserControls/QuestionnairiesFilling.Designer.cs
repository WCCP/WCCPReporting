namespace WccpReporting.POCMappingProgress.UserControls
{
    partial class QuestionnairiesFilling
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pivotGridFieldTypeTT = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldAddressTT = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldTradingNameTT = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldResponse = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldNameTT = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldTTId = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldRegion = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldBaseNew = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldChanel = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldM4 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldM3 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldM2 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldM1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridControl = new DevExpress.XtraPivotGrid.PivotGridControl();
            this.pivotGridFieldCustName = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridField1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldDistr = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldCount = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldValuePerc = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridField2 = new DevExpress.XtraPivotGrid.PivotGridField();
            ((System.ComponentModel.ISupportInitialize)(this.pivotGridControl)).BeginInit();
            this.SuspendLayout();
            // 
            // pivotGridFieldTypeTT
            // 
            this.pivotGridFieldTypeTT.AreaIndex = 7;
            this.pivotGridFieldTypeTT.Caption = "��� ��";
            this.pivotGridFieldTypeTT.FieldName = "OLtype_name";
            this.pivotGridFieldTypeTT.Name = "pivotGridFieldTypeTT";
            // 
            // pivotGridFieldAddressTT
            // 
            this.pivotGridFieldAddressTT.AreaIndex = 4;
            this.pivotGridFieldAddressTT.Caption = "��. ����� ��";
            this.pivotGridFieldAddressTT.FieldName = "OLAddress";
            this.pivotGridFieldAddressTT.Name = "pivotGridFieldAddressTT";
            // 
            // pivotGridFieldTradingNameTT
            // 
            this.pivotGridFieldTradingNameTT.AreaIndex = 5;
            this.pivotGridFieldTradingNameTT.Caption = "����. ��� ��";
            this.pivotGridFieldTradingNameTT.FieldName = "OLTradingName";
            this.pivotGridFieldTradingNameTT.Name = "pivotGridFieldTradingNameTT";
            // 
            // pivotGridFieldResponse
            // 
            this.pivotGridFieldResponse.Area = DevExpress.XtraPivotGrid.PivotArea.ColumnArea;
            this.pivotGridFieldResponse.AreaIndex = 1;
            this.pivotGridFieldResponse.Caption = "���� �����";
            this.pivotGridFieldResponse.FieldName = "ResponseName";
            this.pivotGridFieldResponse.GrandTotalText = "����� ��";
            this.pivotGridFieldResponse.Name = "pivotGridFieldResponse";
            // 
            // pivotGridFieldNameTT
            // 
            this.pivotGridFieldNameTT.AreaIndex = 6;
            this.pivotGridFieldNameTT.Caption = "��. ��� ��";
            this.pivotGridFieldNameTT.FieldName = "OLName";
            this.pivotGridFieldNameTT.Name = "pivotGridFieldNameTT";
            // 
            // pivotGridFieldTTId
            // 
            this.pivotGridFieldTTId.AreaIndex = 3;
            this.pivotGridFieldTTId.Caption = "��� ��";
            this.pivotGridFieldTTId.FieldName = "OL_id";
            this.pivotGridFieldTTId.Name = "pivotGridFieldTTId";
            // 
            // pivotGridFieldRegion
            // 
            this.pivotGridFieldRegion.AreaIndex = 2;
            this.pivotGridFieldRegion.Caption = "������";
            this.pivotGridFieldRegion.FieldName = "Region_name";
            this.pivotGridFieldRegion.Name = "pivotGridFieldRegion";
            // 
            // pivotGridFieldBaseNew
            // 
            this.pivotGridFieldBaseNew.Area = DevExpress.XtraPivotGrid.PivotArea.ColumnArea;
            this.pivotGridFieldBaseNew.AreaIndex = 0;
            this.pivotGridFieldBaseNew.Caption = "����/�����";
            this.pivotGridFieldBaseNew.FieldName = "StatusName";
            this.pivotGridFieldBaseNew.GrandTotalText = "����� ��";
            this.pivotGridFieldBaseNew.Name = "pivotGridFieldBaseNew";
            this.pivotGridFieldBaseNew.TotalsVisibility = DevExpress.XtraPivotGrid.PivotTotalsVisibility.None;
            // 
            // pivotGridFieldChanel
            // 
            this.pivotGridFieldChanel.AreaIndex = 0;
            this.pivotGridFieldChanel.Caption = "����� ��������� �3";
            this.pivotGridFieldChanel.FieldName = "Channel_Type";
            this.pivotGridFieldChanel.Name = "pivotGridFieldChanel";
            // 
            // pivotGridFieldM4
            // 
            this.pivotGridFieldM4.Area = DevExpress.XtraPivotGrid.PivotArea.RowArea;
            this.pivotGridFieldM4.AreaIndex = 0;
            this.pivotGridFieldM4.Caption = "M4";
            this.pivotGridFieldM4.FieldName = "M4";
            this.pivotGridFieldM4.GrandTotalText = "����� ��";
            this.pivotGridFieldM4.Name = "pivotGridFieldM4";
            // 
            // pivotGridFieldM3
            // 
            this.pivotGridFieldM3.Area = DevExpress.XtraPivotGrid.PivotArea.RowArea;
            this.pivotGridFieldM3.AreaIndex = 1;
            this.pivotGridFieldM3.Caption = "M3";
            this.pivotGridFieldM3.FieldName = "M3";
            this.pivotGridFieldM3.Name = "pivotGridFieldM3";
            // 
            // pivotGridFieldM2
            // 
            this.pivotGridFieldM2.Area = DevExpress.XtraPivotGrid.PivotArea.RowArea;
            this.pivotGridFieldM2.AreaIndex = 2;
            this.pivotGridFieldM2.Caption = "M2";
            this.pivotGridFieldM2.FieldName = "M2";
            this.pivotGridFieldM2.Name = "pivotGridFieldM2";
            // 
            // pivotGridFieldM1
            // 
            this.pivotGridFieldM1.Area = DevExpress.XtraPivotGrid.PivotArea.RowArea;
            this.pivotGridFieldM1.AreaIndex = 3;
            this.pivotGridFieldM1.Caption = "M1";
            this.pivotGridFieldM1.FieldName = "M1";
            this.pivotGridFieldM1.Name = "pivotGridFieldM1";
            // 
            // pivotGridControl
            // 
            this.pivotGridControl.Cursor = System.Windows.Forms.Cursors.Default;
            this.pivotGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pivotGridControl.Fields.AddRange(new DevExpress.XtraPivotGrid.PivotGridField[] {
            this.pivotGridFieldM1,
            this.pivotGridFieldM2,
            this.pivotGridFieldM3,
            this.pivotGridFieldM4,
            this.pivotGridFieldChanel,
            this.pivotGridFieldBaseNew,
            this.pivotGridFieldRegion,
            this.pivotGridFieldCustName,
            this.pivotGridFieldTTId,
            this.pivotGridFieldNameTT,
            this.pivotGridFieldResponse,
            this.pivotGridFieldTradingNameTT,
            this.pivotGridFieldAddressTT,
            this.pivotGridFieldTypeTT,
            this.pivotGridField1,
            this.pivotGridFieldDistr,
            this.pivotGridFieldCount,
            this.pivotGridFieldValuePerc,
            this.pivotGridField2});
            this.pivotGridControl.Location = new System.Drawing.Point(0, 0);
            this.pivotGridControl.Name = "pivotGridControl";
            this.pivotGridControl.OptionsDataField.Area = DevExpress.XtraPivotGrid.PivotDataArea.RowArea;
            this.pivotGridControl.OptionsDataField.AreaIndex = 4;
            this.pivotGridControl.Size = new System.Drawing.Size(1469, 772);
            this.pivotGridControl.TabIndex = 0;
            this.pivotGridControl.FieldAreaChanging += new DevExpress.XtraPivotGrid.PivotAreaChangingEventHandler(this.pivotGridControl_FieldAreaChanging);
            this.pivotGridControl.CustomSummary += new DevExpress.XtraPivotGrid.PivotGridCustomSummaryEventHandler(this.pivotGridControl_CustomSummary);
            this.pivotGridControl.FieldAreaChanged += new DevExpress.XtraPivotGrid.PivotFieldEventHandler(this.pivotGridControl_FieldAreaChanged);
            this.pivotGridControl.FieldValueDisplayText += new DevExpress.XtraPivotGrid.PivotFieldDisplayTextEventHandler(this.pivotGridControl_FieldValueDisplayText);
            this.pivotGridControl.CustomCellDisplayText += new DevExpress.XtraPivotGrid.PivotCellDisplayTextEventHandler(this.pivotGridControl_CustomCellDisplayText);
            // 
            // pivotGridFieldCustName
            // 
            this.pivotGridFieldCustName.AreaIndex = 1;
            this.pivotGridFieldCustName.Caption = "����� �����.";
            this.pivotGridFieldCustName.FieldName = "Cust_NAME";
            this.pivotGridFieldCustName.Name = "pivotGridFieldCustName";
            // 
            // pivotGridField1
            // 
            this.pivotGridField1.AreaIndex = 9;
            this.pivotGridField1.Caption = "����� ��";
            this.pivotGridField1.FieldName = "OlChannel";
            this.pivotGridField1.Name = "pivotGridField1";
            // 
            // pivotGridFieldDistr
            // 
            this.pivotGridFieldDistr.AreaIndex = 8;
            this.pivotGridFieldDistr.Caption = "������������";
            this.pivotGridFieldDistr.FieldName = "DISTR_NAME";
            this.pivotGridFieldDistr.Name = "pivotGridFieldDistr";
            // 
            // pivotGridFieldCount
            // 
            this.pivotGridFieldCount.AllowedAreas = ((DevExpress.XtraPivotGrid.PivotGridAllowedAreas)(((DevExpress.XtraPivotGrid.PivotGridAllowedAreas.RowArea | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.ColumnArea) 
            | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.DataArea)));
            this.pivotGridFieldCount.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
            this.pivotGridFieldCount.AreaIndex = 0;
            this.pivotGridFieldCount.Caption = "���-��";
            this.pivotGridFieldCount.EmptyCellText = "0";
            this.pivotGridFieldCount.EmptyValueText = "0";
            this.pivotGridFieldCount.Name = "pivotGridFieldCount";
            this.pivotGridFieldCount.SummaryType = DevExpress.Data.PivotGrid.PivotSummaryType.Custom;
            this.pivotGridFieldCount.UnboundFieldName = "pivotGridFieldCount123";
            this.pivotGridFieldCount.UnboundType = DevExpress.Data.UnboundColumnType.Integer;
            this.pivotGridFieldCount.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            // 
            // pivotGridFieldValuePerc
            // 
            this.pivotGridFieldValuePerc.AllowedAreas = ((DevExpress.XtraPivotGrid.PivotGridAllowedAreas)(((DevExpress.XtraPivotGrid.PivotGridAllowedAreas.RowArea | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.ColumnArea) 
            | DevExpress.XtraPivotGrid.PivotGridAllowedAreas.DataArea)));
            this.pivotGridFieldValuePerc.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
            this.pivotGridFieldValuePerc.AreaIndex = 1;
            this.pivotGridFieldValuePerc.Caption = "���-�� %";
            this.pivotGridFieldValuePerc.EmptyCellText = "0 %";
            this.pivotGridFieldValuePerc.EmptyValueText = "0 %";
            this.pivotGridFieldValuePerc.Name = "pivotGridFieldValuePerc";
            this.pivotGridFieldValuePerc.SortMode = DevExpress.XtraPivotGrid.PivotSortMode.Custom;
            this.pivotGridFieldValuePerc.SummaryType = DevExpress.Data.PivotGrid.PivotSummaryType.Custom;
            this.pivotGridFieldValuePerc.UnboundFieldName = "pivotGridFieldValuePerc123";
            this.pivotGridFieldValuePerc.UnboundType = DevExpress.Data.UnboundColumnType.Integer;
            this.pivotGridFieldValuePerc.UseNativeFormat = DevExpress.Utils.DefaultBoolean.False;
            this.pivotGridFieldValuePerc.ValueFormat.FormatString = "{0}%";
            this.pivotGridFieldValuePerc.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            // 
            // pivotGridField2
            // 
            this.pivotGridField2.Area = DevExpress.XtraPivotGrid.PivotArea.ColumnArea;
            this.pivotGridField2.AreaIndex = 2;
            this.pivotGridField2.Caption = "�������������� ��";
            this.pivotGridField2.FieldName = "OlOwner";
            this.pivotGridField2.Name = "pivotGridField2";
            // 
            // QuestionnairiesFilling
            // 
            this.Controls.Add(this.pivotGridControl);
            this.Name = "QuestionnairiesFilling";
            this.Size = new System.Drawing.Size(1469, 772);
            ((System.ComponentModel.ISupportInitialize)(this.pivotGridControl)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldValuePerc;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldCount;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldTypeTT;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldAddressTT;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldTradingNameTT;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldResponse;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldNameTT;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldTTId;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldRegion;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldBaseNew;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldChanel;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldM4;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldM3;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldM2;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldM1;
        private DevExpress.XtraPivotGrid.PivotGridControl pivotGridControl;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldCustName;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldDistr;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridField1;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridField2;





    }
}
