using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Logica.Reports.Common;
using WccpReporting.POCMappingProgress;
using DevExpress.XtraPivotGrid;
using System.Linq;
using DevExpress.XtraTab;
using DevExpress.XtraGrid.Columns;
using DevExpress.Data;
using WccpReporting.POCMappingProgress.DataProvider;
using WccpReporting.POCMappingProgress.Core;
using WccpReporting.POCMappingProgress.Properties;

namespace WccpReporting.POCMappingProgress.UserControls
{
    public partial class QuestionnairiesFilling : UserControl, IReport
    {
        public QuestionnairiesFilling()
        {
            InitializeComponent();
        }

        public void ShowReport(SettingsForm setForm)
        {
            try
            {
                DataTable dataTable = DataAccessProvider.GetQuestionnairiesFillingData(
                    setForm.SelectedWave, setForm.SelectedRegion, setForm.SelectedChanel, setForm.AccntNotCovTerr);

                pivotGridControl.DataSource = dataTable;
            }
            catch (Exception ex)
            {
                ErrorManager.ShowErrorBox(ex.Message);
            }
        }

        private void pivotGridControl_CustomSummary(object sender, PivotGridCustomSummaryEventArgs e)
        {
            if (e.DataField == pivotGridFieldCount || e.DataField == pivotGridFieldValuePerc)
            {
                e.CustomValue = e.CreateDrillDownDataSource().RowCount.ToString();
            }
        }

        private void pivotGridControl_CustomCellDisplayText(object sender, PivotCellDisplayTextEventArgs e)
        {
            if (e.DataField == pivotGridFieldValuePerc)
            {
                object absVal = e.GetColumnGrandTotal(e.DataField);
                if (e.Value != null && absVal != null)
                {
                    e.DisplayText = Math.Round(int.Parse(e.Value.ToString()) * 100.0 / int.Parse(absVal.ToString()), 2) + " %";
                }
            }
        }

        private void pivotGridControl_FieldValueDisplayText(object sender, PivotFieldDisplayTextEventArgs e)
        {
            if (e.DisplayText == CommonConstants.QuestionnairiesFilling.GrandTotalCaption && e.IsColumn) 
            {
                e.DisplayText = Resources.AllTt;
            }
        }

        bool summarySetted = false;
        private void pivotGridControl_FieldAreaChanging(object sender, PivotAreaChangingEventArgs e)
        {
            summarySetted = false;
        }

        private void pivotGridControl_FieldAreaChanged(object sender, PivotFieldEventArgs e)
        {
            if (e.Field == pivotGridFieldCount || e.Field == pivotGridFieldValuePerc)
            {
                if (e.Field.Area == PivotArea.RowArea)
                {
                    pivotGridControl.OptionsDataField.Area = PivotDataArea.RowArea;
                    e.Field.Area = PivotArea.DataArea;
                }
                if (e.Field.Area == PivotArea.ColumnArea)
                {
                    pivotGridControl.OptionsDataField.Area = PivotDataArea.ColumnArea;
                    e.Field.Area = PivotArea.DataArea;
                }
            }
            else if (!summarySetted && e.Field.Area == PivotArea.DataArea)
            { 
                CustomSummaryType cst = new CustomSummaryType();
                if (cst.ShowDialog() == DialogResult.OK)
                {
                    e.Field.SummaryType = cst.SelectedSummaryType;
                    summarySetted = true;
                }
            }
        }

        #region IReport Members

        public void ShowPrintPreview()
        {
            this.pivotGridControl.ShowPrintPreview();
        }

        public void ExportToXls(string fileName)
        {
            // save names
            string m1 = this.pivotGridFieldM1.Caption,
            m2 = this.pivotGridFieldM2.Caption,
            m3 = this.pivotGridFieldM3.Caption, 
            m4 = this.pivotGridFieldM4.Caption,
            region = this.pivotGridFieldRegion.Caption,
            TTId = this.pivotGridFieldTTId.Caption,
            nameTT = this.pivotGridFieldNameTT.Caption,
            response = this.pivotGridFieldResponse.Caption,
            tradingNameTT = this.pivotGridFieldTradingNameTT.Caption,
            addressTT = this.pivotGridFieldAddressTT.Caption,
            typeTT = this.pivotGridFieldAddressTT.Caption,
            baseNew = this.pivotGridFieldBaseNew.Caption,
            custName = this.pivotGridFieldCustName.Caption,
            distr = this.pivotGridFieldDistr.Caption;

            // change names
            this.pivotGridFieldM1.Caption = CommonConstants.QuestionnairiesFilling.M1FieldExportName;
            this.pivotGridFieldM2.Caption = CommonConstants.QuestionnairiesFilling.M2FieldExportName;
            this.pivotGridFieldM3.Caption = CommonConstants.QuestionnairiesFilling.M3FieldExportName;
            this.pivotGridFieldM4.Caption = CommonConstants.QuestionnairiesFilling.M4FieldExportName;
            this.pivotGridFieldRegion.Caption = CommonConstants.QuestionnairiesFilling.RegionFieldExportName;
            this.pivotGridFieldTTId.Caption = CommonConstants.QuestionnairiesFilling.PocIdFieldExportName;
            this.pivotGridFieldNameTT.Caption = CommonConstants.QuestionnairiesFilling.PocNameFieldExportName;
            this.pivotGridFieldResponse.Caption = CommonConstants.QuestionnairiesFilling.ResponseFieldExportName;
            this.pivotGridFieldTradingNameTT.Caption = CommonConstants.QuestionnairiesFilling.TradingNameFieldExportName;
            this.pivotGridFieldAddressTT.Caption = CommonConstants.QuestionnairiesFilling.AddressFieldExportName;
            this.pivotGridFieldTypeTT.Caption = CommonConstants.QuestionnairiesFilling.PocTypeFieldExportName;
            this.pivotGridFieldBaseNew.Caption = CommonConstants.QuestionnairiesFilling.BaseNewFieldExportName;
            this.pivotGridFieldCustName.Caption = CommonConstants.QuestionnairiesFilling.CustNameFieldExportName;
            this.pivotGridFieldDistr.Caption = CommonConstants.QuestionnairiesFilling.DistrFieldExportName;

            // export
            this.pivotGridControl.ExportToXls(fileName);

            // return old names
            this.pivotGridFieldM1.Caption = m1;
            this.pivotGridFieldM2.Caption = m2;
            this.pivotGridFieldM3.Caption = m3; 
            this.pivotGridFieldM4.Caption = m4;
            this.pivotGridFieldRegion.Caption = region;
            this.pivotGridFieldTTId.Caption = TTId;
            this.pivotGridFieldNameTT.Caption = nameTT;
            this.pivotGridFieldResponse.Caption = response;
            this.pivotGridFieldTradingNameTT.Caption = tradingNameTT;
            this.pivotGridFieldAddressTT.Caption = addressTT;
            this.pivotGridFieldTypeTT.Caption = typeTT;
            this.pivotGridFieldBaseNew.Caption = baseNew;
            this.pivotGridFieldCustName.Caption = custName;
            this.pivotGridFieldDistr.Caption = distr;
        }

        public void ExportToXlsx(string fileName)
        {
            // export
            // save names
            string m1 = this.pivotGridFieldM1.Caption,
            m2 = this.pivotGridFieldM2.Caption,
            m3 = this.pivotGridFieldM3.Caption,
            m4 = this.pivotGridFieldM4.Caption,
            region = this.pivotGridFieldRegion.Caption,
            TTId = this.pivotGridFieldTTId.Caption,
            nameTT = this.pivotGridFieldNameTT.Caption,
            response = this.pivotGridFieldResponse.Caption,
            tradingNameTT = this.pivotGridFieldTradingNameTT.Caption,
            addressTT = this.pivotGridFieldAddressTT.Caption,
            typeTT = this.pivotGridFieldAddressTT.Caption,
            baseNew = this.pivotGridFieldBaseNew.Caption,
            custName = this.pivotGridFieldCustName.Caption,
            distr = this.pivotGridFieldDistr.Caption;

            // change names
            this.pivotGridFieldM1.Caption = CommonConstants.QuestionnairiesFilling.M1FieldExportName;
            this.pivotGridFieldM2.Caption = CommonConstants.QuestionnairiesFilling.M2FieldExportName;
            this.pivotGridFieldM3.Caption = CommonConstants.QuestionnairiesFilling.M3FieldExportName;
            this.pivotGridFieldM4.Caption = CommonConstants.QuestionnairiesFilling.M4FieldExportName;
            this.pivotGridFieldRegion.Caption = CommonConstants.QuestionnairiesFilling.RegionFieldExportName;
            this.pivotGridFieldTTId.Caption = CommonConstants.QuestionnairiesFilling.PocIdFieldExportName;
            this.pivotGridFieldNameTT.Caption = CommonConstants.QuestionnairiesFilling.PocNameFieldExportName;
            this.pivotGridFieldResponse.Caption = CommonConstants.QuestionnairiesFilling.ResponseFieldExportName;
            this.pivotGridFieldTradingNameTT.Caption = CommonConstants.QuestionnairiesFilling.TradingNameFieldExportName;
            this.pivotGridFieldAddressTT.Caption = CommonConstants.QuestionnairiesFilling.AddressFieldExportName;
            this.pivotGridFieldTypeTT.Caption = CommonConstants.QuestionnairiesFilling.PocTypeFieldExportName;
            this.pivotGridFieldBaseNew.Caption = CommonConstants.QuestionnairiesFilling.BaseNewFieldExportName;
            this.pivotGridFieldCustName.Caption = CommonConstants.QuestionnairiesFilling.CustNameFieldExportName;
            this.pivotGridFieldDistr.Caption = CommonConstants.QuestionnairiesFilling.DistrFieldExportName;

            // export
            this.pivotGridControl.ExportToXlsx(fileName);

            // return old names
            this.pivotGridFieldM1.Caption = m1;
            this.pivotGridFieldM2.Caption = m2;
            this.pivotGridFieldM3.Caption = m3;
            this.pivotGridFieldM4.Caption = m4;
            this.pivotGridFieldRegion.Caption = region;
            this.pivotGridFieldTTId.Caption = TTId;
            this.pivotGridFieldNameTT.Caption = nameTT;
            this.pivotGridFieldResponse.Caption = response;
            this.pivotGridFieldTradingNameTT.Caption = tradingNameTT;
            this.pivotGridFieldAddressTT.Caption = addressTT;
            this.pivotGridFieldTypeTT.Caption = typeTT;
            this.pivotGridFieldBaseNew.Caption = baseNew;
            this.pivotGridFieldCustName.Caption = custName;
            this.pivotGridFieldDistr.Caption = distr;
        }

        #endregion
    }
}