﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WccpReporting.POCMappingProgress.DataProvider
{
    public static class CommonConstants
    {
        public static int ChnlKaTradeId = 3;

        public static class QuestionnairiesFilling
        {
            public static string FldValueTypeName = "ValueType";
            public static string FldStatusName = "StatusName";
            public static string FldM1IdName = "Merch_id";
            public static string ValueTypeCount = "Кол-во ТТ";
            public static string ValueTypePercent = "Кол-во ТТ%";
            public static string StatusNew = "Новые ТТ";
            public static string GrandTotalCaption = "Grand Total";
            public static string AllTTSummary = "Все ТТ";

            public static string M1FieldExportName = "Off M1, On M1";
            public static string M2FieldExportName = "Off M2, On M2";
            public static string M3FieldExportName = "Off M3, On M3";
            public static string M4FieldExportName = "Off M4, On M4";
            public static string RegionFieldExportName = "Region";
            public static string PocIdFieldExportName = "POC ID";
            public static string PocNameFieldExportName = "Region";
            public static string ResponseFieldExportName = "OffAdditionalData, OnAdditionalData";
            public static string TradingNameFieldExportName = "POC Trading Name";
            public static string AddressFieldExportName = "POC Address";
            public static string PocTypeFieldExportName = "Type";
            public static string BaseNewFieldExportName = "Status";
            public static string CustNameFieldExportName = "Cust Name";
            public static string DistrFieldExportName = "Distributor";
        }

        public static class SettingsForm 
        {
            public static string WaveIsEmpty = "{0}Волна пустая {1}";
            public static string RegionIsEmpty = "{0}Регион пустой {1}";
            public static string ChanelIsEmpty = "{0}Канал пустой {1}";
            public static string Error = "Ошибка";
        }

        public static class WccpUIControl 
        {
            public static string QuestionnairiesFilling = "Заполнение анкет";
            public static string POCMappingProgress = "POCMappingProgress_";
            public static string ExportAllTabs = "Експортировать все вкладки?";
            public static string Info = "Информация";
            public static string Things = "штук";
            public static string ChooseDirectory = "Виберите папку";
            public static string ExportError = "Ошибка експорта файла: ";
            public static string Error = "Ошибка";

            public static string ClientsCardsFilling = "Заполнение карточек клиентов";
        }
    }
}
