﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Logica.Reports.DataAccess;
using System.Data.SqlClient;

namespace WccpReporting.POCMappingProgress.DataProvider
{
    class DataAccessProvider
    {
        internal static DataTable GetQuestionnairiesFillingData(int waveId, string staff, string chanel, bool doNotAccTerrCover)
        {
            DataTable res = null;

            DataAccessLayer.OpenConnection();

            DataSet ds = DataAccessLayer.ExecuteStoredProcedure(SqlConstants.ProcQuestionnairiesFilling,
                new SqlParameter(SqlConstants.ParamWaveId, waveId),
                new SqlParameter(SqlConstants.ParamStaffM4, "(" + staff + ")"),
                new SqlParameter(SqlConstants.ParamStaffM3Channel, "(" + chanel + ")"),
                new SqlParameter(SqlConstants.ParamToAccntNotCovTerr, doNotAccTerrCover));

            if (null != ds && ds.Tables.Count > 0)
                res = ds.Tables[0];
            DataAccessLayer.CloseConnection();

            return res;
        }

        internal static DataTable GetClientsCardsFillingData(int waveId, string staff, string chanel, bool doNotAccTerrCover)
        {
            DataTable res = null;

            DataAccessLayer.OpenConnection();

            DataSet ds = DataAccessLayer.ExecuteStoredProcedure(SqlConstants.ProcClientsCardsFilling,
                new SqlParameter(SqlConstants.ParamWaveId, waveId),
                new SqlParameter(SqlConstants.ParamStaffM4, "(" + staff + ")"),
                new SqlParameter(SqlConstants.ParamStaffM3Channel, "(" + chanel + ")"),
                new SqlParameter(SqlConstants.ParamToAccntNotCovTerr, doNotAccTerrCover));

            if (null != ds && ds.Tables.Count > 0)
                res = ds.Tables[0];
            DataAccessLayer.CloseConnection();

            return res;
        }

        /// <summary>
        /// Gets active waves
        /// </summary>
        /// <returns>Table with active waves</returns>
        internal static DataTable GetWaves()
        {
            DataTable res = null;

            DataAccessLayer.OpenConnection();

            DataSet ds = DataAccessLayer.ExecuteStoredProcedure(SqlConstants.ProcGetWaves, new SqlParameter[] { });
            if (null != ds && ds.Tables.Count > 0)
                res = ds.Tables[0];

            DataAccessLayer.CloseConnection();

            return res;
        }

        /// <summary>
        /// Gets regions
        /// </summary>
        /// <returns>Table with regions</returns>
        internal static DataTable GetStaffM4()
        {
            DataTable res = null;
            DataAccessLayer.OpenConnection();
            DataSet ds = DataAccessLayer.ExecuteStoredProcedure(SqlConstants.ProcGetStaffM4, new SqlParameter[] { });
            if (null != ds && ds.Tables.Count > 0)
            {
                res = ds.Tables[0];
            }
            DataAccessLayer.CloseConnection();
            return res;
        }

        /// <summary>
        /// Gets chanels
        /// </summary>
        /// <returns>Table with chanels</returns>
        internal static DataTable GetChanels()
        {
            DataTable res = null;
            DataAccessLayer.OpenConnection();
            DataSet ds = DataAccessLayer.ExecuteStoredProcedure(SqlConstants.ProcChanelTypeList, new SqlParameter[] { });

            if (null != ds && ds.Tables.Count > 0)
                res = ds.Tables[0];
            DataAccessLayer.CloseConnection();

            return res;
            
        }
    }
}
