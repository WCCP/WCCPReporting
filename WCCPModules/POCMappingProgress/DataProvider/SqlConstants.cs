﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WccpReporting.POCMappingProgress.DataProvider
{
    public class SqlConstants
    {
        // SP names
        public const string ProcGetStaffM4 = "sp_StaffM4";
        public const string ProcChanelTypeList = "spDW_ChannelList";
        public const string ProcQuestionnairiesFilling = "DW_POC_Mapping_Questionnaire_Fill";
        public const string ProcClientsCardsFilling = "dw_poc_mapping_fields_val";
        public const string ProcGetWaves = "sp_POCMappingWave";

        // SP Params
        public const string ParamWaveId = "wave_ID";
        public const string ParamStaffM3Channel = "Channel_TypeM3";
        public const string ParamStaffM4 = "Staff_M4";
        public const string ParamToAccntNotCovTerr = "NotAccntUncovTerr";

        // tables
        public const string TblWaves = "DW_POCMapping_Wave";

        // SP fieds
        public const string FldValue = "value";
    }
}
