namespace WccpReporting.POCMappingProgress
{
    partial class SettingsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SettingsForm));
            this.lookUpWave = new DevExpress.XtraEditors.LookUpEdit();
            this.chkBoxChanel = new DevExpress.XtraEditors.CheckedComboBoxEdit();
            this.labelRegion = new DevExpress.XtraEditors.LabelControl();
            this.chkBoxStaffM4 = new DevExpress.XtraEditors.CheckedComboBoxEdit();
            this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.imCollection = new DevExpress.Utils.ImageCollection(this.components);
            this.btnYes = new DevExpress.XtraEditors.SimpleButton();
            this.lStaffChannel = new DevExpress.XtraEditors.LabelControl();
            this.lWave = new DevExpress.XtraEditors.LabelControl();
            this.lblAccntNotCovTerr = new System.Windows.Forms.Label();
            this.chkAccntNotCovTerr = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpWave.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkBoxChanel.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkBoxStaffM4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imCollection)).BeginInit();
            this.SuspendLayout();
            // 
            // lookUpWave
            // 
            this.lookUpWave.Location = new System.Drawing.Point(127, 16);
            this.lookUpWave.Name = "lookUpWave";
            this.lookUpWave.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpWave.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("WaveName", 100, "WaveName")});
            this.lookUpWave.Properties.DisplayMember = "WaveName";
            this.lookUpWave.Properties.NullText = "";
            this.lookUpWave.Properties.PopupFormMinSize = new System.Drawing.Size(20, 50);
            this.lookUpWave.Properties.ShowFooter = false;
            this.lookUpWave.Properties.ShowHeader = false;
            this.lookUpWave.Properties.ValueMember = "Wave_ID";
            this.lookUpWave.Size = new System.Drawing.Size(204, 20);
            this.lookUpWave.TabIndex = 33;
            this.lookUpWave.EditValueChanged += new System.EventHandler(this.editor_EditValueChanged);
            // 
            // chkBoxChanel
            // 
            this.chkBoxChanel.Location = new System.Drawing.Point(127, 72);
            this.chkBoxChanel.Name = "chkBoxChanel";
            this.chkBoxChanel.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.chkBoxChanel.Properties.DisplayMember = "ChanelType";
            this.chkBoxChanel.Properties.SelectAllItemCaption = "(���)";
            this.chkBoxChanel.Properties.ValueMember = "ChanelType_id";
            this.chkBoxChanel.Size = new System.Drawing.Size(204, 20);
            this.chkBoxChanel.TabIndex = 32;
            this.chkBoxChanel.EditValueChanged += new System.EventHandler(this.editor_EditValueChanged);
            // 
            // labelRegion
            // 
            this.labelRegion.Location = new System.Drawing.Point(50, 47);
            this.labelRegion.Name = "labelRegion";
            this.labelRegion.Size = new System.Drawing.Size(69, 13);
            this.labelRegion.TabIndex = 29;
            this.labelRegion.Text = "�������� �4:";
            // 
            // chkBoxStaffM4
            // 
            this.chkBoxStaffM4.Location = new System.Drawing.Point(127, 44);
            this.chkBoxStaffM4.Name = "chkBoxStaffM4";
            this.chkBoxStaffM4.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.chkBoxStaffM4.Properties.DisplayMember = "Name";
            this.chkBoxStaffM4.Properties.SelectAllItemCaption = "(���)";
            this.chkBoxStaffM4.Properties.ValueMember = "StaffId";
            this.chkBoxStaffM4.Size = new System.Drawing.Size(204, 20);
            this.chkBoxStaffM4.TabIndex = 28;
            this.chkBoxStaffM4.EditValueChanged += new System.EventHandler(this.editor_EditValueChanged);
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.ImageIndex = 1;
            this.btnCancel.ImageList = this.imCollection;
            this.btnCancel.Location = new System.Drawing.Point(259, 138);
            this.btnCancel.MaximumSize = new System.Drawing.Size(72, 23);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(72, 23);
            this.btnCancel.TabIndex = 35;
            this.btnCancel.Text = "���";
            // 
            // imCollection
            // 
            this.imCollection.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imCollection.ImageStream")));
            this.imCollection.Images.SetKeyName(0, "check_24.png");
            this.imCollection.Images.SetKeyName(1, "close_24.png");
            // 
            // btnYes
            // 
            this.btnYes.ImageIndex = 0;
            this.btnYes.ImageList = this.imCollection;
            this.btnYes.Location = new System.Drawing.Point(178, 138);
            this.btnYes.Name = "btnYes";
            this.btnYes.Size = new System.Drawing.Size(75, 23);
            this.btnYes.TabIndex = 34;
            this.btnYes.Text = "��";
            this.btnYes.Click += new System.EventHandler(this.btnYes_Click);
            // 
            // lStaffChannel
            // 
            this.lStaffChannel.Location = new System.Drawing.Point(12, 75);
            this.lStaffChannel.Name = "lStaffChannel";
            this.lStaffChannel.Size = new System.Drawing.Size(108, 13);
            this.lStaffChannel.TabIndex = 36;
            this.lStaffChannel.Text = "����� ��������� �3:";
            // 
            // lWave
            // 
            this.lWave.Location = new System.Drawing.Point(85, 19);
            this.lWave.Name = "lWave";
            this.lWave.Size = new System.Drawing.Size(34, 13);
            this.lWave.TabIndex = 37;
            this.lWave.Text = "�����:";
            // 
            // lblAccntNotCovTerr
            // 
            this.lblAccntNotCovTerr.AutoSize = true;
            this.lblAccntNotCovTerr.Location = new System.Drawing.Point(98, 105);
            this.lblAccntNotCovTerr.Name = "lblAccntNotCovTerr";
            this.lblAccntNotCovTerr.Size = new System.Drawing.Size(213, 13);
            this.lblAccntNotCovTerr.TabIndex = 38;
            this.lblAccntNotCovTerr.Text = "�� ��������� ���������� ����������:";
            // 
            // chkAccntNotCovTerr
            // 
            this.chkAccntNotCovTerr.AutoSize = true;
            this.chkAccntNotCovTerr.Location = new System.Drawing.Point(317, 105);
            this.chkAccntNotCovTerr.Name = "chkAccntNotCovTerr";
            this.chkAccntNotCovTerr.Size = new System.Drawing.Size(15, 14);
            this.chkAccntNotCovTerr.TabIndex = 39;
            this.chkAccntNotCovTerr.UseVisualStyleBackColor = true;
            // 
            // SettingsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(340, 172);
            this.Controls.Add(this.chkAccntNotCovTerr);
            this.Controls.Add(this.lblAccntNotCovTerr);
            this.Controls.Add(this.lWave);
            this.Controls.Add(this.lStaffChannel);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnYes);
            this.Controls.Add(this.lookUpWave);
            this.Controls.Add(this.chkBoxChanel);
            this.Controls.Add(this.labelRegion);
            this.Controls.Add(this.chkBoxStaffM4);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SettingsForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "���������";
            ((System.ComponentModel.ISupportInitialize)(this.lookUpWave.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkBoxChanel.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkBoxStaffM4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imCollection)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.LookUpEdit lookUpWave;
        private DevExpress.XtraEditors.CheckedComboBoxEdit chkBoxChanel;
        private DevExpress.XtraEditors.LabelControl labelRegion;
        private DevExpress.XtraEditors.CheckedComboBoxEdit chkBoxStaffM4;
        private DevExpress.XtraEditors.SimpleButton btnCancel;
        private DevExpress.XtraEditors.SimpleButton btnYes;
        private DevExpress.Utils.ImageCollection imCollection;
        private DevExpress.XtraEditors.LabelControl lStaffChannel;
        private DevExpress.XtraEditors.LabelControl lWave;
        private System.Windows.Forms.Label lblAccntNotCovTerr;
        private System.Windows.Forms.CheckBox chkAccntNotCovTerr;


    }
}