﻿using DevExpress.XtraBars;
using DevExpress.XtraTab;
using Logica.Reports.BaseReportControl;
using Logica.Reports.Common.WaitWindow;
using System;
using System.Windows.Forms;
using WccpReporting.POCMappingProgress;
using WccpReporting.POCMappingProgress.Core;
using WccpReporting.POCMappingProgress.DataProvider;
using WccpReporting.POCMappingProgress.UserControls;

namespace WccpReporting
{
    public partial class WccpUIControl : DevExpress.XtraEditors.XtraUserControl
    {
        QuestionnairiesFilling formsFillingReport = new QuestionnairiesFilling();
        ClientsCardsFilling cardsFillingReport = new ClientsCardsFilling();
        SettingsForm setForm = new SettingsForm();

        public WccpUIControl()
        {
            InitializeComponent();
        }

        public int ReportInit()
        {
            return CallSettingsForm();
        }

        private int CallSettingsForm()
        {
            if (setForm.ShowDialog() == DialogResult.OK)
            {
                ShowReports();
                return 0;
            }

            return 1;
        }

        internal virtual void ShowReports()
        {
            WaitManager.StartWait();

            while (tabManager.TabPages.Count > 0)
            {
                tabManager.TabPages.RemoveAt(0);
            }

            if (setForm.IsRu)
            {
                btnExportAllToExcelXlsx.Visibility = BarItemVisibility.Always;
                btnExportToExcelXlsx.Visibility = BarItemVisibility.Always;
            }
            else
            {
                btnExportAllToExcelXlsx.Visibility = BarItemVisibility.Never;
                btnExportToExcelXlsx.Visibility = BarItemVisibility.Never;
            }
            
            XtraTabPage page;

            if (setForm.SelectedChanel != CommonConstants.ChnlKaTradeId.ToString())
            {
                formsFillingReport.ShowReport(setForm);
                page = new XtraTabPage();
                page.Controls.Add(formsFillingReport as Control);
                page.Tag = formsFillingReport;
                page.Text = CommonConstants.WccpUIControl.QuestionnairiesFilling;
                (page as Control).Dock = DockStyle.Fill;
                (formsFillingReport as Control).Dock = DockStyle.Fill;
                tabManager.TabPages.Add(page);
            }

            cardsFillingReport.ShowReport(setForm);
            page = new XtraTabPage();
            page.Controls.Add(cardsFillingReport as Control);
            page.Tag = cardsFillingReport;
            page.Text = CommonConstants.WccpUIControl.ClientsCardsFilling;
            (page as Control).Dock = DockStyle.Fill;
            (cardsFillingReport as Control).Dock = DockStyle.Fill;
            tabManager.TabPages.Add(page);

            WaitManager.StopWait();
        }

        #region Menu Event

        private void btnSettings_ItemClick(object sender, ItemClickEventArgs e)
        {
            CallSettingsForm();
        }

        private void btnClose_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (null != tabManager.SelectedTabPage)
            {
                tabManager.TabPages.Remove(tabManager.SelectedTabPage);
            }
        }


        private void btnPrint_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (tabManager.SelectedTabPage != null)
            {
                IReport report = tabManager.SelectedTabPage.Tag as IReport;
                if (report != null)
                {
                    report.ShowPrintPreview();
                }
            }
        }

        private void btnRefresh_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (setForm.ValidateData(true))
            {
                ShowReports();
            }
        }

        #region Export engine

        private void btnExportTo_ItemClick(object sender, ItemClickEventArgs e)
        {
            Export(ExportToType.Xls);
        }

        private void Export(ExportToType type)
        {
            if (null == tabManager.SelectedTabPage)
                return;

            SaveFileDialog sfd = new SaveFileDialog();

            sfd.InitialDirectory = System.Reflection.Assembly.GetExecutingAssembly().Location;
            sfd.FileName = CommonConstants.WccpUIControl.POCMappingProgress + tabManager.SelectedTabPage.Text;
            sfd.Filter = string.Format("(*.{0})|*.{0}", type.ToString().ToLower());
            sfd.FilterIndex = 1;
            sfd.OverwritePrompt = true;
            sfd.RestoreDirectory = true;

            if (sfd.ShowDialog() != DialogResult.Cancel)
            {
                ExportEngine(tabManager.SelectedTabPage, sfd.FileName, type);
            }
        }

        private void btnExportAll_ItemClick(object sender, ItemClickEventArgs e)
        {
            ExportToType type = (ExportToType)e.Item.Tag;
            if (tabManager.TabPages.Count < 2)
            {
                Export((ExportToType)e.Item.Tag);
                return;
            }
            if (DialogResult.Yes == MessageBox.Show(CommonConstants.WccpUIControl.ExportAllTabs + " (" + tabManager.TabPages.Count + " " + CommonConstants.WccpUIControl.Things + ")", CommonConstants.WccpUIControl.Info, MessageBoxButtons.YesNo, MessageBoxIcon.Question))
            {
                FolderBrowserDialog fbd = new FolderBrowserDialog();
                fbd.Description = CommonConstants.WccpUIControl.ChooseDirectory;
                string path = System.Reflection.Assembly.GetExecutingAssembly().Location;
                path = path.Substring(0, path.LastIndexOf(@"\") + 1);
                fbd.SelectedPath = path;

                if (fbd.ShowDialog() != DialogResult.Cancel)
                {
                    foreach (XtraTabPage page in tabManager.TabPages)
                    {
                        string fName = string.Format("{0}\\" + CommonConstants.WccpUIControl.POCMappingProgress + "{1}.{2}", fbd.SelectedPath, page.Text, type.ToString().ToLower());
                        ExportEngine(page, fName, type);

                    }
                }
            }
        }

        private void ExportEngine(XtraTabPage page, string fName, ExportToType type)
        {
            IReport report = page.Tag as IReport;
            if (report != null)
            {
                try
                {
                    switch (type)
                    {
                        case ExportToType.Xls:report.ExportToXls(fName); break;
                        case ExportToType.Xlsx: report.ExportToXlsx(fName); break;
                        default: return;
                    }

                }
                catch (Exception ex)
                {
                    MessageBox.Show(CommonConstants.WccpUIControl.ExportError + fName, CommonConstants.WccpUIControl.Error);
                }
            }
        }

        private void btnExportToExcelXlsx_ItemClick(object sender, ItemClickEventArgs e)
        {
            Export(ExportToType.Xlsx);
        }

        #endregion

        #endregion
    }
}
