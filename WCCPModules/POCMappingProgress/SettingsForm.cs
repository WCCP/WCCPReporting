using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using WccpReporting.POCMappingProgress.DataProvider;
using DevExpress.XtraEditors.Controls;

namespace WccpReporting.POCMappingProgress
{
    public partial class SettingsForm : DevExpress.XtraEditors.XtraForm
    {
        const int countyear = 10;

        public int SelectedWave { get; set; }
        public string SelectedRegion { get; set; }
        public string SelectedChanel { get; set; }
        public bool IsRu { get; set; }

        public bool AccntNotCovTerr
        {
            get { return chkAccntNotCovTerr.Checked; }
        }

        public SettingsForm()
        {
            InitializeComponent();
            BuildSettings();
            this.DialogResult = DialogResult.Cancel;
        }

        private void BuildSettings()
        {
            // Select data for Waves
            DataTable tblWaves = DataAccessProvider.GetWaves();
            
            lookUpWave.Properties.DataSource = tblWaves;

            if (tblWaves != null && tblWaves.Rows.Count > 0)
            {
                lookUpWave.EditValue = tblWaves.Rows[tblWaves.Rows.Count - 1][lookUpWave.Properties.ValueMember];
            }
            
            lookUpWave.Properties.ForceInitialize();
            
            // Select data for M4
            chkBoxStaffM4.Properties.DataSource = DataAccessProvider.GetStaffM4();
            CheckedListBoxItemCollection itemsM4 = chkBoxStaffM4.Properties.GetItems();

            foreach (CheckedListBoxItem item in itemsM4)
            {
                item.CheckState = CheckState.Checked;
            }

            if (itemsM4.Count == 1)
            {
                this.chkBoxStaffM4.Properties.ReadOnly = true;
            }

            // Select data for Chanels
            chkBoxChanel.Properties.DataSource = DataAccessProvider.GetChanels();

            foreach (CheckedListBoxItem item in chkBoxChanel.Properties.GetItems())
            {
                item.CheckState = CheckState.Checked;
            }
            
            editor_EditValueChanged(lookUpWave, null);
            SetBtnEnabling();
        }   

        private void btnYes_Click(Object sender, EventArgs e)
        {
            if (ValidateData())
            {
                DialogResult = DialogResult.OK;
            }
        }

        public bool ValidateData()
        {
            return ValidateData(false);
        }

        public bool ValidateData(bool showError)
        {
            Boolean isValid = true;
            string errorText = string.Empty;

            if (this.lookUpWave.EditValue != null && this.lookUpWave.EditValue is int)
            {
                SelectedWave = (int)this.lookUpWave.EditValue;
            }
            else
            {
                errorText = string.Format(CommonConstants.SettingsForm.WaveIsEmpty, errorText, Environment.NewLine);
                isValid = false;
            }

            if (chkBoxStaffM4.EditValue != null && !string.IsNullOrEmpty(chkBoxStaffM4.EditValue.ToString()))
            {
                SelectedRegion = chkBoxStaffM4.EditValue.ToString();
            }
            else
            {
                errorText = string.Format(CommonConstants.SettingsForm.RegionIsEmpty, errorText, Environment.NewLine);
                isValid = false;
            }

            if (chkBoxChanel.EditValue != null && !string.IsNullOrEmpty(chkBoxChanel.EditValue.ToString()))
            {
                SelectedChanel = chkBoxChanel.EditValue.ToString();
            }
            else
            {
                errorText = string.Format(CommonConstants.SettingsForm.ChanelIsEmpty, errorText, Environment.NewLine);
                isValid = false;
            }

            if (showError && !string.IsNullOrEmpty(errorText))
            {
                MessageBox.Show(errorText, CommonConstants.SettingsForm.Error, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            return isValid;
        }

        private void SetBtnEnabling()
        {
            btnYes.Enabled = ValidateData();
        }

        private void ShowHideCheckBox(object sender)
        {
            if (sender.Equals(lookUpWave))
            {
                DataRowView o = (DataRowView) lookUpWave.GetSelectedDataRow();

                if (o == null)
                    return;
                int countryId = Int32.Parse(o.Row["Country_ID"].ToString());
                
                if (countryId == 2)
                {
                    chkAccntNotCovTerr.Visible = true;
                    lblAccntNotCovTerr.Visible = true;
                    IsRu = true;
                }
                else
                {
                    chkAccntNotCovTerr.Visible = false;
                    lblAccntNotCovTerr.Visible = false;
                    IsRu = false;
                }
            }
        }

        private void editor_EditValueChanged(object sender, EventArgs e)
        {
            SetBtnEnabling();
            ShowHideCheckBox(sender);
        }
    }
}