﻿using System;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace SoftServe.Reports.EquipmentCoolersAllocation.Forms
{
    public partial class SettingsForm : XtraForm
    {
        private ReportInfo _settings;

        internal ReportInfo Settings
        {
            get { return _settings; }
        }

        /// <summary>
        /// Creates the new instance od <c>SettingsForm</c>/>
        /// </summary>
        public SettingsForm()
        {
            InitializeComponent();

            SetDateIntervals();
            InitControls();

            _settings = GetSettingsData();
        }

        /// <summary>
        /// Assigns datasources to contols
        /// </summary>
        private void InitControls()
        {
            cbRegions.LoadDataSource(DataProvider.GetTerritoryTree(3, 3));
            cbRegions.SelectAll();

            cbEquipmentType.LoadDataSource(DataProvider.GetModelsList());
            cbEquipmentType.SelectAll();
        }

        /// <summary>
        /// Sets default date intervals
        /// </summary>
        private void SetDateIntervals()
        {
            DateTime lCurrentMonthBeginning = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);

            tReportInterval.To = DateTime.Now;
            tReportInterval.From = lCurrentMonthBeginning;

            tReportInterval.ActivateRestrictionsFrom();
            tReportInterval.ActivateRestrictionsTo();
            tReportInterval.ActivateRestrictions();
        }

        /// <summary>
        /// Performs gathering of settings info
        /// </summary>
        /// <returns>Current setting info</returns>
        internal ReportInfo GetSettingsData()
        {
            ReportInfo settings = new ReportInfo();

            settings.ReportDates = tReportInterval.Interval;
            settings.RegionIds = cbRegions.GetComboBoxCheckedValues();
            settings.EquipmentTypeIds = cbEquipmentType.GetComboBoxCheckedValues();

            return settings;
        }

        /// <summary>
        /// Handles Click event of generateReportButton
        /// </summary>
        /// <param name="sender">generateReportButton</param>
        /// <param name="e">EventArgs</param>
        private void generateReportButton_Click(object sender, EventArgs e)
        {
            ReportInfo lSettings = GetSettingsData();
            string message;

            if (AreSettingsValid(lSettings, out message))
            {
                DialogResult = DialogResult.OK;
                _settings = lSettings;
            }
            else
            {
                XtraMessageBox.Show(message, "Предупреждение", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        /// <summary>
        /// Validates selected data
        /// </summary>
        /// <param name="reportInfo">Settings to be checked</param>
        /// <param name="message">Warning message</param>
        /// <returns><c>true</c> if settings are valid, <c>false</c> otherwise</returns>
        private bool AreSettingsValid(ReportInfo reportInfo, out string message)
        {
            message = reportInfo.ReportDates.From.Year == reportInfo.ReportDates.To.Year
                           ? string.Empty
                           : "Дата С и дата По не должны выходить за рамки одного года";

            message += reportInfo.EquipmentTypeIds.Count > 0
                           ? string.Empty
                           : "Не выбран вид оборудования";

            return message.Length == 0;
        }

        /// <summary>
        /// Handles Closed event of cbRegions: sets bGenerateReport state 
        /// </summary>
        /// <param name="sender">cbRegions</param>
        /// <param name="e">Event args</param>
        private void cbRegions_Closed(object sender, DevExpress.XtraEditors.Controls.ClosedEventArgs e)
        {
            bGenerateReport.Enabled = cbRegions.GetComboBoxCheckedValues().Count > 0;
        }

        /// <summary>
        /// Loads settings
        /// </summary>
        /// <param name="settings">Settings to be loaded</param>
        private void LoadSettings(ReportInfo settings)
        {
            //load dates
            tReportInterval.To = settings.ReportDates.To;
            tReportInterval.From = settings.ReportDates.From;

            //load regions
            cbRegions.Properties.Items.BeginUpdate();

            for (int i = 0; i < cbRegions.Properties.Items.Count; i++)
            {
                cbRegions.Properties.Items[i].CheckState = CheckState.Unchecked;
            }

            foreach (string region in settings.RegionIds)
            {
               for(int i=0; i<cbRegions.Properties.Items.Count; i++)
               {
                   if(cbRegions.Properties.Items[i].Value.ToString() == region)
                   {
                       cbRegions.Properties.Items[i].CheckState = CheckState.Checked;
                   }
               }
            }

            cbRegions.Properties.Items.EndUpdate();

            //load model types
            cbEquipmentType.Properties.Items.BeginUpdate();

            for (int i = 0; i < cbEquipmentType.Properties.Items.Count; i++)
            {
                cbEquipmentType.Properties.Items[i].CheckState = CheckState.Unchecked;
            }

            foreach (string type in settings.EquipmentTypeIds)
            {
                for (int i = 0; i < cbEquipmentType.Properties.Items.Count; i++)
                {
                    if (cbEquipmentType.Properties.Items[i].Value.ToString() == type)
                    {
                        cbEquipmentType.Properties.Items[i].CheckState = CheckState.Checked;
                    }
                }
            }

            cbEquipmentType.Properties.Items.EndUpdate();
        }

        /// <summary>
        /// Hanles FormClosed event of SettingsForm
        /// </summary>
        private void SettingsForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            LoadSettings(_settings);
        }
    }
}
