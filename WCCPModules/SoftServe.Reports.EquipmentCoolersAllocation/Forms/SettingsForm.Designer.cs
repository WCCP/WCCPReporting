﻿namespace SoftServe.Reports.EquipmentCoolersAllocation.Forms
{
    partial class SettingsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SettingsForm));
            this.bCancel = new DevExpress.XtraEditors.SimpleButton();
            this.bGenerateReport = new DevExpress.XtraEditors.SimpleButton();
            this.lRegion = new DevExpress.XtraEditors.LabelControl();
            this.tReportInterval = new Logica.Reports.BaseReportControl.CommonControls.TimeInterval();
            this.cbRegions = new Logica.Reports.BaseReportControl.CommonControls.ComboboxExtended();
            this.cbEquipmentType = new Logica.Reports.BaseReportControl.CommonControls.ComboboxExtended();
            this.lEquipmentType = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.cbRegions.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbEquipmentType.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // bCancel
            // 
            this.bCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.bCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.bCancel.Image = ((System.Drawing.Image)(resources.GetObject("bCancel.Image")));
            this.bCancel.Location = new System.Drawing.Point(305, 133);
            this.bCancel.Name = "bCancel";
            this.bCancel.Size = new System.Drawing.Size(85, 23);
            this.bCancel.TabIndex = 3;
            this.bCancel.Text = "Отмена";
            // 
            // bGenerateReport
            // 
            this.bGenerateReport.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.bGenerateReport.Image = ((System.Drawing.Image)(resources.GetObject("bGenerateReport.Image")));
            this.bGenerateReport.Location = new System.Drawing.Point(214, 133);
            this.bGenerateReport.Name = "bGenerateReport";
            this.bGenerateReport.Size = new System.Drawing.Size(85, 23);
            this.bGenerateReport.TabIndex = 2;
            this.bGenerateReport.Text = "Запустить";
            this.bGenerateReport.Click += new System.EventHandler(this.generateReportButton_Click);
            // 
            // lRegion
            // 
            this.lRegion.Location = new System.Drawing.Point(87, 57);
            this.lRegion.Name = "lRegion";
            this.lRegion.Size = new System.Drawing.Size(39, 13);
            this.lRegion.TabIndex = 6;
            this.lRegion.Text = "Регион:";
            // 
            // tReportInterval
            // 
            this.tReportInterval.AllowNulls = DevExpress.Utils.DefaultBoolean.False;
            this.tReportInterval.BackColor = System.Drawing.Color.Transparent;
            this.tReportInterval.Description = "Период времени:";
            this.tReportInterval.From = new System.DateTime(((long)(0)));
            this.tReportInterval.Location = new System.Drawing.Point(-60, 22);
            this.tReportInterval.Name = "tReportInterval";
            this.tReportInterval.Size = new System.Drawing.Size(450, 26);
            this.tReportInterval.TabIndex = 7;
            this.tReportInterval.To = new System.DateTime(((long)(0)));
            // 
            // cbRegions
            // 
            this.cbRegions.Delimiter = ",";
            this.cbRegions.EditValue = "";
            this.cbRegions.Location = new System.Drawing.Point(150, 54);
            this.cbRegions.Name = "cbRegions";
            this.cbRegions.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbRegions.Properties.DisplayMember = "GeographyName";
            this.cbRegions.Properties.DropDownRows = 15;
            this.cbRegions.Properties.SelectAllItemCaption = "(Выбрать все)";
            this.cbRegions.Properties.ShowButtons = false;
            this.cbRegions.Properties.ValidateOnEnterKey = true;
            this.cbRegions.Properties.ValueMember = "GeographyID";
            this.cbRegions.Size = new System.Drawing.Size(237, 20);
            this.cbRegions.TabIndex = 4;
            this.cbRegions.Closed += new DevExpress.XtraEditors.Controls.ClosedEventHandler(this.cbRegions_Closed);
            // 
            // cbEquipmentType
            // 
            this.cbEquipmentType.Delimiter = ",";
            this.cbEquipmentType.EditValue = "";
            this.cbEquipmentType.Location = new System.Drawing.Point(150, 86);
            this.cbEquipmentType.Name = "cbEquipmentType";
            this.cbEquipmentType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbEquipmentType.Properties.DisplayMember = "FieldName";
            this.cbEquipmentType.Properties.DropDownRows = 15;
            this.cbEquipmentType.Properties.SelectAllItemCaption = "(Выбрать все)";
            this.cbEquipmentType.Properties.ShowButtons = false;
            this.cbEquipmentType.Properties.ValidateOnEnterKey = true;
            this.cbEquipmentType.Properties.ValueMember = "NaturalID";
            this.cbEquipmentType.Size = new System.Drawing.Size(237, 20);
            this.cbEquipmentType.TabIndex = 8;
            // 
            // lEquipmentType
            // 
            this.lEquipmentType.Location = new System.Drawing.Point(27, 89);
            this.lEquipmentType.Name = "lEquipmentType";
            this.lEquipmentType.Size = new System.Drawing.Size(99, 13);
            this.lEquipmentType.TabIndex = 9;
            this.lEquipmentType.Text = "Вид оборудования:";
            // 
            // SettingsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(399, 164);
            this.Controls.Add(this.lEquipmentType);
            this.Controls.Add(this.cbEquipmentType);
            this.Controls.Add(this.tReportInterval);
            this.Controls.Add(this.lRegion);
            this.Controls.Add(this.cbRegions);
            this.Controls.Add(this.bCancel);
            this.Controls.Add(this.bGenerateReport);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SettingsForm";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Параметры к отчету \"Coolers Allocation\"";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.SettingsForm_FormClosed);
            ((System.ComponentModel.ISupportInitialize)(this.cbRegions.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbEquipmentType.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton bCancel;
        private DevExpress.XtraEditors.SimpleButton bGenerateReport;
        private Logica.Reports.BaseReportControl.CommonControls.ComboboxExtended cbRegions;
        private DevExpress.XtraEditors.LabelControl lRegion;
        private Logica.Reports.BaseReportControl.CommonControls.TimeInterval tReportInterval;
        private Logica.Reports.BaseReportControl.CommonControls.ComboboxExtended cbEquipmentType;
        private DevExpress.XtraEditors.LabelControl lEquipmentType;
    }
}