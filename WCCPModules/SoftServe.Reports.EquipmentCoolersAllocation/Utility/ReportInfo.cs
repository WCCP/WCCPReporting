﻿using System.Collections.Generic;
using Logica.Reports.BaseReportControl.CommonFunctionality;

namespace SoftServe.Reports.EquipmentCoolersAllocation
{
    /// <summary>
    /// Describes the report settings
    /// </summary>
    public class ReportInfo
    {
        public DateInterval ReportDates { get; set; }

        public List<string> RegionIds { get; set; }

        public List<string> EquipmentTypeIds { get; set; }
    }
}
