﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Logica.Reports.BaseReportControl.CommonFunctionality;
using Logica.Reports.DataAccess;

namespace SoftServe.Reports.EquipmentCoolersAllocation
{
    internal static class DataProvider
    {
        /// <summary>
        /// Gets the territory tree with different levels quantity
        /// </summary>
        /// <param name="levelFrom">The highest level of territory division</param>
        /// <param name="levelTo">The lowest level of territory division</param>
        /// <returns>Tree table structure</returns>
        public static DataTable GetTerritoryTree(int? levelFrom, int? levelTo)
        {
            DataTable lDataTable = null;
            DataSet dataSet = levelFrom.HasValue && levelTo.HasValue ? DataAccessLayer.ExecuteStoredProcedure(Constants.SP_TERRITORY_TREE, new[] { 
                new SqlParameter(Constants.SP_TERRITORY_TREE_LevelFrom_PARAM, levelFrom.Value),
                new SqlParameter(Constants.SP_TERRITORY_TREE_LevelTo_PARAM, levelTo.Value)
                })
            : DataAccessLayer.ExecuteStoredProcedure(Constants.SP_TERRITORY_TREE);

            if (dataSet != null && dataSet.Tables.Count > 0)
            {
                lDataTable = dataSet.Tables[0];
            }

            return lDataTable;
        }

        /// <summary>
        /// Gets the models types list
        /// </summary>
        /// <returns>The models types list</returns>
        public static DataTable GetModelsList()
        {
            DataTable lDataTable = null;
            DataSet dataSet = DataAccessLayer.ExecuteStoredProcedure(Constants.SP_MODELS_LIST);

            if (dataSet != null && dataSet.Tables.Count > 0)
            {
                lDataTable = dataSet.Tables[0];

                DataView lStaffingView = lDataTable.DefaultView;
                lStaffingView.RowFilter = "Level = 0";
                lDataTable = lStaffingView.ToTable();
            }

            return lDataTable;
        }

        /// <summary>
        /// Gets the main report data.
        /// </summary>
        /// <returns>The main report data.</returns>
        public static DataTable GetMainReportData(ReportInfo settings)
        {
            DataTable lDataTable = null;

            DataSet dataSet = DataAccessLayer.ExecuteStoredProcedure(Constants.SP_GET_REPORT_DATA, new[] { 
                new SqlParameter(Constants.SP_GET_REPORT_DATA_DateStart_PARAM, settings.ReportDates.From),
                new SqlParameter(Constants.SP_GET_REPORT_DATA_DateEnd_PARAM, settings.ReportDates.To),
                new SqlParameter(Constants.SP_GET_REPORT_DATA_Region_PARAM, XmlHelper.GetXmlDescription(settings.RegionIds)),
                new SqlParameter(Constants.SP_GET_REPORT_DATA_POSGroups_PARAM, XmlHelper.GetXmlDescription(settings.EquipmentTypeIds))
                });

            if (dataSet != null)
            {
                lDataTable = dataSet.Tables[0];
            }
            
            return lDataTable;
        }
    }
}
