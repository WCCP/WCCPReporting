﻿namespace SoftServe.Reports.EquipmentCoolersAllocation.UserControls
{
    partial class MainControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gridControl = new DevExpress.XtraGrid.GridControl();
            this.gridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colTerritory = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRegion = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colM4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colM3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colKeyTown = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDistrName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWorkSchema = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTTChannel = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCustName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCustTerritory = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWarehouseName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWarehouseTerritory = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPOSGetLastPeriod = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPOSGetThisPeriod = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPOSGetAll = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPOSGetTransitWarehouse = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPOSGetMainWarehouse = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPOSNewInstalledInTT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPOSOldInstalledInTT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPOSRecoveredInstalledInTT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPOSMovementInTT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPOSReturned = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPOSReturnedByTechReason = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPOSRepaired = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPOSRepairedM1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWarehouseOrdersAmount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPOSWarehouseStock = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPOSTransitStock = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPOSNewWarehouseStock = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPOSOldWarehouseStock = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPOSRecoveredWarehouseStock = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPOSReservedKAON = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPOSReservedSunrise = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPOSForRecovering = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPOSInRecovering = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPOSNewOldInTT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPOSNewOldInTTInRepair = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPOSNewOldInTTSunrise = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPOSLostInWarehouse = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPOSLostInTT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPOSIllegal = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPOSToWipeOut = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPOSWipedOutInWarehouse = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPOSBrokenToWipeOut = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPOSNotWipedOut = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPOSReadyTotal = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView)).BeginInit();
            this.SuspendLayout();
            // 
            // gridControl
            // 
            this.gridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl.Location = new System.Drawing.Point(0, 0);
            this.gridControl.MainView = this.gridView;
            this.gridControl.Name = "gridControl";
            this.gridControl.Size = new System.Drawing.Size(900, 500);
            this.gridControl.TabIndex = 0;
            this.gridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView});
            // 
            // gridView
            // 
            this.gridView.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.gridView.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridView.Appearance.HeaderPanel.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridView.Appearance.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridView.ColumnPanelRowHeight = 60;
            this.gridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colTerritory,
            this.colRegion,
            this.colM4,
            this.colM3,
            this.colKeyTown,
            this.colDistrName,
            this.colWorkSchema,
            this.colTTChannel,
            this.colCustName,
            this.colCustTerritory,
            this.colWarehouseName,
            this.colWarehouseTerritory,
            this.colPOSGetLastPeriod,
            this.colPOSGetThisPeriod,
            this.colPOSGetAll,
            this.colPOSGetTransitWarehouse,
            this.colPOSGetMainWarehouse,
            this.colPOSNewInstalledInTT,
            this.colPOSOldInstalledInTT,
            this.colPOSRecoveredInstalledInTT,
            this.colPOSMovementInTT,
            this.colPOSReturned,
            this.colPOSReturnedByTechReason,
            this.colPOSRepaired,
            this.colPOSRepairedM1,
            this.colWarehouseOrdersAmount,
            this.colPOSWarehouseStock,
            this.colPOSTransitStock,
            this.colPOSNewWarehouseStock,
            this.colPOSOldWarehouseStock,
            this.colPOSRecoveredWarehouseStock,
            this.colPOSReservedKAON,
            this.colPOSReservedSunrise,
            this.colPOSForRecovering,
            this.colPOSInRecovering,
            this.colPOSNewOldInTT,
            this.colPOSNewOldInTTInRepair,
            this.colPOSNewOldInTTSunrise,
            this.colPOSLostInWarehouse,
            this.colPOSLostInTT,
            this.colPOSIllegal,
            this.colPOSToWipeOut,
            this.colPOSWipedOutInWarehouse,
            this.colPOSBrokenToWipeOut,
            this.colPOSNotWipedOut,
            this.colPOSReadyTotal});
            this.gridView.GridControl = this.gridControl;
            this.gridView.GroupFormat = "{0}: {1}";
            this.gridView.GroupSummary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Custom, "UpliftPct", null, "{0:N2}", "2"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "UpliftDal", null, "{0:N3}"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "UpliftMACO", null, "{0:N2}"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Custom, "AvgMACO", null, "{0:N3}", "7"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "SalesDal_Promo", null, "{0:N3}")});
            this.gridView.Name = "gridView";
            this.gridView.OptionsBehavior.Editable = false;
            this.gridView.OptionsLayout.LayoutVersion = "1";
            this.gridView.OptionsPrint.AutoWidth = false;
            this.gridView.OptionsSelection.MultiSelect = true;
            this.gridView.OptionsView.ColumnAutoWidth = false;
            this.gridView.OptionsView.ShowAutoFilterRow = true;
            this.gridView.OptionsView.ShowGroupPanel = false;
            this.gridView.OptionsView.ShowViewCaption = true;
            this.gridView.ViewCaption = "Дата с  _  - , Дата по  _ ";
            this.gridView.ShowCustomizationForm += new System.EventHandler(this.gridView_ShowCustomizationForm);
            this.gridView.BeforeLoadLayout += new DevExpress.Utils.LayoutAllowEventHandler(this.gridView_BeforeLoadLayout);
            // 
            // colTerritory
            // 
            this.colTerritory.Caption = "Территория";
            this.colTerritory.FieldName = "Territory";
            this.colTerritory.Name = "colTerritory";
            this.colTerritory.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colTerritory.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colTerritory.Visible = true;
            this.colTerritory.VisibleIndex = 0;
            this.colTerritory.Width = 100;
            // 
            // colRegion
            // 
            this.colRegion.Caption = "Регион";
            this.colRegion.FieldName = "Region";
            this.colRegion.Name = "colRegion";
            this.colRegion.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colRegion.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colRegion.Visible = true;
            this.colRegion.VisibleIndex = 1;
            this.colRegion.Width = 100;
            // 
            // colM4
            // 
            this.colM4.Caption = "M4";
            this.colM4.FieldName = "M4";
            this.colM4.Name = "colM4";
            this.colM4.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colM4.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colM4.Visible = true;
            this.colM4.VisibleIndex = 2;
            this.colM4.Width = 100;
            // 
            // colM3
            // 
            this.colM3.Caption = "M3";
            this.colM3.FieldName = "M3";
            this.colM3.Name = "colM3";
            this.colM3.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colM3.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colM3.Visible = true;
            this.colM3.VisibleIndex = 3;
            this.colM3.Width = 100;
            // 
            // colKeyTown
            // 
            this.colKeyTown.Caption = "Ключевой город (место локации M3)";
            this.colKeyTown.Name = "colKeyTown";
            this.colKeyTown.OptionsColumn.ShowInCustomizationForm = false;
            this.colKeyTown.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colKeyTown.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            // 
            // colDistrName
            // 
            this.colDistrName.Caption = "Дистрибьютор";
            this.colDistrName.FieldName = "DistrName";
            this.colDistrName.Name = "colDistrName";
            this.colDistrName.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colDistrName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colDistrName.Visible = true;
            this.colDistrName.VisibleIndex = 4;
            this.colDistrName.Width = 100;
            // 
            // colWorkSchema
            // 
            this.colWorkSchema.Caption = "Схема работы";
            this.colWorkSchema.FieldName = "WorkSchema";
            this.colWorkSchema.Name = "colWorkSchema";
            this.colWorkSchema.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colWorkSchema.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colWorkSchema.Visible = true;
            this.colWorkSchema.VisibleIndex = 5;
            // 
            // colTTChannel
            // 
            this.colTTChannel.Caption = "Канал ТТ";
            this.colTTChannel.FieldName = "TTChannel";
            this.colTTChannel.Name = "colTTChannel";
            this.colTTChannel.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colTTChannel.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colTTChannel.Visible = true;
            this.colTTChannel.VisibleIndex = 6;
            // 
            // colCustName
            // 
            this.colCustName.Caption = "Филиал дистрибьютора";
            this.colCustName.FieldName = "CustName";
            this.colCustName.Name = "colCustName";
            this.colCustName.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colCustName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colCustName.Visible = true;
            this.colCustName.VisibleIndex = 7;
            this.colCustName.Width = 150;
            // 
            // colCustTerritory
            // 
            this.colCustTerritory.Caption = "Территория филиала дистрибьютора";
            this.colCustTerritory.FieldName = "CustTerritory";
            this.colCustTerritory.Name = "colCustTerritory";
            this.colCustTerritory.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colCustTerritory.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colCustTerritory.Visible = true;
            this.colCustTerritory.VisibleIndex = 8;
            this.colCustTerritory.Width = 150;
            // 
            // colWarehouseName
            // 
            this.colWarehouseName.Caption = "Название склада ";
            this.colWarehouseName.FieldName = "WarehouseName";
            this.colWarehouseName.Name = "colWarehouseName";
            this.colWarehouseName.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colWarehouseName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colWarehouseName.Visible = true;
            this.colWarehouseName.VisibleIndex = 9;
            this.colWarehouseName.Width = 150;
            // 
            // colWarehouseTerritory
            // 
            this.colWarehouseTerritory.Caption = "Место локации";
            this.colWarehouseTerritory.FieldName = "WarehouseTerritory";
            this.colWarehouseTerritory.Name = "colWarehouseTerritory";
            this.colWarehouseTerritory.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colWarehouseTerritory.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colWarehouseTerritory.Visible = true;
            this.colWarehouseTerritory.VisibleIndex = 10;
            this.colWarehouseTerritory.Width = 150;
            // 
            // colPOSGetLastPeriod
            // 
            this.colPOSGetLastPeriod.Caption = "Количество рабочего оборудования, полученного в прошлый период (пр_c пр_по)";
            this.colPOSGetLastPeriod.DisplayFormat.FormatString = "N0";
            this.colPOSGetLastPeriod.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colPOSGetLastPeriod.FieldName = "POSGetLastPeriod";
            this.colPOSGetLastPeriod.MaxWidth = 175;
            this.colPOSGetLastPeriod.Name = "colPOSGetLastPeriod";
            this.colPOSGetLastPeriod.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colPOSGetLastPeriod.Visible = true;
            this.colPOSGetLastPeriod.VisibleIndex = 11;
            // 
            // colPOSGetThisPeriod
            // 
            this.colPOSGetThisPeriod.Caption = "Количество рабочего оборудования, полученного в текущий год, относительно отчетно" +
    "го периода (нач_года_ по_)";
            this.colPOSGetThisPeriod.DisplayFormat.FormatString = "N0";
            this.colPOSGetThisPeriod.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colPOSGetThisPeriod.FieldName = "POSGetThisPeriod";
            this.colPOSGetThisPeriod.MaxWidth = 215;
            this.colPOSGetThisPeriod.Name = "colPOSGetThisPeriod";
            this.colPOSGetThisPeriod.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colPOSGetThisPeriod.Visible = true;
            this.colPOSGetThisPeriod.VisibleIndex = 12;
            // 
            // colPOSGetAll
            // 
            this.colPOSGetAll.Caption = "Общее количество рабочего оборудования";
            this.colPOSGetAll.DisplayFormat.FormatString = "N0";
            this.colPOSGetAll.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colPOSGetAll.FieldName = "POSGetAll";
            this.colPOSGetAll.MaxWidth = 100;
            this.colPOSGetAll.Name = "colPOSGetAll";
            this.colPOSGetAll.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colPOSGetAll.Visible = true;
            this.colPOSGetAll.VisibleIndex = 13;
            // 
            // colPOSGetTransitWarehouse
            // 
            this.colPOSGetTransitWarehouse.Caption = "Получено на ТРАНЗИТНЫЙ склад (не доступно к установке) с_ по_";
            this.colPOSGetTransitWarehouse.DisplayFormat.FormatString = "N0";
            this.colPOSGetTransitWarehouse.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colPOSGetTransitWarehouse.FieldName = "POSGetTransitWarehouse";
            this.colPOSGetTransitWarehouse.MaxWidth = 175;
            this.colPOSGetTransitWarehouse.Name = "colPOSGetTransitWarehouse";
            this.colPOSGetTransitWarehouse.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colPOSGetTransitWarehouse.Visible = true;
            this.colPOSGetTransitWarehouse.VisibleIndex = 14;
            // 
            // colPOSGetMainWarehouse
            // 
            this.colPOSGetMainWarehouse.Caption = "Получено на ОСНОВНОЙ склад НОВОЕ оборудование за отчетный период (доступно к уста" +
    "новке) с_ по_";
            this.colPOSGetMainWarehouse.DisplayFormat.FormatString = "N0";
            this.colPOSGetMainWarehouse.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colPOSGetMainWarehouse.FieldName = "POSGetMainWarehouse";
            this.colPOSGetMainWarehouse.MaxWidth = 200;
            this.colPOSGetMainWarehouse.Name = "colPOSGetMainWarehouse";
            this.colPOSGetMainWarehouse.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colPOSGetMainWarehouse.Visible = true;
            this.colPOSGetMainWarehouse.VisibleIndex = 15;
            // 
            // colPOSNewInstalledInTT
            // 
            this.colPOSNewInstalledInTT.Caption = "Количество установленных холодильников в полях НОВЫХ с_ по_";
            this.colPOSNewInstalledInTT.DisplayFormat.FormatString = "N0";
            this.colPOSNewInstalledInTT.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colPOSNewInstalledInTT.FieldName = "POSNewInstalledInTT";
            this.colPOSNewInstalledInTT.MaxWidth = 175;
            this.colPOSNewInstalledInTT.Name = "colPOSNewInstalledInTT";
            this.colPOSNewInstalledInTT.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colPOSNewInstalledInTT.Visible = true;
            this.colPOSNewInstalledInTT.VisibleIndex = 16;
            // 
            // colPOSOldInstalledInTT
            // 
            this.colPOSOldInstalledInTT.Caption = "Количество установленных холодильников в полях СТАРЫХ с_ по_";
            this.colPOSOldInstalledInTT.DisplayFormat.FormatString = "N0";
            this.colPOSOldInstalledInTT.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colPOSOldInstalledInTT.FieldName = "POSOldInstalledInTT";
            this.colPOSOldInstalledInTT.MaxWidth = 175;
            this.colPOSOldInstalledInTT.Name = "colPOSOldInstalledInTT";
            this.colPOSOldInstalledInTT.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colPOSOldInstalledInTT.Visible = true;
            this.colPOSOldInstalledInTT.VisibleIndex = 17;
            // 
            // colPOSRecoveredInstalledInTT
            // 
            this.colPOSRecoveredInstalledInTT.Caption = "Количество установленных холодильников в полях новых восстановленных (за месяц) с" +
    "_ по_";
            this.colPOSRecoveredInstalledInTT.DisplayFormat.FormatString = "N0";
            this.colPOSRecoveredInstalledInTT.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colPOSRecoveredInstalledInTT.FieldName = "POSRecoveredInstalledInTT";
            this.colPOSRecoveredInstalledInTT.MaxWidth = 180;
            this.colPOSRecoveredInstalledInTT.Name = "colPOSRecoveredInstalledInTT";
            this.colPOSRecoveredInstalledInTT.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colPOSRecoveredInstalledInTT.Visible = true;
            this.colPOSRecoveredInstalledInTT.VisibleIndex = 18;
            // 
            // colPOSMovementInTT
            // 
            this.colPOSMovementInTT.Caption = "Количество перемещений в полях с_ по_";
            this.colPOSMovementInTT.DisplayFormat.FormatString = "N0";
            this.colPOSMovementInTT.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colPOSMovementInTT.FieldName = "POSMovementInTT";
            this.colPOSMovementInTT.MaxWidth = 175;
            this.colPOSMovementInTT.Name = "colPOSMovementInTT";
            this.colPOSMovementInTT.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colPOSMovementInTT.Visible = true;
            this.colPOSMovementInTT.VisibleIndex = 19;
            // 
            // colPOSReturned
            // 
            this.colPOSReturned.Caption = "Возврат рабочего оборудования с розницы с_ по_";
            this.colPOSReturned.DisplayFormat.FormatString = "N0";
            this.colPOSReturned.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colPOSReturned.FieldName = "POSReturned";
            this.colPOSReturned.MaxWidth = 175;
            this.colPOSReturned.Name = "colPOSReturned";
            this.colPOSReturned.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colPOSReturned.Visible = true;
            this.colPOSReturned.VisibleIndex = 20;
            // 
            // colPOSReturnedByTechReason
            // 
            this.colPOSReturnedByTechReason.Caption = "Возврат по техническим причинам с_ по_";
            this.colPOSReturnedByTechReason.DisplayFormat.FormatString = "N0";
            this.colPOSReturnedByTechReason.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colPOSReturnedByTechReason.FieldName = "POSReturnedByTechReason";
            this.colPOSReturnedByTechReason.MaxWidth = 175;
            this.colPOSReturnedByTechReason.Name = "colPOSReturnedByTechReason";
            this.colPOSReturnedByTechReason.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colPOSReturnedByTechReason.Visible = true;
            this.colPOSReturnedByTechReason.VisibleIndex = 21;
            // 
            // colPOSRepaired
            // 
            this.colPOSRepaired.Caption = "Количество отремонтированного оборудования по данным сервисного провайдера с_ по_" +
    "";
            this.colPOSRepaired.DisplayFormat.FormatString = "N0";
            this.colPOSRepaired.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colPOSRepaired.FieldName = "POSRepaired";
            this.colPOSRepaired.MaxWidth = 200;
            this.colPOSRepaired.Name = "colPOSRepaired";
            this.colPOSRepaired.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colPOSRepaired.Visible = true;
            this.colPOSRepaired.VisibleIndex = 22;
            // 
            // colPOSRepairedM1
            // 
            this.colPOSRepairedM1.Caption = "Количество отремонтированного оборудования по данным М1 с_ по_";
            this.colPOSRepairedM1.DisplayFormat.FormatString = "N0";
            this.colPOSRepairedM1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colPOSRepairedM1.FieldName = "POSRepairedM1";
            this.colPOSRepairedM1.MaxWidth = 175;
            this.colPOSRepairedM1.Name = "colPOSRepairedM1";
            this.colPOSRepairedM1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colPOSRepairedM1.Visible = true;
            this.colPOSRepairedM1.VisibleIndex = 23;
            // 
            // colWarehouseOrdersAmount
            // 
            this.colWarehouseOrdersAmount.Caption = "Количество договоров на_";
            this.colWarehouseOrdersAmount.DisplayFormat.FormatString = "N0";
            this.colWarehouseOrdersAmount.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colWarehouseOrdersAmount.FieldName = "WarehouseOrdersAmount";
            this.colWarehouseOrdersAmount.MaxWidth = 100;
            this.colWarehouseOrdersAmount.Name = "colWarehouseOrdersAmount";
            this.colWarehouseOrdersAmount.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colWarehouseOrdersAmount.Visible = true;
            this.colWarehouseOrdersAmount.VisibleIndex = 24;
            // 
            // colPOSWarehouseStock
            // 
            this.colPOSWarehouseStock.Caption = "Запасы оборудования в РЕМОНТЕ на_";
            this.colPOSWarehouseStock.DisplayFormat.FormatString = "N0";
            this.colPOSWarehouseStock.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colPOSWarehouseStock.FieldName = "POSWarehouseStock";
            this.colPOSWarehouseStock.MaxWidth = 100;
            this.colPOSWarehouseStock.Name = "colPOSWarehouseStock";
            this.colPOSWarehouseStock.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colPOSWarehouseStock.Visible = true;
            this.colPOSWarehouseStock.VisibleIndex = 25;
            // 
            // colPOSTransitStock
            // 
            this.colPOSTransitStock.Caption = "Остаток оборудования в ТРАНЗИТЕ на_";
            this.colPOSTransitStock.DisplayFormat.FormatString = "N0";
            this.colPOSTransitStock.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colPOSTransitStock.FieldName = "POSTransitStock";
            this.colPOSTransitStock.MaxWidth = 100;
            this.colPOSTransitStock.Name = "colPOSTransitStock";
            this.colPOSTransitStock.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colPOSTransitStock.Visible = true;
            this.colPOSTransitStock.VisibleIndex = 26;
            // 
            // colPOSNewWarehouseStock
            // 
            this.colPOSNewWarehouseStock.Caption = "Остаток НОВОГО оборудования на складе для растановки в полях на_";
            this.colPOSNewWarehouseStock.DisplayFormat.FormatString = "N0";
            this.colPOSNewWarehouseStock.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colPOSNewWarehouseStock.FieldName = "POSNewWarehouseStock";
            this.colPOSNewWarehouseStock.MaxWidth = 150;
            this.colPOSNewWarehouseStock.Name = "colPOSNewWarehouseStock";
            this.colPOSNewWarehouseStock.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colPOSNewWarehouseStock.Visible = true;
            this.colPOSNewWarehouseStock.VisibleIndex = 27;
            // 
            // colPOSOldWarehouseStock
            // 
            this.colPOSOldWarehouseStock.Caption = "Остаток СТАРОГО оборудования на складе для расстановки в полях на_";
            this.colPOSOldWarehouseStock.DisplayFormat.FormatString = "N0";
            this.colPOSOldWarehouseStock.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colPOSOldWarehouseStock.FieldName = "POSOldWarehouseStock";
            this.colPOSOldWarehouseStock.MaxWidth = 150;
            this.colPOSOldWarehouseStock.Name = "colPOSOldWarehouseStock";
            this.colPOSOldWarehouseStock.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colPOSOldWarehouseStock.Visible = true;
            this.colPOSOldWarehouseStock.VisibleIndex = 28;
            // 
            // colPOSRecoveredWarehouseStock
            // 
            this.colPOSRecoveredWarehouseStock.Caption = "Остаток восстановленного нового ХО на складе для расстановки на_";
            this.colPOSRecoveredWarehouseStock.DisplayFormat.FormatString = "N0";
            this.colPOSRecoveredWarehouseStock.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colPOSRecoveredWarehouseStock.FieldName = "POSRecoveredWarehouseStock";
            this.colPOSRecoveredWarehouseStock.MaxWidth = 158;
            this.colPOSRecoveredWarehouseStock.Name = "colPOSRecoveredWarehouseStock";
            this.colPOSRecoveredWarehouseStock.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colPOSRecoveredWarehouseStock.Visible = true;
            this.colPOSRecoveredWarehouseStock.VisibleIndex = 29;
            // 
            // colPOSReservedKAON
            // 
            this.colPOSReservedKAON.Caption = "Резерв для проектов КА и on-trade на_";
            this.colPOSReservedKAON.DisplayFormat.FormatString = "N0";
            this.colPOSReservedKAON.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colPOSReservedKAON.FieldName = "POSReservedKAON";
            this.colPOSReservedKAON.MaxWidth = 100;
            this.colPOSReservedKAON.Name = "colPOSReservedKAON";
            this.colPOSReservedKAON.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colPOSReservedKAON.Visible = true;
            this.colPOSReservedKAON.VisibleIndex = 30;
            // 
            // colPOSReservedSunrise
            // 
            this.colPOSReservedSunrise.Caption = "Количество оборудования на складе для национального проекта на_";
            this.colPOSReservedSunrise.DisplayFormat.FormatString = "N0";
            this.colPOSReservedSunrise.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colPOSReservedSunrise.FieldName = "POSReservedSunrise";
            this.colPOSReservedSunrise.MaxWidth = 150;
            this.colPOSReservedSunrise.Name = "colPOSReservedSunrise";
            this.colPOSReservedSunrise.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colPOSReservedSunrise.Visible = true;
            this.colPOSReservedSunrise.VisibleIndex = 31;
            // 
            // colPOSForRecovering
            // 
            this.colPOSForRecovering.Caption = "Оборудование для восстановления в ВЦ на_";
            this.colPOSForRecovering.DisplayFormat.FormatString = "N0";
            this.colPOSForRecovering.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colPOSForRecovering.FieldName = "POSForRecovering";
            this.colPOSForRecovering.MaxWidth = 150;
            this.colPOSForRecovering.Name = "colPOSForRecovering";
            this.colPOSForRecovering.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colPOSForRecovering.Visible = true;
            this.colPOSForRecovering.VisibleIndex = 32;
            // 
            // colPOSInRecovering
            // 
            this.colPOSInRecovering.Caption = "Оборудование, переданное на восстановление в ВЦ на_ ";
            this.colPOSInRecovering.DisplayFormat.FormatString = "N0";
            this.colPOSInRecovering.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colPOSInRecovering.FieldName = "POSInRecovering";
            this.colPOSInRecovering.MaxWidth = 150;
            this.colPOSInRecovering.Name = "colPOSInRecovering";
            this.colPOSInRecovering.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colPOSInRecovering.Visible = true;
            this.colPOSInRecovering.VisibleIndex = 33;
            // 
            // colPOSNewOldInTT
            // 
            this.colPOSNewOldInTT.Caption = "Установлено НОВОЕ + СТАРОЕ оборудование в ТТ рабочее на_";
            this.colPOSNewOldInTT.DisplayFormat.FormatString = "N0";
            this.colPOSNewOldInTT.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colPOSNewOldInTT.FieldName = "POSNewOldInTT";
            this.colPOSNewOldInTT.MaxWidth = 150;
            this.colPOSNewOldInTT.Name = "colPOSNewOldInTT";
            this.colPOSNewOldInTT.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colPOSNewOldInTT.Visible = true;
            this.colPOSNewOldInTT.VisibleIndex = 34;
            // 
            // colPOSNewOldInTTInRepair
            // 
            this.colPOSNewOldInTTInRepair.Caption = "Установлено НОВОЕ + СТАРОЕ оборудование в ТТ в ремонте на_";
            this.colPOSNewOldInTTInRepair.DisplayFormat.FormatString = "N0";
            this.colPOSNewOldInTTInRepair.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colPOSNewOldInTTInRepair.FieldName = "POSNewOldInTTInRepair";
            this.colPOSNewOldInTTInRepair.MaxWidth = 150;
            this.colPOSNewOldInTTInRepair.Name = "colPOSNewOldInTTInRepair";
            this.colPOSNewOldInTTInRepair.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colPOSNewOldInTTInRepair.Visible = true;
            this.colPOSNewOldInTTInRepair.VisibleIndex = 35;
            // 
            // colPOSNewOldInTTSunrise
            // 
            this.colPOSNewOldInTTSunrise.Caption = "Установлено НОВОЕ + СТАРОЕ оборудование в ТТ в рамках национального проекта на_";
            this.colPOSNewOldInTTSunrise.DisplayFormat.FormatString = "N0";
            this.colPOSNewOldInTTSunrise.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colPOSNewOldInTTSunrise.FieldName = "POSNewOldInTTSunrise";
            this.colPOSNewOldInTTSunrise.MaxWidth = 175;
            this.colPOSNewOldInTTSunrise.Name = "colPOSNewOldInTTSunrise";
            this.colPOSNewOldInTTSunrise.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colPOSNewOldInTTSunrise.Visible = true;
            this.colPOSNewOldInTTSunrise.VisibleIndex = 36;
            // 
            // colPOSLostInWarehouse
            // 
            this.colPOSLostInWarehouse.Caption = "Утеряно на складе на_";
            this.colPOSLostInWarehouse.DisplayFormat.FormatString = "N0";
            this.colPOSLostInWarehouse.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colPOSLostInWarehouse.FieldName = "POSLostInWarehouse";
            this.colPOSLostInWarehouse.MaxWidth = 100;
            this.colPOSLostInWarehouse.Name = "colPOSLostInWarehouse";
            this.colPOSLostInWarehouse.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colPOSLostInWarehouse.Visible = true;
            this.colPOSLostInWarehouse.VisibleIndex = 37;
            // 
            // colPOSLostInTT
            // 
            this.colPOSLostInTT.Caption = "Утеряно в полях на_";
            this.colPOSLostInTT.DisplayFormat.FormatString = "N0";
            this.colPOSLostInTT.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colPOSLostInTT.FieldName = "POSLostInTT";
            this.colPOSLostInTT.MaxWidth = 100;
            this.colPOSLostInTT.Name = "colPOSLostInTT";
            this.colPOSLostInTT.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colPOSLostInTT.Visible = true;
            this.colPOSLostInTT.VisibleIndex = 38;
            // 
            // colPOSIllegal
            // 
            this.colPOSIllegal.Caption = "Юридические проблемы на_";
            this.colPOSIllegal.DisplayFormat.FormatString = "N0";
            this.colPOSIllegal.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colPOSIllegal.FieldName = "POSIllegal";
            this.colPOSIllegal.MaxWidth = 100;
            this.colPOSIllegal.Name = "colPOSIllegal";
            this.colPOSIllegal.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colPOSIllegal.Visible = true;
            this.colPOSIllegal.VisibleIndex = 39;
            // 
            // colPOSToWipeOut
            // 
            this.colPOSToWipeOut.Caption = "Количество сломанного оборудования на списания на_";
            this.colPOSToWipeOut.DisplayFormat.FormatString = "N0";
            this.colPOSToWipeOut.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colPOSToWipeOut.FieldName = "POSToWipeOut";
            this.colPOSToWipeOut.MaxWidth = 150;
            this.colPOSToWipeOut.Name = "colPOSToWipeOut";
            this.colPOSToWipeOut.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colPOSToWipeOut.Visible = true;
            this.colPOSToWipeOut.VisibleIndex = 40;
            // 
            // colPOSWipedOutInWarehouse
            // 
            this.colPOSWipedOutInWarehouse.Caption = "Количество списанного оборудования, находящихся на складе на_";
            this.colPOSWipedOutInWarehouse.DisplayFormat.FormatString = "N0";
            this.colPOSWipedOutInWarehouse.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colPOSWipedOutInWarehouse.FieldName = "POSWipedOutInWarehouse";
            this.colPOSWipedOutInWarehouse.MaxWidth = 150;
            this.colPOSWipedOutInWarehouse.Name = "colPOSWipedOutInWarehouse";
            this.colPOSWipedOutInWarehouse.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colPOSWipedOutInWarehouse.Visible = true;
            this.colPOSWipedOutInWarehouse.VisibleIndex = 41;
            // 
            // colPOSBrokenToWipeOut
            // 
            this.colPOSBrokenToWipeOut.Caption = "Количество сломанного оборудования, подтвержденного к списанию на_";
            this.colPOSBrokenToWipeOut.DisplayFormat.FormatString = "N0";
            this.colPOSBrokenToWipeOut.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colPOSBrokenToWipeOut.FieldName = "POSBrokenToWipeOut";
            this.colPOSBrokenToWipeOut.MaxWidth = 150;
            this.colPOSBrokenToWipeOut.Name = "colPOSBrokenToWipeOut";
            this.colPOSBrokenToWipeOut.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colPOSBrokenToWipeOut.Visible = true;
            this.colPOSBrokenToWipeOut.VisibleIndex = 42;
            // 
            // colPOSNotWipedOut
            // 
            this.colPOSNotWipedOut.Caption = "CHECK TOTAL ACT на_";
            this.colPOSNotWipedOut.DisplayFormat.FormatString = "N0";
            this.colPOSNotWipedOut.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colPOSNotWipedOut.FieldName = "POSNotWipedOut";
            this.colPOSNotWipedOut.MaxWidth = 100;
            this.colPOSNotWipedOut.Name = "colPOSNotWipedOut";
            this.colPOSNotWipedOut.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colPOSNotWipedOut.Visible = true;
            this.colPOSNotWipedOut.VisibleIndex = 43;
            // 
            // colPOSReadyTotal
            // 
            this.colPOSReadyTotal.Caption = "Всего пригодного оборудования на_";
            this.colPOSReadyTotal.DisplayFormat.FormatString = "N0";
            this.colPOSReadyTotal.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colPOSReadyTotal.FieldName = "POSReadyTotal";
            this.colPOSReadyTotal.MaxWidth = 100;
            this.colPOSReadyTotal.Name = "colPOSReadyTotal";
            this.colPOSReadyTotal.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colPOSReadyTotal.Visible = true;
            this.colPOSReadyTotal.VisibleIndex = 44;
            // 
            // MainControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gridControl);
            this.Name = "MainControl";
            this.Size = new System.Drawing.Size(900, 500);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        internal DevExpress.XtraGrid.GridControl gridControl;
        internal DevExpress.XtraGrid.Views.Grid.GridView gridView;
        private DevExpress.XtraGrid.Columns.GridColumn colTerritory;
        private DevExpress.XtraGrid.Columns.GridColumn colRegion;
        private DevExpress.XtraGrid.Columns.GridColumn colM4;
        private DevExpress.XtraGrid.Columns.GridColumn colM3;
        private DevExpress.XtraGrid.Columns.GridColumn colKeyTown;
        private DevExpress.XtraGrid.Columns.GridColumn colDistrName;
        private DevExpress.XtraGrid.Columns.GridColumn colWorkSchema;
        private DevExpress.XtraGrid.Columns.GridColumn colTTChannel;
        private DevExpress.XtraGrid.Columns.GridColumn colCustName;
        private DevExpress.XtraGrid.Columns.GridColumn colCustTerritory;
        private DevExpress.XtraGrid.Columns.GridColumn colWarehouseName;
        private DevExpress.XtraGrid.Columns.GridColumn colWarehouseTerritory;
        private DevExpress.XtraGrid.Columns.GridColumn colPOSGetLastPeriod;
        private DevExpress.XtraGrid.Columns.GridColumn colPOSGetThisPeriod;
        private DevExpress.XtraGrid.Columns.GridColumn colPOSGetAll;
        private DevExpress.XtraGrid.Columns.GridColumn colPOSGetTransitWarehouse;
        private DevExpress.XtraGrid.Columns.GridColumn colPOSGetMainWarehouse;
        private DevExpress.XtraGrid.Columns.GridColumn colPOSNewInstalledInTT;
        private DevExpress.XtraGrid.Columns.GridColumn colPOSOldInstalledInTT;
        private DevExpress.XtraGrid.Columns.GridColumn colPOSRecoveredInstalledInTT;
        private DevExpress.XtraGrid.Columns.GridColumn colPOSMovementInTT;
        private DevExpress.XtraGrid.Columns.GridColumn colPOSReturned;
        private DevExpress.XtraGrid.Columns.GridColumn colPOSReturnedByTechReason;
        private DevExpress.XtraGrid.Columns.GridColumn colPOSRepaired;
        private DevExpress.XtraGrid.Columns.GridColumn colPOSRepairedM1;
        private DevExpress.XtraGrid.Columns.GridColumn colWarehouseOrdersAmount;
        private DevExpress.XtraGrid.Columns.GridColumn colPOSWarehouseStock;
        private DevExpress.XtraGrid.Columns.GridColumn colPOSTransitStock;
        private DevExpress.XtraGrid.Columns.GridColumn colPOSNewWarehouseStock;
        private DevExpress.XtraGrid.Columns.GridColumn colPOSOldWarehouseStock;
        private DevExpress.XtraGrid.Columns.GridColumn colPOSRecoveredWarehouseStock;
        private DevExpress.XtraGrid.Columns.GridColumn colPOSReservedKAON;
        private DevExpress.XtraGrid.Columns.GridColumn colPOSReservedSunrise;
        private DevExpress.XtraGrid.Columns.GridColumn colPOSForRecovering;
        private DevExpress.XtraGrid.Columns.GridColumn colPOSInRecovering;
        private DevExpress.XtraGrid.Columns.GridColumn colPOSNewOldInTT;
        private DevExpress.XtraGrid.Columns.GridColumn colPOSNewOldInTTInRepair;
        private DevExpress.XtraGrid.Columns.GridColumn colPOSNewOldInTTSunrise;
        private DevExpress.XtraGrid.Columns.GridColumn colPOSLostInWarehouse;
        private DevExpress.XtraGrid.Columns.GridColumn colPOSLostInTT;
        private DevExpress.XtraGrid.Columns.GridColumn colPOSIllegal;
        private DevExpress.XtraGrid.Columns.GridColumn colPOSToWipeOut;
        private DevExpress.XtraGrid.Columns.GridColumn colPOSWipedOutInWarehouse;
        private DevExpress.XtraGrid.Columns.GridColumn colPOSBrokenToWipeOut;
        private DevExpress.XtraGrid.Columns.GridColumn colPOSNotWipedOut;
        private DevExpress.XtraGrid.Columns.GridColumn colPOSReadyTotal;


    }
}
