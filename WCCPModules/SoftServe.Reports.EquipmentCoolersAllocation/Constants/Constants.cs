﻿namespace SoftServe.Reports.EquipmentCoolersAllocation
{
    /// <summary>
    /// Stored procedures and their params names 
    /// </summary>
    public static class Constants
    {
        #region Settings Window

        public const string SP_TERRITORY_TREE = "spDW_GetTerritoryTree";
        public const string SP_TERRITORY_TREE_LevelFrom_PARAM = "LevelFrom";
        public const string SP_TERRITORY_TREE_LevelTo_PARAM = "LevelTo";

        public const string SP_MODELS_LIST = "Pos.spDW_GetGroupsWithModels";
        
        #endregion Settings Window

        #region Main report tab

        public const string SP_GET_REPORT_DATA = "Pos.spDW_GetCoolersAllocation";

        public const string SP_GET_REPORT_DATA_DateStart_PARAM = "DateStart";
        public const string SP_GET_REPORT_DATA_DateEnd_PARAM = "DateEnd";

        public const string SP_GET_REPORT_DATA_Region_PARAM = "Region";

        public const string SP_GET_REPORT_DATA_POSGroups_PARAM = "POSGroups";
        
        #endregion Main report tab
    }
}
