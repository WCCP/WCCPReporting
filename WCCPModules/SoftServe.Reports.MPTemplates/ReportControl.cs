﻿using System;
using DevExpress.XtraEditors;
using DevExpress.XtraTab.ViewInfo;
using Logica.Reports.BaseReportControl;
using Logica.Reports.ConfigXmlParser.Model;
using DevExpress.XtraTab;
using SoftServe.Reports.MPTemplates;
using SoftServe.Reports.ContractMgmt;
using System.Windows.Forms;

namespace WccpReporting
{
    /// <summary>
    /// 
    /// </summary>
    public partial class WccpUIControl : BaseReportUserControl
    {
        private TemplateListTab tabTemplatesList;

        /// <summary>
        /// Initializes a new instance of the <see cref="WccpUIControl"/> class.
        /// </summary>
        public WccpUIControl(int reportId)
            : base(reportId)
        {
            Application.ThreadException += new System.Threading.ThreadExceptionEventHandler(Application_ThreadException);
            TabControl.CloseButtonClick += TabControl_CloseButtonClick;

            InitializeComponent();

            tabTemplatesList = new TemplateListTab(this);

            TabControl.ClosePageButtonShowMode = ClosePageButtonShowMode.InAllTabPageHeaders;

            UpdateAllSheets();
        }

        void TabControl_CloseButtonClick(object sender, EventArgs e)
        {
            ClosePageButtonEventArgs arg = e as ClosePageButtonEventArgs;

            bool isClose = true;

            if (arg == null || !(arg.Page is TemplateDetailsTab)) return;

            TemplateDetailsControl content = ((TemplateDetailsTab)arg.Page).Content;
            if (content != null && content.IsModified)
            {
                DialogResult result = XtraMessageBox.Show(Resource.SaveUnsavedData, Resource.Warning, MessageBoxButtons.YesNoCancel);
                if (result == DialogResult.Yes)
                    isClose = content.SaveData();
                if (result == DialogResult.Cancel)
                    isClose = false;
            }
            if (isClose)
            {
                TabControl.TabPages.Remove((XtraTabPage) arg.Page);
            }
        }

        /// <summary>
        /// Updates all sheets.
        /// </summary>
        private void UpdateAllSheets()
        {
            SheetParamCollection parameterCollection = new SheetParamCollection { TabId = tabTemplatesList.TabId, TableDataType = TableType.Fact };
            tabTemplatesList.UpdateSheet(parameterCollection);
        }

        /// <summary>
        /// Handles the ThreadException event of the Application control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Threading.ThreadExceptionEventArgs"/> instance containing the event data.</param>
        private void Application_ThreadException(object sender, System.Threading.ThreadExceptionEventArgs e)
        {
            ExceptionHandler.HandleException(e.Exception);
        }
    }
}