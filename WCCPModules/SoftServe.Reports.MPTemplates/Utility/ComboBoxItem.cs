﻿namespace SoftServe.Reports.MPTemplates.Utility
{
    /// <summary>
    /// 
    /// </summary>
    public class ComboBoxItem
    {
        public ComboBoxItem(int id, string text)
        {
            Id = id;
            Text = text;
        }

        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        /// <value>The id.</value>
        public int Id
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the text.
        /// </summary>
        /// <value>The text.</value>
        public string Text
        {
            get;
            set;
        }

        /// <summary>
        /// Returns a <see cref="System.String"/> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String"/> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            return Text;
        }
    }
}
