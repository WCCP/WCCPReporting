﻿namespace SoftServe.Reports.MPTemplates
{
    partial class TemplateListControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TemplateListControl));
            this.gridControl = new DevExpress.XtraGrid.GridControl();
            this.gridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.columnId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnTemplateType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnTemplateName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnCreatorName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnChanelName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnLevelName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnIsApproved = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnCurrentCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnActiveCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.barManager = new DevExpress.XtraBars.BarManager();
            this.standaloneTool = new DevExpress.XtraBars.Bar();
            this.barButtonNew = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonEdit = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.imCollection = new DevExpress.Utils.ImageCollection();
            this.barEditDateFrom = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemDateFrom = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.barEditDateTo = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemDateTo = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemTextEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemColorEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemColorEdit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imCollection)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateFrom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateFrom.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateTo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateTo.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemColorEdit1)).BeginInit();
            this.SuspendLayout();
            // 
            // gridControl
            // 
            this.gridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl.Location = new System.Drawing.Point(0, 34);
            this.gridControl.MainView = this.gridView;
            this.gridControl.Name = "gridControl";
            this.gridControl.Size = new System.Drawing.Size(800, 313);
            this.gridControl.TabIndex = 1;
            this.gridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView});
            // 
            // gridView
            // 
            this.gridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.columnId,
            this.columnTemplateType,
            this.columnTemplateName,
            this.columnCreatorName,
            this.columnChanelName,
            this.columnLevelName,
            this.columnIsApproved,
            this.columnCurrentCount,
            this.columnActiveCount});
            this.gridView.GridControl = this.gridControl;
            this.gridView.Name = "gridView";
            this.gridView.OptionsFilter.MaxCheckedListItemCount = 10000;
            this.gridView.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView.OptionsView.ShowAutoFilterRow = true;
            this.gridView.OptionsView.ShowGroupPanel = false;
            this.gridView.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.columnTemplateType, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridView_FocusedRowChanged);
            this.gridView.DoubleClick += new System.EventHandler(this.gridView_DoubleClick);
            // 
            // columnId
            // 
            this.columnId.AppearanceHeader.Options.UseTextOptions = true;
            this.columnId.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnId.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.columnId.Caption = "Id";
            this.columnId.FieldName = "Id";
            this.columnId.Name = "columnId";
            this.columnId.OptionsColumn.AllowEdit = false;
            this.columnId.OptionsColumn.AllowShowHide = false;
            this.columnId.OptionsColumn.ReadOnly = true;
            // 
            // columnTemplateType
            // 
            this.columnTemplateType.AppearanceHeader.Options.UseTextOptions = true;
            this.columnTemplateType.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnTemplateType.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.columnTemplateType.Caption = "Тип шаблона";
            this.columnTemplateType.FieldName = "TemplateType";
            this.columnTemplateType.Name = "columnTemplateType";
            this.columnTemplateType.OptionsColumn.AllowEdit = false;
            this.columnTemplateType.OptionsColumn.ReadOnly = true;
            this.columnTemplateType.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.columnTemplateType.Visible = true;
            this.columnTemplateType.VisibleIndex = 0;
            // 
            // columnTemplateName
            // 
            this.columnTemplateName.AppearanceHeader.Options.UseTextOptions = true;
            this.columnTemplateName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnTemplateName.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.columnTemplateName.Caption = "Имя шаблона";
            this.columnTemplateName.FieldName = "TemplateName";
            this.columnTemplateName.Name = "columnTemplateName";
            this.columnTemplateName.OptionsColumn.AllowEdit = false;
            this.columnTemplateName.OptionsColumn.ReadOnly = true;
            this.columnTemplateName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.columnTemplateName.Visible = true;
            this.columnTemplateName.VisibleIndex = 1;
            // 
            // columnCreatorName
            // 
            this.columnCreatorName.AppearanceHeader.Options.UseTextOptions = true;
            this.columnCreatorName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnCreatorName.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.columnCreatorName.Caption = "Создатель";
            this.columnCreatorName.FieldName = "CreatorName";
            this.columnCreatorName.Name = "columnCreatorName";
            this.columnCreatorName.OptionsColumn.AllowEdit = false;
            this.columnCreatorName.OptionsColumn.ReadOnly = true;
            this.columnCreatorName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.columnCreatorName.Visible = true;
            this.columnCreatorName.VisibleIndex = 7;
            // 
            // columnChanelName
            // 
            this.columnChanelName.AppearanceHeader.Options.UseTextOptions = true;
            this.columnChanelName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnChanelName.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.columnChanelName.Caption = "Канал";
            this.columnChanelName.FieldName = "ChanelName";
            this.columnChanelName.Name = "columnChanelName";
            this.columnChanelName.OptionsColumn.AllowEdit = false;
            this.columnChanelName.OptionsColumn.ReadOnly = true;
            this.columnChanelName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.columnChanelName.Visible = true;
            this.columnChanelName.VisibleIndex = 2;
            // 
            // columnLevelName
            // 
            this.columnLevelName.AppearanceHeader.Options.UseTextOptions = true;
            this.columnLevelName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnLevelName.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.columnLevelName.Caption = "Уровень";
            this.columnLevelName.FieldName = "LevelName";
            this.columnLevelName.Name = "columnLevelName";
            this.columnLevelName.OptionsColumn.AllowEdit = false;
            this.columnLevelName.OptionsColumn.ReadOnly = true;
            this.columnLevelName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.columnLevelName.Visible = true;
            this.columnLevelName.VisibleIndex = 3;
            // 
            // columnIsApproved
            // 
            this.columnIsApproved.AppearanceHeader.Options.UseTextOptions = true;
            this.columnIsApproved.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnIsApproved.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.columnIsApproved.Caption = "Утвержден";
            this.columnIsApproved.FieldName = "IsApproved";
            this.columnIsApproved.Name = "columnIsApproved";
            this.columnIsApproved.OptionsColumn.AllowEdit = false;
            this.columnIsApproved.OptionsColumn.ReadOnly = true;
            this.columnIsApproved.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.columnIsApproved.Visible = true;
            this.columnIsApproved.VisibleIndex = 4;
            // 
            // columnCurrentCount
            // 
            this.columnCurrentCount.AppearanceHeader.Options.UseTextOptions = true;
            this.columnCurrentCount.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnCurrentCount.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.columnCurrentCount.Caption = "Текущих";
            this.columnCurrentCount.FieldName = "CurrentCount";
            this.columnCurrentCount.Name = "columnCurrentCount";
            this.columnCurrentCount.OptionsColumn.AllowEdit = false;
            this.columnCurrentCount.OptionsColumn.ReadOnly = true;
            this.columnCurrentCount.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.columnCurrentCount.Visible = true;
            this.columnCurrentCount.VisibleIndex = 5;
            // 
            // columnActiveCount
            // 
            this.columnActiveCount.AppearanceHeader.Options.UseTextOptions = true;
            this.columnActiveCount.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnActiveCount.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.columnActiveCount.Caption = "Всего активностей";
            this.columnActiveCount.FieldName = "ActiveCount";
            this.columnActiveCount.Name = "columnActiveCount";
            this.columnActiveCount.OptionsColumn.AllowEdit = false;
            this.columnActiveCount.OptionsColumn.ReadOnly = true;
            this.columnActiveCount.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.columnActiveCount.Visible = true;
            this.columnActiveCount.VisibleIndex = 6;
            // 
            // barManager
            // 
            this.barManager.AllowCustomization = false;
            this.barManager.AllowMoveBarOnToolbar = false;
            this.barManager.AllowQuickCustomization = false;
            this.barManager.AllowShowToolbarsPopup = false;
            this.barManager.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.standaloneTool});
            this.barManager.DockControls.Add(this.barDockControlTop);
            this.barManager.DockControls.Add(this.barDockControlBottom);
            this.barManager.DockControls.Add(this.barDockControlLeft);
            this.barManager.DockControls.Add(this.barDockControlRight);
            this.barManager.Form = this;
            this.barManager.Images = this.imCollection;
            this.barManager.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barButtonNew,
            this.barButtonEdit,
            this.barEditDateFrom,
            this.barEditDateTo});
            this.barManager.MaxItemId = 19;
            this.barManager.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemDateFrom,
            this.repositoryItemTextEdit1,
            this.repositoryItemTextEdit2,
            this.repositoryItemDateTo,
            this.repositoryItemColorEdit1});
            // 
            // standaloneTool
            // 
            this.standaloneTool.BarName = "Tools";
            this.standaloneTool.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Top;
            this.standaloneTool.DockCol = 0;
            this.standaloneTool.DockRow = 0;
            this.standaloneTool.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.standaloneTool.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.barButtonNew, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.barButtonEdit, "", true, true, true, 0, null, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph)});
            this.standaloneTool.OptionsBar.AllowQuickCustomization = false;
            this.standaloneTool.OptionsBar.DisableCustomization = true;
            this.standaloneTool.OptionsBar.DrawDragBorder = false;
            this.standaloneTool.OptionsBar.UseWholeRow = true;
            this.standaloneTool.Text = "Tools";
            // 
            // barButtonNew
            // 
            this.barButtonNew.Caption = "Новый";
            this.barButtonNew.Id = 5;
            this.barButtonNew.ImageIndex = 0;
            this.barButtonNew.Name = "barButtonNew";
            this.barButtonNew.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonNew_ItemClick);
            // 
            // barButtonEdit
            // 
            this.barButtonEdit.Caption = "Редактировать";
            this.barButtonEdit.Id = 10;
            this.barButtonEdit.ImageIndex = 1;
            this.barButtonEdit.Name = "barButtonEdit";
            this.barButtonEdit.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonEdit_ItemClick);
            // 
            // imCollection
            // 
            this.imCollection.ImageSize = new System.Drawing.Size(24, 24);
            this.imCollection.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imCollection.ImageStream")));
            this.imCollection.Images.SetKeyName(0, "add.png");
            this.imCollection.Images.SetKeyName(1, "edit.png");
            this.imCollection.Images.SetKeyName(2, "delete.png");
            this.imCollection.Images.SetKeyName(3, "templates_highlight.PNG");
            // 
            // barEditDateFrom
            // 
            this.barEditDateFrom.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.barEditDateFrom.Caption = "Клиент с истекающим контраком в период с:";
            this.barEditDateFrom.Edit = this.repositoryItemDateFrom;
            this.barEditDateFrom.Id = 11;
            this.barEditDateFrom.ImageIndex = 3;
            this.barEditDateFrom.Name = "barEditDateFrom";
            this.barEditDateFrom.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.barEditDateFrom.Width = 100;
            // 
            // repositoryItemDateFrom
            // 
            this.repositoryItemDateFrom.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.repositoryItemDateFrom.AutoHeight = false;
            this.repositoryItemDateFrom.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateFrom.Name = "repositoryItemDateFrom";
            this.repositoryItemDateFrom.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            // 
            // barEditDateTo
            // 
            this.barEditDateTo.Caption = "по:";
            this.barEditDateTo.Edit = this.repositoryItemDateTo;
            this.barEditDateTo.Id = 14;
            this.barEditDateTo.Name = "barEditDateTo";
            this.barEditDateTo.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.barEditDateTo.Width = 100;
            // 
            // repositoryItemDateTo
            // 
            this.repositoryItemDateTo.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.repositoryItemDateTo.AutoHeight = false;
            this.repositoryItemDateTo.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateTo.Name = "repositoryItemDateTo";
            this.repositoryItemDateTo.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // repositoryItemTextEdit2
            // 
            this.repositoryItemTextEdit2.AutoHeight = false;
            this.repositoryItemTextEdit2.Name = "repositoryItemTextEdit2";
            // 
            // repositoryItemColorEdit1
            // 
            this.repositoryItemColorEdit1.AutoHeight = false;
            this.repositoryItemColorEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemColorEdit1.Name = "repositoryItemColorEdit1";
            // 
            // TemplateListControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.Controls.Add(this.gridControl);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Name = "TemplateListControl";
            this.Size = new System.Drawing.Size(800, 347);
            this.Load += new System.EventHandler(this.TemplateListControl_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imCollection)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateFrom.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateFrom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateTo.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateTo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemColorEdit1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView;
        private DevExpress.XtraGrid.Columns.GridColumn columnId;
        private DevExpress.XtraBars.BarManager barManager;
        private DevExpress.XtraBars.Bar standaloneTool;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarButtonItem barButtonNew;
        private DevExpress.Utils.ImageCollection imCollection;
        private DevExpress.XtraBars.BarButtonItem barButtonEdit;
        private DevExpress.XtraBars.BarEditItem barEditDateFrom;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateFrom;
        private DevExpress.XtraBars.BarEditItem barEditDateTo;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateTo;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemColorEdit repositoryItemColorEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn columnTemplateType;
        private DevExpress.XtraGrid.Columns.GridColumn columnTemplateName;
        private DevExpress.XtraGrid.Columns.GridColumn columnCreatorName;
        private DevExpress.XtraGrid.Columns.GridColumn columnChanelName;
        private DevExpress.XtraGrid.Columns.GridColumn columnLevelName;
        private DevExpress.XtraGrid.Columns.GridColumn columnIsApproved;
        private DevExpress.XtraGrid.Columns.GridColumn columnCurrentCount;
        private DevExpress.XtraGrid.Columns.GridColumn columnActiveCount;


    }
}
