﻿using System.Collections.ObjectModel;
using System.Data;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using DevExpress.XtraTab;
using Logica.Reports.BaseReportControl;
using Logica.Reports.BaseReportControl.CommonControls;
using Logica.Reports.BaseReportControl.Utility;
using Logica.Reports.Common;
using Logica.Reports.Common.WaitWindow;

namespace SoftServe.Reports.MPTemplates
{
    /// <summary>
    /// </summary>
    public partial class TemplateDetailsControl : CommonBaseControl
    {
        #region Fields

        internal TemplateDetailsCommon controlCommon;
        internal TemplateDetailsGoals controlGoals;
        private DataTable dtDetails;
        private TemplateDetailsDTO originalTemplateData;

        #endregion

        #region Constructors

        /// <summary>
        ///   Initializes a new instance of the <see cref = "TemplateDetailsControl" /> class.
        /// </summary>
        public TemplateDetailsControl()
        {
            InitializeComponent();

            controlCommon = new TemplateDetailsCommon() {MainControl = this};
            controlGoals = new TemplateDetailsGoals() {MainControl = this};
        }

        /// <summary>
        ///   Initializes a new instance of the <see cref = "TemplateDetailsControl" /> class.
        /// </summary>
        public TemplateDetailsControl(CommonBaseTab parentTab, int templateId)
            : base(parentTab)
        {
            InitializeComponent();

            TemplateId = templateId;

            controlCommon = new TemplateDetailsCommon { MainControl = this };
            controlGoals = new TemplateDetailsGoals { MainControl = this };

            InitTabControl();

        }

        #endregion

        #region Instance Properties

        public bool IsModified
        {
            get
            {
                TemplateDetailsDTO currentTemplateData = GetTemplateData();
                bool goalTabModified = controlGoals.WasGoalTabModified;
                return !originalTemplateData.Equals(currentTemplateData) || goalTabModified;
            }
        }

        /// <summary>
        ///   Gets or sets the template details.
        /// </summary>
        /// <value>The template details.</value>
        public DataTable TemplateDetails
        {
            get
            {
                if (dtDetails == null)
                {
                    dtDetails = DataProvider.GetTemaplateDetails(TemplateId);
                }

                return dtDetails;
            }
            set { dtDetails = value; }
        }

        /// <summary>
        ///   Gets a value indicating whether this instance is level valid.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance is level valid; otherwise, <c>false</c>.
        /// </value>
        private bool IsLevelValid
        {
            get
            {
                //BUG #17796. МПШ: Закладка "Общие настройки": зберігається шаблон з Уровень = М3 і розбивка по території - Область 
                //BUG #25599. МПШ: неправильна робота при виборі значень параметрів "Уровень" і ""
                bool isCommonLevelInvalid = controlCommon.SelectedLevel == LevelType.M3 &&
                                            (controlGoals.SelectedSplitByPOCType == SplitPOCByType.ByRegion || controlGoals.SelectedSplitByPOCType == SplitPOCByType.ByDistrict);
                return !isCommonLevelInvalid;
            }
        }

        #endregion

        #region Instance Methods

        /// <summary>
        ///   Loads the data.
        /// </summary>
        public void LoadData()
        {
            LoadAccessLevel();

            TemplateDetails = null;

            UpdateAdditionalTemplateInfo();

            LoadTabs();

            UpdateBarButtons();
        }

        /// <summary>
        ///   Saves the template.
        /// </summary>
        internal override bool SaveData()
        {
            if (!IsLevelValid)
            {
                XtraMessageBox.Show(Resource.ErrorM3AndRegion,
                                    Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            // Validate all controls
            ValidateMetadata commonValidateResult = controlCommon.ValidateData(); //Gett validate metadata.
            ValidateMetadata goalsValidateResult = controlGoals.ValidateData();
            if (commonValidateResult.IsCorrect && goalsValidateResult.IsCorrect) //Check whether all bookmarks are valid.
            {
                // Collect all fields info
                TemplateDetailsDTO templateDetails = GetTemplateData();

                // Save template's common info
                WaitManager.StartWait();
                
                int retCode = DataProvider.SaveTemplate(templateDetails, IsEditMode);

                

                if (retCode > 0)
                {
                    TemplateId = retCode;

                    // Save documents
                    SaveDocuments();

                    //save aims
                    controlGoals.SaveAllAims(TemplateId);

                    UpdateTabName(templateDetails.FIELD_ACTION_NAME);

                    // Refresh list tab
                    if (ParentTab != null)
                    {
                        ParentTab.UpdateTemplateListTab();
                    }

                    // Update all details
                    LoadData();

                    WaitManager.StopWait();

                    XtraMessageBox.Show(Resource.SavedSuccessfull, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);

                    return true;
                }
                else if (retCode == Constants.ErrorTemplateWithThisNameExists)
                {
                    XtraMessageBox.Show(Resource.ErrorTemplateWithThisNameExists,
                                        Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    XtraMessageBox.Show(string.Format(Resource.SaveError, retCode),
                                        Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                StringBuilder messageBuilder = new StringBuilder(Resource.SaveInvalidData);
                    //Bookmarks were not valid, so try to compose message to user with usage of validate metadata.
                int invalidBookmarkCount = 0;

                if (!commonValidateResult.IsCorrect && commonValidateResult.Message != null)
                {
                    invalidBookmarkCount++;
                    messageBuilder.AppendFormat("\n\t{0}.)", invalidBookmarkCount);
                    messageBuilder.Append(commonValidateResult.Message);
                }
                if (!goalsValidateResult.IsCorrect && goalsValidateResult.Message != null)
                {
                    invalidBookmarkCount++;
                    messageBuilder.AppendFormat("\n\t{0}.) ", invalidBookmarkCount);
                    messageBuilder.AppendFormat("{0}:", Resource.TabGoals);
                    messageBuilder.Append("\n");
                    messageBuilder.Append(goalsValidateResult.Message);
                }
                XtraMessageBox.Show(messageBuilder.ToString(), Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Stop);
                // XtraMessageBox.Show(Resource.SaveInvalidData, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }

            return false;
        }

        /// <summary>
        ///   Adds the tab.
        /// </summary>
        /// <param name = "content">The content.</param>
        /// <param name = "tabName">Name of the tab.</param>
        private void AddTab(CommonBaseControl content, string tabName)
        {
            if (content != null && ParentTab != null)
            {
                CommonBaseTab newCommonBaseTab = new CommonBaseTab();
                newCommonBaseTab.Init(ParentTab.Bruc, content, tabName);
                tabControl.TabPages.Add(newCommonBaseTab);
                content.AutoScroll = true;
                content.MainData = MainData;
            }
        }

        /// <summary>
        ///   Deletes the template.
        /// </summary>
        private void DeleteTemplate()
        {
            if (TemplateId > 0 && ParentTab != null)
            {
                if (XtraMessageBox.Show(
                    string.Format(Resource.ConfirmTemplateDelete, ParentTab.Text),
                    Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    int result = DataProvider.DeleteTemplateDetails(TemplateId);
                    if (result > 0)
                    {
                        ParentTab.CloseTab();
                        ParentTab.UpdateTemplateListTab();
                    }
                    else if (result == Constants.ErrorActivityOnTemplateExists)
                    {
                        ErrorManager.ShowErrorBox(Resource.ActivityOnTemplateCreated);
                    }
                    else
                    {
                        ErrorManager.ShowErrorBox(Resource.ErrorTemplateDeleting);
                    }
                }
            }
        }

        /// <summary>
        ///   Enables the sub tab.
        /// </summary>
        /// <param name = "type">The type.</param>
        /// <param name = "enable">if set to <c>true</c> [enable].</param>
        private void EnableSubTab(TemplateDetailsTabType type, bool enable)
        {
            GetSubTab(type).PageEnabled = enable;
        }

        /// <summary>
        ///   Gets the sub tab.
        /// </summary>
        /// <param name = "type">The type.</param>
        /// <returns></returns>
        private CommonBaseTab GetSubTab(TemplateDetailsTabType type)
        {
            return tabControl.TabPages[(int) type] as CommonBaseTab;
        }

        private TemplateDetailsDTO GetTemplateData()
        {
            TemplateDetailsDTO templateDetails = new TemplateDetailsDTO();
            controlCommon.GetAllFields(templateDetails, TemplateId);
            controlGoals.GetAllFields(templateDetails, TemplateId);
            return templateDetails;
        }

        /// <summary>
        ///   Inits the tab control.
        /// </summary>
        private void InitTabControl()
        {
            AddTab(controlCommon, Resource.TabInfo);
            AddTab(controlGoals, Resource.TabGoals);
        }

        /// <summary>
        ///   Loads the access level.
        /// </summary>
        private void LoadAccessLevel()
        {
            MainData.AccessLevel = DataProvider.AccessLevel;
            MainData.UserLevel = DataProvider.UserLevel;
        }

        /// <summary>
        ///   Loads the sub tabs.
        /// </summary>
        private void LoadTabs()
        {
            controlGoals.LoadData(TemplateId, TemplateDetails);
            controlCommon.LoadData(TemplateId, TemplateDetails);
            originalTemplateData = GetTemplateData();
        }

        /// <summary>
        ///   Saves the documents.
        /// </summary>
        private void SaveDocuments()
        {
            // Save newly added documents
            Collection<FileUploader.FileDataInfo> documentsAdded = controlCommon.GetDocumentChanges(DataRowState.Added);
            foreach (FileUploader.FileDataInfo doc in documentsAdded)
            {
                if (!string.IsNullOrEmpty(doc.FilePath) && doc.FileData != null && doc.FileData.Length > 0)
                {
                    WaitManager.StartWait();
                    DataProvider.InsertDocument(TemplateId, doc.FilePath, doc.FileData);
                    WaitManager.StopWait();
                }
            }

            // Deleted documents
            Collection<FileUploader.FileDataInfo> documentsDeleted = controlCommon.GetDocumentChanges(DataRowState.Deleted);
            foreach (FileUploader.FileDataInfo doc in documentsDeleted)
            {
                if (doc.Id > 0)
                {
                    WaitManager.StartWait();
                    DataProvider.DeleteDocument(doc.Id);
                    WaitManager.StopWait();
                }
            }
        }

        /// <summary>
        ///   Updates additional template information like activity type etc.
        /// </summary>
        private void UpdateAdditionalTemplateInfo()
        {
            DataRow settingsRow = null;
            if (TemplateDetails.Rows.Count > 0)
                settingsRow = TemplateDetails.Rows[0];

            if (settingsRow != null)
                MainData.ActivityTemplate = (TemplateType) ConvertEx.ToInt(TemplateDetails.Rows[0][Constants.FIELD_ACTION_TYPE]);
        }

        /// <summary>
        ///   Updates the bar menu.
        /// </summary>
        private void UpdateBarButtons()
        {
            TemplateDetailsTabType tabType = (TemplateDetailsTabType) tabControl.SelectedTabPageIndex;
            barButtonRemove.Enabled = IsEditMode && MainData.AccessLevel >= (int)AccessLevelType.ReadWrite;
            barButtonSave.Enabled = MainData.AccessLevel >= (int)AccessLevelType.ReadWrite;
        }

        /// <summary>
        ///   Updates the name of the tab.
        /// </summary>
        private void UpdateTabName(string tabName)
        {
            if (ParentTab != null)
            {
                ParentTab.Text = tabName;
            }
        }

        #endregion

        #region Event Handling

        /// <summary>
        ///   Handles the ItemClick event of the barButtonRemove control.
        /// </summary>
        /// <param name = "sender">The source of the event.</param>
        /// <param name = "e">The <see cref = "DevExpress.XtraBars.ItemClickEventArgs" /> instance containing the event data.</param>
        private void barButtonRemove_ItemClick(object sender, ItemClickEventArgs e)
        {
            DeleteTemplate();
        }

        /// <summary>
        ///   Handles the ItemClick event of the barButtonSave control.
        /// </summary>
        /// <param name = "sender">The source of the event.</param>
        /// <param name = "e">The <see cref = "DevExpress.XtraBars.ItemClickEventArgs" /> instance containing the event data.</param>
        private void barButtonSave_ItemClick(object sender, ItemClickEventArgs e)
        {
            SaveData();
        }

        /// <summary>
        ///   Handles the SelectedPageChanged event of the tabControl control.
        /// </summary>
        /// <param name = "sender">The source of the event.</param>
        /// <param name = "e">The <see cref = "DevExpress.XtraTab.TabPageChangedEventArgs" /> instance containing the event data.</param>
        private void tabControl_SelectedPageChanged(object sender, TabPageChangedEventArgs e)
        {
            UpdateBarButtons();
        }

        #endregion
    }
}