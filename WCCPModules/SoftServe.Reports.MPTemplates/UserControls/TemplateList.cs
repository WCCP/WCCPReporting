﻿using System;
using System.Windows.Forms;
using SoftServe.Reports.MPTemplates;
using DevExpress.XtraGrid;
using System.Data;
using System.Globalization;
using DevExpress.XtraEditors;
using Logica.Reports.Common;
using DevExpress.XtraGrid.Views.Grid;
using System.Drawing;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using Logica.Reports.BaseReportControl.Utility;

namespace SoftServe.Reports.MPTemplates
{
    /// <summary>
    /// 
    /// </summary>
    public partial class TemplateListControl : CommonBaseControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TemplateListControl"/> class.
        /// </summary>
        public TemplateListControl()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="TemplateListControl"/> class.
        /// </summary>
        /// <param name="parentTab">The parent tab.</param>
        public TemplateListControl(CommonBaseTab parentTab)
            : base(parentTab)
        {
            InitializeComponent();
        }

        /// <summary>
        /// Gets the grid control.
        /// </summary>
        /// <value>The grid control.</value>
        public GridControl GridControl
        {
            get
            {
                return gridControl;
            }
        }

        /// <summary>
        /// Gets or sets the template list.
        /// </summary>
        /// <value>The template list.</value>
        public DataTable TemplateList
        {
            set
            {
                gridControl.DataSource = value;
                LoadAccessLevel();

                MainData.ActiveActivityCount = this.ActiveActivityCount;
                MainData.CurrentActivityCount = this.CurrentActivityCount;

                UpdateButtonsState();
            }
            get
            {
                gridView.PostEditor();
                gridView.UpdateCurrentRow();
                return (DataTable)gridControl.DataSource;
            }
        }

        /// <summary>
        /// Gets current activity count, created for this template
        /// </summary>
        public int CurrentActivityCount
        {
            get 
            {
                int res = -1;
                if (gridView != null && gridView.FocusedRowHandle >= 0)
                {
                    res = ConvertEx.ToInt(gridView.GetFocusedRowCellValue(columnCurrentCount));
                }

                return res;
            }
        }

        /// <summary>
        /// Gets active activity count, created for this template
        /// </summary>
        public int ActiveActivityCount
        {
            get
            {
                int res = -1;
                if (gridView != null && gridView.FocusedRowHandle >= 0)
                {
                    res = ConvertEx.ToInt(gridView.GetFocusedRowCellValue(columnActiveCount));
                }

                return res;
              }
        }

        /// <summary>
        /// Loads the access level.
        /// </summary>
        private void LoadAccessLevel()
        {
            MainData.AccessLevel = DataProvider.AccessLevel;
            MainData.UserLevel = DataProvider.UserLevel;
        }

        /// <summary>
        /// Handles the Load event of the TemplateListControl control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void TemplateListControl_Load(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// News this instance.
        /// </summary>
        private void New()
        {
            LoadDetailsTab(false);
        }

        /// <summary>
        /// Edits this instance.
        /// </summary>
        private void Edit()
        {
            if (!MainData.CheckAccessLevel(AccessLevelType.Unknown))
            {
                LoadDetailsTab(true);
            }
        }

        /// <summary>
        /// Loads the details tab.
        /// </summary>
        /// <param name="isEditMode">if set to <c>true</c> [is edit mode].</param>
        private void LoadDetailsTab(bool isEditMode)
        {
            if (ParentTab != null)
            {                
                string strTabName = Resource.NewDetailsTabName;
                int templateId = 0;
                if (isEditMode)
                {
                    templateId = Convert.ToInt32(gridView.GetFocusedRowCellValue(columnId));
                    strTabName = Convert.ToString(gridView.GetFocusedRowCellValue(columnTemplateName));
                }
                if (!string.IsNullOrEmpty(strTabName))
                {
                    ParentTab.LoadDetailsTab(strTabName, templateId, isEditMode);
                }
            }
        }

        /// <summary>
        /// Updates the state of the buttons.
        /// </summary>
        private void UpdateButtonsState()
        {
            gridView.PostEditor();
            gridView.UpdateCurrentRow();

            barButtonNew.Enabled = MainData.AccessLevel >= (int)AccessLevelType.ReadWrite;
            barButtonEdit.Enabled = gridView.FocusedRowHandle >= 0 && !MainData.CheckAccessLevel(AccessLevelType.Unknown);
            barButtonEdit.Caption = MainData.AccessLevel >= (int)AccessLevelType.ReadWrite ? Resource.ButtonEdit : Resource.ButtonView;
        }

        /// <summary>
        /// Handles the FocusedRowChanged event of the gridView control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs"/> instance containing the event data.</param>
        private void gridView_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            UpdateButtonsState();
        }

        /// <summary>
        /// Handles the ItemClick event of the barButtonNew control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="DevExpress.XtraBars.ItemClickEventArgs"/> instance containing the event data.</param>
        private void barButtonNew_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            New();
        }

        /// <summary>
        /// Handles the ItemClick event of the barButtonEdit control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="DevExpress.XtraBars.ItemClickEventArgs"/> instance containing the event data.</param>
        private void barButtonEdit_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Edit();
        }

        /// <summary>
        /// Handles the DoubleClick event of the gridView control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void gridView_DoubleClick(object sender, EventArgs e)
        {
            if (gridView.FocusedRowHandle >= 0)
            {
                Point pt = gridView.GridControl.PointToClient(Control.MousePosition);
                GridHitInfo info = gridView.CalcHitInfo(pt);
                if (info.InRow || info.InRowCell)
                {
                    Edit();
                }
            }
        }        
    }
}
