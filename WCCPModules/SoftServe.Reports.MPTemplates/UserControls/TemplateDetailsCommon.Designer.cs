﻿namespace SoftServe.Reports.MPTemplates
{
    partial class TemplateDetailsCommon
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.xtraScrollableControl1 = new DevExpress.XtraEditors.XtraScrollableControl();
            this.groupPeriods = new DevExpress.XtraEditors.GroupControl();
            this.dateActiveFromMonth = new Logica.Reports.BaseReportControl.CommonControls.DateCombo();
            this.dateActiveToMonth = new Logica.Reports.BaseReportControl.CommonControls.DateCombo();
            this.dateActiveToDay = new Logica.Reports.BaseReportControl.CommonControls.DateCombo();
            this.dateFirstToYear = new Logica.Reports.BaseReportControl.CommonControls.DateCombo();
            this.dateFirstToMonth = new Logica.Reports.BaseReportControl.CommonControls.DateCombo();
            this.dateSecondToYear = new Logica.Reports.BaseReportControl.CommonControls.DateCombo();
            this.dateSecondFromYear = new Logica.Reports.BaseReportControl.CommonControls.DateCombo();
            this.dateSecondToMonth = new Logica.Reports.BaseReportControl.CommonControls.DateCombo();
            this.dateFirstFromYear = new Logica.Reports.BaseReportControl.CommonControls.DateCombo();
            this.dateSecondFromMonth = new Logica.Reports.BaseReportControl.CommonControls.DateCombo();
            this.dateSecondToDay = new Logica.Reports.BaseReportControl.CommonControls.DateCombo();
            this.dateFirstToDay = new Logica.Reports.BaseReportControl.CommonControls.DateCombo();
            this.dateSecondFromDay = new Logica.Reports.BaseReportControl.CommonControls.DateCombo();
            this.dateFirstFromMonth = new Logica.Reports.BaseReportControl.CommonControls.DateCombo();
            this.dateFirstFromDay = new Logica.Reports.BaseReportControl.CommonControls.DateCombo();
            this.dateActiveFromDay = new Logica.Reports.BaseReportControl.CommonControls.DateCombo();
            this.spinPeriodsMonthCheck = new DevExpress.XtraEditors.SpinEdit();
            this.lblPeriods4 = new DevExpress.XtraEditors.LabelControl();
            this.checkPeriodsFromTR = new DevExpress.XtraEditors.CheckEdit();
            this.lblPeriods6 = new DevExpress.XtraEditors.LabelControl();
            this.lblPeriods9 = new DevExpress.XtraEditors.LabelControl();
            this.lblPeriods10 = new DevExpress.XtraEditors.LabelControl();
            this.lblPeriods7 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.lblPeriods2 = new DevExpress.XtraEditors.LabelControl();
            this.lblPeriods3 = new DevExpress.XtraEditors.LabelControl();
            this.lblPeriods8 = new DevExpress.XtraEditors.LabelControl();
            this.lblPeriods5 = new DevExpress.XtraEditors.LabelControl();
            this.lblPeriods1 = new DevExpress.XtraEditors.LabelControl();
            this.lblDCRName = new DevExpress.XtraEditors.LabelControl();
            this.groupPosition = new DevExpress.XtraEditors.GroupControl();
            this.labelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.lblPositionExecutor = new DevExpress.XtraEditors.LabelControl();
            this.lblPositionControlGroup = new DevExpress.XtraEditors.LabelControl();
            this.lblPositionControlFact = new DevExpress.XtraEditors.LabelControl();
            this.lblPositionApprover = new DevExpress.XtraEditors.LabelControl();
            this.lblPositionTargetManager = new DevExpress.XtraEditors.LabelControl();
            this.lblPositionCreator = new DevExpress.XtraEditors.LabelControl();
            this.cbPositionExecutor = new DevExpress.XtraEditors.LookUpEdit();
            this.cbPositionControlFact = new DevExpress.XtraEditors.LookUpEdit();
            this.cbPositionControlGroup = new DevExpress.XtraEditors.LookUpEdit();
            this.cbPositionApprover = new DevExpress.XtraEditors.LookUpEdit();
            this.cbPositionCreator = new DevExpress.XtraEditors.LookUpEdit();
            this.cbPositionTargetManager = new DevExpress.XtraEditors.LookUpEdit();
            this.groupPromoGroup = new DevExpress.XtraEditors.GroupControl();
            this.checkPromoInActiveWaveThR = new DevExpress.XtraEditors.CheckEdit();
            this.checkPromoAllowFromThR = new DevExpress.XtraEditors.CheckEdit();
            this.checkPromoCascadeEdit = new DevExpress.XtraEditors.CheckEdit();
            this.groupKPK = new DevExpress.XtraEditors.GroupControl();
            this.checkKPKConnectionDate = new DevExpress.XtraEditors.CheckEdit();
            this.checkKPKShow = new DevExpress.XtraEditors.CheckEdit();
            this.lblKPKTemplateDescription = new DevExpress.XtraEditors.LabelControl();
            this.memoKPKTemplateDescription = new DevExpress.XtraEditors.MemoEdit();
            this.groupMain = new DevExpress.XtraEditors.GroupControl();
            this.ebTemplateName = new DevExpress.XtraEditors.TextEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl13 = new DevExpress.XtraEditors.LabelControl();
            this.cbTemplateType = new DevExpress.XtraEditors.LookUpEdit();
            this.checkCannotModify = new DevExpress.XtraEditors.CheckEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.documentUploader = new Logica.Reports.BaseReportControl.CommonControls.FileUploader();
            this.memoTemplateDescription = new DevExpress.XtraEditors.MemoEdit();
            this.labelControl18 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl12 = new DevExpress.XtraEditors.LabelControl();
            this.cbTemplateLevel = new DevExpress.XtraEditors.LookUpEdit();
            this.labelControl16 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl19 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl15 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl17 = new DevExpress.XtraEditors.LabelControl();
            this.cbTemplateChannel = new DevExpress.XtraEditors.LookUpEdit();
            this.labelControl14 = new DevExpress.XtraEditors.LabelControl();
            this.checkApproved = new DevExpress.XtraEditors.CheckEdit();
            this.groupControlGroup = new DevExpress.XtraEditors.GroupControl();
            this.checkControlInActiveWaveThR = new DevExpress.XtraEditors.CheckEdit();
            this.checkControlAllowFromThR = new DevExpress.XtraEditors.CheckEdit();
            this.checkControlCascadeEdit = new DevExpress.XtraEditors.CheckEdit();
            this.xtraScrollableControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupPeriods)).BeginInit();
            this.groupPeriods.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spinPeriodsMonthCheck.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkPeriodsFromTR.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupPosition)).BeginInit();
            this.groupPosition.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbPositionExecutor.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbPositionControlFact.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbPositionControlGroup.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbPositionApprover.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbPositionCreator.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbPositionTargetManager.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupPromoGroup)).BeginInit();
            this.groupPromoGroup.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkPromoInActiveWaveThR.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkPromoAllowFromThR.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkPromoCascadeEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupKPK)).BeginInit();
            this.groupKPK.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkKPKConnectionDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkKPKShow.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoKPKTemplateDescription.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupMain)).BeginInit();
            this.groupMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ebTemplateName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbTemplateType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkCannotModify.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoTemplateDescription.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbTemplateLevel.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbTemplateChannel.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkApproved.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControlGroup)).BeginInit();
            this.groupControlGroup.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkControlInActiveWaveThR.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkControlAllowFromThR.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkControlCascadeEdit.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // xtraScrollableControl1
            // 
            this.xtraScrollableControl1.Controls.Add(this.groupPeriods);
            this.xtraScrollableControl1.Controls.Add(this.lblDCRName);
            this.xtraScrollableControl1.Controls.Add(this.groupPosition);
            this.xtraScrollableControl1.Controls.Add(this.groupPromoGroup);
            this.xtraScrollableControl1.Controls.Add(this.groupKPK);
            this.xtraScrollableControl1.Controls.Add(this.groupMain);
            this.xtraScrollableControl1.Controls.Add(this.checkApproved);
            this.xtraScrollableControl1.Controls.Add(this.groupControlGroup);
            this.xtraScrollableControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraScrollableControl1.Location = new System.Drawing.Point(0, 0);
            this.xtraScrollableControl1.Name = "xtraScrollableControl1";
            this.xtraScrollableControl1.Size = new System.Drawing.Size(1022, 597);
            this.xtraScrollableControl1.TabIndex = 0;
            // 
            // groupPeriods
            // 
            this.groupPeriods.Controls.Add(this.dateActiveFromMonth);
            this.groupPeriods.Controls.Add(this.dateActiveToMonth);
            this.groupPeriods.Controls.Add(this.dateActiveToDay);
            this.groupPeriods.Controls.Add(this.dateFirstToYear);
            this.groupPeriods.Controls.Add(this.dateFirstToMonth);
            this.groupPeriods.Controls.Add(this.dateSecondToYear);
            this.groupPeriods.Controls.Add(this.dateSecondFromYear);
            this.groupPeriods.Controls.Add(this.dateSecondToMonth);
            this.groupPeriods.Controls.Add(this.dateFirstFromYear);
            this.groupPeriods.Controls.Add(this.dateSecondFromMonth);
            this.groupPeriods.Controls.Add(this.dateSecondToDay);
            this.groupPeriods.Controls.Add(this.dateFirstToDay);
            this.groupPeriods.Controls.Add(this.dateSecondFromDay);
            this.groupPeriods.Controls.Add(this.dateFirstFromMonth);
            this.groupPeriods.Controls.Add(this.dateFirstFromDay);
            this.groupPeriods.Controls.Add(this.dateActiveFromDay);
            this.groupPeriods.Controls.Add(this.spinPeriodsMonthCheck);
            this.groupPeriods.Controls.Add(this.lblPeriods4);
            this.groupPeriods.Controls.Add(this.checkPeriodsFromTR);
            this.groupPeriods.Controls.Add(this.lblPeriods6);
            this.groupPeriods.Controls.Add(this.lblPeriods9);
            this.groupPeriods.Controls.Add(this.lblPeriods10);
            this.groupPeriods.Controls.Add(this.lblPeriods7);
            this.groupPeriods.Controls.Add(this.labelControl1);
            this.groupPeriods.Controls.Add(this.lblPeriods2);
            this.groupPeriods.Controls.Add(this.lblPeriods3);
            this.groupPeriods.Controls.Add(this.lblPeriods8);
            this.groupPeriods.Controls.Add(this.lblPeriods5);
            this.groupPeriods.Controls.Add(this.lblPeriods1);
            this.groupPeriods.Location = new System.Drawing.Point(458, 28);
            this.groupPeriods.Name = "groupPeriods";
            this.groupPeriods.Size = new System.Drawing.Size(560, 254);
            this.groupPeriods.TabIndex = 14;
            this.groupPeriods.Text = "Периоды:";
            // 
            // dateActiveFromMonth
            // 
            this.dateActiveFromMonth.AllowsNull = true;
            this.dateActiveFromMonth.Location = new System.Drawing.Point(207, 42);
            this.dateActiveFromMonth.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dateActiveFromMonth.Name = "dateActiveFromMonth";
            this.dateActiveFromMonth.Size = new System.Drawing.Size(80, 22);
            this.dateActiveFromMonth.TabIndex = 9;
            this.dateActiveFromMonth.Tag = "12";
            this.dateActiveFromMonth.Value = 0;
            this.dateActiveFromMonth.ValueObj = 0;
            this.dateActiveFromMonth.EditValueChanged += new System.EventHandler(this.MonthYear_EditValueChanged);
            this.dateActiveFromMonth.Validating += new System.ComponentModel.CancelEventHandler(this.View_Validating);
            // 
            // dateActiveToMonth
            // 
            this.dateActiveToMonth.AllowsNull = true;
            this.dateActiveToMonth.Location = new System.Drawing.Point(382, 42);
            this.dateActiveToMonth.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dateActiveToMonth.Name = "dateActiveToMonth";
            this.dateActiveToMonth.Size = new System.Drawing.Size(80, 22);
            this.dateActiveToMonth.TabIndex = 9;
            this.dateActiveToMonth.Tag = "12";
            this.dateActiveToMonth.Value = 0;
            this.dateActiveToMonth.ValueObj = 0;
            this.dateActiveToMonth.EditValueChanged += new System.EventHandler(this.MonthYear_EditValueChanged);
            this.dateActiveToMonth.Validating += new System.ComponentModel.CancelEventHandler(this.View_Validating);
            // 
            // dateActiveToDay
            // 
            this.dateActiveToDay.AllowsNull = true;
            this.dateActiveToDay.Location = new System.Drawing.Point(316, 42);
            this.dateActiveToDay.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dateActiveToDay.Name = "dateActiveToDay";
            this.dateActiveToDay.Size = new System.Drawing.Size(60, 22);
            this.dateActiveToDay.TabIndex = 9;
            this.dateActiveToDay.Tag = "31";
            this.dateActiveToDay.Value = 0;
            this.dateActiveToDay.ValueObj = 0;
            this.dateActiveToDay.EditValueChanged += new System.EventHandler(this.MonthYear_EditValueChanged);
            this.dateActiveToDay.Validating += new System.ComponentModel.CancelEventHandler(this.View_Validating);
            // 
            // dateFirstToYear
            // 
            this.dateFirstToYear.AllowsNull = true;
            this.dateFirstToYear.Location = new System.Drawing.Point(468, 157);
            this.dateFirstToYear.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dateFirstToYear.Name = "dateFirstToYear";
            this.dateFirstToYear.Size = new System.Drawing.Size(80, 22);
            this.dateFirstToYear.TabIndex = 9;
            this.dateFirstToYear.Tag = "1";
            this.dateFirstToYear.Value = 0;
            this.dateFirstToYear.ValueObj = 0;
            this.dateFirstToYear.EditValueChanged += new System.EventHandler(this.MonthYear_EditValueChanged);
            this.dateFirstToYear.Validating += new System.ComponentModel.CancelEventHandler(this.View_Validating);
            // 
            // dateFirstToMonth
            // 
            this.dateFirstToMonth.AllowsNull = true;
            this.dateFirstToMonth.Location = new System.Drawing.Point(382, 157);
            this.dateFirstToMonth.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dateFirstToMonth.Name = "dateFirstToMonth";
            this.dateFirstToMonth.Size = new System.Drawing.Size(80, 22);
            this.dateFirstToMonth.TabIndex = 9;
            this.dateFirstToMonth.Tag = "12";
            this.dateFirstToMonth.Value = 0;
            this.dateFirstToMonth.ValueObj = 0;
            this.dateFirstToMonth.EditValueChanged += new System.EventHandler(this.MonthYear_EditValueChanged);
            this.dateFirstToMonth.Validating += new System.ComponentModel.CancelEventHandler(this.View_Validating);
            // 
            // dateSecondToYear
            // 
            this.dateSecondToYear.AllowsNull = true;
            this.dateSecondToYear.Location = new System.Drawing.Point(468, 214);
            this.dateSecondToYear.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dateSecondToYear.Name = "dateSecondToYear";
            this.dateSecondToYear.Size = new System.Drawing.Size(80, 22);
            this.dateSecondToYear.TabIndex = 9;
            this.dateSecondToYear.Tag = "1";
            this.dateSecondToYear.Value = 0;
            this.dateSecondToYear.ValueObj = 0;
            this.dateSecondToYear.EditValueChanged += new System.EventHandler(this.MonthYear_EditValueChanged);
            this.dateSecondToYear.Validating += new System.ComponentModel.CancelEventHandler(this.View_Validating);
            // 
            // dateSecondFromYear
            // 
            this.dateSecondFromYear.AllowsNull = true;
            this.dateSecondFromYear.Location = new System.Drawing.Point(212, 214);
            this.dateSecondFromYear.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dateSecondFromYear.Name = "dateSecondFromYear";
            this.dateSecondFromYear.Size = new System.Drawing.Size(80, 22);
            this.dateSecondFromYear.TabIndex = 9;
            this.dateSecondFromYear.Tag = "1";
            this.dateSecondFromYear.Value = 0;
            this.dateSecondFromYear.ValueObj = 0;
            this.dateSecondFromYear.EditValueChanged += new System.EventHandler(this.MonthYear_EditValueChanged);
            this.dateSecondFromYear.Validating += new System.ComponentModel.CancelEventHandler(this.View_Validating);
            // 
            // dateSecondToMonth
            // 
            this.dateSecondToMonth.AllowsNull = true;
            this.dateSecondToMonth.Location = new System.Drawing.Point(382, 214);
            this.dateSecondToMonth.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dateSecondToMonth.Name = "dateSecondToMonth";
            this.dateSecondToMonth.Size = new System.Drawing.Size(80, 22);
            this.dateSecondToMonth.TabIndex = 9;
            this.dateSecondToMonth.Tag = "12";
            this.dateSecondToMonth.Value = 0;
            this.dateSecondToMonth.ValueObj = 0;
            this.dateSecondToMonth.EditValueChanged += new System.EventHandler(this.MonthYear_EditValueChanged);
            this.dateSecondToMonth.Validating += new System.ComponentModel.CancelEventHandler(this.View_Validating);
            // 
            // dateFirstFromYear
            // 
            this.dateFirstFromYear.AllowsNull = true;
            this.dateFirstFromYear.Location = new System.Drawing.Point(212, 157);
            this.dateFirstFromYear.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dateFirstFromYear.Name = "dateFirstFromYear";
            this.dateFirstFromYear.Size = new System.Drawing.Size(80, 22);
            this.dateFirstFromYear.TabIndex = 9;
            this.dateFirstFromYear.Tag = "1";
            this.dateFirstFromYear.Value = 0;
            this.dateFirstFromYear.ValueObj = 0;
            this.dateFirstFromYear.EditValueChanged += new System.EventHandler(this.MonthYear_EditValueChanged);
            this.dateFirstFromYear.Validating += new System.ComponentModel.CancelEventHandler(this.View_Validating);
            // 
            // dateSecondFromMonth
            // 
            this.dateSecondFromMonth.AllowsNull = true;
            this.dateSecondFromMonth.Location = new System.Drawing.Point(126, 214);
            this.dateSecondFromMonth.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dateSecondFromMonth.Name = "dateSecondFromMonth";
            this.dateSecondFromMonth.Size = new System.Drawing.Size(80, 22);
            this.dateSecondFromMonth.TabIndex = 9;
            this.dateSecondFromMonth.Tag = "12";
            this.dateSecondFromMonth.Value = 0;
            this.dateSecondFromMonth.ValueObj = 0;
            this.dateSecondFromMonth.EditValueChanged += new System.EventHandler(this.MonthYear_EditValueChanged);
            this.dateSecondFromMonth.Validating += new System.ComponentModel.CancelEventHandler(this.View_Validating);
            // 
            // dateSecondToDay
            // 
            this.dateSecondToDay.AllowsNull = true;
            this.dateSecondToDay.Location = new System.Drawing.Point(316, 214);
            this.dateSecondToDay.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dateSecondToDay.Name = "dateSecondToDay";
            this.dateSecondToDay.Size = new System.Drawing.Size(60, 22);
            this.dateSecondToDay.TabIndex = 9;
            this.dateSecondToDay.Tag = "31";
            this.dateSecondToDay.Value = 0;
            this.dateSecondToDay.ValueObj = 0;
            this.dateSecondToDay.EditValueChanged += new System.EventHandler(this.MonthYear_EditValueChanged);
            this.dateSecondToDay.Validating += new System.ComponentModel.CancelEventHandler(this.View_Validating);
            // 
            // dateFirstToDay
            // 
            this.dateFirstToDay.AllowsNull = true;
            this.dateFirstToDay.Location = new System.Drawing.Point(316, 157);
            this.dateFirstToDay.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dateFirstToDay.Name = "dateFirstToDay";
            this.dateFirstToDay.Size = new System.Drawing.Size(60, 22);
            this.dateFirstToDay.TabIndex = 9;
            this.dateFirstToDay.Tag = "31";
            this.dateFirstToDay.Value = 0;
            this.dateFirstToDay.ValueObj = 0;
            this.dateFirstToDay.EditValueChanged += new System.EventHandler(this.MonthYear_EditValueChanged);
            this.dateFirstToDay.Validating += new System.ComponentModel.CancelEventHandler(this.View_Validating);
            // 
            // dateSecondFromDay
            // 
            this.dateSecondFromDay.AllowsNull = true;
            this.dateSecondFromDay.Location = new System.Drawing.Point(60, 214);
            this.dateSecondFromDay.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dateSecondFromDay.Name = "dateSecondFromDay";
            this.dateSecondFromDay.Size = new System.Drawing.Size(60, 22);
            this.dateSecondFromDay.TabIndex = 9;
            this.dateSecondFromDay.Tag = "31";
            this.dateSecondFromDay.Value = 0;
            this.dateSecondFromDay.ValueObj = 0;
            this.dateSecondFromDay.EditValueChanged += new System.EventHandler(this.MonthYear_EditValueChanged);
            this.dateSecondFromDay.Validating += new System.ComponentModel.CancelEventHandler(this.View_Validating);
            // 
            // dateFirstFromMonth
            // 
            this.dateFirstFromMonth.AllowsNull = true;
            this.dateFirstFromMonth.Location = new System.Drawing.Point(126, 157);
            this.dateFirstFromMonth.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dateFirstFromMonth.Name = "dateFirstFromMonth";
            this.dateFirstFromMonth.Size = new System.Drawing.Size(80, 22);
            this.dateFirstFromMonth.TabIndex = 9;
            this.dateFirstFromMonth.Tag = "12";
            this.dateFirstFromMonth.Value = 0;
            this.dateFirstFromMonth.ValueObj = 0;
            this.dateFirstFromMonth.EditValueChanged += new System.EventHandler(this.MonthYear_EditValueChanged);
            this.dateFirstFromMonth.Validating += new System.ComponentModel.CancelEventHandler(this.View_Validating);
            // 
            // dateFirstFromDay
            // 
            this.dateFirstFromDay.AllowsNull = true;
            this.dateFirstFromDay.Location = new System.Drawing.Point(60, 157);
            this.dateFirstFromDay.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dateFirstFromDay.Name = "dateFirstFromDay";
            this.dateFirstFromDay.Size = new System.Drawing.Size(60, 22);
            this.dateFirstFromDay.TabIndex = 9;
            this.dateFirstFromDay.Tag = "31";
            this.dateFirstFromDay.Value = 0;
            this.dateFirstFromDay.ValueObj = 0;
            this.dateFirstFromDay.EditValueChanged += new System.EventHandler(this.MonthYear_EditValueChanged);
            this.dateFirstFromDay.Validating += new System.ComponentModel.CancelEventHandler(this.View_Validating);
            // 
            // dateActiveFromDay
            // 
            this.dateActiveFromDay.AllowsNull = true;
            this.dateActiveFromDay.Location = new System.Drawing.Point(142, 42);
            this.dateActiveFromDay.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dateActiveFromDay.Name = "dateActiveFromDay";
            this.dateActiveFromDay.Size = new System.Drawing.Size(60, 22);
            this.dateActiveFromDay.TabIndex = 9;
            this.dateActiveFromDay.Tag = "31";
            this.dateActiveFromDay.Value = 0;
            this.dateActiveFromDay.ValueObj = 0;
            this.dateActiveFromDay.EditValueChanged += new System.EventHandler(this.MonthYear_EditValueChanged);
            this.dateActiveFromDay.Validating += new System.ComponentModel.CancelEventHandler(this.View_Validating);
            // 
            // spinPeriodsMonthCheck
            // 
            this.spinPeriodsMonthCheck.EditValue = new decimal(new int[] {
            3,
            0,
            0,
            0});
            this.spinPeriodsMonthCheck.Location = new System.Drawing.Point(304, 79);
            this.spinPeriodsMonthCheck.Name = "spinPeriodsMonthCheck";
            this.spinPeriodsMonthCheck.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.spinPeriodsMonthCheck.Properties.IsFloatValue = false;
            this.spinPeriodsMonthCheck.Properties.Mask.EditMask = "N00";
            this.spinPeriodsMonthCheck.Properties.MaxValue = new decimal(new int[] {
            36,
            0,
            0,
            0});
            this.spinPeriodsMonthCheck.Properties.MinValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.spinPeriodsMonthCheck.Size = new System.Drawing.Size(47, 20);
            this.spinPeriodsMonthCheck.TabIndex = 4;
            this.spinPeriodsMonthCheck.Spin += new DevExpress.XtraEditors.Controls.SpinEventHandler(this.spinPeriodsMonthCheck_Spin);
            this.spinPeriodsMonthCheck.EditValueChanged += new System.EventHandler(this.spinPeriodsMonthCheck_EditValueChanged);
            // 
            // lblPeriods4
            // 
            this.lblPeriods4.Location = new System.Drawing.Point(357, 83);
            this.lblPeriods4.Name = "lblPeriods4";
            this.lblPeriods4.Size = new System.Drawing.Size(17, 13);
            this.lblPeriods4.TabIndex = 4;
            this.lblPeriods4.Text = "мес";
            // 
            // checkPeriodsFromTR
            // 
            this.checkPeriodsFromTR.Location = new System.Drawing.Point(14, 108);
            this.checkPeriodsFromTR.Name = "checkPeriodsFromTR";
            this.checkPeriodsFromTR.Properties.Caption = "Брать базовые периоды из активной волны Th.R";
            this.checkPeriodsFromTR.Size = new System.Drawing.Size(279, 19);
            this.checkPeriodsFromTR.TabIndex = 8;
            this.checkPeriodsFromTR.CheckedChanged += new System.EventHandler(this.checkPeriodsFromTR_CheckedChanged);
            // 
            // lblPeriods6
            // 
            this.lblPeriods6.Location = new System.Drawing.Point(44, 160);
            this.lblPeriods6.Name = "lblPeriods6";
            this.lblPeriods6.Size = new System.Drawing.Size(5, 13);
            this.lblPeriods6.TabIndex = 4;
            this.lblPeriods6.Text = "с";
            // 
            // lblPeriods9
            // 
            this.lblPeriods9.Location = new System.Drawing.Point(44, 217);
            this.lblPeriods9.Name = "lblPeriods9";
            this.lblPeriods9.Size = new System.Drawing.Size(5, 13);
            this.lblPeriods9.TabIndex = 4;
            this.lblPeriods9.Text = "с";
            // 
            // lblPeriods10
            // 
            this.lblPeriods10.Location = new System.Drawing.Point(300, 218);
            this.lblPeriods10.Name = "lblPeriods10";
            this.lblPeriods10.Size = new System.Drawing.Size(12, 13);
            this.lblPeriods10.TabIndex = 4;
            this.lblPeriods10.Text = "по";
            // 
            // lblPeriods7
            // 
            this.lblPeriods7.Location = new System.Drawing.Point(300, 161);
            this.lblPeriods7.Name = "lblPeriods7";
            this.lblPeriods7.Size = new System.Drawing.Size(12, 13);
            this.lblPeriods7.TabIndex = 4;
            this.lblPeriods7.Text = "по";
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(131, 46);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(5, 13);
            this.labelControl1.TabIndex = 4;
            this.labelControl1.Text = "с";
            // 
            // lblPeriods2
            // 
            this.lblPeriods2.Location = new System.Drawing.Point(295, 46);
            this.lblPeriods2.Name = "lblPeriods2";
            this.lblPeriods2.Size = new System.Drawing.Size(12, 13);
            this.lblPeriods2.TabIndex = 4;
            this.lblPeriods2.Text = "по";
            // 
            // lblPeriods3
            // 
            this.lblPeriods3.Appearance.Options.UseTextOptions = true;
            this.lblPeriods3.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.lblPeriods3.Location = new System.Drawing.Point(17, 83);
            this.lblPeriods3.Name = "lblPeriods3";
            this.lblPeriods3.Size = new System.Drawing.Size(268, 13);
            this.lblPeriods3.TabIndex = 4;
            this.lblPeriods3.Text = "Отслеживать прирост после окончания активности:";
            // 
            // lblPeriods8
            // 
            this.lblPeriods8.Location = new System.Drawing.Point(17, 195);
            this.lblPeriods8.Name = "lblPeriods8";
            this.lblPeriods8.Size = new System.Drawing.Size(126, 13);
            this.lblPeriods8.TabIndex = 4;
            this.lblPeriods8.Text = "Второй базовый период:";
            // 
            // lblPeriods5
            // 
            this.lblPeriods5.Location = new System.Drawing.Point(17, 138);
            this.lblPeriods5.Name = "lblPeriods5";
            this.lblPeriods5.Size = new System.Drawing.Size(129, 13);
            this.lblPeriods5.TabIndex = 4;
            this.lblPeriods5.Text = "Первый базовый период:";
            // 
            // lblPeriods1
            // 
            this.lblPeriods1.Location = new System.Drawing.Point(17, 46);
            this.lblPeriods1.Name = "lblPeriods1";
            this.lblPeriods1.Size = new System.Drawing.Size(104, 13);
            this.lblPeriods1.TabIndex = 4;
            this.lblPeriods1.Text = "Период активности:";
            // 
            // lblDCRName
            // 
            this.lblDCRName.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal;
            this.lblDCRName.Location = new System.Drawing.Point(12, 9);
            this.lblDCRName.Name = "lblDCRName";
            this.lblDCRName.Size = new System.Drawing.Size(41, 13);
            this.lblDCRName.TabIndex = 11;
            this.lblDCRName.Text = "Создал:";
            // 
            // groupPosition
            // 
            this.groupPosition.Controls.Add(this.labelControl11);
            this.groupPosition.Controls.Add(this.labelControl10);
            this.groupPosition.Controls.Add(this.labelControl9);
            this.groupPosition.Controls.Add(this.labelControl8);
            this.groupPosition.Controls.Add(this.labelControl7);
            this.groupPosition.Controls.Add(this.labelControl6);
            this.groupPosition.Controls.Add(this.lblPositionExecutor);
            this.groupPosition.Controls.Add(this.lblPositionControlGroup);
            this.groupPosition.Controls.Add(this.lblPositionControlFact);
            this.groupPosition.Controls.Add(this.lblPositionApprover);
            this.groupPosition.Controls.Add(this.lblPositionTargetManager);
            this.groupPosition.Controls.Add(this.lblPositionCreator);
            this.groupPosition.Controls.Add(this.cbPositionExecutor);
            this.groupPosition.Controls.Add(this.cbPositionControlFact);
            this.groupPosition.Controls.Add(this.cbPositionControlGroup);
            this.groupPosition.Controls.Add(this.cbPositionApprover);
            this.groupPosition.Controls.Add(this.cbPositionCreator);
            this.groupPosition.Controls.Add(this.cbPositionTargetManager);
            this.groupPosition.Location = new System.Drawing.Point(461, 287);
            this.groupPosition.Name = "groupPosition";
            this.groupPosition.Size = new System.Drawing.Size(188, 307);
            this.groupPosition.TabIndex = 13;
            this.groupPosition.Text = "Должности:";
            // 
            // labelControl11
            // 
            this.labelControl11.Appearance.ForeColor = System.Drawing.Color.Red;
            this.labelControl11.Appearance.Options.UseForeColor = true;
            this.labelControl11.Location = new System.Drawing.Point(112, 258);
            this.labelControl11.Name = "labelControl11";
            this.labelControl11.Size = new System.Drawing.Size(6, 13);
            this.labelControl11.TabIndex = 10;
            this.labelControl11.Text = "*";
            // 
            // labelControl10
            // 
            this.labelControl10.Appearance.ForeColor = System.Drawing.Color.Red;
            this.labelControl10.Appearance.Options.UseForeColor = true;
            this.labelControl10.Location = new System.Drawing.Point(142, 210);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(6, 13);
            this.labelControl10.TabIndex = 10;
            this.labelControl10.Text = "*";
            // 
            // labelControl9
            // 
            this.labelControl9.Appearance.ForeColor = System.Drawing.Color.Red;
            this.labelControl9.Appearance.Options.UseForeColor = true;
            this.labelControl9.Location = new System.Drawing.Point(118, 163);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(6, 13);
            this.labelControl9.TabIndex = 10;
            this.labelControl9.Text = "*";
            // 
            // labelControl8
            // 
            this.labelControl8.Appearance.ForeColor = System.Drawing.Color.Red;
            this.labelControl8.Appearance.Options.UseForeColor = true;
            this.labelControl8.Location = new System.Drawing.Point(135, 116);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(6, 13);
            this.labelControl8.TabIndex = 10;
            this.labelControl8.Text = "*";
            // 
            // labelControl7
            // 
            this.labelControl7.Appearance.ForeColor = System.Drawing.Color.Red;
            this.labelControl7.Appearance.Options.UseForeColor = true;
            this.labelControl7.Location = new System.Drawing.Point(156, 68);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(6, 13);
            this.labelControl7.TabIndex = 10;
            this.labelControl7.Text = "*";
            // 
            // labelControl6
            // 
            this.labelControl6.Appearance.ForeColor = System.Drawing.Color.Red;
            this.labelControl6.Appearance.Options.UseForeColor = true;
            this.labelControl6.Location = new System.Drawing.Point(130, 21);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(6, 13);
            this.labelControl6.TabIndex = 10;
            this.labelControl6.Text = "*";
            // 
            // lblPositionExecutor
            // 
            this.lblPositionExecutor.Location = new System.Drawing.Point(6, 116);
            this.lblPositionExecutor.Name = "lblPositionExecutor";
            this.lblPositionExecutor.Size = new System.Drawing.Size(123, 13);
            this.lblPositionExecutor.TabIndex = 4;
            this.lblPositionExecutor.Text = "Исполнитель в рознице:";
            // 
            // lblPositionControlGroup
            // 
            this.lblPositionControlGroup.Location = new System.Drawing.Point(6, 68);
            this.lblPositionControlGroup.Name = "lblPositionControlGroup";
            this.lblPositionControlGroup.Size = new System.Drawing.Size(145, 13);
            this.lblPositionControlGroup.TabIndex = 4;
            this.lblPositionControlGroup.Text = "Выбор контрольной группы:";
            // 
            // lblPositionControlFact
            // 
            this.lblPositionControlFact.Location = new System.Drawing.Point(6, 258);
            this.lblPositionControlFact.Name = "lblPositionControlFact";
            this.lblPositionControlFact.Size = new System.Drawing.Size(100, 13);
            this.lblPositionControlFact.TabIndex = 4;
            this.lblPositionControlFact.Text = "Контроллер факта:";
            // 
            // lblPositionApprover
            // 
            this.lblPositionApprover.Location = new System.Drawing.Point(6, 210);
            this.lblPositionApprover.Name = "lblPositionApprover";
            this.lblPositionApprover.Size = new System.Drawing.Size(130, 13);
            this.lblPositionApprover.TabIndex = 4;
            this.lblPositionApprover.Text = "Утверждает активность:";
            // 
            // lblPositionTargetManager
            // 
            this.lblPositionTargetManager.Location = new System.Drawing.Point(6, 163);
            this.lblPositionTargetManager.Name = "lblPositionTargetManager";
            this.lblPositionTargetManager.Size = new System.Drawing.Size(106, 13);
            this.lblPositionTargetManager.TabIndex = 4;
            this.lblPositionTargetManager.Text = "Постановщик целей:";
            // 
            // lblPositionCreator
            // 
            this.lblPositionCreator.Location = new System.Drawing.Point(6, 21);
            this.lblPositionCreator.Name = "lblPositionCreator";
            this.lblPositionCreator.Size = new System.Drawing.Size(121, 13);
            this.lblPositionCreator.TabIndex = 4;
            this.lblPositionCreator.Text = "Инициатор активности:";
            // 
            // cbPositionExecutor
            // 
            this.cbPositionExecutor.Location = new System.Drawing.Point(6, 135);
            this.cbPositionExecutor.Name = "cbPositionExecutor";
            this.cbPositionExecutor.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbPositionExecutor.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("PositionName", "Должность")});
            this.cbPositionExecutor.Properties.DisplayMember = "PositionName";
            this.cbPositionExecutor.Properties.NullText = "<Не задано>";
            this.cbPositionExecutor.Properties.ValueMember = "Position_ID";
            this.cbPositionExecutor.Size = new System.Drawing.Size(175, 20);
            this.cbPositionExecutor.TabIndex = 1;
            this.cbPositionExecutor.Validating += new System.ComponentModel.CancelEventHandler(this.View_Validating);
            // 
            // cbPositionControlFact
            // 
            this.cbPositionControlFact.Location = new System.Drawing.Point(6, 277);
            this.cbPositionControlFact.Name = "cbPositionControlFact";
            this.cbPositionControlFact.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbPositionControlFact.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("PositionName", "Должность")});
            this.cbPositionControlFact.Properties.DisplayMember = "PositionName";
            this.cbPositionControlFact.Properties.NullText = "<Не задано>";
            this.cbPositionControlFact.Properties.ValueMember = "Position_ID";
            this.cbPositionControlFact.Size = new System.Drawing.Size(175, 20);
            this.cbPositionControlFact.TabIndex = 1;
            this.cbPositionControlFact.Validating += new System.ComponentModel.CancelEventHandler(this.View_Validating);
            // 
            // cbPositionControlGroup
            // 
            this.cbPositionControlGroup.Location = new System.Drawing.Point(6, 87);
            this.cbPositionControlGroup.Name = "cbPositionControlGroup";
            this.cbPositionControlGroup.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbPositionControlGroup.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("PositionName", "Должность")});
            this.cbPositionControlGroup.Properties.DisplayMember = "PositionName";
            this.cbPositionControlGroup.Properties.NullText = "<Не задано>";
            this.cbPositionControlGroup.Properties.ValueMember = "Position_ID";
            this.cbPositionControlGroup.Size = new System.Drawing.Size(175, 20);
            this.cbPositionControlGroup.TabIndex = 1;
            this.cbPositionControlGroup.Validating += new System.ComponentModel.CancelEventHandler(this.View_Validating);
            // 
            // cbPositionApprover
            // 
            this.cbPositionApprover.Location = new System.Drawing.Point(6, 229);
            this.cbPositionApprover.Name = "cbPositionApprover";
            this.cbPositionApprover.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbPositionApprover.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("PositionName", "Должность")});
            this.cbPositionApprover.Properties.DisplayMember = "PositionName";
            this.cbPositionApprover.Properties.NullText = "<Не задано>";
            this.cbPositionApprover.Properties.ValueMember = "Position_ID";
            this.cbPositionApprover.Size = new System.Drawing.Size(175, 20);
            this.cbPositionApprover.TabIndex = 1;
            this.cbPositionApprover.Validating += new System.ComponentModel.CancelEventHandler(this.View_Validating);
            // 
            // cbPositionCreator
            // 
            this.cbPositionCreator.Location = new System.Drawing.Point(6, 40);
            this.cbPositionCreator.Name = "cbPositionCreator";
            this.cbPositionCreator.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbPositionCreator.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("PositionName", "Должность")});
            this.cbPositionCreator.Properties.DisplayMember = "PositionName";
            this.cbPositionCreator.Properties.NullText = "<Не задано>";
            this.cbPositionCreator.Properties.ValueMember = "Position_ID";
            this.cbPositionCreator.Size = new System.Drawing.Size(175, 20);
            this.cbPositionCreator.TabIndex = 1;
            this.cbPositionCreator.Validating += new System.ComponentModel.CancelEventHandler(this.View_Validating);
            // 
            // cbPositionTargetManager
            // 
            this.cbPositionTargetManager.Location = new System.Drawing.Point(6, 182);
            this.cbPositionTargetManager.Name = "cbPositionTargetManager";
            this.cbPositionTargetManager.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbPositionTargetManager.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("PositionName", "Должность")});
            this.cbPositionTargetManager.Properties.DisplayMember = "PositionName";
            this.cbPositionTargetManager.Properties.NullText = "<Не задано>";
            this.cbPositionTargetManager.Properties.ValueMember = "Position_ID";
            this.cbPositionTargetManager.Size = new System.Drawing.Size(175, 20);
            this.cbPositionTargetManager.TabIndex = 1;
            this.cbPositionTargetManager.Validating += new System.ComponentModel.CancelEventHandler(this.View_Validating);
            // 
            // groupPromoGroup
            // 
            this.groupPromoGroup.Controls.Add(this.checkPromoInActiveWaveThR);
            this.groupPromoGroup.Controls.Add(this.checkPromoAllowFromThR);
            this.groupPromoGroup.Controls.Add(this.checkPromoCascadeEdit);
            this.groupPromoGroup.Location = new System.Drawing.Point(655, 288);
            this.groupPromoGroup.Name = "groupPromoGroup";
            this.groupPromoGroup.Size = new System.Drawing.Size(363, 106);
            this.groupPromoGroup.TabIndex = 16;
            this.groupPromoGroup.Text = "Формирование промо группы ТТ:";
            // 
            // checkPromoInActiveWaveThR
            // 
            this.checkPromoInActiveWaveThR.Location = new System.Drawing.Point(6, 74);
            this.checkPromoInActiveWaveThR.Name = "checkPromoInActiveWaveThR";
            this.checkPromoInActiveWaveThR.Properties.Caption = "Все ТТ из адрески должны быть в активной волне Th.R";
            this.checkPromoInActiveWaveThR.Size = new System.Drawing.Size(322, 19);
            this.checkPromoInActiveWaveThR.TabIndex = 8;
            // 
            // checkPromoAllowFromThR
            // 
            this.checkPromoAllowFromThR.Location = new System.Drawing.Point(6, 50);
            this.checkPromoAllowFromThR.Name = "checkPromoAllowFromThR";
            this.checkPromoAllowFromThR.Properties.Caption = "Разрешено вносить ТТ  в адреску не из фильтра Th.R";
            this.checkPromoAllowFromThR.Size = new System.Drawing.Size(305, 19);
            this.checkPromoAllowFromThR.TabIndex = 8;
            // 
            // checkPromoCascadeEdit
            // 
            this.checkPromoCascadeEdit.Location = new System.Drawing.Point(6, 26);
            this.checkPromoCascadeEdit.Name = "checkPromoCascadeEdit";
            this.checkPromoCascadeEdit.Properties.Caption = "Каскадное редактирование адрески";
            this.checkPromoCascadeEdit.Size = new System.Drawing.Size(234, 19);
            this.checkPromoCascadeEdit.TabIndex = 8;
            // 
            // groupKPK
            // 
            this.groupKPK.Controls.Add(this.checkKPKConnectionDate);
            this.groupKPK.Controls.Add(this.checkKPKShow);
            this.groupKPK.Controls.Add(this.lblKPKTemplateDescription);
            this.groupKPK.Controls.Add(this.memoKPKTemplateDescription);
            this.groupKPK.Location = new System.Drawing.Point(4, 434);
            this.groupKPK.Name = "groupKPK";
            this.groupKPK.Size = new System.Drawing.Size(451, 160);
            this.groupKPK.TabIndex = 18;
            this.groupKPK.Text = "КПК:";
            // 
            // checkKPKConnectionDate
            // 
            this.checkKPKConnectionDate.Location = new System.Drawing.Point(6, 44);
            this.checkKPKConnectionDate.Name = "checkKPKConnectionDate";
            this.checkKPKConnectionDate.Properties.Caption = "С учетом даты подключения";
            this.checkKPKConnectionDate.Size = new System.Drawing.Size(234, 19);
            this.checkKPKConnectionDate.TabIndex = 8;
            // 
            // checkKPKShow
            // 
            this.checkKPKShow.Location = new System.Drawing.Point(6, 22);
            this.checkKPKShow.Name = "checkKPKShow";
            this.checkKPKShow.Properties.Caption = "Отображать в КПК";
            this.checkKPKShow.Size = new System.Drawing.Size(234, 19);
            this.checkKPKShow.TabIndex = 8;
            this.checkKPKShow.EditValueChanged += new System.EventHandler(this.checkKPKShow_EditValueChanged);
            // 
            // lblKPKTemplateDescription
            // 
            this.lblKPKTemplateDescription.Location = new System.Drawing.Point(8, 68);
            this.lblKPKTemplateDescription.Name = "lblKPKTemplateDescription";
            this.lblKPKTemplateDescription.Size = new System.Drawing.Size(189, 13);
            this.lblKPKTemplateDescription.TabIndex = 4;
            this.lblKPKTemplateDescription.Text = "Шаблон описания активности в КПК:";
            // 
            // memoKPKTemplateDescription
            // 
            this.memoKPKTemplateDescription.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.memoKPKTemplateDescription.Location = new System.Drawing.Point(8, 87);
            this.memoKPKTemplateDescription.Name = "memoKPKTemplateDescription";
            this.memoKPKTemplateDescription.Size = new System.Drawing.Size(437, 63);
            this.memoKPKTemplateDescription.TabIndex = 5;
            this.memoKPKTemplateDescription.Validating += new System.ComponentModel.CancelEventHandler(this.View_Validating);
            // 
            // groupMain
            // 
            this.groupMain.Controls.Add(this.ebTemplateName);
            this.groupMain.Controls.Add(this.labelControl2);
            this.groupMain.Controls.Add(this.labelControl13);
            this.groupMain.Controls.Add(this.cbTemplateType);
            this.groupMain.Controls.Add(this.checkCannotModify);
            this.groupMain.Controls.Add(this.labelControl3);
            this.groupMain.Controls.Add(this.documentUploader);
            this.groupMain.Controls.Add(this.memoTemplateDescription);
            this.groupMain.Controls.Add(this.labelControl18);
            this.groupMain.Controls.Add(this.labelControl12);
            this.groupMain.Controls.Add(this.cbTemplateLevel);
            this.groupMain.Controls.Add(this.labelControl16);
            this.groupMain.Controls.Add(this.labelControl4);
            this.groupMain.Controls.Add(this.labelControl19);
            this.groupMain.Controls.Add(this.labelControl15);
            this.groupMain.Controls.Add(this.labelControl5);
            this.groupMain.Controls.Add(this.labelControl17);
            this.groupMain.Controls.Add(this.cbTemplateChannel);
            this.groupMain.Controls.Add(this.labelControl14);
            this.groupMain.Location = new System.Drawing.Point(4, 29);
            this.groupMain.Name = "groupMain";
            this.groupMain.Size = new System.Drawing.Size(451, 402);
            this.groupMain.TabIndex = 17;
            this.groupMain.Text = "Шаблон активности:";
            // 
            // ebTemplateName
            // 
            this.ebTemplateName.Location = new System.Drawing.Point(6, 51);
            this.ebTemplateName.Name = "ebTemplateName";
            this.ebTemplateName.Size = new System.Drawing.Size(210, 20);
            this.ebTemplateName.TabIndex = 3;
            this.ebTemplateName.Validating += new System.ComponentModel.CancelEventHandler(this.View_Validating);
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(8, 32);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(23, 13);
            this.labelControl2.TabIndex = 4;
            this.labelControl2.Text = "Имя:";
            // 
            // labelControl13
            // 
            this.labelControl13.Appearance.ForeColor = System.Drawing.Color.Red;
            this.labelControl13.Appearance.Options.UseForeColor = true;
            this.labelControl13.Location = new System.Drawing.Point(37, 32);
            this.labelControl13.Name = "labelControl13";
            this.labelControl13.Size = new System.Drawing.Size(6, 13);
            this.labelControl13.TabIndex = 10;
            this.labelControl13.Text = "*";
            // 
            // cbTemplateType
            // 
            this.cbTemplateType.Location = new System.Drawing.Point(235, 51);
            this.cbTemplateType.Name = "cbTemplateType";
            this.cbTemplateType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbTemplateType.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("ActionType_Name", "Тип")});
            this.cbTemplateType.Properties.DisplayMember = "ActionType_Name";
            this.cbTemplateType.Properties.NullText = "<Не задано>";
            this.cbTemplateType.Properties.ValueMember = "ActionType_ID";
            this.cbTemplateType.Size = new System.Drawing.Size(210, 20);
            this.cbTemplateType.TabIndex = 1;
            this.cbTemplateType.EditValueChanged += new System.EventHandler(this.cbTemplateType_EditValueChanged);
            this.cbTemplateType.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(this.cbTemplateType_EditValueChanging);
            this.cbTemplateType.Validating += new System.ComponentModel.CancelEventHandler(this.View_Validating);
            // 
            // checkCannotModify
            // 
            this.checkCannotModify.Location = new System.Drawing.Point(4, 381);
            this.checkCannotModify.Name = "checkCannotModify";
            this.checkCannotModify.Properties.Caption = "Запретить менять описание активности";
            this.checkCannotModify.Size = new System.Drawing.Size(420, 19);
            this.checkCannotModify.TabIndex = 8;
            this.checkCannotModify.CheckedChanged += new System.EventHandler(this.checkCannotModify_CheckedChanged);
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(235, 32);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(22, 13);
            this.labelControl3.TabIndex = 4;
            this.labelControl3.Text = "Тип:";
            // 
            // documentUploader
            // 
            this.documentUploader.ButtonLoadText = "Загрузить файл";
            this.documentUploader.FieldNameFileData = "Presentation_FileData";
            this.documentUploader.FieldNameFileName = "Presentation_FileName";
            this.documentUploader.FieldNameID = "Presentation_ID";
            this.documentUploader.Location = new System.Drawing.Point(235, 156);
            this.documentUploader.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.documentUploader.MaxItemsCount = 10;
            this.documentUploader.Name = "documentUploader";
            this.documentUploader.ReadOnly = false;
            this.documentUploader.Size = new System.Drawing.Size(210, 219);
            this.documentUploader.TabIndex = 9;
            this.documentUploader.GetFileData += new Logica.Reports.BaseReportControl.CommonControls.FileUploader.FileDataEventHandler(this.documentUploader_GetFileData);
            // 
            // memoTemplateDescription
            // 
            this.memoTemplateDescription.Location = new System.Drawing.Point(6, 156);
            this.memoTemplateDescription.Name = "memoTemplateDescription";
            this.memoTemplateDescription.Size = new System.Drawing.Size(210, 219);
            this.memoTemplateDescription.TabIndex = 5;
            this.memoTemplateDescription.Validating += new System.ComponentModel.CancelEventHandler(this.View_Validating);
            // 
            // labelControl18
            // 
            this.labelControl18.Appearance.ForeColor = System.Drawing.Color.Red;
            this.labelControl18.Appearance.Options.UseForeColor = true;
            this.labelControl18.Location = new System.Drawing.Point(303, 137);
            this.labelControl18.Name = "labelControl18";
            this.labelControl18.Size = new System.Drawing.Size(6, 13);
            this.labelControl18.TabIndex = 10;
            this.labelControl18.Text = "*";
            // 
            // labelControl12
            // 
            this.labelControl12.Appearance.ForeColor = System.Drawing.Color.Red;
            this.labelControl12.Appearance.Options.UseForeColor = true;
            this.labelControl12.Location = new System.Drawing.Point(263, 32);
            this.labelControl12.Name = "labelControl12";
            this.labelControl12.Size = new System.Drawing.Size(6, 13);
            this.labelControl12.TabIndex = 10;
            this.labelControl12.Text = "*";
            // 
            // cbTemplateLevel
            // 
            this.cbTemplateLevel.Location = new System.Drawing.Point(6, 101);
            this.cbTemplateLevel.Name = "cbTemplateLevel";
            this.cbTemplateLevel.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbTemplateLevel.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("ActionLevel_Name", "Уровень")});
            this.cbTemplateLevel.Properties.DisplayMember = "ActionLevel_Name";
            this.cbTemplateLevel.Properties.NullText = "<Не задано>";
            this.cbTemplateLevel.Properties.ValueMember = "ActionLevel_ID";
            this.cbTemplateLevel.Size = new System.Drawing.Size(210, 20);
            this.cbTemplateLevel.TabIndex = 1;
            this.cbTemplateLevel.Validating += new System.ComponentModel.CancelEventHandler(this.View_Validating);
            // 
            // labelControl16
            // 
            this.labelControl16.Location = new System.Drawing.Point(235, 137);
            this.labelControl16.Name = "labelControl16";
            this.labelControl16.Size = new System.Drawing.Size(62, 13);
            this.labelControl16.TabIndex = 4;
            this.labelControl16.Text = "Документы:";
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(235, 82);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(91, 13);
            this.labelControl4.TabIndex = 4;
            this.labelControl4.Text = "Канал персонала:";
            // 
            // labelControl19
            // 
            this.labelControl19.Appearance.ForeColor = System.Drawing.Color.Red;
            this.labelControl19.Appearance.Options.UseForeColor = true;
            this.labelControl19.Location = new System.Drawing.Point(133, 137);
            this.labelControl19.Name = "labelControl19";
            this.labelControl19.Size = new System.Drawing.Size(6, 13);
            this.labelControl19.TabIndex = 10;
            this.labelControl19.Text = "*";
            // 
            // labelControl15
            // 
            this.labelControl15.Appearance.ForeColor = System.Drawing.Color.Red;
            this.labelControl15.Appearance.Options.UseForeColor = true;
            this.labelControl15.Location = new System.Drawing.Point(332, 82);
            this.labelControl15.Name = "labelControl15";
            this.labelControl15.Size = new System.Drawing.Size(6, 13);
            this.labelControl15.TabIndex = 10;
            this.labelControl15.Text = "*";
            // 
            // labelControl5
            // 
            this.labelControl5.Location = new System.Drawing.Point(6, 82);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(47, 13);
            this.labelControl5.TabIndex = 4;
            this.labelControl5.Text = "Уровень:";
            // 
            // labelControl17
            // 
            this.labelControl17.Location = new System.Drawing.Point(6, 137);
            this.labelControl17.Name = "labelControl17";
            this.labelControl17.Size = new System.Drawing.Size(115, 13);
            this.labelControl17.TabIndex = 4;
            this.labelControl17.Text = "Описание активности:";
            // 
            // cbTemplateChannel
            // 
            this.cbTemplateChannel.EditValue = "";
            this.cbTemplateChannel.Location = new System.Drawing.Point(235, 101);
            this.cbTemplateChannel.Name = "cbTemplateChannel";
            this.cbTemplateChannel.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbTemplateChannel.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("ChanelType", "Канал")});
            this.cbTemplateChannel.Properties.DisplayMember = "ChanelType";
            this.cbTemplateChannel.Properties.NullText = "<Не задано>";
            this.cbTemplateChannel.Properties.ShowFooter = false;
            this.cbTemplateChannel.Properties.ShowHeader = false;
            this.cbTemplateChannel.Properties.ValueMember = "ChanelType_id";
            this.cbTemplateChannel.Size = new System.Drawing.Size(210, 20);
            this.cbTemplateChannel.TabIndex = 1;
            this.cbTemplateChannel.Validating += new System.ComponentModel.CancelEventHandler(this.View_Validating);
            // 
            // labelControl14
            // 
            this.labelControl14.Appearance.ForeColor = System.Drawing.Color.Red;
            this.labelControl14.Appearance.Options.UseForeColor = true;
            this.labelControl14.Location = new System.Drawing.Point(59, 82);
            this.labelControl14.Name = "labelControl14";
            this.labelControl14.Size = new System.Drawing.Size(6, 13);
            this.labelControl14.TabIndex = 10;
            this.labelControl14.Text = "*";
            // 
            // checkApproved
            // 
            this.checkApproved.Location = new System.Drawing.Point(784, 7);
            this.checkApproved.Name = "checkApproved";
            this.checkApproved.Properties.Caption = "Шаблон утвержден для использования";
            this.checkApproved.Size = new System.Drawing.Size(234, 19);
            this.checkApproved.TabIndex = 15;
            // 
            // groupControlGroup
            // 
            this.groupControlGroup.Controls.Add(this.checkControlInActiveWaveThR);
            this.groupControlGroup.Controls.Add(this.checkControlAllowFromThR);
            this.groupControlGroup.Controls.Add(this.checkControlCascadeEdit);
            this.groupControlGroup.Location = new System.Drawing.Point(655, 400);
            this.groupControlGroup.Name = "groupControlGroup";
            this.groupControlGroup.Size = new System.Drawing.Size(363, 110);
            this.groupControlGroup.TabIndex = 12;
            this.groupControlGroup.Text = "Формирование контрольной группы ТТ:";
            // 
            // checkControlInActiveWaveThR
            // 
            this.checkControlInActiveWaveThR.Location = new System.Drawing.Point(6, 79);
            this.checkControlInActiveWaveThR.Name = "checkControlInActiveWaveThR";
            this.checkControlInActiveWaveThR.Properties.Caption = "Все ТТ из адрески должны быть в активной волне Th.R";
            this.checkControlInActiveWaveThR.Size = new System.Drawing.Size(322, 19);
            this.checkControlInActiveWaveThR.TabIndex = 8;
            // 
            // checkControlAllowFromThR
            // 
            this.checkControlAllowFromThR.Location = new System.Drawing.Point(6, 54);
            this.checkControlAllowFromThR.Name = "checkControlAllowFromThR";
            this.checkControlAllowFromThR.Properties.Caption = "Разрешено вносить ТТ  в адреску не из фильтра Th.R";
            this.checkControlAllowFromThR.Size = new System.Drawing.Size(305, 19);
            this.checkControlAllowFromThR.TabIndex = 8;
            // 
            // checkControlCascadeEdit
            // 
            this.checkControlCascadeEdit.Location = new System.Drawing.Point(6, 30);
            this.checkControlCascadeEdit.Name = "checkControlCascadeEdit";
            this.checkControlCascadeEdit.Properties.Caption = "Каскадное редактирование адрески";
            this.checkControlCascadeEdit.Size = new System.Drawing.Size(234, 19);
            this.checkControlCascadeEdit.TabIndex = 8;
            // 
            // TemplateDetailsCommon
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.xtraScrollableControl1);
            this.Name = "TemplateDetailsCommon";
            this.Size = new System.Drawing.Size(1022, 597);
            this.xtraScrollableControl1.ResumeLayout(false);
            this.xtraScrollableControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupPeriods)).EndInit();
            this.groupPeriods.ResumeLayout(false);
            this.groupPeriods.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spinPeriodsMonthCheck.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkPeriodsFromTR.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupPosition)).EndInit();
            this.groupPosition.ResumeLayout(false);
            this.groupPosition.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbPositionExecutor.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbPositionControlFact.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbPositionControlGroup.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbPositionApprover.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbPositionCreator.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbPositionTargetManager.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupPromoGroup)).EndInit();
            this.groupPromoGroup.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.checkPromoInActiveWaveThR.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkPromoAllowFromThR.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkPromoCascadeEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupKPK)).EndInit();
            this.groupKPK.ResumeLayout(false);
            this.groupKPK.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkKPKConnectionDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkKPKShow.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoKPKTemplateDescription.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupMain)).EndInit();
            this.groupMain.ResumeLayout(false);
            this.groupMain.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ebTemplateName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbTemplateType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkCannotModify.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoTemplateDescription.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbTemplateLevel.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbTemplateChannel.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkApproved.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControlGroup)).EndInit();
            this.groupControlGroup.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.checkControlInActiveWaveThR.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkControlAllowFromThR.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkControlCascadeEdit.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.XtraScrollableControl xtraScrollableControl1;
        private DevExpress.XtraEditors.GroupControl groupPeriods;
        private Logica.Reports.BaseReportControl.CommonControls.DateCombo dateActiveFromMonth;
        private Logica.Reports.BaseReportControl.CommonControls.DateCombo dateActiveToMonth;
        private Logica.Reports.BaseReportControl.CommonControls.DateCombo dateActiveToDay;
        private Logica.Reports.BaseReportControl.CommonControls.DateCombo dateFirstToYear;
        private Logica.Reports.BaseReportControl.CommonControls.DateCombo dateFirstToMonth;
        private Logica.Reports.BaseReportControl.CommonControls.DateCombo dateSecondToYear;
        private Logica.Reports.BaseReportControl.CommonControls.DateCombo dateSecondFromYear;
        private Logica.Reports.BaseReportControl.CommonControls.DateCombo dateSecondToMonth;
        private Logica.Reports.BaseReportControl.CommonControls.DateCombo dateFirstFromYear;
        private Logica.Reports.BaseReportControl.CommonControls.DateCombo dateSecondFromMonth;
        private Logica.Reports.BaseReportControl.CommonControls.DateCombo dateSecondToDay;
        private Logica.Reports.BaseReportControl.CommonControls.DateCombo dateFirstToDay;
        private Logica.Reports.BaseReportControl.CommonControls.DateCombo dateSecondFromDay;
        private Logica.Reports.BaseReportControl.CommonControls.DateCombo dateFirstFromMonth;
        private Logica.Reports.BaseReportControl.CommonControls.DateCombo dateFirstFromDay;
        private Logica.Reports.BaseReportControl.CommonControls.DateCombo dateActiveFromDay;
        private DevExpress.XtraEditors.SpinEdit spinPeriodsMonthCheck;
        private DevExpress.XtraEditors.LabelControl lblPeriods4;
        private DevExpress.XtraEditors.CheckEdit checkPeriodsFromTR;
        private DevExpress.XtraEditors.LabelControl lblPeriods6;
        private DevExpress.XtraEditors.LabelControl lblPeriods9;
        private DevExpress.XtraEditors.LabelControl lblPeriods10;
        private DevExpress.XtraEditors.LabelControl lblPeriods7;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LabelControl lblPeriods2;
        private DevExpress.XtraEditors.LabelControl lblPeriods3;
        private DevExpress.XtraEditors.LabelControl lblPeriods8;
        private DevExpress.XtraEditors.LabelControl lblPeriods5;
        private DevExpress.XtraEditors.LabelControl lblPeriods1;
        private DevExpress.XtraEditors.LabelControl lblDCRName;
        private DevExpress.XtraEditors.GroupControl groupPosition;
        private DevExpress.XtraEditors.LabelControl labelControl11;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.LabelControl lblPositionExecutor;
        private DevExpress.XtraEditors.LabelControl lblPositionControlGroup;
        private DevExpress.XtraEditors.LabelControl lblPositionControlFact;
        private DevExpress.XtraEditors.LabelControl lblPositionApprover;
        private DevExpress.XtraEditors.LabelControl lblPositionTargetManager;
        private DevExpress.XtraEditors.LabelControl lblPositionCreator;
        private DevExpress.XtraEditors.LookUpEdit cbPositionExecutor;
        private DevExpress.XtraEditors.LookUpEdit cbPositionControlFact;
        private DevExpress.XtraEditors.LookUpEdit cbPositionControlGroup;
        private DevExpress.XtraEditors.LookUpEdit cbPositionApprover;
        private DevExpress.XtraEditors.LookUpEdit cbPositionCreator;
        private DevExpress.XtraEditors.LookUpEdit cbPositionTargetManager;
		private DevExpress.XtraEditors.GroupControl groupPromoGroup;
        private DevExpress.XtraEditors.CheckEdit checkPromoInActiveWaveThR;
        private DevExpress.XtraEditors.CheckEdit checkPromoAllowFromThR;
        private DevExpress.XtraEditors.CheckEdit checkPromoCascadeEdit;
        private DevExpress.XtraEditors.GroupControl groupKPK;
        private DevExpress.XtraEditors.CheckEdit checkKPKConnectionDate;
        private DevExpress.XtraEditors.CheckEdit checkKPKShow;
        private DevExpress.XtraEditors.LabelControl lblKPKTemplateDescription;
        private DevExpress.XtraEditors.MemoEdit memoKPKTemplateDescription;
        private DevExpress.XtraEditors.GroupControl groupMain;
        private Logica.Reports.BaseReportControl.CommonControls.FileUploader documentUploader;
        private DevExpress.XtraEditors.CheckEdit checkCannotModify;
        private DevExpress.XtraEditors.MemoEdit memoTemplateDescription;
        private DevExpress.XtraEditors.TextEdit ebTemplateName;
        private DevExpress.XtraEditors.LookUpEdit cbTemplateLevel;
        private DevExpress.XtraEditors.LookUpEdit cbTemplateChannel;
        private DevExpress.XtraEditors.LookUpEdit cbTemplateType;
        private DevExpress.XtraEditors.CheckEdit checkApproved;
		private DevExpress.XtraEditors.GroupControl groupControlGroup;
		private DevExpress.XtraEditors.CheckEdit checkControlInActiveWaveThR;
        private DevExpress.XtraEditors.CheckEdit checkControlAllowFromThR;
        private DevExpress.XtraEditors.CheckEdit checkControlCascadeEdit;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.LabelControl labelControl14;
        private DevExpress.XtraEditors.LabelControl labelControl13;
        private DevExpress.XtraEditors.LabelControl labelControl12;
        private DevExpress.XtraEditors.LabelControl labelControl19;
        private DevExpress.XtraEditors.LabelControl labelControl18;
        private DevExpress.XtraEditors.LabelControl labelControl15;
        private DevExpress.XtraEditors.LabelControl labelControl16;
        private DevExpress.XtraEditors.LabelControl labelControl17;

    }
}
