﻿namespace SoftServe.Reports.MPTemplates
{
    partial class TemplateDetailsGoals
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TemplateDetailsGoals));
            this.layoutControl2 = new DevExpress.XtraLayout.LayoutControl();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.checkEdit1 = new DevExpress.XtraEditors.CheckEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit1 = new DevExpress.XtraEditors.TextEdit();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.checkEdit2 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit3 = new DevExpress.XtraEditors.CheckEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.checkEdit5 = new DevExpress.XtraEditors.CheckEdit();
            this.groupControl3 = new DevExpress.XtraEditors.GroupControl();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.lookUpEdit3 = new DevExpress.XtraEditors.LookUpEdit();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton3 = new DevExpress.XtraEditors.SimpleButton();
            this.lookUpEdit4 = new DevExpress.XtraEditors.LookUpEdit();
            this.checkEdit6 = new DevExpress.XtraEditors.CheckEdit();
            this.simpleButton4 = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.xtraScrollableControl = new DevExpress.XtraEditors.XtraScrollableControl();
            this.splitContainerControl2 = new DevExpress.XtraEditors.SplitContainerControl();
            this.splitContainerKPI = new DevExpress.XtraEditors.SplitContainerControl();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.listGoals = new DevExpress.XtraEditors.ListBoxControl();
            this.groupAims = new DevExpress.XtraEditors.GroupControl();
            this.checkShowAll = new DevExpress.XtraEditors.CheckEdit();
            this.labelEdit = new DevExpress.XtraEditors.LabelControl();
            this.textCurAimName = new DevExpress.XtraEditors.TextEdit();
            this.panelGoalConf = new DevExpress.XtraEditors.PanelControl();
            this.lcpFactsCalcWay = new DevExpress.XtraEditors.LookUpEdit();
            this.lcpPeriodCalk = new DevExpress.XtraEditors.LookUpEdit();
            this.lblFactCalcway = new DevExpress.XtraEditors.LabelControl();
            this.lblPeriodCalc = new DevExpress.XtraEditors.LabelControl();
            this.checkCannotChangeSKU = new DevExpress.XtraEditors.CheckEdit();
            this.checkCannotChangeKPI = new DevExpress.XtraEditors.CheckEdit();
            this.groupTragetSplit = new DevExpress.XtraEditors.GroupControl();
            this.checkAllowMultipleGoals = new DevExpress.XtraEditors.CheckEdit();
            this.checkSplitByPOCType = new DevExpress.XtraEditors.CheckEdit();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.cbSplitByPOCType = new DevExpress.XtraEditors.LookUpEdit();
            this.cbSplitByTime = new DevExpress.XtraEditors.LookUpEdit();
            this.groupGoalsPlanning = new DevExpress.XtraEditors.GroupControl();
            this.checkUseAimValue = new DevExpress.XtraEditors.CheckEdit();
            this.checkAccPerfOfTargetVal = new DevExpress.XtraEditors.CheckEdit();
            this.checkReversExecution = new DevExpress.XtraEditors.CheckEdit();
            this.simpleEdit = new DevExpress.XtraEditors.SimpleButton();
            this.cbVisitChecked = new DevExpress.XtraEditors.LookUpEdit();
            this.simpleSave = new DevExpress.XtraEditors.SimpleButton();
            this.simpleDelete = new DevExpress.XtraEditors.SimpleButton();
            this.cbGoalType = new DevExpress.XtraEditors.LookUpEdit();
            this.checkUseInCalculations = new DevExpress.XtraEditors.CheckEdit();
            this.simpleAdd = new DevExpress.XtraEditors.SimpleButton();
            this.labelVisitChecked = new DevExpress.XtraEditors.LabelControl();
            this.labelGoalType = new DevExpress.XtraEditors.LabelControl();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.groupQPQDigit = new DevExpress.XtraEditors.GroupControl();
            this.qpqTreeDigit = new Logica.Reports.BaseReportControl.CommonControls.QPQTree.QPQTree();
            this.groupQPQLogic = new DevExpress.XtraEditors.GroupControl();
            this.qpqTreeLogic = new Logica.Reports.BaseReportControl.CommonControls.QPQTree.QPQTree();
            this.groupSKU = new DevExpress.XtraEditors.GroupControl();
            this.skuTreeCtrl = new Logica.Reports.BaseReportControl.CommonControls.SKUTree();
            this.groupConditionsList = new DevExpress.XtraEditors.GroupControl();
            this.treeKPI = new Logica.Reports.BaseReportControl.CommonControls.KPITree();
            this.groupEffectiveness = new DevExpress.XtraEditors.GroupControl();
            this.chlbIPTRList = new DevExpress.XtraEditors.CheckedListBoxControl();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).BeginInit();
            this.layoutControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).BeginInit();
            this.groupControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit6.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            this.xtraScrollableControl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).BeginInit();
            this.splitContainerControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerKPI)).BeginInit();
            this.splitContainerKPI.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.listGoals)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupAims)).BeginInit();
            this.groupAims.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkShowAll.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textCurAimName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelGoalConf)).BeginInit();
            this.panelGoalConf.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lcpFactsCalcWay.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcpPeriodCalk.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkCannotChangeSKU.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkCannotChangeKPI.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupTragetSplit)).BeginInit();
            this.groupTragetSplit.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkAllowMultipleGoals.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkSplitByPOCType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbSplitByPOCType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbSplitByTime.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupGoalsPlanning)).BeginInit();
            this.groupGoalsPlanning.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkUseAimValue.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkAccPerfOfTargetVal.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkReversExecution.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbVisitChecked.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbGoalType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkUseInCalculations.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupQPQDigit)).BeginInit();
            this.groupQPQDigit.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupQPQLogic)).BeginInit();
            this.groupQPQLogic.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupSKU)).BeginInit();
            this.groupSKU.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupConditionsList)).BeginInit();
            this.groupConditionsList.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupEffectiveness)).BeginInit();
            this.groupEffectiveness.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chlbIPTRList)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControl2
            // 
            this.layoutControl2.AutoScroll = false;
            this.layoutControl2.Controls.Add(this.panelControl2);
            this.layoutControl2.Controls.Add(this.groupControl1);
            this.layoutControl2.Controls.Add(this.groupControl2);
            this.layoutControl2.Controls.Add(this.panelControl3);
            this.layoutControl2.Controls.Add(this.groupControl3);
            this.layoutControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl2.Location = new System.Drawing.Point(0, 0);
            this.layoutControl2.Margin = new System.Windows.Forms.Padding(4);
            this.layoutControl2.Name = "layoutControl2";
            this.layoutControl2.Root = this.layoutControlGroup2;
            this.layoutControl2.Size = new System.Drawing.Size(646, 679);
            this.layoutControl2.TabIndex = 0;
            this.layoutControl2.Text = "layoutControl1";
            // 
            // panelControl2
            // 
            this.panelControl2.Location = new System.Drawing.Point(106, 26);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(538, 651);
            this.panelControl2.TabIndex = 26;
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.checkEdit1);
            this.groupControl1.Controls.Add(this.labelControl1);
            this.groupControl1.Controls.Add(this.textEdit1);
            this.groupControl1.Location = new System.Drawing.Point(106, 2);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(538, 20);
            this.groupControl1.TabIndex = 24;
            this.groupControl1.Text = "Цели";
            // 
            // checkEdit1
            // 
            this.checkEdit1.Dock = System.Windows.Forms.DockStyle.Top;
            this.checkEdit1.Location = new System.Drawing.Point(2, 21);
            this.checkEdit1.Margin = new System.Windows.Forms.Padding(4);
            this.checkEdit1.Name = "checkEdit1";
            this.checkEdit1.Properties.Caption = "Показать все цели";
            this.checkEdit1.Size = new System.Drawing.Size(534, 19);
            this.checkEdit1.TabIndex = 29;
            this.checkEdit1.CheckedChanged += new System.EventHandler(this.checkShowAll_CheckedChanged);
            // 
            // labelControl1
            // 
            this.labelControl1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.labelControl1.Location = new System.Drawing.Point(2, -15);
            this.labelControl1.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(110, 13);
            this.labelControl1.TabIndex = 11;
            this.labelControl1.Text = "Редактируемая цель:";
            this.labelControl1.Visible = false;
            // 
            // textEdit1
            // 
            this.textEdit1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.textEdit1.Location = new System.Drawing.Point(2, -2);
            this.textEdit1.Name = "textEdit1";
            this.textEdit1.Size = new System.Drawing.Size(534, 20);
            this.textEdit1.TabIndex = 1;
            this.textEdit1.Visible = false;
            // 
            // groupControl2
            // 
            this.groupControl2.Controls.Add(this.checkEdit2);
            this.groupControl2.Controls.Add(this.checkEdit3);
            this.groupControl2.Controls.Add(this.labelControl2);
            this.groupControl2.Controls.Add(this.labelControl3);
            this.groupControl2.Location = new System.Drawing.Point(2, 2);
            this.groupControl2.Margin = new System.Windows.Forms.Padding(4);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Padding = new System.Windows.Forms.Padding(10);
            this.groupControl2.Size = new System.Drawing.Size(100, 20);
            this.groupControl2.TabIndex = 21;
            this.groupControl2.Text = "Разрезы планирования целей:";
            // 
            // checkEdit2
            // 
            this.checkEdit2.Location = new System.Drawing.Point(9, 43);
            this.checkEdit2.Margin = new System.Windows.Forms.Padding(4);
            this.checkEdit2.Name = "checkEdit2";
            this.checkEdit2.Properties.Caption = "Разрешить добавлять больше одной цели";
            this.checkEdit2.Size = new System.Drawing.Size(329, 19);
            this.checkEdit2.TabIndex = 10;
            this.checkEdit2.CheckedChanged += new System.EventHandler(this.checkAllowMultipleGoals_CheckedChanged);
            // 
            // checkEdit3
            // 
            this.checkEdit3.Location = new System.Drawing.Point(6, 121);
            this.checkEdit3.Margin = new System.Windows.Forms.Padding(4);
            this.checkEdit3.Name = "checkEdit3";
            this.checkEdit3.Properties.Caption = "Разбивка целей по типу ТТ";
            this.checkEdit3.Size = new System.Drawing.Size(237, 19);
            this.checkEdit3.TabIndex = 9;
            this.checkEdit3.Visible = false;
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(18, 160);
            this.labelControl2.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(145, 13);
            this.labelControl2.TabIndex = 6;
            this.labelControl2.Text = "По территории и персоналу:";
            this.labelControl2.Visible = false;
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(16, 82);
            this.labelControl3.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(62, 13);
            this.labelControl3.TabIndex = 6;
            this.labelControl3.Text = "По времени:";
            this.labelControl3.Visible = false;
            // 
            // panelControl3
            // 
            this.panelControl3.Controls.Add(this.checkEdit5);
            this.panelControl3.Location = new System.Drawing.Point(2, 54);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Padding = new System.Windows.Forms.Padding(10);
            this.panelControl3.Size = new System.Drawing.Size(100, 623);
            this.panelControl3.TabIndex = 23;
            // 
            // checkEdit5
            // 
            this.checkEdit5.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.checkEdit5.Location = new System.Drawing.Point(12, 592);
            this.checkEdit5.Margin = new System.Windows.Forms.Padding(4);
            this.checkEdit5.Name = "checkEdit5";
            this.checkEdit5.Properties.Caption = "Запретить менять условия в активностях";
            this.checkEdit5.Size = new System.Drawing.Size(76, 19);
            this.checkEdit5.TabIndex = 19;
            // 
            // groupControl3
            // 
            this.groupControl3.Controls.Add(this.simpleButton1);
            this.groupControl3.Controls.Add(this.lookUpEdit3);
            this.groupControl3.Controls.Add(this.simpleButton2);
            this.groupControl3.Controls.Add(this.simpleButton3);
            this.groupControl3.Controls.Add(this.lookUpEdit4);
            this.groupControl3.Controls.Add(this.checkEdit6);
            this.groupControl3.Controls.Add(this.simpleButton4);
            this.groupControl3.Controls.Add(this.labelControl4);
            this.groupControl3.Controls.Add(this.labelControl5);
            this.groupControl3.Location = new System.Drawing.Point(2, 26);
            this.groupControl3.Name = "groupControl3";
            this.groupControl3.Padding = new System.Windows.Forms.Padding(10);
            this.groupControl3.Size = new System.Drawing.Size(100, 24);
            this.groupControl3.TabIndex = 25;
            this.groupControl3.Text = "Планирование целей:";
            // 
            // simpleButton1
            // 
            this.simpleButton1.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton1.Image")));
            this.simpleButton1.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButton1.Location = new System.Drawing.Point(110, 34);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(40, 40);
            this.simpleButton1.TabIndex = 29;
            this.simpleButton1.ToolTip = "Редактировать цель";
            this.simpleButton1.Click += new System.EventHandler(this.simpleEdit_Click);
            // 
            // lookUpEdit3
            // 
            this.lookUpEdit3.Location = new System.Drawing.Point(100, 163);
            this.lookUpEdit3.Name = "lookUpEdit3";
            this.lookUpEdit3.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit3.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Text", "Период")});
            this.lookUpEdit3.Properties.DisplayMember = "Text";
            this.lookUpEdit3.Properties.ValueMember = "Id";
            this.lookUpEdit3.Size = new System.Drawing.Size(233, 20);
            this.lookUpEdit3.TabIndex = 27;
            // 
            // simpleButton2
            // 
            this.simpleButton2.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton2.Image")));
            this.simpleButton2.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButton2.Location = new System.Drawing.Point(64, 34);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(40, 40);
            this.simpleButton2.TabIndex = 28;
            this.simpleButton2.ToolTip = "Сохранить цель";
            this.simpleButton2.Click += new System.EventHandler(this.simpleSaveAim_Click);
            // 
            // simpleButton3
            // 
            this.simpleButton3.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton3.Image")));
            this.simpleButton3.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButton3.Location = new System.Drawing.Point(156, 34);
            this.simpleButton3.Name = "simpleButton3";
            this.simpleButton3.Size = new System.Drawing.Size(40, 40);
            this.simpleButton3.TabIndex = 25;
            this.simpleButton3.ToolTip = "Удалить цель";
            this.simpleButton3.Click += new System.EventHandler(this.simpleDeleteGoal_Click);
            // 
            // lookUpEdit4
            // 
            this.lookUpEdit4.Location = new System.Drawing.Point(100, 124);
            this.lookUpEdit4.Name = "lookUpEdit4";
            this.lookUpEdit4.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit4.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("AimType_Name", "Тип")});
            this.lookUpEdit4.Properties.DisplayMember = "AimType_Name";
            this.lookUpEdit4.Properties.ValueMember = "AimType_Id";
            this.lookUpEdit4.Size = new System.Drawing.Size(233, 20);
            this.lookUpEdit4.TabIndex = 26;
            this.lookUpEdit4.EditValueChanged += new System.EventHandler(this.cbGoalType_EditValueChanged);
            // 
            // checkEdit6
            // 
            this.checkEdit6.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.checkEdit6.Location = new System.Drawing.Point(12, -7);
            this.checkEdit6.Margin = new System.Windows.Forms.Padding(4);
            this.checkEdit6.Name = "checkEdit6";
            this.checkEdit6.Properties.Caption = "Учитывать  при выполнении";
            this.checkEdit6.Size = new System.Drawing.Size(76, 19);
            this.checkEdit6.TabIndex = 11;
            // 
            // simpleButton4
            // 
            this.simpleButton4.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton4.Image")));
            this.simpleButton4.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButton4.Location = new System.Drawing.Point(18, 34);
            this.simpleButton4.Name = "simpleButton4";
            this.simpleButton4.Size = new System.Drawing.Size(40, 40);
            this.simpleButton4.TabIndex = 0;
            this.simpleButton4.ToolTip = "Добавить цель";
            this.simpleButton4.Click += new System.EventHandler(this.simpleAddGoal_Click);
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(21, 166);
            this.labelControl4.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(41, 13);
            this.labelControl4.TabIndex = 24;
            this.labelControl4.Text = "Визиты:";
            // 
            // labelControl5
            // 
            this.labelControl5.Location = new System.Drawing.Point(21, 127);
            this.labelControl5.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(49, 13);
            this.labelControl5.TabIndex = 10;
            this.labelControl5.Text = "Тип цели:";
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup2.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem6,
            this.layoutControlItem7,
            this.layoutControlItem8,
            this.layoutControlItem9,
            this.layoutControlItem10});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "Root";
            this.layoutControlGroup2.OptionsItemText.TextToControlDistance = 5;
            this.layoutControlGroup2.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup2.Size = new System.Drawing.Size(646, 679);
            this.layoutControlGroup2.Text = "Root";
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.groupControl2;
            this.layoutControlItem6.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem6.Name = "layoutControlItem1";
            this.layoutControlItem6.Size = new System.Drawing.Size(104, 24);
            this.layoutControlItem6.Text = "layoutControlItem1";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem6.TextToControlDistance = 0;
            this.layoutControlItem6.TextVisible = false;
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.panelControl3;
            this.layoutControlItem7.CustomizationFormText = "layoutControlItem2";
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 52);
            this.layoutControlItem7.Name = "layoutControlItem2";
            this.layoutControlItem7.Size = new System.Drawing.Size(104, 627);
            this.layoutControlItem7.Text = "layoutControlItem2";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem7.TextToControlDistance = 0;
            this.layoutControlItem7.TextVisible = false;
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.groupControl1;
            this.layoutControlItem8.CustomizationFormText = "layoutControlItem4";
            this.layoutControlItem8.Location = new System.Drawing.Point(104, 0);
            this.layoutControlItem8.Name = "layoutControlItem4";
            this.layoutControlItem8.Size = new System.Drawing.Size(542, 24);
            this.layoutControlItem8.Text = "layoutControlItem4";
            this.layoutControlItem8.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem8.TextToControlDistance = 0;
            this.layoutControlItem8.TextVisible = false;
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.groupControl3;
            this.layoutControlItem9.CustomizationFormText = "layoutControlItem3";
            this.layoutControlItem9.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem9.Name = "layoutControlItem3";
            this.layoutControlItem9.Size = new System.Drawing.Size(104, 28);
            this.layoutControlItem9.Text = "layoutControlItem3";
            this.layoutControlItem9.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem9.TextToControlDistance = 0;
            this.layoutControlItem9.TextVisible = false;
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.Control = this.panelControl2;
            this.layoutControlItem10.CustomizationFormText = "layoutControlItem5";
            this.layoutControlItem10.Location = new System.Drawing.Point(104, 24);
            this.layoutControlItem10.Name = "layoutControlItem5";
            this.layoutControlItem10.Size = new System.Drawing.Size(542, 655);
            this.layoutControlItem10.Text = "layoutControlItem5";
            this.layoutControlItem10.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem10.TextToControlDistance = 0;
            this.layoutControlItem10.TextVisible = false;
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.splitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl1.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Size = new System.Drawing.Size(200, 100);
            this.splitContainerControl1.TabIndex = 0;
            // 
            // xtraScrollableControl
            // 
            this.xtraScrollableControl.AlwaysScrollActiveControlIntoView = false;
            this.xtraScrollableControl.AutoScrollMinSize = new System.Drawing.Size(1000, 400);
            this.xtraScrollableControl.Controls.Add(this.splitContainerControl2);
            this.xtraScrollableControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraScrollableControl.Location = new System.Drawing.Point(0, 0);
            this.xtraScrollableControl.Name = "xtraScrollableControl";
            this.xtraScrollableControl.Size = new System.Drawing.Size(1000, 539);
            this.xtraScrollableControl.TabIndex = 22;
            // 
            // splitContainerControl2
            // 
            this.splitContainerControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl2.Horizontal = false;
            this.splitContainerControl2.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl2.Margin = new System.Windows.Forms.Padding(2);
            this.splitContainerControl2.Name = "splitContainerControl2";
            this.splitContainerControl2.Panel1.Controls.Add(this.splitContainerKPI);
            this.splitContainerControl2.Panel1.Text = "Panel1";
            this.splitContainerControl2.Panel2.Text = "Panel2";
            this.splitContainerControl2.PanelVisibility = DevExpress.XtraEditors.SplitPanelVisibility.Panel1;
            this.splitContainerControl2.Size = new System.Drawing.Size(1000, 539);
            this.splitContainerControl2.SplitterPosition = 694;
            this.splitContainerControl2.TabIndex = 21;
            this.splitContainerControl2.Text = "splitContainerControl2";
            // 
            // splitContainerKPI
            // 
            this.splitContainerKPI.AlwaysScrollActiveControlIntoView = false;
            this.splitContainerKPI.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.splitContainerKPI.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerKPI.Location = new System.Drawing.Point(0, 0);
            this.splitContainerKPI.Margin = new System.Windows.Forms.Padding(2);
            this.splitContainerKPI.Name = "splitContainerKPI";
            this.splitContainerKPI.Panel1.Controls.Add(this.layoutControl1);
            this.splitContainerKPI.Panel1.MinSize = 565;
            this.splitContainerKPI.Panel1.Text = "Panel1";
            this.splitContainerKPI.Panel2.Controls.Add(this.groupQPQDigit);
            this.splitContainerKPI.Panel2.Controls.Add(this.groupQPQLogic);
            this.splitContainerKPI.Panel2.Controls.Add(this.groupSKU);
            this.splitContainerKPI.Panel2.Controls.Add(this.groupConditionsList);
            this.splitContainerKPI.Panel2.Controls.Add(this.groupEffectiveness);
            this.splitContainerKPI.Panel2.MinSize = 250;
            this.splitContainerKPI.Panel2.Padding = new System.Windows.Forms.Padding(2);
            this.splitContainerKPI.Panel2.Text = "Panel2";
            this.splitContainerKPI.Size = new System.Drawing.Size(1000, 539);
            this.splitContainerKPI.SplitterPosition = 572;
            this.splitContainerKPI.TabIndex = 20;
            this.splitContainerKPI.Text = "splitContainerKPI";
            // 
            // layoutControl1
            // 
            this.layoutControl1.AllowDrop = true;
            this.layoutControl1.AutoScroll = false;
            this.layoutControl1.Controls.Add(this.panelControl1);
            this.layoutControl1.Controls.Add(this.groupAims);
            this.layoutControl1.Controls.Add(this.panelGoalConf);
            this.layoutControl1.Controls.Add(this.groupTragetSplit);
            this.layoutControl1.Controls.Add(this.groupGoalsPlanning);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Margin = new System.Windows.Forms.Padding(0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(371, 234, 250, 350);
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(572, 535);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.listGoals);
            this.panelControl1.Location = new System.Drawing.Point(287, 116);
            this.panelControl1.Margin = new System.Windows.Forms.Padding(0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(283, 417);
            this.panelControl1.TabIndex = 27;
            // 
            // listGoals
            // 
            this.listGoals.DisplayMember = "Text";
            this.listGoals.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listGoals.HorizontalScrollbar = true;
            this.listGoals.Location = new System.Drawing.Point(2, 2);
            this.listGoals.Margin = new System.Windows.Forms.Padding(2);
            this.listGoals.Name = "listGoals";
            this.listGoals.Size = new System.Drawing.Size(279, 413);
            this.listGoals.SortOrder = System.Windows.Forms.SortOrder.Ascending;
            this.listGoals.TabIndex = 0;
            this.listGoals.ValueMember = "Id";
            this.listGoals.SelectedIndexChanged += new System.EventHandler(this.listGoals_SelectedIndexChanged);
            // 
            // groupAims
            // 
            this.groupAims.Controls.Add(this.checkShowAll);
            this.groupAims.Controls.Add(this.labelEdit);
            this.groupAims.Controls.Add(this.textCurAimName);
            this.groupAims.Location = new System.Drawing.Point(287, 2);
            this.groupAims.Margin = new System.Windows.Forms.Padding(0);
            this.groupAims.MaximumSize = new System.Drawing.Size(0, 110);
            this.groupAims.MinimumSize = new System.Drawing.Size(0, 110);
            this.groupAims.Name = "groupAims";
            this.groupAims.Padding = new System.Windows.Forms.Padding(8);
            this.groupAims.Size = new System.Drawing.Size(283, 110);
            this.groupAims.TabIndex = 25;
            this.groupAims.Text = "Цели:";
            // 
            // checkShowAll
            // 
            this.checkShowAll.Location = new System.Drawing.Point(9, 31);
            this.checkShowAll.Name = "checkShowAll";
            this.checkShowAll.Properties.Caption = "Показать все цели";
            this.checkShowAll.Size = new System.Drawing.Size(241, 19);
            this.checkShowAll.TabIndex = 29;
            this.checkShowAll.CheckedChanged += new System.EventHandler(this.checkShowAll_CheckedChanged);
            // 
            // labelEdit
            // 
            this.labelEdit.Location = new System.Drawing.Point(13, 64);
            this.labelEdit.Name = "labelEdit";
            this.labelEdit.Size = new System.Drawing.Size(79, 13);
            this.labelEdit.TabIndex = 11;
            this.labelEdit.Text = "Название цели:";
            this.labelEdit.Visible = false;
            // 
            // textCurAimName
            // 
            this.textCurAimName.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.textCurAimName.Location = new System.Drawing.Point(10, 80);
            this.textCurAimName.Margin = new System.Windows.Forms.Padding(2, 4, 2, 2);
            this.textCurAimName.Name = "textCurAimName";
            this.textCurAimName.Size = new System.Drawing.Size(263, 20);
            this.textCurAimName.TabIndex = 1;
            this.textCurAimName.Visible = false;
            // 
            // panelGoalConf
            // 
            this.panelGoalConf.Controls.Add(this.lcpFactsCalcWay);
            this.panelGoalConf.Controls.Add(this.lcpPeriodCalk);
            this.panelGoalConf.Controls.Add(this.lblFactCalcway);
            this.panelGoalConf.Controls.Add(this.lblPeriodCalc);
            this.panelGoalConf.Controls.Add(this.checkCannotChangeSKU);
            this.panelGoalConf.Controls.Add(this.checkCannotChangeKPI);
            this.panelGoalConf.Location = new System.Drawing.Point(2, 364);
            this.panelGoalConf.Margin = new System.Windows.Forms.Padding(0);
            this.panelGoalConf.Name = "panelGoalConf";
            this.panelGoalConf.Padding = new System.Windows.Forms.Padding(8);
            this.panelGoalConf.Size = new System.Drawing.Size(281, 169);
            this.panelGoalConf.TabIndex = 24;
            // 
            // lcpFactsCalcWay
            // 
            this.lcpFactsCalcWay.Location = new System.Drawing.Point(168, 43);
            this.lcpFactsCalcWay.Name = "lcpFactsCalcWay";
            this.lcpFactsCalcWay.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lcpFactsCalcWay.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Text", "CalcType"),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 20, DevExpress.Utils.FormatType.Numeric, "", false, DevExpress.Utils.HorzAlignment.Default)});
            this.lcpFactsCalcWay.Properties.DisplayMember = "Text";
            this.lcpFactsCalcWay.Properties.DropDownRows = 2;
            this.lcpFactsCalcWay.Properties.PopupFormMinSize = new System.Drawing.Size(120, 0);
            this.lcpFactsCalcWay.Properties.PopupWidth = 120;
            this.lcpFactsCalcWay.Properties.ShowFooter = false;
            this.lcpFactsCalcWay.Properties.ShowHeader = false;
            this.lcpFactsCalcWay.Properties.ValueMember = "Id";
            this.lcpFactsCalcWay.Size = new System.Drawing.Size(100, 20);
            this.lcpFactsCalcWay.TabIndex = 32;
            // 
            // lcpPeriodCalk
            // 
            this.lcpPeriodCalk.Location = new System.Drawing.Point(168, 12);
            this.lcpPeriodCalk.Name = "lcpPeriodCalk";
            this.lcpPeriodCalk.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lcpPeriodCalk.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Text", "Month", 20, DevExpress.Utils.FormatType.Numeric, "", true, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 20, DevExpress.Utils.FormatType.Numeric, "", false, DevExpress.Utils.HorzAlignment.Default)});
            this.lcpPeriodCalk.Properties.DisplayMember = "Text";
            this.lcpPeriodCalk.Properties.PopupFormMinSize = new System.Drawing.Size(100, 0);
            this.lcpPeriodCalk.Properties.PopupWidth = 100;
            this.lcpPeriodCalk.Properties.ShowFooter = false;
            this.lcpPeriodCalk.Properties.ShowHeader = false;
            this.lcpPeriodCalk.Properties.ValueMember = "Id";
            this.lcpPeriodCalk.Size = new System.Drawing.Size(100, 20);
            this.lcpPeriodCalk.TabIndex = 31;
            // 
            // lblFactCalcway
            // 
            this.lblFactCalcway.Location = new System.Drawing.Point(12, 46);
            this.lblFactCalcway.Name = "lblFactCalcway";
            this.lblFactCalcway.Size = new System.Drawing.Size(132, 13);
            this.lblFactCalcway.TabIndex = 30;
            this.lblFactCalcway.Text = "Способ подсчета фактов:";
            // 
            // lblPeriodCalc
            // 
            this.lblPeriodCalc.Location = new System.Drawing.Point(12, 15);
            this.lblPeriodCalc.Name = "lblPeriodCalc";
            this.lblPeriodCalc.Size = new System.Drawing.Size(151, 13);
            this.lblPeriodCalc.TabIndex = 28;
            this.lblPeriodCalc.Text = "Расчетный период (месяцев):";
            // 
            // checkCannotChangeSKU
            // 
            this.checkCannotChangeSKU.AutoSizeInLayoutControl = true;
            this.checkCannotChangeSKU.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.checkCannotChangeSKU.Location = new System.Drawing.Point(10, 121);
            this.checkCannotChangeSKU.Name = "checkCannotChangeSKU";
            this.checkCannotChangeSKU.Properties.Caption = "Запретить менять СКЮ в активностях";
            this.checkCannotChangeSKU.Size = new System.Drawing.Size(261, 19);
            this.checkCannotChangeSKU.TabIndex = 19;
            // 
            // checkCannotChangeKPI
            // 
            this.checkCannotChangeKPI.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.checkCannotChangeKPI.Location = new System.Drawing.Point(10, 140);
            this.checkCannotChangeKPI.Name = "checkCannotChangeKPI";
            this.checkCannotChangeKPI.Properties.Caption = "Запретить менять условия в активностях";
            this.checkCannotChangeKPI.Size = new System.Drawing.Size(261, 19);
            this.checkCannotChangeKPI.TabIndex = 19;
            // 
            // groupTragetSplit
            // 
            this.groupTragetSplit.Controls.Add(this.checkAllowMultipleGoals);
            this.groupTragetSplit.Controls.Add(this.checkSplitByPOCType);
            this.groupTragetSplit.Controls.Add(this.labelControl6);
            this.groupTragetSplit.Controls.Add(this.labelControl7);
            this.groupTragetSplit.Controls.Add(this.cbSplitByPOCType);
            this.groupTragetSplit.Controls.Add(this.cbSplitByTime);
            this.groupTragetSplit.Location = new System.Drawing.Point(2, 2);
            this.groupTragetSplit.Margin = new System.Windows.Forms.Padding(0);
            this.groupTragetSplit.MaximumSize = new System.Drawing.Size(281, 110);
            this.groupTragetSplit.MinimumSize = new System.Drawing.Size(281, 110);
            this.groupTragetSplit.Name = "groupTragetSplit";
            this.groupTragetSplit.Padding = new System.Windows.Forms.Padding(8);
            this.groupTragetSplit.Size = new System.Drawing.Size(281, 110);
            this.groupTragetSplit.TabIndex = 22;
            this.groupTragetSplit.Text = "Разрезы планирования целей:";
            // 
            // checkAllowMultipleGoals
            // 
            this.checkAllowMultipleGoals.Location = new System.Drawing.Point(9, 31);
            this.checkAllowMultipleGoals.MinimumSize = new System.Drawing.Size(262, 0);
            this.checkAllowMultipleGoals.Name = "checkAllowMultipleGoals";
            this.checkAllowMultipleGoals.Properties.Caption = "Разрешить добавлять больше одной цели";
            this.checkAllowMultipleGoals.Size = new System.Drawing.Size(262, 19);
            this.checkAllowMultipleGoals.TabIndex = 10;
            this.checkAllowMultipleGoals.CheckedChanged += new System.EventHandler(this.checkAllowMultipleGoals_CheckedChanged);
            // 
            // checkSplitByPOCType
            // 
            this.checkSplitByPOCType.Location = new System.Drawing.Point(2, 112);
            this.checkSplitByPOCType.Name = "checkSplitByPOCType";
            this.checkSplitByPOCType.Properties.Caption = "Разбивка целей по типу ТТ";
            this.checkSplitByPOCType.Size = new System.Drawing.Size(278, 19);
            this.checkSplitByPOCType.TabIndex = 9;
            this.checkSplitByPOCType.Visible = false;
            // 
            // labelControl6
            // 
            this.labelControl6.Location = new System.Drawing.Point(14, 149);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(145, 13);
            this.labelControl6.TabIndex = 6;
            this.labelControl6.Text = "По территории и персоналу:";
            this.labelControl6.Visible = false;
            // 
            // labelControl7
            // 
            this.labelControl7.Location = new System.Drawing.Point(12, 85);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(62, 13);
            this.labelControl7.TabIndex = 6;
            this.labelControl7.Text = "По времени:";
            this.labelControl7.Visible = false;
            // 
            // cbSplitByPOCType
            // 
            this.cbSplitByPOCType.Location = new System.Drawing.Point(4, 141);
            this.cbSplitByPOCType.Name = "cbSplitByPOCType";
            this.cbSplitByPOCType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbSplitByPOCType.Properties.DisplayMember = "TargetRegion_Name";
            this.cbSplitByPOCType.Properties.NullText = "<Не задано>";
            this.cbSplitByPOCType.Properties.ShowFooter = false;
            this.cbSplitByPOCType.Properties.ShowHeader = false;
            this.cbSplitByPOCType.Properties.ValueMember = "TargetRegion_ID";
            this.cbSplitByPOCType.Size = new System.Drawing.Size(245, 20);
            this.cbSplitByPOCType.TabIndex = 5;
            this.cbSplitByPOCType.Visible = false;
            // 
            // cbSplitByTime
            // 
            this.cbSplitByTime.Location = new System.Drawing.Point(4, 77);
            this.cbSplitByTime.Name = "cbSplitByTime";
            this.cbSplitByTime.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbSplitByTime.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TargetPeriod_Name", "Период")});
            this.cbSplitByTime.Properties.DisplayMember = "TargetPeriod_Name";
            this.cbSplitByTime.Properties.NullText = "<Не задано>";
            this.cbSplitByTime.Properties.ValueMember = "TargetPeriod_ID";
            this.cbSplitByTime.Size = new System.Drawing.Size(245, 20);
            this.cbSplitByTime.TabIndex = 5;
            this.cbSplitByTime.Visible = false;
            // 
            // groupGoalsPlanning
            // 
            this.groupGoalsPlanning.Controls.Add(this.checkUseAimValue);
            this.groupGoalsPlanning.Controls.Add(this.checkAccPerfOfTargetVal);
            this.groupGoalsPlanning.Controls.Add(this.checkReversExecution);
            this.groupGoalsPlanning.Controls.Add(this.simpleEdit);
            this.groupGoalsPlanning.Controls.Add(this.cbVisitChecked);
            this.groupGoalsPlanning.Controls.Add(this.simpleSave);
            this.groupGoalsPlanning.Controls.Add(this.simpleDelete);
            this.groupGoalsPlanning.Controls.Add(this.cbGoalType);
            this.groupGoalsPlanning.Controls.Add(this.checkUseInCalculations);
            this.groupGoalsPlanning.Controls.Add(this.simpleAdd);
            this.groupGoalsPlanning.Controls.Add(this.labelVisitChecked);
            this.groupGoalsPlanning.Controls.Add(this.labelGoalType);
            this.groupGoalsPlanning.Location = new System.Drawing.Point(2, 116);
            this.groupGoalsPlanning.Margin = new System.Windows.Forms.Padding(2);
            this.groupGoalsPlanning.MaximumSize = new System.Drawing.Size(0, 244);
            this.groupGoalsPlanning.MinimumSize = new System.Drawing.Size(0, 244);
            this.groupGoalsPlanning.Name = "groupGoalsPlanning";
            this.groupGoalsPlanning.Padding = new System.Windows.Forms.Padding(8);
            this.groupGoalsPlanning.Size = new System.Drawing.Size(281, 244);
            this.groupGoalsPlanning.TabIndex = 27;
            this.groupGoalsPlanning.Text = "Планирование целей:";
            // 
            // checkUseAimValue
            // 
            this.checkUseAimValue.Location = new System.Drawing.Point(10, 142);
            this.checkUseAimValue.Name = "checkUseAimValue";
            this.checkUseAimValue.Properties.Caption = "Использовать целевое значение";
            this.checkUseAimValue.Size = new System.Drawing.Size(261, 19);
            this.checkUseAimValue.TabIndex = 32;
            this.checkUseAimValue.CheckedChanged += new System.EventHandler(this.checkUseAimValue_CheckedChanged);
            // 
            // checkAccPerfOfTargetVal
            // 
            this.checkAccPerfOfTargetVal.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.checkAccPerfOfTargetVal.Location = new System.Drawing.Point(10, 177);
            this.checkAccPerfOfTargetVal.Name = "checkAccPerfOfTargetVal";
            this.checkAccPerfOfTargetVal.Properties.Caption = "Учитывать выполнение по целевому значению";
            this.checkAccPerfOfTargetVal.Size = new System.Drawing.Size(261, 19);
            this.checkAccPerfOfTargetVal.TabIndex = 31;
            // 
            // checkReversExecution
            // 
            this.checkReversExecution.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.checkReversExecution.Location = new System.Drawing.Point(10, 196);
            this.checkReversExecution.Name = "checkReversExecution";
            this.checkReversExecution.Properties.Caption = "Обратное выполнение";
            this.checkReversExecution.Size = new System.Drawing.Size(261, 19);
            this.checkReversExecution.TabIndex = 30;
            // 
            // simpleEdit
            // 
            this.simpleEdit.Image = ((System.Drawing.Image)(resources.GetObject("simpleEdit.Image")));
            this.simpleEdit.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleEdit.Location = new System.Drawing.Point(82, 31);
            this.simpleEdit.Margin = new System.Windows.Forms.Padding(2);
            this.simpleEdit.Name = "simpleEdit";
            this.simpleEdit.Size = new System.Drawing.Size(30, 32);
            this.simpleEdit.TabIndex = 29;
            this.simpleEdit.ToolTip = "Редактировать цель";
            this.simpleEdit.Click += new System.EventHandler(this.simpleEdit_Click);
            // 
            // cbVisitChecked
            // 
            this.cbVisitChecked.Location = new System.Drawing.Point(71, 115);
            this.cbVisitChecked.Margin = new System.Windows.Forms.Padding(2);
            this.cbVisitChecked.Name = "cbVisitChecked";
            this.cbVisitChecked.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbVisitChecked.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Text", "Период")});
            this.cbVisitChecked.Properties.DisplayMember = "Text";
            this.cbVisitChecked.Properties.ValueMember = "Id";
            this.cbVisitChecked.Size = new System.Drawing.Size(198, 20);
            this.cbVisitChecked.TabIndex = 27;
            // 
            // simpleSave
            // 
            this.simpleSave.Image = ((System.Drawing.Image)(resources.GetObject("simpleSave.Image")));
            this.simpleSave.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleSave.Location = new System.Drawing.Point(46, 31);
            this.simpleSave.Margin = new System.Windows.Forms.Padding(2);
            this.simpleSave.Name = "simpleSave";
            this.simpleSave.Size = new System.Drawing.Size(30, 32);
            this.simpleSave.TabIndex = 28;
            this.simpleSave.ToolTip = "Сохранить цель";
            this.simpleSave.Click += new System.EventHandler(this.simpleSaveAim_Click);
            // 
            // simpleDelete
            // 
            this.simpleDelete.Image = ((System.Drawing.Image)(resources.GetObject("simpleDelete.Image")));
            this.simpleDelete.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleDelete.Location = new System.Drawing.Point(117, 31);
            this.simpleDelete.Margin = new System.Windows.Forms.Padding(2);
            this.simpleDelete.Name = "simpleDelete";
            this.simpleDelete.Size = new System.Drawing.Size(30, 32);
            this.simpleDelete.TabIndex = 25;
            this.simpleDelete.ToolTip = "Удалить цель";
            this.simpleDelete.Click += new System.EventHandler(this.simpleDeleteGoal_Click);
            // 
            // cbGoalType
            // 
            this.cbGoalType.Location = new System.Drawing.Point(71, 84);
            this.cbGoalType.Margin = new System.Windows.Forms.Padding(2);
            this.cbGoalType.Name = "cbGoalType";
            this.cbGoalType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbGoalType.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("AimType_Name", "Тип")});
            this.cbGoalType.Properties.DisplayMember = "AimType_Name";
            this.cbGoalType.Properties.ValueMember = "AimType_Id";
            this.cbGoalType.Size = new System.Drawing.Size(198, 20);
            this.cbGoalType.TabIndex = 26;
            this.cbGoalType.EditValueChanged += new System.EventHandler(this.cbGoalType_EditValueChanged);
            this.cbGoalType.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(this.cbGoalType_EditValueChanging);
            // 
            // checkUseInCalculations
            // 
            this.checkUseInCalculations.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.checkUseInCalculations.Location = new System.Drawing.Point(10, 215);
            this.checkUseInCalculations.Name = "checkUseInCalculations";
            this.checkUseInCalculations.Properties.Caption = "Учитывать  при выполнении";
            this.checkUseInCalculations.Size = new System.Drawing.Size(261, 19);
            this.checkUseInCalculations.TabIndex = 11;
            // 
            // simpleAdd
            // 
            this.simpleAdd.Image = ((System.Drawing.Image)(resources.GetObject("simpleAdd.Image")));
            this.simpleAdd.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleAdd.Location = new System.Drawing.Point(12, 31);
            this.simpleAdd.Margin = new System.Windows.Forms.Padding(2);
            this.simpleAdd.Name = "simpleAdd";
            this.simpleAdd.Size = new System.Drawing.Size(30, 32);
            this.simpleAdd.TabIndex = 0;
            this.simpleAdd.ToolTip = "Добавить цель";
            this.simpleAdd.Click += new System.EventHandler(this.simpleAddGoal_Click);
            // 
            // labelVisitChecked
            // 
            this.labelVisitChecked.Location = new System.Drawing.Point(12, 118);
            this.labelVisitChecked.Name = "labelVisitChecked";
            this.labelVisitChecked.Size = new System.Drawing.Size(41, 13);
            this.labelVisitChecked.TabIndex = 24;
            this.labelVisitChecked.Text = "Визиты:";
            // 
            // labelGoalType
            // 
            this.labelGoalType.Location = new System.Drawing.Point(12, 86);
            this.labelGoalType.Name = "labelGoalType";
            this.labelGoalType.Size = new System.Drawing.Size(49, 13);
            this.labelGoalType.TabIndex = 10;
            this.labelGoalType.Text = "Тип цели:";
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem4,
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.layoutControlItem3,
            this.layoutControlItem5});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.OptionsItemText.TextToControlDistance = 5;
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup1.Size = new System.Drawing.Size(572, 535);
            this.layoutControlGroup1.Text = "layoutControlGroup1";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.groupTragetSplit;
            this.layoutControlItem4.CustomizationFormText = "layoutControlItem4";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(285, 114);
            this.layoutControlItem4.Text = "layoutControlItem4";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextToControlDistance = 0;
            this.layoutControlItem4.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.panelGoalConf;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 362);
            this.layoutControlItem1.MinSize = new System.Drawing.Size(104, 24);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(285, 173);
            this.layoutControlItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem1.Text = "layoutControlItem1";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.groupAims;
            this.layoutControlItem2.CustomizationFormText = "layoutControlItem2";
            this.layoutControlItem2.Location = new System.Drawing.Point(285, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(287, 114);
            this.layoutControlItem2.Text = "layoutControlItem2";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextToControlDistance = 0;
            this.layoutControlItem2.TextVisible = false;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.panelControl1;
            this.layoutControlItem3.CustomizationFormText = "layoutControlItem3";
            this.layoutControlItem3.Location = new System.Drawing.Point(285, 114);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(287, 421);
            this.layoutControlItem3.Text = "layoutControlItem3";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextToControlDistance = 0;
            this.layoutControlItem3.TextVisible = false;
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.groupGoalsPlanning;
            this.layoutControlItem5.CustomizationFormText = "layoutControlItem5";
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 114);
            this.layoutControlItem5.MaxSize = new System.Drawing.Size(0, 248);
            this.layoutControlItem5.MinSize = new System.Drawing.Size(24, 248);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(285, 248);
            this.layoutControlItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem5.Text = "layoutControlItem5";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem5.TextToControlDistance = 0;
            this.layoutControlItem5.TextVisible = false;
            // 
            // groupQPQDigit
            // 
            this.groupQPQDigit.Controls.Add(this.qpqTreeDigit);
            this.groupQPQDigit.Location = new System.Drawing.Point(174, 272);
            this.groupQPQDigit.Name = "groupQPQDigit";
            this.groupQPQDigit.Size = new System.Drawing.Size(102, 112);
            this.groupQPQDigit.TabIndex = 27;
            this.groupQPQDigit.Text = "Список условий активностей:";
            // 
            // qpqTreeDigit
            // 
            this.qpqTreeDigit.Dock = System.Windows.Forms.DockStyle.Fill;
            this.qpqTreeDigit.Location = new System.Drawing.Point(2, 21);
            this.qpqTreeDigit.Margin = new System.Windows.Forms.Padding(0);
            this.qpqTreeDigit.Name = "qpqTreeDigit";
            this.qpqTreeDigit.ReadOnly = false;
            this.qpqTreeDigit.SelectedStringIdList = null;
            this.qpqTreeDigit.Size = new System.Drawing.Size(98, 89);
            this.qpqTreeDigit.TabIndex = 0;
            // 
            // groupQPQLogic
            // 
            this.groupQPQLogic.Controls.Add(this.qpqTreeLogic);
            this.groupQPQLogic.Location = new System.Drawing.Point(50, 272);
            this.groupQPQLogic.Name = "groupQPQLogic";
            this.groupQPQLogic.Size = new System.Drawing.Size(102, 112);
            this.groupQPQLogic.TabIndex = 26;
            this.groupQPQLogic.Text = "Список условий активностей:";
            // 
            // qpqTreeLogic
            // 
            this.qpqTreeLogic.Dock = System.Windows.Forms.DockStyle.Fill;
            this.qpqTreeLogic.Location = new System.Drawing.Point(2, 21);
            this.qpqTreeLogic.Margin = new System.Windows.Forms.Padding(0);
            this.qpqTreeLogic.Name = "qpqTreeLogic";
            this.qpqTreeLogic.ReadOnly = false;
            this.qpqTreeLogic.SelectedStringIdList = null;
            this.qpqTreeLogic.Size = new System.Drawing.Size(98, 89);
            this.qpqTreeLogic.TabIndex = 0;
            // 
            // groupSKU
            // 
            this.groupSKU.Controls.Add(this.skuTreeCtrl);
            this.groupSKU.Location = new System.Drawing.Point(50, 169);
            this.groupSKU.Name = "groupSKU";
            this.groupSKU.Size = new System.Drawing.Size(102, 97);
            this.groupSKU.TabIndex = 25;
            this.groupSKU.Text = "Список СКЮ для целей по V продаж:";
            // 
            // skuTreeCtrl
            // 
            this.skuTreeCtrl.AllowedLevel = 0;
            this.skuTreeCtrl.AutoScroll = true;
            this.skuTreeCtrl.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(245)))), ((int)(((byte)(241)))));
            this.skuTreeCtrl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.skuTreeCtrl.Location = new System.Drawing.Point(2, 21);
            this.skuTreeCtrl.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.skuTreeCtrl.Name = "skuTreeCtrl";
            this.skuTreeCtrl.ReadOnly = false;
            this.skuTreeCtrl.Size = new System.Drawing.Size(98, 74);
            this.skuTreeCtrl.TabIndex = 2;
            // 
            // groupConditionsList
            // 
            this.groupConditionsList.Controls.Add(this.treeKPI);
            this.groupConditionsList.Location = new System.Drawing.Point(48, 71);
            this.groupConditionsList.Name = "groupConditionsList";
            this.groupConditionsList.Size = new System.Drawing.Size(102, 93);
            this.groupConditionsList.TabIndex = 24;
            this.groupConditionsList.Text = "Список условий активностей:";
            // 
            // treeKPI
            // 
            this.treeKPI.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treeKPI.Location = new System.Drawing.Point(2, 21);
            this.treeKPI.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.treeKPI.Name = "treeKPI";
            this.treeKPI.ReadOnly = false;
            this.treeKPI.Size = new System.Drawing.Size(98, 70);
            this.treeKPI.TabIndex = 19;
            // 
            // groupEffectiveness
            // 
            this.groupEffectiveness.Appearance.BackColor = System.Drawing.Color.White;
            this.groupEffectiveness.Appearance.ForeColor = System.Drawing.Color.White;
            this.groupEffectiveness.Appearance.Options.UseBackColor = true;
            this.groupEffectiveness.Appearance.Options.UseForeColor = true;
            this.groupEffectiveness.Controls.Add(this.chlbIPTRList);
            this.groupEffectiveness.Location = new System.Drawing.Point(46, 2);
            this.groupEffectiveness.Margin = new System.Windows.Forms.Padding(2);
            this.groupEffectiveness.Name = "groupEffectiveness";
            this.groupEffectiveness.Size = new System.Drawing.Size(104, 68);
            this.groupEffectiveness.TabIndex = 22;
            this.groupEffectiveness.Text = "Эффективность:";
            this.groupEffectiveness.UseDisabledStatePainter = false;
            // 
            // chlbIPTRList
            // 
            this.chlbIPTRList.CheckOnClick = true;
            this.chlbIPTRList.DisplayMember = "ClassName";
            this.chlbIPTRList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chlbIPTRList.Location = new System.Drawing.Point(2, 21);
            this.chlbIPTRList.Margin = new System.Windows.Forms.Padding(0);
            this.chlbIPTRList.Name = "chlbIPTRList";
            this.chlbIPTRList.Size = new System.Drawing.Size(100, 45);
            this.chlbIPTRList.TabIndex = 1;
            this.chlbIPTRList.ValueMember = "EquipmentClass_ID";
            this.chlbIPTRList.ItemCheck += new DevExpress.XtraEditors.Controls.ItemCheckEventHandler(this.chlbIPTRList_ItemCheck);
            // 
            // TemplateDetailsGoals
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.Controls.Add(this.xtraScrollableControl);
            this.Name = "TemplateDetailsGoals";
            this.Size = new System.Drawing.Size(1000, 539);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).EndInit();
            this.layoutControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            this.groupControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).EndInit();
            this.groupControl3.ResumeLayout(false);
            this.groupControl3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit6.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            this.xtraScrollableControl.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).EndInit();
            this.splitContainerControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerKPI)).EndInit();
            this.splitContainerKPI.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.listGoals)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupAims)).EndInit();
            this.groupAims.ResumeLayout(false);
            this.groupAims.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkShowAll.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textCurAimName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelGoalConf)).EndInit();
            this.panelGoalConf.ResumeLayout(false);
            this.panelGoalConf.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lcpFactsCalcWay.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcpPeriodCalk.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkCannotChangeSKU.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkCannotChangeKPI.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupTragetSplit)).EndInit();
            this.groupTragetSplit.ResumeLayout(false);
            this.groupTragetSplit.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkAllowMultipleGoals.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkSplitByPOCType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbSplitByPOCType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbSplitByTime.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupGoalsPlanning)).EndInit();
            this.groupGoalsPlanning.ResumeLayout(false);
            this.groupGoalsPlanning.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkUseAimValue.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkAccPerfOfTargetVal.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkReversExecution.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbVisitChecked.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbGoalType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkUseInCalculations.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupQPQDigit)).EndInit();
            this.groupQPQDigit.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupQPQLogic)).EndInit();
            this.groupQPQLogic.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupSKU)).EndInit();
            this.groupSKU.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupConditionsList)).EndInit();
            this.groupConditionsList.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupEffectiveness)).EndInit();
            this.groupEffectiveness.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chlbIPTRList)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControl2;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.CheckEdit checkEdit1;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.TextEdit textEdit1;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraEditors.CheckEdit checkEdit2;
        private DevExpress.XtraEditors.CheckEdit checkEdit3;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.PanelControl panelControl3;
        private DevExpress.XtraEditors.CheckEdit checkEdit5;
        private DevExpress.XtraEditors.GroupControl groupControl3;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit3;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
        private DevExpress.XtraEditors.SimpleButton simpleButton3;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit4;
        private DevExpress.XtraEditors.CheckEdit checkEdit6;
        private DevExpress.XtraEditors.SimpleButton simpleButton4;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DevExpress.XtraEditors.XtraScrollableControl xtraScrollableControl;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerKPI;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.ListBoxControl listGoals;
        private DevExpress.XtraEditors.GroupControl groupAims;
        private DevExpress.XtraEditors.CheckEdit checkShowAll;
        private DevExpress.XtraEditors.LabelControl labelEdit;
        private DevExpress.XtraEditors.TextEdit textCurAimName;
        private DevExpress.XtraEditors.PanelControl panelGoalConf;
        private DevExpress.XtraEditors.CheckEdit checkCannotChangeSKU;
        private DevExpress.XtraEditors.CheckEdit checkCannotChangeKPI;
        private DevExpress.XtraEditors.GroupControl groupTragetSplit;
        private DevExpress.XtraEditors.CheckEdit checkAllowMultipleGoals;
        private DevExpress.XtraEditors.CheckEdit checkSplitByPOCType;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.LookUpEdit cbSplitByPOCType;
        private DevExpress.XtraEditors.LookUpEdit cbSplitByTime;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraEditors.GroupControl groupSKU;
        private Logica.Reports.BaseReportControl.CommonControls.SKUTree skuTreeCtrl;
        private DevExpress.XtraEditors.GroupControl groupConditionsList;
        private Logica.Reports.BaseReportControl.CommonControls.KPITree treeKPI;
        private DevExpress.XtraEditors.GroupControl groupEffectiveness;
        private DevExpress.XtraEditors.CheckedListBoxControl chlbIPTRList;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraEditors.GroupControl groupGoalsPlanning;
        private DevExpress.XtraEditors.SimpleButton simpleEdit;
        private DevExpress.XtraEditors.LookUpEdit cbVisitChecked;
        private DevExpress.XtraEditors.SimpleButton simpleSave;
        private DevExpress.XtraEditors.SimpleButton simpleDelete;
        private DevExpress.XtraEditors.LookUpEdit cbGoalType;
        private DevExpress.XtraEditors.CheckEdit checkUseInCalculations;
        private DevExpress.XtraEditors.SimpleButton simpleAdd;
        private DevExpress.XtraEditors.LabelControl labelVisitChecked;
        private DevExpress.XtraEditors.LabelControl labelGoalType;
        private DevExpress.XtraEditors.GroupControl groupQPQLogic;
        private Logica.Reports.BaseReportControl.CommonControls.QPQTree.QPQTree qpqTreeLogic;
        private DevExpress.XtraEditors.GroupControl groupQPQDigit;
        private Logica.Reports.BaseReportControl.CommonControls.QPQTree.QPQTree qpqTreeDigit;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraEditors.CheckEdit checkReversExecution;
        private DevExpress.XtraEditors.LabelControl lblPeriodCalc;
        private DevExpress.XtraEditors.LabelControl lblFactCalcway;
        private DevExpress.XtraEditors.CheckEdit checkUseAimValue;
        private DevExpress.XtraEditors.CheckEdit checkAccPerfOfTargetVal;
        private DevExpress.XtraEditors.LookUpEdit lcpFactsCalcWay;
        private DevExpress.XtraEditors.LookUpEdit lcpPeriodCalk;
    }
}
