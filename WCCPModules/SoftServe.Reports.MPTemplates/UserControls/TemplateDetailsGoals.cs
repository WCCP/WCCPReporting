﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlTypes;
using System.Linq;
using System.Windows.Forms;
using DevExpress.Data.PLinq.Helpers;
using DevExpress.XtraEditors;
using Logica.Reports.BaseReportControl;
using Logica.Reports.BaseReportControl.Utility;
using SoftServe.Reports.MPTemplates.Utility;

namespace SoftServe.Reports.MPTemplates
{
    /// <summary>
    /// Describes Goals tab
    /// </summary>
    public partial class TemplateDetailsGoals : CommonBaseControl
    {
        /// <summary>
        /// Gets the selected split by POC type id.
        /// </summary>
        /// <value>The selected split by POC type id.</value>
        public SplitPOCByType SelectedSplitByPOCType
        {
            get
            {
                return (SplitPOCByType)ConvertEx.ToInt(cbSplitByPOCType.EditValue);
            }
        }

        #region Fields
        private bool WasDataLoaded = false;
        private bool isLogicTreeFirstDraw = true;
        private bool isDigitTreeFirstDraw = true;

        internal TemplateDetailsControl MainControl;

        /// <summary>
        /// List of all aims
        /// </summary>
        private List<DataTransferObjectAim> Aims;

        /// <summary>
        /// SKU tree template
        /// </summary>
        private DataTable SKUtreeTable;

        /// <summary>
        /// KPI tree template
        /// </summary>
        private DataTable KPItreeTable;

        /// <summary>
        /// Questionnary logic tree template
        /// </summary>
        private DataTable QPQLogicTreeTable;

        /// <summary>
        /// Questionnary digit tree template
        /// </summary>
        private DataTable QPQDigitTreeTable;

        /// <summary>
        /// List of aims showen in ListGoal list
        /// </summary>
        List<ComboBoxItem> AimList;

        /// <summary>
        /// Current aim version
        /// </summary>
        private DataTransferObjectAim aimCur;

        /// <summary>
        /// Original aim version
        /// </summary>
        private DataTransferObjectAim aimOriginal;

        /// <summary>
        /// Allows to skip listGoals_SelectedIndexChanged event
        /// </summary>
        private bool isNotChangeIndexEvent = false;

        /// <summary>
        /// Allows to skip cbGoalType_EditValueChanged event
        /// </summary>
        private bool isNotComboEvent = true;

        private List<ComboBoxItem> lFactTypeSource;

        private List<ComboBoxItem> lFactCalcMonth; 


        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="TemplateDetailsGoals"/> class.
        /// </summary>
        public TemplateDetailsGoals()
        {
            InitializeComponent();

            chlbIPTRList.DataSource = DataProvider.GetIPTRList();

            Aims = new List<DataTransferObjectAim>();

            SKUtreeTable = DataProvider.GetSKUTree(-1);
            KPItreeTable = DataProvider.GetKPITree(-1);
            QPQLogicTreeTable = DataProvider.GetQPQTree(-1, 1);
            QPQDigitTreeTable = DataProvider.GetQPQTree(-1, 2);

            AimList = new List<ComboBoxItem>();

            aimCur = new DataTransferObjectAim();
            aimOriginal = new DataTransferObjectAim();
        }

        #endregion Constructors

        #region Public methods
        /// <summary>
        /// Loads the data.
        /// </summary>
        /// <param name="templateId">The template id.</param>
        /// <param name="dtDetails">DataTable details</param>
        public override void LoadData(int templateId, DataTable dtDetails)
        {
            base.LoadData(templateId, dtDetails);
            //listGoals.SelectedIndexChanged -= listGoals_SelectedIndexChanged;

            if (WasDataLoaded)
            {
                isNotChangeIndexEvent = listGoals.SelectedIndex > -1;
                //isNotComboEvent = true;
            }

            TemplateId = templateId;

            Aims = new List<DataTransferObjectAim>();

            LoadAllAimsFromDB();

            if (!WasDataLoaded)
            {
                LoadDropDowns();
                checkShowAll.Checked = false;
            }

            RefreshAimList(checkShowAll.Checked, (GoalType)cbGoalType.EditValue);

            AllFieldsData = dtDetails;

            RedrawButtonAccess();

            LoadAim(AimRecordState.Unknown, ConvertEx.ToInt(cbGoalType.EditValue), false);

            cbGoalType.Properties.ReadOnly = false;
            checkShowAll.Properties.ReadOnly = false;

            if (MainData.AccessLevel <= (int)AccessLevelType.ReadOnly)
            {
                checkCannotChangeSKU.Enabled = false;
                checkCannotChangeKPI.Enabled = false;
            }

            if (MainData.ActivityTemplate == TemplateType.PriceActivities)
            {
                cbGoalType.Enabled = false;
                checkShowAll.Enabled = false;
                skuTreeCtrl.AllowedLevel = 2;

                textCurAimName.Visible = false;
                labelEdit.Visible = false;
            }

            //listGoals.SelectedIndex = -1;
            //listGoals.SelectedIndexChanged += listGoals_SelectedIndexChanged;

            WasDataLoaded = true;
        }

        /// <summary>
        /// Gets all fields.
        /// </summary>
        /// <param name="dto">The dto</param>
        /// <param name="templateId">The template id</param>
        public void GetAllFields(TemplateDetailsDTO dto, int templateId)
        {
            dto.FIELD_COMMON_TEMPLATE_ID = templateId;

            dto.FIELD_GOALS_DISABLE_CHANGE_SKU = checkCannotChangeSKU.Checked;
            dto.FIELD_GOALS_DISABLE_CHANGE_KPI = checkCannotChangeKPI.Checked;
            dto.FIELD_GOALS_DISABLE_CHANGE_TYPE = checkSplitByPOCType.Checked;

            dto.FIELD_GOALS_ALLOW_MULTIPLE_GOALS = checkAllowMultipleGoals.Checked;

            dto.FIELD_GOALS_TARGET_PERIOD_TYPE = ConvertEx.ToInt(cbSplitByTime.EditValue);
            dto.FIELD_GOALS_TARGET_REGION_TYPE = ConvertEx.ToInt(cbSplitByPOCType.EditValue);
        }

        /// <summary>
        /// Save all aims to DB
        /// </summary>
        public void SaveAllAims(int templateId)
        {
            // in case there is an unsaved aim; all data is valid
            GetAllAimFields();

            //XtraMessageBox.Show("cur: "+ aimCur.Id.ToString() +" "+aimCur.Name + " " + aimCur.State.ToString()+"; orig: " + aimOriginal.Id.ToString() +" "+aimOriginal.Name + " " + aimOriginal.State.ToString());

            if (IsModified())
            {
                if (aimCur.State == AimRecordState.New)
                {
                    labelEdit.Visible = false;
                    textCurAimName.Visible = false;

                    SaveAimToList(aimCur);
                }
                else
                {
                    SaveAimToList(aimCur);
                }
            }
            // end in case there is an unsaved aim

            //XtraMessageBox.Show("cur: " + aimCur.Id.ToString() + " " + aimCur.Name + " " + aimCur.State.ToString() + "; orig: " + aimOriginal.Id.ToString() + " " + aimOriginal.Name + " " + aimOriginal.State.ToString());

            foreach (DataTransferObjectAim aim in Aims)// new aims that have been deleled
            {
                if (aim.State == AimRecordState.Deleted && aim.Id == -1)
                {
                    aim.State = AimRecordState.Unknown;
                }
            }

            foreach (DataTransferObjectAim aim in Aims)
            {
                if (aim.State == AimRecordState.Updated)
                {
                    SaveAimToDB(aim, templateId);
                }
                else
                {
                    if (aim.State == AimRecordState.Deleted)
                    {
                        DataProvider.DeleteAim(aim.Id);
                    }
                }
            }

            //XtraMessageBox.Show("cur: " + aimCur.Id.ToString() + " " + aimCur.Name + " " + aimCur.State.ToString() + "; orig: " + aimOriginal.Id.ToString() + " " + aimOriginal.Name + " " + aimOriginal.State.ToString());

            aimCur.Copy(aimOriginal);

            //XtraMessageBox.Show("cur: " + aimCur.Id.ToString() + " " + aimCur.Name + " " + aimCur.State.ToString() + "; orig: " + aimOriginal.Id.ToString() + " " + aimOriginal.Name + " " + aimOriginal.State.ToString());
        }

        /// <summary>
        /// Determines whether there were changes on this tab
        /// </summary>
        public bool WasGoalTabModified
        {
            get
            {
                bool wasModified;
                int countAimsChanged = 0;
                foreach (DataTransferObjectAim aim in Aims)
                {
                    if (aim.State == AimRecordState.Updated || aim.State == AimRecordState.Deleted)
                    {
                        countAimsChanged++;
                    }
                }

                wasModified = countAimsChanged > 0 ? true : false;

                GetAllAimFields();

                wasModified = wasModified || IsModified();

                return wasModified;
            }
        }

        /// <summary>
        /// Overrides base validate method and returns metadata of validate result. 
        /// </summary>
        /// <returns>whether is valid</returns>
        internal override ValidateMetadata ValidateData()
        {
            ValidateMetadata baseResult = base.ValidateData();

            GetAllAimFields();

            string mess = string.Empty;

            baseResult.IsCorrect = aimCur.State == AimRecordState.Unknown || IsAimCurValid(out mess);

            if (!baseResult.IsCorrect)
            {
                baseResult.Message = string.Format("\t\t\t-{0}", mess);
            }

            return baseResult;
        }

        #endregion Public methods

        #region IPTR methods
        /// <summary>
        /// Gets the selected IPTR id list.
        /// </summary>
        /// <value>The selected IPTR id list.</value>
        private string SelectedIPTRList
        {
            get
            {
                return chlbIPTRList.CheckedIndices.Count > 0 ? ConvertEx.ToString(chlbIPTRList.GetItemValue(chlbIPTRList.CheckedIndices[0])) : string.Empty;
            }
        }

        /// <summary>
        /// Loads the IPTR list
        /// </summary>
        private void LoadIPTRList(string Id)
        {
            chlbIPTRList.UnCheckAll();

            int id = int.Parse(Id);

            for (int i = 0; i < (chlbIPTRList.DataSource as DataTable).Rows.Count; i++)
            {
                if (ConvertEx.ToInt(chlbIPTRList.GetItemValue(i)) == id)
                {
                    chlbIPTRList.SetItemChecked(i, true);
                }
            }
        }

        #endregion IPTR methods

        #region Event handling
        /// <summary>
        /// Allows to check only one node in IPTR list
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void chlbIPTRList_ItemCheck(object sender, DevExpress.XtraEditors.Controls.ItemCheckEventArgs e)
        {
            if (e.State != CheckState.Checked)
            {
                return;
            }

            CheckedListBoxControl lb = sender as CheckedListBoxControl;

            for (int i = 0; i < lb.ItemCount; i++)
            {
                if (i != e.Index)
                {
                    lb.SetItemChecked(i, false);
                }
            }
        }

        /// <summary>
        /// Change access to conrols after checking MultAims checkbox
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void checkAllowMultipleGoals_CheckedChanged(object sender, EventArgs e)
        {
            RedrawButtonAccess();
        }

        /// <summary>
        /// Determines whether all aims should be shown
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void checkShowAll_CheckedChanged(object sender, EventArgs e)
        {
            int index;

            if (checkShowAll.Checked)
            {
                cbGoalType.Enabled = false;

                if (listGoals.SelectedIndex > -1)
                {
                    isNotChangeIndexEvent = true;/////////////////////
                    index = ConvertEx.ToInt(listGoals.SelectedValue);
                    RefreshAimList(true, (GoalType)aimCur.Type);
                    isNotChangeIndexEvent = true;/////////////////////
                    listGoals.SelectedIndex = GetIdByValue(index);
                }
                else
                {
                    RefreshAimList(true, (GoalType)cbGoalType.EditValue);
                }
            }
            else
            {
                if (!(MainData.ActivityTemplate == TemplateType.PriceActivities))
                {
                    cbGoalType.Enabled = true;
                }

                if (listGoals.SelectedIndex > -1)
                {
                    isNotChangeIndexEvent = true;/////////////////////
                    index = ConvertEx.ToInt(listGoals.SelectedValue);
                    isNotChangeIndexEvent = true;/////////////////////
                    RefreshAimList(false, (GoalType)aimCur.Type);
                    listGoals.SelectedIndex = GetIdByValue(index);
                }
                else
                {
                    RefreshAimList(false, (GoalType)cbGoalType.EditValue);
                }
            }

            RedrawButtonAccess();
        }

        /// <summary>
        /// Implements aims reloading
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cbGoalType_EditValueChanging(object sender, DevExpress.XtraEditors.Controls.ChangingEventArgs e)
        {
            if (isNotComboEvent)
            {
                isNotComboEvent = false;
                return;
            }

                if (!checkShowAll.Checked&&simpleSave.Enabled)
                {
                        //in case it is modified
                    if (IsModified())
                    {
                        DialogResult result = XtraMessageBox.Show(Resource.ConfirmSaveAim, Resource.Warning,
                            MessageBoxButtons.YesNoCancel);

                        if (aimCur.State != AimRecordState.New)
                        {
                            isNotChangeIndexEvent = true;
                        }

                        if (result == DialogResult.Yes)
                        {
                            string mes;
                            if (!IsAimCurValid(out mes))
                            {
                                XtraMessageBox.Show(mes, Resource.Warning, MessageBoxButtons.OK);
                                e.Cancel = true;
                            }
                            else
                            {
                                SaveAimToList(aimCur); // save modified aim
                            }
                        }

                        if (result == DialogResult.Cancel)
                        {
                            e.Cancel = true;
                        }
                    }
                    //end in case it is modified
            }
        }

        /// <summary>
        /// Implements aims reloading
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cbGoalType_EditValueChanged(object sender, EventArgs e)
        {
            if (!checkShowAll.Checked)
            {
                RefreshAimList(false, (GoalType)cbGoalType.EditValue);
                LoadAim(AimRecordState.Unknown, (int)cbGoalType.EditValue, false);
            }

            labelEdit.Visible = false;
            textCurAimName.Visible = false;

            RedrawButtonAccess();
        }

        /// <summary>
        /// Implements trees reloading
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void listGoals_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (isNotChangeIndexEvent)
            {
                isNotChangeIndexEvent = false;
                return;
            }
            bool isModified = IsModified();
           
            if (!isModified)
            {
                LoadAim(Aims[ConvertEx.ToInt(listGoals.SelectedValue)], true);

                if (checkShowAll.Checked)
                {
                    cbGoalType.EditValue = aimCur.Type;
                }
            }

            //in case it is modified
            if (isModified)
            {
                DialogResult result = XtraMessageBox.Show(Resource.ConfirmSaveAim, Resource.Warning, MessageBoxButtons.YesNoCancel);
                if (result == DialogResult.Yes)
                {
                    string mes;
                    if (!IsAimCurValid(out mes))
                    {
                        XtraMessageBox.Show(mes, Resource.Warning, MessageBoxButtons.OK);

                        isNotChangeIndexEvent = true;
                        listGoals.SelectedIndex = GetIdByValue(Aims.FindIndex(aim => aimOriginal.Equals(aim)));

                        return;
                    }

                    int index = ConvertEx.ToInt(listGoals.SelectedValue);

                    // save modified aim
                    SaveAimToList(aimCur);
                    isNotChangeIndexEvent = true;
                    RefreshAimList(checkShowAll.Checked, (GoalType)cbGoalType.EditValue);
                    // end save modified aim

                    isNotChangeIndexEvent = true;
                    listGoals.SelectedIndex = GetIdByValue(index);

                    LoadAim(Aims[index], true);
                }

                if (result == DialogResult.No)
                {
                    LoadAim(Aims[ConvertEx.ToInt(listGoals.SelectedValue)], true);

                    if (checkShowAll.Checked)
                    {
                        cbGoalType.EditValue = aimCur.Type;
                    }
                }

                if (result == DialogResult.Cancel)
                {
                    isNotChangeIndexEvent = true;
                    listGoals.SelectedIndex = GetIdByValue(Aims.FindIndex(aim => aimOriginal.Equals(aim)));

                    return;
                }
            }
            //end in case it is modified

            textCurAimName.Visible = false;
            labelEdit.Visible = false;

            RedrawButtonAccess();
        }

        /// <summary>
        /// Adds aim
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void simpleAddGoal_Click(object sender, EventArgs e)
        {

            textCurAimName.Visible = true;
            labelEdit.Visible = true;
            //simpleSave.Enabled = true;

            if (listGoals.SelectedIndex > -1)
            {
                isNotChangeIndexEvent = true;
                listGoals.SelectedIndex = -1;
            }

            LoadAim(AimRecordState.New, ConvertEx.ToInt(cbGoalType.EditValue), true);

            switch ((GoalType)ConvertEx.ToInt(cbGoalType.EditValue))
            {
                case GoalType.SalesVolume:
                    textCurAimName.Text = "Новая цель по объему продаж ";
                    break;
                case GoalType.Merchandising:
                    textCurAimName.Text = "Новая цель по мерчендайзингу ";
                    break;
                case GoalType.Effectiveness:
                    textCurAimName.Text = "Новая цель по эффективности оборудования ";
                    break;
                case GoalType.QuestionnaireLogic:
                    textCurAimName.Text = "Новая цель по логическому вопросу ";
                    break;
                case GoalType.QuestionnaireDigit:
                    textCurAimName.Text = "Новая цель по числовому вопросу ";
                    break;
                case GoalType.InBevDoorCount:
                    textCurAimName.Text = "Новая цель по количеству дверей InBev ";
                    break;
                    case GoalType.MustStock:
                    textCurAimName.Text = "Новая цель по наличию MUST Stock ";
                    break;
            }

            textCurAimName.Text += AimList.Count.ToString();

            aimOriginal.Name = string.Copy(textCurAimName.Text);

            RedrawButtonAccess();
            AllowEdit();
        }

        /// <summary>
        /// Deletes aim
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void simpleDeleteGoal_Click(object sender, EventArgs e)
        {
            DialogResult result = XtraMessageBox.Show(Resource.ConfirmDeleteAim, Resource.Warning, MessageBoxButtons.YesNo);
            if (result == DialogResult.Yes)
            {
                aimOriginal.State = AimRecordState.Deleted;

                int aimType = ConvertEx.ToInt(cbGoalType.EditValue);

                RefreshAimList(checkShowAll.Checked, (GoalType)aimType);

                LoadAim(AimRecordState.Unknown, aimType, false);

                if (checkShowAll.Checked)
                {
                    cbGoalType.EditValue = aimType;
                }
            }

            RedrawButtonAccess();
        }

        /// <summary>
        /// Saves aim
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void simpleSaveAim_Click(object sender, EventArgs e)
        {
            GetAllAimFields();

            string mes;
            if (!IsAimCurValid(out mes))
            {
                XtraMessageBox.Show(mes,
                                    Resource.Warning, MessageBoxButtons.OK);

                return;
            }

            if (IsModified())
            {
                string mes2;
                if (!IsAimCurValid(out mes2))
                {
                    XtraMessageBox.Show(mes2,
                                        Resource.Warning, MessageBoxButtons.OK);

                    return;
                }

                if (aimCur.State == AimRecordState.New)
                {
                    SaveAimToList(aimCur);

                    RefreshAimList(checkShowAll.Checked, (GoalType)cbGoalType.EditValue);

                    isNotChangeIndexEvent = true;
                    listGoals.SelectedIndex = GetIdByValue(Aims.Count - 1);
                    LoadAim(Aims[ConvertEx.ToInt(listGoals.SelectedValue)], true);
                }
                else
                {
                    int index = listGoals.SelectedIndex;
                    SaveAimToList(aimCur);

                    RefreshAimList(checkShowAll.Checked, (GoalType)cbGoalType.EditValue);

                    isNotChangeIndexEvent = true;
                    listGoals.SelectedIndex = index;
                    LoadAim(Aims[ConvertEx.ToInt(listGoals.SelectedValue)], true);
                }
            }


            labelEdit.Visible = false;
            textCurAimName.Visible = false;

            RedrawTreeWindow((GoalType)aimCur.Type, true);
            RedrawAimControls((GoalType)aimCur.Type, false);

            RedrawButtonAccess();
        }

        /// <summary>
        /// Alows aim editing
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void simpleEdit_Click(object sender, EventArgs e)
        {
            AllowEdit();
        }

        /// <summary>
        /// Shows other checkboxes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void checkUseAimValue_CheckedChanged(object sender, EventArgs e)
        {
            checkReversExecution.Visible = checkAccPerfOfTargetVal.Visible = checkUseAimValue.Checked;

            if (!checkUseAimValue.Checked)
            {
                checkReversExecution.Checked = checkAccPerfOfTargetVal.Checked = false;
            }
        }
        
        /// <summary>
        /// Gets new version of aim list
        /// </summary>
        /// 
        /// <param name="showAll">show all aim or only of the current type</param>
        /// <param name="aimType">aim type</param>
        private void RefreshAimList(bool showAll, GoalType aimType)
        {
            AimList = new List<ComboBoxItem>();

            int i = 0;

            if (showAll)
            {
                foreach (DataTransferObjectAim aim in Aims)
                {
                    if (aim.State != AimRecordState.Deleted)
                    {
                        AimList.Add(new ComboBoxItem(i, aim.Name));
                    }

                    i++;
                }
            }
            else
            {
                foreach (DataTransferObjectAim aim in Aims)
                {
                    if (aim.State != AimRecordState.Deleted && aim.Type == ConvertEx.ToInt(aimType))
                    {
                        AimList.Add(new ComboBoxItem(i, aim.Name));
                    }

                    i++;
                }
            }

            listGoals.SelectedIndexChanged -= listGoals_SelectedIndexChanged;
            listGoals.BeginUpdate();
            try
            {
                listGoals.DataSource = AimList;
            }
            catch (Exception e)
            { }
            listGoals.EndUpdate();

            listGoals.SelectedIndex = -1;
            listGoals.SelectedIndexChanged += listGoals_SelectedIndexChanged;
        }

        /// <summary>
        /// Change tree window according to the aim type
        /// </summary>
        /// <param name="aimType">aim type</param>
        /// <param name="visible">if the tree should be visible</param>
        private void RedrawTreeWindow(GoalType aimType, bool visible)
        {
            switch (aimType)
            {
                case GoalType.SalesVolume:
                    groupSKU.Dock = DockStyle.Fill;
                    groupSKU.Visible = true;
                    groupSKU.Text = "Список СКЮ для целей по V продаж:";
                    skuTreeCtrl.Visible = visible;
                    skuTreeCtrl.ReadOnly = true;

                    groupConditionsList.Visible = false;
                    groupEffectiveness.Visible = false;
                    groupQPQDigit.Visible = false;
                    groupQPQLogic.Visible = false;
                    break;

                case GoalType.Merchandising:
                    groupConditionsList.Dock = DockStyle.Fill;
                    groupConditionsList.Visible = true;
                    treeKPI.Visible = visible;
                    treeKPI.ReadOnly = true;

                    groupSKU.Visible = false;
                    groupEffectiveness.Visible = false;
                    groupQPQDigit.Visible = false;
                    groupQPQLogic.Visible = false;
                    break;

                case GoalType.Effectiveness:
                    groupEffectiveness.Dock = DockStyle.Fill;
                    groupEffectiveness.Visible = true;
                    chlbIPTRList.Visible = visible;
                    chlbIPTRList.Enabled = false;

                    groupConditionsList.Visible = false;
                    groupSKU.Visible = false;
                    groupQPQDigit.Visible = false;
                    groupQPQLogic.Visible = false;
                    break;

                case GoalType.OnInvoice:
                    groupSKU.Visible = false;
                    groupConditionsList.Visible = false;
                    groupEffectiveness.Visible = false;
                    groupQPQDigit.Visible = false;
                    groupQPQLogic.Visible = false;
                    break;

                case GoalType.QuestionnaireLogic:
                    groupQPQLogic.Dock = DockStyle.Fill;
                    groupQPQLogic.Visible = true;
                    qpqTreeLogic.Visible = visible;
                    qpqTreeLogic.ReadOnly = true;

                    groupEffectiveness.Visible = false;
                    groupConditionsList.Visible = false;
                    groupSKU.Visible = false;
                    groupQPQDigit.Visible = false;
                    break;

                case GoalType.QuestionnaireDigit:
                    groupQPQDigit.Dock = DockStyle.Fill;
                    groupQPQDigit.Visible = true;
                    qpqTreeDigit.Visible = visible;
                    qpqTreeDigit.ReadOnly = true;

                    groupEffectiveness.Visible = false;
                    groupConditionsList.Visible = false;
                    groupSKU.Visible = false;
                    groupQPQLogic.Visible = false;
                    break;

                case GoalType.InBevDoorCount:
                    groupQPQDigit.Visible = false;
                    qpqTreeDigit.Visible = false;
                    groupEffectiveness.Visible = false;
                    groupConditionsList.Visible = false;
                    groupSKU.Visible = false;
                    groupQPQLogic.Visible = false;
                    break;

                case GoalType.MustStock:
                    groupSKU.Dock = DockStyle.Fill;
                    groupSKU.Visible = true;
                    groupSKU.Text = "Список СКЮ для целей по наличию MUST Stock:";
                    skuTreeCtrl.Visible = visible;
                    skuTreeCtrl.ReadOnly = true;

                    groupConditionsList.Visible = false;
                    groupEffectiveness.Visible = false;
                    groupQPQDigit.Visible = false;
                    groupQPQLogic.Visible = false;
                    break;
            }
        }

        /// <summary>
        /// Change aim cintrls according to the aim type
        /// </summary>
        /// <param name="aimType">aim type</param>
        /// <param name="enabled">if the tree should be enabled</param>
        private void RedrawAimControls(GoalType aimType, bool enabled)
        {
            checkUseInCalculations.Checked = aimCur.IsUsedInCalculations;
            checkUseInCalculations.Enabled = enabled;

            checkReversExecution.Checked = aimCur.IsInReverseExecution;
            checkReversExecution.Enabled = enabled;

            checkUseAimValue.Checked = aimCur.IsUseAimValue;
            checkUseAimValue.Enabled = enabled;

            checkAccPerfOfTargetVal.Checked = aimCur.IsAccPerfOfTargetVal;
            checkAccPerfOfTargetVal.Enabled = enabled;
            checkAccPerfOfTargetVal.Visible = checkUseAimValue.Checked;


            cbVisitChecked.EditValue = aimCur.IsPeriodCalc ? 1 : 0;
            cbVisitChecked.Enabled = enabled;
            labelVisitChecked.Visible = enabled;
            cbVisitChecked.Visible = aimType == GoalType.Merchandising || aimType == GoalType.QuestionnaireDigit ||
                    aimType == GoalType.QuestionnaireLogic;
            checkReversExecution.Visible = aimType == GoalType.QuestionnaireDigit ||
                                           (aimType == GoalType.Merchandising && checkUseAimValue.Checked);
            labelVisitChecked.Visible = cbVisitChecked.Visible;

            lblPeriodCalc.Visible = lblFactCalcway.Visible =
            lcpPeriodCalk.Visible = lcpFactsCalcWay.Visible = aimType == GoalType.MustStock;
            lcpPeriodCalk.Enabled = lcpFactsCalcWay.Enabled 
            = lblFactCalcway.Enabled = lblPeriodCalc.Enabled= enabled;
            lcpPeriodCalk.EditValue = aimCur.FactCalcPeriod;

            lcpFactsCalcWay.EditValue = aimCur.FactCalcWay;
            lcpPeriodCalk.EditValue = aimCur.FactCalcPeriod;
                
            checkUseAimValue.Visible = aimType == GoalType.Merchandising;
        }

        /// <summary>
        /// Change state of Save and Delete buttons
        /// </summary>
        private void RedrawButtonAccess()
        {
            checkAllowMultipleGoals.Enabled = MainData.AccessLevel > (int)AccessLevelType.ReadOnly && (CurAimsCount() == 0 || CurAimsCount() == 1 && aimCur.State != AimRecordState.New);

            if (MainData.ActivityTemplate == TemplateType.PriceActivities)
            {
                checkAllowMultipleGoals.Enabled = false;
                checkShowAll.Enabled = false;
                cbGoalType.Enabled = false;
            }

            simpleDelete.Enabled = MainData.AccessLevel > (int)AccessLevelType.ReadOnly && aimCur.State != AimRecordState.New && listGoals.SelectedIndex > -1 && !textCurAimName.Visible;
            simpleSave.Enabled = MainData.AccessLevel > (int)AccessLevelType.ReadOnly && aimCur.State == AimRecordState.New || listGoals.SelectedIndex > -1 && textCurAimName.Visible;
            simpleEdit.Enabled = MainData.AccessLevel > (int)AccessLevelType.ReadOnly && aimCur.State != AimRecordState.New && listGoals.SelectedIndex > -1 && !textCurAimName.Visible;
            simpleAdd.Enabled = MainData.AccessLevel > (int)AccessLevelType.ReadOnly && aimCur.State != AimRecordState.New && (checkAllowMultipleGoals.Checked || !checkAllowMultipleGoals.Checked && CurAimsCount() == 0) && !checkShowAll.Checked && !textCurAimName.Visible;
        }

        private int CurAimsCount()
        {
            int i = 0;

            foreach (DataTransferObjectAim aim in Aims)
            {
                if (aim.State != AimRecordState.Deleted && aim.State != AimRecordState.Unknown)
                {
                    i++;
                }
            }

            return i;
        }

        public void AllowEdit()
        {
            if (aimCur.State != AimRecordState.Unknown)
            {
                switch ((GoalType)aimCur.Type)
                {
                    case GoalType.SalesVolume:
                        skuTreeCtrl.ReadOnly = false;
                        break;

                    case GoalType.Merchandising:
                        treeKPI.ReadOnly = false;
                        break;

                    case GoalType.Effectiveness:
                        chlbIPTRList.Enabled = true;
                        break;

                    case GoalType.QuestionnaireLogic:
                        qpqTreeLogic.ReadOnly = false;
                        break;

                    case GoalType.QuestionnaireDigit:
                        qpqTreeDigit.ReadOnly = false;
                        break;
                    
                    case GoalType.InBevDoorCount:
                        break;
                    
                    case GoalType.MustStock:
                        skuTreeCtrl.ReadOnly = false;
                        break;
                }
            }

            labelEdit.Visible = true;
            textCurAimName.Visible = true;

            RedrawAimControls((GoalType)aimCur.Type, true);

            RedrawButtonAccess();
        }

        /// <summary>
        /// Determines whether the aim is valid
        /// </summary>
        /// <param name="Message">error message</param>
        /// <returns>whether the aim is valid</returns>
        private bool IsAimCurValid(out string Message)
        {
            bool isValid = true;
            Message = string.Empty;

            if (aimCur.Name.Trim() == string.Empty)
            {
                isValid = false;
                Message = "Нельзя сохранить цель без названия";
            }

            int index = Aims.FindIndex(aim => aim.Name.Trim() == aimCur.Name.Trim() && aim.State != AimRecordState.Deleted);
            if (index >= 0 && (aimCur.State == AimRecordState.New || !aimOriginal.Equals(Aims[index])))
            {
                isValid = false;
                Message = "Цель с заданным названием уже существует";
            }

            if (aimCur.Name.Length >= 100)
            {
                isValid = false;
                Message = "Слишком длинное название цели";
            }

            if (aimCur.SelectedIdList == string.Empty && (GoalType) aimCur.Type != GoalType.InBevDoorCount)
            {
                isValid = false;
                Message = "Нельзя сохранить пустую цель";
            }

            return isValid;
        }

        #endregion Event handling

        #region Data loading
        /// <summary>
        /// Sets tempate all fields data.
        /// </summary>
        /// <value>All fields data.</value>
        private DataTable AllFieldsData
        {
            set
            {
                if (value != null && value.Rows.Count == 1)
                {
                    DataRow lRow = value.Rows[0];

                    checkCannotChangeSKU.Checked = ConvertEx.ToBool(lRow[Constants.FIELD_GOALS_DISABLE_CHANGE_SKU]);
                    checkCannotChangeKPI.Checked = ConvertEx.ToBool(lRow[Constants.FIELD_GOALS_DISABLE_CHANGE_KPI]);
                    checkSplitByPOCType.Checked = ConvertEx.ToBool(lRow[Constants.FIELD_GOALS_DISABLE_CHANGE_TYPE]);

                    cbSplitByTime.EditValue = ConvertEx.ToInt(lRow[Constants.FIELD_GOALS_TARGET_PERIOD_TYPE]);
                    cbSplitByPOCType.EditValue = ConvertEx.ToInt(lRow[Constants.FIELD_GOALS_TARGET_REGION_TYPE]);

                    checkAllowMultipleGoals.Checked = ConvertEx.ToBool(lRow[Constants.FIELD_GOALS_ALLOW_MULTIPLE_GOALS]);
                }
            }
        }

        /// <summary>
        /// Loads the drop downs.
        /// </summary>
        private void LoadDropDowns()
        {
            cbSplitByTime.Properties.DataSource = DataProvider.SplitTargetsByTimeList;
            cbSplitByTime.EditValue = (int)SplitPOCByTime.Monthly;

            cbSplitByPOCType.Properties.DataSource = DataProvider.SplitTargetsByPocType;
            cbSplitByPOCType.EditValue = (int)SplitPOCByType.ByM3;

            cbGoalType.EditValueChanged -= cbGoalType_EditValueChanged;
            cbGoalType.Properties.DataSource = DataProvider.GoalTypesList;
            cbGoalType.EditValue = (int)GoalType.SalesVolume;
            cbGoalType.EditValueChanged += cbGoalType_EditValueChanged;

            List<ComboBoxItem> l = new List<ComboBoxItem>();
            l.AddRange(new[] { new ComboBoxItem((int)CheckByVisitsType.ByLastVisit, "по последнему визиту"), new ComboBoxItem((int)CheckByVisitsType.ByMonth, "за месяц") });
            cbVisitChecked.Properties.DataSource = l;

            lFactTypeSource = new List<ComboBoxItem>();
            lFactTypeSource.AddRange(new[] { new ComboBoxItem((int)FactCalcType.FactSales, "Факт из продаж"), 
                new ComboBoxItem((int)FactCalcType.FactRemainder, "Факт по остаткам в ТТ") });
            lcpFactsCalcWay.Properties.DataSource = lFactTypeSource;

            lFactCalcMonth = new List<ComboBoxItem>();
            for (int i = 1; i < 13; i++)
            {
                lFactCalcMonth.Add(new ComboBoxItem(i, i.ToString()));
            }
            lcpPeriodCalk.Properties.DataSource = lFactCalcMonth;
        }

        /// <summary>
        /// Reads info from DB and Loads it into Aims liast
        /// </summary>
        private void LoadAllAimsFromDB()
        {
            DataTable dt = DataProvider.GetAimList(TemplateId);

            DataTransferObjectAim aim = new DataTransferObjectAim();

            foreach (DataRow dr in dt.Rows)
            {
                aim = new DataTransferObjectAim();

                aim.Id = ConvertEx.ToInt(dr["Aim_Id"]);
                aim.Name = ConvertEx.ToString(dr["Aim_Name"]);
                aim.Type = ConvertEx.ToInt(dr["AimType_Id"]);

                aim.IsUsedInCalculations = ConvertEx.ToBool(dr["isUseInCompleted"]);
                aim.IsPeriodCalc = ConvertEx.ToBool(dr["isPeriodCalcAim"]);
                aim.IsInReverseExecution = ConvertEx.ToBool(dr["IsInReverseExec"]);

                aim.FactCalcWay = ConvertEx.ToInt(dr["FactCalcType"]);
                aim.FactCalcPeriod = ConvertEx.ToInt(dr["FactCalcPeriod"]);
                aim.IsUseAimValue = ConvertEx.ToBool(dr["isUseAimValue"]);
                aim.IsAccPerfOfTargetVal = ConvertEx.ToBool(dr["isAccPerfOfTargetVal"]);

                aim.State = AimRecordState.Normal;
                aim.SelectedIdList = LoadAimSelectedId(aim, null);

                Aims.Add(aim);
            }
        }

        /// <summary>
        /// Loads aim
        /// </summary>
        /// <param name="aimId">aim Id</param>
        private void LoadAim(DataTransferObjectAim aim, bool visible)// not empty
        {
            aimOriginal = aim;
            aimCur = new DataTransferObjectAim(aimOriginal);
            //
            DataTable dt = null;

            switch ((GoalType)aimCur.Type)
            // parse Id list
            {
                case GoalType.SalesVolume:

                    ClearTable(SKUtreeTable);
                    dt = SKUtreeTable;

                    foreach (string Id in aimCur.SelectedIdList.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
                    {
                        foreach (DataRow dr in dt.Rows)
                        {
                            if (ConvertEx.ToString(dr["Id"]) == Id)
                            {
                                dr["Checked"] = true;
                                break;
                            }
                        }
                    }

                    skuTreeCtrl.Load(SKUtreeTable);
                    break;

                case GoalType.Merchandising:

                    ClearTable(KPItreeTable);
                    dt = KPItreeTable;

                    foreach (string Id in aimCur.SelectedIdList.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
                    {
                        foreach (DataRow dr in dt.Rows)
                        {
                            if (ConvertEx.ToString(dr["ID"]) == Id)
                            {
                                dr["Checked"] = true;
                                break;
                            }
                        }
                    }

                    treeKPI.LoadKPITree(KPItreeTable);
                    break;

                case GoalType.Effectiveness:
                    chlbIPTRList.UnCheckAll();

                    foreach (string Id in aimCur.SelectedIdList.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
                    {
                        LoadIPTRList(Id);
                    }
                    break;

                case GoalType.QuestionnaireLogic:
                    ClearTable(QPQLogicTreeTable);

                    dt = QPQLogicTreeTable;

                    foreach (
                        string Id in
                            aimCur.SelectedIdList.Split(new char[] {','}, StringSplitOptions.RemoveEmptyEntries))
                    {
                        foreach (DataRow dr in dt.Rows)
                        {
                            if (ConvertEx.ToGuid(dr["ID"]).ToString().Equals(Id))
                            {
                                dr["Checked"] = true;
                                break;
                            }
                        }
                    }
                    qpqTreeLogic.LoadQPQTree(QPQLogicTreeTable);

                    if (checkShowAll.CheckState == CheckState.Checked && isLogicTreeFirstDraw)
                    {
                        isLogicTreeFirstDraw = false;
                        ClearTable(QPQLogicTreeTable);

                        dt = QPQLogicTreeTable;

                        foreach (
                            string Id in
                                aimCur.SelectedIdList.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
                        {
                            foreach (DataRow dr in dt.Rows)
                            {
                                if (ConvertEx.ToGuid(dr["ID"]).ToString().Equals(Id))
                                {
                                    dr["Checked"] = true;
                                    break;
                                }
                            }
                        }
                        qpqTreeLogic.LoadQPQTree(QPQLogicTreeTable);
                    }
                    break;

                case GoalType.QuestionnaireDigit:
                    ClearTable(QPQDigitTreeTable);

                    dt = QPQDigitTreeTable;

                    foreach (
                        string Id in
                            aimCur.SelectedIdList.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
                    {
                        foreach (DataRow dr in dt.Rows)
                        {
                            if (ConvertEx.ToGuid(dr["ID"]).ToString().Equals(Id))
                            {
                                dr["Checked"] = true;
                                break;
                            }
                        }

                    }
                    qpqTreeDigit.LoadQPQTree(QPQDigitTreeTable);

                    if (checkShowAll.CheckState == CheckState.Checked && isDigitTreeFirstDraw)
                    {
                        isDigitTreeFirstDraw = false;
                        ClearTable(QPQDigitTreeTable);

                        dt = QPQDigitTreeTable;

                        foreach (
                            string Id in
                                aimCur.SelectedIdList.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
                        {
                            foreach (DataRow dr in dt.Rows)
                            {
                                if (ConvertEx.ToGuid(dr["ID"]).ToString().Equals(Id))
                                {
                                    dr["Checked"] = true;
                                    break;
                                }
                            }

                        }
                        qpqTreeDigit.LoadQPQTree(QPQDigitTreeTable);
                    }
                    break;

                case GoalType.InBevDoorCount:
                    break;

                case GoalType.MustStock:

                    ClearTable(SKUtreeTable);
                    dt = SKUtreeTable;

                    foreach (string Id in aimCur.SelectedIdList.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
                    {
                        foreach (DataRow dr in dt.Rows)
                        {
                            if (ConvertEx.ToString(dr["Id"]) == Id)
                            {
                                dr["Checked"] = true;
                                break;
                            }
                        }
                    }

                    skuTreeCtrl.Load(SKUtreeTable);
                    break;
            }
            // end parse Id list

            textCurAimName.Text = string.Copy(aimCur.Name);

            RedrawTreeWindow((GoalType)aimCur.Type, visible);
            RedrawAimControls((GoalType)aimCur.Type, false);
        }

        /// <summary>
        /// Loads aim
        /// </summary>
        /// <param name="aimId">aim id</param>
        /// <param name="aimType">aim type</param>
        private void LoadAim(AimRecordState aimState, int aimType, bool visible)// is empty
        {
            aimOriginal = new DataTransferObjectAim();

            aimOriginal.State = aimState;
            aimOriginal.Type = aimType;

            aimCur = new DataTransferObjectAim(aimOriginal);

            switch ((GoalType)aimType)
            {
                case GoalType.SalesVolume:
                    ClearTable(SKUtreeTable);
                    skuTreeCtrl.Load(SKUtreeTable);
                    break;

                case GoalType.Merchandising:
                    ClearTable(KPItreeTable);
                    treeKPI.LoadKPITree(KPItreeTable);
                    break;

                case GoalType.Effectiveness:
                    chlbIPTRList.UnCheckAll();
                    break;

                case GoalType.QuestionnaireLogic:
                    ClearTable(QPQLogicTreeTable);
                    qpqTreeLogic.LoadQPQTree(QPQLogicTreeTable);
                    break;

                case GoalType.QuestionnaireDigit:
                    ClearTable(QPQDigitTreeTable);
                    qpqTreeDigit.LoadQPQTree(QPQDigitTreeTable);
                    break;
                
                case GoalType.InBevDoorCount:
                    break;

                case GoalType.MustStock:
                    ClearTable(SKUtreeTable);
                    skuTreeCtrl.Load(SKUtreeTable);
                    break;
            }

            textCurAimName.Text = string.Copy(aimCur.Name);

            RedrawTreeWindow((GoalType)aimCur.Type, visible);
            RedrawAimControls((GoalType)aimCur.Type, visible);
        }

        /// <summary>
        /// Builds Ids list
        /// </summary>
        /// <param name="aim">aim for which the list is builded</param>
        /// <param name="source">source table</param>
        /// <returns>Ids list</returns>
        private string LoadAimSelectedId(DataTransferObjectAim aim, DataTable source)
        {
            DataTable dt = null;
            string s = string.Empty;

            switch ((GoalType)aim.Type)
            // parse Id list
            {
                case GoalType.SalesVolume:
                    dt = source == null ? DataProvider.GetSKUTree(aim.Id) : source;

                    foreach (DataRow dr in dt.Rows)
                    {
                        if (ConvertEx.ToBool(dr["Checked"]))
                        {
                            s += "," + ConvertEx.ToString(dr["Id"]);
                        }
                    }

                    break;

                case GoalType.Merchandising:
                    dt = source == null ? DataProvider.GetKPITree(aim.Id) : source;

                    foreach (DataRow dr in dt.Rows)
                    {
                        if (ConvertEx.ToBool(dr["Checked"]))
                        {
                            s += "," + ConvertEx.ToString(dr["ID"]);
                        }
                    }

                    break;

                case GoalType.Effectiveness:
                    dt = source == null ? DataProvider.GetIPTRAim(aim.Id) : source;

                    foreach (DataRow dr in dt.Rows)
                    {
                        s += "," + ConvertEx.ToString(dr["EquipmentClass_ID"]);
                    }

                    break;

                case GoalType.QuestionnaireLogic:
                    dt = source == null ? DataProvider.GetQPQTree(aim.Id, 1) : source;

                    foreach (DataRow dr in dt.Rows)
                    {
                        if (ConvertEx.ToBool(dr["Checked"]))
                        {
                            s += "," + ConvertEx.ToString(dr["ID"]);
                        }
                    }

                    break;

                case GoalType.QuestionnaireDigit:
                    dt = source == null ? DataProvider.GetQPQTree(aim.Id, 2) : source;

                    foreach (DataRow dr in dt.Rows)
                    {
                        if (ConvertEx.ToBool(dr["Checked"]))
                        {
                            s += "," + ConvertEx.ToString(dr["ID"]);
                        }
                    }

                    break;
                case GoalType.InBevDoorCount:
                    break;
                case GoalType.MustStock:
                    dt = source == null ? DataProvider.GetMustStockTree(aim.Id) : source;

                    foreach (DataRow dr in dt.Rows)
                    {
                        if (ConvertEx.ToBool(dr["Checked"]))
                        {
                            s += "," + ConvertEx.ToString(dr["ID"]);
                        }
                    }
                    break;
            }

            s = s.TrimStart(',');
            // end parse Id list

            return s;
        }

        /// <summary>
        /// Determines whether current and original versions are different
        /// </summary>
        /// <returns>whether the aim was modified</returns>
        private bool IsModified()
        {
            GetAllAimFields();

            return !aimOriginal.Equals(aimCur);
        }

        /// <summary>
        /// Clears trees' tables Checked column
        /// </summary>
        /// <param name="dt">table to clear</param>
        private void ClearTable(DataTable dt)
        {
            foreach (DataRow dr in dt.Rows)
            {
                dr["Checked"] = false;
            }
        }

        /// <summary>
        /// Gets Id in the list of aim by aim Id
        /// </summary>
        /// <param name="value">aim Id</param>
        /// <returns>list Id</returns>
        private int GetIdByValue(int value)
        {
            int index = -1, i = 0;

            foreach (ComboBoxItem item in AimList)
            {
                if (item.Id == value)
                {
                    index = i;
                    break;
                }
                else
                {
                    i++;
                }
            }

            return index;
        }

        #endregion Data loading

        #region Data saving
        /// <summary>
        /// Gets current aim values
        /// </summary>
        private void GetAllAimFields()
        {
            //aimCur.AimId
            aimCur.Name = string.Copy(textCurAimName.Text);
            //aimCur.AimType
            aimCur.IsUsedInCalculations = checkUseInCalculations.Checked;
            aimCur.IsPeriodCalc = ConvertEx.ToBool(cbVisitChecked.EditValue);

            switch ((GoalType)ConvertEx.ToInt(aimCur.Type))
            {
                case GoalType.SalesVolume:
                    aimCur.SelectedXmlList = skuTreeCtrl.SelectedXMLIdList;
                    aimCur.SelectedIdList = LoadAimSelectedId(aimCur, skuTreeCtrl.DataSourcePosted);
                    break;
                case GoalType.Merchandising:
                    aimCur.SelectedXmlList = treeKPI.SelectedXmlIdList;
                    aimCur.SelectedIdList = LoadAimSelectedId(aimCur, treeKPI.DataSourcePosted);
                    aimCur.IsInReverseExecution = checkReversExecution.Checked;
                    aimCur.IsUseAimValue = checkUseAimValue.Checked;
                    aimCur.IsAccPerfOfTargetVal = checkAccPerfOfTargetVal.Checked;

                    break;
                case GoalType.Effectiveness:
                    aimCur.SelectedIdList = SelectedIPTRList;
                    break;
                
                case GoalType.QuestionnaireLogic:
                    aimCur.SelectedXmlList = qpqTreeLogic.SelectedXmlIdList;
                    aimCur.SelectedIdList = LoadAimSelectedId(aimCur, qpqTreeLogic.DataSourcePosted);
                    break;

                case GoalType.QuestionnaireDigit:
                    aimCur.SelectedXmlList = qpqTreeDigit.SelectedXmlIdList;
                    aimCur.SelectedIdList = LoadAimSelectedId(aimCur, qpqTreeDigit.DataSourcePosted);
                    aimCur.IsInReverseExecution = checkReversExecution.Checked;
                    break;

                case GoalType.InBevDoorCount:
                    break;

                case GoalType.MustStock:
                    aimCur.SelectedXmlList = skuTreeCtrl.SelectedXMLIdList;
                    aimCur.SelectedIdList = LoadAimSelectedId(aimCur, skuTreeCtrl.DataSourcePosted);
                    aimCur.FactCalcPeriod = Convert.ToInt32(lcpPeriodCalk.EditValue);
                    aimCur.FactCalcWay = Convert.ToInt32(lcpFactsCalcWay.EditValue);
                    break;
            }
        }

        /// <summary>
        /// Saves aim
        /// </summary>
        private void SaveAimToList(DataTransferObjectAim aim)
        {
            if (aim.State == AimRecordState.New)
            {
                aimOriginal.Copy(aim);

                Aims.Add(aim);
                aim.State = AimRecordState.Updated;
            }
            else
            {
                aimOriginal.Copy(aim);
                aimOriginal.State = AimRecordState.Updated;
            }
        }

        /// <summary>
        /// Saves aim
        /// </summary>
        private void SaveAimToDB(DataTransferObjectAim aim, int templateId)
        {
            aim.Id = DataProvider.UpdateAim(templateId, aim.Name, aim.Type, aim.IsUsedInCalculations, aim.IsPeriodCalc, 
                aim.Id,aim.FactCalcPeriod,aim.FactCalcWay, aim.IsUseAimValue, aim.IsInReverseExecution, aim.IsAccPerfOfTargetVal);

            switch ((GoalType)aim.Type)
            {
                case GoalType.SalesVolume:
                    aim.Id = DataProvider.UpdateSKUAim(aim.Id, aim.SelectedXmlList);
                    break;

                case GoalType.Merchandising:
                    aim.Id = DataProvider.UpdateKPIAim(aim.Id, aim.SelectedXmlList);
                    break;

                case GoalType.Effectiveness:
                    aim.Id = DataProvider.UpdateIPTRAim(aim.Id, ConvertEx.ToInt(aim.SelectedIdList));
                    break;

                case GoalType.QuestionnaireLogic:
                    aim.Id = DataProvider.UpdateQPQAim(aim.Id, 1, aim.SelectedIdList);
                    break;

                case GoalType.QuestionnaireDigit:
                    aim.Id = DataProvider.UpdateQPQAim(aim.Id, 2, aim.IsInReverseExecution, aim.SelectedIdList);
                    break;

                case GoalType.InBevDoorCount:
                    break;

                case GoalType.MustStock:
                    aim.Id = DataProvider.UpdateSKUAim(aim.Id, aim.SelectedXmlList);
                    break;
            }

            aim.State = AimRecordState.Normal;
        }

        #endregion Data saving
    }
}