﻿using System;
using System.ComponentModel;
using System.Data;
using DevExpress.XtraEditors;
using Logica.Reports.BaseReportControl.CommonControls;
using System.Collections.ObjectModel;
using Logica.Reports.BaseReportControl.Utility;
using System.Windows.Forms;
using System.Collections.Generic;

namespace SoftServe.Reports.MPTemplates
{
    /// <summary>
    /// 
    /// </summary>
    public partial class TemplateDetailsCommon : CommonBaseControl
    {
        private List<Control> controlsToRefresh;

        internal TemplateDetailsControl MainControl;

        /// <summary>
        /// Initializes a new instance of the <see cref="TemplateDetailsCommon"/> class.
        /// </summary>
        public TemplateDetailsCommon()
        {
            TemplateId = 0;

            InitializeComponent();
            InitControlsToRefresh();
            cbTemplateChannel.EditValue = Constants.OFF_TRADE_CHNL_ID;
        }

        /// <summary>
        /// Gets the selected level.
        /// </summary>
        /// <value>The selected level.</value>
        public LevelType SelectedLevel
        {
            get
            {
                return (LevelType)ConvertEx.ToInt(cbTemplateLevel.EditValue);
            }
        }

        /// <summary>
        /// Sets all fields data.
        /// </summary>
        /// <value>All fields data.</value>
        private DataTable AllFieldsData
        {
            set
            {
                if (value != null && value.Rows.Count == 1)
                {
                    DataRow row = value.Rows[0];

                    // Base
                    lblDCRName.Text = string.Format(Resource.LabelTemplateCreator,
                        ConvertEx.ToString(row[Constants.FIELD_COMMON_CREATOR_NAME]),
                        ConvertEx.ToDateTime(row[Constants.FIELD_COMMON_CREATION_DATE]).Date.ToString("d"));
                    checkApproved.Checked = ConvertEx.ToBool(row[Constants.FIELD_COMMON_APPROVER]);

                    // Template
                    ebTemplateName.EditValue = ConvertEx.ToString(row[Constants.FIELD_ACTION_NAME]);
                    cbTemplateType.EditValue = ConvertEx.ToInt(row[Constants.FIELD_ACTION_TYPE]);
                    cbTemplateLevel.EditValue = ConvertEx.ToByte(row[Constants.FIELD_ACTION_LEVEL]);
                    memoTemplateDescription.EditValue = ConvertEx.ToString(row[Constants.FIELD_ACTION_DESCRIPTION]);
                    checkCannotModify.Checked = ConvertEx.ToBool(row[Constants.FIELD_ACTION_CHANGE]);
                    int chnl_id = ConvertEx.ToInt(row[Constants.FIELD_ACTION_CHANNEL]);
                    cbTemplateChannel.EditValue = chnl_id > 0 ? chnl_id : Constants.OFF_TRADE_CHNL_ID;

                    // Position
                    cbPositionCreator.EditValue = ConvertEx.ToInt(row[Constants.FIELD_POSITION_INITIATOR]);
                    cbPositionTargetManager.EditValue = ConvertEx.ToInt(row[Constants.FIELD_POSITION_PLANNER]);
                    cbPositionControlGroup.EditValue = ConvertEx.ToInt(row[Constants.FIELD_POSITION_CONTROLLER]);
                    cbPositionApprover.EditValue = ConvertEx.ToInt(row[Constants.FIELD_POSITION_APPROVER]);
                    cbPositionExecutor.EditValue = ConvertEx.ToInt(row[Constants.FIELD_POSITION_EXECUTOR]);
                    cbPositionControlFact.EditValue = ConvertEx.ToInt(row[Constants.FIELD_POSITION_CHECKER]);

                    // Promo
                    checkPromoCascadeEdit.Checked = ConvertEx.ToBool(row[Constants.FIELD_PROMO_CASCADE]);
                    checkPromoAllowFromThR.Checked = ConvertEx.ToBool(row[Constants.FIELD_PROMO_MANUALLY]);
                    checkPromoInActiveWaveThR.Checked = ConvertEx.ToBool(row[Constants.FIELD_PROMO_CHECK_TR]);

                    // Control
                    checkControlCascadeEdit.Checked = ConvertEx.ToBool(row[Constants.FIELD_CONTROL_CASCADE]);
                    checkControlAllowFromThR.Checked = ConvertEx.ToBool(row[Constants.FIELD_CONTROL_MANUALLY]);
                    checkControlInActiveWaveThR.Checked = ConvertEx.ToBool(row[Constants.FIELD_CONTROL_CHECK_TR]);

                    // KPK
                    checkKPKShow.Checked = ConvertEx.ToBool(row[Constants.FIELD_KPK_SHOW]);
                    checkKPKConnectionDate.Checked = ConvertEx.ToBool(row[Constants.FIELD_KPK_CHECK_DATE]);
                    memoKPKTemplateDescription.EditValue = ConvertEx.ToString(row[Constants.FIELD_KPK_DESCRIPTION]);

                    // Periods
                    checkPeriodsFromTR.Checked = ConvertEx.ToBool(row[Constants.FIELD_PERIODS_FROM_TR]);
                    spinPeriodsMonthCheck.EditValue = ConvertEx.ToInt(row[Constants.FIELD_PERIODS_MONITORING_PERIOD]);

                    dateActiveFromDay.Value = ConvertEx.ToInt(row[Constants.FIELD_PERIODS_ACTION_START_DAY]);
                    dateActiveFromMonth.Value = ConvertEx.ToInt(row[Constants.FIELD_PERIODS_ACTION_START_MONTH]);
                    dateActiveToDay.Value = ConvertEx.ToInt(row[Constants.FIELD_PERIODS_ACTION_END_DAY]);
                    dateActiveToMonth.Value = ConvertEx.ToInt(row[Constants.FIELD_PERIODS_ACTION_END_MONTH]);

                    dateFirstFromDay.Value = ConvertEx.ToInt(row[Constants.FIELD_PERIODS_BASE1_START_DAY]);
                    dateFirstFromMonth.Value = ConvertEx.ToInt(row[Constants.FIELD_PERIODS_BASE1_START_MONTH]);
                    dateFirstFromYear.Value = ConvertEx.ToInt(row[Constants.FIELD_PERIODS_BASE1_START_YEAR_OFFSET]);

                    dateFirstToDay.Value = ConvertEx.ToInt(row[Constants.FIELD_PERIODS_BASE1_END_DAY]);
                    dateFirstToMonth.Value = ConvertEx.ToInt(row[Constants.FIELD_PERIODS_BASE1_END_MONTH]);
                    dateFirstToYear.Value = ConvertEx.ToInt(row[Constants.FIELD_PERIODS_BASE1_END_YEAR_OFFSET]);

                    dateSecondFromDay.Value = ConvertEx.ToInt(row[Constants.FIELD_PERIODS_BASE2_START_DAY]);
                    dateSecondFromMonth.Value = ConvertEx.ToInt(row[Constants.FIELD_PERIODS_BASE2_START_MONTH]);
                    dateSecondFromYear.Value = ConvertEx.ToInt(row[Constants.FIELD_PERIODS_BASE2_START_YEAR_OFFSET]);

                    dateSecondToDay.Value = ConvertEx.ToInt(row[Constants.FIELD_PERIODS_BASE2_END_DAY]);
                    dateSecondToMonth.Value = ConvertEx.ToInt(row[Constants.FIELD_PERIODS_BASE2_END_MONTH]);
                    dateSecondToYear.Value = ConvertEx.ToInt(row[Constants.FIELD_PERIODS_BASE2_END_YEAR_OFFSET]);

                }
            }
        }

        /// <summary>
        /// Loads the data.
        /// </summary>
        /// <param name="templateId">The template id.</param>
        public override void LoadData(int templateId, DataTable dtDetails)
        {
            TemplateId = templateId;

            this.Refresh();

            AllFieldsData = dtDetails;
            LoadDocuments();

            UpdateControlsState();

            base.LoadData(templateId, dtDetails);

            // post process read only mode
            checkApproved.Properties.ReadOnly = !(MainData.AccessLevel >= (int)AccessLevelType.Approve);
        }

        /// <summary>
        /// Updates the periods month check.
        /// </summary>
        private void UpdatePeriodsMonthCheck()
        {
            spinPeriodsMonthCheck.Properties.Increment = spinPeriodsMonthCheck.Value >= 12 ? 6 : 1;
        }

        /// <summary>
        /// Gets the template details.
        /// </summary>
        /// <value>The template details.</value>
        public void GetAllFields(TemplateDetailsDTO dto, int templateId)
        {
            dto.FIELD_COMMON_TEMPLATE_ID = templateId;

            // Base
            dto.FIELD_COMMON_CREATOR_NAME = string.Empty;
            dto.FIELD_COMMON_CREATION_DATE = DateTime.Now;
            dto.FIELD_COMMON_APPROVER = checkApproved.Checked;

            // Template
            dto.FIELD_ACTION_NAME = ConvertEx.ToString(ebTemplateName.EditValue);
            dto.FIELD_ACTION_TYPE = ConvertEx.ToInt(cbTemplateType.EditValue);
            dto.FIELD_ACTION_LEVEL = ConvertEx.ToInt(cbTemplateLevel.EditValue);
            dto.FIELD_ACTION_CHANNEL = ConvertEx.ToInt(cbTemplateChannel.EditValue);
            dto.FIELD_ACTION_DESCRIPTION = ConvertEx.ToString(memoTemplateDescription.EditValue);
            dto.FIELD_ACTION_CHANGE = checkCannotModify.Checked;

            // Position
            dto.FIELD_POSITION_INITIATOR = ConvertEx.ToInt(cbPositionCreator.EditValue);
            dto.FIELD_POSITION_PLANNER = ConvertEx.ToInt(cbPositionTargetManager.EditValue);
            dto.FIELD_POSITION_CONTROLLER = ConvertEx.ToInt(cbPositionControlGroup.EditValue);
            dto.FIELD_POSITION_APPROVER = ConvertEx.ToInt(cbPositionApprover.EditValue);
            dto.FIELD_POSITION_EXECUTOR = ConvertEx.ToInt(cbPositionExecutor.EditValue);
            dto.FIELD_POSITION_CHECKER = ConvertEx.ToInt(cbPositionControlFact.EditValue);

            // Promo
            dto.FIELD_PROMO_CASCADE = checkPromoCascadeEdit.Checked;
            dto.FIELD_PROMO_MANUALLY = checkPromoAllowFromThR.Checked;
            dto.FIELD_PROMO_CHECK_TR = checkPromoInActiveWaveThR.Checked;

            // Control
            dto.FIELD_CONTROL_CASCADE = checkControlCascadeEdit.Checked;
            dto.FIELD_CONTROL_MANUALLY = checkControlAllowFromThR.Checked;
            dto.FIELD_CONTROL_CHECK_TR = checkControlInActiveWaveThR.Checked;

            // KPK
            dto.FIELD_KPK_SHOW = checkKPKShow.Checked;
            dto.FIELD_KPK_CHECK_DATE = checkKPKConnectionDate.Checked;
            dto.FIELD_KPK_DESCRIPTION = ConvertEx.ToString(memoKPKTemplateDescription.EditValue);

            // Periods
            dto.FIELD_PERIODS_FROM_TR = checkPeriodsFromTR.Checked;
            dto.FIELD_PERIODS_MONITORING_PERIOD = ConvertEx.ToInt(spinPeriodsMonthCheck.EditValue);

            dto.FIELD_PERIODS_ACTION_START_DAY = ConvertEx.ToInt(dateActiveFromDay.Value);
            dto.FIELD_PERIODS_ACTION_START_MONTH = ConvertEx.ToInt(dateActiveFromMonth.Value);
            dto.FIELD_PERIODS_ACTION_END_DAY = ConvertEx.ToInt(dateActiveToDay.Value);
            dto.FIELD_PERIODS_ACTION_END_MONTH = ConvertEx.ToInt(dateActiveToMonth.Value);

            dto.FIELD_PERIODS_BASE1_START_DAY = ConvertEx.ToInt(dateFirstFromDay.Value);
            dto.FIELD_PERIODS_BASE1_START_MONTH = ConvertEx.ToInt(dateFirstFromMonth.Value);
            dto.FIELD_PERIODS_BASE1_START_YEAR_OFFSET = ConvertEx.ToInt(dateFirstFromYear.Value);

            dto.FIELD_PERIODS_BASE1_END_DAY = ConvertEx.ToInt(dateFirstToDay.Value);
            dto.FIELD_PERIODS_BASE1_END_MONTH = ConvertEx.ToInt(dateFirstToMonth.Value);
            dto.FIELD_PERIODS_BASE1_END_YEAR_OFFSET = ConvertEx.ToInt(dateFirstToYear.Value);

            dto.FIELD_PERIODS_BASE2_START_DAY = ConvertEx.ToInt(dateSecondFromDay.Value);
            dto.FIELD_PERIODS_BASE2_START_MONTH = ConvertEx.ToInt(dateSecondFromMonth.Value);
            dto.FIELD_PERIODS_BASE2_START_YEAR_OFFSET = ConvertEx.ToInt(dateSecondFromYear.Value);

            dto.FIELD_PERIODS_BASE2_END_DAY = ConvertEx.ToInt(dateSecondToDay.Value);
            dto.FIELD_PERIODS_BASE2_END_MONTH = ConvertEx.ToInt(dateSecondToMonth.Value);
            dto.FIELD_PERIODS_BASE2_END_YEAR_OFFSET = ConvertEx.ToInt(dateSecondToYear.Value);
        }

        /// <summary>
        /// Gets the document changes.
        /// </summary>
        /// <param name="rowStates">The row states.</param>
        /// <returns></returns>
        public Collection<FileUploader.FileDataInfo> GetDocumentChanges(DataRowState rowStates)
        {
            return documentUploader.GetFiles(rowStates);
        }

        /// <summary>
        /// Forces the control to invalidate its client area and immediately redraw itself and any child controls.
        /// </summary>
        private void Refresh()
        {
            foreach (Control c in controlsToRefresh)
            {
                if (c is CheckEdit)
                {
                    (c as CheckEdit).Checked = false;
                    continue;
                }

                if (c is BaseEdit)
                {
                    (c as BaseEdit).EditValue = null;
                    continue;
                }

                if (c is DateCombo)
                {
                    (c as DateCombo).ValueObj = null;
                    continue;
                }
            }

            this.spinPeriodsMonthCheck.Value = 3;
            LoadDropDowns();
        }

        /// <summary>
        /// Loads the documents.
        /// </summary>
        private void LoadDocuments()
        {
            documentUploader.Data = DataProvider.GetDocumentList(TemplateId);
        }

        /// <summary>
        /// Loads the drop downs.
        /// </summary>
        private void LoadDropDowns()
        {
            LoadDates();

            cbTemplateChannel.Properties.DataSource = DataProvider.ChannelList;
            cbTemplateType.Properties.DataSource = DataProvider.ActionTypeList;
            cbTemplateLevel.Properties.DataSource = DataProvider.ActionLevelList;

            cbPositionApprover.Properties.DataSource =
            cbPositionControlFact.Properties.DataSource =
            cbPositionControlGroup.Properties.DataSource =
            cbPositionCreator.Properties.DataSource =
            cbPositionExecutor.Properties.DataSource =
            cbPositionTargetManager.Properties.DataSource = DataProvider.PositionList;

            cbTemplateLevel.EditValue = (byte)LevelType.M3;
            cbTemplateChannel.EditValue = Constants.OFF_TRADE_CHNL_ID;
        }

        /// <summary>
        /// Loads the dates.
        /// </summary>
        private void LoadDates()
        {
            int yearPrev = DateTime.Today.Year - 1;
            int yearCurrent = DateTime.Today.Year;

            // Active period
            dateActiveFromMonth.LoadMonths();
            dateActiveFromDay.LoadDays(DateTime.Today.Year, dateActiveFromMonth.Value);

            dateActiveToMonth.LoadMonths();
            dateActiveToDay.LoadDays(DateTime.Today.Year, dateActiveToMonth.Value);

            // First
            dateFirstFromYear.LoadYearFlags();
            dateFirstFromMonth.LoadMonths();
            dateFirstFromDay.LoadDays(
                dateFirstFromYear.Value == (int)DateCombo.YearFlag.Current ? yearCurrent : yearPrev,
            dateFirstFromMonth.Value);

            dateFirstToYear.LoadYearFlags();
            dateFirstToMonth.LoadMonths();
            dateFirstToDay.LoadDays(
                dateFirstToYear.Value == (int)DateCombo.YearFlag.Current ? yearCurrent : yearPrev,
                dateFirstToMonth.Value);

            // Secons
            dateSecondFromYear.LoadYearFlags();
            dateSecondFromMonth.LoadMonths();
            dateSecondFromDay.LoadDays(
                dateSecondFromYear.Value == (int)DateCombo.YearFlag.Current ? yearCurrent : yearPrev,
                dateSecondFromMonth.Value);

            dateSecondToYear.LoadYearFlags();
            dateSecondToMonth.LoadMonths();
            dateSecondToDay.LoadDays(
                dateSecondToYear.Value == (int)DateCombo.YearFlag.Current ? yearCurrent : yearPrev,
                dateSecondToMonth.Value);
        }

        /// <summary>
        /// Updates the state of the controls.
        /// </summary>
        private void UpdateControlsState()
        {
            checkKPKConnectionDate.Enabled = checkKPKShow.Checked;
            lblKPKTemplateDescription.Enabled = checkKPKShow.Checked;
            memoKPKTemplateDescription.Properties.ReadOnly = !checkKPKShow.Checked;
        }

        private void InitControlsToRefresh()
        {
            controlsToRefresh = new List<Control> 
          { 
            ebTemplateName,
            cbTemplateType,
            checkCannotModify,
            memoTemplateDescription,
            documentUploader,
            checkCannotModify,
            checkKPKShow,
            checkKPKConnectionDate,
            memoKPKTemplateDescription,
            checkApproved,
            spinPeriodsMonthCheck,
            checkPeriodsFromTR,
            dateActiveFromDay,
            dateActiveFromMonth,
            dateActiveToDay,
            dateActiveToMonth,
            dateFirstFromDay,
            dateFirstFromMonth,
            dateFirstFromYear,
            dateFirstToDay,
            dateFirstToMonth,
            dateFirstToYear,
            dateSecondFromDay,
            dateSecondFromMonth,
            dateSecondFromYear,
            dateSecondToDay,
            dateSecondToMonth,
            dateSecondToYear,
            cbPositionApprover,
            cbPositionControlFact,
            cbPositionControlGroup,
            cbPositionCreator,
            cbPositionExecutor,
            cbPositionTargetManager,
            cbTemplateChannel,
            cbTemplateLevel,
            cbTemplateType,
            checkPromoAllowFromThR,
            checkPromoCascadeEdit,
            checkPromoInActiveWaveThR,
            checkControlAllowFromThR,
            checkControlCascadeEdit,
            checkControlInActiveWaveThR
          };
        }

        /// <summary>
        /// Handles the EditValueChanged event of the MonthYear control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void MonthYear_EditValueChanged(object sender, EventArgs e)
        {
            int yearPrev = DateTime.Today.Year - 1;
            int yearCurrent = DateTime.Today.Year;

            if (sender.Equals(dateActiveFromMonth))
            {
                dateActiveFromDay.LoadDays(DateTime.Today.Year, dateActiveFromMonth.Value);
            }
            else if (sender.Equals(dateActiveToMonth))
            {
                dateActiveToDay.LoadDays(DateTime.Today.Year, dateActiveToMonth.Value);
            }
            else if (sender.Equals(dateFirstFromMonth) || sender.Equals(dateFirstFromYear))
            {
                dateFirstFromDay.LoadDays(
                    dateFirstFromYear.Value == (int)DateCombo.YearFlag.Current ? yearCurrent : yearPrev,
                    dateFirstFromMonth.Value);
            }
            else if (sender.Equals(dateFirstToMonth) || sender.Equals(dateFirstToYear))
            {
                dateFirstToDay.LoadDays(
                    dateFirstToYear.Value == (int)DateCombo.YearFlag.Current ? yearCurrent : yearPrev,
                    dateFirstToMonth.Value);
            }
            else if (sender.Equals(dateSecondFromMonth) || sender.Equals(dateSecondFromYear))
            {
                dateSecondFromDay.LoadDays(
                    dateSecondFromYear.Value == (int)DateCombo.YearFlag.Current ? yearCurrent : yearPrev,
                    dateSecondFromMonth.Value);
            }
            else if (sender.Equals(dateSecondToMonth) || sender.Equals(dateSecondToYear))
            {
                dateSecondToDay.LoadDays(
                    dateSecondToYear.Value == (int)DateCombo.YearFlag.Current ? yearCurrent : yearPrev,
                    dateSecondToMonth.Value);
            }
        }

        /// <summary>
        /// Handles the Validating event of the ebTemplateName control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.ComponentModel.CancelEventArgs"/> instance containing the event data.</param>
        private void View_Validating(object sender, CancelEventArgs e)
        {
            if (sender != null && !(sender as Control).Enabled) return;

            BaseEdit baseEdit = sender as BaseEdit;
            if (baseEdit != null)
            {
                if (baseEdit.EditValue == null || string.IsNullOrEmpty(baseEdit.EditValue.ToString()))
                {
                    if (baseEdit.Equals(memoTemplateDescription) && documentUploader.HasRows ||
                        baseEdit.Equals(memoKPKTemplateDescription))
                    {
                        e.Cancel = false;
                    }
                    else
                    {
                        e.Cancel = true;
                        baseEdit.ErrorText = Resource.FieldEmptyError;
                    }
                }
            }
            else if (sender != null && !e.Cancel)
            {
                if (sender == dateFirstFromDay)
                {
                    DateTimeComparer base1Start = new DateTimeComparer(dateFirstFromYear.Value, dateFirstFromMonth.Value, dateFirstFromDay.Value);
                    DateTimeComparer base1End = new DateTimeComparer(dateFirstToYear.Value, dateFirstToMonth.Value, dateFirstToDay.Value);
                    e.Cancel = base1Start.Compare(base1End) >= 0;
                    if (e.Cancel)
                    {
                        SetError(dateFirstFromDay.InnerControl, Resource.ErrorDateToGreaterDateFrom);
                    }
                }
                else if (sender == dateSecondFromDay)
                {
                    DateTimeComparer base2Start = new DateTimeComparer(dateSecondFromYear.Value, dateSecondFromMonth.Value, dateSecondFromDay.Value);
                    DateTimeComparer base2End = new DateTimeComparer(dateSecondToYear.Value, dateSecondToMonth.Value, dateSecondToDay.Value);
                    e.Cancel = base2Start.Compare(base2End) >= 0;
                    if (e.Cancel)
                    {
                        SetError(dateSecondFromDay.InnerControl, Resource.ErrorDateToGreaterDateFrom);
                    }
                }
            }
        }

        /// <summary>
        /// Handles the EditValueChanged event of the checkKPKShow control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void checkKPKShow_EditValueChanged(object sender, EventArgs e)
        {
            UpdateControlsState();
        }

        /// <summary>
        /// Documents the uploader_ get file data.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The e.</param>
        private void documentUploader_GetFileData(object sender, FileUploader.FileDataInfo e)
        {
            e.FileData = DataProvider.GetDocument(e.Id);
        }

        /// <summary>
        /// Handles the CheckedChanged event of the checkCannotModify control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void checkCannotModify_CheckedChanged(object sender, EventArgs e)
        {
            if (checkCannotModify.Checked && String.IsNullOrEmpty(memoTemplateDescription.Text))
            {
                memoTemplateDescription.ErrorText = Resource.FieldEmptyError;
                checkCannotModify.Checked = false;
            }
            documentUploader.ReadOnly = memoTemplateDescription.Properties.ReadOnly = checkCannotModify.Checked;
        }

        private void checkPeriodsFromTR_CheckedChanged(object sender, EventArgs e)
        {
            dateFirstFromDay.Enabled = dateFirstFromMonth.Enabled = dateFirstFromYear.Enabled =
                dateFirstToDay.Enabled = dateFirstToMonth.Enabled = dateFirstToYear.Enabled =
                dateSecondFromDay.Enabled = dateSecondFromMonth.Enabled = dateSecondFromYear.Enabled =
                dateSecondToDay.Enabled = dateSecondToMonth.Enabled = dateSecondToYear.Enabled = !checkPeriodsFromTR.Checked;
        }

        /// <summary>
        /// Handles the EditValueChanged event of the spinPeriodsMonthCheck control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void spinPeriodsMonthCheck_EditValueChanged(object sender, EventArgs e)
        {
            UpdatePeriodsMonthCheck();
        }

        /// <summary>
        /// Handles the Spin event of the spinPeriodsMonthCheck control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="DevExpress.XtraEditors.Controls.SpinEventArgs"/> instance containing the event data.</param>
        private void spinPeriodsMonthCheck_Spin(object sender, DevExpress.XtraEditors.Controls.SpinEventArgs e)
        {
            UpdatePeriodsMonthCheck();
        }



        /// <summary>
        /// Handles event of template type drop-down and updates relative information into main data.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cbTemplateType_EditValueChanged(object sender, EventArgs e)
        {
            this.MainData.ActivityTemplate = (TemplateType)ConvertEx.ToInt(cbTemplateType.EditValue);

            if ((this.MainData.ActivityTemplate == TemplateType.PriceActivities || (TemplateType)oldType == TemplateType.PriceActivities) && TemplateId == 0)
            {
                MainControl.controlGoals.LoadData(TemplateId, MainControl.TemplateDetails);
            }
        }

        private int oldType;

        private void cbTemplateType_EditValueChanging(object sender, DevExpress.XtraEditors.Controls.ChangingEventArgs e)
        {
            oldType = ConvertEx.ToInt(e.OldValue);

            if (((TemplateType)ConvertEx.ToInt(e.NewValue) == TemplateType.PriceActivities || (TemplateType)oldType == TemplateType.PriceActivities) && TemplateId == 0 && MainControl.controlGoals.WasGoalTabModified)
            {
                    DialogResult result = XtraMessageBox.Show("Закладка Настройка целей была изменена. Все равно изменить тип?", "Предупреждение", MessageBoxButtons.YesNoCancel);
                    if (result == DialogResult.No || result == DialogResult.Cancel)
                    {
                       e.Cancel = true;
                    }
            }
        }

    }
}