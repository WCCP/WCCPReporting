﻿using System;
using System.Windows.Forms;
using SoftServe.Reports.MPTemplates;
using DevExpress.XtraGrid;
using System.Data;
using DevExpress.XtraEditors.DXErrorProvider;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraTreeList;
using DevExpress.XtraTreeList.Columns;
using Logica.Reports.BaseReportControl.CommonControls;
using Logica.Reports.BaseReportControl.Utility;
using Logica.Reports.BaseReportControl.CommonControls.Addresses.DataAccess;
using Logica.Reports.BaseReportControl;

namespace SoftServe.Reports.MPTemplates
{
    /// <summary>
    /// 
    /// </summary>
    public partial class CommonBaseControl : UserControl
    {
        public event EventHandler UpdateMainView;
        internal CommonBaseExternalData MainData { get; set; }

        DXErrorProvider errorProvider = new DXErrorProvider();

        /// <summary>
        /// Initializes a new instance of the <see cref="CommonBaseControl"/> class.
        /// </summary>
        public CommonBaseControl()
        {
            MainData = new CommonBaseExternalData();
            errorProvider.ContainerControl = this;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CommonBaseControl"/> class.
        /// </summary>
        /// <param name="parentTab">The parent tab.</param>
        public CommonBaseControl(CommonBaseTab parentTab)
        {
            MainData = new CommonBaseExternalData();
            errorProvider.ContainerControl = this;
            this.ParentTab = parentTab;
            this.HaveUnsavedData = false;
        }

        /// <summary>
        /// Gets or sets the parent tab.
        /// </summary>
        /// <value>The parent tab.</value>
        public CommonBaseTab ParentTab
        {
            get;
            set;
        }

        /// <summary>
        /// Gets the template id.
        /// </summary>
        /// <value>The template id.</value>
        public int TemplateId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether control have unsaved data.
        /// </summary>
        /// <value><c>true</c> if control have unsaved data; otherwise, <c>false</c>.</value>
        internal virtual bool HaveUnsavedData { get; set; }

        /// <summary>
        /// Saves the data.
        /// </summary>
        internal virtual bool SaveData()
        {
            return true;
        }

        /// <summary>
        /// Sets the error.
        /// </summary>
        /// <param name="control">The control.</param>
        /// <param name="errorText">The error text.</param>
        internal void SetError(Control control, string errorText)
        {
            if (errorProvider != null && control != null && !string.IsNullOrEmpty(errorText))
            {
                errorProvider.SetError(control, errorText);
            }
        }

        /// <summary>
        /// Clears the errors.
        /// </summary>
        internal void ClearErrors()
        {
            if (errorProvider != null)
            {
                errorProvider.ClearErrors();
            }
        }

        /// <summary>
        /// Loads this instance.
        /// </summary>
        public virtual void LoadData(int templateId, DataTable dtDetails)
        {
            ClearErrors();

            UpdateReadOnlyMode();
        }

        /// <summary>
        /// Validates the data and returns validate metadata.
        /// </summary>
        /// <returns></returns>
        internal virtual ValidateMetadata ValidateData()
        {
            bool isValid = this.ValidateChildren(ValidationConstraints.Selectable);
            if (isValid && errorProvider != null)
            {
                errorProvider.ClearErrors();
            }
            else if (!isValid && ParentTab != null)
            {
                ParentTab.ActivateTab();
            }
            ValidateMetadata result = new ValidateMetadata();
            result.IsCorrect = isValid;
            return result;
        }

        /// <summary>
        /// Gets a value indicating whether this instance is edit mode.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance is edit mode; otherwise, <c>false</c>.
        /// </value>
        protected bool IsEditMode
        {
            get
            {
                return TemplateId > 0;
            }
        }

        /// <summary>
        /// Fires the update main view.
        /// </summary>
        protected void FireUpdateMainView()
        {
            if (UpdateMainView != null)
            {
                UpdateMainView(this, new EventArgs());
            }
        }

        /// <summary>
        /// Updates the read only mode.
        /// </summary>
        public virtual void UpdateReadOnlyMode()
        {
            if (MainData.AccessLevel <= (int)AccessLevelType.ReadOnly)
            {
                SetReadOnlyModeRecursively(this);
            }
        }

        /// <summary>
        /// Sets the read only mode recursively.
        /// </summary>
        /// <param name="currentControl">The current control.</param>
        protected void SetReadOnlyModeRecursively(Control currentControl)
        {
            if (currentControl != null)
            {
                foreach (Control control in currentControl.Controls)
                {
                    if (control != null)
                    {
                        if (control is BaseEdit)
                        {
                            (control as BaseEdit).Properties.AllowFocused = false;
                            (control as BaseEdit).Properties.ReadOnly = true;
                        }
                        else if (control is IControlReadOnly)
                        {
                            (control as IControlReadOnly).ReadOnly = true;
                        }
                        else if (control is GridControl)
                        {
                            GridControl gridControl = control as GridControl;
                            foreach (GridView view in gridControl.Views)
                            {
                                gridControl.UseEmbeddedNavigator = false;
                                foreach (GridColumn column in view.Columns)
                                {
                                    column.OptionsColumn.AllowEdit = false;
                                    column.OptionsColumn.ReadOnly = true;
                                }
                            }
                        }
                        else if (control is TreeList)
                        {
                            TreeList treeList = control as TreeList;
                            foreach (TreeListColumn column in treeList.Columns)
                            {
                                column.OptionsColumn.AllowEdit = false;
                                column.OptionsColumn.ReadOnly = true;
                            }
                        }
                        else if (control.HasChildren)
                        {
                            SetReadOnlyModeRecursively(control);
                        }
                    }
                }
            }
        }
    }
}
