﻿using System;

namespace SoftServe.Reports.MPTemplates
{
    /// <summary>
    /// 
    /// </summary>
    public class CommonBaseExternalData
    {
        public int AccessLevel { get; set; }
        public UserLevelType UserLevel { get; set; }

        public int CurrentActivityCount { get; set; }
        public int ActiveActivityCount { get; set; }
        /// <summary>
        /// Gets or sets activity's template.
        /// </summary>
        public TemplateType ActivityTemplate { get; set; }

        /// <summary>
        /// Checks the access level.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <returns></returns>
        public bool CheckAccessLevel(AccessLevelType type)
        {
           return AccessLevel == (int)type;
        }
    }
}
