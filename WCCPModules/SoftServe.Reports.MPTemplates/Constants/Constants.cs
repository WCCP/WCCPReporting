﻿namespace SoftServe.Reports.MPTemplates
{
    /// <summary>
    /// 
    /// </summary>
    public static class Constants
    {
        public const int TAB_MAX_COUNT = 10;
        public const int OFF_TRADE_CHNL_ID = 1;
        public const string DELIMITER_ID_LIST = ",";

        #region Main template list
        public const string SP_INSERT_TEMPLATE_COMMON = "spDW_MPT_InsertTemplateDetails";
        public const string SP_UPDATE_TEMPLATE_COMMON = "spDW_MPT_UpdateTemplateDetails";

        public const string SP_DELETE_TEMPLATE_COMMON = "spDW_MPT_DeleteTemplateDetails";
        public const string SP_DELETE_TEMPLATE_COMMON_PARAM1 = "Action_ID";

        public const string SP_GET_ACCESS_LEVEL = "spDW_MPT_GetAccessLevel";

        public const string SP_GET_USER_LEVEL = "spDW_GetUserLevel";

        #endregion Main template list

        #region Common details tab
        public const string SP_GET_TEMPLATE_LIST = "spDW_MPT_GetTemplatesList";
        public const string SP_GET_TEMPLATE_LIST_PARAM1 = "ID";

        public const string SP_GET_CHANNEL_LIST = "spDW_ChannelList";

        public const string SP_GET_ACTION_TYPE_LIST = "spDW_MPT_GetActionTypeList";

        public const string SP_GET_ACTION_LEVEL_LIST = "spDW_MPT_GetActionLevelList";

        public const string SP_GET_POSITION_LIST = "spDW_URM_ListPositions";
        public const string SP_GET_POSITION_LIST_PARAM1 = "Position_ID";
        public const string SP_GET_POSITION_LIST_PARAM2 = "User_ID";
        public const string SP_GET_POSITION_LIST_PARAM3 = "isOFF";

        public const string SP_GET_COMMON_VIEW = "spDW_MPT_GetTemplateDetails";
        public const string SP_GET_COMMON_VIEW_PARAM1 = "Action_ID";

        public const string SP_GET_DOCS_LIST = "spDW_MPT_GetPresentationList";
        public const string SP_GET_DOCS_LIST_PARAM1 = "Action_ID";

        public const string SP_GET_DOCS_DATA = "spDW_MPT_GetPresentationData";
        public const string SP_GET_DOCS_DATA_PARAM1 = "Presentation_ID";

        public const string SP_INSERT_DOCS_DATA = "spDW_MPT_InsertPresentation";
        public const string SP_INSERT_DOCS_DATA_PARAM1 = "Action_ID";
        public const string SP_INSERT_DOCS_DATA_PARAM2 = "Presentation_FileName";
        public const string SP_INSERT_DOCS_DATA_PARAM3 = "Presentation_Data";

        public const string SP_UPDATE_DOCS_DATA = "spDW_MPT_UpdatePresentation";
        public const string SP_UPDATE_DOCS_DATA_PARAM1 = "Presentation_ID";
        public const string SP_UPDATE_DOCS_DATA_PARAM2 = "Presentation_FileName";
        public const string SP_UPDATE_DOCS_DATA_PARAM3 = "Presentation_Data";

        public const string SP_DELETE_DOCS_DATA = "spDW_MPT_DeletePresentation";
        public const string SP_DELETE_DOCS_DATA_PARAM1 = "Presentation_ID";
        
        #endregion Common details tab

        #region Goals config tab
        public const string SP_GET_GOAL_TYPES_LIST = "spDW_GetAimTypes";

        public const string SP_GET_AIM_LIST = "spDW_MPT_ListAim";
        public const string SP_GET_AIM_LIST_PARAM1 = "Action_ID";

        public const string SP_GET_SKU_TREE_AIM = "spDW_GetSKUTreeAim2";
        public const string SP_GET_SKU_TREE_AIM_PARAM1 = "Aim_ID";

        public const string SP_GET_KPI_TREE_AIM = "spDW_GetKPITreeAim";
        public const string SP_GET_KPI_TREE_AIM_PARAM1 = "Aim_ID";

        public const string SP_GET_QPQ_TREE_AIM = "spDW_QuestionnaireGetTree";
        public const string SP_GET_QPQ_TREE_AIM_PARAM_ID = "@AimID";
        public const string SP_GET_QPQ_TREE_AIM_PARAM_Type = "@AimTypeID";

        public const string SP_UPDATE_QPQ_AIM = "spDW_QuestionnaireUpdateAim";
        public const string SP_UPDATE_QPQ_AIM_PARAM_ID = "@AimID";
        public const string SP_UPDATE_QPQ_AIM_PARAM_Type = "@AimTypeID";
        public const string SP_UPDATE_QPQ_AIM_DOCITM = "@DocItm";
        public const string SP_UPDATE_QPQ_AIM_ISIN_REVERSE_EXEC = "@IsInReverseExec";

        public const string SP_GET_IPTR_LIST_AIM = "spDW_IPTR_EquipmentClassList";

        public const string SP_GET_IPTR_AIM = "spDW_MPT_GetIPTRAim";
        public const string SP_GET_IPTR_AIM_PARAM1 = "Aim_ID";

        public const string SP_UPDATE_AIM = "spDW_MPT_UpdateAim";
        public const string SP_UPDATE_AIM_PARAM1 = "Action_ID";
        public const string SP_UPDATE_AIM_PARAM2 = "Aim_Name";
        public const string SP_UPDATE_AIM_PARAM3 = "AimType_Id";
        public const string SP_UPDATE_AIM_PARAM4 = "isUseInCompleted";
        public const string SP_UPDATE_AIM_PARAM5 = "isPeriodCalcAim";
        public const string SP_UPDATE_AIM_PARAM6 = "@Aim_Id";
        public const string SP_UPDATE_AIM_PARAM7 = "Id";
        public const string SP_UPDATE_AIM_PARAM8 = "@FactCalcPeriod";
        public const string SP_UPDATE_AIM_PARAM9 = "@FactCalcType";

        public const string SP_UPDATE_AIM_PARAM10 = "@isUseAimValue";
        public const string SP_UPDATE_AIM_PARAM11 = "@IsInReverseExec";
        public const string SP_UPDATE_AIM_PARAM12 = "@isAccPerfOfTargetVal";

        public const string SP_UPDATE_KPI_AIM = "spDW_MPT_UpdateKPIAim";
        public const string SP_UPDATE_KPI_AIM_PARAM1 = "Aim_ID";
        public const string SP_UPDATE_KPI_AIM_PARAM2 = "Data";

        public const string SP_UPDATE_SKU_AIM = "spDW_MPT_UpdateSKUAim2";
        public const string SP_UPDATE_SKU_AIM_PARAM1 = "Aim_Id";
        public const string SP_UPDATE_SKU_AIM_PARAM2 = "Data";

        public const string SP_UPDATE_IPTR_AIM = "spDW_MPT_UpdateIPTRAim";
        public const string SP_UPDATE_IPTR_AIM_PARAM1 = "Aim_Id";
        public const string SP_UPDATE_IPTR_AIM_PARAM2 = "EquipmentClass_ID";

        public const string SP_DELETE_AIM = "spDW_MPT_DeleteAim";
        public const string SP_DELETE_AIM_PARAM1 = "Aim_Id";

        // old version
        public const string SP_GET_SPLIT_TARGET_BY_TIME = "spDW_MPT_GetTargetPeriodsList";

        // old version
        public const string SP_GET_SPLIT_TARGET_BY_POC_TYPE = "spDW_MPT_GetTargetRegionsList";

        #endregion Goals config tab


        // Fields for Tab Common Information
        public const string FIELD_COMMON_TEMPLATE_ID = "Action_ID";
        public const string FIELD_COMMON_CREATOR_NAME = "CreatorName";
        public const string FIELD_COMMON_CREATION_DATE = "CreationDate";
        public const string FIELD_COMMON_APPROVER = "isApproved";

        public const string FIELD_ACTION_NAME = "Action_Name";
        public const string FIELD_ACTION_TYPE = "ActionType_ID";
        public const string FIELD_ACTION_LEVEL = "Action_Level";
        public const string FIELD_ACTION_CHANNEL = "ChanelType_ID";
        public const string FIELD_ACTION_DESCRIPTION = "Action_Description";
        public const string FIELD_ACTION_CHANGE = "DisableChangeActivityDescription";

        public const string FIELD_POSITION_INITIATOR = "Initiator";
        public const string FIELD_POSITION_PLANNER = "Planner";
        public const string FIELD_POSITION_CONTROLLER = "Controller";
        public const string FIELD_POSITION_APPROVER = "Approver";
        public const string FIELD_POSITION_EXECUTOR = "Executor";
        public const string FIELD_POSITION_CHECKER = "OlChecker";

        public const string FIELD_PROMO_CASCADE = "OL_Edit_Cascade";
        public const string FIELD_PROMO_MANUALLY = "OL_Add_Manually";
        public const string FIELD_PROMO_CHECK_TR = "OL_Check_TR";

        public const string FIELD_CONTROL_CASCADE = "Cnt_Edit_Cascade";
        public const string FIELD_CONTROL_MANUALLY = "Cnt_Add_Manually";
        public const string FIELD_CONTROL_CHECK_TR = "Cnt_Check_TR";

        public const string FIELD_KPK_SHOW = "isShowMobile";
        public const string FIELD_KPK_CHECK_DATE = "isCheckDateMobile";
        public const string FIELD_KPK_DESCRIPTION = "MobileCommentTemplate";

        public const string FIELD_PERIODS_MONITORING_PERIOD = "Monitoring_After_Action_Month";
        public const string FIELD_PERIODS_FROM_TR = "isBaseFromTR";

        public const string FIELD_PERIODS_ACTION_START_DAY = "Action_Start_Day";
        public const string FIELD_PERIODS_ACTION_START_MONTH = "Action_Start_Month";
        public const string FIELD_PERIODS_ACTION_END_DAY = "Action_End_Day";
        public const string FIELD_PERIODS_ACTION_END_MONTH = "Action_End_Month";

        public const string FIELD_PERIODS_BASE1_START_DAY = "Base1_Start_Day";
        public const string FIELD_PERIODS_BASE1_START_MONTH = "Base1_Start_Month";
        public const string FIELD_PERIODS_BASE1_START_YEAR_OFFSET = "Base1_Start_Year_OffSet";

        public const string FIELD_PERIODS_BASE1_END_DAY = "Base1_End_Day";
        public const string FIELD_PERIODS_BASE1_END_MONTH = "Base1_End_Month";
        public const string FIELD_PERIODS_BASE1_END_YEAR_OFFSET = "Base1_End_Year_OffSet";

        public const string FIELD_PERIODS_BASE2_START_DAY = "Base2_Start_Day";
        public const string FIELD_PERIODS_BASE2_START_MONTH = "Base2_Start_Month";
        public const string FIELD_PERIODS_BASE2_START_YEAR_OFFSET = "Base2_Start_Year_OffSet";

        public const string FIELD_PERIODS_BASE2_END_DAY = "Base2_End_Day";
        public const string FIELD_PERIODS_BASE2_END_MONTH = "Base2_End_Month";
        public const string FIELD_PERIODS_BASE2_END_YEAR_OFFSET = "Base2_End_Year_OffSet";

        public const string FIELD_GOALS_DISABLE_CHANGE_SKU = "DisableChangeSKU";
        public const string FIELD_GOALS_DISABLE_CHANGE_KPI = "DisableChangeKPI";
        public const string FIELD_GOALS_DISABLE_CHANGE_TYPE = "DivTargetByOlType";

        public const string FIELD_GOALS_TARGET_PERIOD_TYPE = "TargetPeriodType";
        public const string FIELD_GOALS_TARGET_REGION_TYPE = "TargetRegionType";

        public const string FIELD_GOALS_SKU_ID_LIST = "SKUList";
        public const string FIELD_GOALS_KPI_ID_TREE = "KPIList";

        //new
        public const string FIELD_GOALS_ALLOW_MULTIPLE_GOALS = "isEnableMoreOneAim";
        //

        public const int ErrorActivityOnTemplateExists = -1000;
        public const int ErrorTemplateWithThisNameExists = -123;
    }

    /// <summary>
    /// 
    /// </summary>
    public enum TemplateDetailsTabType
    {
        /// <summary>
        /// 
        /// </summary>
        Common = 0,
        /// <summary>
        /// 
        /// </summary>
        Goals = 1
    }

    /// <summary>
    /// 
    /// </summary>
    public enum LevelType
    {
        /// <summary>
        /// 
        /// </summary>
        Unknown = 0,
        /// <summary>
        /// 
        /// </summary>
        M3 = 3,
        /// <summary>
        /// 
        /// </summary>
        Region = 4,
        /// <summary>
        /// 
        /// </summary>
        Country = 5
    }

    /// <summary>
    /// 
    /// </summary>
    public enum SplitPOCByTime
    {
        /// <summary>
        /// 
        /// </summary>
        Monthly = 0,
        /// <summary>
        /// 
        /// </summary>
        Weekly = 1
    }

    /// <summary>
    /// 
    /// </summary>
    public enum SplitPOCByType
    {
        /// <summary>
        /// 
        /// </summary>
        ByRegion = 0,
        /// <summary>
        /// 
        /// </summary>
        ByDistrict = 1,
        /// <summary>
        /// 
        /// </summary>
        ByM3 = 2,
        /// <summary>
        /// 
        /// </summary>
        ByM2 = 3,
        /// <summary>
        /// 
        /// </summary>
        ByPOC = 4
    }


    /// <summary>
    /// 
    /// </summary>
    public enum UserLevelType
    {
        /// <summary>
        /// 
        /// </summary>
        Unknown = 0,
        /// <summary>
        /// 
        /// </summary>
        M2 = 2,
        /// <summary>
        /// 
        /// </summary>
        M3 = 3,
        /// <summary>
        /// 
        /// </summary>
        M4 = 4,
        /// <summary>
        /// 
        /// </summary>
        M5 = 5
    }

    /// <summary>
    /// 
    /// </summary>
    public enum AccessLevelType
    {
        Unknown = -1,
        /// <summary>
        /// 
        /// </summary>
        ReadOnly = 0,
        /// <summary>
        /// 
        /// </summary>
        ReadWrite = 2,
        /// <summary>
        /// 
        /// </summary>
        Approve = 4
    }

    public enum TemplateType
    {
        Unknown = 0,
        PriceActivities = 1,
        TMActivities = 2,
        ThrM3Activities = 3,
        ThrM4Activities = 4
    }
       
    public enum GoalType
    {
        SalesVolume = 1,
        Merchandising = 2,
        Effectiveness = 3,
        OnInvoice = 4,
        QuestionnaireLogic = 5,
        QuestionnaireDigit = 6,
        InBevDoorCount = 7,
        MustStock = 8
    }

    public enum CheckByVisitsType
    {
        ByLastVisit = 0,
        ByMonth = 1
    }

    public enum AimRecordState
    {
        Normal = 0,
        Updated = 1,
        Deleted = 2,
        New = 3,
        Unknown = 4
    }

    public enum FactCalcType
    {
        FactSales = 1,
        FactRemainder =2
    }
}

