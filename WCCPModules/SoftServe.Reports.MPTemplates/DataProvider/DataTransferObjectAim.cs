﻿using System.Data.SqlTypes;

namespace SoftServe.Reports.MPTemplates
{
    public class DataTransferObjectAim
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Type { get; set; }

        public bool IsUseAimValue { get; set; } //Gap 2015_5
        public bool IsAccPerfOfTargetVal { get; set; } //Gap 2015_5
        public int FactCalcWay { get; set; } //Gap 2015_5
        public int FactCalcPeriod { get; set; } //Gap 2015_5


        public bool IsUsedInCalculations { get; set; }
        public bool IsInReverseExecution { get; set; } //Gap 2015_3
        public bool IsPeriodCalc { get; set; }

        public string SelectedIdList { get; set; }
        public SqlXml SelectedXmlList { get; set; }

        public AimRecordState State { get; set; } 
  
        public DataTransferObjectAim()
        {
            this.Id = -1;
            this.Name = string.Empty;
            this.Type = 0;

            this.SelectedIdList = string.Empty;
            this.SelectedXmlList = null;
            
            this.IsUsedInCalculations = false;
            this.IsInReverseExecution = false;
            this.IsUseAimValue = false;
            this.IsAccPerfOfTargetVal = false;
            this.IsPeriodCalc = true;
            this.FactCalcPeriod = 0;
            this.FactCalcWay = 0;
            this.State = AimRecordState.Unknown;

        }

        public DataTransferObjectAim(DataTransferObjectAim other)
        {
            this.Id = other.Id;
            this.Name = other.Name;
            this.Type = other.Type;

            this.SelectedIdList = string.Copy(other.SelectedIdList);
            this.SelectedXmlList = other.SelectedXmlList;

            this.IsUsedInCalculations = other.IsUsedInCalculations;
            this.IsInReverseExecution = other.IsInReverseExecution;
            this.IsUseAimValue = other.IsUseAimValue;
            this.IsAccPerfOfTargetVal = other.IsAccPerfOfTargetVal;
            this.IsPeriodCalc = other.IsPeriodCalc;
            this.FactCalcPeriod = other.FactCalcPeriod;
            this.FactCalcWay = other.FactCalcWay;
            this.State = other.State;
        }

        public void Copy(DataTransferObjectAim other)
        {
            this.Id = other.Id;
            this.Name = string.Copy(other.Name);
            this.Type = other.Type;

            this.SelectedIdList = string.Copy(other.SelectedIdList);
            this.SelectedXmlList = other.SelectedXmlList;

            this.IsUsedInCalculations = other.IsUsedInCalculations;
            this.IsInReverseExecution = other.IsInReverseExecution;
            this.IsUseAimValue = other.IsUseAimValue;
            this.IsAccPerfOfTargetVal = other.IsAccPerfOfTargetVal;
            this.IsPeriodCalc = other.IsPeriodCalc;
            this.FactCalcPeriod = other.FactCalcPeriod;
            this.FactCalcWay = other.FactCalcWay;
            this.State = other.State;
        }

        public bool Equals(DataTransferObjectAim other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return this.Id == other.Id
                   && this.Name == other.Name
                   && this.Type == other.Type
                   && this.SelectedIdList == other.SelectedIdList
                   && this.IsUsedInCalculations == other.IsUsedInCalculations
                   && this.IsInReverseExecution == other.IsInReverseExecution
                   && this.IsUseAimValue == other.IsUseAimValue
                   && this.IsAccPerfOfTargetVal == other.IsAccPerfOfTargetVal
                   && this.IsPeriodCalc == other.IsPeriodCalc
                   && this.FactCalcPeriod == other.FactCalcPeriod
                   && this.FactCalcWay == other.FactCalcWay;
        }
    }
}
