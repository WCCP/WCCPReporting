﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlTypes;
using BLToolkit.Data;
using Logica.Reports.BaseReportControl.Utility;
using Logica.Reports.DataAccess;
using SoftServe.Reports.ContractMgmt;

namespace SoftServe.Reports.MPTemplates
{
    class DataProvider
    {
        private static DbManager _db;

        #region Properties

        private static DbManager Db
        {
            get
            {
                if (_db == null)
                {
                    DbManager.DefaultConfiguration = ""; //to reset possible previous changes
                    DbManager.AddConnectionString(HostConfiguration.DBSqlConnection);
                    _db = new DbManager();
                    _db.Command.CommandTimeout = 15 * 60; // 15 minutes by default
                }
                return _db;
            }
        }

        #endregion

        #region DAL

        /// <summary>
        /// Gets the split targets by time list.
        /// </summary>
        /// <value>The split targets by time list.</value>
        public static DataTable SplitTargetsByTimeList
        {
            get
            {
                try
                {
                    DataTable dataTable = Db.SetSpCommand(Constants.SP_GET_SPLIT_TARGET_BY_TIME).ExecuteDataTable();
                    if (dataTable != null)
                    {
                        return dataTable;
                    }
                }
                catch (Exception ex)
                {
                    ExceptionHandler.HandleException(ex);
                }
                return null;
            }
        }

        /// <summary>
        /// Gets the type of the split targets by poc.
        /// </summary>
        /// <value>The type of the split targets by poc.</value>
        public static DataTable SplitTargetsByPocType
        {
            get
            {
                try
                {
                    DataTable dataTable = Db.SetSpCommand(Constants.SP_GET_SPLIT_TARGET_BY_POC_TYPE).ExecuteDataTable();
                    if (dataTable != null)
                    {
                        return dataTable;
                    }
                }
                catch (Exception ex)
                {
                    ExceptionHandler.HandleException(ex);
                }
                return null;
            }
        }

        /// <summary>
        /// Gets the contract template list.
        /// </summary>
        /// <value>The contract template list.</value>
        public static DataTable ContractTemplateList
        {
            get
            {
                try
                {
                    DataTable dataTable = Db.SetSpCommand(Constants.SP_GET_TEMPLATE_LIST).ExecuteDataTable();
                    if (dataTable != null)
                    {
                        return dataTable;
                    }
                }
                catch (Exception ex)
                {
                    ExceptionHandler.HandleException(ex);
                }
                return null;
            }
        }

        /// <summary>
        /// Gets the channel list.
        /// </summary>
        /// <value>The channel list.</value>
        public static DataTable ChannelList
        {
            get
            {
                try
                {
                    DataTable dataTable = Db.SetSpCommand(Constants.SP_GET_CHANNEL_LIST).ExecuteDataTable();
                    if (dataTable != null)
                    {
                        return dataTable;
                    }
                }
                catch (Exception ex)
                {
                    ExceptionHandler.HandleException(ex);
                }
                return null;
            }
        }

        /// <summary>
        /// Gets the action type list.
        /// </summary>
        /// <value>The action type list.</value>
        public static DataTable ActionTypeList
        {
            get
            {
                try
                {
                    DataTable dataTable = Db.SetSpCommand(Constants.SP_GET_ACTION_TYPE_LIST).ExecuteDataTable();
                    if (dataTable != null)
                    {
                        return dataTable;
                    }
                }
                catch (Exception ex)
                {
                    ExceptionHandler.HandleException(ex);
                }
                return null;
            }
        }

        /// <summary>
        /// Gets the action level list.
        /// </summary>
        /// <value>The action level list.</value>
        public static DataTable ActionLevelList
        {
            get
            {
                try
                {
                    DataTable dataTable = Db.SetSpCommand(Constants.SP_GET_ACTION_LEVEL_LIST).ExecuteDataTable();
                    if (dataTable != null)
                    {
                        return dataTable;
                    }
                }
                catch (Exception ex)
                {
                    ExceptionHandler.HandleException(ex);
                }
                return null;
            }
        }

        /// <summary>
        /// Gets the position list.
        /// </summary>
        /// <value>The position list.</value>
        public static DataTable PositionList
        {
            get
            {
                try
                {
                    DataTable dataTable = Db.SetSpCommand(Constants.SP_GET_POSITION_LIST,
                        Db.Parameter(Constants.SP_GET_POSITION_LIST_PARAM1, DBNull.Value),
                        Db.Parameter(Constants.SP_GET_POSITION_LIST_PARAM2, DBNull.Value),
                        Db.Parameter(Constants.SP_GET_POSITION_LIST_PARAM3, 0)).ExecuteDataTable();
                    if (dataTable != null)
                    {
                        return dataTable;
                    }
                }
                catch (Exception ex)
                {
                    ExceptionHandler.HandleException(ex);
                }
                return null;
            }
        }

        /// <summary>
        /// Gets the common view.
        /// </summary>
        /// <param name="templateId">The template id.</param>
        /// <returns></returns>
        public static DataTable GetTemaplateDetails(int templateId)
        {
            try
            {
                DataTable dataTable = Db.SetSpCommand(Constants.SP_GET_COMMON_VIEW,
                    Db.Parameter(Constants.SP_GET_COMMON_VIEW_PARAM1, templateId)).ExecuteDataTable();

                if (dataTable != null)
                {
                    return dataTable;
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler.HandleException(ex);
            }
            return null;
        }

        /// <summary>
        /// Deletes the template details.
        /// </summary>
        /// <param name="templateId">The template id.</param>
        /// <returns></returns>
        public static int DeleteTemplateDetails(int templateId)
        {
            try
            {
                Object result = Db.SetSpCommand(Constants.SP_DELETE_TEMPLATE_COMMON,
                    Db.Parameter(Constants.SP_DELETE_TEMPLATE_COMMON_PARAM1, templateId)).ExecuteScalar();
                return ConvertEx.ToInt(result);
            }
            catch (Exception ex)
            {
                ExceptionHandler.HandleException(ex);
            }
            return -1;
        }

        /// <summary>
        /// Gets the document list.
        /// </summary>
        /// <param name="templateId">The template id.</param>
        /// <returns></returns>
        public static DataTable GetDocumentList(int templateId)
        {
            try
            {
                DataTable dataTable = Db.SetSpCommand(Constants.SP_GET_DOCS_LIST,
                    Db.Parameter(Constants.SP_GET_DOCS_LIST_PARAM1, templateId)).ExecuteDataTable();

                if (dataTable != null)
                {
                   return dataTable;
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler.HandleException(ex);
            }
            return null;
        }

        /// <summary>
        /// Gets the document.
        /// </summary>
        /// <param name="docId">The doc id.</param>
        /// <returns></returns>
        public static byte[] GetDocument(int docId)
        {
            try
            {
                object objFile = Db.SetSpCommand(Constants.SP_GET_DOCS_DATA,
                    Db.Parameter(Constants.SP_GET_DOCS_DATA_PARAM1, docId)).ExecuteScalar();

                return ConvertEx.ToBytesArray(objFile);
            }
            catch (Exception ex)
            {
                ExceptionHandler.HandleException(ex);
            }
            return null;
        }

        /// <summary>
        /// Inserts the document.
        /// </summary>
        /// <param name="templateId">The template id.</param>
        /// <param name="fileName">Name of the file.</param>
        /// <param name="fileData">The file data.</param>
        /// <returns></returns>
        public static int InsertDocument(int templateId, string fileName, byte[] fileData)
        {
            try
            {
                object result = Db.SetSpCommand(Constants.SP_INSERT_DOCS_DATA,
                    Db.Parameter(Constants.SP_INSERT_DOCS_DATA_PARAM1, templateId),
                    Db.Parameter(Constants.SP_INSERT_DOCS_DATA_PARAM2, fileName),
                    Db.Parameter(Constants.SP_INSERT_DOCS_DATA_PARAM3, fileData)).ExecuteScalar();             

                return ConvertEx.ToInt(result);
            }
            catch (Exception ex)
            {
                ExceptionHandler.HandleException(ex);
            }
            return -1;
        }

        /// <summary>
        /// Deletes the document.
        /// </summary>
        /// <param name="docId">The doc id.</param>
        /// <returns></returns>
        public static int DeleteDocument(int docId)
        {
            try
            {
                object result = Db.SetSpCommand(Constants.SP_DELETE_DOCS_DATA,
                    Db.Parameter(Constants.SP_DELETE_DOCS_DATA_PARAM1, docId)).ExecuteScalar();
            
                return ConvertEx.ToInt(result);
            }
            catch (Exception ex)
            {
                ExceptionHandler.HandleException(ex);
            }
            return -1;
        }

        /// <summary>
        /// Saves the template.
        /// </summary>
        /// <param name="dto">The dto.</param>
        /// <param name="edit">if set to <c>true</c> [edit].</param>
        /// <returns></returns>
        public static int SaveTemplate(TemplateDetailsDTO dto, bool edit)
        {
            try
            {
                Object[] sqlParameters = new Object[]
            {
                Db.Parameter(Constants.FIELD_COMMON_TEMPLATE_ID, dto.FIELD_COMMON_TEMPLATE_ID),
                Db.Parameter(Constants.FIELD_COMMON_APPROVER, dto.FIELD_COMMON_APPROVER),
               
                Db.Parameter(Constants.FIELD_ACTION_NAME, dto.FIELD_ACTION_NAME),
                Db.Parameter(Constants.FIELD_ACTION_TYPE, dto.FIELD_ACTION_TYPE),
                Db.Parameter(Constants.FIELD_ACTION_LEVEL, dto.FIELD_ACTION_LEVEL),
                Db.Parameter(Constants.FIELD_ACTION_CHANNEL, dto.FIELD_ACTION_CHANNEL),
                Db.Parameter(Constants.FIELD_ACTION_DESCRIPTION, dto.FIELD_ACTION_DESCRIPTION),
                Db.Parameter(Constants.FIELD_ACTION_CHANGE, dto.FIELD_ACTION_CHANGE),
                
                Db.Parameter(Constants.FIELD_POSITION_INITIATOR, dto.FIELD_POSITION_INITIATOR),
                Db.Parameter(Constants.FIELD_POSITION_PLANNER, dto.FIELD_POSITION_PLANNER),
                Db.Parameter(Constants.FIELD_POSITION_CONTROLLER, dto.FIELD_POSITION_CONTROLLER),
                Db.Parameter(Constants.FIELD_POSITION_APPROVER, dto.FIELD_POSITION_APPROVER),
                Db.Parameter(Constants.FIELD_POSITION_EXECUTOR, dto.FIELD_POSITION_EXECUTOR),
                Db.Parameter(Constants.FIELD_POSITION_CHECKER, dto.FIELD_POSITION_CHECKER),
                
                Db.Parameter(Constants.FIELD_PROMO_CASCADE, dto.FIELD_PROMO_CASCADE),
                Db.Parameter(Constants.FIELD_PROMO_MANUALLY, dto.FIELD_PROMO_MANUALLY),
                Db.Parameter(Constants.FIELD_PROMO_CHECK_TR, dto.FIELD_PROMO_CHECK_TR),
                
                Db.Parameter(Constants.FIELD_CONTROL_CASCADE, dto.FIELD_CONTROL_CASCADE),
                Db.Parameter(Constants.FIELD_CONTROL_MANUALLY, dto.FIELD_CONTROL_MANUALLY),
                Db.Parameter(Constants.FIELD_CONTROL_CHECK_TR, dto.FIELD_CONTROL_CHECK_TR),
                
                Db.Parameter(Constants.FIELD_KPK_SHOW, dto.FIELD_KPK_SHOW),
                Db.Parameter(Constants.FIELD_KPK_CHECK_DATE, dto.FIELD_KPK_CHECK_DATE),
                Db.Parameter(Constants.FIELD_KPK_DESCRIPTION, dto.FIELD_KPK_DESCRIPTION),
                
                Db.Parameter(Constants.FIELD_PERIODS_FROM_TR, dto.FIELD_PERIODS_FROM_TR),
                Db.Parameter(Constants.FIELD_PERIODS_MONITORING_PERIOD, dto.FIELD_PERIODS_MONITORING_PERIOD),
                
                Db.Parameter(Constants.FIELD_PERIODS_ACTION_START_DAY, dto.FIELD_PERIODS_ACTION_START_DAY),
                Db.Parameter(Constants.FIELD_PERIODS_ACTION_START_MONTH, dto.FIELD_PERIODS_ACTION_START_MONTH),
                Db.Parameter(Constants.FIELD_PERIODS_ACTION_END_DAY, dto.FIELD_PERIODS_ACTION_END_DAY),
                Db.Parameter(Constants.FIELD_PERIODS_ACTION_END_MONTH, dto.FIELD_PERIODS_ACTION_END_MONTH),

                Db.Parameter(Constants.FIELD_PERIODS_BASE1_START_DAY, dto.FIELD_PERIODS_BASE1_START_DAY),
                Db.Parameter(Constants.FIELD_PERIODS_BASE1_START_MONTH, dto.FIELD_PERIODS_BASE1_START_MONTH),
                Db.Parameter(Constants.FIELD_PERIODS_BASE1_START_YEAR_OFFSET, dto.FIELD_PERIODS_BASE1_START_YEAR_OFFSET),

                Db.Parameter(Constants.FIELD_PERIODS_BASE1_END_DAY, dto.FIELD_PERIODS_BASE1_END_DAY),
                Db.Parameter(Constants.FIELD_PERIODS_BASE1_END_MONTH, dto.FIELD_PERIODS_BASE1_END_MONTH),
                Db.Parameter(Constants.FIELD_PERIODS_BASE1_END_YEAR_OFFSET, dto.FIELD_PERIODS_BASE1_END_YEAR_OFFSET),

                Db.Parameter(Constants.FIELD_PERIODS_BASE2_START_DAY, dto.FIELD_PERIODS_BASE2_START_DAY),
                Db.Parameter(Constants.FIELD_PERIODS_BASE2_START_MONTH, dto.FIELD_PERIODS_BASE2_START_MONTH),
                Db.Parameter(Constants.FIELD_PERIODS_BASE2_START_YEAR_OFFSET, dto.FIELD_PERIODS_BASE2_START_YEAR_OFFSET),

                Db.Parameter(Constants.FIELD_PERIODS_BASE2_END_DAY, dto.FIELD_PERIODS_BASE2_END_DAY),
                Db.Parameter(Constants.FIELD_PERIODS_BASE2_END_MONTH, dto.FIELD_PERIODS_BASE2_END_MONTH),
                Db.Parameter(Constants.FIELD_PERIODS_BASE2_END_YEAR_OFFSET, dto.FIELD_PERIODS_BASE2_END_YEAR_OFFSET),

                Db.Parameter(Constants.FIELD_GOALS_DISABLE_CHANGE_SKU, dto.FIELD_GOALS_DISABLE_CHANGE_SKU),
                Db.Parameter(Constants.FIELD_GOALS_DISABLE_CHANGE_KPI, dto.FIELD_GOALS_DISABLE_CHANGE_KPI),
                Db.Parameter(Constants.FIELD_GOALS_DISABLE_CHANGE_TYPE, dto.FIELD_GOALS_DISABLE_CHANGE_TYPE),

                Db.Parameter(Constants.FIELD_GOALS_TARGET_PERIOD_TYPE, dto.FIELD_GOALS_TARGET_PERIOD_TYPE),
                Db.Parameter(Constants.FIELD_GOALS_TARGET_REGION_TYPE, dto.FIELD_GOALS_TARGET_REGION_TYPE),

                Db.Parameter(Constants.FIELD_GOALS_ALLOW_MULTIPLE_GOALS, dto.FIELD_GOALS_ALLOW_MULTIPLE_GOALS)
            };

                int templateId;
                if (!edit)// new template
                {
                    templateId = ConvertEx.ToInt(Db.SetSpCommand(Constants.SP_INSERT_TEMPLATE_COMMON, sqlParameters).ExecuteScalar());
                }
                else
                {
                    templateId = ConvertEx.ToInt(Db.SetSpCommand(Constants.SP_UPDATE_TEMPLATE_COMMON, sqlParameters).ExecuteScalar());
                }

                return templateId;
            }
            catch (Exception ex)
            {
                ExceptionHandler.HandleException(ex);
            }
            return -1;
        }

        /// <summary>
        /// Gets the access level.
        /// </summary>
        /// <value>The access level.</value>
        internal static int AccessLevel
        {
            get
            {
                return ConvertEx.ToInt(Db.SetSpCommand(Constants.SP_GET_ACCESS_LEVEL).ExecuteScalar());
            }
        }

        /// <summary>
        /// Gets the user level. (M2, 3, 4...?)
        /// </summary>
        /// <value>The user level.</value>
        internal static UserLevelType UserLevel
        {
            get
            {
                return (UserLevelType)ConvertEx.ToInt(Db.SetSpCommand(Constants.SP_GET_USER_LEVEL).ExecuteScalar());
            }
        }

        /// <summary>
        /// Gets the channel list.
        /// </summary>
        /// <value>the channel list.</value>
        public static DataTable GoalTypesList
        {
            get
            {
                try
                {
                    DataTable dataTable = Db.SetSpCommand(Constants.SP_GET_GOAL_TYPES_LIST).ExecuteDataTable();
                    if (dataTable != null)
                    {
                       return dataTable;
                    }
                }
                catch (Exception ex)
                {
                    ExceptionHandler.HandleException(ex);
                }
                return null;
            }
        }

        /// <summary>
        /// Returns list of all aim in the current template
        /// </summary>
        /// <param name="templateId">template Id</param>
        /// <returns>list of all aim in the current template</returns>
        public static DataTable GetAimList(int templateId)
        {
            try
            {
                object objTemplateId = DBNull.Value;
                if (templateId > 0)
                {
                    objTemplateId = templateId;
                }

                     DataTable dataTable = Db.SetSpCommand(Constants.SP_GET_AIM_LIST,
                    Db.Parameter(Constants.SP_GET_AIM_LIST_PARAM1, objTemplateId)).ExecuteDataTable();

                     if (dataTable != null)
                     {
                         return dataTable;
                     }
            }
            catch (Exception ex)
            {
                ExceptionHandler.HandleException(ex);
            }
            return null;
        }

        /// <summary>
        /// Gets the SKU tree
        /// </summary>
        /// <param name="templateId">The template id.</param>
        /// <returns>the SKU tree</returns>
        public static DataTable GetSKUTree(int aimId)
        {
            try
            {
                object objAimId = DBNull.Value;
                if (aimId > 0)
                {
                    objAimId = aimId;
                }

                DataTable dataTable = Db.SetSpCommand(Constants.SP_GET_SKU_TREE_AIM,
                    Db.Parameter(Constants.SP_GET_SKU_TREE_AIM_PARAM1, objAimId)).ExecuteDataTable();

                if (dataTable != null)
                {
                    return dataTable;
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler.HandleException(ex);
            }
            return null;
        }

        /// <summary>
        /// Gets the KPI tree
        /// </summary>
        /// <param name="templateId">template id</param>
        /// <returns>KPI tree</returns>
        public static DataTable GetKPITree(int aimId)
        {
            try
            {
                object objAimId = DBNull.Value;
                if (aimId > 0)
                {
                    objAimId = aimId;
                }

                DataTable dataTable = Db.SetSpCommand(Constants.SP_GET_KPI_TREE_AIM,
                    Db.Parameter(Constants.SP_GET_KPI_TREE_AIM_PARAM1, objAimId)).ExecuteDataTable();

                if (dataTable != null)
                {
                    return dataTable;
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler.HandleException(ex);
            }
            return null;
        }

        /// <summary>
        /// Gets the QPQ tree
        /// </summary>
        /// <param name="templateId">template id</param>
        /// <returns>QPQ tree</returns>
        public static DataTable GetQPQTree(int aimId, int qpqType)
        {
            try
            {
                object objAimId = DBNull.Value;
                if (aimId > 0)
                {
                    objAimId = aimId;
                }

                DataTable dataTable = Db.SetSpCommand(Constants.SP_GET_QPQ_TREE_AIM,
                    Db.Parameter(Constants.SP_GET_QPQ_TREE_AIM_PARAM_ID, objAimId),
                    Db.Parameter(Constants.SP_GET_QPQ_TREE_AIM_PARAM_Type, qpqType)).ExecuteDataTable();

                if (dataTable != null)
                {
                   return dataTable;
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler.HandleException(ex);
            }
            return null;
        }

        /// <summary>
        /// List of IPTR conditions
        /// </summary>
        /// <returns>list of IPTR conditions</returns>
        public static DataTable GetIPTRList()
        {
            try
            {
                DataTable dataTable = Db.SetSpCommand(Constants.SP_GET_IPTR_LIST_AIM).ExecuteDataTable();

                if (dataTable != null)
                {
                    return dataTable;
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler.HandleException(ex);
            }
            return null;
        }

        /// <summary>
        /// Gets the IPTR aim
        /// </summary>
        /// <param name="aimId">aim id</param>
        /// <returns>IPTR aim</returns>
        public static DataTable GetIPTRAim(int aimId)
        {
            try
            {
                object objAimId = 0;

                if (aimId > 0)
                {
                    objAimId = aimId;
                }

                DataTable dataTable = Db.SetSpCommand(Constants.SP_GET_IPTR_AIM,
                    Db.Parameter(Constants.SP_GET_IPTR_AIM_PARAM1, objAimId)).ExecuteDataTable();
                if (dataTable != null)
                {
                    return dataTable;
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler.HandleException(ex);
            }
            return null;
        }

        /// <summary>
        /// Gets the MustStock SKU tree
        /// </summary>
        /// <param name="templateId">template id</param>
        /// <returns>QPQ tree</returns>
        public static DataTable GetMustStockTree(int aimId)
        {
            try
            {
                object objAimId = DBNull.Value;
                if (aimId > 0)
                {
                    objAimId = aimId;
                }

                DataTable dataTable = Db.SetSpCommand(Constants.SP_GET_SKU_TREE_AIM,
                    Db.Parameter(Constants.SP_GET_SKU_TREE_AIM_PARAM1, objAimId)).ExecuteDataTable();

                if (dataTable != null)
                {
                    return dataTable;
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler.HandleException(ex);
            }
            return null;
        }
        
        /// <summary>
        /// Updates aim
        /// </summary>
        /// <param name="templateId">template Id</param>
        /// <param name="aimName">aim name</param>
        /// <param name="aimType">aim type</param>
        /// <param name="isUsedInComlpeted">whether is used in comlpeted</param>
        /// <param name="isPeriodCalcAim">whether is period calculated aim</param>
        /// <param name="aimId">aim Id</param>
        /// <returns>aim Id</returns>
        public static int UpdateAim(int templateId, string aimName, int aimType, bool isUsedInComlpeted, bool isPeriodCalcAim, int aimId,
            int factCalcPeriod, int factCalcWay, bool isUseAimValue, bool isInReverseExecution, bool isAccPerfOfTargetVal)
        {
            object objAimId = DBNull.Value;
            if (aimId > 0)
            {
                objAimId = aimId; 
            }

            try
            {
                Db.SetSpCommand(Constants.SP_UPDATE_AIM,
                    Db.Parameter(Constants.SP_UPDATE_AIM_PARAM1, templateId),
                    Db.Parameter(Constants.SP_UPDATE_AIM_PARAM2, aimName),
                    Db.Parameter(Constants.SP_UPDATE_AIM_PARAM3, aimType),
                    Db.Parameter(Constants.SP_UPDATE_AIM_PARAM4, isUsedInComlpeted),
                    Db.Parameter(Constants.SP_UPDATE_AIM_PARAM5, isPeriodCalcAim),
                    Db.Parameter(ParameterDirection.Output, Constants.SP_UPDATE_AIM_PARAM6, DbType.Int32),
                    Db.Parameter(Constants.SP_UPDATE_AIM_PARAM7, objAimId),
                    Db.Parameter(Constants.SP_UPDATE_AIM_PARAM8, aimType == (int) GoalType.MustStock ? factCalcPeriod:(object)DBNull.Value),
                    Db.Parameter(Constants.SP_UPDATE_AIM_PARAM9, aimType == (int)GoalType.MustStock? factCalcWay:(object)DBNull.Value),
                    Db.Parameter(Constants.SP_UPDATE_AIM_PARAM10, aimType == (int)GoalType.Merchandising ? isUseAimValue : (object)DBNull.Value),
                    Db.Parameter(Constants.SP_UPDATE_AIM_PARAM11, aimType == (int)GoalType.Merchandising ? isInReverseExecution : (object)DBNull.Value),
                    Db.Parameter(Constants.SP_UPDATE_AIM_PARAM12, aimType == (int)GoalType.Merchandising ? isAccPerfOfTargetVal : (object)DBNull.Value)
                    ).ExecuteScalar();
                object val = Db.Command.Parameters[Constants.SP_UPDATE_AIM_PARAM6];
                return ConvertEx.ToInt(((IDataParameter)val).Value);
            }
            catch (Exception ex)
            {
                ExceptionHandler.HandleException(ex);
            }

            return -1;
        }

        /// <summary>
        /// Updates KPI aim
        /// </summary>
        /// <param name="aimId">aim Id</param>
        /// <param name="data">selected items list</param>
        /// <returns>aim Id</returns>
        public static int UpdateKPIAim(int aimId, SqlXml data)
        {
            object objAimId = DBNull.Value;
            if (aimId > 0)
            {
                objAimId = aimId;
            }

            try
            {
                object result = Db.SetSpCommand(Constants.SP_UPDATE_KPI_AIM,
                    Db.Parameter(Constants.SP_UPDATE_KPI_AIM_PARAM1, objAimId),
                    Db.Parameter(Constants.SP_UPDATE_KPI_AIM_PARAM2, data)).ExecuteScalar();

                return ConvertEx.ToInt(result);
            }
            catch (Exception ex)
            {
                ExceptionHandler.HandleException(ex);
            }

            return -1;
        }

        /// <summary>
        /// Updates SKU aim
        /// </summary>
        /// <param name="aimId">aim Id</param>
        /// <param name="SKUlist">selected items list</param>
        /// <returns></returns>
        public static int UpdateSKUAim(int aimId, SqlXml SKUlist)
        {
            object objAimId = DBNull.Value;
            if (aimId > 0)
            {
                objAimId = aimId;
            }

            try
            {
                object result = Db.SetSpCommand(Constants.SP_UPDATE_SKU_AIM,
                    Db.Parameter(Constants.SP_UPDATE_SKU_AIM_PARAM1, objAimId),
                    Db.Parameter(Constants.SP_UPDATE_SKU_AIM_PARAM2, SKUlist)).ExecuteScalar();

                return ConvertEx.ToInt(result);
            }
            catch (Exception ex)
            {
                ExceptionHandler.HandleException(ex);
            }

            return -1;
        }

        /// <summary>
        /// Updates IPTR aim
        /// </summary>
        /// <param name="aimId">aim Id</param>
        /// <param name="equipmentClass">equipment class</param>
        /// <returns>aim Id</returns>
        public static int UpdateIPTRAim(int aimId, int equipmentClass)
        {
            object objAimId = DBNull.Value;
            if (aimId > 0)
            {
                objAimId = aimId;
            }

            try
            {
                object result = Db.SetSpCommand(Constants.SP_UPDATE_IPTR_AIM,
                    Db.Parameter(Constants.SP_UPDATE_IPTR_AIM_PARAM1, objAimId),
                    Db.Parameter(Constants.SP_UPDATE_IPTR_AIM_PARAM2, equipmentClass)).ExecuteScalar();

                return ConvertEx.ToInt(result);
            }
            catch (Exception ex)
            {
                ExceptionHandler.HandleException(ex);
            }

            return -1;
        }

        /// <summary>
        /// Updates QPQ aim
        /// </summary>
        /// <param name="aimId">aim Id</param>
        /// <param name="aimType"> 1 if logic, 2 if digit</param>
        /// <param name="selectedId"> id if checked item in UI</param>>
        /// <returns>aim Id</returns>
        internal static int UpdateQPQAim(int aimId, int aimType, bool isInReverseExec, string selectedId)
        {
            object objAimId = DBNull.Value;
            if (aimId > 0)
            {
                objAimId = aimId;
            }

            try
            {
                object result = Db.SetSpCommand(Constants.SP_UPDATE_QPQ_AIM,
                    Db.Parameter(Constants.SP_UPDATE_QPQ_AIM_PARAM_ID, objAimId),
                    Db.Parameter(Constants.SP_UPDATE_QPQ_AIM_PARAM_Type, aimType),
                    Db.Parameter(Constants.SP_UPDATE_QPQ_AIM_ISIN_REVERSE_EXEC, isInReverseExec),
                    Db.Parameter(Constants.SP_UPDATE_QPQ_AIM_DOCITM, ConvertEx.ToGuid(selectedId), DbType.Guid)).ExecuteScalar();

                return ConvertEx.ToInt(result);
            }
            catch (Exception ex)
            {
                ExceptionHandler.HandleException(ex);
            }

            return -1;
        }

        /// <summary>
        /// Updates QPQ aim
        /// </summary>
        /// <param name="aimId">aim Id</param>
        /// <param name="aimType"> 1 if logic, 2 if digit</param>
        /// <param name="selectedId"> id if checked item in UI</param>>
        /// <returns>aim Id</returns>
        internal static int UpdateQPQAim(int aimId, int aimType, string selectedId)
        {
            object objAimId = DBNull.Value;
            if (aimId > 0)
            {
                objAimId = aimId;
            }

            try
            {
                object result = Db.SetSpCommand(Constants.SP_UPDATE_QPQ_AIM,
                    Db.Parameter(Constants.SP_UPDATE_QPQ_AIM_PARAM_ID, objAimId),
                    Db.Parameter(Constants.SP_UPDATE_QPQ_AIM_PARAM_Type, aimType),
                    Db.Parameter(Constants.SP_UPDATE_QPQ_AIM_DOCITM, ConvertEx.ToGuid(selectedId),DbType.Guid)).ExecuteScalar();

                return ConvertEx.ToInt(result);
            }
            catch (Exception ex)
            {
                ExceptionHandler.HandleException(ex);
            }

            return -1;
        }

        /// <summary>
        /// Deletes aim
        /// </summary>
        /// <param name="aimId">aim Id</param>
        public static void DeleteAim(int aimId)
        {
            try
            {
                object objAimId = 0;
                if (aimId > 0)
                {
                    objAimId = aimId;
                }

                Db.SetSpCommand(Constants.SP_DELETE_AIM,
                    Db.Parameter(Constants.SP_DELETE_AIM_PARAM1, objAimId)).ExecuteDataTable();
            }

            catch (Exception ex)
            {
                ExceptionHandler.HandleException(ex);
            }

        }
        #endregion

    }

}
