﻿using System;
using System.Data.SqlTypes;

namespace SoftServe.Reports.MPTemplates
{
    /// <summary>
    /// 
    /// </summary>
    public class TemplateDetailsDTO
    {
        public int FIELD_COMMON_TEMPLATE_ID { get; set; }

        public string FIELD_COMMON_CREATOR_NAME { get; set; }
        public DateTime FIELD_COMMON_CREATION_DATE { get; set; }
        public bool FIELD_COMMON_APPROVER { get; set; }

        public string FIELD_ACTION_NAME { get; set; }
        public int FIELD_ACTION_TYPE { get; set; }
        public int FIELD_ACTION_LEVEL { get; set; }
        public int FIELD_ACTION_CHANNEL { get; set; }
        public string FIELD_ACTION_DESCRIPTION { get; set; }
        public bool FIELD_ACTION_CHANGE { get; set; }

        public int FIELD_POSITION_INITIATOR { get; set; }
        public int FIELD_POSITION_PLANNER { get; set; }
        public int FIELD_POSITION_CONTROLLER { get; set; }
        public int FIELD_POSITION_APPROVER { get; set; }
        public int FIELD_POSITION_EXECUTOR { get; set; }
        public int FIELD_POSITION_CHECKER { get; set; }

        public bool FIELD_PROMO_CASCADE { get; set; }
        public bool FIELD_PROMO_MANUALLY { get; set; }
        public bool FIELD_PROMO_CHECK_TR { get; set; }

        public bool FIELD_CONTROL_CASCADE { get; set; }
        public bool FIELD_CONTROL_MANUALLY { get; set; }
        public bool FIELD_CONTROL_CHECK_TR { get; set; }

        public bool FIELD_KPK_SHOW { get; set; }
        public bool FIELD_KPK_CHECK_DATE { get; set; }
        public string FIELD_KPK_DESCRIPTION { get; set; }

        public bool FIELD_PERIODS_FROM_TR { get; set; }
        public int FIELD_PERIODS_MONITORING_PERIOD { get; set; }

        public int FIELD_PERIODS_ACTION_START_DAY { get; set; }
        public int FIELD_PERIODS_ACTION_START_MONTH { get; set; }
        public int FIELD_PERIODS_ACTION_END_DAY { get; set; }
        public int FIELD_PERIODS_ACTION_END_MONTH { get; set; }

        public int FIELD_PERIODS_BASE1_START_DAY { get; set; }
        public int FIELD_PERIODS_BASE1_START_MONTH { get; set; }
        public int FIELD_PERIODS_BASE1_START_YEAR_OFFSET { get; set; }

        public int FIELD_PERIODS_BASE1_END_DAY { get; set; }
        public int FIELD_PERIODS_BASE1_END_MONTH { get; set; }
        public int FIELD_PERIODS_BASE1_END_YEAR_OFFSET { get; set; }

        public int FIELD_PERIODS_BASE2_START_DAY { get; set; }
        public int FIELD_PERIODS_BASE2_START_MONTH { get; set; }
        public int FIELD_PERIODS_BASE2_START_YEAR_OFFSET { get; set; }

        public int FIELD_PERIODS_BASE2_END_DAY { get; set; }
        public int FIELD_PERIODS_BASE2_END_MONTH { get; set; }
        public int FIELD_PERIODS_BASE2_END_YEAR_OFFSET { get; set; }

        public bool FIELD_GOALS_DISABLE_CHANGE_SKU { get; set; }
        public bool FIELD_GOALS_DISABLE_CHANGE_KPI { get; set; }
        public bool FIELD_GOALS_DISABLE_CHANGE_TYPE { get; set; }

        public int FIELD_GOALS_TARGET_PERIOD_TYPE { get; set; }
        public int FIELD_GOALS_TARGET_REGION_TYPE { get; set; }

        //new
        public bool FIELD_GOALS_ALLOW_MULTIPLE_GOALS { get; set; }

       public bool Equals(TemplateDetailsDTO other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return other.FIELD_COMMON_TEMPLATE_ID == FIELD_COMMON_TEMPLATE_ID
                && Equals(other.FIELD_COMMON_CREATOR_NAME, FIELD_COMMON_CREATOR_NAME)
                //&& other.FIELD_COMMON_CREATION_DATE.Equals(FIELD_COMMON_CREATION_DATE) 
                && other.FIELD_COMMON_APPROVER.Equals(FIELD_COMMON_APPROVER)
                && Equals(other.FIELD_ACTION_NAME, FIELD_ACTION_NAME)
                && other.FIELD_ACTION_TYPE == FIELD_ACTION_TYPE
                && other.FIELD_ACTION_LEVEL == FIELD_ACTION_LEVEL
                && other.FIELD_ACTION_CHANNEL == FIELD_ACTION_CHANNEL
                && Equals(other.FIELD_ACTION_DESCRIPTION, FIELD_ACTION_DESCRIPTION)
                && other.FIELD_ACTION_CHANGE.Equals(FIELD_ACTION_CHANGE)
                && other.FIELD_POSITION_INITIATOR == FIELD_POSITION_INITIATOR
                && other.FIELD_POSITION_PLANNER == FIELD_POSITION_PLANNER
                && other.FIELD_POSITION_CONTROLLER == FIELD_POSITION_CONTROLLER
                && other.FIELD_POSITION_APPROVER == FIELD_POSITION_APPROVER
                && other.FIELD_POSITION_EXECUTOR == FIELD_POSITION_EXECUTOR
                && other.FIELD_POSITION_CHECKER == FIELD_POSITION_CHECKER
                && other.FIELD_PROMO_CASCADE.Equals(FIELD_PROMO_CASCADE)
                && other.FIELD_PROMO_MANUALLY.Equals(FIELD_PROMO_MANUALLY)
                && other.FIELD_PROMO_CHECK_TR.Equals(FIELD_PROMO_CHECK_TR)
                && other.FIELD_CONTROL_CASCADE.Equals(FIELD_CONTROL_CASCADE)
                && other.FIELD_CONTROL_MANUALLY.Equals(FIELD_CONTROL_MANUALLY)
                && other.FIELD_CONTROL_CHECK_TR.Equals(FIELD_CONTROL_CHECK_TR)
                && other.FIELD_KPK_SHOW.Equals(FIELD_KPK_SHOW)
                && other.FIELD_KPK_CHECK_DATE.Equals(FIELD_KPK_CHECK_DATE)
                && Equals(other.FIELD_KPK_DESCRIPTION, FIELD_KPK_DESCRIPTION)
                && other.FIELD_PERIODS_FROM_TR.Equals(FIELD_PERIODS_FROM_TR)
                && other.FIELD_PERIODS_MONITORING_PERIOD == FIELD_PERIODS_MONITORING_PERIOD
                && other.FIELD_PERIODS_ACTION_START_DAY == FIELD_PERIODS_ACTION_START_DAY
                && other.FIELD_PERIODS_ACTION_START_MONTH == FIELD_PERIODS_ACTION_START_MONTH
                && other.FIELD_PERIODS_ACTION_END_DAY == FIELD_PERIODS_ACTION_END_DAY
                && other.FIELD_PERIODS_ACTION_END_MONTH == FIELD_PERIODS_ACTION_END_MONTH
                && other.FIELD_PERIODS_BASE1_START_DAY == FIELD_PERIODS_BASE1_START_DAY
                && other.FIELD_PERIODS_BASE1_START_MONTH == FIELD_PERIODS_BASE1_START_MONTH
                && other.FIELD_PERIODS_BASE1_START_YEAR_OFFSET == FIELD_PERIODS_BASE1_START_YEAR_OFFSET
                && other.FIELD_PERIODS_BASE1_END_DAY == FIELD_PERIODS_BASE1_END_DAY
                && other.FIELD_PERIODS_BASE1_END_MONTH == FIELD_PERIODS_BASE1_END_MONTH
                && other.FIELD_PERIODS_BASE1_END_YEAR_OFFSET == FIELD_PERIODS_BASE1_END_YEAR_OFFSET
                && other.FIELD_PERIODS_BASE2_START_DAY == FIELD_PERIODS_BASE2_START_DAY
                && other.FIELD_PERIODS_BASE2_START_MONTH == FIELD_PERIODS_BASE2_START_MONTH
                && other.FIELD_PERIODS_BASE2_START_YEAR_OFFSET == FIELD_PERIODS_BASE2_START_YEAR_OFFSET
                && other.FIELD_PERIODS_BASE2_END_DAY == FIELD_PERIODS_BASE2_END_DAY
                && other.FIELD_PERIODS_BASE2_END_MONTH == FIELD_PERIODS_BASE2_END_MONTH
                && other.FIELD_PERIODS_BASE2_END_YEAR_OFFSET == FIELD_PERIODS_BASE2_END_YEAR_OFFSET
                && other.FIELD_GOALS_DISABLE_CHANGE_SKU.Equals(FIELD_GOALS_DISABLE_CHANGE_SKU)
                && other.FIELD_GOALS_DISABLE_CHANGE_KPI.Equals(FIELD_GOALS_DISABLE_CHANGE_KPI)
                && other.FIELD_GOALS_DISABLE_CHANGE_TYPE.Equals(FIELD_GOALS_DISABLE_CHANGE_TYPE)
                && other.FIELD_GOALS_TARGET_PERIOD_TYPE == FIELD_GOALS_TARGET_PERIOD_TYPE
                && other.FIELD_GOALS_TARGET_REGION_TYPE == FIELD_GOALS_TARGET_REGION_TYPE
                && other.FIELD_GOALS_ALLOW_MULTIPLE_GOALS == FIELD_GOALS_ALLOW_MULTIPLE_GOALS;
                //&& Equals(other.FIELD_GOALS_SKU_ID_LIST, FIELD_GOALS_SKU_ID_LIST) 
                //&& Equals(other.FIELD_GOALS_KPI_ID_TREE.Value, FIELD_GOALS_KPI_ID_TREE.Value);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != typeof (TemplateDetailsDTO)) return false;
            return Equals((TemplateDetailsDTO) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int result = FIELD_COMMON_TEMPLATE_ID;
                result = (result*397) ^ (FIELD_COMMON_CREATOR_NAME != null ? FIELD_COMMON_CREATOR_NAME.GetHashCode() : 0);
                //result = (result*397) ^ FIELD_COMMON_CREATION_DATE.GetHashCode();
                result = (result*397) ^ FIELD_COMMON_APPROVER.GetHashCode();
                result = (result*397) ^ (FIELD_ACTION_NAME != null ? FIELD_ACTION_NAME.GetHashCode() : 0);
                result = (result*397) ^ FIELD_ACTION_TYPE;
                result = (result*397) ^ FIELD_ACTION_LEVEL;
                result = (result*397) ^ FIELD_ACTION_CHANNEL;
                result = (result*397) ^ (FIELD_ACTION_DESCRIPTION != null ? FIELD_ACTION_DESCRIPTION.GetHashCode() : 0);
                result = (result*397) ^ FIELD_ACTION_CHANGE.GetHashCode();
                result = (result*397) ^ FIELD_POSITION_INITIATOR;
                result = (result*397) ^ FIELD_POSITION_PLANNER;
                result = (result*397) ^ FIELD_POSITION_CONTROLLER;
                result = (result*397) ^ FIELD_POSITION_APPROVER;
                result = (result*397) ^ FIELD_POSITION_EXECUTOR;
                result = (result*397) ^ FIELD_POSITION_CHECKER;
                result = (result*397) ^ FIELD_PROMO_CASCADE.GetHashCode();
                result = (result*397) ^ FIELD_PROMO_MANUALLY.GetHashCode();
                result = (result*397) ^ FIELD_PROMO_CHECK_TR.GetHashCode();
                result = (result*397) ^ FIELD_CONTROL_CASCADE.GetHashCode();
                result = (result*397) ^ FIELD_CONTROL_MANUALLY.GetHashCode();
                result = (result*397) ^ FIELD_CONTROL_CHECK_TR.GetHashCode();
                result = (result*397) ^ FIELD_KPK_SHOW.GetHashCode();
                result = (result*397) ^ FIELD_KPK_CHECK_DATE.GetHashCode();
                result = (result*397) ^ (FIELD_KPK_DESCRIPTION != null ? FIELD_KPK_DESCRIPTION.GetHashCode() : 0);
                result = (result*397) ^ FIELD_PERIODS_FROM_TR.GetHashCode();
                result = (result*397) ^ FIELD_PERIODS_MONITORING_PERIOD;
                result = (result*397) ^ FIELD_PERIODS_ACTION_START_DAY;
                result = (result*397) ^ FIELD_PERIODS_ACTION_START_MONTH;
                result = (result*397) ^ FIELD_PERIODS_ACTION_END_DAY;
                result = (result*397) ^ FIELD_PERIODS_ACTION_END_MONTH;
                result = (result*397) ^ FIELD_PERIODS_BASE1_START_DAY;
                result = (result*397) ^ FIELD_PERIODS_BASE1_START_MONTH;
                result = (result*397) ^ FIELD_PERIODS_BASE1_START_YEAR_OFFSET;
                result = (result*397) ^ FIELD_PERIODS_BASE1_END_DAY;
                result = (result*397) ^ FIELD_PERIODS_BASE1_END_MONTH;
                result = (result*397) ^ FIELD_PERIODS_BASE1_END_YEAR_OFFSET;
                result = (result*397) ^ FIELD_PERIODS_BASE2_START_DAY;
                result = (result*397) ^ FIELD_PERIODS_BASE2_START_MONTH;
                result = (result*397) ^ FIELD_PERIODS_BASE2_START_YEAR_OFFSET;
                result = (result*397) ^ FIELD_PERIODS_BASE2_END_DAY;
                result = (result*397) ^ FIELD_PERIODS_BASE2_END_MONTH;
                result = (result*397) ^ FIELD_PERIODS_BASE2_END_YEAR_OFFSET;
                result = (result*397) ^ FIELD_GOALS_DISABLE_CHANGE_SKU.GetHashCode();
                result = (result*397) ^ FIELD_GOALS_DISABLE_CHANGE_KPI.GetHashCode();
                result = (result*397) ^ FIELD_GOALS_DISABLE_CHANGE_TYPE.GetHashCode();
                result = (result*397) ^ FIELD_GOALS_TARGET_PERIOD_TYPE;
                result = (result*397) ^ FIELD_GOALS_TARGET_REGION_TYPE;
                //result = (result*397) ^ (FIELD_GOALS_SKU_ID_LIST != null ? FIELD_GOALS_SKU_ID_LIST.GetHashCode() : 0);
                //result = (result*397) ^ (FIELD_GOALS_KPI_ID_TREE != null ? FIELD_GOALS_KPI_ID_TREE.GetHashCode() : 0);
                return result;
            }
        }
    }
}
