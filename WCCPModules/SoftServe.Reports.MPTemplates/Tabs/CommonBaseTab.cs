﻿using System;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraTab;
using Logica.Reports.BaseReportControl;
using Logica.Reports.Common.WaitWindow;
using Logica.Reports.ConfigXmlParser.Model;

namespace SoftServe.Reports.MPTemplates
{
    public class CommonBaseTab : XtraTabPage
    {
        #region Fields

        private Guid tabId = Guid.NewGuid();

        #endregion

        #region Instance Properties

        /// <summary>
        ///   Gets or sets the bruc.
        /// </summary>
        /// <value>The bruc.</value>
        public BaseReportUserControl Bruc { get; set; }

        /// <summary>
        ///   Gets the tab id.
        /// </summary>
        /// <value>The tab id.</value>
        public Guid TabId
        {
            get { return tabId; }
        }

        /// <summary>
        ///   Gets or sets the user control.
        /// </summary>
        /// <value>The user control.</value>
        public CommonBaseControl UserControl { get; set; }

        /// <summary>
        ///   Gets or sets the sheet settings.
        /// </summary>
        /// <value>The sheet settings.</value>
        protected SheetParamCollection SheetSettings { get; set; }

        #endregion

        #region Instance Methods

        /// <summary>
        ///   Loads the data.
        /// </summary>
        public virtual void LoadData()
        {
        }

        /// <summary>
        ///   Activates the tab.
        /// </summary>
        public void ActivateTab()
        {
            this.TabControl.SelectedTabPage = this;
        }

        /// <summary>
        ///   Closes the tab.
        /// </summary>
        public void CloseTab()
        {
            if (Bruc != null)
            {
                Bruc.TabControl.TabPages.Remove(this);
            }
        }

        /// <summary>
        ///   Inits the specified bruc.
        /// </summary>
        /// <param name = "bruc">The bruc.</param>
        /// <param name = "userControl">The user control.</param>
        /// <param name = "tabName">Name of the tab.</param>
        public void Init(BaseReportUserControl bruc, CommonBaseControl userControl, string tabName)
        {
            if (!this.DesignMode)
            {
                Bruc = bruc;
                Text = tabName;
                LoadControl(userControl);

                if (Bruc != null)
                {
                    Bruc.RefreshClick += parent_RefreshClick;
                    Bruc.MenuButtonsRendering += (x, args) =>
                                                     {
                                                         if (args.SelectedPage == this)
                                                         {
                                                             args.ShowParametersBtn = false;
                                                             args.ShowExportAllBtn = false;
                                                             args.ShowExportBtn = false;
                                                             args.ShowPrintAllBtn = false;
                                                             args.ShowPrintBtn = false;
                                                         }
                                                     };
                }
            }
        }

        /// <summary>
        ///   Loads the details tab.
        /// </summary>
        public void LoadDetailsTab(string tabName, int templateId, bool checkUniqueName)
        {
            if (Bruc != null)
            {
                CommonBaseTab existingTab = null;
                if (checkUniqueName)
                {
                    existingTab = GetTabByName(tabName);
                }

                if (existingTab == null)
                {
                    if (Bruc.TabControl.TabPages.Count > Constants.TAB_MAX_COUNT)
                    {
                        XtraMessageBox.Show(Resource.TabMaxCount, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }

                    TemplateDetailsTab tabTemplateList = new TemplateDetailsTab(Bruc, tabName, templateId);
                    SheetParamCollection parameterCollection = new SheetParamCollection {TabId = tabTemplateList.TabId, TableDataType = TableType.Fact};
                    tabTemplateList.UpdateSheet(parameterCollection);
                }
                else
                {
                    Bruc.TabControl.SelectedTabPage = existingTab;
                }
            }
        }

        /// <summary>
        ///   Updates the data.
        /// </summary>
        /// <param name = "parameters">The parameters.</param>
        /// <param name = "isForce">if set to <c>true</c> [is force].</param>
        public void UpdateData(SheetParamCollection parameters, bool isForce)
        {
            if (parameters == null || (!isForce && CompareSettings(parameters)))
            {
                return;
            }

            SheetSettings = parameters;

            WaitManager.StartWait();
            Bruc.ConstructTemporaryData(parameters);

            LoadData();

            Bruc.DestructTemporaryData();
            WaitManager.StopWait();
        }

        /// <summary>
        ///   Updates the sheet.
        /// </summary>
        /// <param name = "settings">The settings.</param>
        public void UpdateSheet(SheetParamCollection settings)
        {
            if (settings != null)
            {
                if (!Bruc.TabControl.TabPages.Contains(this))
                {
                    Bruc.TabControl.TabPages.Add(this);
                }
                UpdateData(settings, false);
                Bruc.TabControl.SelectedTabPage = this;
            }
            else if (Bruc.TabControl.TabPages.Contains(this))
            {
                Bruc.TabControl.TabPages.Remove(this);
            }
        }

        /// <summary>
        ///   Updates the template list tab.
        /// </summary>
        public void UpdateTemplateListTab()
        {
            foreach (CommonBaseTab tab in Bruc.TabControl.TabPages)
            {
                if (tab is TemplateListTab)
                {
                    tab.UpdateData(SheetSettings, true);
                    break;
                }
            }
        }

        /// <summary>
        ///   Compares the settings.
        /// </summary>
        /// <param name = "settings">The settings.</param>
        /// <returns></returns>
        private bool CompareSettings(SheetParamCollection settings)
        {
            if (null == SheetSettings || SheetSettings.Count != settings.Count)
            {
                return false;
            }
            foreach (SheetParam param in settings)
            {
                if (!SheetSettings.Exists(p => p.SqlParamName.Equals(param.SqlParamName) && p.Value.Equals(param.Value)))
                {
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        ///   Gets the name of the tab by.
        /// </summary>
        /// <param name = "tabName">Name of the tab.</param>
        /// <returns></returns>
        private CommonBaseTab GetTabByName(string tabName)
        {
            foreach (CommonBaseTab tab in Bruc.TabControl.TabPages)
            {
                if (string.Compare(tab.Text, tabName, false) == 0)
                {
                    return tab;
                }
            }
            return null;
        }

        /// <summary>
        ///   Loads the control.
        /// </summary>
        /// <param name = "userControl">The user control.</param>
        private void LoadControl(CommonBaseControl userControl)
        {
            if (userControl != null)
            {
                UserControl = userControl;
                userControl.Dock = DockStyle.Fill;
                userControl.ParentTab = this;
                Controls.Add(userControl);
            }
        }

        #endregion

        #region Event Handling

        /// <summary>
        ///   Parent_s the refresh click.
        /// </summary>
        /// <param name = "sender">The sender.</param>
        /// <param name = "selectedPage">The selected page.</param>
        private void parent_RefreshClick(object sender, XtraTabPage selectedPage)
        {
            if (this == selectedPage)
            {
                UpdateData(SheetSettings, true);
            }
        }

        #endregion
    }
}