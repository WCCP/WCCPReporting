﻿using DevExpress.XtraTab;
using Logica.Reports.BaseReportControl;

namespace SoftServe.Reports.MPTemplates
{
    /// <summary>
    /// </summary>
    public class TemplateDetailsTab : CommonBaseTab
    {
        #region Constructors

        /// <summary>
        ///   Initializes a new instance of the <see cref = "TemplateDetailsTab" /> class.
        /// </summary>
        /// <param name = "bruc">The bruc.</param>
        /// <param name = "isEditMode">if set to <c>true</c> [is edit mode].</param>
        /// <param name = "tabName">Name of the tab.</param>
        public TemplateDetailsTab(BaseReportUserControl bruc, string tabName, int templateId)
        {
            if (!this.DesignMode && bruc != null)
            {
                ShowCloseButton = DevExpress.Utils.DefaultBoolean.True;

                Init(bruc, new TemplateDetailsControl(this, templateId), tabName);
            }
        }

        #endregion

        #region Instance Properties

        /// <summary>
        ///   Gets the view.
        /// </summary>
        /// <value>The view.</value>
        public TemplateDetailsControl Content
        {
            get { return UserControl as TemplateDetailsControl; }
        }

        #endregion

        #region Instance Methods

        /// <summary>
        ///   Loads the data.
        /// </summary>
        public override void LoadData()
        {
            if (Content != null)
            {
                Content.LoadData();
            }
        }

        /// <summary>
        ///   Parent_s the tool button visibility check.
        /// </summary>
        /// <param name = "sender">The sender.</param>
        /// <param name = "selectedPage">The selected page.</param>
        private void parent_ToolButtonVisibilityCheck(object sender, XtraTabPage selectedPage)
        {
            if (this == selectedPage)
            {
                DevExpress.XtraBars.BarItem barItem = sender as DevExpress.XtraBars.BarItem;
                if (barItem != null)
                {
                    barItem.Visibility = DevExpress.XtraBars.BarItemVisibility.Always;
                }
            }
        }

        #endregion
    }
}