﻿using Logica.Reports.BaseReportControl;

namespace SoftServe.Reports.MPTemplates
{
    /// <summary>
    /// 
    /// </summary>
    public class TemplateListTab : CommonBaseTab
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TemplateListTab"/> class.
        /// </summary>
        /// <param name="parent">The parent.</param>
        public TemplateListTab(BaseReportUserControl bruc)
        {
            if (!this.DesignMode && bruc != null)
            {
                Init(bruc, new TemplateListControl(this), Resource.TemplateList);

                ShowCloseButton = DevExpress.Utils.DefaultBoolean.False;
            }
        }

        /// <summary>
        /// Gets the view.
        /// </summary>
        /// <value>The view.</value>
        public TemplateListControl Content
        {
            get
            {
                return base.UserControl as TemplateListControl;
            }
        }

        /// <summary>
        /// Loads the data.
        /// </summary>
        public override void LoadData()
        {
            Content.TemplateList = DataProvider.ContractTemplateList;
        }
    }
}
