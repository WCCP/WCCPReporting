﻿namespace SoftServe.Reports.EveryVisitInventory
{
    partial class SettingsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SettingsForm));
            this.mailPanel = new System.Windows.Forms.TableLayoutPanel();
            this.dateToGroupControl = new DevExpress.XtraEditors.GroupControl();
            this.dateToDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.waveGroupControl = new DevExpress.XtraEditors.GroupControl();
            this.waveGridLookUp = new Logica.Reports.BaseReportControl.CommonControls.GridLookUpWithMultipleChecking();
            this.userControl21View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colWaveName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWaveStartDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWaveEndDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWaveId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.locationGroupControl = new DevExpress.XtraEditors.GroupControl();
            this.locationCheckedComboBoxEdit = new DevExpress.XtraEditors.CheckedComboBoxEdit();
            this.lastInventoryStatusGroupControl = new DevExpress.XtraEditors.GroupControl();
            this.lastInventoryStatusCheckedComboBoxEdit = new DevExpress.XtraEditors.CheckedComboBoxEdit();
            this.equipmentGroupGroupControl = new DevExpress.XtraEditors.GroupControl();
            this.equipmentGroupCheckedComboBoxEdit = new DevExpress.XtraEditors.CheckedComboBoxEdit();
            this.staffGroupControl = new DevExpress.XtraEditors.GroupControl();
            this.staffTreeList = new DevExpress.XtraTreeList.TreeList();
            this.colName = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.colStaffID = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.colLevel = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.okButton = new DevExpress.XtraEditors.SimpleButton();
            this.cancelButton = new DevExpress.XtraEditors.SimpleButton();
            this.mailPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateToGroupControl)).BeginInit();
            this.dateToGroupControl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateToDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateToDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.waveGroupControl)).BeginInit();
            this.waveGroupControl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.waveGridLookUp.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.userControl21View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.locationGroupControl)).BeginInit();
            this.locationGroupControl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.locationCheckedComboBoxEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lastInventoryStatusGroupControl)).BeginInit();
            this.lastInventoryStatusGroupControl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lastInventoryStatusCheckedComboBoxEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.equipmentGroupGroupControl)).BeginInit();
            this.equipmentGroupGroupControl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.equipmentGroupCheckedComboBoxEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.staffGroupControl)).BeginInit();
            this.staffGroupControl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.staffTreeList)).BeginInit();
            this.SuspendLayout();
            // 
            // mailPanel
            // 
            this.mailPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.mailPanel.ColumnCount = 2;
            this.mailPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 56.82759F));
            this.mailPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 43.17241F));
            this.mailPanel.Controls.Add(this.dateToGroupControl, 0, 0);
            this.mailPanel.Controls.Add(this.waveGroupControl, 0, 1);
            this.mailPanel.Controls.Add(this.locationGroupControl, 0, 2);
            this.mailPanel.Controls.Add(this.lastInventoryStatusGroupControl, 0, 3);
            this.mailPanel.Controls.Add(this.equipmentGroupGroupControl, 0, 4);
            this.mailPanel.Controls.Add(this.staffGroupControl, 1, 0);
            this.mailPanel.Location = new System.Drawing.Point(5, 5);
            this.mailPanel.Name = "mailPanel";
            this.mailPanel.RowCount = 5;
            this.mailPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            this.mailPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            this.mailPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            this.mailPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            this.mailPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            this.mailPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.mailPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.mailPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.mailPanel.Size = new System.Drawing.Size(725, 300);
            this.mailPanel.TabIndex = 0;
            // 
            // dateToGroupControl
            // 
            this.dateToGroupControl.Controls.Add(this.dateToDateEdit);
            this.dateToGroupControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dateToGroupControl.Location = new System.Drawing.Point(3, 3);
            this.dateToGroupControl.Name = "dateToGroupControl";
            this.dateToGroupControl.Size = new System.Drawing.Size(406, 54);
            this.dateToGroupControl.TabIndex = 1;
            this.dateToGroupControl.Text = "Дата ПО";
            // 
            // dateToDateEdit
            // 
            this.dateToDateEdit.EditValue = null;
            this.dateToDateEdit.Location = new System.Drawing.Point(5, 24);
            this.dateToDateEdit.Name = "dateToDateEdit";
            this.dateToDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateToDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateToDateEdit.Size = new System.Drawing.Size(396, 20);
            this.dateToDateEdit.TabIndex = 0;
            this.dateToDateEdit.EditValueChanged += new System.EventHandler(this.dateToDateEdit_EditValueChanged);
            // 
            // waveGroupControl
            // 
            this.waveGroupControl.Controls.Add(this.waveGridLookUp);
            this.waveGroupControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.waveGroupControl.Location = new System.Drawing.Point(3, 63);
            this.waveGroupControl.Name = "waveGroupControl";
            this.waveGroupControl.Size = new System.Drawing.Size(406, 54);
            this.waveGroupControl.TabIndex = 2;
            this.waveGroupControl.Text = "Волна";
            // 
            // waveGridLookUp
            // 
            this.waveGridLookUp.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.waveGridLookUp.Delimiter = ", ";
            this.waveGridLookUp.EditValue = "";
            this.waveGridLookUp.Location = new System.Drawing.Point(6, 25);
            this.waveGridLookUp.Name = "waveGridLookUp";
            this.waveGridLookUp.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.waveGridLookUp.Properties.DisplayMember = "FieldName";
            this.waveGridLookUp.Properties.NullText = "";
            this.waveGridLookUp.Properties.PopupFormSize = new System.Drawing.Size(395, 0);
            this.waveGridLookUp.Properties.PopupResizeMode = DevExpress.XtraEditors.Controls.ResizeMode.FrameResize;
            this.waveGridLookUp.Properties.ValueMember = "wInvPeriodId";
            this.waveGridLookUp.Properties.View = this.userControl21View;
            this.waveGridLookUp.Size = new System.Drawing.Size(395, 20);
            this.waveGridLookUp.TabIndex = 83;
            this.waveGridLookUp.Closed += new DevExpress.XtraEditors.Controls.ClosedEventHandler(this.waveGridLookUp_Closed);
            // 
            // userControl21View
            // 
            this.userControl21View.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colWaveName,
            this.colWaveStartDate,
            this.colWaveEndDate,
            this.colWaveId});
            this.userControl21View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.userControl21View.Name = "userControl21View";
            this.userControl21View.OptionsFilter.AllowFilterEditor = false;
            this.userControl21View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.userControl21View.OptionsView.ColumnAutoWidth = false;
            this.userControl21View.OptionsView.ShowGroupPanel = false;
            // 
            // colWaveName
            // 
            this.colWaveName.Caption = "Название Инвентаризации";
            this.colWaveName.CustomizationCaption = "Название Инвентаризации";
            this.colWaveName.FieldName = "FieldName";
            this.colWaveName.Name = "colWaveName";
            this.colWaveName.OptionsColumn.AllowEdit = false;
            this.colWaveName.OptionsColumn.AllowShowHide = false;
            this.colWaveName.OptionsColumn.ReadOnly = true;
            this.colWaveName.Visible = true;
            this.colWaveName.VisibleIndex = 0;
            this.colWaveName.Width = 200;
            // 
            // colWaveStartDate
            // 
            this.colWaveStartDate.Caption = "Дата С";
            this.colWaveStartDate.CustomizationCaption = "Дата С";
            this.colWaveStartDate.FieldName = "StartDate";
            this.colWaveStartDate.Name = "colWaveStartDate";
            this.colWaveStartDate.OptionsColumn.AllowEdit = false;
            this.colWaveStartDate.OptionsColumn.AllowShowHide = false;
            this.colWaveStartDate.OptionsColumn.ReadOnly = true;
            this.colWaveStartDate.Visible = true;
            this.colWaveStartDate.VisibleIndex = 1;
            this.colWaveStartDate.Width = 74;
            // 
            // colWaveEndDate
            // 
            this.colWaveEndDate.Caption = "Дата ПО";
            this.colWaveEndDate.CustomizationCaption = "Дата ПО";
            this.colWaveEndDate.FieldName = "EndDate";
            this.colWaveEndDate.Name = "colWaveEndDate";
            this.colWaveEndDate.OptionsColumn.AllowShowHide = false;
            this.colWaveEndDate.OptionsColumn.ShowInCustomizationForm = false;
            this.colWaveEndDate.Visible = true;
            this.colWaveEndDate.VisibleIndex = 2;
            this.colWaveEndDate.Width = 74;
            // 
            // colWaveId
            // 
            this.colWaveId.Caption = "WaveId";
            this.colWaveId.FieldName = "wInvPeriodId";
            this.colWaveId.Name = "colWaveId";
            this.colWaveId.OptionsColumn.AllowShowHide = false;
            this.colWaveId.OptionsColumn.ShowInCustomizationForm = false;
            // 
            // locationGroupControl
            // 
            this.locationGroupControl.Controls.Add(this.locationCheckedComboBoxEdit);
            this.locationGroupControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.locationGroupControl.Location = new System.Drawing.Point(3, 123);
            this.locationGroupControl.Name = "locationGroupControl";
            this.locationGroupControl.Size = new System.Drawing.Size(406, 54);
            this.locationGroupControl.TabIndex = 4;
            this.locationGroupControl.Text = "Локация ";
            // 
            // locationCheckedComboBoxEdit
            // 
            this.locationCheckedComboBoxEdit.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.locationCheckedComboBoxEdit.Location = new System.Drawing.Point(6, 25);
            this.locationCheckedComboBoxEdit.Name = "locationCheckedComboBoxEdit";
            this.locationCheckedComboBoxEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.locationCheckedComboBoxEdit.Properties.SelectAllItemCaption = "(Все)";
            this.locationCheckedComboBoxEdit.Size = new System.Drawing.Size(395, 20);
            this.locationCheckedComboBoxEdit.TabIndex = 0;
            // 
            // lastInventoryStatusGroupControl
            // 
            this.lastInventoryStatusGroupControl.Controls.Add(this.lastInventoryStatusCheckedComboBoxEdit);
            this.lastInventoryStatusGroupControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lastInventoryStatusGroupControl.Location = new System.Drawing.Point(3, 183);
            this.lastInventoryStatusGroupControl.Name = "lastInventoryStatusGroupControl";
            this.lastInventoryStatusGroupControl.Size = new System.Drawing.Size(406, 54);
            this.lastInventoryStatusGroupControl.TabIndex = 3;
            this.lastInventoryStatusGroupControl.Text = "Последний статус инвентаризации";
            // 
            // lastInventoryStatusCheckedComboBoxEdit
            // 
            this.lastInventoryStatusCheckedComboBoxEdit.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lastInventoryStatusCheckedComboBoxEdit.Location = new System.Drawing.Point(6, 25);
            this.lastInventoryStatusCheckedComboBoxEdit.Name = "lastInventoryStatusCheckedComboBoxEdit";
            this.lastInventoryStatusCheckedComboBoxEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lastInventoryStatusCheckedComboBoxEdit.Properties.SelectAllItemCaption = "(Все)";
            this.lastInventoryStatusCheckedComboBoxEdit.Size = new System.Drawing.Size(395, 20);
            this.lastInventoryStatusCheckedComboBoxEdit.TabIndex = 0;
            // 
            // equipmentGroupGroupControl
            // 
            this.equipmentGroupGroupControl.Controls.Add(this.equipmentGroupCheckedComboBoxEdit);
            this.equipmentGroupGroupControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.equipmentGroupGroupControl.Location = new System.Drawing.Point(3, 243);
            this.equipmentGroupGroupControl.Name = "equipmentGroupGroupControl";
            this.equipmentGroupGroupControl.Size = new System.Drawing.Size(406, 54);
            this.equipmentGroupGroupControl.TabIndex = 5;
            this.equipmentGroupGroupControl.Text = "Группа оборудования ";
            // 
            // equipmentGroupCheckedComboBoxEdit
            // 
            this.equipmentGroupCheckedComboBoxEdit.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.equipmentGroupCheckedComboBoxEdit.Location = new System.Drawing.Point(6, 25);
            this.equipmentGroupCheckedComboBoxEdit.Name = "equipmentGroupCheckedComboBoxEdit";
            this.equipmentGroupCheckedComboBoxEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.equipmentGroupCheckedComboBoxEdit.Properties.SelectAllItemCaption = "(Все)";
            this.equipmentGroupCheckedComboBoxEdit.Size = new System.Drawing.Size(395, 20);
            this.equipmentGroupCheckedComboBoxEdit.TabIndex = 0;
            // 
            // staffGroupControl
            // 
            this.staffGroupControl.Controls.Add(this.staffTreeList);
            this.staffGroupControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.staffGroupControl.Location = new System.Drawing.Point(415, 3);
            this.staffGroupControl.Name = "staffGroupControl";
            this.mailPanel.SetRowSpan(this.staffGroupControl, 5);
            this.staffGroupControl.Size = new System.Drawing.Size(307, 294);
            this.staffGroupControl.TabIndex = 0;
            this.staffGroupControl.Text = "Персонал";
            // 
            // staffTreeList
            // 
            this.staffTreeList.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.colName,
            this.colStaffID,
            this.colLevel});
            this.staffTreeList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.staffTreeList.KeyFieldName = "Id";
            this.staffTreeList.Location = new System.Drawing.Point(2, 21);
            this.staffTreeList.Name = "staffTreeList";
            this.staffTreeList.OptionsBehavior.AllowRecursiveNodeChecking = true;
            this.staffTreeList.OptionsBehavior.Editable = false;
            this.staffTreeList.OptionsBehavior.EnableFiltering = true;
            this.staffTreeList.OptionsBehavior.ReadOnly = true;
            this.staffTreeList.OptionsBehavior.ShowToolTips = false;
            this.staffTreeList.OptionsFilter.AllowColumnMRUFilterList = false;
            this.staffTreeList.OptionsFilter.AllowMRUFilterList = false;
            this.staffTreeList.OptionsFilter.ShowAllValuesInCheckedFilterPopup = false;
            this.staffTreeList.OptionsFilter.ShowAllValuesInFilterPopup = true;
            this.staffTreeList.OptionsMenu.EnableColumnMenu = false;
            this.staffTreeList.OptionsMenu.EnableFooterMenu = false;
            this.staffTreeList.OptionsMenu.ShowAutoFilterRowItem = false;
            this.staffTreeList.OptionsView.ShowCheckBoxes = true;
            this.staffTreeList.OptionsView.ShowColumns = false;
            this.staffTreeList.OptionsView.ShowHorzLines = false;
            this.staffTreeList.OptionsView.ShowIndicator = false;
            this.staffTreeList.OptionsView.ShowVertLines = false;
            this.staffTreeList.ParentFieldName = "ParentId";
            this.staffTreeList.Size = new System.Drawing.Size(303, 271);
            this.staffTreeList.TabIndex = 0;
            // 
            // colName
            // 
            this.colName.Caption = "Name";
            this.colName.FieldName = "Name";
            this.colName.MinWidth = 32;
            this.colName.Name = "colName";
            this.colName.Visible = true;
            this.colName.VisibleIndex = 0;
            this.colName.Width = 91;
            // 
            // colStaffID
            // 
            this.colStaffID.Caption = "StaffID";
            this.colStaffID.FieldName = "Id";
            this.colStaffID.Name = "colStaffID";
            this.colStaffID.OptionsColumn.AllowMoveToCustomizationForm = false;
            this.colStaffID.OptionsColumn.ShowInCustomizationForm = false;
            // 
            // colLevel
            // 
            this.colLevel.Caption = "Level";
            this.colLevel.FieldName = "Level";
            this.colLevel.Name = "colLevel";
            this.colLevel.OptionsColumn.AllowMoveToCustomizationForm = false;
            this.colLevel.OptionsColumn.ShowInCustomizationForm = false;
            // 
            // okButton
            // 
            this.okButton.Image = ((System.Drawing.Image)(resources.GetObject("okButton.Image")));
            this.okButton.Location = new System.Drawing.Point(483, 311);
            this.okButton.Name = "okButton";
            this.okButton.Size = new System.Drawing.Size(146, 23);
            this.okButton.TabIndex = 1;
            this.okButton.Text = "Сгенерировать отчет";
            this.okButton.Click += new System.EventHandler(this.okButton_Click);
            // 
            // cancelButton
            // 
            this.cancelButton.Image = ((System.Drawing.Image)(resources.GetObject("cancelButton.Image")));
            this.cancelButton.Location = new System.Drawing.Point(635, 311);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(92, 23);
            this.cancelButton.TabIndex = 2;
            this.cancelButton.Text = "Отменить";
            this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
            // 
            // SettingsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(735, 342);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.okButton);
            this.Controls.Add(this.mailPanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SettingsForm";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Параметры к отчету \"Ежевизитная инвентаризация\"";
            this.mailPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dateToGroupControl)).EndInit();
            this.dateToGroupControl.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dateToDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateToDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.waveGroupControl)).EndInit();
            this.waveGroupControl.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.waveGridLookUp.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.userControl21View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.locationGroupControl)).EndInit();
            this.locationGroupControl.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.locationCheckedComboBoxEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lastInventoryStatusGroupControl)).EndInit();
            this.lastInventoryStatusGroupControl.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lastInventoryStatusCheckedComboBoxEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.equipmentGroupGroupControl)).EndInit();
            this.equipmentGroupGroupControl.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.equipmentGroupCheckedComboBoxEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.staffGroupControl)).EndInit();
            this.staffGroupControl.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.staffTreeList)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel mailPanel;
        private DevExpress.XtraEditors.SimpleButton okButton;
        private DevExpress.XtraEditors.SimpleButton cancelButton;
        private DevExpress.XtraEditors.GroupControl staffGroupControl;
        private DevExpress.XtraTreeList.TreeList staffTreeList;
        private DevExpress.XtraEditors.GroupControl dateToGroupControl;
        private DevExpress.XtraEditors.GroupControl waveGroupControl;
        private DevExpress.XtraEditors.GroupControl equipmentGroupGroupControl;
        private DevExpress.XtraEditors.GroupControl locationGroupControl;
        private DevExpress.XtraEditors.GroupControl lastInventoryStatusGroupControl;
        private DevExpress.XtraEditors.DateEdit dateToDateEdit;
        private DevExpress.XtraEditors.CheckedComboBoxEdit equipmentGroupCheckedComboBoxEdit;
        private DevExpress.XtraEditors.CheckedComboBoxEdit locationCheckedComboBoxEdit;
        private DevExpress.XtraEditors.CheckedComboBoxEdit lastInventoryStatusCheckedComboBoxEdit;
        private DevExpress.XtraTreeList.Columns.TreeListColumn colName;
        private DevExpress.XtraEditors.CheckEdit activeVaweCheckEdit;
        private Logica.Reports.BaseReportControl.CommonControls.GridLookUpWithMultipleChecking waveGridLookUp;
        private DevExpress.XtraGrid.Views.Grid.GridView userControl21View;
        private DevExpress.XtraGrid.Columns.GridColumn colWaveName;
        private DevExpress.XtraGrid.Columns.GridColumn colWaveStartDate;
        private DevExpress.XtraGrid.Columns.GridColumn colWaveEndDate;
        private DevExpress.XtraTreeList.Columns.TreeListColumn colStaffID;
        private DevExpress.XtraTreeList.Columns.TreeListColumn colLevel;
        private DevExpress.XtraGrid.Columns.GridColumn colWaveId;
    }
}