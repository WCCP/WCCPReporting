﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Net;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Logica.Reports.Common;
using SoftServe.Reports.EveryVisitInventory.Models;
using SoftServe.Reports.EveryVisitInventory.Properties;

namespace SoftServe.Reports.EveryVisitInventory
{
    public partial class PhotoForm : XtraForm
    {
        #region Fields
        private readonly List<string> _fileNames;
        private readonly List<Photo> _photos = new List<Photo>();
        #endregion

        #region Constructor
        public PhotoForm(List<string> fileNames)
        {
            InitializeComponent();
            _fileNames = fileNames;
        }
        #endregion

        #region Methods
        private void LoadServiceImages()
        {
            for (int i = 0; i < _fileNames.Count; i++)
            {
                try
                {
                    var photoLink = string.Format(Resources.ContentServiceRequest, _fileNames[i]);
                    var photoStream = GetFileContent(photoLink);
                    var image = Image.FromStream(photoStream);
                    _photos.Add(new Photo(_fileNames[i], image, i));
                }
                catch (Exception ex)
                {
                    ErrorManager.ShowErrorBox(string.Format("Не получилось отобразить фотографию {0}: {1}", _fileNames[i], ex.Message));
                }
            }
        }

        private static Stream GetFileContent(string photoLink)
        {
            Stream result = null;
            var lRequest = (HttpWebRequest)HttpWebRequest.Create(photoLink);

            int statusCode = 0;
            WebResponse response = null;

            response = lRequest.GetResponse();
            if (response is HttpWebResponse)
            {
                var webResponse = (HttpWebResponse)response;
                statusCode = (int)webResponse.StatusCode;
                result = webResponse.GetResponseStream();
            }

            if (statusCode != 200)
            {
                throw new Exception("Ошибка в работе сервиса");
            }
            return result;
        }
        #endregion

        #region Event handlers
        private void PhotoForm_Load(object sender, EventArgs e)
        {
            LoadServiceImages();
            listBoxControl.DataSource = _photos;
        }

        private void listBoxControl_SelectedIndexChanged(object sender, EventArgs e)
        {
            var photo = listBoxControl.SelectedItem as Photo;
            pictureEdit.Image = photo != null ? photo.Image : null;
        }

        private void prevButton_Click(object sender, EventArgs e)
        {
            var selectedIndex = listBoxControl.SelectedIndex;
            if (selectedIndex > 0)
            {
                listBoxControl.SetSelected(selectedIndex - 1, true);
            }
        }

        private void nextButton_Click(object sender, EventArgs e)
        {
            var selectedIndex = listBoxControl.SelectedIndex;
            if (selectedIndex < _photos.Count - 1)
            {
                listBoxControl.SetSelected(selectedIndex + 1, true);
            }
        }

        private void listBoxControl_MouseMove(object sender, MouseEventArgs e)
        {
            int index = listBoxControl.IndexFromPoint(new Point(e.X, e.Y));
            if (index != -1)
            {
                var photo = listBoxControl.GetItem(index) as Photo;
                if (photo != null)
                toolTipController.ShowHint(photo.Path, listBoxControl.PointToScreen(new Point(e.X, e.Y)));
            }
            else
            {
                toolTipController.HideHint();
            }
        }

        private void listBoxControl_MouseLeave(object sender, EventArgs e)
        {
            toolTipController.HideHint();
        }

        private void turnLeftButton_Click(object sender, EventArgs e)
        {
            var photo = listBoxControl.SelectedItem as Photo;
            if (photo != null && photo.Image != null)
            {
                photo.Image.RotateFlip(RotateFlipType.Rotate270FlipNone);
                pictureEdit.Image = photo.Image;
                
            }
        }

        private void turnRightButton_Click(object sender, EventArgs e)
        {
            var photo = listBoxControl.SelectedItem as Photo;
            if (photo != null && photo.Image != null)
            {
                photo.Image.RotateFlip(RotateFlipType.Rotate90FlipNone);
                pictureEdit.Image = photo.Image;
            }
        }
        #endregion
    }
}