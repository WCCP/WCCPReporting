﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using System.Xml.Linq;
using DevExpress.XtraGrid.Views.BandedGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using Logica.Reports.BaseReportControl;
using SoftServe.Reports.EveryVisitInventory.Properties;

namespace SoftServe.Reports.EveryVisitInventory.Controls
{
    public partial class DataControl : UserControl
    {
        #region Fields
        private readonly List<string> _statusesGray;
        private readonly List<string> _statusesGreen;
        private readonly List<string> _statusesYellow;
        private readonly List<string> _statusesOrange;
        private readonly List<string> _statusesRed;

        private readonly Dictionary<BandedGridColumn, BandedGridColumn> _datesPhotos = new Dictionary<BandedGridColumn, BandedGridColumn>();
        #endregion

        #region Constructor
        public DataControl()
        {
            InitializeComponent();

            _statusesGray = new List<string>(Resources.StatusesGrey.Split(','));
            _statusesGreen = new List<string>(Resources.StatusesGreen.Split(','));
            _statusesYellow = new List<string>(Resources.StatusesYellow.Split(','));
            _statusesOrange = new List<string>(Resources.StatusesOrange.Split(','));
            _statusesRed = new List<string>(Resources.StatusesRed.Split(','));

            _datesPhotos.Add(colDay0, colDayPhoto0);
            _datesPhotos.Add(colDay1, colDayPhoto1);
            _datesPhotos.Add(colDay2, colDayPhoto2);
            _datesPhotos.Add(colDay3, colDayPhoto3);
            _datesPhotos.Add(colDay4, colDayPhoto4);
            _datesPhotos.Add(colDay5, colDayPhoto5);
            _datesPhotos.Add(colDay6, colDayPhoto6);
            _datesPhotos.Add(colDay7, colDayPhoto7);
            _datesPhotos.Add(colDay8, colDayPhoto8);
            _datesPhotos.Add(colDay9, colDayPhoto9);
            _datesPhotos.Add(colDay10, colDayPhoto10);
            _datesPhotos.Add(colDay11, colDayPhoto11);
            _datesPhotos.Add(colDay12, colDayPhoto12);
            _datesPhotos.Add(colDay13, colDayPhoto13);
            _datesPhotos.Add(colDay14, colDayPhoto14);
            _datesPhotos.Add(colDay15, colDayPhoto15);
            _datesPhotos.Add(colDay16, colDayPhoto16);
            _datesPhotos.Add(colDay17, colDayPhoto17);
            _datesPhotos.Add(colDay18, colDayPhoto18);
            _datesPhotos.Add(colDay19, colDayPhoto19);
            _datesPhotos.Add(colDay20, colDayPhoto20);
            _datesPhotos.Add(colDay21, colDayPhoto21);
            _datesPhotos.Add(colDay22, colDayPhoto22);
            _datesPhotos.Add(colDay23, colDayPhoto23);
            _datesPhotos.Add(colDay24, colDayPhoto24);
            _datesPhotos.Add(colDay25, colDayPhoto25);
            _datesPhotos.Add(colDay26, colDayPhoto26);
            _datesPhotos.Add(colDay27, colDayPhoto27);
            _datesPhotos.Add(colDay28, colDayPhoto28);
            _datesPhotos.Add(colDay29, colDayPhoto29);
            _datesPhotos.Add(colDay30, colDayPhoto30);
            _datesPhotos.Add(colPosLastStatus, colPosPhotoReal);
        }
        #endregion

        #region Properties
        public object DataSource
        {
            set
            {
                gridControl.BeginUpdate();
                gridControl.DataSource = value;
                gridControl.EndUpdate();
            }
        }
        #endregion

        #region Event handlers
        private void gridViewData_BeforeLoadLayout(object sender, DevExpress.Utils.LayoutAllowEventArgs e)
        {
            if (string.IsNullOrEmpty(e.PreviousVersion))
            {
                e.Allow = false;
                return;
            }

            e.Allow = e.PreviousVersion == gridView.OptionsLayout.LayoutVersion;
        }

        private void gridView_RowCellStyle(object sender, RowCellStyleEventArgs e)
        {
            SetDayCellColor(e);
        }

        private void gridView_CustomUnboundColumnData(object sender, CustomColumnDataEventArgs e)
        {
            if (e.Column == colPosPhoto)
            {
                object filesXml = gridView.GetListSourceRowCellValue(e.ListSourceRowIndex, colPosPhotoReal);
                if (filesXml != null && filesXml != DBNull.Value)
                {
                    var root = XElement.Parse(string.Format("<Root>{0}</Root>", filesXml));
                    var links = from el in root.Elements("ContentFileUniqueName")
                                select string.Format(Resources.ContentServiceRequest, el.Value);
                    e.Value = string.Join("  ", links);
                }
            }

        }

        private void gridView_CustomRowCellEdit(object sender, CustomRowCellEditEventArgs e)
        {
            int visibleRowHandle = e.RowHandle;
            if (visibleRowHandle < 0)
            {
                return;
            }
            if (e.Column is BandedGridColumn && _datesPhotos.Keys.Contains(e.Column))
            {
                var columPhotoFieldName = _datesPhotos[(BandedGridColumn)e.Column].FieldName;
                var link = GetPhotoXml(visibleRowHandle, columPhotoFieldName);

                if (link != null && link != DBNull.Value)
                {
                    e.RepositoryItem = repositoryItemHyperLinkEdit;
                }
            }
        }

        private void gridView_RowCellClick(object sender, RowCellClickEventArgs e)
        {
            int visibleRowHandle = e.RowHandle;
            if (visibleRowHandle < 0)
            {
                return;
            }
            if (e.Column == colPosPhoto && e.CellValue != null)
            {
                object filesXml = GetPhotoXml(visibleRowHandle, colPosPhotoReal.FieldName);
                ShowPhotoForm(filesXml);
                return;
            }
            if (_datesPhotos.Keys.Contains(e.Column) && e.CellValue != null)
            {
                string fieldName = _datesPhotos[(BandedGridColumn)e.Column].FieldName;
                object filesXml = GetPhotoXml(visibleRowHandle, fieldName);
                ShowPhotoForm(filesXml);
            }
        }
        #endregion

        #region Methods
        internal void Export(ExportToType exportToType, string path)
        {
            switch (exportToType)
            {
                case ExportToType.Xlsx:
                    gridControl.ExportToXlsx(path);
                    break;
                case ExportToType.Xls:
                    gridControl.ExportToXls(path);
                    break;
            }
        }

        internal void SaveLayout(string path)
        {
            gridControl.MainView.SaveLayoutToXml(path);
        }

        internal void RestoreLayout(string path)
        {
            if (File.Exists(path))
            {
                gridControl.MainView.RestoreLayoutFromXml(path);
            }
        }

        internal void SetDayColumnCaptions(DateTime dateTo)
        {
            colDay0.Caption = dateTo.Date.AddDays(-30).ToShortDateString();
            colDay1.Caption = dateTo.Date.AddDays(-29).ToShortDateString();
            colDay2.Caption = dateTo.Date.AddDays(-28).ToShortDateString();
            colDay3.Caption = dateTo.Date.AddDays(-27).ToShortDateString();
            colDay4.Caption = dateTo.Date.AddDays(-26).ToShortDateString();
            colDay5.Caption = dateTo.Date.AddDays(-25).ToShortDateString();
            colDay6.Caption = dateTo.Date.AddDays(-24).ToShortDateString();
            colDay7.Caption = dateTo.Date.AddDays(-23).ToShortDateString();
            colDay8.Caption = dateTo.Date.AddDays(-22).ToShortDateString();
            colDay9.Caption = dateTo.Date.AddDays(-21).ToShortDateString();
            colDay10.Caption = dateTo.Date.AddDays(-20).ToShortDateString();
            colDay11.Caption = dateTo.Date.AddDays(-19).ToShortDateString();
            colDay12.Caption = dateTo.Date.AddDays(-18).ToShortDateString();
            colDay13.Caption = dateTo.Date.AddDays(-17).ToShortDateString();
            colDay14.Caption = dateTo.Date.AddDays(-16).ToShortDateString();
            colDay15.Caption = dateTo.Date.AddDays(-15).ToShortDateString();
            colDay16.Caption = dateTo.Date.AddDays(-14).ToShortDateString();
            colDay17.Caption = dateTo.Date.AddDays(-13).ToShortDateString();
            colDay18.Caption = dateTo.Date.AddDays(-12).ToShortDateString();
            colDay19.Caption = dateTo.Date.AddDays(-11).ToShortDateString();
            colDay20.Caption = dateTo.Date.AddDays(-10).ToShortDateString();
            colDay21.Caption = dateTo.Date.AddDays(-9).ToShortDateString();
            colDay22.Caption = dateTo.Date.AddDays(-8).ToShortDateString();
            colDay23.Caption = dateTo.Date.AddDays(-7).ToShortDateString();
            colDay24.Caption = dateTo.Date.AddDays(-6).ToShortDateString();
            colDay25.Caption = dateTo.Date.AddDays(-5).ToShortDateString();
            colDay26.Caption = dateTo.Date.AddDays(-4).ToShortDateString();
            colDay27.Caption = dateTo.Date.AddDays(-3).ToShortDateString();
            colDay28.Caption = dateTo.Date.AddDays(-2).ToShortDateString();
            colDay29.Caption = dateTo.Date.AddDays(-1).ToShortDateString();
            colDay30.Caption = dateTo.Date.ToShortDateString();
        }

        private void SetDayCellColor(RowCellStyleEventArgs e)
        {
            if (e.Column.FieldName.StartsWith("Date") || e.Column.FieldName == "LastStatus")
            {
                string cellValue = e.CellValue == null ? string.Empty: e.CellValue.ToString().Trim();
                if (_statusesGray.Contains(cellValue))
                {
                    e.Appearance.BackColor = Color.Gray;
                }
                if (_statusesGreen.Contains(cellValue))
                {
                    e.Appearance.BackColor = Color.Green;
                }
                if (_statusesYellow.Contains(cellValue))
                {
                    e.Appearance.BackColor = Color.Yellow;
                }
                if (_statusesOrange.Contains(cellValue))
                {
                    e.Appearance.BackColor = Color.Orange;
                }
                if (_statusesRed.Contains(cellValue))
                {
                    e.Appearance.BackColor = Color.Red;
                }
            }
        }

        private static void ShowPhotoForm(object filesXml)
        {
            if (filesXml != null && filesXml != DBNull.Value)
            {
                var root = XElement.Parse(string.Format("<Root>{0}</Root>", filesXml));
                var fileNames = (from el in root.Elements("ContentFileUniqueName") select el.Value).ToList();
                var photoForm = new PhotoForm(fileNames);
                photoForm.ShowDialog();
            }
        }

        private object GetPhotoXml(int visibleRowHandle, string columPhotoFieldName)
        {
            var dataSourceRowIndex = gridView.GetDataSourceRowIndex(visibleRowHandle);
            object link = ((DataView)gridView.DataSource)[dataSourceRowIndex][columPhotoFieldName];
            return link;
        }
        #endregion
    }
}
