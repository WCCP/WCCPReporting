﻿namespace SoftServe.Reports.EveryVisitInventory.Controls
{
    partial class DataControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gridControl = new DevExpress.XtraGrid.GridControl();
            this.gridView = new DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView();
            this.colPosGroup = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colPosType = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colPosSerialNo = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colPosInventNo = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colPosBrand = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colYPosYearProduction = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colTechnicalCondition = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colPosSapId = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colPosBluetooth = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colPosInSystemOrNew = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colPosBrandCorrected = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colYPosYearProductionCorrected = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colPosLocation = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colPosLocationCode = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colPosLocationName = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colPosLocationTradingName = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colPosLocationCodeFound = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colPosLocationNameFound = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colPosLocationTradingNameFound = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colM6Name = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colM5Name = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colM4Name = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colM3Name = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colM2Name = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colM1Name = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colDay0 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colDay1 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colDay2 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colDay3 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colDay4 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colDay5 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colDay6 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colDay7 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colDay8 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colDay9 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colDay10 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colDay11 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colDay12 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colDay13 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colDay14 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colDay15 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colDay16 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colDay17 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colDay18 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colDay19 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colDay20 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colDay21 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colDay22 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colDay23 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colDay24 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colDay25 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colDay26 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colDay27 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colDay28 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colDay29 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colDay30 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colPosLastStatus = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colPosAbsenceReason = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colPosPhoto = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.repositoryItemMemoEdit = new DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit();
            this.colPosPhotoReal = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colDayPhoto0 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colDayPhoto1 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colDayPhoto2 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colDayPhoto3 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colDayPhoto4 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colDayPhoto5 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colDayPhoto6 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colDayPhoto7 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colDayPhoto8 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colDayPhoto9 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colDayPhoto10 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colDayPhoto11 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colDayPhoto12 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colDayPhoto13 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colDayPhoto14 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colDayPhoto15 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colDayPhoto16 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colDayPhoto17 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colDayPhoto18 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colDayPhoto19 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colDayPhoto20 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colDayPhoto21 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colDayPhoto22 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colDayPhoto23 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colDayPhoto24 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colDayPhoto25 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colDayPhoto26 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colDayPhoto27 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colDayPhoto28 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colDayPhoto29 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colDayPhoto30 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.repositoryItemHyperLinkEdit = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.colActualAddress = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandPOS = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandPosInfoInSystem = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandPosInfoCorrected = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandLocationInSystem = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandLocationOfFoundPos = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandOrgSructure = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandInventarizationInfo = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit)).BeginInit();
            this.SuspendLayout();
            // 
            // gridControl
            // 
            this.gridControl.Cursor = System.Windows.Forms.Cursors.Default;
            this.gridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl.Location = new System.Drawing.Point(0, 0);
            this.gridControl.MainView = this.gridView;
            this.gridControl.Name = "gridControl";
            this.gridControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemHyperLinkEdit,
            this.repositoryItemMemoEdit});
            this.gridControl.Size = new System.Drawing.Size(1091, 416);
            this.gridControl.TabIndex = 0;
            this.gridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView});
            // 
            // gridView
            // 
            this.gridView.Bands.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.bandPOS,
            this.bandOrgSructure,
            this.bandInventarizationInfo});
            this.gridView.ColumnPanelRowHeight = 40;
            this.gridView.Columns.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn[] {
            this.colPosGroup,
            this.colPosType,
            this.colPosSerialNo,
            this.colPosInventNo,
            this.colPosBrand,
            this.colYPosYearProduction,
            this.colTechnicalCondition,
            this.colPosSapId,
            this.colPosBluetooth,
            this.colPosInSystemOrNew,
            this.colPosBrandCorrected,
            this.colYPosYearProductionCorrected,
            this.colPosLocation,
            this.colPosLocationCode,
            this.colPosLocationName,
            this.colPosLocationTradingName,
            this.colActualAddress,
            this.colPosLocationCodeFound,
            this.colPosLocationNameFound,
            this.colPosLocationTradingNameFound,
            this.colM6Name,
            this.colM5Name,
            this.colM4Name,
            this.colM3Name,
            this.colM2Name,
            this.colM1Name,
            this.colDay0,
            this.colDay1,
            this.colDay2,
            this.colDay3,
            this.colDay4,
            this.colDay5,
            this.colDay6,
            this.colDay7,
            this.colDay8,
            this.colDay9,
            this.colDay10,
            this.colDay11,
            this.colDay12,
            this.colDay13,
            this.colDay14,
            this.colDay15,
            this.colDay16,
            this.colDay17,
            this.colDay18,
            this.colDay19,
            this.colDay20,
            this.colDay21,
            this.colDay22,
            this.colDay23,
            this.colDay24,
            this.colDay25,
            this.colDay26,
            this.colDay27,
            this.colDay28,
            this.colDay29,
            this.colDay30,
            this.colPosLastStatus,
            this.colPosAbsenceReason,
            this.colPosPhoto,
            this.colPosPhotoReal,
            this.colDayPhoto0,
            this.colDayPhoto1,
            this.colDayPhoto2,
            this.colDayPhoto3,
            this.colDayPhoto4,
            this.colDayPhoto5,
            this.colDayPhoto6,
            this.colDayPhoto7,
            this.colDayPhoto8,
            this.colDayPhoto9,
            this.colDayPhoto10,
            this.colDayPhoto11,
            this.colDayPhoto12,
            this.colDayPhoto13,
            this.colDayPhoto14,
            this.colDayPhoto15,
            this.colDayPhoto16,
            this.colDayPhoto17,
            this.colDayPhoto18,
            this.colDayPhoto19,
            this.colDayPhoto20,
            this.colDayPhoto21,
            this.colDayPhoto22,
            this.colDayPhoto23,
            this.colDayPhoto24,
            this.colDayPhoto25,
            this.colDayPhoto26,
            this.colDayPhoto27,
            this.colDayPhoto28,
            this.colDayPhoto29,
            this.colDayPhoto30});
            this.gridView.GridControl = this.gridControl;
            this.gridView.Name = "gridView";
            this.gridView.OptionsBehavior.AllowIncrementalSearch = true;
            this.gridView.OptionsBehavior.Editable = false;
            this.gridView.OptionsBehavior.ReadOnly = true;
            this.gridView.OptionsLayout.LayoutVersion = "1";
            this.gridView.OptionsPrint.AutoWidth = false;
            this.gridView.OptionsSelection.MultiSelect = true;
            this.gridView.OptionsView.ShowAutoFilterRow = true;
            this.gridView.OptionsView.ShowGroupPanel = false;
            this.gridView.RowCellClick += new DevExpress.XtraGrid.Views.Grid.RowCellClickEventHandler(this.gridView_RowCellClick);
            this.gridView.RowCellStyle += new DevExpress.XtraGrid.Views.Grid.RowCellStyleEventHandler(this.gridView_RowCellStyle);
            this.gridView.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gridView_CustomRowCellEdit);
            this.gridView.CustomUnboundColumnData += new DevExpress.XtraGrid.Views.Base.CustomColumnDataEventHandler(this.gridView_CustomUnboundColumnData);
            this.gridView.BeforeLoadLayout += new DevExpress.Utils.LayoutAllowEventHandler(this.gridViewData_BeforeLoadLayout);
            // 
            // colPosGroup
            // 
            this.colPosGroup.AppearanceHeader.Options.UseTextOptions = true;
            this.colPosGroup.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colPosGroup.AppearanceHeader.TextOptions.Trimming = DevExpress.Utils.Trimming.None;
            this.colPosGroup.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colPosGroup.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colPosGroup.AutoFillDown = true;
            this.colPosGroup.Caption = "Группа оборудования";
            this.colPosGroup.FieldName = "POSGroup_Name";
            this.colPosGroup.Name = "colPosGroup";
            this.colPosGroup.OptionsColumn.AllowEdit = false;
            this.colPosGroup.OptionsColumn.ReadOnly = true;
            this.colPosGroup.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colPosGroup.Visible = true;
            this.colPosGroup.Width = 125;
            // 
            // colPosType
            // 
            this.colPosType.AppearanceHeader.Options.UseTextOptions = true;
            this.colPosType.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colPosType.AppearanceHeader.TextOptions.Trimming = DevExpress.Utils.Trimming.None;
            this.colPosType.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colPosType.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colPosType.AutoFillDown = true;
            this.colPosType.Caption = "Модель";
            this.colPosType.FieldName = "SYS_POS_Name";
            this.colPosType.Name = "colPosType";
            this.colPosType.OptionsColumn.AllowEdit = false;
            this.colPosType.OptionsColumn.ReadOnly = true;
            this.colPosType.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colPosType.Visible = true;
            // 
            // colPosSerialNo
            // 
            this.colPosSerialNo.AppearanceHeader.Options.UseTextOptions = true;
            this.colPosSerialNo.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colPosSerialNo.AppearanceHeader.TextOptions.Trimming = DevExpress.Utils.Trimming.None;
            this.colPosSerialNo.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colPosSerialNo.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colPosSerialNo.AutoFillDown = true;
            this.colPosSerialNo.Caption = "Серийный номер";
            this.colPosSerialNo.FieldName = "SYS_POS_SerialNo";
            this.colPosSerialNo.Name = "colPosSerialNo";
            this.colPosSerialNo.OptionsColumn.AllowEdit = false;
            this.colPosSerialNo.OptionsColumn.ReadOnly = true;
            this.colPosSerialNo.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colPosSerialNo.Visible = true;
            // 
            // colPosInventNo
            // 
            this.colPosInventNo.AppearanceHeader.Options.UseTextOptions = true;
            this.colPosInventNo.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colPosInventNo.AppearanceHeader.TextOptions.Trimming = DevExpress.Utils.Trimming.None;
            this.colPosInventNo.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colPosInventNo.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colPosInventNo.AutoFillDown = true;
            this.colPosInventNo.Caption = "Инвентарный номер ";
            this.colPosInventNo.FieldName = "SYS_Invent_No";
            this.colPosInventNo.Name = "colPosInventNo";
            this.colPosInventNo.OptionsColumn.AllowEdit = false;
            this.colPosInventNo.OptionsColumn.ReadOnly = true;
            this.colPosInventNo.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colPosInventNo.Visible = true;
            // 
            // colPosBrand
            // 
            this.colPosBrand.AppearanceHeader.Options.UseTextOptions = true;
            this.colPosBrand.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colPosBrand.AppearanceHeader.TextOptions.Trimming = DevExpress.Utils.Trimming.None;
            this.colPosBrand.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colPosBrand.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colPosBrand.AutoFillDown = true;
            this.colPosBrand.Caption = "Бренд";
            this.colPosBrand.FieldName = "POSBrand_Name";
            this.colPosBrand.Name = "colPosBrand";
            this.colPosBrand.OptionsColumn.AllowEdit = false;
            this.colPosBrand.OptionsColumn.ReadOnly = true;
            this.colPosBrand.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colPosBrand.Visible = true;
            // 
            // colYPosYearProduction
            // 
            this.colYPosYearProduction.AppearanceHeader.Options.UseTextOptions = true;
            this.colYPosYearProduction.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colYPosYearProduction.AppearanceHeader.TextOptions.Trimming = DevExpress.Utils.Trimming.None;
            this.colYPosYearProduction.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colYPosYearProduction.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colYPosYearProduction.AutoFillDown = true;
            this.colYPosYearProduction.Caption = "Дата изготовления";
            this.colYPosYearProduction.FieldName = "SYS_POS_YearProduction";
            this.colYPosYearProduction.Name = "colYPosYearProduction";
            this.colYPosYearProduction.OptionsColumn.AllowEdit = false;
            this.colYPosYearProduction.OptionsColumn.ReadOnly = true;
            this.colYPosYearProduction.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colYPosYearProduction.Visible = true;
            this.colYPosYearProduction.Width = 89;
            // 
            // colTechnicalCondition
            // 
            this.colTechnicalCondition.AppearanceHeader.Options.UseTextOptions = true;
            this.colTechnicalCondition.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTechnicalCondition.AppearanceHeader.TextOptions.Trimming = DevExpress.Utils.Trimming.None;
            this.colTechnicalCondition.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colTechnicalCondition.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTechnicalCondition.AutoFillDown = true;
            this.colTechnicalCondition.Caption = "Техническое состояние";
            this.colTechnicalCondition.FieldName = "TCDesc";
            this.colTechnicalCondition.Name = "colTechnicalCondition";
            this.colTechnicalCondition.OptionsColumn.AllowEdit = false;
            this.colTechnicalCondition.OptionsColumn.ReadOnly = true;
            this.colTechnicalCondition.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colTechnicalCondition.Visible = true;
            this.colTechnicalCondition.Width = 112;
            // 
            // colPosSapId
            // 
            this.colPosSapId.AppearanceHeader.Options.UseTextOptions = true;
            this.colPosSapId.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colPosSapId.AppearanceHeader.TextOptions.Trimming = DevExpress.Utils.Trimming.None;
            this.colPosSapId.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colPosSapId.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colPosSapId.AutoFillDown = true;
            this.colPosSapId.Caption = "SAP Код";
            this.colPosSapId.FieldName = "SAP_ID";
            this.colPosSapId.Name = "colPosSapId";
            this.colPosSapId.OptionsColumn.AllowEdit = false;
            this.colPosSapId.OptionsColumn.ReadOnly = true;
            this.colPosSapId.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colPosSapId.Visible = true;
            // 
            // colPosBluetooth
            // 
            this.colPosBluetooth.AppearanceHeader.Options.UseTextOptions = true;
            this.colPosBluetooth.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colPosBluetooth.AppearanceHeader.TextOptions.Trimming = DevExpress.Utils.Trimming.None;
            this.colPosBluetooth.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colPosBluetooth.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colPosBluetooth.AutoFillDown = true;
            this.colPosBluetooth.Caption = "Bluetooth/QR";
            this.colPosBluetooth.FieldName = "Bluetooth";
            this.colPosBluetooth.Name = "colPosBluetooth";
            this.colPosBluetooth.OptionsColumn.AllowEdit = false;
            this.colPosBluetooth.OptionsColumn.ReadOnly = true;
            this.colPosBluetooth.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colPosBluetooth.Visible = true;
            // 
            // colPosInSystemOrNew
            // 
            this.colPosInSystemOrNew.AppearanceHeader.Options.UseTextOptions = true;
            this.colPosInSystemOrNew.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colPosInSystemOrNew.AppearanceHeader.TextOptions.Trimming = DevExpress.Utils.Trimming.None;
            this.colPosInSystemOrNew.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colPosInSystemOrNew.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colPosInSystemOrNew.AutoFillDown = true;
            this.colPosInSystemOrNew.Caption = "Оборудование в системе/новое?";
            this.colPosInSystemOrNew.FieldName = "COLName10";
            this.colPosInSystemOrNew.Name = "colPosInSystemOrNew";
            this.colPosInSystemOrNew.OptionsColumn.AllowEdit = false;
            this.colPosInSystemOrNew.OptionsColumn.ReadOnly = true;
            this.colPosInSystemOrNew.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colPosInSystemOrNew.Visible = true;
            this.colPosInSystemOrNew.Width = 270;
            // 
            // colPosBrandCorrected
            // 
            this.colPosBrandCorrected.AppearanceHeader.Options.UseTextOptions = true;
            this.colPosBrandCorrected.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colPosBrandCorrected.AppearanceHeader.TextOptions.Trimming = DevExpress.Utils.Trimming.None;
            this.colPosBrandCorrected.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colPosBrandCorrected.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colPosBrandCorrected.AutoFillDown = true;
            this.colPosBrandCorrected.Caption = "Бренд скорректированный";
            this.colPosBrandCorrected.FieldName = "InvPOSBrand_Name";
            this.colPosBrandCorrected.Name = "colPosBrandCorrected";
            this.colPosBrandCorrected.OptionsColumn.AllowEdit = false;
            this.colPosBrandCorrected.OptionsColumn.ReadOnly = true;
            this.colPosBrandCorrected.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colPosBrandCorrected.Visible = true;
            // 
            // colYPosYearProductionCorrected
            // 
            this.colYPosYearProductionCorrected.AppearanceHeader.Options.UseTextOptions = true;
            this.colYPosYearProductionCorrected.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colYPosYearProductionCorrected.AppearanceHeader.TextOptions.Trimming = DevExpress.Utils.Trimming.None;
            this.colYPosYearProductionCorrected.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colYPosYearProductionCorrected.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colYPosYearProductionCorrected.AutoFillDown = true;
            this.colYPosYearProductionCorrected.Caption = "Дата изготовления скорректированная";
            this.colYPosYearProductionCorrected.FieldName = "InvYearProduction";
            this.colYPosYearProductionCorrected.Name = "colYPosYearProductionCorrected";
            this.colYPosYearProductionCorrected.OptionsColumn.AllowEdit = false;
            this.colYPosYearProductionCorrected.OptionsColumn.ReadOnly = true;
            this.colYPosYearProductionCorrected.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colYPosYearProductionCorrected.Visible = true;
            // 
            // colPosLocation
            // 
            this.colPosLocation.AppearanceHeader.Options.UseTextOptions = true;
            this.colPosLocation.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colPosLocation.AppearanceHeader.TextOptions.Trimming = DevExpress.Utils.Trimming.None;
            this.colPosLocation.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colPosLocation.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colPosLocation.AutoFillDown = true;
            this.colPosLocation.Caption = "Локация";
            this.colPosLocation.FieldName = "Location";
            this.colPosLocation.Name = "colPosLocation";
            this.colPosLocation.OptionsColumn.AllowEdit = false;
            this.colPosLocation.OptionsColumn.ReadOnly = true;
            this.colPosLocation.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colPosLocation.Visible = true;
            // 
            // colPosLocationCode
            // 
            this.colPosLocationCode.AppearanceHeader.Options.UseTextOptions = true;
            this.colPosLocationCode.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colPosLocationCode.AppearanceHeader.TextOptions.Trimming = DevExpress.Utils.Trimming.None;
            this.colPosLocationCode.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colPosLocationCode.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colPosLocationCode.AutoFillDown = true;
            this.colPosLocationCode.Caption = "Код ТТ/Склада";
            this.colPosLocationCode.FieldName = "CODE_OL_ID_WH";
            this.colPosLocationCode.Name = "colPosLocationCode";
            this.colPosLocationCode.OptionsColumn.AllowEdit = false;
            this.colPosLocationCode.OptionsColumn.ReadOnly = true;
            this.colPosLocationCode.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colPosLocationCode.Visible = true;
            // 
            // colPosLocationName
            // 
            this.colPosLocationName.AppearanceHeader.Options.UseTextOptions = true;
            this.colPosLocationName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colPosLocationName.AppearanceHeader.TextOptions.Trimming = DevExpress.Utils.Trimming.None;
            this.colPosLocationName.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colPosLocationName.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colPosLocationName.AutoFillDown = true;
            this.colPosLocationName.Caption = "Юридическое название ТТ/ Название склада";
            this.colPosLocationName.FieldName = "OL_WH_NAME";
            this.colPosLocationName.Name = "colPosLocationName";
            this.colPosLocationName.OptionsColumn.AllowEdit = false;
            this.colPosLocationName.OptionsColumn.ReadOnly = true;
            this.colPosLocationName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colPosLocationName.Visible = true;
            // 
            // colPosLocationTradingName
            // 
            this.colPosLocationTradingName.AppearanceHeader.Options.UseTextOptions = true;
            this.colPosLocationTradingName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colPosLocationTradingName.AppearanceHeader.TextOptions.Trimming = DevExpress.Utils.Trimming.None;
            this.colPosLocationTradingName.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colPosLocationTradingName.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colPosLocationTradingName.AutoFillDown = true;
            this.colPosLocationTradingName.Caption = "Фактическое название ТТ/ Название склада";
            this.colPosLocationTradingName.FieldName = "OL_WH_TradName";
            this.colPosLocationTradingName.Name = "colPosLocationTradingName";
            this.colPosLocationTradingName.OptionsColumn.AllowEdit = false;
            this.colPosLocationTradingName.OptionsColumn.ReadOnly = true;
            this.colPosLocationTradingName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colPosLocationTradingName.Visible = true;
            // 
            // colPosLocationCodeFound
            // 
            this.colPosLocationCodeFound.AppearanceHeader.Options.UseTextOptions = true;
            this.colPosLocationCodeFound.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colPosLocationCodeFound.AppearanceHeader.TextOptions.Trimming = DevExpress.Utils.Trimming.None;
            this.colPosLocationCodeFound.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colPosLocationCodeFound.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colPosLocationCodeFound.AutoFillDown = true;
            this.colPosLocationCodeFound.Caption = "Код ТТ/Склада найденный";
            this.colPosLocationCodeFound.FieldName = "CODE_OL_ID_WH_Found";
            this.colPosLocationCodeFound.Name = "colPosLocationCodeFound";
            this.colPosLocationCodeFound.OptionsColumn.AllowEdit = false;
            this.colPosLocationCodeFound.OptionsColumn.ReadOnly = true;
            this.colPosLocationCodeFound.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colPosLocationCodeFound.Visible = true;
            this.colPosLocationCodeFound.Width = 96;
            // 
            // colPosLocationNameFound
            // 
            this.colPosLocationNameFound.AppearanceHeader.Options.UseTextOptions = true;
            this.colPosLocationNameFound.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colPosLocationNameFound.AppearanceHeader.TextOptions.Trimming = DevExpress.Utils.Trimming.None;
            this.colPosLocationNameFound.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colPosLocationNameFound.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colPosLocationNameFound.AutoFillDown = true;
            this.colPosLocationNameFound.Caption = "Юридическое название ТТ/ Название склада найденное";
            this.colPosLocationNameFound.FieldName = "OL_WH_NAME_Found";
            this.colPosLocationNameFound.Name = "colPosLocationNameFound";
            this.colPosLocationNameFound.OptionsColumn.AllowEdit = false;
            this.colPosLocationNameFound.OptionsColumn.ReadOnly = true;
            this.colPosLocationNameFound.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colPosLocationNameFound.Visible = true;
            // 
            // colPosLocationTradingNameFound
            // 
            this.colPosLocationTradingNameFound.AppearanceHeader.Options.UseTextOptions = true;
            this.colPosLocationTradingNameFound.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colPosLocationTradingNameFound.AppearanceHeader.TextOptions.Trimming = DevExpress.Utils.Trimming.None;
            this.colPosLocationTradingNameFound.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colPosLocationTradingNameFound.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colPosLocationTradingNameFound.AutoFillDown = true;
            this.colPosLocationTradingNameFound.Caption = "Фактическое название ТТ/ Название склада найденное";
            this.colPosLocationTradingNameFound.FieldName = "OL_WH_TradName_Found";
            this.colPosLocationTradingNameFound.Name = "colPosLocationTradingNameFound";
            this.colPosLocationTradingNameFound.OptionsColumn.AllowEdit = false;
            this.colPosLocationTradingNameFound.OptionsColumn.ReadOnly = true;
            this.colPosLocationTradingNameFound.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colPosLocationTradingNameFound.Visible = true;
            // 
            // colM6Name
            // 
            this.colM6Name.AppearanceHeader.Options.UseTextOptions = true;
            this.colM6Name.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colM6Name.AppearanceHeader.TextOptions.Trimming = DevExpress.Utils.Trimming.None;
            this.colM6Name.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colM6Name.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colM6Name.AutoFillDown = true;
            this.colM6Name.Caption = "м6";
            this.colM6Name.FieldName = "M6";
            this.colM6Name.Name = "colM6Name";
            this.colM6Name.OptionsColumn.AllowEdit = false;
            this.colM6Name.OptionsColumn.ReadOnly = true;
            this.colM6Name.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colM6Name.Visible = true;
            // 
            // colM5Name
            // 
            this.colM5Name.AppearanceHeader.Options.UseTextOptions = true;
            this.colM5Name.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colM5Name.AppearanceHeader.TextOptions.Trimming = DevExpress.Utils.Trimming.None;
            this.colM5Name.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colM5Name.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colM5Name.AutoFillDown = true;
            this.colM5Name.Caption = "м5";
            this.colM5Name.FieldName = "M5";
            this.colM5Name.Name = "colM5Name";
            this.colM5Name.OptionsColumn.AllowEdit = false;
            this.colM5Name.OptionsColumn.ReadOnly = true;
            this.colM5Name.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colM5Name.Visible = true;
            // 
            // colM4Name
            // 
            this.colM4Name.AppearanceHeader.Options.UseTextOptions = true;
            this.colM4Name.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colM4Name.AppearanceHeader.TextOptions.Trimming = DevExpress.Utils.Trimming.None;
            this.colM4Name.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colM4Name.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colM4Name.AutoFillDown = true;
            this.colM4Name.Caption = "м4";
            this.colM4Name.FieldName = "M4";
            this.colM4Name.Name = "colM4Name";
            this.colM4Name.OptionsColumn.AllowEdit = false;
            this.colM4Name.OptionsColumn.ReadOnly = true;
            this.colM4Name.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colM4Name.Visible = true;
            // 
            // colM3Name
            // 
            this.colM3Name.AppearanceHeader.Options.UseTextOptions = true;
            this.colM3Name.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colM3Name.AppearanceHeader.TextOptions.Trimming = DevExpress.Utils.Trimming.None;
            this.colM3Name.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colM3Name.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colM3Name.AutoFillDown = true;
            this.colM3Name.Caption = "м3";
            this.colM3Name.FieldName = "M3";
            this.colM3Name.Name = "colM3Name";
            this.colM3Name.OptionsColumn.AllowEdit = false;
            this.colM3Name.OptionsColumn.ReadOnly = true;
            this.colM3Name.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colM3Name.Visible = true;
            // 
            // colM2Name
            // 
            this.colM2Name.AppearanceHeader.Options.UseTextOptions = true;
            this.colM2Name.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colM2Name.AppearanceHeader.TextOptions.Trimming = DevExpress.Utils.Trimming.None;
            this.colM2Name.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colM2Name.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colM2Name.AutoFillDown = true;
            this.colM2Name.Caption = "м2";
            this.colM2Name.FieldName = "M2";
            this.colM2Name.Name = "colM2Name";
            this.colM2Name.OptionsColumn.AllowEdit = false;
            this.colM2Name.OptionsColumn.ReadOnly = true;
            this.colM2Name.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colM2Name.Visible = true;
            // 
            // colM1Name
            // 
            this.colM1Name.AppearanceHeader.Options.UseTextOptions = true;
            this.colM1Name.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colM1Name.AppearanceHeader.TextOptions.Trimming = DevExpress.Utils.Trimming.None;
            this.colM1Name.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colM1Name.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colM1Name.AutoFillDown = true;
            this.colM1Name.Caption = "м1";
            this.colM1Name.FieldName = "M1";
            this.colM1Name.Name = "colM1Name";
            this.colM1Name.OptionsColumn.AllowEdit = false;
            this.colM1Name.OptionsColumn.ReadOnly = true;
            this.colM1Name.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colM1Name.Visible = true;
            // 
            // colDay0
            // 
            this.colDay0.Caption = "0";
            this.colDay0.FieldName = "Date0";
            this.colDay0.Name = "colDay0";
            this.colDay0.OptionsColumn.AllowEdit = false;
            this.colDay0.OptionsColumn.ReadOnly = true;
            this.colDay0.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colDay0.Visible = true;
            // 
            // colDay1
            // 
            this.colDay1.Caption = "1";
            this.colDay1.FieldName = "Date1";
            this.colDay1.Name = "colDay1";
            this.colDay1.OptionsColumn.AllowEdit = false;
            this.colDay1.OptionsColumn.ReadOnly = true;
            this.colDay1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colDay1.Visible = true;
            // 
            // colDay2
            // 
            this.colDay2.Caption = "2";
            this.colDay2.FieldName = "Date2";
            this.colDay2.Name = "colDay2";
            this.colDay2.OptionsColumn.AllowEdit = false;
            this.colDay2.OptionsColumn.ReadOnly = true;
            this.colDay2.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colDay2.Visible = true;
            // 
            // colDay3
            // 
            this.colDay3.Caption = "3";
            this.colDay3.FieldName = "Date3";
            this.colDay3.Name = "colDay3";
            this.colDay3.OptionsColumn.AllowEdit = false;
            this.colDay3.OptionsColumn.ReadOnly = true;
            this.colDay3.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colDay3.Visible = true;
            // 
            // colDay4
            // 
            this.colDay4.Caption = "4";
            this.colDay4.FieldName = "Date4";
            this.colDay4.Name = "colDay4";
            this.colDay4.OptionsColumn.AllowEdit = false;
            this.colDay4.OptionsColumn.ReadOnly = true;
            this.colDay4.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colDay4.Visible = true;
            // 
            // colDay5
            // 
            this.colDay5.Caption = "5";
            this.colDay5.FieldName = "Date5";
            this.colDay5.Name = "colDay5";
            this.colDay5.OptionsColumn.AllowEdit = false;
            this.colDay5.OptionsColumn.ReadOnly = true;
            this.colDay5.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colDay5.Visible = true;
            // 
            // colDay6
            // 
            this.colDay6.Caption = "6";
            this.colDay6.FieldName = "Date6";
            this.colDay6.Name = "colDay6";
            this.colDay6.OptionsColumn.AllowEdit = false;
            this.colDay6.OptionsColumn.ReadOnly = true;
            this.colDay6.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colDay6.Visible = true;
            // 
            // colDay7
            // 
            this.colDay7.Caption = "7";
            this.colDay7.FieldName = "Date7";
            this.colDay7.Name = "colDay7";
            this.colDay7.OptionsColumn.AllowEdit = false;
            this.colDay7.OptionsColumn.ReadOnly = true;
            this.colDay7.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colDay7.Visible = true;
            // 
            // colDay8
            // 
            this.colDay8.Caption = "8";
            this.colDay8.FieldName = "Date8";
            this.colDay8.Name = "colDay8";
            this.colDay8.OptionsColumn.AllowEdit = false;
            this.colDay8.OptionsColumn.ReadOnly = true;
            this.colDay8.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colDay8.Visible = true;
            // 
            // colDay9
            // 
            this.colDay9.Caption = "9";
            this.colDay9.FieldName = "Date9";
            this.colDay9.Name = "colDay9";
            this.colDay9.OptionsColumn.AllowEdit = false;
            this.colDay9.OptionsColumn.ReadOnly = true;
            this.colDay9.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colDay9.Visible = true;
            // 
            // colDay10
            // 
            this.colDay10.Caption = "10";
            this.colDay10.FieldName = "Date10";
            this.colDay10.Name = "colDay10";
            this.colDay10.OptionsColumn.AllowEdit = false;
            this.colDay10.OptionsColumn.ReadOnly = true;
            this.colDay10.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colDay10.Visible = true;
            // 
            // colDay11
            // 
            this.colDay11.Caption = "11";
            this.colDay11.FieldName = "Date11";
            this.colDay11.Name = "colDay11";
            this.colDay11.OptionsColumn.AllowEdit = false;
            this.colDay11.OptionsColumn.ReadOnly = true;
            this.colDay11.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colDay11.Visible = true;
            // 
            // colDay12
            // 
            this.colDay12.Caption = "12";
            this.colDay12.FieldName = "Date12";
            this.colDay12.Name = "colDay12";
            this.colDay12.OptionsColumn.AllowEdit = false;
            this.colDay12.OptionsColumn.ReadOnly = true;
            this.colDay12.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colDay12.Visible = true;
            // 
            // colDay13
            // 
            this.colDay13.Caption = "13";
            this.colDay13.FieldName = "Date13";
            this.colDay13.Name = "colDay13";
            this.colDay13.OptionsColumn.AllowEdit = false;
            this.colDay13.OptionsColumn.ReadOnly = true;
            this.colDay13.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colDay13.Visible = true;
            // 
            // colDay14
            // 
            this.colDay14.Caption = "14";
            this.colDay14.FieldName = "Date14";
            this.colDay14.Name = "colDay14";
            this.colDay14.OptionsColumn.AllowEdit = false;
            this.colDay14.OptionsColumn.ReadOnly = true;
            this.colDay14.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colDay14.Visible = true;
            // 
            // colDay15
            // 
            this.colDay15.Caption = "15";
            this.colDay15.FieldName = "Date15";
            this.colDay15.Name = "colDay15";
            this.colDay15.OptionsColumn.AllowEdit = false;
            this.colDay15.OptionsColumn.ReadOnly = true;
            this.colDay15.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colDay15.Visible = true;
            // 
            // colDay16
            // 
            this.colDay16.Caption = "16";
            this.colDay16.FieldName = "Date16";
            this.colDay16.Name = "colDay16";
            this.colDay16.OptionsColumn.AllowEdit = false;
            this.colDay16.OptionsColumn.ReadOnly = true;
            this.colDay16.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colDay16.Visible = true;
            // 
            // colDay17
            // 
            this.colDay17.Caption = "17";
            this.colDay17.FieldName = "Date17";
            this.colDay17.Name = "colDay17";
            this.colDay17.OptionsColumn.AllowEdit = false;
            this.colDay17.OptionsColumn.ReadOnly = true;
            this.colDay17.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colDay17.Visible = true;
            // 
            // colDay18
            // 
            this.colDay18.Caption = "18";
            this.colDay18.FieldName = "Date18";
            this.colDay18.Name = "colDay18";
            this.colDay18.OptionsColumn.AllowEdit = false;
            this.colDay18.OptionsColumn.ReadOnly = true;
            this.colDay18.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colDay18.Visible = true;
            // 
            // colDay19
            // 
            this.colDay19.Caption = "19";
            this.colDay19.FieldName = "Date19";
            this.colDay19.Name = "colDay19";
            this.colDay19.OptionsColumn.AllowEdit = false;
            this.colDay19.OptionsColumn.ReadOnly = true;
            this.colDay19.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colDay19.Visible = true;
            // 
            // colDay20
            // 
            this.colDay20.Caption = "20";
            this.colDay20.FieldName = "Date20";
            this.colDay20.Name = "colDay20";
            this.colDay20.OptionsColumn.AllowEdit = false;
            this.colDay20.OptionsColumn.ReadOnly = true;
            this.colDay20.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colDay20.Visible = true;
            // 
            // colDay21
            // 
            this.colDay21.Caption = "21";
            this.colDay21.FieldName = "Date21";
            this.colDay21.Name = "colDay21";
            this.colDay21.OptionsColumn.AllowEdit = false;
            this.colDay21.OptionsColumn.ReadOnly = true;
            this.colDay21.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colDay21.Visible = true;
            // 
            // colDay22
            // 
            this.colDay22.Caption = "22";
            this.colDay22.FieldName = "Date22";
            this.colDay22.Name = "colDay22";
            this.colDay22.OptionsColumn.AllowEdit = false;
            this.colDay22.OptionsColumn.ReadOnly = true;
            this.colDay22.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colDay22.Visible = true;
            // 
            // colDay23
            // 
            this.colDay23.Caption = "23";
            this.colDay23.FieldName = "Date23";
            this.colDay23.Name = "colDay23";
            this.colDay23.OptionsColumn.AllowEdit = false;
            this.colDay23.OptionsColumn.ReadOnly = true;
            this.colDay23.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colDay23.Visible = true;
            // 
            // colDay24
            // 
            this.colDay24.Caption = "24";
            this.colDay24.FieldName = "Date24";
            this.colDay24.Name = "colDay24";
            this.colDay24.OptionsColumn.AllowEdit = false;
            this.colDay24.OptionsColumn.ReadOnly = true;
            this.colDay24.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colDay24.Visible = true;
            // 
            // colDay25
            // 
            this.colDay25.Caption = "25";
            this.colDay25.FieldName = "Date25";
            this.colDay25.Name = "colDay25";
            this.colDay25.OptionsColumn.AllowEdit = false;
            this.colDay25.OptionsColumn.ReadOnly = true;
            this.colDay25.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colDay25.Visible = true;
            // 
            // colDay26
            // 
            this.colDay26.Caption = "26";
            this.colDay26.FieldName = "Date26";
            this.colDay26.Name = "colDay26";
            this.colDay26.OptionsColumn.AllowEdit = false;
            this.colDay26.OptionsColumn.ReadOnly = true;
            this.colDay26.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colDay26.Visible = true;
            // 
            // colDay27
            // 
            this.colDay27.Caption = "27";
            this.colDay27.FieldName = "Date27";
            this.colDay27.Name = "colDay27";
            this.colDay27.OptionsColumn.AllowEdit = false;
            this.colDay27.OptionsColumn.ReadOnly = true;
            this.colDay27.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colDay27.Visible = true;
            // 
            // colDay28
            // 
            this.colDay28.Caption = "28";
            this.colDay28.FieldName = "Date28";
            this.colDay28.Name = "colDay28";
            this.colDay28.OptionsColumn.AllowEdit = false;
            this.colDay28.OptionsColumn.ReadOnly = true;
            this.colDay28.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colDay28.Visible = true;
            // 
            // colDay29
            // 
            this.colDay29.Caption = "29";
            this.colDay29.FieldName = "Date29";
            this.colDay29.Name = "colDay29";
            this.colDay29.OptionsColumn.AllowEdit = false;
            this.colDay29.OptionsColumn.ReadOnly = true;
            this.colDay29.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colDay29.Visible = true;
            // 
            // colDay30
            // 
            this.colDay30.Caption = "30";
            this.colDay30.FieldName = "Date30";
            this.colDay30.Name = "colDay30";
            this.colDay30.OptionsColumn.AllowEdit = false;
            this.colDay30.OptionsColumn.ReadOnly = true;
            this.colDay30.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colDay30.Visible = true;
            // 
            // colPosLastStatus
            // 
            this.colPosLastStatus.AppearanceHeader.Options.UseTextOptions = true;
            this.colPosLastStatus.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colPosLastStatus.AppearanceHeader.TextOptions.Trimming = DevExpress.Utils.Trimming.None;
            this.colPosLastStatus.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colPosLastStatus.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colPosLastStatus.AutoFillDown = true;
            this.colPosLastStatus.Caption = "Последний статус";
            this.colPosLastStatus.FieldName = "LastStatus";
            this.colPosLastStatus.Name = "colPosLastStatus";
            this.colPosLastStatus.OptionsColumn.AllowEdit = false;
            this.colPosLastStatus.OptionsColumn.ReadOnly = true;
            this.colPosLastStatus.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colPosLastStatus.Visible = true;
            // 
            // colPosAbsenceReason
            // 
            this.colPosAbsenceReason.AppearanceHeader.Options.UseTextOptions = true;
            this.colPosAbsenceReason.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colPosAbsenceReason.AppearanceHeader.TextOptions.Trimming = DevExpress.Utils.Trimming.None;
            this.colPosAbsenceReason.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colPosAbsenceReason.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colPosAbsenceReason.AutoFillDown = true;
            this.colPosAbsenceReason.Caption = "Причина отсутствия";
            this.colPosAbsenceReason.FieldName = "AbsentReason";
            this.colPosAbsenceReason.Name = "colPosAbsenceReason";
            this.colPosAbsenceReason.OptionsColumn.AllowEdit = false;
            this.colPosAbsenceReason.OptionsColumn.ReadOnly = true;
            this.colPosAbsenceReason.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colPosAbsenceReason.Visible = true;
            // 
            // colPosPhoto
            // 
            this.colPosPhoto.AppearanceCell.Options.UseTextOptions = true;
            this.colPosPhoto.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.colPosPhoto.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.colPosPhoto.AppearanceHeader.Options.UseTextOptions = true;
            this.colPosPhoto.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colPosPhoto.AppearanceHeader.TextOptions.Trimming = DevExpress.Utils.Trimming.None;
            this.colPosPhoto.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colPosPhoto.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colPosPhoto.AutoFillDown = true;
            this.colPosPhoto.Caption = "Фото";
            this.colPosPhoto.ColumnEdit = this.repositoryItemMemoEdit;
            this.colPosPhoto.FieldName = "FotoCustom";
            this.colPosPhoto.Name = "colPosPhoto";
            this.colPosPhoto.OptionsColumn.AllowEdit = false;
            this.colPosPhoto.OptionsColumn.ReadOnly = true;
            this.colPosPhoto.OptionsFilter.AllowFilter = false;
            this.colPosPhoto.UnboundType = DevExpress.Data.UnboundColumnType.String;
            this.colPosPhoto.Visible = true;
            this.colPosPhoto.Width = 910;
            // 
            // repositoryItemMemoEdit
            // 
            this.repositoryItemMemoEdit.AppearanceReadOnly.Options.UseTextOptions = true;
            this.repositoryItemMemoEdit.AppearanceReadOnly.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.repositoryItemMemoEdit.Name = "repositoryItemMemoEdit";
            this.repositoryItemMemoEdit.ReadOnly = true;
            // 
            // colPosPhotoReal
            // 
            this.colPosPhotoReal.FieldName = "Foto";
            this.colPosPhotoReal.Name = "colPosPhotoReal";
            this.colPosPhotoReal.OptionsColumn.ShowInCustomizationForm = false;
            this.colPosPhotoReal.OptionsColumn.ShowInExpressionEditor = false;
            // 
            // colDayPhoto0
            // 
            this.colDayPhoto0.FieldName = "FotoDate0";
            this.colDayPhoto0.Name = "colDayPhoto0";
            this.colDayPhoto0.OptionsColumn.ShowInCustomizationForm = false;
            this.colDayPhoto0.OptionsColumn.ShowInExpressionEditor = false;
            // 
            // colDayPhoto1
            // 
            this.colDayPhoto1.FieldName = "FotoDate1";
            this.colDayPhoto1.Name = "colDayPhoto1";
            this.colDayPhoto1.OptionsColumn.ShowInCustomizationForm = false;
            this.colDayPhoto1.OptionsColumn.ShowInExpressionEditor = false;
            // 
            // colDayPhoto2
            // 
            this.colDayPhoto2.FieldName = "FotoDate2";
            this.colDayPhoto2.Name = "colDayPhoto2";
            this.colDayPhoto2.OptionsColumn.ShowInCustomizationForm = false;
            this.colDayPhoto2.OptionsColumn.ShowInExpressionEditor = false;
            // 
            // colDayPhoto3
            // 
            this.colDayPhoto3.FieldName = "FotoDate3";
            this.colDayPhoto3.Name = "colDayPhoto3";
            this.colDayPhoto3.OptionsColumn.ShowInCustomizationForm = false;
            this.colDayPhoto3.OptionsColumn.ShowInExpressionEditor = false;
            // 
            // colDayPhoto4
            // 
            this.colDayPhoto4.FieldName = "FotoDate4";
            this.colDayPhoto4.Name = "colDayPhoto4";
            this.colDayPhoto4.OptionsColumn.ShowInCustomizationForm = false;
            this.colDayPhoto4.OptionsColumn.ShowInExpressionEditor = false;
            // 
            // colDayPhoto5
            // 
            this.colDayPhoto5.FieldName = "FotoDate5";
            this.colDayPhoto5.Name = "colDayPhoto5";
            this.colDayPhoto5.OptionsColumn.ShowInCustomizationForm = false;
            this.colDayPhoto5.OptionsColumn.ShowInExpressionEditor = false;
            // 
            // colDayPhoto6
            // 
            this.colDayPhoto6.FieldName = "FotoDate6";
            this.colDayPhoto6.Name = "colDayPhoto6";
            this.colDayPhoto6.OptionsColumn.ShowInCustomizationForm = false;
            this.colDayPhoto6.OptionsColumn.ShowInExpressionEditor = false;
            // 
            // colDayPhoto7
            // 
            this.colDayPhoto7.FieldName = "FotoDate7";
            this.colDayPhoto7.Name = "colDayPhoto7";
            // 
            // colDayPhoto8
            // 
            this.colDayPhoto8.FieldName = "FotoDate8";
            this.colDayPhoto8.Name = "colDayPhoto8";
            this.colDayPhoto8.OptionsColumn.ShowInCustomizationForm = false;
            this.colDayPhoto8.OptionsColumn.ShowInExpressionEditor = false;
            // 
            // colDayPhoto9
            // 
            this.colDayPhoto9.FieldName = "FotoDate9";
            this.colDayPhoto9.Name = "colDayPhoto9";
            this.colDayPhoto9.OptionsColumn.ShowInCustomizationForm = false;
            this.colDayPhoto9.OptionsColumn.ShowInExpressionEditor = false;
            // 
            // colDayPhoto10
            // 
            this.colDayPhoto10.FieldName = "FotoDate10";
            this.colDayPhoto10.Name = "colDayPhoto10";
            this.colDayPhoto10.OptionsColumn.ShowInCustomizationForm = false;
            this.colDayPhoto10.OptionsColumn.ShowInExpressionEditor = false;
            // 
            // colDayPhoto11
            // 
            this.colDayPhoto11.FieldName = "FotoDate11";
            this.colDayPhoto11.Name = "colDayPhoto11";
            this.colDayPhoto11.OptionsColumn.ShowInCustomizationForm = false;
            this.colDayPhoto11.OptionsColumn.ShowInExpressionEditor = false;
            // 
            // colDayPhoto12
            // 
            this.colDayPhoto12.FieldName = "FotoDate12";
            this.colDayPhoto12.Name = "colDayPhoto12";
            this.colDayPhoto12.OptionsColumn.ShowInCustomizationForm = false;
            this.colDayPhoto12.OptionsColumn.ShowInExpressionEditor = false;
            // 
            // colDayPhoto13
            // 
            this.colDayPhoto13.FieldName = "FotoDate13";
            this.colDayPhoto13.Name = "colDayPhoto13";
            this.colDayPhoto13.OptionsColumn.ShowInCustomizationForm = false;
            this.colDayPhoto13.OptionsColumn.ShowInExpressionEditor = false;
            // 
            // colDayPhoto14
            // 
            this.colDayPhoto14.FieldName = "FotoDate14";
            this.colDayPhoto14.Name = "colDayPhoto14";
            this.colDayPhoto14.OptionsColumn.ShowInCustomizationForm = false;
            this.colDayPhoto14.OptionsColumn.ShowInExpressionEditor = false;
            // 
            // colDayPhoto15
            // 
            this.colDayPhoto15.FieldName = "FotoDate15";
            this.colDayPhoto15.Name = "colDayPhoto15";
            // 
            // colDayPhoto16
            // 
            this.colDayPhoto16.FieldName = "FotoDate16";
            this.colDayPhoto16.Name = "colDayPhoto16";
            // 
            // colDayPhoto17
            // 
            this.colDayPhoto17.FieldName = "FotoDate17";
            this.colDayPhoto17.Name = "colDayPhoto17";
            // 
            // colDayPhoto18
            // 
            this.colDayPhoto18.FieldName = "FotoDate18";
            this.colDayPhoto18.Name = "colDayPhoto18";
            // 
            // colDayPhoto19
            // 
            this.colDayPhoto19.FieldName = "FotoDate19";
            this.colDayPhoto19.Name = "colDayPhoto19";
            // 
            // colDayPhoto20
            // 
            this.colDayPhoto20.FieldName = "FotoDate20";
            this.colDayPhoto20.Name = "colDayPhoto20";
            // 
            // colDayPhoto21
            // 
            this.colDayPhoto21.FieldName = "FotoDate21";
            this.colDayPhoto21.Name = "colDayPhoto21";
            // 
            // colDayPhoto22
            // 
            this.colDayPhoto22.FieldName = "FotoDate22";
            this.colDayPhoto22.Name = "colDayPhoto22";
            // 
            // colDayPhoto23
            // 
            this.colDayPhoto23.FieldName = "FotoDate23";
            this.colDayPhoto23.Name = "colDayPhoto23";
            // 
            // colDayPhoto24
            // 
            this.colDayPhoto24.FieldName = "FotoDate24";
            this.colDayPhoto24.Name = "colDayPhoto24";
            // 
            // colDayPhoto25
            // 
            this.colDayPhoto25.FieldName = "FotoDate25";
            this.colDayPhoto25.Name = "colDayPhoto25";
            // 
            // colDayPhoto26
            // 
            this.colDayPhoto26.FieldName = "FotoDate26";
            this.colDayPhoto26.Name = "colDayPhoto26";
            // 
            // colDayPhoto27
            // 
            this.colDayPhoto27.FieldName = "FotoDate27";
            this.colDayPhoto27.Name = "colDayPhoto27";
            // 
            // colDayPhoto28
            // 
            this.colDayPhoto28.FieldName = "FotoDate28";
            this.colDayPhoto28.Name = "colDayPhoto28";
            // 
            // colDayPhoto29
            // 
            this.colDayPhoto29.FieldName = "FotoDate29";
            this.colDayPhoto29.Name = "colDayPhoto29";
            // 
            // colDayPhoto30
            // 
            this.colDayPhoto30.FieldName = "FotoDate30";
            this.colDayPhoto30.Name = "colDayPhoto30";
            // 
            // repositoryItemHyperLinkEdit
            // 
            this.repositoryItemHyperLinkEdit.AutoHeight = false;
            this.repositoryItemHyperLinkEdit.Name = "repositoryItemHyperLinkEdit";
            this.repositoryItemHyperLinkEdit.SingleClick = true;
            // 
            // colActualAddress
            // 
            this.colActualAddress.AppearanceHeader.Options.UseTextOptions = true;
            this.colActualAddress.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colActualAddress.AppearanceHeader.TextOptions.Trimming = DevExpress.Utils.Trimming.None;
            this.colActualAddress.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colActualAddress.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colActualAddress.AutoFillDown = true;
            this.colActualAddress.Caption = "Фактический адрес ТТ/Склада";
            this.colActualAddress.FieldName = "OL_WH_ActualAddress";
            this.colActualAddress.Name = "colActualAddress";
            this.colActualAddress.Visible = true;
            // 
            // bandPOS
            // 
            this.bandPOS.AppearanceHeader.Options.UseTextOptions = true;
            this.bandPOS.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandPOS.AppearanceHeader.TextOptions.Trimming = DevExpress.Utils.Trimming.EllipsisCharacter;
            this.bandPOS.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.bandPOS.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandPOS.Caption = "Оборудование";
            this.bandPOS.Children.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.bandPosInfoInSystem,
            this.bandPosInfoCorrected,
            this.bandLocationInSystem,
            this.bandLocationOfFoundPos});
            this.bandPOS.Name = "bandPOS";
            this.bandPOS.VisibleIndex = 0;
            this.bandPOS.Width = 1817;
            // 
            // bandPosInfoInSystem
            // 
            this.bandPosInfoInSystem.AppearanceHeader.Options.UseTextOptions = true;
            this.bandPosInfoInSystem.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandPosInfoInSystem.AppearanceHeader.TextOptions.Trimming = DevExpress.Utils.Trimming.EllipsisCharacter;
            this.bandPosInfoInSystem.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.bandPosInfoInSystem.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandPosInfoInSystem.Caption = "Данные об оборудовании в системе";
            this.bandPosInfoInSystem.Columns.Add(this.colPosGroup);
            this.bandPosInfoInSystem.Columns.Add(this.colPosType);
            this.bandPosInfoInSystem.Columns.Add(this.colPosSerialNo);
            this.bandPosInfoInSystem.Columns.Add(this.colPosInventNo);
            this.bandPosInfoInSystem.Columns.Add(this.colPosBrand);
            this.bandPosInfoInSystem.Columns.Add(this.colYPosYearProduction);
            this.bandPosInfoInSystem.Columns.Add(this.colTechnicalCondition);
            this.bandPosInfoInSystem.Columns.Add(this.colPosSapId);
            this.bandPosInfoInSystem.Columns.Add(this.colPosBluetooth);
            this.bandPosInfoInSystem.Columns.Add(this.colPosInSystemOrNew);
            this.bandPosInfoInSystem.Name = "bandPosInfoInSystem";
            this.bandPosInfoInSystem.RowCount = 2;
            this.bandPosInfoInSystem.VisibleIndex = 0;
            this.bandPosInfoInSystem.Width = 1046;
            // 
            // bandPosInfoCorrected
            // 
            this.bandPosInfoCorrected.AppearanceHeader.Options.UseTextOptions = true;
            this.bandPosInfoCorrected.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandPosInfoCorrected.AppearanceHeader.TextOptions.Trimming = DevExpress.Utils.Trimming.EllipsisCharacter;
            this.bandPosInfoCorrected.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.bandPosInfoCorrected.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandPosInfoCorrected.Caption = "Скорректированные данные";
            this.bandPosInfoCorrected.Columns.Add(this.colPosBrandCorrected);
            this.bandPosInfoCorrected.Columns.Add(this.colYPosYearProductionCorrected);
            this.bandPosInfoCorrected.Name = "bandPosInfoCorrected";
            this.bandPosInfoCorrected.RowCount = 2;
            this.bandPosInfoCorrected.VisibleIndex = 1;
            this.bandPosInfoCorrected.Width = 150;
            // 
            // bandLocationInSystem
            // 
            this.bandLocationInSystem.AppearanceHeader.Options.UseTextOptions = true;
            this.bandLocationInSystem.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandLocationInSystem.AppearanceHeader.TextOptions.Trimming = DevExpress.Utils.Trimming.EllipsisCharacter;
            this.bandLocationInSystem.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.bandLocationInSystem.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandLocationInSystem.Caption = "Локация в системе";
            this.bandLocationInSystem.Columns.Add(this.colPosLocation);
            this.bandLocationInSystem.Columns.Add(this.colPosLocationCode);
            this.bandLocationInSystem.Columns.Add(this.colPosLocationName);
            this.bandLocationInSystem.Columns.Add(this.colPosLocationTradingName);
            this.bandLocationInSystem.Columns.Add(this.colActualAddress);
            this.bandLocationInSystem.Name = "bandLocationInSystem";
            this.bandLocationInSystem.VisibleIndex = 2;
            this.bandLocationInSystem.Width = 375;
            // 
            // bandLocationOfFoundPos
            // 
            this.bandLocationOfFoundPos.AppearanceHeader.Options.UseTextOptions = true;
            this.bandLocationOfFoundPos.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandLocationOfFoundPos.AppearanceHeader.TextOptions.Trimming = DevExpress.Utils.Trimming.EllipsisCharacter;
            this.bandLocationOfFoundPos.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.bandLocationOfFoundPos.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandLocationOfFoundPos.Caption = "Локация найденного оборудования";
            this.bandLocationOfFoundPos.Columns.Add(this.colPosLocationCodeFound);
            this.bandLocationOfFoundPos.Columns.Add(this.colPosLocationNameFound);
            this.bandLocationOfFoundPos.Columns.Add(this.colPosLocationTradingNameFound);
            this.bandLocationOfFoundPos.Name = "bandLocationOfFoundPos";
            this.bandLocationOfFoundPos.VisibleIndex = 3;
            this.bandLocationOfFoundPos.Width = 246;
            // 
            // bandOrgSructure
            // 
            this.bandOrgSructure.AppearanceHeader.Options.UseTextOptions = true;
            this.bandOrgSructure.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandOrgSructure.AppearanceHeader.TextOptions.Trimming = DevExpress.Utils.Trimming.EllipsisCharacter;
            this.bandOrgSructure.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.bandOrgSructure.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandOrgSructure.Caption = "Орг. структура";
            this.bandOrgSructure.Columns.Add(this.colM6Name);
            this.bandOrgSructure.Columns.Add(this.colM5Name);
            this.bandOrgSructure.Columns.Add(this.colM4Name);
            this.bandOrgSructure.Columns.Add(this.colM3Name);
            this.bandOrgSructure.Columns.Add(this.colM2Name);
            this.bandOrgSructure.Columns.Add(this.colM1Name);
            this.bandOrgSructure.Name = "bandOrgSructure";
            this.bandOrgSructure.RowCount = 3;
            this.bandOrgSructure.VisibleIndex = 1;
            this.bandOrgSructure.Width = 450;
            // 
            // bandInventarizationInfo
            // 
            this.bandInventarizationInfo.AppearanceHeader.Options.UseTextOptions = true;
            this.bandInventarizationInfo.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandInventarizationInfo.AppearanceHeader.TextOptions.Trimming = DevExpress.Utils.Trimming.EllipsisCharacter;
            this.bandInventarizationInfo.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.bandInventarizationInfo.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandInventarizationInfo.Caption = "Данные об инвентаризации";
            this.bandInventarizationInfo.Columns.Add(this.colDay0);
            this.bandInventarizationInfo.Columns.Add(this.colDay1);
            this.bandInventarizationInfo.Columns.Add(this.colDay2);
            this.bandInventarizationInfo.Columns.Add(this.colDay3);
            this.bandInventarizationInfo.Columns.Add(this.colDay4);
            this.bandInventarizationInfo.Columns.Add(this.colDay5);
            this.bandInventarizationInfo.Columns.Add(this.colDay6);
            this.bandInventarizationInfo.Columns.Add(this.colDay7);
            this.bandInventarizationInfo.Columns.Add(this.colDay8);
            this.bandInventarizationInfo.Columns.Add(this.colDay9);
            this.bandInventarizationInfo.Columns.Add(this.colDay10);
            this.bandInventarizationInfo.Columns.Add(this.colDay11);
            this.bandInventarizationInfo.Columns.Add(this.colDay12);
            this.bandInventarizationInfo.Columns.Add(this.colDay13);
            this.bandInventarizationInfo.Columns.Add(this.colDay14);
            this.bandInventarizationInfo.Columns.Add(this.colDay15);
            this.bandInventarizationInfo.Columns.Add(this.colDay16);
            this.bandInventarizationInfo.Columns.Add(this.colDay17);
            this.bandInventarizationInfo.Columns.Add(this.colDay18);
            this.bandInventarizationInfo.Columns.Add(this.colDay19);
            this.bandInventarizationInfo.Columns.Add(this.colDay20);
            this.bandInventarizationInfo.Columns.Add(this.colDay21);
            this.bandInventarizationInfo.Columns.Add(this.colDay22);
            this.bandInventarizationInfo.Columns.Add(this.colDay23);
            this.bandInventarizationInfo.Columns.Add(this.colDay24);
            this.bandInventarizationInfo.Columns.Add(this.colDay25);
            this.bandInventarizationInfo.Columns.Add(this.colDay26);
            this.bandInventarizationInfo.Columns.Add(this.colDay27);
            this.bandInventarizationInfo.Columns.Add(this.colDay28);
            this.bandInventarizationInfo.Columns.Add(this.colDay29);
            this.bandInventarizationInfo.Columns.Add(this.colDay30);
            this.bandInventarizationInfo.Columns.Add(this.colPosLastStatus);
            this.bandInventarizationInfo.Columns.Add(this.colPosAbsenceReason);
            this.bandInventarizationInfo.Columns.Add(this.colPosPhoto);
            this.bandInventarizationInfo.Name = "bandInventarizationInfo";
            this.bandInventarizationInfo.RowCount = 3;
            this.bandInventarizationInfo.VisibleIndex = 2;
            this.bandInventarizationInfo.Width = 3385;
            // 
            // DataControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gridControl);
            this.Name = "DataControl";
            this.Size = new System.Drawing.Size(1091, 416);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gridControl;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colPosGroup;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colPosType;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colPosSerialNo;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colPosInventNo;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colPosBrand;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colYPosYearProduction;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colPosSapId;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colPosBluetooth;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colPosInSystemOrNew;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colTechnicalCondition;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colPosBrandCorrected;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colYPosYearProductionCorrected;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colPosLocation;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colPosLocationCode;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colPosLocationName;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colPosLocationTradingName;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colPosLocationCodeFound;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colPosLocationNameFound;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colPosLocationTradingNameFound;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colM6Name;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colM5Name;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colM4Name;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colM3Name;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colM2Name;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colM1Name;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colDay0;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colDay1;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colDay2;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colDay3;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colDay4;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colDay5;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colDay6;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colDay7;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colDay8;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colDay9;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colDay10;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colDay11;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colDay12;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colDay13;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colDay14;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colDay15;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colDay16;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colDay17;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colDay18;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colDay19;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colDay20;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colDay21;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colDay22;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colDay23;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colDay24;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colDay25;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colDay26;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colDay27;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colDay28;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colDay29;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colDay30;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colPosLastStatus;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colPosAbsenceReason;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colPosPhoto;
        internal DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView gridView;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colPosPhotoReal;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEdit;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colDayPhoto0;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colDayPhoto1;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colDayPhoto2;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colDayPhoto3;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colDayPhoto4;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colDayPhoto5;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colDayPhoto6;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colDayPhoto7;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colDayPhoto8;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colDayPhoto9;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colDayPhoto10;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colDayPhoto11;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colDayPhoto12;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colDayPhoto13;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colDayPhoto14;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colDayPhoto15;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colDayPhoto16;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colDayPhoto17;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colDayPhoto18;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colDayPhoto19;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colDayPhoto20;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colDayPhoto21;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colDayPhoto22;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colDayPhoto23;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colDayPhoto24;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colDayPhoto25;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colDayPhoto26;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colDayPhoto27;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colDayPhoto28;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colDayPhoto29;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colDayPhoto30;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit repositoryItemMemoEdit;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colActualAddress;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand bandPOS;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand bandPosInfoInSystem;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand bandPosInfoCorrected;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand bandLocationInSystem;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand bandLocationOfFoundPos;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand bandOrgSructure;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand bandInventarizationInfo;

    }
}
