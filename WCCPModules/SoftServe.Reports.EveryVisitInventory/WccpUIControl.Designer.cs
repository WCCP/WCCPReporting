﻿namespace SoftServe.Reports.EveryVisitInventory
{
    partial class WccpUIControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(WccpUIControl));
            this.barManager = new DevExpress.XtraBars.BarManager(this.components);
            this.menuBar = new DevExpress.XtraBars.Bar();
            this.refreshButton = new DevExpress.XtraBars.BarLargeButtonItem();
            this.settingsButton = new DevExpress.XtraBars.BarLargeButtonItem();
            this.bExport = new DevExpress.XtraBars.BarSubItem();
            this.exportToXlsxButton = new DevExpress.XtraBars.BarLargeButtonItem();
            this.exportToXlsButton = new DevExpress.XtraBars.BarLargeButtonItem();
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.bExportToXlsx = new DevExpress.XtraBars.BarLargeButtonItem();
            this.barSubItem2 = new DevExpress.XtraBars.BarSubItem();
            this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            this.bClearFilter = new DevExpress.XtraBars.BarButtonItem();
            this.bEditFilter = new DevExpress.XtraBars.BarButtonItem();
            this.barToggleSwitchItem1 = new DevExpress.XtraBars.BarToggleSwitchItem();
            this.cShowTotals = new DevExpress.XtraBars.BarCheckItem();
            this.imageCollection = new DevExpress.Utils.ImageCollection(this.components);
            this.tabControl = new DevExpress.XtraTab.XtraTabControl();
            this.dataTabPage = new DevExpress.XtraTab.XtraTabPage();
            this.dataControl = new SoftServe.Reports.EveryVisitInventory.Controls.DataControl();
            ((System.ComponentModel.ISupportInitialize)(this.barManager)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabControl)).BeginInit();
            this.tabControl.SuspendLayout();
            this.dataTabPage.SuspendLayout();
            this.SuspendLayout();
            // 
            // barManager
            // 
            this.barManager.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.menuBar,
            this.bar2,
            this.bar3});
            this.barManager.DockControls.Add(this.barDockControlTop);
            this.barManager.DockControls.Add(this.barDockControlBottom);
            this.barManager.DockControls.Add(this.barDockControlLeft);
            this.barManager.DockControls.Add(this.barDockControlRight);
            this.barManager.Form = this;
            this.barManager.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.settingsButton,
            this.refreshButton,
            this.bExport,
            this.exportToXlsButton,
            this.bExportToXlsx,
            this.barSubItem2,
            this.barButtonItem1,
            this.bClearFilter,
            this.bEditFilter,
            this.barToggleSwitchItem1,
            this.cShowTotals,
            this.exportToXlsxButton});
            this.barManager.LargeImages = this.imageCollection;
            this.barManager.MainMenu = this.bar2;
            this.barManager.MaxItemId = 12;
            this.barManager.StatusBar = this.bar3;
            // 
            // menuBar
            // 
            this.menuBar.BarName = "Tools";
            this.menuBar.DockCol = 0;
            this.menuBar.DockRow = 0;
            this.menuBar.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.menuBar.FloatLocation = new System.Drawing.Point(730, 120);
            this.menuBar.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.refreshButton),
            new DevExpress.XtraBars.LinkPersistInfo(this.settingsButton),
            new DevExpress.XtraBars.LinkPersistInfo(this.bExport)});
            this.menuBar.OptionsBar.AllowQuickCustomization = false;
            this.menuBar.OptionsBar.DisableClose = true;
            this.menuBar.OptionsBar.DisableCustomization = true;
            this.menuBar.OptionsBar.UseWholeRow = true;
            this.menuBar.Text = "Tools";
            // 
            // refreshButton
            // 
            this.refreshButton.Caption = "Обновить";
            this.refreshButton.Id = 1;
            this.refreshButton.LargeImageIndex = 1;
            this.refreshButton.Name = "refreshButton";
            this.refreshButton.ShowCaptionOnBar = false;
            this.refreshButton.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.refreshButton_ItemClick);
            // 
            // settingsButton
            // 
            this.settingsButton.Caption = "Настройка параметров";
            this.settingsButton.Id = 0;
            this.settingsButton.LargeImageIndex = 0;
            this.settingsButton.Name = "settingsButton";
            this.settingsButton.ShowCaptionOnBar = false;
            this.settingsButton.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.settingsButton_ItemClick);
            // 
            // bExport
            // 
            this.bExport.Caption = "Экспортировать";
            this.bExport.Glyph = ((System.Drawing.Image)(resources.GetObject("bExport.Glyph")));
            this.bExport.Id = 2;
            this.bExport.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.exportToXlsxButton),
            new DevExpress.XtraBars.LinkPersistInfo(this.exportToXlsButton)});
            this.bExport.Name = "bExport";
            this.bExport.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionInMenu;
            // 
            // exportToXlsxButton
            // 
            this.exportToXlsxButton.Caption = "Xlsx";
            this.exportToXlsxButton.Glyph = ((System.Drawing.Image)(resources.GetObject("exportToXlsxButton.Glyph")));
            this.exportToXlsxButton.Id = 11;
            this.exportToXlsxButton.Name = "exportToXlsxButton";
            this.exportToXlsxButton.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.exportToXlsxButton.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.exportToXlsxButton_ItemClick);
            // 
            // exportToXlsButton
            // 
            this.exportToXlsButton.Caption = "Xls";
            this.exportToXlsButton.Glyph = ((System.Drawing.Image)(resources.GetObject("exportToXlsButton.Glyph")));
            this.exportToXlsButton.Id = 3;
            this.exportToXlsButton.Name = "exportToXlsButton";
            this.exportToXlsButton.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.exportToXlsButton.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.exportToXlsButton_ItemClick);
            // 
            // bar2
            // 
            this.bar2.BarName = "Main menu";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 1;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar2.OptionsBar.MultiLine = true;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Main menu";
            this.bar2.Visible = false;
            // 
            // bar3
            // 
            this.bar3.BarName = "Status bar";
            this.bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar3.OptionsBar.AllowQuickCustomization = false;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Status bar";
            this.bar3.Visible = false;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(1186, 59);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 400);
            this.barDockControlBottom.Size = new System.Drawing.Size(1186, 23);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 59);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 341);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(1186, 59);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 341);
            // 
            // bExportToXlsx
            // 
            this.bExportToXlsx.Caption = "Xlsx";
            this.bExportToXlsx.Glyph = ((System.Drawing.Image)(resources.GetObject("bExportToXlsx.Glyph")));
            this.bExportToXlsx.Id = 4;
            this.bExportToXlsx.LargeImageIndex = 1;
            this.bExportToXlsx.Name = "bExportToXlsx";
            // 
            // barSubItem2
            // 
            this.barSubItem2.Caption = "barSubItem2";
            this.barSubItem2.Id = 5;
            this.barSubItem2.LargeImageIndex = 3;
            this.barSubItem2.Name = "barSubItem2";
            // 
            // barButtonItem1
            // 
            this.barButtonItem1.Caption = "barButtonItem1";
            this.barButtonItem1.Id = 6;
            this.barButtonItem1.Name = "barButtonItem1";
            // 
            // bClearFilter
            // 
            this.bClearFilter.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.bClearFilter.Caption = "Очистить фильтр";
            this.bClearFilter.Id = 7;
            this.bClearFilter.Name = "bClearFilter";
            this.bClearFilter.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // bEditFilter
            // 
            this.bEditFilter.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.bEditFilter.Caption = "Редактировать фильтр";
            this.bEditFilter.Id = 8;
            this.bEditFilter.Name = "bEditFilter";
            this.bEditFilter.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // barToggleSwitchItem1
            // 
            this.barToggleSwitchItem1.Caption = "barToggleSwitchItem1";
            this.barToggleSwitchItem1.Id = 9;
            this.barToggleSwitchItem1.Name = "barToggleSwitchItem1";
            // 
            // cShowTotals
            // 
            this.cShowTotals.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.cShowTotals.Caption = "Отобразить Totals";
            this.cShowTotals.Id = 10;
            this.cShowTotals.Name = "cShowTotals";
            // 
            // imageCollection
            // 
            this.imageCollection.ImageSize = new System.Drawing.Size(24, 24);
            this.imageCollection.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection.ImageStream")));
            this.imageCollection.Images.SetKeyName(0, "briefcase_ok_24.png");
            this.imageCollection.Images.SetKeyName(1, "refresh_24.png");
            // 
            // tabControl
            // 
            this.tabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl.HeaderLocation = DevExpress.XtraTab.TabHeaderLocation.Bottom;
            this.tabControl.Location = new System.Drawing.Point(0, 59);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedTabPage = this.dataTabPage;
            this.tabControl.ShowTabHeader = DevExpress.Utils.DefaultBoolean.False;
            this.tabControl.Size = new System.Drawing.Size(1186, 341);
            this.tabControl.TabIndex = 4;
            this.tabControl.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.dataTabPage});
            // 
            // dataTabPage
            // 
            this.dataTabPage.Controls.Add(this.dataControl);
            this.dataTabPage.Name = "dataTabPage";
            this.dataTabPage.Size = new System.Drawing.Size(1180, 335);
            this.dataTabPage.Text = "Ежевизитная инвентаризация";
            // 
            // dataControl
            // 
            this.dataControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataControl.Location = new System.Drawing.Point(0, 0);
            this.dataControl.Name = "dataControl";
            this.dataControl.Size = new System.Drawing.Size(1180, 335);
            this.dataControl.TabIndex = 0;
            // 
            // WccpUIControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tabControl);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Name = "WccpUIControl";
            this.Size = new System.Drawing.Size(1186, 423);
            ((System.ComponentModel.ISupportInitialize)(this.barManager)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabControl)).EndInit();
            this.tabControl.ResumeLayout(false);
            this.dataTabPage.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager;
        private DevExpress.XtraBars.Bar menuBar;
        private DevExpress.XtraBars.BarLargeButtonItem refreshButton;
        private DevExpress.XtraBars.BarLargeButtonItem settingsButton;
        private DevExpress.XtraBars.BarSubItem bExport;
        private DevExpress.XtraBars.BarLargeButtonItem exportToXlsButton;
        private DevExpress.XtraBars.BarLargeButtonItem bExportToXlsx;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarSubItem barSubItem2;
        private DevExpress.XtraBars.BarButtonItem barButtonItem1;
        private DevExpress.XtraBars.BarButtonItem bClearFilter;
        private DevExpress.XtraBars.BarButtonItem bEditFilter;
        private DevExpress.XtraBars.BarToggleSwitchItem barToggleSwitchItem1;
        private DevExpress.XtraBars.BarCheckItem cShowTotals;
        private DevExpress.Utils.ImageCollection imageCollection;
        private DevExpress.XtraTab.XtraTabControl tabControl;
        private DevExpress.XtraTab.XtraTabPage dataTabPage;
        private Controls.DataControl dataControl;
        private DevExpress.XtraBars.BarLargeButtonItem exportToXlsxButton;

    }
}
