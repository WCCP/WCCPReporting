﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BLToolkit.DataAccess;
using BLToolkit.Mapping;


namespace SoftServe.Reports.EveryVisitInventory.Models
{
    public class Wave
    {
        [MapField("wInvPeriodId"), PrimaryKey]
        public int Id { get; set; }

        [MapField("FieldName")]
        public string Name { get; set; }
        
        [MapField("StartDate")]
        public DateTime? StartDate { get; set; }

        [MapField("EndDate")]
        public DateTime? EndDate { get; set; }

        [MapField("IsActive")]
        public bool? IsActive { get; set; }
    }
}
