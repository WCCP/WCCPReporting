﻿using BLToolkit.DataAccess;
using BLToolkit.Mapping;

namespace SoftServe.Reports.EveryVisitInventory.Models
{
    public class PosGroup
    {
        [MapField("POSGroup_ID"), PrimaryKey]
        public int Id { get; set; }

        [MapField("POSGroup_Name")]
        public string Name { get; set; }
    }
}
