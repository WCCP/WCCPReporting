﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BLToolkit.DataAccess;
using BLToolkit.Mapping;


namespace SoftServe.Reports.EveryVisitInventory.Models
{
    public class StaffItem
    {
        [MapField("StaffId"), PrimaryKey]
        public int Id { get; set; }

        [MapField("ParentId")]
        public int ParentId { get; set; }

        [MapField("Name")]
        public string Name { get; set; }

        //[MapField("StaffID")]
        //public int? StaffId { get; set; }

        [MapField("Level")]
        public int Level { get; set; }

        public override string ToString()
        {
            return Name ?? string.Empty;
        }

        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }
    }
}
