﻿namespace SoftServe.Reports.EveryVisitInventory.Models
{
    public class Location
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}
