﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using SoftServe.Reports.EveryVisitInventory.Properties;


namespace SoftServe.Reports.EveryVisitInventory.Models
{
    public class Photo
    {
        public Photo(string path, Image image, int number)
        {
            Image = image;
            Path = path;
            Caption = Resources.Photo + " " + number;
        }

        public string Path { get; private set; }
        public Image Image { get; set; }
        public string Caption { get; private set; }
    }
}
