﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BLToolkit.DataAccess;
using BLToolkit.Mapping;


namespace SoftServe.Reports.EveryVisitInventory.Models
{
    public class InventoryAuxilaryStatus
    {
        [MapField("ID_InventoryAuxilaryStatus"), PrimaryKey]
        public int Id { get; set; }

        [MapField("InventoryAuxilaryStatus")]
        public string Name { get; set; }
    }
}
