﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SoftServe.Reports.EveryVisitInventory.Models
{
    public class Settings
    {
        public List<string> StaffIds { get; set; }

        public DateTime DateTo { get; set; }

        public List<string> WaveIds { get; set; }

        public List<string> StatusIds { get; set; }

        public List<string> LocationIds { get; set; }

        public List<string> PosGroupIds { get; set; }
    }
}
