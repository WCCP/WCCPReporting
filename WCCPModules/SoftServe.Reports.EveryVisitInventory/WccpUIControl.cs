﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Windows.Forms;
using Logica.Reports.BaseReportControl;
using Logica.Reports.Common;
using Logica.Reports.Common.WaitWindow;
using SoftServe.Core.Common.View;
using SoftServe.Reports.EveryVisitInventory.DataAccess;
using SoftServe.Reports.EveryVisitInventory.Models;


namespace SoftServe.Reports.EveryVisitInventory
{
    [ToolboxItem(false)]
    public partial class WccpUIControl : SimpleControlView
    {
        #region Fields
        private SettingsForm _settingsForm;
        private Settings _currentSettings;

        private const string COMPANY_DIRECTORY = "SoftServe";
        private const string PROGRAM_DIRECTORY = "WCCP Reporting";
        private const string LAYOUT_FILENAME = "EveryVisitInventory";
        #endregion

        #region Constructor
        public WccpUIControl()
        {
            InitializeComponent();

            _settingsForm = new SettingsForm();
        }
        #endregion

        #region Methods
        public int InitReport()
        {
            WaitManager.StartWait();
            _settingsForm.LoadDataSources();
            WaitManager.StopWait();

            RestoreLayout();
            return HandleSettingsForm() ? 0 : 1;
        }

        private void GenerateReport()
        {
            WaitManager.StartWait();
            dataControl.DataSource = DataProvider.GetDetails(_currentSettings);
            dataControl.SetDayColumnCaptions(_currentSettings.DateTo);
            WaitManager.StopWait();
        }

        private bool HandleSettingsForm()
        {
            if (_settingsForm.ShowDialog(this) == DialogResult.OK)
            {
                _currentSettings = _settingsForm.Settings;

                GenerateReport();
                return true;
            }

            return false;
        }

        private string SelectFilePath(string reportCaption, ExportToType exportType)
        {
            String res = String.Empty;
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Title = "Сохранить";
            sfd.InitialDirectory = Assembly.GetExecutingAssembly().Location;
            sfd.FileName = reportCaption;
            sfd.Filter = String.Format("(*.{0})|*.{0}", exportType.ToString().ToLower());
            sfd.FilterIndex = 1;
            sfd.OverwritePrompt = true;
            sfd.RestoreDirectory = true;

            if (sfd.ShowDialog() != DialogResult.Cancel)
            {
                res = sfd.FileName;
            }

            return res;
        }

        private void ExportTab(ExportToType exportType)
        {
            string lPath = SelectFilePath(dataTabPage.Text, exportType);

            if (!string.IsNullOrEmpty(lPath))
            {
                try
                {
                    dataControl.Export(exportType, lPath);
                }
                catch (Exception)
                {
                    ErrorManager.ShowErrorBox("Ошибка при экспорте");
                }
            }
        }
        #endregion

        #region Event handlers
        private void settingsButton_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            HandleSettingsForm();
        }

        private void refreshButton_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            GenerateReport();
        }

        private void exportToXlsxButton_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            ExportTab(ExportToType.Xlsx);
        }

        private void exportToXlsButton_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            ExportTab(ExportToType.Xls);
        }
        #endregion
        

        #region Save/restore layout

        private string GetLayoutPath()
        {
            string companyDirPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), COMPANY_DIRECTORY);

            if (!Directory.Exists(companyDirPath))
            {
                Directory.CreateDirectory(companyDirPath);
            }

            string programDirPath = Path.Combine(companyDirPath, PROGRAM_DIRECTORY);

            if (!Directory.Exists(programDirPath))
            {
                Directory.CreateDirectory(programDirPath);
            }

            return Path.Combine(programDirPath, LAYOUT_FILENAME);
        }

        internal void SaveLayout()
        {
            try
            {
                string layoutPath = GetLayoutPath();
                dataControl.SaveLayout(layoutPath);
            }
            catch (Exception) { }
        }

        private void RestoreLayout()
        {
            try
            {
                string layoutPath = GetLayoutPath();
                dataControl.RestoreLayout(layoutPath);
            }
            catch (Exception) { }
        }

        #endregion
    }
}
