﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using BLToolkit.Data;
using Logica.Reports.BaseReportControl.CommonFunctionality;
using Logica.Reports.DataAccess;
using SoftServe.Reports.EveryVisitInventory.Models;

namespace SoftServe.Reports.EveryVisitInventory.DataAccess
{
    public static class DataProvider
    {
        #region Fields
        private static DbManager _db;
        #endregion

        #region Properties
        private static DbManager Db
        {
            get
            {
                if (_db == null)
                {
                    DbManagerInit();
                }
                else if (CheckDbWasChanged())
                {
                    _db.Close();
                    DbManagerInit();
                }
                return _db;
            }
        }
        #endregion

        #region Methods
        public static List<StaffItem> GetStaffTree(DateTime dateTo)
        {
            return
                Db.SetSpCommand(Constants.SP_GETSTAFFTREE,
                    Db.Parameter(Constants.SP_GETSTAFFTREE_DATEFROM, dateTo.AddDays(- Constants.DAYS_COUNT)),
                    Db.Parameter(Constants.SP_GETSTAFFTREE_DATETO, dateTo)).ExecuteList<StaffItem>().ToList();
        }

        public static DataTable GetWaves()
        {
            return
                Db.SetSpCommand(Constants.SP_GETWAVES, Db.Parameter(Constants.SP_GETWAVES_TYPE, 3)).ExecuteDataTable();
        }

        public static List<InventoryAuxilaryStatus> GetInventoryAuxilaryStatuses()
        {
            return
                Db.SetCommand(CommandType.Text, Constants.SQL_SELECT_INVENTORYAUXILARYSTATUS)
                    .ExecuteList<InventoryAuxilaryStatus>();
        }

        public static List<PosGroup> GetPosGroupes(List<string> invPeriodIds)
        {
            object invPeriodIdsXml = DBNull.Value;
            if (invPeriodIds != null && invPeriodIds.Count > 0)
            {
                invPeriodIdsXml = XmlHelper.GetXmlDescription(invPeriodIds);
            }
            //int invPeriodId = 67;
            return
                Db.SetSpCommand(Constants.SP_GETPOSGROUPS, Db.Parameter(Constants.SP_GETPOSGROUPS_PERIODID, invPeriodIdsXml))
                    .ExecuteList<PosGroup>();
        }

        public static DataTable GetDetails(Settings settings)
        {
            object staffIds = DBNull.Value; 
            if (settings.StaffIds.Count > 0)
            {
                staffIds = XmlHelper.GetXmlDescription(settings.StaffIds);
                //staffIds = "<root><row field_id=\"5159\" /></root>";
            }
            object waveIds = DBNull.Value; ;
            if (settings.WaveIds.Count > 0)
            {
                waveIds = XmlHelper.GetXmlDescription(settings.WaveIds);
            }
            object statusIds = DBNull.Value;
            if (settings.StatusIds.Count > 0)
            {
                statusIds = XmlHelper.GetXmlDescription(settings.StatusIds);
            }
            object posGroupIds = DBNull.Value; 
            if (settings.PosGroupIds.Count > 0)
            {
                posGroupIds = XmlHelper.GetXmlDescription(settings.PosGroupIds);
            }
            object locationIds = DBNull.Value;
            if (settings.LocationIds.Count > 0)
            {
                locationIds = XmlHelper.GetXmlDescription(settings.LocationIds);
            }

            return Db.SetSpCommand(Constants.SP_GETREPORTDATA,
                Db.Parameter(Constants.SP_GETREPORTDATA_DATEFROM, settings.DateTo.AddDays(- Constants.DAYS_COUNT)),
                Db.Parameter(Constants.SP_GETREPORTDATA_DATETO, settings.DateTo),
                Db.Parameter(Constants.SP_GETREPORTDATA_STAFF, staffIds),
                Db.Parameter(Constants.SP_GETREPORTDATA_WAVEIDS, waveIds), //67
                Db.Parameter(Constants.SP_GETREPORTDATA_STATUSIDS, statusIds),
                Db.Parameter(Constants.SP_GETREPORTDATA_POSGROUPIDS, posGroupIds),
                Db.Parameter(Constants.SP_GETREPORTDATA_LOCATIONS, locationIds) 
                ).ExecuteDataTable();
        }

        private static bool CheckDbWasChanged()
        {
            return !HostConfiguration.DBSqlConnection.Contains(_db.Connection.ConnectionString);
        }

        private static void DbManagerInit()
        {
            DbManager.DefaultConfiguration = ""; //to reset possible previous changes
            DbManager.AddConnectionString(HostConfiguration.DBSqlConnection);
            _db = new DbManager();
            _db.Command.CommandTimeout = 30 * 60; // 30 minutes by default
        }
        #endregion
    }
}
