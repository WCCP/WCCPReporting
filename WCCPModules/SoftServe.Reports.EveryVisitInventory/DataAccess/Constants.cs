﻿namespace SoftServe.Reports.EveryVisitInventory.DataAccess
{
    public static class Constants
    {
        #region Settings Form
        public const string SP_GETSTAFFTREE = "spDW_Get_OrgStrByUSER";
        public const string SP_GETSTAFFTREE_DATEFROM = "DateFrom";
        public const string SP_GETSTAFFTREE_DATETO = "DateTo";

        public const string SP_GETWAVES = "Pos.spDW_GetInventoryWaves";
        public const string SP_GETWAVES_TYPE = "InvType";

        public const string SQL_SELECT_INVENTORYAUXILARYSTATUS = "select * from DW_InventoryAuxilaryStatus ORDER BY InventoryAuxilaryStatus";

        public const string SP_GETPOSGROUPS = "spDW_Get_InventoryPeriodByGroup";
        public const string SP_GETPOSGROUPS_PERIODID = "wInvPeriodId";

        public const string SP_GETREPORTDATA = "spDW_Get_InventoryByVisit_Fact_POS";
        public const string SP_GETREPORTDATA_DATEFROM = "DateFrom";
        public const string SP_GETREPORTDATA_DATETO = "DateTo";
        public const string SP_GETREPORTDATA_STAFF = "M1";
        public const string SP_GETREPORTDATA_WAVEIDS = "wInvPeriodID";
        public const string SP_GETREPORTDATA_STATUSIDS = "LastPOS_StatusInventory";
        public const string SP_GETREPORTDATA_POSGROUPIDS = "POS_Group";
        public const string SP_GETREPORTDATA_LOCATIONS = "POS_Location";
        
        public const int DAYS_COUNT = 30;
        #endregion

        #region Main Report

        #endregion
    }
}
