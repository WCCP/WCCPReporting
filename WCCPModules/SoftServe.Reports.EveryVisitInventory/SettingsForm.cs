﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using Logica.Reports.BaseReportControl.CommonFunctionality;
using SoftServe.Reports.EveryVisitInventory.DataAccess;
using SoftServe.Reports.EveryVisitInventory.Models;
using SoftServe.Reports.EveryVisitInventory.Properties;


namespace SoftServe.Reports.EveryVisitInventory
{
    public partial class SettingsForm : XtraForm
    {
        #region Fields
        private bool _isLoading = false;
        #endregion

        #region Constructor
        public SettingsForm()
        {
            InitializeComponent();
        }
        #endregion

        #region Properties
        public Settings Settings { get; set; }
        #endregion

        #region Methods
        public void LoadDataSources()
        {
            _isLoading = true;

            LoadStaff(DateTime.Now.Date);
            LoadWaves();
            LoadDate();
            LoadStatuses();
            LoadLocations();
            LoadPosGroupes();

            _isLoading = false;
        }

        private void LoadStaff(DateTime dateTo)
        {
            var staffList = DataProvider.GetStaffTree(dateTo);
            //var staffList = DataProvider.GetStaffTree();
            staffTreeList.DataSource = staffList;
            staffTreeList.ExpandAll();
            staffTreeList.CollapseAll();
        }

        private void LoadWaves()
        {
            var wavesDataTable = DataProvider.GetWaves();
            waveGridLookUp.LoadDataSource(wavesDataTable);
            waveGridLookUp.SelectAll();
        }

        private void LoadDate()
        {
            dateToDateEdit.DateTime = DateTime.Now.Date;
        }

        private void LoadStatuses()
        {
            var statuses = DataProvider.GetInventoryAuxilaryStatuses();
            foreach (var status in statuses)
            {
                lastInventoryStatusCheckedComboBoxEdit.Properties.Items.Add(status.Id, status.Name);
            }
            lastInventoryStatusCheckedComboBoxEdit.CheckAll();
        }

        private void LoadLocations()
        {
            var locations = new List<Location>()
            {
                new Location {Id = 0, Name = Resources.Warehouse},
                new Location {Id = 1, Name = Resources.Outlet},
            };
            foreach (var location in locations)
            {
                locationCheckedComboBoxEdit.Properties.Items.Add(location.Id, location.Name);
            }
            locationCheckedComboBoxEdit.CheckAll();
        }

        private void LoadPosGroupes()
        {
            var invPeriodIds = waveGridLookUp.SelectedValues(colWaveId.FieldName);
            equipmentGroupCheckedComboBoxEdit.Properties.Items.Clear();
            var posGroupes = DataProvider.GetPosGroupes(invPeriodIds);
            foreach (var posGroup in posGroupes)
            {
                equipmentGroupCheckedComboBoxEdit.Properties.Items.Add(posGroup.Id, posGroup.Name);
            }
            equipmentGroupCheckedComboBoxEdit.CheckAll();
        }

        private void GetSettings()
        {
            Settings = new Settings();

            Settings.StaffIds = GetM1Checked();
            Settings.DateTo = dateToDateEdit.DateTime;
            Settings.WaveIds = waveGridLookUp.SelectedValues(colWaveId.FieldName);
            Settings.StatusIds = lastInventoryStatusCheckedComboBoxEdit.Properties.Items.GetCheckedValues().Select(id => id.ToString()).ToList();
            Settings.LocationIds = locationCheckedComboBoxEdit.Properties.Items.GetCheckedValues().Select(id => id.ToString()).ToList();
            Settings.PosGroupIds = equipmentGroupCheckedComboBoxEdit.Properties.Items.GetCheckedValues().Select(id => id.ToString()).ToList();

        }

        private List<string> GetM1Checked()
        {
            var checkedNodes = staffTreeList.GetAllCheckedNodes();
            if (checkedNodes != null && checkedNodes.Count > 0)
            {
                return checkedNodes.Select(node => node.GetValue(colStaffID).ToString()).ToList();

            }
            return new List<string>();
        }

        private bool ValidateSettings(out string lWarningMessage)
        {
            string lMessage = string.Empty;
            if (Settings.StaffIds.Count == 0)
            {
                lMessage += "- Нужно выбрать персонал;" +
                            Environment.NewLine;
            }
            if (Settings.WaveIds.Count == 0)
            {
                lMessage += "- Нужно выбрать хотя бы одну волну;" +
                            Environment.NewLine;
            }
            if (Settings.StatusIds.Count == 0)
            {
                lMessage += "- Нужно выбрать хотя бы один последний статус инвентаризации;" +
                            Environment.NewLine;
            }
            if (Settings.LocationIds.Count == 0)
            {
                lMessage += "- Нужно выбрать хотя бы одину локацию;" +
                            Environment.NewLine;
            }
            if (Settings.PosGroupIds.Count == 0)
            {
                lMessage += "- Нужно выбрать хотя бы одну группу оборудования;" +
                            Environment.NewLine;
            }

            lWarningMessage = lMessage;

            return string.IsNullOrEmpty(lMessage);
        }

        private void ComboBoxUnselectAll(CheckedComboBoxEdit checkedComboBoxEdit)
        {
            for (int i = 0; i < checkedComboBoxEdit.Properties.Items.Count; i++)
            {
                checkedComboBoxEdit.Properties.Items[i].CheckState = CheckState.Unchecked;
            }
        }

        #endregion

        #region Event handlers
        private void activeVaweCheckEdit_CheckedChanged(object sender, EventArgs e)
        {
            string filter = activeVaweCheckEdit.Checked ? "IsActive = 1" : "1 = 1";
            waveGridLookUp.FilterList(filter);
        }

        private void dateToDateEdit_EditValueChanged(object sender, EventArgs e)
        {
            if (!_isLoading)
            {
                LoadStaff(dateToDateEdit.DateTime);
            }
        }

        private void waveGridLookUp_Closed(object sender, ClosedEventArgs e)
        {
            if (!_isLoading)
            {
                LoadPosGroupes();
            }
        }

        private void okButton_Click(object sender, EventArgs e)
        {
            GetSettings();
            string lWarningMessage;

            if (ValidateSettings(out lWarningMessage))
            {
                DialogResult = DialogResult.OK;
            }
            else
            {
                XtraMessageBox.Show("Некорректный набор параметров:" + Environment.NewLine + lWarningMessage, "Параметры", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }
        #endregion
    }
}
