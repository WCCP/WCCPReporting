﻿namespace SoftServe.Reports.EveryVisitInventory
{
    partial class PhotoForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PhotoForm));
            this.mainPanel = new DevExpress.XtraEditors.PanelControl();
            this.splitContainerControl = new DevExpress.XtraEditors.SplitContainerControl();
            this.listBoxControl = new DevExpress.XtraEditors.ListBoxControl();
            this.toolTipController = new DevExpress.Utils.ToolTipController(this.components);
            this.pictureEdit = new DevExpress.XtraEditors.PictureEdit();
            this.buttonsPanel = new DevExpress.XtraEditors.PanelControl();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.turnRightButton = new DevExpress.XtraEditors.SimpleButton();
            this.prevButton = new DevExpress.XtraEditors.SimpleButton();
            this.turnLeftButton = new DevExpress.XtraEditors.SimpleButton();
            this.nextButton = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.mainPanel)).BeginInit();
            this.mainPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl)).BeginInit();
            this.splitContainerControl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.listBoxControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonsPanel)).BeginInit();
            this.buttonsPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // mainPanel
            // 
            this.mainPanel.Controls.Add(this.splitContainerControl);
            this.mainPanel.Controls.Add(this.buttonsPanel);
            this.mainPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainPanel.Location = new System.Drawing.Point(0, 0);
            this.mainPanel.Name = "mainPanel";
            this.mainPanel.Size = new System.Drawing.Size(649, 389);
            this.mainPanel.TabIndex = 0;
            // 
            // splitContainerControl
            // 
            this.splitContainerControl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainerControl.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl.Name = "splitContainerControl";
            this.splitContainerControl.Panel1.Controls.Add(this.listBoxControl);
            this.splitContainerControl.Panel1.Text = "Panel1";
            this.splitContainerControl.Panel2.Controls.Add(this.pictureEdit);
            this.splitContainerControl.Panel2.Text = "Panel2";
            this.splitContainerControl.Size = new System.Drawing.Size(649, 354);
            this.splitContainerControl.SplitterPosition = 123;
            this.splitContainerControl.TabIndex = 3;
            this.splitContainerControl.Text = "splitContainerControl1";
            // 
            // listBoxControl
            // 
            this.listBoxControl.DisplayMember = "Caption";
            this.listBoxControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listBoxControl.Location = new System.Drawing.Point(0, 0);
            this.listBoxControl.Name = "listBoxControl";
            this.listBoxControl.Size = new System.Drawing.Size(123, 354);
            this.listBoxControl.TabIndex = 2;
            this.listBoxControl.ToolTipController = this.toolTipController;
            this.listBoxControl.ValueMember = "Caption";
            this.listBoxControl.SelectedIndexChanged += new System.EventHandler(this.listBoxControl_SelectedIndexChanged);
            this.listBoxControl.MouseLeave += new System.EventHandler(this.listBoxControl_MouseLeave);
            this.listBoxControl.MouseMove += new System.Windows.Forms.MouseEventHandler(this.listBoxControl_MouseMove);
            // 
            // pictureEdit
            // 
            this.pictureEdit.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureEdit.Location = new System.Drawing.Point(0, 0);
            this.pictureEdit.Name = "pictureEdit";
            this.pictureEdit.Properties.AllowFocused = false;
            this.pictureEdit.Properties.ReadOnly = true;
            this.pictureEdit.Properties.ShowScrollBars = true;
            this.pictureEdit.Properties.ShowZoomSubMenu = DevExpress.Utils.DefaultBoolean.True;
            this.pictureEdit.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit.Size = new System.Drawing.Size(521, 354);
            this.pictureEdit.TabIndex = 0;
            // 
            // buttonsPanel
            // 
            this.buttonsPanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonsPanel.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.buttonsPanel.Appearance.Options.UseBackColor = true;
            this.buttonsPanel.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.buttonsPanel.Controls.Add(this.panelControl1);
            this.buttonsPanel.Location = new System.Drawing.Point(0, 353);
            this.buttonsPanel.Name = "buttonsPanel";
            this.buttonsPanel.Size = new System.Drawing.Size(649, 36);
            this.buttonsPanel.TabIndex = 1;
            // 
            // panelControl1
            // 
            this.panelControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl1.Controls.Add(this.turnRightButton);
            this.panelControl1.Controls.Add(this.prevButton);
            this.panelControl1.Controls.Add(this.turnLeftButton);
            this.panelControl1.Controls.Add(this.nextButton);
            this.panelControl1.Location = new System.Drawing.Point(225, 3);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(198, 31);
            this.panelControl1.TabIndex = 2;
            // 
            // turnRightButton
            // 
            this.turnRightButton.AllowFocus = false;
            this.turnRightButton.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.turnRightButton.Image = ((System.Drawing.Image)(resources.GetObject("turnRightButton.Image")));
            this.turnRightButton.Location = new System.Drawing.Point(154, 4);
            this.turnRightButton.Name = "turnRightButton";
            this.turnRightButton.Size = new System.Drawing.Size(40, 23);
            this.turnRightButton.TabIndex = 4;
            this.turnRightButton.Click += new System.EventHandler(this.turnRightButton_Click);
            // 
            // prevButton
            // 
            this.prevButton.AllowFocus = false;
            this.prevButton.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.prevButton.Image = ((System.Drawing.Image)(resources.GetObject("prevButton.Image")));
            this.prevButton.Location = new System.Drawing.Point(3, 4);
            this.prevButton.Name = "prevButton";
            this.prevButton.Size = new System.Drawing.Size(40, 23);
            this.prevButton.TabIndex = 0;
            this.prevButton.TabStop = false;
            this.prevButton.Click += new System.EventHandler(this.prevButton_Click);
            // 
            // turnLeftButton
            // 
            this.turnLeftButton.AllowFocus = false;
            this.turnLeftButton.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.turnLeftButton.Image = ((System.Drawing.Image)(resources.GetObject("turnLeftButton.Image")));
            this.turnLeftButton.Location = new System.Drawing.Point(110, 4);
            this.turnLeftButton.Name = "turnLeftButton";
            this.turnLeftButton.Size = new System.Drawing.Size(40, 23);
            this.turnLeftButton.TabIndex = 3;
            this.turnLeftButton.Click += new System.EventHandler(this.turnLeftButton_Click);
            // 
            // nextButton
            // 
            this.nextButton.AllowFocus = false;
            this.nextButton.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.nextButton.Image = ((System.Drawing.Image)(resources.GetObject("nextButton.Image")));
            this.nextButton.Location = new System.Drawing.Point(47, 4);
            this.nextButton.Name = "nextButton";
            this.nextButton.Size = new System.Drawing.Size(40, 23);
            this.nextButton.TabIndex = 1;
            this.nextButton.TabStop = false;
            this.nextButton.Click += new System.EventHandler(this.nextButton_Click);
            // 
            // PhotoForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(649, 389);
            this.Controls.Add(this.mainPanel);
            this.Name = "PhotoForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Ежевизитная инвентаризация - Просмотр фото";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.PhotoForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.mainPanel)).EndInit();
            this.mainPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl)).EndInit();
            this.splitContainerControl.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.listBoxControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonsPanel)).EndInit();
            this.buttonsPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl mainPanel;
        private DevExpress.XtraEditors.PanelControl buttonsPanel;
        private DevExpress.XtraEditors.PictureEdit pictureEdit;
        private DevExpress.XtraEditors.SimpleButton nextButton;
        private DevExpress.XtraEditors.SimpleButton prevButton;
        private DevExpress.XtraEditors.ListBoxControl listBoxControl;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.Utils.ToolTipController toolTipController;
        private DevExpress.XtraEditors.SimpleButton turnRightButton;
        private DevExpress.XtraEditors.SimpleButton turnLeftButton;
    }
}