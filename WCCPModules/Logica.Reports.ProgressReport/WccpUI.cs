using System;
using System.Windows.Forms;
using Logica.Reports.Common;
using System.ComponentModel.Composition;
using ModularWinApp.Core.Interfaces;
using Logica.Reports.DataAccess;

namespace WccpReporting
{
    [Export(typeof(IStartupClass))]
    [ExportMetadata("ModuleName", "wccpProgressReport.dll")]
    class WccpUI : IStartupClass
    {
        private WccpUIControl _reportControl;
        private string _reportCaption;

        public Control ReportControl
        {
            get { return _reportControl; }
        }

        public string ReportCaption
        {
            get { return _reportCaption; }
        }

        private int tab = 0;

        //���������� ������ ������
        public string GetVersion()
        {
            return System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();
        }

        public int ShowUI(int reportId, string reportCaption)
        {
            try
            {
                WCCPConst.ConnectionString = HostConfiguration.DBSqlConnection;

                FormDialog fmInputProgressReportDataForm = new FormDialog();

                try
                {
                    fmInputProgressReportDataForm.ReportName = reportCaption;

                    if (fmInputProgressReportDataForm.ShowDialog() == DialogResult.OK)
                    {

                        fmInputProgressReportDataForm.ReportName = fmInputProgressReportDataForm.ReportName + " " + fmInputProgressReportDataForm.WaveName;
                        _reportCaption = fmInputProgressReportDataForm.ReportName;

                        ReportControlOptions.ReportTitle = reportCaption;
                        ReportControlOptions.WaveId = fmInputProgressReportDataForm.WaveId;
                        ReportControlOptions.WaveName = fmInputProgressReportDataForm.WaveName;
                        ReportControlOptions.IsActiveWave = fmInputProgressReportDataForm.IsActiveWave;

                        _reportControl = new WccpUIControl();

                        return 0;
                    }
                }
                finally
                {
                    fmInputProgressReportDataForm.Dispose();
                }
            }
            catch (Exception e)
            {
                ErrorManager.ShowErrorBox(e.Message);
            }

            return 1;
        }

        public void CloseUI()
        { }

        public bool AllowClose()
        {
            return true;
        }
    }
}
