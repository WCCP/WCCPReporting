using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.IO;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.Diagnostics;
using System.Reflection;
using DevExpress.XtraPivotGrid;
using DevExpress.XtraPrinting;
using Logica.Reports.BaseReportControl;
using Logica.Reports.Common.WaitWindow;
using ModularWinApp;

namespace WccpReporting
{
    public partial class WccpUIControl : DevExpress.XtraEditors.XtraUserControl
    {
        private string _ReportTitle;
        private string _userLogin;
        private string _folderForExport = "ExportedReports";
        private string _userExportRoot;
        public string ReportTitle
        {
            get { return _ReportTitle; }
            set { _ReportTitle = value; lbReportTitle.Text = value; }
        }

        public string UserLogin
        {
            get { return _userLogin; }
            set
            {
                string temp = string.Empty;
                _userLogin = string.Empty;

                if (value.Contains("\\"))
                {
                    temp = value.Remove(0, value.LastIndexOf('\\') + 1);
                }
                else
                {
                    temp = value;
                }
                _userLogin += temp.Replace('.', '_');
            }
        }

        public int WaveId
        { get; set; }

        private string _WaveName;
        public string WaveName
        {
            get { return _WaveName; }
            set { _WaveName = value; edWaveText.Text = value; }
        }

        public bool IsActiveWave
        { get; set; }

        public WccpUIControl()
        {
            InitializeComponent();
        }

        private void ReportControl_Load(object sender, EventArgs e)
        {
            ReportTitle = ReportControlOptions.ReportTitle;
            WaveId = ReportControlOptions.WaveId;
            WaveName = ReportControlOptions.WaveName;
            IsActiveWave = ReportControlOptions.IsActiveWave;

            try
            {

                Cursor.Current = Cursors.WaitCursor;
                WaitManager.StartWait();
                pivotGridControl.DataSource = DataAccessProvider.GetReportData(WaveId);
            }
            catch (Exception ex)
            {
                MessageBox.Show("������ Exception:" + ex.Message, "������", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
                WaitManager.StopWait();
            }
        }

        private string SelectFilePath(ExportToType type)
        {
            String res = String.Empty;
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Title = "���������";
            sfd.InitialDirectory = Assembly.GetExecutingAssembly().Location;
            sfd.FileName = "Report";
            sfd.Filter = String.Format("(*.{0})|*.{0}", type.ToString().ToLower());
            sfd.FilterIndex = 1;
            sfd.OverwritePrompt = true;
            sfd.RestoreDirectory = true;

            if (sfd.ShowDialog() != DialogResult.Cancel)
            {
                res = sfd.FileName;
            }
            return res;
        }

        //����� � Excel
        private void tbtnReportToExcel_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            string _path = SelectFilePath(ExportToType.Xlsx);
                try
                {
                    if (!string.IsNullOrEmpty(_path))
                    {
                        pivotGridControl.ExportToXlsx(_path);
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "��������������", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
        }

        //�������� �����
        private void tbtnReportRefresh_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                FormDialog fmInputProgressReportDataForm = new FormDialog();
                try
                {
                    fmInputProgressReportDataForm.ReportName = ReportTitle;
                    if (fmInputProgressReportDataForm.ShowDialog() == DialogResult.OK)
                    {
                        WaveName = fmInputProgressReportDataForm.WaveName;

                        Cursor.Current = Cursors.WaitCursor;
                        try
                        {
                            pivotGridControl.DataSource = DataAccessProvider.GetReportData(fmInputProgressReportDataForm.WaveId);
                        }
                        finally
                        {
                            Cursor.Current = Cursors.Default;
                        }
                    }
                }
                finally
                {
                    fmInputProgressReportDataForm.Dispose();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("������ Exception:" + ex.Message, "������", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void pivotGridControl_CustomDrawFieldValue(object sender, DevExpress.XtraPivotGrid.PivotCustomDrawFieldValueEventArgs e)
        {
            if (e.Value == null)
            {
                return;
            }
            if (e.Field == pivotGridField12 && e.Value is bool)
            {
                e.Info.Caption = (bool)e.Value ? "��" : "���";
            }
        }

        private void pivotGridControl_CustomFilterPopupItems(object sender, DevExpress.XtraPivotGrid.PivotCustomFilterPopupItemsEventArgs e)
        {
            if (e.Field == pivotGridField12)
            {
                foreach (var item in e.Items)
                {
                    item.Text = (bool)item.Value ? "��" : "���";
                }
            }
        }
    }
}
