﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BLToolkit.Data;
using Logica.Reports.DataAccess;
using System.Data;

namespace WccpReporting
{
    public static class DataAccessProvider
    {
        private static string qrySelectActiveWave = @"select Wave_ID, WaveName from DW_TomasResearch_Wave 
                                               where isActive = 1 and Country_id in (%)
                                               order by WaveName";
        private static string qrySelectAllWave = @"select Wave_ID, WaveName 
                                            from DW_TomasResearch_Wave 
                                            where Wave_id > 0 and Country_id in (%)
                                            order by WaveName";

        private static DbManager _db;

        private static DbManager Db
        {
            get
            {
                if (_db == null)
                {
                    DbManager.DefaultConfiguration = ""; //to reset possible previous changes
                    DbManager.AddConnectionString(HostConfiguration.DBSqlConnection);
                    _db = new DbManager();
                    _db.Command.CommandTimeout = 15 * 60; // 15 minutes by default
                }
                return _db;
            }
        }

        public static DataTable GetReportData(int waveId)
        {
            DataTable ds = Db.SetSpCommand("DW_TomasResearch_ProgressReport",
                Db.Parameter("wave_id", waveId)).ExecuteDataTable();

            return ds;
        }

        public static DataTable GetCountries()
        {
            DataTable ds = Db.SetCommand("select CountryID from dbo.fn_CountryByUser(system_user)").ExecuteDataTable();

            return ds;
        }

        public static DataTable GetAllWaves(string contriesList)
        {
            string lSelectAllWave = qrySelectAllWave.Replace("%", contriesList);
            DataTable ds = Db.SetCommand(lSelectAllWave).ExecuteDataTable();

            return ds;
        }

        public static DataTable GetActiveWaves(string contriesList)
        {
            string lSelectActiveWave = qrySelectActiveWave.Replace("%", contriesList);
            DataTable ds = Db.SetCommand(lSelectActiveWave).ExecuteDataTable();

            return ds;
        }
    }
}
