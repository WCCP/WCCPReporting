﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("wccpProgressReport")]
[assembly: AssemblyDescription("Thomas Research Progress Report")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Logica, SoftServe Inc.")]
[assembly: AssemblyProduct("WCCP Reporting")]
[assembly: AssemblyCopyright("Copyright (c) 2009 - 2010 Logica, 2011 - 2014 SoftServe Inc.")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("bfc4ad70-2d40-4545-8f2b-d61ffd88baeb")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("2.0.5.64999")]
[assembly: AssemblyFileVersion("2.0.5.64999")]
