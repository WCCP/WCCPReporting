using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Logica.Reports.ProgressReport.Properties;

namespace WccpReporting
{
    public partial class FormDialog : Form
    {
        private DataTable _userCountries = new DataTable();
        private DataTable _waves = new DataTable();

        private string _reportName;
        public string ReportName
        {
            get { return _reportName; }
            set
            {
                _reportName = value;
                edReportName.Text = value;
            }
        }

        public int WaveId
        {
            get;
            set;
        }

        public string WaveName
        {
            get;
            set;
        }

        public bool IsActiveWave
        {
            get;
            set;
        }

        public FormDialog()
        {
            InitializeComponent();
        }

        private void btnYes_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(cboxWave.Text))
            {
                XtraMessageBox.Show("����� ������� �����", "��������������", MessageBoxButtons.OK,
                                    MessageBoxIcon.Warning);
                return;
            }

            WaveId = (int)cboxWave.EditValue;
            WaveName = cboxWave.Text;
            if (chboxOnlyActiveWave.Checked)
            {
                IsActiveWave = true;
            }
            else
            {
                IsActiveWave = false;
            }
            Settings.Default.IsActive = IsActiveWave;
            Settings.Default.WaveId = WaveId;
            Settings.Default.Save();
            //
            DialogResult = DialogResult.OK;
        }

        private void btnHelp_Click(object sender, EventArgs e)
        {
            MessageBox.Show(WCCPConst.msgNoHelp, "����������", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void InputProgressReportDataForm_Shown(object sender, EventArgs e)
        {
            if (Settings.Default.WaveId > 0)
            {
                chboxOnlyActiveWave.Checked = Settings.Default.IsActive;
                cboxWave.EditValue = Settings.Default.WaveId;
            }
            else
            {
                chboxOnlyActiveWave.Checked = true;
            }
            cboxWave.Focus();
        }

        //��������� �����
        private void InputProgressReportDataForm_Load(object sender, EventArgs e)
        {
            WaveId = -1;
            WaveName = String.Empty;
            IsActiveWave = true;
            try
            {
                _userCountries = DataAccessProvider.GetCountries();

                if (_userCountries.Rows.Count == 0)
                    throw new Exception("���������� ���������� ������ ������������");

                _waves = DataAccessProvider.GetActiveWaves(GetCountriesList());
                cboxWave.Properties.DataSource = _waves;

                if (_waves.Rows.Count > 0)
                {
                    cboxWave.ItemIndex = 0;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(WCCPConst.msgErrorCreateWaveList + "\n(" + ex.Message + ")", "������", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private string GetCountriesList()
        {
            return string.Join(",", (from DataRow row in _userCountries.Rows select row.Field<int>("CountryID").ToString()).ToArray());
        }

        private void chboxOnlyActiveWave_CheckedChanged(object sender, EventArgs e)
        {
            if (_userCountries.Rows.Count == 0)
            {
                return;
            }
                        
            try
            {
                _waves = chboxOnlyActiveWave.Checked ? DataAccessProvider.GetActiveWaves(GetCountriesList()) : DataAccessProvider.GetAllWaves(GetCountriesList());
                cboxWave.Properties.DataSource = _waves;

                if (_waves.Rows.Count > 0)
                {
                    cboxWave.ItemIndex = 0;
                }

                cboxWave.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show(WCCPConst.msgErrorCreateWaveList + "\n(" + ex.Message + ")", "������", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

    }
}