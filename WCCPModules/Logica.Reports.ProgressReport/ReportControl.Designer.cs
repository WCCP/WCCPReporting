namespace WccpReporting
{
    partial class WccpUIControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(WccpUIControl));
            this.pnReportSheet = new DevExpress.XtraEditors.PanelControl();
            this.tcReports = new DevExpress.XtraTab.XtraTabControl();
            this.tpTP = new DevExpress.XtraTab.XtraTabPage();
            this.pivotGridControl = new DevExpress.XtraPivotGrid.PivotGridControl();
            this.pivotGridField1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridField2 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridField3 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridField4 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridField5 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridField6 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridField7 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridField8 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridField12 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridField9 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridField10 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridField11 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridField24 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridField25 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridField26 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pnReportHeader = new DevExpress.XtraEditors.PanelControl();
            this.edWaveText = new DevExpress.XtraEditors.TextEdit();
            this.barManagerReport = new DevExpress.XtraBars.BarManager();
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.tbtnReportToExcel = new DevExpress.XtraBars.BarButtonItem();
            this.tbtnReportRefresh = new DevExpress.XtraBars.BarButtonItem();
            this.standaloneBarDockControl1 = new DevExpress.XtraBars.StandaloneBarDockControl();
            this.barAndDockingController1 = new DevExpress.XtraBars.BarAndDockingController();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.imgNormal = new DevExpress.Utils.ImageCollection();
            this.imgLarge = new DevExpress.Utils.ImageCollection();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.lbReportTitle = new System.Windows.Forms.Label();
            this.pnReportToolBar = new DevExpress.XtraEditors.PanelControl();
            this.dWTomasResearchProgressReportBindingSource = new System.Windows.Forms.BindingSource();
            this.oleDbConnection1 = new System.Data.OleDb.OleDbConnection();
            this.oleDbCommand1 = new System.Data.OleDb.OleDbCommand();
            this.oleDbSelectCommand1 = new System.Data.OleDb.OleDbCommand();
            this.oleDbInsertCommand1 = new System.Data.OleDb.OleDbCommand();
            this.oleDbUpdateCommand1 = new System.Data.OleDb.OleDbCommand();
            this.oleDbDeleteCommand1 = new System.Data.OleDb.OleDbCommand();
            this.oleDbDataAdapter1 = new System.Data.OleDb.OleDbDataAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.pnReportSheet)).BeginInit();
            this.pnReportSheet.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tcReports)).BeginInit();
            this.tcReports.SuspendLayout();
            this.tpTP.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pivotGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnReportHeader)).BeginInit();
            this.pnReportHeader.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.edWaveText.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManagerReport)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barAndDockingController1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgNormal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgLarge)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnReportToolBar)).BeginInit();
            this.pnReportToolBar.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dWTomasResearchProgressReportBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // pnReportSheet
            // 
            this.pnReportSheet.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnReportSheet.Controls.Add(this.tcReports);
            this.pnReportSheet.Controls.Add(this.pnReportHeader);
            this.pnReportSheet.Controls.Add(this.pnReportToolBar);
            this.pnReportSheet.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnReportSheet.Location = new System.Drawing.Point(0, 0);
            this.pnReportSheet.Name = "pnReportSheet";
            this.pnReportSheet.Size = new System.Drawing.Size(877, 425);
            this.pnReportSheet.TabIndex = 0;
            // 
            // tcReports
            // 
            this.tcReports.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tcReports.HeaderLocation = DevExpress.XtraTab.TabHeaderLocation.Bottom;
            this.tcReports.Location = new System.Drawing.Point(0, 102);
            this.tcReports.Name = "tcReports";
            this.tcReports.SelectedTabPage = this.tpTP;
            this.tcReports.Size = new System.Drawing.Size(877, 323);
            this.tcReports.TabIndex = 2;
            this.tcReports.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.tpTP});
            // 
            // tpTP
            // 
            this.tpTP.Controls.Add(this.pivotGridControl);
            this.tpTP.Name = "tpTP";
            this.tpTP.Size = new System.Drawing.Size(871, 295);
            this.tpTP.Text = "�������� - ��";
            // 
            // pivotGridControl
            // 
            this.pivotGridControl.Cursor = System.Windows.Forms.Cursors.Default;
            this.pivotGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pivotGridControl.Fields.AddRange(new DevExpress.XtraPivotGrid.PivotGridField[] {
            this.pivotGridField1,
            this.pivotGridField2,
            this.pivotGridField3,
            this.pivotGridField4,
            this.pivotGridField5,
            this.pivotGridField6,
            this.pivotGridField7,
            this.pivotGridField8,
            this.pivotGridField12,
            this.pivotGridField9,
            this.pivotGridField10,
            this.pivotGridField11,
            this.pivotGridField24,
            this.pivotGridField25,
            this.pivotGridField26});
            this.pivotGridControl.Location = new System.Drawing.Point(0, 0);
            this.pivotGridControl.Name = "pivotGridControl";
            this.pivotGridControl.OptionsCustomization.AllowEdit = false;
            this.pivotGridControl.OptionsDataField.Area = DevExpress.XtraPivotGrid.PivotDataArea.RowArea;
            this.pivotGridControl.OptionsDataField.AreaIndex = 2;
            this.pivotGridControl.OptionsDataField.Caption = "������";
            this.pivotGridControl.OptionsMenu.EnableHeaderAreaMenu = false;
            this.pivotGridControl.Size = new System.Drawing.Size(871, 295);
            this.pivotGridControl.TabIndex = 3;
            this.pivotGridControl.CustomFilterPopupItems += new DevExpress.XtraPivotGrid.PivotCustomFilterPopupItemsEventHandler(this.pivotGridControl_CustomFilterPopupItems);
            this.pivotGridControl.CustomDrawFieldValue += new DevExpress.XtraPivotGrid.PivotCustomDrawFieldValueEventHandler(this.pivotGridControl_CustomDrawFieldValue);
            // 
            // pivotGridField1
            // 
            this.pivotGridField1.Area = DevExpress.XtraPivotGrid.PivotArea.RowArea;
            this.pivotGridField1.AreaIndex = 0;
            this.pivotGridField1.Caption = "��";
            this.pivotGridField1.FieldName = "��";
            this.pivotGridField1.Name = "pivotGridField1";
            // 
            // pivotGridField2
            // 
            this.pivotGridField2.Area = DevExpress.XtraPivotGrid.PivotArea.RowArea;
            this.pivotGridField2.AreaIndex = 1;
            this.pivotGridField2.Caption = "���";
            this.pivotGridField2.FieldName = "���";
            this.pivotGridField2.Name = "pivotGridField2";
            // 
            // pivotGridField3
            // 
            this.pivotGridField3.AreaIndex = 0;
            this.pivotGridField3.Caption = "��";
            this.pivotGridField3.FieldName = "��";
            this.pivotGridField3.Name = "pivotGridField3";
            // 
            // pivotGridField4
            // 
            this.pivotGridField4.AreaIndex = 1;
            this.pivotGridField4.Caption = "��";
            this.pivotGridField4.FieldName = "��";
            this.pivotGridField4.Name = "pivotGridField4";
            // 
            // pivotGridField5
            // 
            this.pivotGridField5.AreaIndex = 5;
            this.pivotGridField5.Caption = "�������";
            this.pivotGridField5.FieldName = "�������";
            this.pivotGridField5.Name = "pivotGridField5";
            // 
            // pivotGridField6
            // 
            this.pivotGridField6.AreaIndex = 6;
            this.pivotGridField6.Caption = "����������";
            this.pivotGridField6.FieldName = "����������";
            this.pivotGridField6.Name = "pivotGridField6";
            // 
            // pivotGridField7
            // 
            this.pivotGridField7.AreaIndex = 7;
            this.pivotGridField7.Caption = "��";
            this.pivotGridField7.FieldName = "��";
            this.pivotGridField7.Name = "pivotGridField7";
            // 
            // pivotGridField8
            // 
            this.pivotGridField8.AreaIndex = 8;
            this.pivotGridField8.Caption = "�����(������)";
            this.pivotGridField8.FieldName = "�����(������)";
            this.pivotGridField8.Name = "pivotGridField8";
            // 
            // pivotGridField12
            // 
            this.pivotGridField12.AreaIndex = 9;
            this.pivotGridField12.Caption = "�� �� ��������";
            this.pivotGridField12.FieldName = "inRoute";
            this.pivotGridField12.Name = "pivotGridField12";
            // 
            // pivotGridField9
            // 
            this.pivotGridField9.Area = DevExpress.XtraPivotGrid.PivotArea.ColumnArea;
            this.pivotGridField9.AreaIndex = 0;
            this.pivotGridField9.Caption = "����������";
            this.pivotGridField9.FieldName = "����������";
            this.pivotGridField9.Name = "pivotGridField9";
            // 
            // pivotGridField10
            // 
            this.pivotGridField10.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
            this.pivotGridField10.AreaIndex = 0;
            this.pivotGridField10.Caption = "���-�� ��";
            this.pivotGridField10.FieldName = "�����";
            this.pivotGridField10.Name = "pivotGridField10";
            this.pivotGridField10.Options.AllowEdit = false;
            this.pivotGridField10.SummaryType = DevExpress.Data.PivotGrid.PivotSummaryType.Count;
            // 
            // pivotGridField11
            // 
            this.pivotGridField11.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
            this.pivotGridField11.AreaIndex = 1;
            this.pivotGridField11.Caption = "% ��";
            this.pivotGridField11.CellFormat.FormatString = "p0";
            this.pivotGridField11.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.pivotGridField11.FieldName = "�����";
            this.pivotGridField11.GrandTotalCellFormat.FormatString = "p0";
            this.pivotGridField11.GrandTotalCellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.pivotGridField11.GrandTotalText = "�����";
            this.pivotGridField11.Name = "pivotGridField11";
            this.pivotGridField11.Options.AllowEdit = false;
            this.pivotGridField11.SummaryDisplayType = DevExpress.Data.PivotGrid.PivotSummaryDisplayType.PercentOfRow;
            this.pivotGridField11.SummaryType = DevExpress.Data.PivotGrid.PivotSummaryType.Count;
            this.pivotGridField11.TotalCellFormat.FormatString = "p0";
            this.pivotGridField11.TotalCellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.pivotGridField11.TotalValueFormat.FormatString = "p0";
            this.pivotGridField11.TotalValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.pivotGridField11.ValueFormat.FormatString = "p0";
            this.pivotGridField11.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            // 
            // pivotGridField24
            // 
            this.pivotGridField24.AreaIndex = 2;
            this.pivotGridField24.Caption = "�����";
            this.pivotGridField24.FieldName = "�����";
            this.pivotGridField24.Name = "pivotGridField24";
            // 
            // pivotGridField25
            // 
            this.pivotGridField25.AreaIndex = 3;
            this.pivotGridField25.Caption = "�������";
            this.pivotGridField25.FieldName = "�������";
            this.pivotGridField25.Name = "pivotGridField25";
            // 
            // pivotGridField26
            // 
            this.pivotGridField26.AreaIndex = 4;
            this.pivotGridField26.Caption = "���������";
            this.pivotGridField26.FieldName = "���������";
            this.pivotGridField26.Name = "pivotGridField26";
            // 
            // pnReportHeader
            // 
            this.pnReportHeader.Controls.Add(this.edWaveText);
            this.pnReportHeader.Controls.Add(this.labelControl1);
            this.pnReportHeader.Controls.Add(this.lbReportTitle);
            this.pnReportHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnReportHeader.Location = new System.Drawing.Point(0, 40);
            this.pnReportHeader.Name = "pnReportHeader";
            this.pnReportHeader.Size = new System.Drawing.Size(877, 62);
            this.pnReportHeader.TabIndex = 1;
            // 
            // edWaveText
            // 
            this.edWaveText.Location = new System.Drawing.Point(52, 36);
            this.edWaveText.MenuManager = this.barManagerReport;
            this.edWaveText.Name = "edWaveText";
            this.edWaveText.Properties.Appearance.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.edWaveText.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.edWaveText.Properties.Appearance.Options.UseBackColor = true;
            this.edWaveText.Properties.Appearance.Options.UseFont = true;
            this.edWaveText.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.edWaveText.Size = new System.Drawing.Size(200, 20);
            this.edWaveText.TabIndex = 2;
            this.edWaveText.TabStop = false;
            // 
            // barManagerReport
            // 
            this.barManagerReport.AllowCustomization = false;
            this.barManagerReport.AllowMoveBarOnToolbar = false;
            this.barManagerReport.AllowQuickCustomization = false;
            this.barManagerReport.AllowShowToolbarsPopup = false;
            this.barManagerReport.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar2});
            this.barManagerReport.Controller = this.barAndDockingController1;
            this.barManagerReport.DockControls.Add(this.barDockControlTop);
            this.barManagerReport.DockControls.Add(this.barDockControlBottom);
            this.barManagerReport.DockControls.Add(this.barDockControlLeft);
            this.barManagerReport.DockControls.Add(this.barDockControlRight);
            this.barManagerReport.DockControls.Add(this.standaloneBarDockControl1);
            this.barManagerReport.Form = this;
            this.barManagerReport.Images = this.imgNormal;
            this.barManagerReport.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.tbtnReportToExcel,
            this.tbtnReportRefresh});
            this.barManagerReport.LargeImages = this.imgLarge;
            this.barManagerReport.MainMenu = this.bar2;
            this.barManagerReport.MaxItemId = 3;
            this.barManagerReport.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit1});
            // 
            // bar2
            // 
            this.bar2.BarName = "menuReport";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Standalone;
            this.bar2.FloatLocation = new System.Drawing.Point(217, 125);
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.None, this.tbtnReportToExcel, "", false, false, true, 0),
            new DevExpress.XtraBars.LinkPersistInfo(this.tbtnReportRefresh, true)});
            this.bar2.OptionsBar.AllowQuickCustomization = false;
            this.bar2.OptionsBar.MultiLine = true;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.StandaloneBarDockControl = this.standaloneBarDockControl1;
            this.bar2.Text = "menuReport";
            // 
            // tbtnReportToExcel
            // 
            this.tbtnReportToExcel.Caption = "������� � xlsx";
            this.tbtnReportToExcel.Hint = "����� � Excel";
            this.tbtnReportToExcel.Id = 0;
            this.tbtnReportToExcel.LargeImageIndex = 3;
            this.tbtnReportToExcel.Name = "tbtnReportToExcel";
            this.tbtnReportToExcel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.tbtnReportToExcel_ItemClick);
            // 
            // tbtnReportRefresh
            // 
            this.tbtnReportRefresh.Caption = "��������� ������";
            this.tbtnReportRefresh.Hint = "��������� ������";
            this.tbtnReportRefresh.Id = 2;
            this.tbtnReportRefresh.LargeImageIndex = 2;
            this.tbtnReportRefresh.Name = "tbtnReportRefresh";
            this.tbtnReportRefresh.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.tbtnReportRefresh_ItemClick);
            // 
            // standaloneBarDockControl1
            // 
            this.standaloneBarDockControl1.AutoSize = true;
            this.standaloneBarDockControl1.CausesValidation = false;
            this.standaloneBarDockControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.standaloneBarDockControl1.Location = new System.Drawing.Point(2, 2);
            this.standaloneBarDockControl1.Name = "standaloneBarDockControl1";
            this.standaloneBarDockControl1.Size = new System.Drawing.Size(873, 36);
            this.standaloneBarDockControl1.Text = "standaloneBarDockControl1";
            // 
            // barAndDockingController1
            // 
            this.barAndDockingController1.PaintStyleName = "WindowsXP";
            this.barAndDockingController1.PropertiesBar.AllowLinkLighting = false;
            this.barAndDockingController1.PropertiesBar.DefaultGlyphSize = new System.Drawing.Size(16, 16);
            this.barAndDockingController1.PropertiesBar.DefaultLargeGlyphSize = new System.Drawing.Size(32, 32);
            this.barAndDockingController1.PropertiesBar.LargeIcons = true;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(877, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 425);
            this.barDockControlBottom.Size = new System.Drawing.Size(877, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 0);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 425);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(877, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 425);
            // 
            // imgNormal
            // 
            this.imgNormal.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imgNormal.ImageStream")));
            // 
            // imgLarge
            // 
            this.imgLarge.ImageSize = new System.Drawing.Size(24, 24);
            this.imgLarge.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imgLarge.ImageStream")));
            this.imgLarge.Images.SetKeyName(0, "excel.bmp");
            this.imgLarge.Images.SetKeyName(1, "book_ok_32.png");
            this.imgLarge.Images.SetKeyName(2, "briefcase_ok_24.png");
            this.imgLarge.Images.SetKeyName(3, "Excel_32.bmp");
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl1.Location = new System.Drawing.Point(8, 39);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(38, 13);
            this.labelControl1.TabIndex = 1;
            this.labelControl1.Text = "�����:";
            // 
            // lbReportTitle
            // 
            this.lbReportTitle.Dock = System.Windows.Forms.DockStyle.Top;
            this.lbReportTitle.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lbReportTitle.Location = new System.Drawing.Point(2, 2);
            this.lbReportTitle.Name = "lbReportTitle";
            this.lbReportTitle.Size = new System.Drawing.Size(873, 18);
            this.lbReportTitle.TabIndex = 0;
            this.lbReportTitle.Text = "Title";
            this.lbReportTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pnReportToolBar
            // 
            this.pnReportToolBar.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.pnReportToolBar.Controls.Add(this.standaloneBarDockControl1);
            this.pnReportToolBar.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnReportToolBar.Location = new System.Drawing.Point(0, 0);
            this.pnReportToolBar.Name = "pnReportToolBar";
            this.pnReportToolBar.Size = new System.Drawing.Size(877, 40);
            this.pnReportToolBar.TabIndex = 0;
            // 
            // oleDbCommand1
            // 
            this.oleDbCommand1.CommandText = "DW_TomasResearch_ProgressReport";
            this.oleDbCommand1.CommandTimeout = 300;
            this.oleDbCommand1.CommandType = System.Data.CommandType.StoredProcedure;
            this.oleDbCommand1.Connection = this.oleDbConnection1;
            // 
            // oleDbSelectCommand1
            // 
            this.oleDbSelectCommand1.CommandTimeout = 300;
            this.oleDbSelectCommand1.Connection = this.oleDbConnection1;
            // 
            // oleDbDataAdapter1
            // 
            this.oleDbDataAdapter1.DeleteCommand = this.oleDbDeleteCommand1;
            this.oleDbDataAdapter1.InsertCommand = this.oleDbInsertCommand1;
            this.oleDbDataAdapter1.SelectCommand = this.oleDbSelectCommand1;
            this.oleDbDataAdapter1.UpdateCommand = this.oleDbUpdateCommand1;
            // 
            // WccpUIControl
            // 
            this.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Appearance.Options.UseFont = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.pnReportSheet);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Name = "WccpUIControl";
            this.Size = new System.Drawing.Size(877, 425);
            this.Load += new System.EventHandler(this.ReportControl_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pnReportSheet)).EndInit();
            this.pnReportSheet.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tcReports)).EndInit();
            this.tcReports.ResumeLayout(false);
            this.tpTP.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pivotGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnReportHeader)).EndInit();
            this.pnReportHeader.ResumeLayout(false);
            this.pnReportHeader.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.edWaveText.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManagerReport)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barAndDockingController1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgNormal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgLarge)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnReportToolBar)).EndInit();
            this.pnReportToolBar.ResumeLayout(false);
            this.pnReportToolBar.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dWTomasResearchProgressReportBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl pnReportSheet;
        private DevExpress.XtraEditors.PanelControl pnReportToolBar;
        private DevExpress.XtraBars.BarManager barManagerReport;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.StandaloneBarDockControl standaloneBarDockControl1;
        private DevExpress.XtraBars.BarAndDockingController barAndDockingController1;
        private DevExpress.XtraBars.BarButtonItem tbtnReportToExcel;
        private DevExpress.Utils.ImageCollection imgNormal;
        private DevExpress.Utils.ImageCollection imgLarge;
        private DevExpress.XtraEditors.PanelControl pnReportHeader;
        private System.Windows.Forms.Label lbReportTitle;
        private DevExpress.XtraEditors.TextEdit edWaveText;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
        private DevExpress.XtraBars.BarButtonItem tbtnReportRefresh;
        private System.Data.OleDb.OleDbConnection oleDbConnection1;
        private System.Data.OleDb.OleDbCommand oleDbCommand1;
        private System.Data.OleDb.OleDbCommand oleDbSelectCommand1;
        private System.Data.OleDb.OleDbCommand oleDbInsertCommand1;
        private System.Data.OleDb.OleDbCommand oleDbUpdateCommand1;
        private System.Data.OleDb.OleDbCommand oleDbDeleteCommand1;
        private System.Data.OleDb.OleDbDataAdapter oleDbDataAdapter1;
        private System.Windows.Forms.BindingSource dWTomasResearchProgressReportBindingSource;
        private DevExpress.XtraTab.XtraTabControl tcReports;
        private DevExpress.XtraTab.XtraTabPage tpTP;
        private DevExpress.XtraPivotGrid.PivotGridControl pivotGridControl;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridField1;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridField2;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridField3;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridField4;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridField5;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridField6;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridField7;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridField8;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridField9;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridField10;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridField11;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridField24;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridField25;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridField26;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridField12;
    }
}
