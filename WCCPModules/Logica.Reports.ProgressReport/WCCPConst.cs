using System;

namespace WccpReporting
{
    static class ReportControlOptions
    {
        static public  string ReportTitle;
        static public  int WaveId;
        static public  string WaveName;
        static public  bool IsActiveWave;
        static public  bool IsSkin= false;
    };

    static class WCCPConst
    {
        public static string root {
            //get { return WCCPAPI.GetApplicationPath(); }
            get { return AppDomain.CurrentDomain.BaseDirectory; }
        }
        static public string Version = "2.0.5.0";
        static public string ConnectionString;
        static public string frptexlTPProgressReport  = "TPProgressReport.xls";
        static public string frptexlTTProgressReport  = "TTProgressReport.xls";
        static public string msgNoHelp              = "� ������ ������ ��������� ������� �����������";
        static public string msgErrorCreateWaveList = "�� ���� ������� ������ ����";
        static public string msgErrorSaveOptions    = "�� ���� ��������� �����";
    }

}
