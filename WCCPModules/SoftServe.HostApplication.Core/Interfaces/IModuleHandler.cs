﻿namespace ModularWinApp.Core.Interfaces
{
    public interface IModuleHandler
    {
        void InitializeModules();

        IStartupClass GetWccpModuleInstance(string moduleName);

        //IDataModule DataModule { get; }
    }
}
