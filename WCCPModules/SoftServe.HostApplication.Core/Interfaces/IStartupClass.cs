﻿using System.Windows.Forms;

namespace ModularWinApp.Core.Interfaces
{
    public interface IStartupClass
    {
        Control ReportControl { get; }
        string ReportCaption { get; }

        string GetVersion();
        int ShowUI(int reportId, string reportCaption);
        bool AllowClose();
        void CloseUI();
    }
}
