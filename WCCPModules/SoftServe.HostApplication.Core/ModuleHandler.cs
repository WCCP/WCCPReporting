﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.Configuration;

// MEF References
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.ComponentModel.Composition.Primitives;
using System.ComponentModel.Composition.ReflectionModel;
using System.ComponentModel.Composition.AttributedModel;
//Core Reference
using System.Windows.Forms;
using ModularWinApp.Core.Interfaces;
using System.IO;

namespace ModularWinApp.Core
{
    [Export(typeof(IModuleHandler))]
    public class ModuleHandler : IDisposable, IModuleHandler
    {
        // AggregateCatalog stores the MEF Catalogs
        AggregateCatalog _catalog = new AggregateCatalog();
        CompositionContainer _container;

        public void InitializeModules()
        {
            // Create a new catalog from the ModularWinApp.Core
            _catalog.Catalogs.Add(new DirectoryCatalog(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "*.dll"));

            // Create the CompositionContainer
            //CompositionContainer
            _container = new CompositionContainer(_catalog);

            // Do the MEF Magic
            try
            {
                _container.ComposeParts(this);
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message + "\r\n" + e.StackTrace);

                var typeLoadException = e as ReflectionTypeLoadException;
                Exception[] loaderExceptions = typeLoadException.LoaderExceptions;

                if (loaderExceptions != null)
                {
                    foreach (Exception loaderException in loaderExceptions)
                    {
                        MessageBox.Show(loaderException.Message + "\r\n" + loaderException.StackTrace);
                    }
                }
            }
        }

        public IStartupClass GetWccpModuleInstance(string moduleName)
        {
            try
            {
                IStartupClass instance = _container.GetExports<IStartupClass, IModuleAttribute>()
                               .Where(e => e.Metadata.ModuleName.ToUpper().Equals(moduleName.ToUpper()))
                               .Select(e => e.Value)
                               .FirstOrDefault();

                return instance;
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message + "\r\n" + e.StackTrace);

                var typeLoadException = e as ReflectionTypeLoadException;
                Exception[] loaderExceptions = typeLoadException.LoaderExceptions;

                if (loaderExceptions != null)
                {
                    foreach (Exception loaderException in loaderExceptions)
                    {
                        MessageBox.Show(loaderException.Message + "\r\n" + loaderException.StackTrace);
                    }
                }
            }

            return null;
        }

        public void Dispose()
        {
            _catalog.Dispose();
            _catalog = null;
        }
    }
}
